+++
type = "question"
title = "Listening to wireless traffic through wired connection to router"
description = '''Hey I was wondering if it is possible on my desktop pc with wired connection to router, no wireless, to listen to all the wifi traffic that goes to the router? So I eg. can listen to the traffic that comes from my smartphone or laptop from my desktop pc? If possible, can it be unencrypted data as I ...'''
date = "2013-03-04T08:52:00Z"
lastmod = "2013-03-04T09:00:00Z"
weight = 19129
keywords = [ "wireless", "packets", "wired", "sniff", "wireshark" ]
aliases = [ "/questions/19129" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Listening to wireless traffic through wired connection to router](/questions/19129/listening-to-wireless-traffic-through-wired-connection-to-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19129-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19129-score" class="post-score" title="current number of votes">0</div><span id="post-19129-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey I was wondering if it is possible on my desktop pc with wired connection to router, no wireless, to listen to all the wifi traffic that goes to the router? So I eg. can listen to the traffic that comes from my smartphone or laptop from my desktop pc?</p><p>If possible, can it be unencrypted data as I have the wired connection to the router, or will it always be encrypted when having a wpa secure network?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-wired" rel="tag" title="see questions tagged &#39;wired&#39;">wired</span> <span class="post-tag tag-link-sniff" rel="tag" title="see questions tagged &#39;sniff&#39;">sniff</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Mar '13, 08:52</strong></p><img src="https://secure.gravatar.com/avatar/7422439eb48d877f251d49df3527a54e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DonTommy&#39;s gravatar image" /><p><span>DonTommy</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DonTommy has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Mar '13, 09:47</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-19129" class="comments-container"></div><div id="comment-tools-19129" class="comment-tools"></div><div class="clear"></div><div id="comment-19129-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19130"></span>

<div id="answer-container-19130" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19130-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19130-score" class="post-score" title="current number of votes">0</div><span id="post-19130-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No. Unless you can persuade the wireless router to forward all packets to the wired port, which may not be possible with many wireless routers and their standard firmware.</p><p>Depending on the router you may be able to install custom router firmware (Google for choices) that may allow you to capture files (tcpdump) on the router for later analysis by Wireshark on your pc.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Mar '13, 09:00</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-19130" class="comments-container"></div><div id="comment-tools-19130" class="comment-tools"></div><div class="clear"></div><div id="comment-19130-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

