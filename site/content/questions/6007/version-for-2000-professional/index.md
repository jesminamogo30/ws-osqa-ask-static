+++
type = "question"
title = "Version for 2000 professional"
description = '''I need a version of wireshark to run on 2000 professional is place where this is available. '''
date = "2011-08-31T07:44:00Z"
lastmod = "2011-08-31T08:42:00Z"
weight = 6007
keywords = [ "versions" ]
aliases = [ "/questions/6007" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Version for 2000 professional](/questions/6007/version-for-2000-professional)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6007-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6007-score" class="post-score" title="current number of votes">0</div><span id="post-6007-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need a version of wireshark to run on 2000 professional is place where this is available.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-versions" rel="tag" title="see questions tagged &#39;versions&#39;">versions</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Aug '11, 07:44</strong></p><img src="https://secure.gravatar.com/avatar/f2df67b371a7d55bed3c1591abd07614?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mschmidtwrx&#39;s gravatar image" /><p><span>mschmidtwrx</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mschmidtwrx has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-6007" class="comments-container"></div><div id="comment-tools-6007" class="comment-tools"></div><div class="clear"></div><div id="comment-6007-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6012"></span>

<div id="answer-container-6012" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6012-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6012-score" class="post-score" title="current number of votes">2</div><span id="post-6012-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The last release that supported Windows 2000 was 1.2.18. The 1.2 branch reached <a href="http://wiki.wireshark.org/Development/LifeCycle">end of life</a> this past June. You can download 1.2.18 from the <a href="http://www.wireshark.org/download/win32/all-versions/">release archives</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Aug '11, 08:42</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-6012" class="comments-container"></div><div id="comment-tools-6012" class="comment-tools"></div><div class="clear"></div><div id="comment-6012-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

