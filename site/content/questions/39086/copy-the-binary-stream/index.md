+++
type = "question"
title = "Copy the Binary Stream"
description = '''I would like to ask if there is a way to copy the binary stream of a packet. I right click to the packet i want, then i select Copy-&amp;gt; Bytes-&amp;gt; Binary Stream, but when i try to paste it in a text editor it doesn&#x27;t work. If i try the same for the Hex Stream everything works fine. I also would lik...'''
date = "2015-01-12T13:06:00Z"
lastmod = "2015-01-13T07:58:00Z"
weight = 39086
keywords = [ "copy-paste", "copy", "copying" ]
aliases = [ "/questions/39086" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Copy the Binary Stream](/questions/39086/copy-the-binary-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39086-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39086-score" class="post-score" title="current number of votes">0</div><span id="post-39086-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to ask if there is a way to copy the binary stream of a packet.</p><p>I right click to the packet i want, then i select Copy-&gt; Bytes-&gt; Binary Stream, but when i try to paste it in a text editor it doesn't work. If i try the same for the Hex Stream everything works fine.</p><p>I also would like to ask if there is another way to copy the values of the Packet Bytes Pane so that i can paste them in a text editor?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-copy-paste" rel="tag" title="see questions tagged &#39;copy-paste&#39;">copy-paste</span> <span class="post-tag tag-link-copy" rel="tag" title="see questions tagged &#39;copy&#39;">copy</span> <span class="post-tag tag-link-copying" rel="tag" title="see questions tagged &#39;copying&#39;">copying</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jan '15, 13:06</strong></p><img src="https://secure.gravatar.com/avatar/d8a12d1a9f0522530f85690edda833ff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Thodoris&#39;s gravatar image" /><p><span>Thodoris</span><br />
<span class="score" title="10 reputation points">10</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Thodoris has no accepted answers">0%</span></p></div></div><div id="comments-container-39086" class="comments-container"></div><div id="comment-tools-39086" class="comment-tools"></div><div class="clear"></div><div id="comment-39086-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39087"></span>

<div id="answer-container-39087" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39087-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39087-score" class="post-score" title="current number of votes">0</div><span id="post-39087-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Maybe using "Follow TCP stream" and then saving as RAW will do the trick?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jan '15, 14:30</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-39087" class="comments-container"><span id="39090"></span><div id="comment-39090" class="comment"><div id="post-39090-score" class="comment-score"></div><div class="comment-text"><p>I have tried it using "Follow UDP stream" but it didn't work.</p></div><div id="comment-39090-info" class="comment-info"><span class="comment-age">(13 Jan '15, 06:16)</span> <span class="comment-user userinfo">Thodoris</span></div></div><span id="39092"></span><div id="comment-39092" class="comment"><div id="post-39092-score" class="comment-score"></div><div class="comment-text"><p>According to the comments found in the source code: XXX - this is not understood by most applications, but can be pasted into the better hex editors - is there something better that we can do?</p><p>So I would not expect the binary stream to be easily pasted in a text editor</p></div><div id="comment-39092-info" class="comment-info"><span class="comment-age">(13 Jan '15, 07:58)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-39087" class="comment-tools"></div><div class="clear"></div><div id="comment-39087-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

