+++
type = "question"
title = "RTT to ACK the segment"
description = ''' Here is a capture of Kerberos encrypted LDAP traffic between the domain controller 2008R2 and Exchange server on Windows Server 2012. Assuming I graphed it correctly (Allow subdissectors to reassemble TCP streams = OFF) RTT seems to be high 0.5ms for 2 systems on the same local area network, suspec...'''
date = "2014-09-03T13:29:00Z"
lastmod = "2014-09-03T13:29:00Z"
weight = 35970
keywords = [ "ack", "rtt", "to" ]
aliases = [ "/questions/35970" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [RTT to ACK the segment](/questions/35970/rtt-to-ack-the-segment)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35970-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35970-score" class="post-score" title="current number of votes">0</div><span id="post-35970-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/graph_1.jpg" alt="alt text" /></p><p>Here is a capture of Kerberos encrypted LDAP traffic between the domain controller 2008R2 and Exchange server on Windows Server 2012. Assuming I graphed it correctly (Allow subdissectors to reassemble TCP streams = OFF) RTT seems to be high 0.5ms for 2 systems on the same local area network, suspecting latency to be the result of a network setting on both or one of the systems.</p><p>Is there a way to see what both systems don't agree on in the trace or it's trial and error kind of troubleshooting?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ack" rel="tag" title="see questions tagged &#39;ack&#39;">ack</span> <span class="post-tag tag-link-rtt" rel="tag" title="see questions tagged &#39;rtt&#39;">rtt</span> <span class="post-tag tag-link-to" rel="tag" title="see questions tagged &#39;to&#39;">to</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '14, 13:29</strong></p><img src="https://secure.gravatar.com/avatar/bcfdf26904f3a8a9fb69c7ca0dc5e7b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="net_tech&#39;s gravatar image" /><p><span>net_tech</span><br />
<span class="score" title="116 reputation points">116</span><span title="30 badges"><span class="badge1">●</span><span class="badgecount">30</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="net_tech has 2 accepted answers">13%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Sep '14, 13:31</strong> </span></p></div></div><div id="comments-container-35970" class="comments-container"></div><div id="comment-tools-35970" class="comment-tools"></div><div class="clear"></div><div id="comment-35970-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

