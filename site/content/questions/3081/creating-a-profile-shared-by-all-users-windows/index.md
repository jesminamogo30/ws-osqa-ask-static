+++
type = "question"
title = "Creating a profile shared by all users (windows)?"
description = '''Is there a way to create a Wireshark/Tshark profile that is shared by all the users of the machine under windows? It seems that profiles are saved by default in %APPDATA% which is unique to each user. Thanks!'''
date = "2011-03-24T08:19:00Z"
lastmod = "2011-03-25T07:43:00Z"
weight = 3081
keywords = [ "windows", "configuration", "profiles", "wireshark" ]
aliases = [ "/questions/3081" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Creating a profile shared by all users (windows)?](/questions/3081/creating-a-profile-shared-by-all-users-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3081-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3081-score" class="post-score" title="current number of votes">0</div><span id="post-3081-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to create a Wireshark/Tshark profile that is shared by all the users of the machine under windows? It seems that profiles are saved by default in %APPDATA% which is unique to each user.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span> <span class="post-tag tag-link-profiles" rel="tag" title="see questions tagged &#39;profiles&#39;">profiles</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Mar '11, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/9b7b5e633f7836289c2fc6c3934bffaf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="r0u1i&#39;s gravatar image" /><p><span>r0u1i</span><br />
<span class="score" title="61 reputation points">61</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="r0u1i has no accepted answers">0%</span></p></div></div><div id="comments-container-3081" class="comments-container"></div><div id="comment-tools-3081" class="comment-tools"></div><div class="clear"></div><div id="comment-3081-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3091"></span>

<div id="answer-container-3091" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3091-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3091-score" class="post-score" title="current number of votes">1</div><span id="post-3091-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="r0u1i has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You should be able to achieve what you ask if you install the portable version of Wireshark in a directory every user has access to. I haven't tried it myself but I think it should work just fine.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Mar '11, 15:35</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-3091" class="comments-container"><span id="3123"></span><div id="comment-3123" class="comment"><div id="post-3123-score" class="comment-score"></div><div class="comment-text"><p>interesting idea, thanks</p></div><div id="comment-3123-info" class="comment-info"><span class="comment-age">(25 Mar '11, 07:43)</span> <span class="comment-user userinfo">r0u1i</span></div></div></div><div id="comment-tools-3091" class="comment-tools"></div><div class="clear"></div><div id="comment-3091-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

