+++
type = "question"
title = "No DHCP Protocol"
description = '''I Run Wire shark to look for my Sky username and password, I don&#x27;t get any DHCP packets except DHCPV6 am I doing something wrong?'''
date = "2014-08-09T04:32:00Z"
lastmod = "2014-08-10T06:08:00Z"
weight = 35345
keywords = [ "dhcp" ]
aliases = [ "/questions/35345" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No DHCP Protocol](/questions/35345/no-dhcp-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35345-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35345-score" class="post-score" title="current number of votes">0</div><span id="post-35345-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I Run Wire shark to look for my Sky username and password, I don't get any DHCP packets except DHCPV6 am I doing something wrong?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dhcp" rel="tag" title="see questions tagged &#39;dhcp&#39;">dhcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Aug '14, 04:32</strong></p><img src="https://secure.gravatar.com/avatar/1f12e7a91a707b2dd397dd20905d190a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dosprompt&#39;s gravatar image" /><p><span>Dosprompt</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dosprompt has no accepted answers">0%</span></p></div></div><div id="comments-container-35345" class="comments-container"><span id="35361"></span><div id="comment-35361" class="comment"><div id="post-35361-score" class="comment-score"></div><div class="comment-text"><p>Just to understand your question: How is DHCP related to you Sky username/password?</p></div><div id="comment-35361-info" class="comment-info"><span class="comment-age">(10 Aug '14, 06:08)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-35345" class="comment-tools"></div><div class="clear"></div><div id="comment-35345-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

