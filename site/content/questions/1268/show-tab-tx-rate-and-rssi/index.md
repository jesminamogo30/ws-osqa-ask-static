+++
type = "question"
title = "Show tab Tx Rate and RSSI"
description = '''I can show tab TX Rate and RSSI at wireshark so that i can them clearly Can you help me? Thak you!'''
date = "2010-12-07T01:04:00Z"
lastmod = "2010-12-07T01:04:00Z"
weight = 1268
keywords = [ "tools" ]
aliases = [ "/questions/1268" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Show tab Tx Rate and RSSI](/questions/1268/show-tab-tx-rate-and-rssi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1268-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1268-score" class="post-score" title="current number of votes">0</div><span id="post-1268-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can show tab TX Rate and RSSI at wireshark so that i can them clearly Can you help me? Thak you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tools" rel="tag" title="see questions tagged &#39;tools&#39;">tools</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Dec '10, 01:04</strong></p><img src="https://secure.gravatar.com/avatar/4f406866b1740fdc247ba628d2515395?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="haquyen&#39;s gravatar image" /><p><span>haquyen</span><br />
<span class="score" title="1 reputation points">1</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="haquyen has no accepted answers">0%</span></p></div></div><div id="comments-container-1268" class="comments-container"></div><div id="comment-tools-1268" class="comment-tools"></div><div class="clear"></div><div id="comment-1268-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

