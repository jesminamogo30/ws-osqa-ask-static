+++
type = "question"
title = "Not able to capture tcp packets"
description = '''Hi,  I am running packet capture code using pcap library on my router, when i enable &quot;Default route &quot; in my router i am getting packets but only DNS not TCP packets, if i disable &quot;Default route&quot; i am getting TCP packets.  Does packet structure depends on &quot;Default route&quot;? or pcap does not capture whe...'''
date = "2017-01-25T01:05:00Z"
lastmod = "2017-01-25T01:05:00Z"
weight = 59041
keywords = [ "networking" ]
aliases = [ "/questions/59041" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Not able to capture tcp packets](/questions/59041/not-able-to-capture-tcp-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59041-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59041-score" class="post-score" title="current number of votes">0</div><span id="post-59041-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am running packet capture code using pcap library on my router, when i enable "Default route " in my router i am getting packets but only DNS not TCP packets, if i disable "Default route" i am getting TCP packets. Does packet structure depends on "Default route"? or pcap does not capture when "Default route" is enabled in router?</p><p>Thanks, Manjunath</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-networking" rel="tag" title="see questions tagged &#39;networking&#39;">networking</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jan '17, 01:05</strong></p><img src="https://secure.gravatar.com/avatar/02d05fc12a966485297d3250aa37e6c8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ManjunathMandya&#39;s gravatar image" /><p><span>ManjunathMandya</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ManjunathMandya has no accepted answers">0%</span></p></div></div><div id="comments-container-59041" class="comments-container"></div><div id="comment-tools-59041" class="comment-tools"></div><div class="clear"></div><div id="comment-59041-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

