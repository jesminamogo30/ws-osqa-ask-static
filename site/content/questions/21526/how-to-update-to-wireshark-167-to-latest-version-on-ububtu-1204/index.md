+++
type = "question"
title = "How to update to wireshark 1.6.7 to latest version on ububtu 12.04"
description = '''I have wireshark 1.6.7 on ubuntu 12.04. How can i upgrade my wireshark to latest stable release'''
date = "2013-05-28T09:20:00Z"
lastmod = "2013-05-28T09:29:00Z"
weight = 21526
keywords = [ "update" ]
aliases = [ "/questions/21526" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to update to wireshark 1.6.7 to latest version on ububtu 12.04](/questions/21526/how-to-update-to-wireshark-167-to-latest-version-on-ububtu-1204)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21526-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21526-score" class="post-score" title="current number of votes">0</div><span id="post-21526-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have wireshark 1.6.7 on ubuntu 12.04. How can i upgrade my wireshark to latest stable release</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-update" rel="tag" title="see questions tagged &#39;update&#39;">update</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 May '13, 09:20</strong></p><img src="https://secure.gravatar.com/avatar/489a484ec86acc8c6f49da1c7b6913bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tronatorx&#39;s gravatar image" /><p><span>tronatorx</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tronatorx has no accepted answers">0%</span></p></div></div><div id="comments-container-21526" class="comments-container"></div><div id="comment-tools-21526" class="comment-tools"></div><div class="clear"></div><div id="comment-21526-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21528"></span>

<div id="answer-container-21528" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21528-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21528-score" class="post-score" title="current number of votes">0</div><span id="post-21528-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>1.6.7 is the latest version available from the distro repositories for 12.04. As shown in the answers to <a href="http://ask.wireshark.org/questions/1243/upgrade-wireshark-in-ubuntu">this</a> similar question, you can either compile from source or install from a third-party ppa.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 May '13, 09:29</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-21528" class="comments-container"></div><div id="comment-tools-21528" class="comment-tools"></div><div class="clear"></div><div id="comment-21528-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

