+++
type = "question"
title = "I see no web traffic from my own Wifi clients"
description = '''Hi all, I would like to get the web traffic from my own Wifi with Kali Linux and Wireshark. Unfortunately, I only see my local web traffic and not from my smartphone by example. As wifi adapter I used ALFA AWUS036NHA. Has anyone an idea? Thanks, Chucky'''
date = "2016-06-23T02:13:00Z"
lastmod = "2016-06-27T06:16:00Z"
weight = 53626
keywords = [ "backtrack-linux", "traffic", "kali-linux", "backtrack" ]
aliases = [ "/questions/53626" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [I see no web traffic from my own Wifi clients](/questions/53626/i-see-no-web-traffic-from-my-own-wifi-clients)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53626-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53626-score" class="post-score" title="current number of votes">0</div><span id="post-53626-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I would like to get the web traffic from my own Wifi with Kali Linux and Wireshark. Unfortunately, I only see my local web traffic and not from my smartphone by example. As wifi adapter I used ALFA AWUS036NHA.</p><p>Has anyone an idea?</p><p>Thanks, Chucky</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-backtrack-linux" rel="tag" title="see questions tagged &#39;backtrack-linux&#39;">backtrack-linux</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-kali-linux" rel="tag" title="see questions tagged &#39;kali-linux&#39;">kali-linux</span> <span class="post-tag tag-link-backtrack" rel="tag" title="see questions tagged &#39;backtrack&#39;">backtrack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jun '16, 02:13</strong></p><img src="https://secure.gravatar.com/avatar/f414a89e2a75fcf81035aa0c961d4cb2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Chucky&#39;s gravatar image" /><p><span>Chucky</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Chucky has no accepted answers">0%</span></p></div></div><div id="comments-container-53626" class="comments-container"></div><div id="comment-tools-53626" class="comment-tools"></div><div class="clear"></div><div id="comment-53626-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53627"></span>

<div id="answer-container-53627" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53627-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53627-score" class="post-score" title="current number of votes">1</div><span id="post-53627-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, I do have an idea. Search through about 20 similar questions exhaustively answered here, especially <a href="https://ask.wireshark.org/users/112/bob-jones">answers provided by <span>@Bob Jones</span></a> are very useful.</p><p>Your keywords would be "monitor mode", "promiscuous mode", "wireless driver".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jun '16, 02:29</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-53627" class="comments-container"><span id="53672"></span><div id="comment-53672" class="comment"><div id="post-53672-score" class="comment-score"></div><div class="comment-text"><p>[I converted your comment to an answer since, well, it answers the question--though indirectly :-)]</p></div><div id="comment-53672-info" class="comment-info"><span class="comment-age">(27 Jun '16, 06:16)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div></div><div id="comment-tools-53627" class="comment-tools"></div><div class="clear"></div><div id="comment-53627-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

