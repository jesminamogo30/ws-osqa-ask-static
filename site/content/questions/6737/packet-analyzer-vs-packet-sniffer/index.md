+++
type = "question"
title = "Packet analyzer vs. Packet sniffer"
description = '''Is there a difference between them? '''
date = "2011-10-05T10:43:00Z"
lastmod = "2011-10-07T13:33:00Z"
weight = 6737
keywords = [ "sniffer", "analyzer", "packet" ]
aliases = [ "/questions/6737" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packet analyzer vs. Packet sniffer](/questions/6737/packet-analyzer-vs-packet-sniffer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6737-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6737-score" class="post-score" title="current number of votes">1</div><span id="post-6737-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a difference between them?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffer" rel="tag" title="see questions tagged &#39;sniffer&#39;">sniffer</span> <span class="post-tag tag-link-analyzer" rel="tag" title="see questions tagged &#39;analyzer&#39;">analyzer</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Oct '11, 10:43</strong></p><img src="https://secure.gravatar.com/avatar/ecd04a9deee50019127ef4f7e3eca413?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="YKG&#39;s gravatar image" /><p><span>YKG</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="YKG has no accepted answers">0%</span></p></div></div><div id="comments-container-6737" class="comments-container"></div><div id="comment-tools-6737" class="comment-tools"></div><div class="clear"></div><div id="comment-6737-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6743"></span>

<div id="answer-container-6743" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6743-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6743-score" class="post-score" title="current number of votes">5</div><span id="post-6743-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Mostly a semantic difference, but yes:</p><ul><li><p>A packet <strong>sniffer</strong> records packets observed on a network interface.</p></li><li><p>A packet <strong>analyzer</strong> looks at packets and tries to make some inferences about what they contain.</p></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Oct '11, 14:17</strong></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="multipleinterfaces has 9 accepted answers">12%</span></p></div></div><div id="comments-container-6743" class="comments-container"><span id="6798"></span><div id="comment-6798" class="comment"><div id="post-6798-score" class="comment-score">2</div><div class="comment-text"><p>And a lot of packet sniffers (including the original Sniffer software that ran on DOS, and its Windows successor, as well as tcpdump, Wireshark, snoop, Microsoft Network Monitor, and so on) are both sniffers (in that they can capture network traffic) and analyzers (as they can dissect packets and analyze them).</p></div><div id="comment-6798-info" class="comment-info"><span class="comment-age">(07 Oct '11, 13:33)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-6743" class="comment-tools"></div><div class="clear"></div><div id="comment-6743-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

