+++
type = "question"
title = "How do you recognise packets from an ads server on youtube?"
description = '''I want to block ads on my smart tv throw my router and I am using whireshark to capture the ip adress and server adress from which those ads come from and block them in my router firewall.Destination appears only as &quot;Broadcast&quot; bassicaly no ip adress. Do you have any ideas how I could block these ad...'''
date = "2017-06-28T23:29:00Z"
lastmod = "2017-06-28T23:29:00Z"
weight = 62389
keywords = [ "smarttv", "youtube", "ads", "block" ]
aliases = [ "/questions/62389" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do you recognise packets from an ads server on youtube?](/questions/62389/how-do-you-recognise-packets-from-an-ads-server-on-youtube)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62389-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62389-score" class="post-score" title="current number of votes">0</div><span id="post-62389-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to block ads on my smart tv throw my router and I am using whireshark to capture the ip adress and server adress from which those ads come from and block them in my router firewall.Destination appears only as "Broadcast" bassicaly no ip adress.<img src="https://osqa-ask.wireshark.org/upfiles/Untitled_Z0bkKxf.png" alt="alt text" /></p><p>Do you have any ideas how I could block these ads through wireshark?This is my first time using wireshark so I'm bassically a noob in this field.If you guys could help me I would be really greatful.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-smarttv" rel="tag" title="see questions tagged &#39;smarttv&#39;">smarttv</span> <span class="post-tag tag-link-youtube" rel="tag" title="see questions tagged &#39;youtube&#39;">youtube</span> <span class="post-tag tag-link-ads" rel="tag" title="see questions tagged &#39;ads&#39;">ads</span> <span class="post-tag tag-link-block" rel="tag" title="see questions tagged &#39;block&#39;">block</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jun '17, 23:29</strong></p><img src="https://secure.gravatar.com/avatar/e2d67398cca2fa9bb0c8eb9e0bcac2a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ameridian&#39;s gravatar image" /><p><span>Ameridian</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ameridian has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Jun '17, 23:31</strong> </span></p></div></div><div id="comments-container-62389" class="comments-container"></div><div id="comment-tools-62389" class="comment-tools"></div><div class="clear"></div><div id="comment-62389-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

