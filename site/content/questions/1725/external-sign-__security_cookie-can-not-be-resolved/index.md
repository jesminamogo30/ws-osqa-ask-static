+++
type = "question"
title = "external sign &quot;__security_cookie&quot; can not be resolved，紧急，谢谢！"
description = '''My Wireshark Version : V1.4.4; Using compiler: Microsoft Visual studio 2008 when &quot;.toolslemonlemon.c&quot; file compiled, encoutered error messages like as:  lemon.obj : error LNK2019 : can not resolve external sign &quot;__security_cookie&quot;, this sign was referred in _ErrorMsg function. lemon.obj : error LNK2...'''
date = "2011-01-12T23:55:00Z"
lastmod = "2011-01-12T23:55:00Z"
weight = 1725
keywords = [ "lemon", "__security_cookie", "1077" ]
aliases = [ "/questions/1725" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [external sign "\_\_security\_cookie" can not be resolved，紧急，谢谢！](/questions/1725/external-sign-__security_cookie-can-not-be-resolved)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1725-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1725-score" class="post-score" title="current number of votes">0</div><span id="post-1725-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My Wireshark Version : V1.4.4;<br />
Using compiler: Microsoft Visual studio 2008<br />
when ".toolslemonlemon.c" file compiled, encoutered error messages like as:<br />
</p><p>lemon.obj : error LNK2019 : can not resolve external sign "__security_cookie", this sign was referred in _ErrorMsg function.<br />
lemon.obj : error LNK2019 : can not resolve external sign "@<span class="__cf_email__" data-cfemail="abf4d8cec8ded9c2dfd2f4c8c4c4c0c2ceeb">[email protected]</span>", this sign was referred in _ErrorMsg function.<br />
lemon.exe : fatal error LNK1120 : two commands could not be resolved;<br />
NMAKE : fatal error U1077: "c:program filesmicrosoft visual studio 9.0bincl.exe":return code "0x2";<br />
stop<br />
NAMKE: fatal error U1077: "..native-nmake.cmd":return code "0x2"<br />
stop<br />
NMAKE : fatal error U1077: "c:program filesmicrosoft visual studio 9.0binnmake.exe":return code "0x2";<br />
stop</p><p>thie info is interpreted from chinese error info,thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lemon" rel="tag" title="see questions tagged &#39;lemon&#39;">lemon</span> <span class="post-tag tag-link-__security_cookie" rel="tag" title="see questions tagged &#39;__security_cookie&#39;">__security_cookie</span> <span class="post-tag tag-link-1077" rel="tag" title="see questions tagged &#39;1077&#39;">1077</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jan '11, 23:55</strong></p><img src="https://secure.gravatar.com/avatar/dc85b96f8633909445b035c43d8ccd8b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Friend1900&#39;s gravatar image" /><p><span>Friend1900</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Friend1900 has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-1725" class="comments-container"></div><div id="comment-tools-1725" class="comment-tools"></div><div class="clear"></div><div id="comment-1725-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

