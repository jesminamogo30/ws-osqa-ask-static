+++
type = "question"
title = "Wireshark will not capture my iPod packets?"
description = '''I&#x27;m new to everything networking, ranging from everything to everything. I&#x27;m only here because I want to practice monitoring devices. I can see Wireshark will capture my main computers packets, but I want to be able to monitor my iPod just for learning purposes. When I filter my own IP address, I ge...'''
date = "2014-11-30T02:43:00Z"
lastmod = "2014-11-30T02:43:00Z"
weight = 38240
keywords = [ "ipod" ]
aliases = [ "/questions/38240" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark will not capture my iPod packets?](/questions/38240/wireshark-will-not-capture-my-ipod-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38240-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38240-score" class="post-score" title="current number of votes">0</div><span id="post-38240-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm new to everything networking, ranging from everything to everything. I'm only here because I want to practice monitoring devices.</p><p>I can see Wireshark will capture my main computers packets, but I want to be able to monitor my iPod just for learning purposes. When I filter my own IP address, I get all types of packets! It captures everything I do, however, on my iPod, it doesn't capture anything when I go into google and all that jazz. How can I go about fixing this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipod" rel="tag" title="see questions tagged &#39;ipod&#39;">ipod</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Nov '14, 02:43</strong></p><img src="https://secure.gravatar.com/avatar/cf7ebe8aba2c16eb1dcc95dc89c41989?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="illQuo&#39;s gravatar image" /><p><span>illQuo</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="illQuo has no accepted answers">0%</span></p></div></div><div id="comments-container-38240" class="comments-container"></div><div id="comment-tools-38240" class="comment-tools"></div><div class="clear"></div><div id="comment-38240-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

