+++
type = "question"
title = "Windows 7 Compatibility"
description = '''At what version did Wireshark become Windows 7 compatible? Not the latest but the very first. '''
date = "2012-04-06T08:43:00Z"
lastmod = "2012-04-06T09:42:00Z"
weight = 9984
keywords = [ "windows7", "compatibility", "versions" ]
aliases = [ "/questions/9984" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 7 Compatibility](/questions/9984/windows-7-compatibility)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9984-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9984-score" class="post-score" title="current number of votes">0</div><span id="post-9984-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>At what version did Wireshark become Windows 7 compatible? Not the latest but the very first.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-compatibility" rel="tag" title="see questions tagged &#39;compatibility&#39;">compatibility</span> <span class="post-tag tag-link-versions" rel="tag" title="see questions tagged &#39;versions&#39;">versions</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '12, 08:43</strong></p><img src="https://secure.gravatar.com/avatar/a6de937345dfa1c2586f60a1daeaceab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cre8tivspirit&#39;s gravatar image" /><p><span>cre8tivspirit</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cre8tivspirit has no accepted answers">0%</span></p></div></div><div id="comments-container-9984" class="comments-container"></div><div id="comment-tools-9984" class="comment-tools"></div><div class="clear"></div><div id="comment-9984-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9988"></span>

<div id="answer-container-9988" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9988-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9988-score" class="post-score" title="current number of votes">3</div><span id="post-9988-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The first version released!</p><p>I just downloaded and installed 0.99.2, and that was able to run and capture traffic on Win 7 SP1 64 bit.</p><p>There were some graphical glitches, but nothing to make it unusable, and I did the capture using the installed WinPCap 4.1.2 rather than revert to 3.1 which came with that version.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Apr '12, 09:11</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Apr '12, 09:12</strong> </span></p></div></div><div id="comments-container-9988" class="comments-container"><span id="9989"></span><div id="comment-9989" class="comment"><div id="post-9989-score" class="comment-score"></div><div class="comment-text"><p><span>@grahamb</span>, I like "Forever!" better ;)</p></div><div id="comment-9989-info" class="comment-info"><span class="comment-age">(06 Apr '12, 09:18)</span> <span class="comment-user userinfo">bstn</span></div></div><span id="9990"></span><div id="comment-9990" class="comment"><div id="post-9990-score" class="comment-score"></div><div class="comment-text"><p>1.2.3 was the first version to ship with a WinPcap installer that supported Windows 7 (4.1.1) but as <span>@grahamb</span> points out any version should work.</p></div><div id="comment-9990-info" class="comment-info"><span class="comment-age">(06 Apr '12, 09:24)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div><span id="9991"></span><div id="comment-9991" class="comment"><div id="post-9991-score" class="comment-score"></div><div class="comment-text"><p><span>@bstn</span> Yep, should have left my first attempt in.</p><p>So actually Wireshark was compatible with Win 7 before Win 7 was even released! Wireshark 0.99.2 is dated 17 July 2006, the first public release of Win 7 was the beta on Jan 2009.</p></div><div id="comment-9991-info" class="comment-info"><span class="comment-age">(06 Apr '12, 09:42)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-9988" class="comment-tools"></div><div class="clear"></div><div id="comment-9988-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

