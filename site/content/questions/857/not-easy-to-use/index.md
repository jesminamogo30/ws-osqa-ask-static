+++
type = "question"
title = "(not) easy to use?"
description = '''I just don&#x27;t understand all the technical words. Now that I have Wireshark, how can I see a network that I&#x27;ve already had access to. I need to read my brothers e-mail. He passed away and has some documents, etc saved in his inbox.'''
date = "2010-11-08T09:31:00Z"
lastmod = "2010-11-08T11:24:00Z"
weight = 857
keywords = [ "use", "easy" ]
aliases = [ "/questions/857" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [(not) easy to use?](/questions/857/not-easy-to-use)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-857-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-857-score" class="post-score" title="current number of votes">0</div><span id="post-857-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just don't understand all the technical words. Now that I have Wireshark, how can I see a network that I've already had access to. I need to read my brothers e-mail. He passed away and has some documents, etc saved in his inbox.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-use" rel="tag" title="see questions tagged &#39;use&#39;">use</span> <span class="post-tag tag-link-easy" rel="tag" title="see questions tagged &#39;easy&#39;">easy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '10, 09:31</strong></p><img src="https://secure.gravatar.com/avatar/856a460f83f81258d9810d764853b769?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="heylookhere&#39;s gravatar image" /><p><span>heylookhere</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="heylookhere has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Nov '10, 10:21</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-857" class="comments-container"></div><div id="comment-tools-857" class="comment-tools"></div><div class="clear"></div><div id="comment-857-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="859"></span>

<div id="answer-container-859" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-859-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-859-score" class="post-score" title="current number of votes">0</div><span id="post-859-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark will not do you any good as the information you are seeking sits on the harddrive. Wireshark can only make things visible that are transferred over the network.</p><p>What you are looking for is a (host) forensics tool. I'm sorry but I can't advice you any further as I have never looked into forensics myself...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Nov '10, 11:08</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-859" class="comments-container"><span id="861"></span><div id="comment-861" class="comment"><div id="post-861-score" class="comment-score"></div><div class="comment-text"><p>it info in his e mail account rests in his g mail account....not the hard drive. what is a host forensic tool? keylogger??</p></div><div id="comment-861-info" class="comment-info"><span class="comment-age">(08 Nov '10, 11:12)</span> <span class="comment-user userinfo">heylookhere</span></div></div><span id="862"></span><div id="comment-862" class="comment"><div id="post-862-score" class="comment-score"></div><div class="comment-text"><p>A (host) forensic tool can search through all data on a hard drive to extract data.</p><p>However, if the mails are on Gmail, then you would need to find out the password to access that mail. I'm sure Google will cooperate in accessing the mail in your situation if you supply them with the information that they need to confirm the authenticity of your request.</p></div><div id="comment-862-info" class="comment-info"><span class="comment-age">(08 Nov '10, 11:19)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="863"></span><div id="comment-863" class="comment"><div id="post-863-score" class="comment-score"></div><div class="comment-text"><p>and my brother's laughed at me when I gave them my "in case I die, here's the master password for my password-manager/acct list...."</p><p>By the way, GMAIL typically is setup to stay logged in. I'm assuming you already tried firing it up to see if it will log you in? Assuming everything is up-n-up, the browser cache/cookie may just let you in.</p></div><div id="comment-863-info" class="comment-info"><span class="comment-age">(08 Nov '10, 11:24)</span> <span class="comment-user userinfo">hansangb</span></div></div></div><div id="comment-tools-859" class="comment-tools"></div><div class="clear"></div><div id="comment-859-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

