+++
type = "question"
title = "Can&#x27;t turn on monitor mode on macbook pro with Wireshark 1.99.9"
description = '''Can anyone tell me why I am getting this error message when trying to capture in monitor mode over Wifi (En1) ? I do not get this error until I enable monitor mode. I am trying to capture all traffic on the network.  &quot;Unable to set data link type on interface &#x27;en1&#x27; (EN10MB is not one of the DLTs sup...'''
date = "2015-10-31T19:03:00Z"
lastmod = "2015-12-31T12:54:00Z"
weight = 47125
keywords = [ "mac", "monitor-mode" ]
aliases = [ "/questions/47125" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can't turn on monitor mode on macbook pro with Wireshark 1.99.9](/questions/47125/cant-turn-on-monitor-mode-on-macbook-pro-with-wireshark-1999)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47125-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47125-score" class="post-score" title="current number of votes">0</div><span id="post-47125-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anyone tell me why I am getting this error message when trying to capture in monitor mode over Wifi (En1) ? I do not get this error until I enable monitor mode. I am trying to capture all traffic on the network.<br />
"Unable to set data link type on interface 'en1' (EN10MB is not one of the DLTs supported by this device)."</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '15, 19:03</strong></p><img src="https://secure.gravatar.com/avatar/cd1a64f7eceba63975477e62e155448d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nikonshooter7&#39;s gravatar image" /><p><span>nikonshooter7</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nikonshooter7 has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Nov '15, 14:14</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-47125" class="comments-container"><span id="47126"></span><div id="comment-47126" class="comment"><div id="post-47126-score" class="comment-score"></div><div class="comment-text"><p>What version of Wireshark are you using? There were, as I remember, some bugs that could cause this.</p></div><div id="comment-47126-info" class="comment-info"><span class="comment-age">(01 Nov '15, 00:57)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="47206"></span><div id="comment-47206" class="comment"><div id="post-47206-score" class="comment-score"></div><div class="comment-text"><p>I am on version Version 1.99.9</p></div><div id="comment-47206-info" class="comment-info"><span class="comment-age">(03 Nov '15, 18:18)</span> <span class="comment-user userinfo">nikonshooter7</span></div></div></div><div id="comment-tools-47125" class="comment-tools"></div><div class="clear"></div><div id="comment-47125-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47207"></span>

<div id="answer-container-47207" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47207-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47207-score" class="post-score" title="current number of votes">0</div><span id="post-47207-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What happens if you try version 2.0.0rc2?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Nov '15, 19:19</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-47207" class="comments-container"><span id="47208"></span><div id="comment-47208" class="comment"><div id="post-47208-score" class="comment-score"></div><div class="comment-text"><p>Will try that now and report back</p></div><div id="comment-47208-info" class="comment-info"><span class="comment-age">(03 Nov '15, 20:14)</span> <span class="comment-user userinfo">nikonshooter7</span></div></div><span id="47350"></span><div id="comment-47350" class="comment"><div id="post-47350-score" class="comment-score"></div><div class="comment-text"><p>Success! Thank you, upgrading to the version 2.0.0rc2 solved the issue</p></div><div id="comment-47350-info" class="comment-info"><span class="comment-age">(06 Nov '15, 13:55)</span> <span class="comment-user userinfo">nikonshooter7</span></div></div><span id="48684"></span><div id="comment-48684" class="comment"><div id="post-48684-score" class="comment-score"></div><div class="comment-text"><p>With version 2.0.0 (stable 18nov15, on a MacBook-10.7) I found that if I changed the settings for the interface (to enable monitor mode) then restarted the app then I could get it into monitor mode after it restarted. Awkwardly enough I seem to have to repeat the procedure to get it capture in non-monitor mode.</p></div><div id="comment-48684-info" class="comment-info"><span class="comment-age">(23 Dec '15, 05:29)</span> <span class="comment-user userinfo">pierz</span></div></div><span id="48779"></span><div id="comment-48779" class="comment"><div id="post-48779-score" class="comment-score"></div><div class="comment-text"><p>As I posted elsewhere - 2.0 didn't do monitor mode 1.12 did 2.0 couldn't be uninstalled apparently re-installing 1.12 said it installed, but was not there. 2.01 says it installed but is not there - even though 2.0 was thrown away 1.12 cannot be re-installed We were told at Sharkfest you could have both 1 and 2 Now I can’t even get 1.12 back</p></div><div id="comment-48779-info" class="comment-info"><span class="comment-age">(31 Dec '15, 12:54)</span> <span class="comment-user userinfo">packetlevel</span></div></div></div><div id="comment-tools-47207" class="comment-tools"></div><div class="clear"></div><div id="comment-47207-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

