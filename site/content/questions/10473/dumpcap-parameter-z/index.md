+++
type = "question"
title = "Dumpcap Parameter -Z"
description = '''Can someone tell me what the parameter &quot;-Z&quot; does for dumpcap? I&#x27;ve seen Wireshark calling dumpcap with that parameter, but the integrated help doesn&#x27;t tell what it means. The complete command line was: &quot;C:&#92;Program Files&#92;Wireshark&#92;dumpcap&quot; -t -n -i &#92;Device&#92;NPF_{F7CB705F-7A3D-4CBF-B881-1D0074D695D7} -...'''
date = "2012-04-27T03:43:00Z"
lastmod = "2012-04-27T06:16:00Z"
weight = 10473
keywords = [ "dumpcap", "parameters" ]
aliases = [ "/questions/10473" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Dumpcap Parameter -Z](/questions/10473/dumpcap-parameter-z)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10473-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10473-score" class="post-score" title="current number of votes">0</div><span id="post-10473-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can someone tell me what the parameter "-Z" does for dumpcap? I've seen Wireshark calling dumpcap with that parameter, but the integrated help doesn't tell what it means. The complete command line was:</p><pre><code>&quot;C:\Program Files\Wireshark\dumpcap&quot; -t -n -i \Device\NPF_{F7CB705F-7A3D-4CBF-B881-1D0074D695D7} -y EN10MB -u -r -i \Device\NPF_{E98CD01D-49F2-412E-A1AD-EE1B89EF3E61} -y EN10MB -u -r -Z 6264</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span> <span class="post-tag tag-link-parameters" rel="tag" title="see questions tagged &#39;parameters&#39;">parameters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Apr '12, 03:43</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-10473" class="comments-container"></div><div id="comment-tools-10473" class="comment-tools"></div><div class="clear"></div><div id="comment-10473-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10475"></span>

<div id="answer-container-10475" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10475-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10475-score" class="post-score" title="current number of votes">0</div><span id="post-10475-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Jasper has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From dumpcap.c:</p><pre><code>/*
 * Determine if dumpcap is being requested to run in a special
 * capture_child mode by going thru the command line args to see if
 * a -Z is present. (-Z is a hidden option).
 *
 * The primary result of running in capture_child mode is that
 * all messages sent out on stderr are in a special type/len/string
 * format to allow message processing by type.  These messages include
 * error messages if dumpcap fails to start the operation it was
 * requested to do, as well as various &quot;status&quot; messages which are sent
 * when an actual capture is in progress, and a &quot;success&quot; message sent
 * if dumpcap was requested to perform an operation other than a
 * capture.
 *
 * Capture_child mode would normally be requested by a parent process
 * which invokes dumpcap and obtains dumpcap stderr output via a pipe
 * to which dumpcap stderr has been redirected.  It might also have
 * another pipe to obtain dumpcap stdout output; for operations other
 * than a capture, that information is formatted specially for easier
 * parsing by the parent process.
 *
 * Capture_child mode needs to be determined immediately upon
 * startup so that any messages generated by dumpcap in this mode
 * (eg: during initialization) will be formatted properly.
 */</code></pre><p>In short, -Z is used when dumpcap is being called from (wire|t)shark to streamline inter-process communication.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Apr '12, 03:57</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-10475" class="comments-container"><span id="10476"></span><div id="comment-10476" class="comment"><div id="post-10476-score" class="comment-score"></div><div class="comment-text"><p>Thx, should have thought of looking at the code. Duh... :-)</p></div><div id="comment-10476-info" class="comment-info"><span class="comment-age">(27 Apr '12, 04:25)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="10478"></span><div id="comment-10478" class="comment"><div id="post-10478-score" class="comment-score"></div><div class="comment-text"><p>Use the source Luke... uhmmm... Jasper :-)</p></div><div id="comment-10478-info" class="comment-info"><span class="comment-age">(27 Apr '12, 04:40)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="10479"></span><div id="comment-10479" class="comment"><div id="post-10479-score" class="comment-score"></div><div class="comment-text"><p>Yes Obi-SYN... err Obi-LAN. :-)</p></div><div id="comment-10479-info" class="comment-info"><span class="comment-age">(27 Apr '12, 05:28)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="10480"></span><div id="comment-10480" class="comment"><div id="post-10480-score" class="comment-score"></div><div class="comment-text"><p>Actually, Laura is Obi and I'm Yoda... well, at least to some people :-)</p><p>(see: <a href="http://thenetworkguy.typepad.com/nau/2008/05/use-the-shell-l.html">http://thenetworkguy.typepad.com/nau/2008/05/use-the-shell-l.html</a>)</p></div><div id="comment-10480-info" class="comment-info"><span class="comment-age">(27 Apr '12, 05:40)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="10481"></span><div id="comment-10481" class="comment"><div id="post-10481-score" class="comment-score"></div><div class="comment-text"><p>Okay... reading the source code I must, yes. :-)</p></div><div id="comment-10481-info" class="comment-info"><span class="comment-age">(27 Apr '12, 06:16)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-10475" class="comment-tools"></div><div class="clear"></div><div id="comment-10475-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

