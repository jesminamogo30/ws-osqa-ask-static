+++
type = "question"
title = "Installation failure of 1.12.0 on Mac OS X 10.5"
description = '''The Wireshark download page currently offers a package for installation on Mac OS X 10.5 and above https://www.wireshark.org/download.html However, when trying to install this package, I got the following error in the installer when selecting the volume to install on: You cannot install Wireshark 1....'''
date = "2014-08-23T18:29:00Z"
lastmod = "2014-08-27T10:35:00Z"
weight = 35687
keywords = [ "installer", "macosx" ]
aliases = [ "/questions/35687" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Installation failure of 1.12.0 on Mac OS X 10.5](/questions/35687/installation-failure-of-1120-on-mac-os-x-105)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35687-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35687-score" class="post-score" title="current number of votes">0</div><span id="post-35687-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The Wireshark download page currently offers a package for installation on Mac OS X 10.5 and above <a href="https://www.wireshark.org/download.html">https://www.wireshark.org/download.html</a></p><p>However, when trying to install this package, I got the following error in the installer when selecting the volume to install on: You cannot install Wireshark 1.12.0 Intel 32 on this volume. This package requires Mac OS X 10.6 or later. If you cannot upgrade you might try installing using MacPorts or Fink.</p><p>If 10.5 is not supported then the download page should be fixed. If 10.5 is supported then the package needs to be fixed.</p><p>The previous stable package of 1.10.9 does install on 10.5.8</p><p>Regards, Andrew</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installer" rel="tag" title="see questions tagged &#39;installer&#39;">installer</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '14, 18:29</strong></p><img src="https://secure.gravatar.com/avatar/6f7e9495660dbcc71cc667c036bd96c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="roughana&#39;s gravatar image" /><p><span>roughana</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="roughana has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Aug '14, 18:35</strong> </span></p></div></div><div id="comments-container-35687" class="comments-container"><span id="35802"></span><div id="comment-35802" class="comment"><div id="post-35802-score" class="comment-score"></div><div class="comment-text"><p>This seems like a bug to me, either way. Is there somewhere this should be reported that will get more traction?</p></div><div id="comment-35802-info" class="comment-info"><span class="comment-age">(27 Aug '14, 05:24)</span> <span class="comment-user userinfo">roughana</span></div></div></div><div id="comment-tools-35687" class="comment-tools"></div><div class="clear"></div><div id="comment-35687-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35815"></span>

<div id="answer-container-35815" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35815-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35815-score" class="post-score" title="current number of votes">0</div><span id="post-35815-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="roughana has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The <a href="https://bugs.wireshark.org/bugzilla/">Wireshark Bugzilla</a> is where bugs get reported, see also the <a href="http://wiki.wireshark.org/ReportingBugs">Reporting Bugs</a> page on the Wiki.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Aug '14, 10:35</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-35815" class="comments-container"></div><div id="comment-tools-35815" class="comment-tools"></div><div class="clear"></div><div id="comment-35815-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

