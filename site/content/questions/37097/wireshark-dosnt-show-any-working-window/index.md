+++
type = "question"
title = "WireShark dosn&#x27;t show any working window"
description = '''In have installed xQuartz (2.7.7) and latest Wireshark version (1.12.1). Mac OS 10.9.5 Selecting the Wireshark application only shows the Wireshark menu, no window is shown. The menu only contains &quot;Services&quot;, &quot;Hide Wireshark&quot;, &quot;Hide Others&quot;, &quot;Show All&quot; and &quot;Quit Wireshark&quot; What is wrong?'''
date = "2014-10-16T02:51:00Z"
lastmod = "2014-10-16T07:02:00Z"
weight = 37097
keywords = [ "mac", "display" ]
aliases = [ "/questions/37097" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark dosn't show any working window](/questions/37097/wireshark-dosnt-show-any-working-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37097-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37097-score" class="post-score" title="current number of votes">0</div><span id="post-37097-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In have installed xQuartz (2.7.7) and latest Wireshark version (1.12.1). Mac OS 10.9.5 Selecting the Wireshark application only shows the Wireshark menu, no window is shown. The menu only contains "Services", "Hide Wireshark", "Hide Others", "Show All" and "Quit Wireshark"</p><p>What is wrong?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Oct '14, 02:51</strong></p><img src="https://secure.gravatar.com/avatar/4fbcd079d2a3cf107f7564a951bc1f70?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="peteri&#39;s gravatar image" /><p><span>peteri</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="peteri has no accepted answers">0%</span></p></div></div><div id="comments-container-37097" class="comments-container"></div><div id="comment-tools-37097" class="comment-tools"></div><div class="clear"></div><div id="comment-37097-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37109"></span>

<div id="answer-container-37109" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37109-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37109-score" class="post-score" title="current number of votes">0</div><span id="post-37109-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I found out the problem, it was multiple monitors connected to my Mac. And XQuartz didn't work with that. If started WireShark with no extra monitors it worked well. After that i could connect my extra monitors again.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Oct '14, 07:02</strong></p><img src="https://secure.gravatar.com/avatar/4fbcd079d2a3cf107f7564a951bc1f70?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="peteri&#39;s gravatar image" /><p><span>peteri</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="peteri has no accepted answers">0%</span></p></div></div><div id="comments-container-37109" class="comments-container"></div><div id="comment-tools-37109" class="comment-tools"></div><div class="clear"></div><div id="comment-37109-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

