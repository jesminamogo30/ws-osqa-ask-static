+++
type = "question"
title = "Extract data from frame"
description = '''I would like to extract data from a frame. For example, a frame has SSL data (Non-ssl from malware). How would I extract that data using tshark. Thx.'''
date = "2012-01-26T08:09:00Z"
lastmod = "2012-01-26T08:30:00Z"
weight = 8623
keywords = [ "tshark" ]
aliases = [ "/questions/8623" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Extract data from frame](/questions/8623/extract-data-from-frame)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8623-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8623-score" class="post-score" title="current number of votes">0</div><span id="post-8623-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to extract data from a frame. For example, a frame has SSL data (Non-ssl from malware). How would I extract that data using tshark.</p><p>Thx.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jan '12, 08:09</strong></p><img src="https://secure.gravatar.com/avatar/28841748040cf2a93d11e731bde86594?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wshk_newb&#39;s gravatar image" /><p><span>wshk_newb</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wshk_newb has no accepted answers">0%</span></p></div></div><div id="comments-container-8623" class="comments-container"><span id="8625"></span><div id="comment-8625" class="comment"><div id="post-8625-score" class="comment-score"></div><div class="comment-text"><p>I would try Splitcap. I have a video on how to use it at http://www.lovemytool.com/blog/2012/01/using-splitcap-to-help-analyze-your-wireshark-trace-files-by-tony-fortunato.html or http://tinyurl.com/6odr3m5</p></div><div id="comment-8625-info" class="comment-info"><span class="comment-age">(26 Jan '12, 08:30)</span> <span class="comment-user userinfo">thetechfirm</span></div></div></div><div id="comment-tools-8623" class="comment-tools"></div><div class="clear"></div><div id="comment-8623-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

