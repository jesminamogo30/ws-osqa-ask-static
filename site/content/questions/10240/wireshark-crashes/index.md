+++
type = "question"
title = "Wireshark crashes"
description = '''Hi, Wireshark crashes when i try to configure ssl preference .When i provide the location of .pem private key file in windows 7 and click on apply,wireshark crashes'''
date = "2012-04-18T05:24:00Z"
lastmod = "2012-04-19T02:23:00Z"
weight = 10240
keywords = [ "ssl" ]
aliases = [ "/questions/10240" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark crashes](/questions/10240/wireshark-crashes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10240-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10240-score" class="post-score" title="current number of votes">0</div><span id="post-10240-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Wireshark crashes when i try to configure ssl preference .When i provide the location of .pem private key file in windows 7 and click on apply,wireshark crashes</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '12, 05:24</strong></p><img src="https://secure.gravatar.com/avatar/54dfb1796a8beedf9843a326d673eaae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vikram&#39;s gravatar image" /><p><span>vikram</span><br />
<span class="score" title="41 reputation points">41</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vikram has no accepted answers">0%</span></p></div></div><div id="comments-container-10240" class="comments-container"></div><div id="comment-tools-10240" class="comment-tools"></div><div class="clear"></div><div id="comment-10240-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10247"></span>

<div id="answer-container-10247" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10247-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10247-score" class="post-score" title="current number of votes">0</div><span id="post-10247-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="vikram has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Which version of Wireshark are you running? Are you running 64-bit or 32-bit Windows? Note that there's a known bug with SSL preferences in Wireshark 1.6.6 on 64-bit Windows. It was <a href="http://staging.wireshark.org/docs/relnotes/wireshark-1.6.7.html">fixed in 1.6.7</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Apr '12, 09:53</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-10247" class="comments-container"><span id="10271"></span><div id="comment-10271" class="comment"><div id="post-10271-score" class="comment-score"></div><div class="comment-text"><p>yes..Gerald ..i am using wireshark 1.6.6 on windows 64 bit..let me try 1.6.7 and update you</p></div><div id="comment-10271-info" class="comment-info"><span class="comment-age">(19 Apr '12, 02:23)</span> <span class="comment-user userinfo">vikram</span></div></div></div><div id="comment-tools-10247" class="comment-tools"></div><div class="clear"></div><div id="comment-10247-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

