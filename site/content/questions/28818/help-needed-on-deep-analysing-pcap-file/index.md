+++
type = "question"
title = "[closed] Help needed on deep analysing PCAP file"
description = '''I got a pcap and need to have it deep analysed to find a netbot, how do i do this with wireshark??'''
date = "2014-01-12T07:22:00Z"
lastmod = "2014-01-12T12:43:00Z"
weight = 28818
keywords = [ "encrypted", "pcap", "botnet" ]
aliases = [ "/questions/28818" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Help needed on deep analysing PCAP file](/questions/28818/help-needed-on-deep-analysing-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28818-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28818-score" class="post-score" title="current number of votes">0</div><span id="post-28818-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I got a pcap and need to have it deep analysed to find a netbot, how do i do this with wireshark??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-encrypted" rel="tag" title="see questions tagged &#39;encrypted&#39;">encrypted</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-botnet" rel="tag" title="see questions tagged &#39;botnet&#39;">botnet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jan '14, 07:22</strong></p><img src="https://secure.gravatar.com/avatar/9cb8adaa160f0a649a97a2f43c7177fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MarkV&#39;s gravatar image" /><p><span>MarkV</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MarkV has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>12 Jan '14, 07:29</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-28818" class="comments-container"><span id="28819"></span><div id="comment-28819" class="comment"><div id="post-28819-score" class="comment-score"></div><div class="comment-text"><p>Again the same question??</p></div><div id="comment-28819-info" class="comment-info"><span class="comment-age">(12 Jan '14, 07:27)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="28821"></span><div id="comment-28821" class="comment"><div id="post-28821-score" class="comment-score"></div><div class="comment-text"><p>No, not the same i got a several parts analysed but still it's not the solution, so clearly, me as a first timer, am doing something wrong. Al do i'm a first timer with wireshark, therefore the question if someone could help or point me in the direction on how to get all the information out of the pcap stream, and more to see the crypted or hidden info or url's wich i can't find</p></div><div id="comment-28821-info" class="comment-info"><span class="comment-age">(12 Jan '14, 07:33)</span> <span class="comment-user userinfo">MarkV</span></div></div><span id="28827"></span><div id="comment-28827" class="comment"><div id="post-28827-score" class="comment-score"></div><div class="comment-text"><p><span>@MarkV</span>, this is a Q&amp;A site for Wireshark and its use. While we often have questions about captures and their contents, for your issue, which is quite general and seems to need a lot of interactive discussion, you will be much better off taking the problem to a malware forum.</p><p>If you have a specific question about Wireshark use do feel free to post it here.</p></div><div id="comment-28827-info" class="comment-info"><span class="comment-age">(12 Jan '14, 12:43)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-28818" class="comment-tools"></div><div class="clear"></div><div id="comment-28818-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "You have been told in other questions, that your request is off-toppic!!" by Kurt Knochner 12 Jan '14, 07:29

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28820"></span>

<div id="answer-container-28820" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28820-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28820-score" class="post-score" title="current number of votes">0</div><span id="post-28820-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You tagged your question with "encrypted", so if you can't decrypt the traffic you're out of luck. Unless you have a different approach (like statistical or differential analysis of meta data) that could deal with it without decrypting things. If you can decrypt the stuff you should look for unusual protocols, unusual protocol activity, hosts you don't recognize, strange delta times, and other things that seem odd.</p><p>This all requires patience and a lot of experience in reading packet traces, of course.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jan '14, 07:29</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-28820" class="comments-container"></div><div id="comment-tools-28820" class="comment-tools"></div><div class="clear"></div><div id="comment-28820-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

