+++
type = "question"
title = "Unrecognized fields and dumping the analyzes to a file"
description = '''Hi, We&#x27;re planning on extending an existing dissector, to support a new protocol extension. In order to see that all &quot;holes&quot; had been implemented, we have a pcap file that contains packets which should be all correctly analyzed by our extension. Is there a way to dump the analyzes of the Wireshark t...'''
date = "2012-12-18T09:01:00Z"
lastmod = "2012-12-18T09:03:00Z"
weight = 17042
keywords = [ "file", "unrezognized", "dump", "analysis" ]
aliases = [ "/questions/17042" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Unrecognized fields and dumping the analyzes to a file](/questions/17042/unrecognized-fields-and-dumping-the-analyzes-to-a-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17042-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17042-score" class="post-score" title="current number of votes">0</div><span id="post-17042-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>We're planning on extending an existing dissector, to support a new protocol extension. In order to see that all "holes" had been implemented, we have a pcap file that contains packets which should be all correctly analyzed by our extension.</p><p>Is there a way to dump the analyzes of the Wireshark to a file and not only to display them by the GUI ? We need that in order to be able to check that there are no more "Unrecognized" and to function as acceptance test of the dissector?</p><p>If you think there is a better way to achieve this, suggestions are truly accepted.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span> <span class="post-tag tag-link-unrezognized" rel="tag" title="see questions tagged &#39;unrezognized&#39;">unrezognized</span> <span class="post-tag tag-link-dump" rel="tag" title="see questions tagged &#39;dump&#39;">dump</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Dec '12, 09:01</strong></p><img src="https://secure.gravatar.com/avatar/e867074b1bd0dbe816dea05d588795bd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lmsne&#39;s gravatar image" /><p><span>lmsne</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lmsne has no accepted answers">0%</span></p></div></div><div id="comments-container-17042" class="comments-container"></div><div id="comment-tools-17042" class="comment-tools"></div><div class="clear"></div><div id="comment-17042-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17043"></span>

<div id="answer-container-17043" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17043-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17043-score" class="post-score" title="current number of votes">2</div><span id="post-17043-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="lmsne has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://www.wireshark.org/docs/man-pages/tshark.html">tshark</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Dec '12, 09:03</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-17043" class="comments-container"></div><div id="comment-tools-17043" class="comment-tools"></div><div class="clear"></div><div id="comment-17043-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

