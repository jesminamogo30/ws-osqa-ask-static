+++
type = "question"
title = "How do you correct C++ Run Time errors when opening I/O Graph"
description = '''I am in a Wireshark training class wit Laura Chappell now and WS consistently encounters a C++ Error were the App has requested the Run Time to terminate in an unusual way. I&#x27;m running Wireshark 1.8.6 SVN Rev 48142 on HP Playbook with Windows XP SP3. I&#x27;m not logging anyting in the event logs and Dr....'''
date = "2013-04-18T12:47:00Z"
lastmod = "2013-04-19T07:10:00Z"
weight = 20588
keywords = [ "c++", "iograph" ]
aliases = [ "/questions/20588" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do you correct C++ Run Time errors when opening I/O Graph](/questions/20588/how-do-you-correct-c-run-time-errors-when-opening-io-graph)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20588-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20588-score" class="post-score" title="current number of votes">0</div><span id="post-20588-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am in a Wireshark training class wit Laura Chappell now and WS consistently encounters a C++ Error were the App has requested the Run Time to terminate in an unusual way.</p><p>I'm running Wireshark 1.8.6 SVN Rev 48142 on HP Playbook with Windows XP SP3. I'm not logging anyting in the event logs and Dr. Watson is not picking anything up. Several of us in class are experiencing this. Have you see this before ? Probable cause, solution?</p><p>Thank you, Brian</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-c++" rel="tag" title="see questions tagged &#39;c++&#39;">c++</span> <span class="post-tag tag-link-iograph" rel="tag" title="see questions tagged &#39;iograph&#39;">iograph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '13, 12:47</strong></p><img src="https://secure.gravatar.com/avatar/6b51e85ded360c2a8999942f5e5c7e41?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bf951&#39;s gravatar image" /><p><span>bf951</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bf951 has no accepted answers">0%</span></p></div></div><div id="comments-container-20588" class="comments-container"></div><div id="comment-tools-20588" class="comment-tools"></div><div class="clear"></div><div id="comment-20588-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20592"></span>

<div id="answer-container-20592" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20592-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20592-score" class="post-score" title="current number of votes">0</div><span id="post-20592-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Night be the problem fixed here <a href="http://anonsvn.wireshark.org/viewvc/viewvc.cgi?view=rev&amp;revision=48893">http://anonsvn.wireshark.org/viewvc/viewvc.cgi?view=rev&amp;revision=48893</a> could you try the latest development build <a href="http://www.wireshark.org/download/automated/">http://www.wireshark.org/download/automated/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Apr '13, 14:02</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-20592" class="comments-container"><span id="20625"></span><div id="comment-20625" class="comment"><div id="post-20625-score" class="comment-score"></div><div class="comment-text"><p>Thank you for your response. I have installed the latest 32 bit development version as of 9 AM EST along with the PCAP update and the issue continues with no logged details. Are there any traces or debug modes I can run the tools in?</p><p>Thanks again, Brian</p></div><div id="comment-20625-info" class="comment-info"><span class="comment-age">(19 Apr '13, 06:38)</span> <span class="comment-user userinfo">bf951</span></div></div><span id="20627"></span><div id="comment-20627" class="comment"><div id="post-20627-score" class="comment-score"></div><div class="comment-text"><p>Best would be to open a bug at <a href="https://bugs.wireshark.org/bugzilla/">https://bugs.wireshark.org/bugzilla/</a> attaching the trace file if possible together with a description on how to reproduce the problem.</p></div><div id="comment-20627-info" class="comment-info"><span class="comment-age">(19 Apr '13, 07:10)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-20592" class="comment-tools"></div><div class="clear"></div><div id="comment-20592-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

