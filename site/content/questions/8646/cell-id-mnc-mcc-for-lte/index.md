+++
type = "question"
title = "Cell-ID/ MNC /MCC for LTE"
description = '''Hi there, I am wondering about some discrepancies that I have noticed when using wireshark to read some LTE log files. Yesterday,some colleagues of mine and I were going through a log file. There were 3 different versions of wireshark being used we discovered later. Version 1.2 / v1.4 / v1.6. What w...'''
date = "2012-01-27T01:43:00Z"
lastmod = "2012-01-28T06:58:00Z"
weight = 8646
keywords = [ "cell-id", "lte", "s1-ap" ]
aliases = [ "/questions/8646" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Cell-ID/ MNC /MCC for LTE](/questions/8646/cell-id-mnc-mcc-for-lte)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8646-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8646-score" class="post-score" title="current number of votes">0</div><span id="post-8646-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there,</p><p>I am wondering about some discrepancies that I have noticed when using wireshark to read some LTE log files. Yesterday,some colleagues of mine and I were going through a log file. There were 3 different versions of wireshark being used we discovered later. Version 1.2 / v1.4 / v1.6. What we discovered while looking at the same S1-AP message in each version was that the cell identities were being presented as different values. Also we had a similar experience with the MNC/MCC. I have not had the time to look for other discrepancies so I am not sure if there are any.</p><p>This concerns me a bit as I know don't know which value is the correct one! Perhaps this has been brought to your attention previously? I you could give me some feedback or information on this I would really appreciate it.</p><p>Best Regards</p><p>Seamus McManus</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cell-id" rel="tag" title="see questions tagged &#39;cell-id&#39;">cell-id</span> <span class="post-tag tag-link-lte" rel="tag" title="see questions tagged &#39;lte&#39;">lte</span> <span class="post-tag tag-link-s1-ap" rel="tag" title="see questions tagged &#39;s1-ap&#39;">s1-ap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '12, 01:43</strong></p><img src="https://secure.gravatar.com/avatar/c151d0e50690d4a98b44e4db6f229c4d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="seamusmc&#39;s gravatar image" /><p><span>seamusmc</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="seamusmc has no accepted answers">0%</span></p></div></div><div id="comments-container-8646" class="comments-container"></div><div id="comment-tools-8646" class="comment-tools"></div><div class="clear"></div><div id="comment-8646-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8661"></span>

<div id="answer-container-8661" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8661-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8661-score" class="post-score" title="current number of votes">0</div><span id="post-8661-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>LTE protocol dissectors are under development so using the latest version 1.6.5 or a development version should give you the best results.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jan '12, 06:58</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-8661" class="comments-container"></div><div id="comment-tools-8661" class="comment-tools"></div><div class="clear"></div><div id="comment-8661-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

