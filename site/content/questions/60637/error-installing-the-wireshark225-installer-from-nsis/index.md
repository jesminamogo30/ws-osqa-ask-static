+++
type = "question"
title = "Error installing the wireshark2.2.5 installer from NSIS"
description = ''' Im using a 64bit Machine but im not able to install '''
date = "2017-04-07T03:53:00Z"
lastmod = "2017-04-13T08:13:00Z"
weight = 60637
keywords = [ "wireshark2.2.5" ]
aliases = [ "/questions/60637" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Error installing the wireshark2.2.5 installer from NSIS](/questions/60637/error-installing-the-wireshark225-installer-from-nsis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60637-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60637-score" class="post-score" title="current number of votes">0</div><span id="post-60637-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/error.JPG" alt="alt text" /></p><p>Im using a 64bit Machine but im not able to install</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark2.2.5" rel="tag" title="see questions tagged &#39;wireshark2.2.5&#39;">wireshark2.2.5</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '17, 03:53</strong></p><img src="https://secure.gravatar.com/avatar/600778689935688cd4eaaa966e880cae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DhanuShalz&#39;s gravatar image" /><p><span>DhanuShalz</span><br />
<span class="score" title="36 reputation points">36</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DhanuShalz has one accepted answer">100%</span></p></img></div></div><div id="comments-container-60637" class="comments-container"><span id="60639"></span><div id="comment-60639" class="comment"><div id="post-60639-score" class="comment-score"></div><div class="comment-text"><p>Did you verify that the operating system is 64bit as well? Maybe you're running a 32bit OS on 64bit hardware...</p></div><div id="comment-60639-info" class="comment-info"><span class="comment-age">(07 Apr '17, 03:56)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="60641"></span><div id="comment-60641" class="comment"><div id="post-60641-score" class="comment-score"></div><div class="comment-text"><p><img src="https://osqa-ask.wireshark.org/upfiles/Captureqewfewf_k0onjy4.JPG" alt="alt text" /></p></div><div id="comment-60641-info" class="comment-info"><span class="comment-age">(07 Apr '17, 04:12)</span> <span class="comment-user userinfo">DhanuShalz</span></div></div><span id="60642"></span><div id="comment-60642" class="comment"><div id="post-60642-score" class="comment-score"></div><div class="comment-text"><p>Is this an official release version or your own built installer?</p></div><div id="comment-60642-info" class="comment-info"><span class="comment-age">(07 Apr '17, 04:26)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="60643"></span><div id="comment-60643" class="comment"><div id="post-60643-score" class="comment-score"></div><div class="comment-text"><p>my own built installer</p></div><div id="comment-60643-info" class="comment-info"><span class="comment-age">(07 Apr '17, 04:28)</span> <span class="comment-user userinfo">DhanuShalz</span></div></div><span id="60646"></span><div id="comment-60646" class="comment"><div id="post-60646-score" class="comment-score"></div><div class="comment-text"><p>Can you try with the release version?</p></div><div id="comment-60646-info" class="comment-info"><span class="comment-age">(07 Apr '17, 05:50)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="60647"></span><div id="comment-60647" class="comment not_top_scorer"><div id="post-60647-score" class="comment-score"></div><div class="comment-text"><p>i downloaded the 2.2.5 installer and i was able to install it!</p><p>but the installer one which i have built im not able to install!</p></div><div id="comment-60647-info" class="comment-info"><span class="comment-age">(07 Apr '17, 06:11)</span> <span class="comment-user userinfo">DhanuShalz</span></div></div><span id="60649"></span><div id="comment-60649" class="comment not_top_scorer"><div id="post-60649-score" class="comment-score"></div><div class="comment-text"><p>As you have seemingly managed to build Wireshark is there any chance you can go back to your 3 "open" Wireshark 2.2.x questions (<a href="https://ask.wireshark.org/questions/60106/nmake-fatal-error-u1052-file-makefilenmake-not-found">here</a>, <a href="https://ask.wireshark.org/questions/60305/wireshark-225-build-errors">here</a> and <a href="https://ask.wireshark.org/questions/60469/error-msb3073-building-wireshark-225">here</a>) and mark the answers which have helped you as "Accepted" by clicking the checkmark icon by the appropriate answer.</p><p>This helps others to see which answer solved the question.</p></div><div id="comment-60649-info" class="comment-info"><span class="comment-age">(07 Apr '17, 06:41)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="60650"></span><div id="comment-60650" class="comment not_top_scorer"><div id="post-60650-score" class="comment-score"></div><div class="comment-text"><p>sure i would that</p></div><div id="comment-60650-info" class="comment-info"><span class="comment-age">(07 Apr '17, 06:43)</span> <span class="comment-user userinfo">DhanuShalz</span></div></div></div><div id="comment-tools-60637" class="comment-tools"><span class="comments-showing"> showing 5 of 8 </span> <a href="#" class="show-all-comments-link">show 3 more comments</a></div><div class="clear"></div><div id="comment-60637-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60648"></span>

<div id="answer-container-60648" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60648-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60648-score" class="post-score" title="current number of votes">0</div><span id="post-60648-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="DhanuShalz has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It would appear that you've broken something when creating your own installer.</p><p>For a 64 bit build of Wireshark the NSIS installer checks the running platform with the code in packaging\nsis\x64.nsh.</p><p>There haven't been any reported issues with this x64 detection and it's running in the release version that does work, so somehow you've built an installer that is unable to correctly detect it's running on an x64 platform.</p><p>Did you build the installer from the same build command prompt with the same environment variable settings as used for the build?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Apr '17, 06:37</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></img></div></div><div id="comments-container-60648" class="comments-container"><span id="60652"></span><div id="comment-60652" class="comment"><div id="post-60652-score" class="comment-score"></div><div class="comment-text"><p>yeah, built from the same command prompt and the same environment variables were used! <span></span><span>@grahamb</span></p></div><div id="comment-60652-info" class="comment-info"><span class="comment-age">(07 Apr '17, 06:46)</span> <span class="comment-user userinfo">DhanuShalz</span></div></div><span id="60654"></span><div id="comment-60654" class="comment"><div id="post-60654-score" class="comment-score"></div><div class="comment-text"><p>NSIS version?</p></div><div id="comment-60654-info" class="comment-info"><span class="comment-age">(07 Apr '17, 07:46)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="60805"></span><div id="comment-60805" class="comment"><div id="post-60805-score" class="comment-score"></div><div class="comment-text"><p>upgrading NSIS to latest version resolved my issue! <span>@grahamb</span></p></div><div id="comment-60805-info" class="comment-info"><span class="comment-age">(13 Apr '17, 08:13)</span> <span class="comment-user userinfo">DhanuShalz</span></div></div></div><div id="comment-tools-60648" class="comment-tools"></div><div class="clear"></div><div id="comment-60648-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

