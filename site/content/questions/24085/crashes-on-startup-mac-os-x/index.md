+++
type = "question"
title = "Crashes on startup - mac os x"
description = '''I just installed xQuartz and Wireshark, however when wireshark asked me where X11 was on first start, I accidently choose XCode -- now it just crashes when I start it.'''
date = "2013-08-27T01:39:00Z"
lastmod = "2013-08-27T01:39:00Z"
weight = 24085
keywords = [ "x11", "mac", "wireshark" ]
aliases = [ "/questions/24085" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Crashes on startup - mac os x](/questions/24085/crashes-on-startup-mac-os-x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24085-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24085-score" class="post-score" title="current number of votes">0</div><span id="post-24085-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed xQuartz and Wireshark, however when wireshark asked me where X11 was on first start, I accidently choose XCode -- now it just crashes when I start it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-x11" rel="tag" title="see questions tagged &#39;x11&#39;">x11</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Aug '13, 01:39</strong></p><img src="https://secure.gravatar.com/avatar/035687df00d162cec025302373ebc076?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chovy&#39;s gravatar image" /><p><span>chovy</span><br />
<span class="score" title="41 reputation points">41</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chovy has no accepted answers">0%</span></p></div></div><div id="comments-container-24085" class="comments-container"></div><div id="comment-tools-24085" class="comment-tools"></div><div class="clear"></div><div id="comment-24085-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

