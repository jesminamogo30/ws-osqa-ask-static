+++
type = "question"
title = "VirtualBox interface not shown in list of interfaces."
description = '''I installed the VirtualBox and it installed VirtualBox Host-Only Network adapter as well. However after reinstalling newer version of VirtualBox the adapter disappeared from list of Wireshark&#x27;s captured interfaces. How can I restore it?'''
date = "2012-10-03T00:01:00Z"
lastmod = "2012-10-03T01:26:00Z"
weight = 14657
keywords = [ "interfacelist", "virtualbox" ]
aliases = [ "/questions/14657" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [VirtualBox interface not shown in list of interfaces.](/questions/14657/virtualbox-interface-not-shown-in-list-of-interfaces)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14657-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14657-score" class="post-score" title="current number of votes">0</div><span id="post-14657-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I installed the VirtualBox and it installed <code>VirtualBox Host-Only Network</code> adapter as well. However after reinstalling newer version of VirtualBox the adapter disappeared from list of Wireshark's captured interfaces. How can I restore it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfacelist" rel="tag" title="see questions tagged &#39;interfacelist&#39;">interfacelist</span> <span class="post-tag tag-link-virtualbox" rel="tag" title="see questions tagged &#39;virtualbox&#39;">virtualbox</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '12, 00:01</strong></p><img src="https://secure.gravatar.com/avatar/fddf252b14742aa2a17f821c9ab71f7d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="T_12&#39;s gravatar image" /><p><span>T_12</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="T_12 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Oct '12, 00:57</strong> </span></p></div></div><div id="comments-container-14657" class="comments-container"></div><div id="comment-tools-14657" class="comment-tools"></div><div class="clear"></div><div id="comment-14657-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14662"></span>

<div id="answer-container-14662" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14662-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14662-score" class="post-score" title="current number of votes">0</div><span id="post-14662-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you <a href="http://www.virtualbox.org/manual/ch06.html#network_hostonly">enable host-only networking</a> after installing the newer version of VirtualBox? If not, you may have to - i.e., perhaps the host-only adapter disappeared from Wireshark's list of interfaces because it disappeared from your installation of VirtualBox.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Oct '12, 01:26</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-14662" class="comments-container"></div><div id="comment-tools-14662" class="comment-tools"></div><div class="clear"></div><div id="comment-14662-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

