+++
type = "question"
title = "Wireshark 1.12 doesn&#x27;t dissect MMS correctly"
description = '''Hello, in this question is an advice to set context = 3 and OID = 1.0.9506.2.3. it&#x27;s works but only in wireshark up 1.10 (for me). Version 1.12 shows another protocol T.125. Mirek'''
date = "2014-09-23T04:11:00Z"
lastmod = "2014-09-23T11:56:00Z"
weight = 36516
keywords = [ "mms" ]
aliases = [ "/questions/36516" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 1.12 doesn't dissect MMS correctly](/questions/36516/wireshark-112-doesnt-dissect-mms-correctly)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36516-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36516-score" class="post-score" title="current number of votes">0</div><span id="post-36516-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, in this <a href="https://ask.wireshark.org/questions/6298/wireshark-cannot-dissect-mms-packets-that-dont-begin-with-initiate">question</a> is an advice to set context = 3 and OID = 1.0.9506.2.3. it's works but only in wireshark up 1.10 (for me). Version 1.12 shows another protocol T.125.</p><p>Mirek</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mms" rel="tag" title="see questions tagged &#39;mms&#39;">mms</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '14, 04:11</strong></p><img src="https://secure.gravatar.com/avatar/c14aa47c7ad9ce08c6dcfb9a38d95c03?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sobmir&#39;s gravatar image" /><p><span>sobmir</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sobmir has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>23 Sep '14, 04:43</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-36516" class="comments-container"></div><div id="comment-tools-36516" class="comment-tools"></div><div class="clear"></div><div id="comment-36516-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36519"></span>

<div id="answer-container-36519" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36519-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36519-score" class="post-score" title="current number of votes">0</div><span id="post-36519-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I've found answer there: <a href="http://blog.iec61850.com/2014/09/problem-with-wireshark-and-mms-in.html">link text</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Sep '14, 05:44</strong></p><img src="https://secure.gravatar.com/avatar/c14aa47c7ad9ce08c6dcfb9a38d95c03?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sobmir&#39;s gravatar image" /><p><span>sobmir</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sobmir has no accepted answers">0%</span></p></div></div><div id="comments-container-36519" class="comments-container"><span id="36520"></span><div id="comment-36520" class="comment"><div id="post-36520-score" class="comment-score"></div><div class="comment-text"><p>To help others looking at this question, the linked blog post indicated that the T.125 dissector should be disabled. However, the <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10350">bug</a> report that the blog post discussed has also been fixed, the heuristics for T.125 have been strengthened to prevent misidentification. The change has not (yet) been backported to the 1.12 release series, so you'll need to either grab an <a href="https://www.wireshark.org/download/automated/">automated build</a> or build wireshark yourself from the master branch.</p></div><div id="comment-36520-info" class="comment-info"><span class="comment-age">(23 Sep '14, 06:01)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="36536"></span><div id="comment-36536" class="comment"><div id="post-36536-score" class="comment-score"></div><div class="comment-text"><p>It is now merged in master-1.12 branch and will be part of Wireshark 1.12.2 (once released).</p></div><div id="comment-36536-info" class="comment-info"><span class="comment-age">(23 Sep '14, 11:56)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-36519" class="comment-tools"></div><div class="clear"></div><div id="comment-36519-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

