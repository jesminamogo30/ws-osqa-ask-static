+++
type = "question"
title = "Decoding Wireless 802.11w Protected Managment Frames"
description = '''Can any of the wireshark versions decode wireless 802.11w Protected Management Frames?'''
date = "2014-09-26T13:49:00Z"
lastmod = "2014-09-26T13:49:00Z"
weight = 36644
keywords = [ "frames", "management", "protected", "802.11w" ]
aliases = [ "/questions/36644" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding Wireless 802.11w Protected Managment Frames](/questions/36644/decoding-wireless-80211w-protected-managment-frames)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36644-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36644-score" class="post-score" title="current number of votes">0</div><span id="post-36644-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can any of the wireshark versions decode wireless 802.11w Protected Management Frames?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-frames" rel="tag" title="see questions tagged &#39;frames&#39;">frames</span> <span class="post-tag tag-link-management" rel="tag" title="see questions tagged &#39;management&#39;">management</span> <span class="post-tag tag-link-protected" rel="tag" title="see questions tagged &#39;protected&#39;">protected</span> <span class="post-tag tag-link-802.11w" rel="tag" title="see questions tagged &#39;802.11w&#39;">802.11w</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '14, 13:49</strong></p><img src="https://secure.gravatar.com/avatar/19ce326c8a9bbf96e6ad2a079aab6660?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dilip&#39;s gravatar image" /><p><span>dilip</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dilip has no accepted answers">0%</span></p></div></div><div id="comments-container-36644" class="comments-container"></div><div id="comment-tools-36644" class="comment-tools"></div><div class="clear"></div><div id="comment-36644-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

