+++
type = "question"
title = "Unable to pcap files"
description = '''I am unable to open wireshark, it just stops responding. Aometimes I am able to open 135 MB file just fine but 35 KB sometimes gives me issues or vice versa In taskmanager I just see the wireshark increasing in memory and CPU. How do I report this ? I am running windows 7 and wireshark version 2.0.1'''
date = "2016-02-08T23:48:00Z"
lastmod = "2016-02-09T02:40:00Z"
weight = 49989
keywords = [ "performance", "cpu" ]
aliases = [ "/questions/49989" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to pcap files](/questions/49989/unable-to-pcap-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49989-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49989-score" class="post-score" title="current number of votes">0</div><span id="post-49989-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am unable to open wireshark, it just stops responding. Aometimes I am able to open 135 MB file just fine but 35 KB sometimes gives me issues or vice versa</p><p>In taskmanager I just see the wireshark increasing in memory and CPU.</p><p>How do I report this ? I am running windows 7 and wireshark version 2.0.1</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-performance" rel="tag" title="see questions tagged &#39;performance&#39;">performance</span> <span class="post-tag tag-link-cpu" rel="tag" title="see questions tagged &#39;cpu&#39;">cpu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Feb '16, 23:48</strong></p><img src="https://secure.gravatar.com/avatar/70528b000906030a5a2bf9afb2b39044?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="csharma&#39;s gravatar image" /><p><span>csharma</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="csharma has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Feb '16, 23:58</strong> </span></p></div></div><div id="comments-container-49989" class="comments-container"></div><div id="comment-tools-49989" class="comment-tools"></div><div class="clear"></div><div id="comment-49989-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49992"></span>

<div id="answer-container-49992" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49992-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49992-score" class="post-score" title="current number of votes">0</div><span id="post-49992-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Possibly a bug, if you can share the capture file that has the problem please raise an entry on the Wireshark <a href="https://bugs.wireshark.org">Bugzilla</a>, attaching the capture to the entry. You can mark the attachment as private to restrict access to the Wireshark Core Developers.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Feb '16, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-49992" class="comments-container"></div><div id="comment-tools-49992" class="comment-tools"></div><div class="clear"></div><div id="comment-49992-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

