+++
type = "question"
title = "[Protocol Comparisons] TCP, UDP, SCTP (DCCP)"
description = '''Hi. I want to know how to measure [1] Throughput [2] Loss [3] Delay and Jitter on my application. I am running a video streaming server in my office on both LAN and WIFI. It runs TCP,UDP and SCTP, all over RTP.   As I understand it, I get the throughput from the IO graphs? in WS. (PLEASE CORRECT ME ...'''
date = "2012-09-08T06:45:00Z"
lastmod = "2012-09-08T06:45:00Z"
weight = 14139
keywords = [ "graphs", "statistics", "qos", "measurements", "protocols" ]
aliases = [ "/questions/14139" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[Protocol Comparisons\] TCP, UDP, SCTP (DCCP)](/questions/14139/protocol-comparisons-tcp-udp-sctp-dccp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14139-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14139-score" class="post-score" title="current number of votes">0</div><span id="post-14139-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi. I want to know how to measure [1] Throughput [2] Loss [3] Delay and Jitter on my application. I am running a video streaming server in my office on both LAN and WIFI. It runs TCP,UDP and SCTP, all over RTP.</p><ol><li>As I understand it, I get the throughput from the IO graphs? in WS. (PLEASE CORRECT ME IF IM WRONG)</li><li>I want to measure delay, but I only know the time when I receive each packet on my client computers. Is there a way to work around this?</li><li>As I understand, Loss = #bytes/packet/data SENT - #bytes/packet/data RECEIVED</li><li>How can one measure Jitter?</li><li>Is there a way for me to save the stats I capture and have another software plot it? or does wireshark have it own comprehensive graph/plot generator?</li></ol><p>I can't find another network analyzer and most people I know say that WS is probably the one that can help me get all these stats. any response will be GREATLY appreciated! Thank you very much!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graphs" rel="tag" title="see questions tagged &#39;graphs&#39;">graphs</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-qos" rel="tag" title="see questions tagged &#39;qos&#39;">qos</span> <span class="post-tag tag-link-measurements" rel="tag" title="see questions tagged &#39;measurements&#39;">measurements</span> <span class="post-tag tag-link-protocols" rel="tag" title="see questions tagged &#39;protocols&#39;">protocols</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Sep '12, 06:45</strong></p><img src="https://secure.gravatar.com/avatar/fc0c70933f4e421298b8731bf69ea1a7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rjnsalvador&#39;s gravatar image" /><p><span>rjnsalvador</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rjnsalvador has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Sep '12, 06:47</strong> </span></p></div></div><div id="comments-container-14139" class="comments-container"></div><div id="comment-tools-14139" class="comment-tools"></div><div class="clear"></div><div id="comment-14139-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

