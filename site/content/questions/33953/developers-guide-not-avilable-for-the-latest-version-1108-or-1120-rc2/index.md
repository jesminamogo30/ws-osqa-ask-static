+++
type = "question"
title = "Developer&#x27;s guide not avilable for the latest version 1.10.8 or 1.12.0-rc2"
description = '''Hi , I am unable to see the latest developer&#x27;s guide. Earlier it was available on the web page. Now in all the links only version 1.8 developer&#x27;s guide only available. http://www.wireshark.org/docs/wsdg_html_chunked/ I think we are having the old version of Developer&#x27;s guide on the web page. ( this ...'''
date = "2014-06-19T02:57:00Z"
lastmod = "2014-06-24T07:42:00Z"
weight = 33953
keywords = [ "developers", "guide", "latest" ]
aliases = [ "/questions/33953" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Developer's guide not avilable for the latest version 1.10.8 or 1.12.0-rc2](/questions/33953/developers-guide-not-avilable-for-the-latest-version-1108-or-1120-rc2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33953-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33953-score" class="post-score" title="current number of votes">0</div><span id="post-33953-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi ,</p><p>I am unable to see the latest developer's guide. Earlier it was available on the web page.</p><p>Now in all the links only version 1.8 developer's guide only available.</p><p><a href="http://www.wireshark.org/docs/wsdg_html_chunked/">http://www.wireshark.org/docs/wsdg_html_chunked/</a></p><p>I think we are having the old version of Developer's guide on the web page. ( this is using SVN but latest uses GIT )</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-developers" rel="tag" title="see questions tagged &#39;developers&#39;">developers</span> <span class="post-tag tag-link-guide" rel="tag" title="see questions tagged &#39;guide&#39;">guide</span> <span class="post-tag tag-link-latest" rel="tag" title="see questions tagged &#39;latest&#39;">latest</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jun '14, 02:57</strong></p><img src="https://secure.gravatar.com/avatar/10646f10c89f6221176215a232f0b0d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="phaniin&#39;s gravatar image" /><p><span>phaniin</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="phaniin has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jun '14, 22:34</strong> </span></p></div></div><div id="comments-container-33953" class="comments-container"></div><div id="comment-tools-33953" class="comment-tools"></div><div class="clear"></div><div id="comment-33953-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33962"></span>

<div id="answer-container-33962" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33962-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33962-score" class="post-score" title="current number of votes">2</div><span id="post-33962-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="phaniin has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please raise a request on <a href="https://bugs.wireshark.org/bugzilla/">bugzilla</a> if there isn't one already there. Submitting a patch to the docs via <a href="https://code.wireshark.org/review/#/q/status:open,n,z">gerrit</a> would also be welcomed.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jun '14, 11:11</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-33962" class="comments-container"><span id="34123"></span><div id="comment-34123" class="comment"><div id="post-34123-score" class="comment-score"></div><div class="comment-text"><p>Thanks for your reply Graham Bloice. The request has been raised and got fixed.</p><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10210">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10210</a></p></div><div id="comment-34123-info" class="comment-info"><span class="comment-age">(24 Jun '14, 07:42)</span> <span class="comment-user userinfo">phaniin</span></div></div></div><div id="comment-tools-33962" class="comment-tools"></div><div class="clear"></div><div id="comment-33962-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

