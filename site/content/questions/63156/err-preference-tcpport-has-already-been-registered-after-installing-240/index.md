+++
type = "question"
title = "Err  Preference tcp.port has already been registered after installing 2.4.0"
description = '''Hello there, I uninstalled Wireshark 64 bit 2.2.x and installed Wireshark 2.4 on a Windows 7 PC. Now I receive an Debug Console prompt that shows me the message &quot;Err Preference tcp.port has already been registered - Press any key to exit&quot;. I already unistalled Wireshark again and deleted the whole f...'''
date = "2017-07-26T23:19:00Z"
lastmod = "2017-07-26T23:54:00Z"
weight = 63156
keywords = [ "registered", "2.4.0", "tcp.port" ]
aliases = [ "/questions/63156" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Err Preference tcp.port has already been registered after installing 2.4.0](/questions/63156/err-preference-tcpport-has-already-been-registered-after-installing-240)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63156-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63156-score" class="post-score" title="current number of votes">0</div><span id="post-63156-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello there,</p><p>I uninstalled Wireshark 64 bit 2.2.x and installed Wireshark 2.4 on a Windows 7 PC. Now I receive an Debug Console prompt that shows me the message "Err Preference tcp.port has already been registered - Press any key to exit". I already unistalled Wireshark again and deleted the whole folder and installed it again - no change. Has anybody an idea to solve this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-registered" rel="tag" title="see questions tagged &#39;registered&#39;">registered</span> <span class="post-tag tag-link-2.4.0" rel="tag" title="see questions tagged &#39;2.4.0&#39;">2.4.0</span> <span class="post-tag tag-link-tcp.port" rel="tag" title="see questions tagged &#39;tcp.port&#39;">tcp.port</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '17, 23:19</strong></p><img src="https://secure.gravatar.com/avatar/8a039c6ad0cfdaa4622179b6d7ce785b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="conf_t&#39;s gravatar image" /><p><span>conf_t</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="conf_t has no accepted answers">0%</span></p></div></div><div id="comments-container-63156" class="comments-container"><span id="63157"></span><div id="comment-63157" class="comment"><div id="post-63157-score" class="comment-score"></div><div class="comment-text"><p>Do you use some dissector plugins?</p></div><div id="comment-63157-info" class="comment-info"><span class="comment-age">(26 Jul '17, 23:30)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-63156" class="comment-tools"></div><div class="clear"></div><div id="comment-63156-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63158"></span>

<div id="answer-container-63158" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63158-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63158-score" class="post-score" title="current number of votes">0</div><span id="post-63158-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, I always used Wireshark out-of-the-box. But this was a hint to also delete the preferences and filter files in my homedir\appdata\roaming\wireshark. And that was it. Thank you anyway, sometimes we don't see the wood for the trees :D</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jul '17, 23:54</strong></p><img src="https://secure.gravatar.com/avatar/8a039c6ad0cfdaa4622179b6d7ce785b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="conf_t&#39;s gravatar image" /><p><span>conf_t</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="conf_t has no accepted answers">0%</span></p></div></div><div id="comments-container-63158" class="comments-container"></div><div id="comment-tools-63158" class="comment-tools"></div><div class="clear"></div><div id="comment-63158-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

