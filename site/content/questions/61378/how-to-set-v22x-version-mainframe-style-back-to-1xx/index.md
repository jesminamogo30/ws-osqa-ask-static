+++
type = "question"
title = "how to set V2.2.X version mainframe style  back to 1.x.x"
description = '''after upgrade wireshark version to 2.2.3. I had changed the font as &quot;Lucida Console &amp;amp;&amp;amp; Font style=Normal &amp;amp;&amp;amp; Size=10&quot; but the mainframe style not changed, the font seems smaller than 1.x.x, the Line spacing is compact. is there has any way to set the style back to 1.x.x ?'''
date = "2017-05-13T00:03:00Z"
lastmod = "2017-05-13T00:03:00Z"
weight = 61378
keywords = [ "style" ]
aliases = [ "/questions/61378" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to set V2.2.X version mainframe style back to 1.x.x](/questions/61378/how-to-set-v22x-version-mainframe-style-back-to-1xx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61378-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61378-score" class="post-score" title="current number of votes">0</div><span id="post-61378-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>after upgrade wireshark version to 2.2.3. I had changed the font as "Lucida Console &amp;&amp; Font style=Normal &amp;&amp; Size=10" but the mainframe style not changed, the font seems smaller than 1.x.x, the Line spacing is compact. is there has any way to set the style back to 1.x.x ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-style" rel="tag" title="see questions tagged &#39;style&#39;">style</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '17, 00:03</strong></p><img src="https://secure.gravatar.com/avatar/853d7093103a60a3b0083b42b705b99e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="neil_hao&#39;s gravatar image" /><p><span>neil_hao</span><br />
<span class="score" title="26 reputation points">26</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="neil_hao has no accepted answers">0%</span></p></div></div><div id="comments-container-61378" class="comments-container"></div><div id="comment-tools-61378" class="comment-tools"></div><div class="clear"></div><div id="comment-61378-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

