+++
type = "question"
title = "packet loss"
description = '''Hi, plz how can i calculate the packet loss with wireshark capture, i have retransmission and duplicated ack packet? Thank yo for your help'''
date = "2015-04-01T01:32:00Z"
lastmod = "2015-04-01T01:34:00Z"
weight = 41083
keywords = [ "retransmissions", "duplicatedack", "packetloss" ]
aliases = [ "/questions/41083" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [packet loss](/questions/41083/packet-loss)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41083-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41083-score" class="post-score" title="current number of votes">0</div><span id="post-41083-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>plz how can i calculate the packet loss with wireshark capture, i have retransmission and duplicated ack packet?</p><p>Thank yo for your help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-retransmissions" rel="tag" title="see questions tagged &#39;retransmissions&#39;">retransmissions</span> <span class="post-tag tag-link-duplicatedack" rel="tag" title="see questions tagged &#39;duplicatedack&#39;">duplicatedack</span> <span class="post-tag tag-link-packetloss" rel="tag" title="see questions tagged &#39;packetloss&#39;">packetloss</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '15, 01:32</strong></p><img src="https://secure.gravatar.com/avatar/90875c0c2524531263f27b57e1d27ea3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hub&#39;s gravatar image" /><p><span>hub</span><br />
<span class="score" title="21 reputation points">21</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hub has no accepted answers">0%</span></p></div></div><div id="comments-container-41083" class="comments-container"></div><div id="comment-tools-41083" class="comment-tools"></div><div class="clear"></div><div id="comment-41083-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41084"></span>

<div id="answer-container-41084" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41084-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41084-score" class="post-score" title="current number of votes">0</div><span id="post-41084-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'd recommend counting "Previous segment not captured" messages, and making sure those are not dropped packets (drop count in the status bar should be 0, which is not a guarantee there is no loss at all, but it helps)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Apr '15, 01:34</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-41084" class="comments-container"></div><div id="comment-tools-41084" class="comment-tools"></div><div class="clear"></div><div id="comment-41084-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

