+++
type = "question"
title = "ACK seq number and number of retransmission"
description = '''http://www.filedropper.com/up2  in the above TCP stream, I notice that the expected next seq number of no.9 TCP packet is is 1902. But the following ack number of the ACK packet is 1903, which is 1 more than 1902. I&#x27;m wondering whether it is due to the retransmission (NO.9 is a retramsmission of NO....'''
date = "2015-10-28T07:37:00Z"
lastmod = "2015-10-28T07:37:00Z"
weight = 47016
keywords = [ "ack", "sequence" ]
aliases = [ "/questions/47016" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [ACK seq number and number of retransmission](/questions/47016/ack-seq-number-and-number-of-retransmission)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47016-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47016-score" class="post-score" title="current number of votes">0</div><span id="post-47016-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><a href="http://www.filedropper.com/up2">http://www.filedropper.com/up2</a> <img src="https://osqa-ask.wireshark.org/upfiles/wireshark_JgaJUrQ.png" alt="alt text" /></p><p>in the above TCP stream, I notice that the expected next seq number of no.9 TCP packet is is 1902. But the following ack number of the ACK packet is 1903, which is 1 more than 1902. I'm wondering whether it is due to the retransmission (NO.9 is a retramsmission of NO.8)? if so, then if there are N retransmission, the ack number is seq+N? if not, what are the causes for the inconsistency?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ack" rel="tag" title="see questions tagged &#39;ack&#39;">ack</span> <span class="post-tag tag-link-sequence" rel="tag" title="see questions tagged &#39;sequence&#39;">sequence</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Oct '15, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/2203cfcd179ad33d34d4f6f0cdedb4da?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kitty&#39;s gravatar image" /><p><span>kitty</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kitty has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Oct '15, 08:29</strong> </span></p></div></div><div id="comments-container-47016" class="comments-container"></div><div id="comment-tools-47016" class="comment-tools"></div><div class="clear"></div><div id="comment-47016-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

