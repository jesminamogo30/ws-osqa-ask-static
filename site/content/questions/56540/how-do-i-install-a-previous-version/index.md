+++
type = "question"
title = "How do I install a previous version?"
description = '''I allowed Wireshark to upgrade itself to the latest version, 2.2.1. Whoops! Other software that I am using is not compatible with that version. Now I am trying to get back to version 1.10.X. I found this: https://www.wireshark.org/download/win64/all-versions/ but the archives do not include an insta...'''
date = "2016-10-20T08:45:00Z"
lastmod = "2016-10-20T17:48:00Z"
weight = 56540
keywords = [ "version" ]
aliases = [ "/questions/56540" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [How do I install a previous version?](/questions/56540/how-do-i-install-a-previous-version)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56540-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56540-score" class="post-score" title="current number of votes">0</div><span id="post-56540-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I allowed Wireshark to upgrade itself to the latest version, 2.2.1. Whoops! Other software that I am using is not compatible with that version. Now I am trying to get back to version 1.10.X. I found this: <a href="https://www.wireshark.org/download/win64/all-versions/">https://www.wireshark.org/download/win64/all-versions/</a> but the archives do not include an installer. Would someone please advise me? Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-version" rel="tag" title="see questions tagged &#39;version&#39;">version</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Oct '16, 08:45</strong></p><img src="https://secure.gravatar.com/avatar/26d7dde8cfb0f101394088c4d8e0c8ba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="verticon&#39;s gravatar image" /><p><span>verticon</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="verticon has no accepted answers">0%</span></p></div></div><div id="comments-container-56540" class="comments-container"></div><div id="comment-tools-56540" class="comment-tools"></div><div class="clear"></div><div id="comment-56540-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="56542"></span>

<div id="answer-container-56542" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56542-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56542-score" class="post-score" title="current number of votes">2</div><span id="post-56542-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="verticon has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to <a href="https://www.wireshark.org/download.html#spelunking">https://www.wireshark.org/download.html#spelunking</a> and select one of the mirrors. Note that when I tried this a few minutes ago, the first one on the list (askApache) returned HTTP 403 Forbidden, but the other ones worked.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Oct '16, 10:02</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-56542" class="comments-container"><span id="56544"></span><div id="comment-56544" class="comment"><div id="post-56544-score" class="comment-score"></div><div class="comment-text"><p>Back up and running! Thanks a lot, Jim!</p></div><div id="comment-56544-info" class="comment-info"><span class="comment-age">(20 Oct '16, 11:25)</span> <span class="comment-user userinfo">verticon</span></div></div></div><div id="comment-tools-56542" class="comment-tools"></div><div class="clear"></div><div id="comment-56542-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="56546"></span>

<div id="answer-container-56546" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56546-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56546-score" class="post-score" title="current number of votes">1</div><span id="post-56546-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I found this: <a href="https://www.wireshark.org/download/win64/all-versions/">https://www.wireshark.org/download/win64/all-versions/</a> but the archives do not include an installer.</p></blockquote><p>The installers are down further in the list; look for files with names such as <code>Wireshark-win64-1.10.14.exe</code>, which is an installer executable for 1.10.14.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Oct '16, 17:48</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-56546" class="comments-container"></div><div id="comment-tools-56546" class="comment-tools"></div><div class="clear"></div><div id="comment-56546-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

