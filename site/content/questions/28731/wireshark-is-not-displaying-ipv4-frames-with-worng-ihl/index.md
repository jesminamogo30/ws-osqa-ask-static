+++
type = "question"
title = "Wireshark is not displaying IPv4 frames with worng IHL"
description = '''Wireshark is unable to capture IPv4 packets with worng IHL or is Windows drop them before sending? '''
date = "2014-01-09T06:58:00Z"
lastmod = "2014-01-09T08:02:00Z"
weight = 28731
keywords = [ "ipv4", "ihl" ]
aliases = [ "/questions/28731" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark is not displaying IPv4 frames with worng IHL](/questions/28731/wireshark-is-not-displaying-ipv4-frames-with-worng-ihl)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28731-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28731-score" class="post-score" title="current number of votes">0</div><span id="post-28731-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark is unable to capture IPv4 packets with worng IHL or is Windows drop them before sending?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipv4" rel="tag" title="see questions tagged &#39;ipv4&#39;">ipv4</span> <span class="post-tag tag-link-ihl" rel="tag" title="see questions tagged &#39;ihl&#39;">ihl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jan '14, 06:58</strong></p><img src="https://secure.gravatar.com/avatar/dd3dcfefcfc1267f216a6d2484d63bbe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dragos&#39;s gravatar image" /><p><span>dragos</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dragos has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Jan '14, 06:59</strong> </span></p></div></div><div id="comments-container-28731" class="comments-container"></div><div id="comment-tools-28731" class="comment-tools"></div><div class="clear"></div><div id="comment-28731-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28733"></span>

<div id="answer-container-28733" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28733-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28733-score" class="post-score" title="current number of votes">1</div><span id="post-28733-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you mean the IP header length by IHL, then yes: the capturing OS could have dropped the frames before Wireshark had a chance to see them. You can try to disable the IPv4 binding on the capturing interface. Depending on the OS, that might change the behavior. If however, the NIC driver dropped the frame (due to some IP and/or TCP offloading) or a router/firewall/etc. in front of your (capturing) system, disabling the IPv4 binding will have (most certainly) no effect, unless you disable IP and/or TCP offloading as well.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jan '14, 08:02</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-28733" class="comments-container"></div><div id="comment-tools-28733" class="comment-tools"></div><div class="clear"></div><div id="comment-28733-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

