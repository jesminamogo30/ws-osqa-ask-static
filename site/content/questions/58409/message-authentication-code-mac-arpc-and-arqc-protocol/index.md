+++
type = "question"
title = "Message authentication code &quot;MAC&quot;, &quot;ARPC&quot;, and &quot;ARQC&quot; protocol"
description = '''Hi dudes. How to check ARPC, ARQC, and MAC communications in a .pcapng file. I will appreciate your help. Thanks!'''
date = "2016-12-28T13:17:00Z"
lastmod = "2016-12-28T13:17:00Z"
weight = 58409
keywords = [ "arqc", "mac", "arpc" ]
aliases = [ "/questions/58409" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Message authentication code "MAC", "ARPC", and "ARQC" protocol](/questions/58409/message-authentication-code-mac-arpc-and-arqc-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58409-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58409-score" class="post-score" title="current number of votes">0</div><span id="post-58409-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi dudes.</p><p>How to check ARPC, ARQC, and MAC communications in a .pcapng file.</p><p>I will appreciate your help.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-arqc" rel="tag" title="see questions tagged &#39;arqc&#39;">arqc</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-arpc" rel="tag" title="see questions tagged &#39;arpc&#39;">arpc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Dec '16, 13:17</strong></p><img src="https://secure.gravatar.com/avatar/995dc97fe105a480a925f5eaa1b1d556?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vicious9999&#39;s gravatar image" /><p><span>Vicious9999</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vicious9999 has no accepted answers">0%</span></p></div></div><div id="comments-container-58409" class="comments-container"></div><div id="comment-tools-58409" class="comment-tools"></div><div class="clear"></div><div id="comment-58409-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

