+++
type = "question"
title = "Capturing 88E1 protocol"
description = '''Hi, i&#x27;m using the Colasoft Packet Builder in order to send out special ethernet frames.. to the same time i&#x27;m using Wireshark in order to monitor my ethernet traffic. A frame like FF FF FF FF FF FF FF FF FF FF FF FF 08 00 00 ... (filled up to 60 bytes) (IPv4) is beeing captured by Wireshark without ...'''
date = "2014-06-20T02:28:00Z"
lastmod = "2014-06-20T06:07:00Z"
weight = 33983
keywords = [ "homeplugprotocol" ]
aliases = [ "/questions/33983" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing 88E1 protocol](/questions/33983/capturing-88e1-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33983-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33983-score" class="post-score" title="current number of votes">0</div><span id="post-33983-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>i'm using the Colasoft Packet Builder in order to send out special ethernet frames.. to the same time i'm using Wireshark in order to monitor my ethernet traffic.</p><p>A frame like FF FF FF FF FF FF FF FF FF FF FF FF 08 00 00 ... (filled up to 60 bytes) (IPv4) is beeing captured by Wireshark without any problems. A frame like FF FF FF FF FF FF FF FF FF FF FF FF 88 E1 00 ... (filled up to 60 bytes) (HomePlug) is not beeing captured? How can I make Wireshak to capture a frame like that?</p><p>best regards</p><p>Michael</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-homeplugprotocol" rel="tag" title="see questions tagged &#39;homeplugprotocol&#39;">homeplugprotocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jun '14, 02:28</strong></p><img src="https://secure.gravatar.com/avatar/77404b69dc951bf0f16954c3e5c86e19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Michi&#39;s gravatar image" /><p><span>Michi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Michi has no accepted answers">0%</span></p></div></div><div id="comments-container-33983" class="comments-container"></div><div id="comment-tools-33983" class="comment-tools"></div><div class="clear"></div><div id="comment-33983-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33989"></span>

<div id="answer-container-33989" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33989-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33989-score" class="post-score" title="current number of votes">0</div><span id="post-33989-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi again,</p><p>tried wireshark with an windows xp notebook, can't believe.. but here i can see my frames.. tried it before with windows 7..</p><p>greez</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jun '14, 06:07</strong></p><img src="https://secure.gravatar.com/avatar/77404b69dc951bf0f16954c3e5c86e19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Michi&#39;s gravatar image" /><p><span>Michi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Michi has no accepted answers">0%</span></p></div></div><div id="comments-container-33989" class="comments-container"></div><div id="comment-tools-33989" class="comment-tools"></div><div class="clear"></div><div id="comment-33989-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

