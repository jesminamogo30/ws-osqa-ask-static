+++
type = "question"
title = "Tick intervals"
description = '''Hello, in Statistics-&amp;gt;IO Graph the smallest Tick interval is 0.001s. It is possible to take data to CSV in smaller intervals? It is needed to analising on microsecond field. Could you help me?'''
date = "2013-10-17T02:35:00Z"
lastmod = "2013-10-17T03:06:00Z"
weight = 26112
keywords = [ "intervals", "tick_interval" ]
aliases = [ "/questions/26112" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Tick intervals](/questions/26112/tick-intervals)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26112-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26112-score" class="post-score" title="current number of votes">0</div><span id="post-26112-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, in Statistics-&gt;IO Graph the smallest Tick interval is 0.001s. It is possible to take data to CSV in smaller intervals? It is needed to analising on microsecond field. Could you help me?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-intervals" rel="tag" title="see questions tagged &#39;intervals&#39;">intervals</span> <span class="post-tag tag-link-tick_interval" rel="tag" title="see questions tagged &#39;tick_interval&#39;">tick_interval</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '13, 02:35</strong></p><img src="https://secure.gravatar.com/avatar/28071bd7cf93e424c03dec9086ffa60f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="net16&#39;s gravatar image" /><p><span>net16</span><br />
<span class="score" title="46 reputation points">46</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="net16 has no accepted answers">0%</span></p></div></div><div id="comments-container-26112" class="comments-container"></div><div id="comment-tools-26112" class="comment-tools"></div><div class="clear"></div><div id="comment-26112-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26113"></span>

<div id="answer-container-26113" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26113-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26113-score" class="post-score" title="current number of votes">1</div><span id="post-26113-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="net16 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See my answer for a similar question:</p><blockquote><p><a href="http://ask.wireshark.org/questions/13084/customizing-tick-interval-in-io-graph">http://ask.wireshark.org/questions/13084/customizing-tick-interval-in-io-graph</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Oct '13, 02:46</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-26113" class="comments-container"><span id="26115"></span><div id="comment-26115" class="comment"><div id="post-26115-score" class="comment-score"></div><div class="comment-text"><p>Kurt, sorry, I have not noticed this post. Thank you very much.</p></div><div id="comment-26115-info" class="comment-info"><span class="comment-age">(17 Oct '13, 02:55)</span> <span class="comment-user userinfo">net16</span></div></div><span id="26116"></span><div id="comment-26116" class="comment"><div id="post-26116-score" class="comment-score"></div><div class="comment-text"><p>No problem.</p><p>Hint: If a supplied answer resolves your question can you please "accept" it by clicking the checkmark icon next to it. This highlights good answers for the benefit of subsequent users with the same or similar questions.</p></div><div id="comment-26116-info" class="comment-info"><span class="comment-age">(17 Oct '13, 02:56)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="26118"></span><div id="comment-26118" class="comment"><div id="post-26118-score" class="comment-score"></div><div class="comment-text"><p>Ok, thank you.</p></div><div id="comment-26118-info" class="comment-info"><span class="comment-age">(17 Oct '13, 03:06)</span> <span class="comment-user userinfo">net16</span></div></div></div><div id="comment-tools-26113" class="comment-tools"></div><div class="clear"></div><div id="comment-26113-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

