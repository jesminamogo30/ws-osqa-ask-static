+++
type = "question"
title = "[closed] how to capture using dumpcap"
description = '''Hello , can someone please tell me how to capture traffic using syntax and limit file size to 2.4GB and create new files once capture reaches 2.4GB . Any suggestions ???? '''
date = "2012-08-23T12:25:00Z"
lastmod = "2012-08-24T19:39:00Z"
weight = 13850
keywords = [ "capture", "dumpcap", "ringbuffer", "wireshark" ]
aliases = [ "/questions/13850" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] how to capture using dumpcap](/questions/13850/how-to-capture-using-dumpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13850-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13850-score" class="post-score" title="current number of votes">0</div><span id="post-13850-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello , can someone please tell me how to capture traffic using syntax and limit file size to 2.4GB and create new files once capture reaches 2.4GB . Any suggestions ????</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span> <span class="post-tag tag-link-ringbuffer" rel="tag" title="see questions tagged &#39;ringbuffer&#39;">ringbuffer</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '12, 12:25</strong></p><img src="https://secure.gravatar.com/avatar/9b296b0c1a89a6d15e65215e5e6c69b6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld0722&#39;s gravatar image" /><p><span>helloworld0722</span><br />
<span class="score" title="10 reputation points">10</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="helloworld0722 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Aug '12, 19:38</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-13850" class="comments-container"><span id="13851"></span><div id="comment-13851" class="comment"><div id="post-13851-score" class="comment-score"></div><div class="comment-text"><p>This is almost identical to your previous <a href="http://ask.wireshark.org/questions/13822/capture-ring-buffer-problem">question</a> so I'm closing this one. Please don't post duplicate questions, just wait a bit longer for a volunteer to post an answer.</p></div><div id="comment-13851-info" class="comment-info"><span class="comment-age">(23 Aug '12, 12:46)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="13870"></span><div id="comment-13870" class="comment"><div id="post-13870-score" class="comment-score"></div><div class="comment-text"><p>helloworld check above link I will comment on that page even though the question is different grahamb is not understanding it..</p></div><div id="comment-13870-info" class="comment-info"><span class="comment-age">(24 Aug '12, 04:53)</span> <span class="comment-user userinfo">Harsha</span></div></div><span id="13887"></span><div id="comment-13887" class="comment"><div id="post-13887-score" class="comment-score"></div><div class="comment-text"><p>What's the difference between "a new file should be made everytime it reaches maximum filesize" and "limit file size to 2.4GB and create new files once capture reaches 2.4GB"?</p></div><div id="comment-13887-info" class="comment-info"><span class="comment-age">(24 Aug '12, 19:39)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-13850" class="comment-tools"></div><div class="clear"></div><div id="comment-13850-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 23 Aug '12, 12:46

</div>

</div>

</div>

