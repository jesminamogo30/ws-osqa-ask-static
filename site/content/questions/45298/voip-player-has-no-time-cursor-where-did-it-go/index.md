+++
type = "question"
title = "VoIP Player Has no Time Cursor.  Where Did It Go?"
description = '''The VoIP Player used to have a cursor or pointer to show you the time position of the playback. This feature disappeared several versions back. Was this on purpose or by accident? Please bring the pointer back!!! This is in Telephony -&amp;gt; VoIP Calls -&amp;gt; Player -&amp;gt; Decode -&amp;gt; Play'''
date = "2015-08-21T11:10:00Z"
lastmod = "2015-08-21T11:10:00Z"
weight = 45298
keywords = [ "telephony", "voip", "player" ]
aliases = [ "/questions/45298" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [VoIP Player Has no Time Cursor. Where Did It Go?](/questions/45298/voip-player-has-no-time-cursor-where-did-it-go)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45298-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45298-score" class="post-score" title="current number of votes">0</div><span id="post-45298-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The VoIP Player used to have a cursor or pointer to show you the time position of the playback. This feature disappeared several versions back. Was this on purpose or by accident? Please bring the pointer back!!! This is in Telephony -&gt; VoIP Calls -&gt; Player -&gt; Decode -&gt; Play</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-telephony" rel="tag" title="see questions tagged &#39;telephony&#39;">telephony</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span> <span class="post-tag tag-link-player" rel="tag" title="see questions tagged &#39;player&#39;">player</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Aug '15, 11:10</strong></p><img src="https://secure.gravatar.com/avatar/f0b4b48833dce9b8674c733a1e8de91f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MikeHannegan&#39;s gravatar image" /><p><span>MikeHannegan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MikeHannegan has no accepted answers">0%</span></p></div></div><div id="comments-container-45298" class="comments-container"></div><div id="comment-tools-45298" class="comment-tools"></div><div class="clear"></div><div id="comment-45298-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

