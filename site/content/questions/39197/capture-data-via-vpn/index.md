+++
type = "question"
title = "Capture data via VPN"
description = '''Hello, i have an iphone running ios8 and i&#x27;ve set up a VPN to my home router running DD-WRT. Is it possible to capture data from my phone if i&#x27;m connected to VPN via 3g from a remote place [not my home connection]?'''
date = "2015-01-16T02:39:00Z"
lastmod = "2015-01-16T07:12:00Z"
weight = 39197
keywords = [ "ios", "vpn", "remote" ]
aliases = [ "/questions/39197" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture data via VPN](/questions/39197/capture-data-via-vpn)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39197-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39197-score" class="post-score" title="current number of votes">0</div><span id="post-39197-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, i have an iphone running ios8 and i've set up a VPN to my home router running DD-WRT. Is it possible to capture data from my phone if i'm connected to VPN via 3g from a remote place [not my home connection]?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ios" rel="tag" title="see questions tagged &#39;ios&#39;">ios</span> <span class="post-tag tag-link-vpn" rel="tag" title="see questions tagged &#39;vpn&#39;">vpn</span> <span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jan '15, 02:39</strong></p><img src="https://secure.gravatar.com/avatar/57a346c51606f30cffeaf3ea7bf48656?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LGMan&#39;s gravatar image" /><p><span>LGMan</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LGMan has one accepted answer">100%</span></p></div></div><div id="comments-container-39197" class="comments-container"></div><div id="comment-tools-39197" class="comment-tools"></div><div class="clear"></div><div id="comment-39197-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39208"></span>

<div id="answer-container-39208" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39208-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39208-score" class="post-score" title="current number of votes">0</div><span id="post-39208-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It would be possible to capture <strong>traffic</strong> of your iPhone <strong>within the mobile operator network</strong> and in front of your router (and within the network of ISPs how route your frames).</p><p><strong>However</strong>, if the VPN works properly, nobody will be able to see <strong>what</strong> you are transfering, as that's the main idea of having a VPN.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Jan '15, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-39208" class="comments-container"></div><div id="comment-tools-39208" class="comment-tools"></div><div class="clear"></div><div id="comment-39208-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

