+++
type = "question"
title = "can i sniff networks in other states"
description = '''is it possible too sniff a certainnetwork in another state or am i consticted to just a certain area '''
date = "2016-04-30T09:33:00Z"
lastmod = "2016-05-01T01:02:00Z"
weight = 52116
keywords = [ "urgent-help" ]
aliases = [ "/questions/52116" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can i sniff networks in other states](/questions/52116/can-i-sniff-networks-in-other-states)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52116-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52116-score" class="post-score" title="current number of votes">0</div><span id="post-52116-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is it possible too sniff a certainnetwork in another state or am i consticted to just a certain area</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-urgent-help" rel="tag" title="see questions tagged &#39;urgent-help&#39;">urgent-help</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Apr '16, 09:33</strong></p><img src="https://secure.gravatar.com/avatar/335fdcbd9dd4bbdb6bb6f00cc7a426e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Brooklyn%20Boy%20Mainia&#39;s gravatar image" /><p><span>Brooklyn Boy...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Brooklyn Boy Mainia has no accepted answers">0%</span></p></div></div><div id="comments-container-52116" class="comments-container"></div><div id="comment-tools-52116" class="comment-tools"></div><div class="clear"></div><div id="comment-52116-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52118"></span>

<div id="answer-container-52118" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52118-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52118-score" class="post-score" title="current number of votes">1</div><span id="post-52118-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That depends one the meaning of "can":</p><ul><li><p>technically, you can (you are able to) sniff any Ethernet network to which you can physically connect your capturing interface, or any WiFi network your capturing WLAN adaptor is close enough to receive. You may use tools for remote capturing - one box, called "probe", is located on such a place that it can meet the requirements above, and either sends the captured frames to your other machine from which you control the capturing process and analyse the capture, or stores the captured data to a file which you retrieve later.</p></li><li><p>legally, you can (you are allowed to) only sniff networks you own yourself or whose owner/administrator has granted you the permission to do so, or for which you have other legal grounds to do so. Being a citizen of the state where the network you are interested in is located is usually not sufficient to give you the right to sniff it.</p></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 May '16, 01:02</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-52118" class="comments-container"></div><div id="comment-tools-52118" class="comment-tools"></div><div class="clear"></div><div id="comment-52118-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

