+++
type = "question"
title = "Windows 8 Support"
description = '''I&#x27;m using the Windows 8 Developer Preview and wireshark does not appear to function. I get the error &quot;The NPF driver isn&#x27;t running. You may have trouble capturing or listing interfaces.&quot; when attempting to run it. Google appears to be full of useless links for this error, so I was hoping I might hav...'''
date = "2011-11-14T13:19:00Z"
lastmod = "2013-06-19T10:42:00Z"
weight = 7425
keywords = [ "winpcap", "windows8", "wireshark" ]
aliases = [ "/questions/7425" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Windows 8 Support](/questions/7425/windows-8-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7425-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7425-score" class="post-score" title="current number of votes">1</div><span id="post-7425-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using the Windows 8 Developer Preview and wireshark does not appear to function.</p><p>I get the error "The NPF driver isn't running. You may have trouble capturing or listing interfaces." when attempting to run it.</p><p>Google appears to be full of useless links for this error, so I was hoping I might have some more luck here.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-windows8" rel="tag" title="see questions tagged &#39;windows8&#39;">windows8</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '11, 13:19</strong></p><img src="https://secure.gravatar.com/avatar/ac3fe7b18c10d02acf924c6c3caad091?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dotmister&#39;s gravatar image" /><p><span>Dotmister</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dotmister has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-7425" class="comments-container"><span id="14387"></span><div id="comment-14387" class="comment"><div id="post-14387-score" class="comment-score"></div><div class="comment-text"><p>I have the exakt same problem running Win8 preview 8400. I'm running both wireshark and wpcap at win7-compability. Both as administrator. I've tried all versions "backwards" down to winXPsp2. To no avail.</p><p>Please advise.</p></div><div id="comment-14387-info" class="comment-info"><span class="comment-age">(19 Sep '12, 14:02)</span> <span class="comment-user userinfo">Fredrik</span></div></div></div><div id="comment-tools-7425" class="comment-tools"></div><div class="clear"></div><div id="comment-7425-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="19881"></span>

<div id="answer-container-19881" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19881-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19881-score" class="post-score" title="current number of votes">4</div><span id="post-19881-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Installing the new version of WinPcap (4.1.3) seems to fix the problem as it is compatible with Windows 8.</p><p><a href="http://www.winpcap.org">http://www.winpcap.org</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Mar '13, 10:08</strong></p><img src="https://secure.gravatar.com/avatar/3a704ea5ea1dbfc90d136bbd7e5ab56e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jda_cfb&#39;s gravatar image" /><p><span>jda_cfb</span><br />
<span class="score" title="86 reputation points">86</span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jda_cfb has one accepted answer">100%</span></p></div></div><div id="comments-container-19881" class="comments-container"><span id="19967"></span><div id="comment-19967" class="comment"><div id="post-19967-score" class="comment-score"></div><div class="comment-text"><p>Hi, I have installed 4.1.3 of WinPcap, it worked. Now I restarted my Computer and the same error appears?!? Any ideas? BR a</p></div><div id="comment-19967-info" class="comment-info"><span class="comment-age">(31 Mar '13, 02:15)</span> <span class="comment-user userinfo">Alexollon</span></div></div><span id="20381"></span><div id="comment-20381" class="comment"><div id="post-20381-score" class="comment-score">1</div><div class="comment-text"><p><span>@Alexollon</span> Did you uncheck 'Start NPF service on startup'? If you did, the service will need to be started manually. Try running <code>net start npf</code> and then restart Wireshark.</p></div><div id="comment-20381-info" class="comment-info"><span class="comment-age">(12 Apr '13, 06:04)</span> <span class="comment-user userinfo">jaraco</span></div></div><span id="22180"></span><div id="comment-22180" class="comment"><div id="post-22180-score" class="comment-score"></div><div class="comment-text"><p>jaraco's solution worked for me, windows 8 user here, never reinstalled anything i just went to command prompt and net start npf like was suggested and it works. i must have been one of those people who also unchecked running npf as a service on startup. thanks jaraco</p></div><div id="comment-22180-info" class="comment-info"><span class="comment-age">(19 Jun '13, 10:42)</span> <span class="comment-user userinfo">Rylan</span></div></div></div><div id="comment-tools-19881" class="comment-tools"></div><div class="clear"></div><div id="comment-19881-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="7427"></span>

<div id="answer-container-7427" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7427-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7427-score" class="post-score" title="current number of votes">4</div><span id="post-7427-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Probably WinPCAP did not install correctly, which is why Wireshark isn't able to capture on your cards. Try deinstalling Wireshark and reinstalling while using the compatibility feature to make the installer think that it is running on Windows 7/Vista: right click and then choose "properties" on the installer, go to the "Compatibility" tab and set it to "Windows 7". After that everything should work.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Nov '11, 15:30</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-7427" class="comments-container"><span id="7449"></span><div id="comment-7449" class="comment"><div id="post-7449-score" class="comment-score"></div><div class="comment-text"><p>http://www.adamncasey.co.uk/upload/image/3570/d/o/</p><p>That doesn't appear to have worked.</p></div><div id="comment-7449-info" class="comment-info"><span class="comment-age">(15 Nov '11, 09:39)</span> <span class="comment-user userinfo">Dotmister</span></div></div><span id="7465"></span><div id="comment-7465" class="comment"><div id="post-7465-score" class="comment-score">3</div><div class="comment-text"><p>U sure ? Remember to set the compatibility for the winpcap installer, not for wireshark - works like a charm for me</p></div><div id="comment-7465-info" class="comment-info"><span class="comment-age">(16 Nov '11, 03:01)</span> <span class="comment-user userinfo">Landi</span></div></div><span id="17417"></span><div id="comment-17417" class="comment"><div id="post-17417-score" class="comment-score"></div><div class="comment-text"><p>i tried installing WinPcap by itself, changed compatibility mode run, and i am an administrator, still didn't work. my windows build is 9200</p></div><div id="comment-17417-info" class="comment-info"><span class="comment-age">(03 Jan '13, 11:04)</span> <span class="comment-user userinfo">hermione</span></div></div><span id="20031"></span><div id="comment-20031" class="comment"><div id="post-20031-score" class="comment-score"></div><div class="comment-text"><p>I tested with 4.1.3 and it works!</p><p>success</p></div><div id="comment-20031-info" class="comment-info"><span class="comment-age">(02 Apr '13, 14:37)</span> <span class="comment-user userinfo">Brinkman88</span></div></div></div><div id="comment-tools-7427" class="comment-tools"></div><div class="clear"></div><div id="comment-7427-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

