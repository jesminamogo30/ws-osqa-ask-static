+++
type = "question"
title = "Wlan Retry"
description = '''Can any one tell me where in the decode window you will find the wlan.fc.retry? I have look everywhere to no avail. When I use a color for it highlights way to many packets. I am thing there may be a bug with this filter in a wireless capture.'''
date = "2017-10-27T10:27:00Z"
lastmod = "2017-10-27T12:00:00Z"
weight = 64300
keywords = [ "wlanfcretry" ]
aliases = [ "/questions/64300" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wlan Retry](/questions/64300/wlan-retry)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64300-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64300-score" class="post-score" title="current number of votes">0</div><span id="post-64300-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can any one tell me where in the decode window you will find the wlan.fc.retry? I have look everywhere to no avail. When I use a color for it highlights way to many packets. I am thing there may be a bug with this filter in a wireless capture.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wlanfcretry" rel="tag" title="see questions tagged &#39;wlanfcretry&#39;">wlanfcretry</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '17, 10:27</strong></p><img src="https://secure.gravatar.com/avatar/328e4e3c363565d7a50d22167dd1a5b8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="philvilla&#39;s gravatar image" /><p><span>philvilla</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="philvilla has no accepted answers">0%</span></p></div></div><div id="comments-container-64300" class="comments-container"></div><div id="comment-tools-64300" class="comment-tools"></div><div class="clear"></div><div id="comment-64300-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="64304"></span>

<div id="answer-container-64304" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64304-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64304-score" class="post-score" title="current number of votes">0</div><span id="post-64304-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Is this what you are looking for? If you see a lot of them it could be because you have a lot of interference.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/retry.png" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Oct '17, 12:00</strong></p><img src="https://secure.gravatar.com/avatar/0a47ef51dd9c9996d194a4983295f5a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob%20Jones&#39;s gravatar image" /><p><span>Bob Jones</span><br />
<span class="score" title="1014 reputation points"><span>1.0k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob Jones has 19 accepted answers">21%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Oct '17, 12:01</strong> </span></p></div></div><div id="comments-container-64304" class="comments-container"></div><div id="comment-tools-64304" class="comment-tools"></div><div class="clear"></div><div id="comment-64304-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

