+++
type = "question"
title = "Remote Linux and local Windows"
description = '''If I am running WireShark on Windows, how do I capture traffic on a remote Linux machine? Do I use a Remote Capture Interfaces as described by section 4.9 of the documentation? The required Remote Packet Capture Protocol service appears to be for Windows only. Thanks'''
date = "2017-06-11T12:08:00Z"
lastmod = "2017-06-11T12:08:00Z"
weight = 61936
keywords = [ "remote" ]
aliases = [ "/questions/61936" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Remote Linux and local Windows](/questions/61936/remote-linux-and-local-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61936-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61936-score" class="post-score" title="current number of votes">0</div><span id="post-61936-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If I am running WireShark on Windows, how do I capture traffic on a remote Linux machine? Do I use a Remote Capture Interfaces as described by section 4.9 of the documentation? The required Remote Packet Capture Protocol service appears to be for Windows only. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jun '17, 12:08</strong></p><img src="https://secure.gravatar.com/avatar/938ce772de83dc07272b66f4b12a5453?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NotionCommotion&#39;s gravatar image" /><p><span>NotionCommotion</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NotionCommotion has no accepted answers">0%</span></p></div></div><div id="comments-container-61936" class="comments-container"></div><div id="comment-tools-61936" class="comment-tools"></div><div class="clear"></div><div id="comment-61936-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

