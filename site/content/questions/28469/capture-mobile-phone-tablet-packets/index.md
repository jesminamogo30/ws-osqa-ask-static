+++
type = "question"
title = "Capture Mobile Phone / tablet Packets"
description = '''Hello there, i would like to know if there is any way to capture cellphone and tablet packets using wireshark. Im running wireshark on win7, and both my cellphone (android) and my tablet (ipad) are connected to the same wifi as my computer. However, i only see my computer&#x27;s packets and overal activi...'''
date = "2013-12-28T23:38:00Z"
lastmod = "2013-12-29T11:20:00Z"
weight = 28469
keywords = [ "mobile", "capture", "tablet", "packet" ]
aliases = [ "/questions/28469" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture Mobile Phone / tablet Packets](/questions/28469/capture-mobile-phone-tablet-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28469-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28469-score" class="post-score" title="current number of votes">0</div><span id="post-28469-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello there, i would like to know if there is any way to capture cellphone and tablet packets using wireshark. Im running wireshark on win7, and both my cellphone (android) and my tablet (ipad) are connected to the same wifi as my computer. However, i only see my computer's packets and overal activity. Everytime i use my ipad or cellphone, wireshark indicates: bogus IP header length.</p><p>Thanks in Advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-tablet" rel="tag" title="see questions tagged &#39;tablet&#39;">tablet</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Dec '13, 23:38</strong></p><img src="https://secure.gravatar.com/avatar/4381ad7409f272423d1a7346a637aa5b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rick_013&#39;s gravatar image" /><p><span>Rick_013</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rick_013 has no accepted answers">0%</span></p></div></div><div id="comments-container-28469" class="comments-container"></div><div id="comment-tools-28469" class="comment-tools"></div><div class="clear"></div><div id="comment-28469-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28473"></span>

<div id="answer-container-28473" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28473-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28473-score" class="post-score" title="current number of votes">0</div><span id="post-28473-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>To capture traffic on wireless traffic please refer to this article - <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">WLAN</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Dec '13, 11:20</strong></p><img src="https://secure.gravatar.com/avatar/94630d1ea1108afeafb344e884044d15?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Boaz%20Galil&#39;s gravatar image" /><p><span>Boaz Galil</span><br />
<span class="score" title="56 reputation points">56</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Boaz Galil has no accepted answers">0%</span></p></div></div><div id="comments-container-28473" class="comments-container"></div><div id="comment-tools-28473" class="comment-tools"></div><div class="clear"></div><div id="comment-28473-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

