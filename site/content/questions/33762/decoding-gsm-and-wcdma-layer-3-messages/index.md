+++
type = "question"
title = "Decoding GSM and WCDMA Layer 3 Messages"
description = '''Hello  I&#x27;m working on my master thesis and have a probleme to solve. In it I try to automaticly analys captured data in a MS SQL Database. My problem ist now, that all layer 3 messages are stored in binary format like : &quot; 9F844932E2C080 &quot; or &quot; C53C185AE0F7808000000000430280 &quot;  How can I decode the m...'''
date = "2014-06-13T02:40:00Z"
lastmod = "2014-06-13T02:40:00Z"
weight = 33762
keywords = [ "wcdma", "layer", "gsm", "3", "decoding" ]
aliases = [ "/questions/33762" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding GSM and WCDMA Layer 3 Messages](/questions/33762/decoding-gsm-and-wcdma-layer-3-messages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33762-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33762-score" class="post-score" title="current number of votes">0</div><span id="post-33762-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I'm working on my master thesis and have a probleme to solve. In it I try to automaticly analys captured data in a MS SQL Database. My problem ist now, that all layer 3 messages are stored in binary format like : " 9F844932E2C080 " or " C53C185AE0F7808000000000430280 " How can I decode the messages back into normal text? Another problem is that the decode process should be in MS SQL too</p><p>greets rostkiste</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wcdma" rel="tag" title="see questions tagged &#39;wcdma&#39;">wcdma</span> <span class="post-tag tag-link-layer" rel="tag" title="see questions tagged &#39;layer&#39;">layer</span> <span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span> <span class="post-tag tag-link-3" rel="tag" title="see questions tagged &#39;3&#39;">3</span> <span class="post-tag tag-link-decoding" rel="tag" title="see questions tagged &#39;decoding&#39;">decoding</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '14, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/b7328a9fe8ddb1a007e60942988d405d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rostkiste&#39;s gravatar image" /><p><span>rostkiste</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rostkiste has no accepted answers">0%</span></p></div></div><div id="comments-container-33762" class="comments-container"></div><div id="comment-tools-33762" class="comment-tools"></div><div class="clear"></div><div id="comment-33762-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

