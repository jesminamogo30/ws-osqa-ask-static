+++
type = "question"
title = "Support and compatibility information  for Wireshark 1.10.1 (32-bit)"
description = '''Hi, Our organization is looking forward to migrating to Windows Server 2008R2 and Windows Server 2012R2. What is the status for compatibility for the version 1.10.1 against these OS. Regards, Amogh'''
date = "2015-11-18T03:37:00Z"
lastmod = "2015-11-18T04:13:00Z"
weight = 47702
keywords = [ "windows2012r2", "supported", "unsupported", "windows2008" ]
aliases = [ "/questions/47702" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Support and compatibility information for Wireshark 1.10.1 (32-bit)](/questions/47702/support-and-compatibility-information-for-wireshark-1101-32-bit)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47702-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47702-score" class="post-score" title="current number of votes">0</div><span id="post-47702-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Our organization is looking forward to migrating to <strong>Windows Server 2008R2</strong> and <strong>Windows Server 2012R2</strong>. What is the status for compatibility for the version 1.10.1 against these OS.</p><p>Regards, Amogh</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows2012r2" rel="tag" title="see questions tagged &#39;windows2012r2&#39;">windows2012r2</span> <span class="post-tag tag-link-supported" rel="tag" title="see questions tagged &#39;supported&#39;">supported</span> <span class="post-tag tag-link-unsupported" rel="tag" title="see questions tagged &#39;unsupported&#39;">unsupported</span> <span class="post-tag tag-link-windows2008" rel="tag" title="see questions tagged &#39;windows2008&#39;">windows2008</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '15, 03:37</strong></p><img src="https://secure.gravatar.com/avatar/80b1398f1dec49d0f4a9e61dd377599e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AmoghP&#39;s gravatar image" /><p><span>AmoghP</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AmoghP has no accepted answers">0%</span></p></div></div><div id="comments-container-47702" class="comments-container"></div><div id="comment-tools-47702" class="comment-tools"></div><div class="clear"></div><div id="comment-47702-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47704"></span>

<div id="answer-container-47704" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47704-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47704-score" class="post-score" title="current number of votes">0</div><span id="post-47704-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>1.10.x reached end of support on 5 June 2015.</p><p>1.12.x is the current stable version, very soon to be replaced with 2.0. See the Wiki <a href="https://wiki.wireshark.org/Development/LifeCycle">LifeCycle</a> page for more info.</p><p>As for Server 2K8 R2 and 2012R2, as they are both post Vista, they will be supported beyond 2.2.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Nov '15, 04:13</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-47704" class="comments-container"></div><div id="comment-tools-47704" class="comment-tools"></div><div class="clear"></div><div id="comment-47704-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

