+++
type = "question"
title = "Is this type of arp attack?"
description = '''I have a bad internet connection then i got this. Can anyone tell me about this? '''
date = "2016-05-15T00:05:00Z"
lastmod = "2016-05-15T02:15:00Z"
weight = 52577
keywords = [ "arp", "capture-filter", "help" ]
aliases = [ "/questions/52577" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Is this type of arp attack?](/questions/52577/is-this-type-of-arp-attack)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52577-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52577-score" class="post-score" title="current number of votes">0</div><span id="post-52577-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a bad internet connection then i got this. Can anyone tell me about this?</p><p><img src="https://osqa-ask.wireshark.org/upfiles/ask_n7jmMn8.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-arp" rel="tag" title="see questions tagged &#39;arp&#39;">arp</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '16, 00:05</strong></p><img src="https://secure.gravatar.com/avatar/d080989c248e47722d25155fa609aece?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="noobzers&#39;s gravatar image" /><p><span>noobzers</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="noobzers has no accepted answers">0%</span></p></img></div></div><div id="comments-container-52577" class="comments-container"></div><div id="comment-tools-52577" class="comment-tools"></div><div class="clear"></div><div id="comment-52577-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="52578"></span>

<div id="answer-container-52578" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52578-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52578-score" class="post-score" title="current number of votes">0</div><span id="post-52578-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well it looks more like a scan. But if this packet appear constantly and in the way you posted here, it can slow down your system. Because ARP processing has often a high cpu priority.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '16, 00:25</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-52578" class="comments-container"></div><div id="comment-tools-52578" class="comment-tools"></div><div class="clear"></div><div id="comment-52578-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="52581"></span>

<div id="answer-container-52581" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52581-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52581-score" class="post-score" title="current number of votes">0</div><span id="post-52581-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If the capture comes from a wireless interface of your PC, and your wireless AP's IP address (the most likely default gateway of your PC) is <strong>not</strong> 192.168.2.99, then someone is trying to map what machines exist in your wireless network and he has already cracked through your wireless security (if you don't use an open wireless network).</p><p>Else (i.e. if the capture comes from any interface of your PC, and regardless whether your router has 192.168.2.99 or not), the equipment which sends these arp requests is doing the scan, and it is likely one of your own ones which did not need to crack the wireless security. In a typical home network, a device doing that on a peaceful purpose is far less likely to exist than one infected with malware, so I'd be on high alert and would closely inspect the source device to find out what happens.</p><p><span><span>@Christian_R</span></span>'s suggestion that someone is scanning the network remotely would be fine if the address range in question would not be private (192.168.x.y/24). A scan over private addresses can <strong>not</strong> be done remotely over internet, it must come from a device for which such addresses are routable, i.e. inside your private network.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '16, 01:47</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 May '16, 01:57</strong> </span></p></div></div><div id="comments-container-52581" class="comments-container"><span id="52582"></span><div id="comment-52582" class="comment"><div id="post-52582-score" class="comment-score"></div><div class="comment-text"><p>I have never said, that the scan is done remotely. Of course it is local, thought it was clear.</p></div><div id="comment-52582-info" class="comment-info"><span class="comment-age">(15 May '16, 01:55)</span> <span class="comment-user userinfo">Christian_R</span></div></div><span id="52583"></span><div id="comment-52583" class="comment"><div id="post-52583-score" class="comment-score"></div><div class="comment-text"><p>Sorry, <span>@Christian_R</span>, the association between "scan" and "remotely over internet" somehow exists in my head, because a remote scan is a much smaller threat than a local one so it falls to other mental category.</p><p>I wanted to stress out that a local device performing a network scan is likely to be already hijacked, and quoted you improperly. Sorry again.</p></div><div id="comment-52583-info" class="comment-info"><span class="comment-age">(15 May '16, 02:04)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="52584"></span><div id="comment-52584" class="comment"><div id="post-52584-score" class="comment-score"></div><div class="comment-text"><p>In yet another way: from what you wrote, it seemed to me that the highest risk you expect is the CPU load coming from ARP processing, which implied to me that you have a remote scan in mind.</p></div><div id="comment-52584-info" class="comment-info"><span class="comment-age">(15 May '16, 02:06)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="52585"></span><div id="comment-52585" class="comment"><div id="post-52585-score" class="comment-score"></div><div class="comment-text"><p>Hm, I thought of this ARP and CPU relation, because the question is, if this ARPs could cause a bad internet connectivity. And this is my opinion to that. There might be more... And there might be another cause, too.</p></div><div id="comment-52585-info" class="comment-info"><span class="comment-age">(15 May '16, 02:15)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-52581" class="comment-tools"></div><div class="clear"></div><div id="comment-52581-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

