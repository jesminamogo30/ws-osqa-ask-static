+++
type = "question"
title = "Loading configuration files %100 Problem"
description = '''Hi all, My wireshark doesnt open few days later after installation. It&#x27;s freezing on &quot;loading configuration files %100&quot; screen. My computer Win 8.1 updated.  I cant terminate the Dumpcap.exe. For kill the dumpcap process I have to restart computer. After restarting the computer I can uninstall/reins...'''
date = "2014-06-24T07:41:00Z"
lastmod = "2014-06-24T10:23:00Z"
weight = 34122
keywords = [ "files", "start", "problem", "loading", "configuration" ]
aliases = [ "/questions/34122" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Loading configuration files %100 Problem](/questions/34122/loading-configuration-files-100-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34122-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34122-score" class="post-score" title="current number of votes">0</div><span id="post-34122-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>My wireshark doesnt open few days later after installation. It's freezing on "loading configuration files %100" screen. My computer Win 8.1 updated.</p><p>I cant terminate the Dumpcap.exe. For kill the dumpcap process I have to restart computer. After restarting the computer I can uninstall/reinstall wireshark and Its working but few days later again same problem accours.</p><p>Do you have any idea about this problem?</p><p>Regards. Emre</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-files" rel="tag" title="see questions tagged &#39;files&#39;">files</span> <span class="post-tag tag-link-start" rel="tag" title="see questions tagged &#39;start&#39;">start</span> <span class="post-tag tag-link-problem" rel="tag" title="see questions tagged &#39;problem&#39;">problem</span> <span class="post-tag tag-link-loading" rel="tag" title="see questions tagged &#39;loading&#39;">loading</span> <span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jun '14, 07:41</strong></p><img src="https://secure.gravatar.com/avatar/8928fe2196be37bfd0a9e414e6c27b96?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="emre&#39;s gravatar image" /><p><span>emre</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="emre has no accepted answers">0%</span></p></div></div><div id="comments-container-34122" class="comments-container"></div><div id="comment-tools-34122" class="comment-tools"></div><div class="clear"></div><div id="comment-34122-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34134"></span>

<div id="answer-container-34134" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34134-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34134-score" class="post-score" title="current number of votes">0</div><span id="post-34134-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The issue comes probably from WinPCAP, as already in <a href="http://ask.wireshark.org/search/?csrfmiddlewaretoken=1df6d9fd5255b3447685fdbe85e8ed10&amp;q=windows+8.1&amp;Submit=Search&amp;t=question">numerous posts</a>. Unfortunately there does not seem to be much activity on WinPCAP development side to fix this issue...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jun '14, 10:23</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-34134" class="comments-container"></div><div id="comment-tools-34134" class="comment-tools"></div><div class="clear"></div><div id="comment-34134-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

