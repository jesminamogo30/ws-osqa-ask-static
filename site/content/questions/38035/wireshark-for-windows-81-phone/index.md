+++
type = "question"
title = "Wireshark for Windows 8.1 Phone????"
description = '''Hey Does anyone know of a program(like wireshark) that i can install on a windows 8.1 PHONE??? Or is there a version for windows phones? I have wireshark on my pc en i can see the traffic from my router to the phone but not tne other way around so i want to see it from my phone. thank you Joeri'''
date = "2014-11-21T01:04:00Z"
lastmod = "2014-11-21T01:04:00Z"
weight = 38035
keywords = [ "phone" ]
aliases = [ "/questions/38035" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark for Windows 8.1 Phone????](/questions/38035/wireshark-for-windows-81-phone)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38035-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38035-score" class="post-score" title="current number of votes">0</div><span id="post-38035-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey</p><p>Does anyone know of a program(like wireshark) that i can install on a windows 8.1 PHONE??? Or is there a version for windows phones? I have wireshark on my pc en i can see the traffic from my router to the phone but not tne other way around so i want to see it from my phone.</p><p>thank you Joeri</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-phone" rel="tag" title="see questions tagged &#39;phone&#39;">phone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '14, 01:04</strong></p><img src="https://secure.gravatar.com/avatar/e2c2b3d5d523d33b393d920398cd230e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JoeriK&#39;s gravatar image" /><p><span>JoeriK</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JoeriK has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Nov '14, 01:23</strong> </span></p></div></div><div id="comments-container-38035" class="comments-container"></div><div id="comment-tools-38035" class="comment-tools"></div><div class="clear"></div><div id="comment-38035-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

