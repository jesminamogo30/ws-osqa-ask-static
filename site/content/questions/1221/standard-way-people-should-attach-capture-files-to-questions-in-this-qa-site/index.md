+++
type = "question"
title = "Standard way people should &quot;attach&quot; capture files to questions in this Q&amp;A site?"
description = '''How does this forum suggest people at least refer to sample capture files they might have. From what it seems the forum software doesn&#x27;t allow uploading such attachments. What are some good alternatives? While many of us have our own web servers to make this relatively easy, this doesn&#x27;t work for th...'''
date = "2010-12-02T23:26:00Z"
lastmod = "2017-07-11T12:41:00Z"
weight = 1221
keywords = [ "attachment" ]
aliases = [ "/questions/1221" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Standard way people should "attach" capture files to questions in this Q&A site?](/questions/1221/standard-way-people-should-attach-capture-files-to-questions-in-this-qa-site)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1221-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1221-score" class="post-score" title="current number of votes">1</div><span id="post-1221-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How does this forum suggest people at least refer to sample capture files they might have. From what it seems the forum software doesn't allow uploading such attachments. What are some good alternatives? While many of us have our own web servers to make this relatively easy, this doesn't work for the average Joe.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-attachment" rel="tag" title="see questions tagged &#39;attachment&#39;">attachment</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Dec '10, 23:26</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Jul '17, 13:08</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-1221" class="comments-container"><span id="1231"></span><div id="comment-1231" class="comment"><div id="post-1231-score" class="comment-score"></div><div class="comment-text"><p>In the past, I've used box.net when I needed to share files. It doesn't cost any money unless the files are large (10MB??) and the folks retrieving the file don't have to sign up.</p></div><div id="comment-1231-info" class="comment-info"><span class="comment-age">(03 Dec '10, 11:11)</span> <span class="comment-user userinfo">hansangb</span></div></div></div><div id="comment-tools-1221" class="comment-tools"></div><div class="clear"></div><div id="comment-1221-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="1236"></span>

<div id="answer-container-1236" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1236-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1236-score" class="post-score" title="current number of votes">1</div><span id="post-1236-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I just thought about Google Docs, and that seems to work well. (Sharing a SNMP capture here - <a href="https://docs.google.com/leaf?id=0Bwx8XaFad3_MYTRkMTk4YWMtZTgyMS00NzA1LWJmMTEtMGI4MWExOThhMDMw&amp;hl=en_GB&amp;authkey=CLqWktYG">snmp.pcap</a> )</p><p>Either way, just as with the mailing list, submitters need to make sure sensitive information about their network isn't made public.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Dec '10, 15:39</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div></div><div id="comments-container-1236" class="comments-container"><span id="5464"></span><div id="comment-5464" class="comment"><div id="post-5464-score" class="comment-score"></div><div class="comment-text"><p>"Either way, just as with the mailing list, submitters need to make sure sensitive information about their network isn't made public."</p><p>Very hard to do, yes?</p><p>I do want somebody to help reading traces, sensitive information, by this time will not show up in a trace.</p></div><div id="comment-5464-info" class="comment-info"><span class="comment-age">(03 Aug '11, 17:41)</span> <span class="comment-user userinfo">LaoziSailor</span></div></div><span id="5467"></span><div id="comment-5467" class="comment"><div id="post-5467-score" class="comment-score"></div><div class="comment-text"><p>If you have sensitive traces, and you need help interpreting them, then either you'll have to run the trace through something that will anonymize sensitive information (network addresses can be anonymized; other information may be difficult or impossible to anonymize) or you'll have to somehow find somebody you can trust to look at the capture.</p></div><div id="comment-5467-info" class="comment-info"><span class="comment-age">(03 Aug '11, 18:00)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-1236" class="comment-tools"></div><div class="clear"></div><div id="comment-1236-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="62678"></span>

<div id="answer-container-62678" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62678-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62678-score" class="post-score" title="current number of votes">0</div><span id="post-62678-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As a possible alternative, you can rename the capture file as a <code>.jpg</code>, and then upload the file as if it were an image, as I've done below. (Remember to change the default image name from <em>alt text</em> to the name of the file so others will know its name when they download it.)</p><p><img src="https://osqa-ask.wireshark.org/upfiles/ethershark-pcapng.jpg" alt="ethershark.pcapng" /></p><p>This <em>should</em> work for small capture files up to 10MB, and the <em>"image"</em> above is an example to test if this actually does work. <strong>EDIT</strong>: It does work, but you need to right-click and choose <em>Copy</em> instead of <em>Save As</em> and then paste it locally (using IE), or right-click and choose <em>View Image, File -&gt; Save Page As</em> (using Firefox).</p><p>If you'd prefer an actual image to be displayed, it's also possible to combine an actual image (perhaps a screenshot of your capture file) with a compressed capture file into a merged file that still appears to be the original image file. There are several youtube videos that illustrate how to do this, such as:</p><ul><li><a href="https://www.youtube.com/watch?v=q6AQL55zMR4">Windows - Howto Hide Files inside JPGs -tweak hack xp vista</a> (4:25)</li><li><a href="https://www.youtube.com/watch?v=sOq7iZ3_u6Q">Hide File Inside Image - Terminal - Ubuntu 9.10</a> (2:11)</li></ul><p>The basic steps are:</p><ol><li>Compress your capture file</li><li>Open command prompt</li><li>Merge image file with capture file:<br />
a. Windows: <code>COPY /B image.jpg + file_pcap.zip image_file_pcap.jpg</code><br />
b. *nix: <code>cat image.jpg file_pcap.zip &gt; image_file_pcap.jpg</code></li><li>Upload merged image file</li></ol><p>I've also attached an image created in this manner, which appears below as a screenshot. To extract the capture file from it (instructions provided using <a href="http://www.7-zip.org/">7-zip</a>):</p><p><img src="https://osqa-ask.wireshark.org/upfiles/ethershark_jpg_pcapng.jpg" alt="ethershark_jpg_pcapng.jpg" /></p><ul><li>Download it by right-clicking on the image and choosing "Save Image As"</li><li>Open it with the 7-Zip File Manager</li><li>Click on Extract</li><li>Choose folder and click OK</li><li>Open the extracted capture file in Wireshark</li></ul><p>The advantage of using this method is that you don't need to create an account elsewhere and all capture files are archived locally. The disadvantage is that the file size is limited and there's a bit of extra work involved with uploading/downloading the capture file(s).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jul '17, 12:41</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span> </br></br></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jan '19, 10:53</strong> </span></p></div></div><div id="comments-container-62678" class="comments-container"></div><div id="comment-tools-62678" class="comment-tools"></div><div class="clear"></div><div id="comment-62678-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

