+++
type = "question"
title = "UDP traffic"
description = '''I see UDP traffic over port 20819 from various IP addresses. Does anyone know where does it come from?'''
date = "2011-11-10T03:14:00Z"
lastmod = "2011-11-10T04:11:00Z"
weight = 7364
keywords = [ "unknown", "udp" ]
aliases = [ "/questions/7364" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [UDP traffic](/questions/7364/udp-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7364-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7364-score" class="post-score" title="current number of votes">0</div><span id="post-7364-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I see UDP traffic over port 20819 from various IP addresses. Does anyone know where does it come from?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-unknown" rel="tag" title="see questions tagged &#39;unknown&#39;">unknown</span> <span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '11, 03:14</strong></p><img src="https://secure.gravatar.com/avatar/7820f7b9638e63d42e5c6fb4de7262d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="brklp&#39;s gravatar image" /><p><span>brklp</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="brklp has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Feb '12, 21:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-7364" class="comments-container"><span id="7366"></span><div id="comment-7366" class="comment"><div id="post-7366-score" class="comment-score"></div><div class="comment-text"><p>Got Skype?</p></div><div id="comment-7366-info" class="comment-info"><span class="comment-age">(10 Nov '11, 03:24)</span> <span class="comment-user userinfo">Landi</span></div></div><span id="7367"></span><div id="comment-7367" class="comment"><div id="post-7367-score" class="comment-score"></div><div class="comment-text"><p>No skype. This is equipment for testing purposes only. No such programmes should be installed.</p></div><div id="comment-7367-info" class="comment-info"><span class="comment-age">(10 Nov '11, 03:26)</span> <span class="comment-user userinfo">brklp</span></div></div></div><div id="comment-tools-7364" class="comment-tools"></div><div class="clear"></div><div id="comment-7364-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7368"></span>

<div id="answer-container-7368" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7368-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7368-score" class="post-score" title="current number of votes">1</div><span id="post-7368-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>then go for netstat -anob (win) or netstat -anplut (*nix) and look for the UDP peers</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Nov '11, 04:11</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div></div><div id="comments-container-7368" class="comments-container"></div><div id="comment-tools-7368" class="comment-tools"></div><div class="clear"></div><div id="comment-7368-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

