+++
type = "question"
title = "v 2.2.0 ethers"
description = '''hi, used to put &quot;ethers&quot; file in appdata, roaming, wireshark.  this does not appear to work any more? at least not for me. windows 10 64 bit nov 26 update: does not work in v2.4.2 either.'''
date = "2016-09-16T13:12:00Z"
lastmod = "2016-09-16T13:12:00Z"
weight = 55606
keywords = [ "ethers", "wireshark" ]
aliases = [ "/questions/55606" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [v 2.2.0 ethers](/questions/55606/v-220-ethers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55606-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55606-score" class="post-score" title="current number of votes">0</div><span id="post-55606-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, used to put "ethers" file in appdata, roaming, wireshark. this does not appear to work any more? at least not for me. windows 10 64 bit</p><p>nov 26 update: does not work in v2.4.2 either.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ethers" rel="tag" title="see questions tagged &#39;ethers&#39;">ethers</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Sep '16, 13:12</strong></p><img src="https://secure.gravatar.com/avatar/5b1802a3dde015a758fb13baafb1605f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="proj964&#39;s gravatar image" /><p><span>proj964</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="proj964 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Nov '17, 06:32</strong> </span></p></div></div><div id="comments-container-55606" class="comments-container"></div><div id="comment-tools-55606" class="comment-tools"></div><div class="clear"></div><div id="comment-55606-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

