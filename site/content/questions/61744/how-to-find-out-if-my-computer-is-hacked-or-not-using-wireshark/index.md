+++
type = "question"
title = "How to find out if my computer is hacked or not using Wireshark?"
description = '''Today is the first time I&#x27;m using Wireshark. I don&#x27;t really know how to interpret all the information there. I mainly want to find out if my computer is hacked or not using Wireshark. What kind of information should I be looking for in Wireshark to find out about possible hacking on my computer?'''
date = "2017-06-02T01:47:00Z"
lastmod = "2017-06-02T02:01:00Z"
weight = 61744
keywords = [ "hacking", "security", "hack" ]
aliases = [ "/questions/61744" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to find out if my computer is hacked or not using Wireshark?](/questions/61744/how-to-find-out-if-my-computer-is-hacked-or-not-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61744-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61744-score" class="post-score" title="current number of votes">0</div><span id="post-61744-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Today is the first time I'm using Wireshark. I don't really know how to interpret all the information there. I mainly want to find out if my computer is hacked or not using Wireshark. What kind of information should I be looking for in Wireshark to find out about possible hacking on my computer?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hacking" rel="tag" title="see questions tagged &#39;hacking&#39;">hacking</span> <span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-hack" rel="tag" title="see questions tagged &#39;hack&#39;">hack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '17, 01:47</strong></p><img src="https://secure.gravatar.com/avatar/c65f5f9dde12f21e334b78a490ef0dfe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kekehuang&#39;s gravatar image" /><p><span>kekehuang</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kekehuang has no accepted answers">0%</span></p></div></div><div id="comments-container-61744" class="comments-container"></div><div id="comment-tools-61744" class="comment-tools"></div><div class="clear"></div><div id="comment-61744-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61745"></span>

<div id="answer-container-61745" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61745-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61745-score" class="post-score" title="current number of votes">1</div><span id="post-61745-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>IMHO, Wireshark isn't really the right tool for this task. Wireshark is a packet analyzer, you need some security focused tools.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jun '17, 02:01</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-61745" class="comments-container"></div><div id="comment-tools-61745" class="comment-tools"></div><div class="clear"></div><div id="comment-61745-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

