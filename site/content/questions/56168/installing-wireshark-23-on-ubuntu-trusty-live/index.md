+++
type = "question"
title = "installing wireshark 2.3 on ubuntu trusty live"
description = '''I would like to know if any of you could get it done to install wireshark 2.3 open source through PPA or some other way, based on linux systems. I can isntall without problems 2.2 version, but with 2.3 im having the problem with dependencies (held packages).'''
date = "2016-10-05T08:43:00Z"
lastmod = "2016-10-05T08:43:00Z"
weight = 56168
keywords = [ "wireshark2.3" ]
aliases = [ "/questions/56168" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [installing wireshark 2.3 on ubuntu trusty live](/questions/56168/installing-wireshark-23-on-ubuntu-trusty-live)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56168-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56168-score" class="post-score" title="current number of votes">0</div><span id="post-56168-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know if any of you could get it done to install wireshark 2.3 open source through PPA or some other way, based on linux systems. I can isntall without problems 2.2 version, but with 2.3 im having the problem with dependencies (held packages).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark2.3" rel="tag" title="see questions tagged &#39;wireshark2.3&#39;">wireshark2.3</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Oct '16, 08:43</strong></p><img src="https://secure.gravatar.com/avatar/1991469f42fc5738af10b0adb8a4ac56?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alex%20Johnson%208&#39;s gravatar image" /><p><span>Alex Johnson 8</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alex Johnson 8 has no accepted answers">0%</span></p></div></div><div id="comments-container-56168" class="comments-container"></div><div id="comment-tools-56168" class="comment-tools"></div><div class="clear"></div><div id="comment-56168-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

