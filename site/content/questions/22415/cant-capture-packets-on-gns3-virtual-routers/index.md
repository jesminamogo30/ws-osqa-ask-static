+++
type = "question"
title = "Can&#x27;t capture packets on GNS3 virtual routers"
description = '''I&#x27;m using debian sid and I installed gns3 and wireshark. I have a router connected to 2 Virtual PCs (using Virtual PC Simulator) and I want to capture the packets, but when I start capturing with wireshark I get the following error &quot;End of file on pipe magic during open&quot;. '''
date = "2013-06-27T08:51:00Z"
lastmod = "2013-06-27T08:51:00Z"
weight = 22415
keywords = [ "capture", "gns3", "debian", "linux" ]
aliases = [ "/questions/22415" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't capture packets on GNS3 virtual routers](/questions/22415/cant-capture-packets-on-gns3-virtual-routers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22415-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22415-score" class="post-score" title="current number of votes">0</div><span id="post-22415-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using debian sid and I installed gns3 and wireshark. I have a router connected to 2 Virtual PCs (using Virtual PC Simulator) and I want to capture the packets, but when I start capturing with wireshark I get the following error "End of file on pipe magic during open".</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-gns3" rel="tag" title="see questions tagged &#39;gns3&#39;">gns3</span> <span class="post-tag tag-link-debian" rel="tag" title="see questions tagged &#39;debian&#39;">debian</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jun '13, 08:51</strong></p><img src="https://secure.gravatar.com/avatar/52095964bc7d27aa8ff4d590f24475ef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Magnus&#39;s gravatar image" /><p><span>Magnus</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Magnus has no accepted answers">0%</span></p></div></div><div id="comments-container-22415" class="comments-container"></div><div id="comment-tools-22415" class="comment-tools"></div><div class="clear"></div><div id="comment-22415-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

