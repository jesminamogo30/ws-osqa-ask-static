+++
type = "question"
title = "Get email content (text) if using a program"
description = '''Hello, sorry if it has been answered before, I didn&#x27;t find it. I found several tutorials on how to get the text of an email using Wireshark, but only if it is sent via browser/webmail. Can I also do that when it is sent via email program (outlook, thunderbird, ...)? If so, how?'''
date = "2015-09-01T08:47:00Z"
lastmod = "2015-09-03T09:39:00Z"
weight = 45567
keywords = [ "mail", "outlook", "client", "thunderbird", "email" ]
aliases = [ "/questions/45567" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Get email content (text) if using a program](/questions/45567/get-email-content-text-if-using-a-program)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45567-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45567-score" class="post-score" title="current number of votes">0</div><span id="post-45567-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, sorry if it has been answered before, I didn't find it.</p><p>I found several tutorials on how to get the text of an email using Wireshark, but only if it is sent via browser/webmail. Can I also do that when it is sent via email program (outlook, thunderbird, ...)? If so, how?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mail" rel="tag" title="see questions tagged &#39;mail&#39;">mail</span> <span class="post-tag tag-link-outlook" rel="tag" title="see questions tagged &#39;outlook&#39;">outlook</span> <span class="post-tag tag-link-client" rel="tag" title="see questions tagged &#39;client&#39;">client</span> <span class="post-tag tag-link-thunderbird" rel="tag" title="see questions tagged &#39;thunderbird&#39;">thunderbird</span> <span class="post-tag tag-link-email" rel="tag" title="see questions tagged &#39;email&#39;">email</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Sep '15, 08:47</strong></p><img src="https://secure.gravatar.com/avatar/2e41c901bb146c3eb10abc6542236c0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Irolan&#39;s gravatar image" /><p><span>Irolan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Irolan has no accepted answers">0%</span></p></div></div><div id="comments-container-45567" class="comments-container"></div><div id="comment-tools-45567" class="comment-tools"></div><div class="clear"></div><div id="comment-45567-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45619"></span>

<div id="answer-container-45619" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45619-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45619-score" class="post-score" title="current number of votes">0</div><span id="post-45619-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Outlook / Office365 on my work system uses TLS, so no. For Thunderbird, only if they are using an unencrypted connection - watch the IMAP/POP/SMTP ports and see what you see.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Sep '15, 08:57</strong></p><img src="https://secure.gravatar.com/avatar/b3a36a856efb60f32f8d059c184a4102?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lordbah&#39;s gravatar image" /><p><span>lordbah</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lordbah has no accepted answers">0%</span></p></div></div><div id="comments-container-45619" class="comments-container"><span id="45620"></span><div id="comment-45620" class="comment"><div id="post-45620-score" class="comment-score"></div><div class="comment-text"><p>Ok, gotcha. Thanks.</p></div><div id="comment-45620-info" class="comment-info"><span class="comment-age">(03 Sep '15, 09:39)</span> <span class="comment-user userinfo">Irolan</span></div></div></div><div id="comment-tools-45619" class="comment-tools"></div><div class="clear"></div><div id="comment-45619-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

