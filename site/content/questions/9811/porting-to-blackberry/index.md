+++
type = "question"
title = "Porting to Blackberry"
description = '''Any plans to port wireshark to blackberry?'''
date = "2012-03-28T04:11:00Z"
lastmod = "2012-03-28T08:01:00Z"
weight = 9811
keywords = [ "blackberry" ]
aliases = [ "/questions/9811" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Porting to Blackberry](/questions/9811/porting-to-blackberry)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9811-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9811-score" class="post-score" title="current number of votes">0</div><span id="post-9811-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Any plans to port wireshark to blackberry?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-blackberry" rel="tag" title="see questions tagged &#39;blackberry&#39;">blackberry</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Mar '12, 04:11</strong></p><img src="https://secure.gravatar.com/avatar/b61f6fc420c749a3039ea271e7f9881e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="palindrome&#39;s gravatar image" /><p><span>palindrome</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="palindrome has no accepted answers">0%</span></p></div></div><div id="comments-container-9811" class="comments-container"></div><div id="comment-tools-9811" class="comment-tools"></div><div class="clear"></div><div id="comment-9811-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9820"></span>

<div id="answer-container-9820" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9820-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9820-score" class="post-score" title="current number of votes">1</div><span id="post-9820-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>None that I know of. I think the traditional Blackberry OS supports only Java applications, in which case it's not going to happen any time soon; I don't know what the Shiny New QNX-Based OS supports, but even if RIM are supporting C apps on it, that's probably not likely to happen soon either.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Mar '12, 08:01</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-9820" class="comments-container"></div><div id="comment-tools-9820" class="comment-tools"></div><div class="clear"></div><div id="comment-9820-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

