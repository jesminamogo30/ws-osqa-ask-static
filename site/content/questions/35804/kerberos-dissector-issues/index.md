+++
type = "question"
title = "Kerberos Dissector Issues"
description = '''I&#x27;m trying to debug a Kerberos issue with PA_S4U_X509_USER padata, i.e. attempting to do S4U with a certificate. In the TGS-REQ I can see the correct padata-type (130) but this is marked as unknown and the dissector doesn&#x27;t unpack the data. Is this a BUG or do I need to configure something? This is ...'''
date = "2014-08-27T06:19:00Z"
lastmod = "2014-08-27T06:19:00Z"
weight = 35804
keywords = [ "kerberos" ]
aliases = [ "/questions/35804" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Kerberos Dissector Issues](/questions/35804/kerberos-dissector-issues)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35804-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35804-score" class="post-score" title="current number of votes">0</div><span id="post-35804-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to debug a Kerberos issue with PA_S4U_X509_USER padata, i.e. attempting to do S4U with a certificate. In the TGS-REQ I can see the correct padata-type (130) but this is marked as unknown and the dissector doesn't unpack the data. Is this a BUG or do I need to configure something? This is with Version 1.12.0 (v1.12.0-0-g4fab41a from master-1.12)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-kerberos" rel="tag" title="see questions tagged &#39;kerberos&#39;">kerberos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Aug '14, 06:19</strong></p><img src="https://secure.gravatar.com/avatar/26573863f4f70d3ba547b3aa71308de4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="John%20Ashman&#39;s gravatar image" /><p><span>John Ashman</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="John Ashman has no accepted answers">0%</span></p></div></div><div id="comments-container-35804" class="comments-container"></div><div id="comment-tools-35804" class="comment-tools"></div><div class="clear"></div><div id="comment-35804-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

