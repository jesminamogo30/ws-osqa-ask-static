+++
type = "question"
title = "How to get Statistics-&gt;Conversation List--&gt;TCP information in a text file?"
description = '''I want to feed the nice table that Statistics-&amp;gt;Conversation List--&amp;gt;TCP makes to a script. How to dump that table in a file?'''
date = "2012-07-28T16:14:00Z"
lastmod = "2016-07-29T19:01:00Z"
weight = 13096
keywords = [ "statistics", "conversationlist", "tcp" ]
aliases = [ "/questions/13096" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How to get Statistics-&gt;Conversation List--&gt;TCP information in a text file?](/questions/13096/how-to-get-statistics-conversation-list-tcp-information-in-a-text-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13096-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13096-score" class="post-score" title="current number of votes">1</div><span id="post-13096-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to feed the nice table that Statistics-&gt;Conversation List--&gt;TCP makes to a script. How to dump that table in a file?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-conversationlist" rel="tag" title="see questions tagged &#39;conversationlist&#39;">conversationlist</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jul '12, 16:14</strong></p><img src="https://secure.gravatar.com/avatar/f17329c0625ce5653b9d44e23e522c03?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="oscarmeyer&#39;s gravatar image" /><p><span>oscarmeyer</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="oscarmeyer has no accepted answers">0%</span></p></div></div><div id="comments-container-13096" class="comments-container"></div><div id="comment-tools-13096" class="comment-tools"></div><div class="clear"></div><div id="comment-13096-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="13097"></span>

<div id="answer-container-13097" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13097-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13097-score" class="post-score" title="current number of votes">1</div><span id="post-13097-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is "Copy" button next at the bottom left in the same window. Click on Copy and paste it to a text file, then save it as CSV file. Open it with a program like Excel or Calc. Now you can do whatever you want from there.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jul '12, 19:10</strong></p><img src="https://secure.gravatar.com/avatar/327a1e6f77d9d54ab928d1b6327da4b8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ntwkeng&#39;s gravatar image" /><p><span>ntwkeng</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ntwkeng has no accepted answers">0%</span></p></div></div><div id="comments-container-13097" class="comments-container"><span id="13100"></span><div id="comment-13100" class="comment"><div id="post-13100-score" class="comment-score"></div><div class="comment-text"><p>Thanks. I meant how to generate that table from the tshark command line and dump it to a file.</p></div><div id="comment-13100-info" class="comment-info"><span class="comment-age">(29 Jul '12, 11:50)</span> <span class="comment-user userinfo">oscarmeyer</span></div></div></div><div id="comment-tools-13097" class="comment-tools"></div><div class="clear"></div><div id="comment-13097-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="13098"></span>

<div id="answer-container-13098" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13098-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13098-score" class="post-score" title="current number of votes">1</div><span id="post-13098-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can use the statistics functions of tshark</p><blockquote><p><code>tshark -r input.cap.pcapng -q -z conv,ip &gt; output.txt</code><br />
<code>tshark -r input.cap.pcapng -q -z conv,tcp &gt; output.txt</code><br />
<code>tshark -r input.cap.pcapng -q -z conv,udp &gt; output.txt</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jul '12, 01:13</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-13098" class="comments-container"><span id="13101"></span><div id="comment-13101" class="comment"><div id="post-13101-score" class="comment-score"></div><div class="comment-text"><p>Saw that, but that only gives a few of the columns. I need the others too, like duration and relative start time.</p></div><div id="comment-13101-info" class="comment-info"><span class="comment-age">(29 Jul '12, 11:51)</span> <span class="comment-user userinfo">oscarmeyer</span></div></div><span id="13102"></span><div id="comment-13102" class="comment"><div id="post-13102-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I need the others too, like <strong>duration</strong> and <strong>relative start time</strong>.</p></blockquote><p>That's in the output. Last two cloumns for <strong><code>-z conv,tcp</code></strong>. Actually it's the same output as in the GUI, with a slightly different formatting. Only bps (bits/s) is missing.</p></div><div id="comment-13102-info" class="comment-info"><span class="comment-age">(29 Jul '12, 11:59)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="13103"></span><div id="comment-13103" class="comment"><div id="post-13103-score" class="comment-score"></div><div class="comment-text"><p>Not for me. What version are you using?</p></div><div id="comment-13103-info" class="comment-info"><span class="comment-age">(29 Jul '12, 12:49)</span> <span class="comment-user userinfo">oscarmeyer</span></div></div><span id="13104"></span><div id="comment-13104" class="comment"><div id="post-13104-score" class="comment-score"></div><div class="comment-text"><p>TShark 1.8.1 (SVN Rev 43946 from /trunk-1.8)</p></div><div id="comment-13104-info" class="comment-info"><span class="comment-age">(29 Jul '12, 14:06)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="13105"></span><div id="comment-13105" class="comment"><div id="post-13105-score" class="comment-score"></div><div class="comment-text"><p>I was one version behind. The latest has the info. Thank you!</p></div><div id="comment-13105-info" class="comment-info"><span class="comment-age">(29 Jul '12, 16:24)</span> <span class="comment-user userinfo">oscarmeyer</span></div></div><span id="54451"></span><div id="comment-54451" class="comment not_top_scorer"><div id="post-54451-score" class="comment-score"></div><div class="comment-text"><p>Somewhere between 1.8 and 1.10 they dropped the <code>-q</code> option in <code>wireshark</code> and so now you want the same arguments but the <code>tshark</code> executable</p></div><div id="comment-54451-info" class="comment-info"><span class="comment-age">(29 Jul '16, 13:45)</span> <span class="comment-user userinfo">huckle</span></div></div><span id="54454"></span><div id="comment-54454" class="comment not_top_scorer"><div id="post-54454-score" class="comment-score"></div><div class="comment-text"><p>The <code>-q</code> option is still there. See the latest online <a href="https://www.wireshark.org/docs/man-pages/tshark.html"><code>tshark man page</code></a>.</p></div><div id="comment-54454-info" class="comment-info"><span class="comment-age">(29 Jul '16, 19:01)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-13098" class="comment-tools"><span class="comments-showing"> showing 5 of 7 </span> <a href="#" class="show-all-comments-link">show 2 more comments</a></div><div class="clear"></div><div id="comment-13098-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

