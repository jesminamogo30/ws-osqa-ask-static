+++
type = "question"
title = "I would like to get hold of the owner of the ethereal.net domain, can anyone help?"
description = '''Hi There, I&#x27;ve been trying to get hold of the owner for a couple of weeks now and have been unable to find any contact information. Ive resorted to asking on the forums in case someone might be able to point me the right way. Thanks.'''
date = "2013-12-21T14:30:00Z"
lastmod = "2014-01-10T19:46:00Z"
weight = 28316
keywords = [ "owner", "contact" ]
aliases = [ "/questions/28316" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [I would like to get hold of the owner of the ethereal.net domain, can anyone help?](/questions/28316/i-would-like-to-get-hold-of-the-owner-of-the-etherealnet-domain-can-anyone-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28316-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28316-score" class="post-score" title="current number of votes">-1</div><span id="post-28316-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi There,</p><p>I've been trying to get hold of the owner for a couple of weeks now and have been unable to find any contact information.</p><p>Ive resorted to asking on the forums in case someone might be able to point me the right way.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-owner" rel="tag" title="see questions tagged &#39;owner&#39;">owner</span> <span class="post-tag tag-link-contact" rel="tag" title="see questions tagged &#39;contact&#39;">contact</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Dec '13, 14:30</strong></p><img src="https://secure.gravatar.com/avatar/b8f4ffcc5170f886b4d07bdaa25070d8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nrs23&#39;s gravatar image" /><p><span>nrs23</span><br />
<span class="score" title="10 reputation points">10</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nrs23 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Jan '14, 19:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-28316" class="comments-container"><span id="28317"></span><div id="comment-28317" class="comment"><div id="post-28317-score" class="comment-score"></div><div class="comment-text"><p>Owner of what?</p></div><div id="comment-28317-info" class="comment-info"><span class="comment-age">(21 Dec '13, 15:36)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="28319"></span><div id="comment-28319" class="comment"><div id="post-28319-score" class="comment-score">3</div><div class="comment-text"><p>Maybe the answer to everything. In that case: 42</p></div><div id="comment-28319-info" class="comment-info"><span class="comment-age">(21 Dec '13, 23:54)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="28323"></span><div id="comment-28323" class="comment"><div id="post-28323-score" class="comment-score">4</div><div class="comment-text"><p>You can get it <a href="http://www.amazon.com/gp/product/B00BZX2P9U/">here</a>.</p></div><div id="comment-28323-info" class="comment-info"><span class="comment-age">(22 Dec '13, 10:01)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="28328"></span><div id="comment-28328" class="comment"><div id="post-28328-score" class="comment-score"></div><div class="comment-text"><p>I hope you did not invest weeks to find that :-))</p></div><div id="comment-28328-info" class="comment-info"><span class="comment-age">(22 Dec '13, 13:38)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="28780"></span><div id="comment-28780" class="comment"><div id="post-28780-score" class="comment-score"></div><div class="comment-text"><p>The owner of the wireshark</p></div><div id="comment-28780-info" class="comment-info"><span class="comment-age">(10 Jan '14, 13:07)</span> <span class="comment-user userinfo">nrs23</span></div></div><span id="28781"></span><div id="comment-28781" class="comment not_top_scorer"><div id="post-28781-score" class="comment-score"></div><div class="comment-text"><p>The code, the name, the trademark or the domain?</p></div><div id="comment-28781-info" class="comment-info"><span class="comment-age">(10 Jan '14, 13:15)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="28783"></span><div id="comment-28783" class="comment not_top_scorer"><div id="post-28783-score" class="comment-score"></div><div class="comment-text"><p>the person who owns the domain of ethereal.net</p></div><div id="comment-28783-info" class="comment-info"><span class="comment-age">(10 Jan '14, 13:17)</span> <span class="comment-user userinfo">nrs23</span></div></div><span id="28790"></span><div id="comment-28790" class="comment not_top_scorer"><div id="post-28790-score" class="comment-score"></div><div class="comment-text"><p>Note that ethereal.net has nothing to do with Wireshark (other than having a link to the Wireshark Web site, for the benefit of people who still think the program is called "Ethereal").</p></div><div id="comment-28790-info" class="comment-info"><span class="comment-age">(10 Jan '14, 19:46)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-28316" class="comment-tools"><span class="comments-showing"> showing 5 of 8 </span> <a href="#" class="show-all-comments-link">show 3 more comments</a></div><div class="clear"></div><div id="comment-28316-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28785"></span>

<div id="answer-container-28785" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28785-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28785-score" class="post-score" title="current number of votes">1</div><span id="post-28785-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't know who owns ethereal.net, but you can contact that person via: <span class="__cf_email__" data-cfemail="cc899884899e898d80e28289988ca8a3a1ada5a2bfaeb5bcbea3b4b5e2afa3a1">[email protected]</span></p><p>See: <a href="http://who.godaddy.com/whoischeck.aspx?domain=ETHEREAL.NET">http://who.godaddy.com/whoischeck.aspx?domain=ETHEREAL.NET</a></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jan '14, 14:29</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-28785" class="comments-container"></div><div id="comment-tools-28785" class="comment-tools"></div><div class="clear"></div><div id="comment-28785-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

