+++
type = "question"
title = "Capture cell phone text messages with Wireshark?"
description = '''Is it possible to capture cell phone text messages with Wireshark if they are connected to the wire less?'''
date = "2012-11-18T08:13:00Z"
lastmod = "2012-11-18T09:11:00Z"
weight = 16010
keywords = [ "texting", "sms", "capture" ]
aliases = [ "/questions/16010" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture cell phone text messages with Wireshark?](/questions/16010/capture-cell-phone-text-messages-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16010-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16010-score" class="post-score" title="current number of votes">0</div><span id="post-16010-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to capture cell phone text messages with Wireshark if they are connected to the wire less?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-texting" rel="tag" title="see questions tagged &#39;texting&#39;">texting</span> <span class="post-tag tag-link-sms" rel="tag" title="see questions tagged &#39;sms&#39;">sms</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '12, 08:13</strong></p><img src="https://secure.gravatar.com/avatar/c6d7d2a1634d4c6a9323d1d0f97a1f17?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tranqmorne&#39;s gravatar image" /><p><span>tranqmorne</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tranqmorne has no accepted answers">0%</span></p></div></div><div id="comments-container-16010" class="comments-container"></div><div id="comment-tools-16010" class="comment-tools"></div><div class="clear"></div><div id="comment-16010-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16011"></span>

<div id="answer-container-16011" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16011-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16011-score" class="post-score" title="current number of votes">0</div><span id="post-16011-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>not if they're cellular messages (SMS), because they are transfered via GSM/UMTs etc. You can probably capture stuff like WhatsApp messages, but they should be encrypted in most cases.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Nov '12, 09:11</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-16011" class="comments-container"></div><div id="comment-tools-16011" class="comment-tools"></div><div class="clear"></div><div id="comment-16011-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

