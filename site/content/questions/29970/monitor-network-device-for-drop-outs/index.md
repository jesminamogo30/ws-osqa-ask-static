+++
type = "question"
title = "Monitor Network Device for Drop Outs"
description = '''I would like to set up a watch on a network device to see if it drops of the network. We just installed a new Fiber Converter and I want to make sure the connection is stable.  Thanks Lee'''
date = "2014-02-18T08:22:00Z"
lastmod = "2014-07-12T03:51:00Z"
weight = 29970
keywords = [ "monitor" ]
aliases = [ "/questions/29970" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Monitor Network Device for Drop Outs](/questions/29970/monitor-network-device-for-drop-outs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29970-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29970-score" class="post-score" title="current number of votes">0</div><span id="post-29970-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to set up a watch on a network device to see if it drops of the network. We just installed a new Fiber Converter and I want to make sure the connection is stable.<br />
</p><p>Thanks</p><p>Lee</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '14, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/75677219f6b60b927dc4d359325c896f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LeeV&#39;s gravatar image" /><p><span>LeeV</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LeeV has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-29970" class="comments-container"></div><div id="comment-tools-29970" class="comment-tools"></div><div class="clear"></div><div id="comment-29970-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="29973"></span>

<div id="answer-container-29973" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29973-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29973-score" class="post-score" title="current number of votes">0</div><span id="post-29973-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That is not a question. And it is neither a task for Wireshark. Take a look at network monitoring systems like <a href="http://www.nagios.org/">Nagios</a> or <a href="https://www.icinga.org/">Icinga</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Feb '14, 09:32</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-29973" class="comments-container"><span id="29974"></span><div id="comment-29974" class="comment"><div id="post-29974-score" class="comment-score"></div><div class="comment-text"><p>Thanks, I will make sure I use a question format next time as well</p></div><div id="comment-29974-info" class="comment-info"><span class="comment-age">(18 Feb '14, 10:04)</span> <span class="comment-user userinfo">LeeV</span></div></div><span id="29975"></span><div id="comment-29975" class="comment"><div id="post-29975-score" class="comment-score"></div><div class="comment-text"><p>Yep, Icinga has those really neat <a href="https://www.icinga.org/about/icinga-reporting/icinga-reporting-workflow/">Jasper Reports</a> :-)</p></div><div id="comment-29975-info" class="comment-info"><span class="comment-age">(18 Feb '14, 11:05)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="29978"></span><div id="comment-29978" class="comment"><div id="post-29978-score" class="comment-score"></div><div class="comment-text"><p>Sorry, I didn't mean to sound harsh, I just forgot the smileys :-)</p></div><div id="comment-29978-info" class="comment-info"><span class="comment-age">(18 Feb '14, 11:41)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-29973" class="comment-tools"></div><div class="clear"></div><div id="comment-29973-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="34613"></span>

<div id="answer-container-34613" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34613-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34613-score" class="post-score" title="current number of votes">0</div><span id="post-34613-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you tried <a href="http://mindarraysystems.com/network-monitoring-software-tools.php">MindArray IPM</a>?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jul '14, 03:51</strong></p><img src="https://secure.gravatar.com/avatar/6cbdf6f391a116c6d85d9e009cfe5c74?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joeshestak&#39;s gravatar image" /><p><span>joeshestak</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joeshestak has no accepted answers">0%</span></p></div></div><div id="comments-container-34613" class="comments-container"></div><div id="comment-tools-34613" class="comment-tools"></div><div class="clear"></div><div id="comment-34613-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

