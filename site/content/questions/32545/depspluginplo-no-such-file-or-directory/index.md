+++
type = "question"
title = ".deps/plugin.Plo: No such file or directory"
description = '''I downloaded the source code for wireshark-1.8.13.I built that code. Then I started custom plugin &quot;foo&quot; as per the given steps in wireshark dev guide i wrote the code for plugin foo http://www.wireshark.org/docs/wsdg_html_chunked/ChDissectAdd.html When started the code compilation with my plugin cod...'''
date = "2014-05-06T04:09:00Z"
lastmod = "2015-02-24T07:28:00Z"
weight = 32545
keywords = [ "wireshark" ]
aliases = [ "/questions/32545" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [.deps/plugin.Plo: No such file or directory](/questions/32545/depspluginplo-no-such-file-or-directory)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32545-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32545-score" class="post-score" title="current number of votes">0</div><span id="post-32545-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded the source code for wireshark-1.8.13.I built that code. Then I started custom plugin "foo" as per the given steps in wireshark dev guide i wrote the code for plugin foo <a href="http://www.wireshark.org/docs/wsdg_html_chunked/ChDissectAdd.html">http://www.wireshark.org/docs/wsdg_html_chunked/ChDissectAdd.html</a></p><p>When started the code compilation with my plugin code changes i face following errors:</p><pre><code>wireshark-1.8.13 #./autogen.sh
wireshark-1.8.13 # ./configure
wireshark-1.8.13 # make
..
Makefile:582: .deps/packet-foo.Plo: No such file or directory
Makefile:583: .deps/plugin.Plo: No such file or directory
make[3]: *** No rule to make target `.deps/plugin.Plo&#39;.  Stop.</code></pre>Can someone please give me pointer/help on this</div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 May '14, 04:09</strong></p><img src="https://secure.gravatar.com/avatar/12e3e74fe4d4f35dc42abca5c7302a1f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pa%20ma&#39;s gravatar image" /><p><span>pa ma</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pa ma has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Feb '15, 04:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-32545" class="comments-container"></div><div id="comment-tools-32545" class="comment-tools"></div><div class="clear"></div><div id="comment-32545-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40047"></span>

<div id="answer-container-40047" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40047-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40047-score" class="post-score" title="current number of votes">0</div><span id="post-40047-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>read WS/doc/README.plugins</p><p><a href="http://anonsvn.wireshark.org/viewvc/trunk/doc/README.plugins?revision=34921&amp;view=markup">http://anonsvn.wireshark.org/viewvc/trunk/doc/README.plugins?revision=34921&amp;view=markup</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Feb '15, 07:28</strong></p><img src="https://secure.gravatar.com/avatar/fc7cd2942568a7b2705e55b532ff85c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mojo0809&#39;s gravatar image" /><p><span>Mojo0809</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mojo0809 has no accepted answers">0%</span></p></div></div><div id="comments-container-40047" class="comments-container"></div><div id="comment-tools-40047" class="comment-tools"></div><div class="clear"></div><div id="comment-40047-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

