+++
type = "question"
title = "No Interface to capture traffic - ﻿﻿OSX 10.10 - Wireshark 1.99.5"
description = '''Hi,  I&#x27;m trying the new 1.99.5 version (Very Nice Interface!) but after the installation no interface are available to be used (I have a MacBook Air with wi-fi and FireWire Ethernet)  I&#x27;ve launched the app from sudo user  Previous version of WireShark works fine and tcpdump works fine. Any suggestio...'''
date = "2015-03-26T09:01:00Z"
lastmod = "2015-03-26T14:25:00Z"
weight = 40906
keywords = [ "osx", "yosemite", "1.99" ]
aliases = [ "/questions/40906" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No Interface to capture traffic - ﻿﻿OSX 10.10 - Wireshark 1.99.5](/questions/40906/no-interface-to-capture-traffic-osx-1010-wireshark-1995)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40906-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40906-score" class="post-score" title="current number of votes">0</div><span id="post-40906-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I'm trying the new 1.99.5 version (Very Nice Interface!) but after the installation no interface are available to be used (I have a MacBook Air with wi-fi and FireWire Ethernet) I've launched the app from sudo user Previous version of WireShark works fine and tcpdump works fine.</p><p>Any suggestion ? Thanks sdc</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-yosemite" rel="tag" title="see questions tagged &#39;yosemite&#39;">yosemite</span> <span class="post-tag tag-link-1.99" rel="tag" title="see questions tagged &#39;1.99&#39;">1.99</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '15, 09:01</strong></p><img src="https://secure.gravatar.com/avatar/0ed2937176f7f17f39222a2517959f38?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="microservo&#39;s gravatar image" /><p><span>microservo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="microservo has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Mar '15, 09:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-40906" class="comments-container"><span id="40918"></span><div id="comment-40918" class="comment"><div id="post-40918-score" class="comment-score"></div><div class="comment-text"><p>What does the command <code>ls -l /dev/bpf*</code> print?</p></div><div id="comment-40918-info" class="comment-info"><span class="comment-age">(26 Mar '15, 14:25)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-40906" class="comment-tools"></div><div class="clear"></div><div id="comment-40906-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

