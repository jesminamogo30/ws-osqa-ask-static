+++
type = "question"
title = "Decode mDNS"
description = '''Hello, I have two questions. I&#x27;m trying to write mDNS queries, to make a service discovery. That why i want to know if it is possible to decode mDNS packets that I took with wireshark? That would be very useful for me! And how do I do with DNS queries? Thanks'''
date = "2012-10-01T02:40:00Z"
lastmod = "2012-10-01T05:43:00Z"
weight = 14622
keywords = [ "query", "mdns", "request", "dns" ]
aliases = [ "/questions/14622" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Decode mDNS](/questions/14622/decode-mdns)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14622-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14622-score" class="post-score" title="current number of votes">0</div><span id="post-14622-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have two questions. I'm trying to write mDNS queries, to make a service discovery. That why i want to know if it is possible to decode mDNS packets that I took with wireshark? That would be very useful for me! And how do I do with DNS queries?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-query" rel="tag" title="see questions tagged &#39;query&#39;">query</span> <span class="post-tag tag-link-mdns" rel="tag" title="see questions tagged &#39;mdns&#39;">mdns</span> <span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span> <span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Oct '12, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/abdbbc315cda65537ef7dd1e9a8d4a54?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="matthieu526&#39;s gravatar image" /><p><span>matthieu526</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="matthieu526 has no accepted answers">0%</span></p></div></div><div id="comments-container-14622" class="comments-container"></div><div id="comment-tools-14622" class="comment-tools"></div><div class="clear"></div><div id="comment-14622-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14624"></span>

<div id="answer-container-14624" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14624-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14624-score" class="post-score" title="current number of votes">0</div><span id="post-14624-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>yes wireshark can decodes mDNS check it on cloudshark</p><p><a href="http://www.cloudshark.org/captures/79165701de02">http://www.cloudshark.org/captures/79165701de02</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Oct '12, 05:35</strong></p><img src="https://secure.gravatar.com/avatar/0cf7e05b14ad6662ecde4c327bb2c39f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Harsha&#39;s gravatar image" /><p><span>Harsha</span><br />
<span class="score" title="46 reputation points">46</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Harsha has no accepted answers">0%</span></p></div></div><div id="comments-container-14624" class="comments-container"><span id="14626"></span><div id="comment-14626" class="comment"><div id="post-14626-score" class="comment-score"></div><div class="comment-text"><p>Yes okay but I don't know how to write the queries now...</p></div><div id="comment-14626-info" class="comment-info"><span class="comment-age">(01 Oct '12, 05:43)</span> <span class="comment-user userinfo">matthieu526</span></div></div></div><div id="comment-tools-14624" class="comment-tools"></div><div class="clear"></div><div id="comment-14624-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

