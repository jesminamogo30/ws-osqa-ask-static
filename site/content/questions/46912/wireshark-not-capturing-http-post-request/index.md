+++
type = "question"
title = "wireshark not capturing http post request"
description = '''I have done the arpspoofing on my victim using  arpspoof -t 192.168.1.206 192.168.1.1 -i wlan0 And i had changed my system into router mode. when my victim opened an website on online website i got the usernames and password of http POST  requests in my wireshark. but when he opened a webpage which ...'''
date = "2015-10-25T10:54:00Z"
lastmod = "2015-10-25T15:25:00Z"
weight = 46912
keywords = [ "arpspoofing", "http" ]
aliases = [ "/questions/46912" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [wireshark not capturing http post request](/questions/46912/wireshark-not-capturing-http-post-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46912-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46912-score" class="post-score" title="current number of votes">0</div><span id="post-46912-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have done the arpspoofing on my victim using arpspoof -t 192.168.1.206 192.168.1.1 -i wlan0</p><p>And i had changed my system into router mode.</p><p>when my victim opened an website on online website i got the usernames and password of http POST requests in my wireshark.</p><p>but when he opened a webpage which is hosted in local server(LAN) when he opened that local site my wireshark is not capturing those packets why?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-arpspoofing" rel="tag" title="see questions tagged &#39;arpspoofing&#39;">arpspoofing</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '15, 10:54</strong></p><img src="https://secure.gravatar.com/avatar/e36cc00d960bddb05ae96f1bc8e4feb8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ashokkrishna&#39;s gravatar image" /><p><span>ashokkrishna</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ashokkrishna has one accepted answer">100%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Oct '15, 11:02</strong> </span></p></div></div><div id="comments-container-46912" class="comments-container"></div><div id="comment-tools-46912" class="comment-tools"></div><div class="clear"></div><div id="comment-46912-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46918"></span>

<div id="answer-container-46918" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46918-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46918-score" class="post-score" title="current number of votes">0</div><span id="post-46918-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="ashokkrishna has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You basically experienced what local means: A direct connection between client and server is possible, without going through the router.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Oct '15, 15:25</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-46918" class="comments-container"></div><div id="comment-tools-46918" class="comment-tools"></div><div class="clear"></div><div id="comment-46918-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

