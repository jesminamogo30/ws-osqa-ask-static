+++
type = "question"
title = "pcap repositories or pcap generation service?"
description = '''We are trying to find large numbers (1,000-10,000) of pcap files for both discreet individual malicious network events as well as pcaps for &quot;normal&quot; network traffic.  We have been using metasploit and wireshark but are looking to extend the breadth and depth of the library we use for testing. Are th...'''
date = "2017-03-09T06:34:00Z"
lastmod = "2017-03-09T07:00:00Z"
weight = 59960
keywords = [ "capture", "pcap", "repository" ]
aliases = [ "/questions/59960" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [pcap repositories or pcap generation service?](/questions/59960/pcap-repositories-or-pcap-generation-service)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59960-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59960-score" class="post-score" title="current number of votes">0</div><span id="post-59960-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We are trying to find large numbers (1,000-10,000) of pcap files for both discreet individual malicious network events as well as pcaps for "normal" network traffic.</p><p>We have been using metasploit and wireshark but are looking to extend the breadth and depth of the library we use for testing.</p><p>Are there any repositories that would contain that many pcaps of individual events, or companies that specialize in capturing that kind of data?</p><p>Thanks in advance,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-repository" rel="tag" title="see questions tagged &#39;repository&#39;">repository</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Mar '17, 06:34</strong></p><img src="https://secure.gravatar.com/avatar/45dd7116c4c3b8b0862773983987ac0b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dougv&#39;s gravatar image" /><p><span>dougv</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dougv has no accepted answers">0%</span></p></div></div><div id="comments-container-59960" class="comments-container"></div><div id="comment-tools-59960" class="comment-tools"></div><div class="clear"></div><div id="comment-59960-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59962"></span>

<div id="answer-container-59962" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59962-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59962-score" class="post-score" title="current number of votes">0</div><span id="post-59962-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not that many files that I know of - this sounds like you're trying to train a software with bad and normal traffic. Problem is, that network captures are almost always sensitive in nature, so nobody is just capturing tons of them and providing them to the public... so you're probably stuck with smaller sets of files on the various sites that publish them; a good starter page (which you probably already know) is <a href="http://www.netresec.com/?page=PcapFiles">http://www.netresec.com/?page=PcapFiles</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Mar '17, 06:48</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-59962" class="comments-container"><span id="59964"></span><div id="comment-59964" class="comment"><div id="post-59964-score" class="comment-score"></div><div class="comment-text"><p>Yes Jasper... that's exactly the purpose. We've pulled all the ones on sites like netresec and are now looking to broaden the sample size we are using for each "event" (normal vs. malicious). I figured if anyone was doing it in the quantities that would be useful for us, that it would be in a lab environment. Thanks for info.</p></div><div id="comment-59964-info" class="comment-info"><span class="comment-age">(09 Mar '17, 06:55)</span> <span class="comment-user userinfo">dougv</span></div></div><span id="59967"></span><div id="comment-59967" class="comment"><div id="post-59967-score" class="comment-score"></div><div class="comment-text"><p>I was going to suggest <a href="https://pcapr.net/home">pcapr</a>, but they don't allow commercial reuse.</p></div><div id="comment-59967-info" class="comment-info"><span class="comment-age">(09 Mar '17, 07:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-59962" class="comment-tools"></div><div class="clear"></div><div id="comment-59962-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

