+++
type = "question"
title = "How long does it take to resolve a bug"
description = '''Usually ,How long does it take to resolve a bug? is it possible a reported bug is not resolved?'''
date = "2017-04-18T00:15:00Z"
lastmod = "2017-04-18T02:18:00Z"
weight = 60870
keywords = [ "bugs" ]
aliases = [ "/questions/60870" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How long does it take to resolve a bug](/questions/60870/how-long-does-it-take-to-resolve-a-bug)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60870-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60870-score" class="post-score" title="current number of votes">0</div><span id="post-60870-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Usually ,How long does it take to resolve a bug? is it possible a reported bug is not resolved?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bugs" rel="tag" title="see questions tagged &#39;bugs&#39;">bugs</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '17, 00:15</strong></p><img src="https://secure.gravatar.com/avatar/28d5dc133c31193058a99892f00a0213?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ghader&#39;s gravatar image" /><p><span>ghader</span><br />
<span class="score" title="61 reputation points">61</span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ghader has no accepted answers">0%</span></p></div></div><div id="comments-container-60870" class="comments-container"></div><div id="comment-tools-60870" class="comment-tools"></div><div class="clear"></div><div id="comment-60870-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60871"></span>

<div id="answer-container-60871" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60871-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60871-score" class="post-score" title="current number of votes">2</div><span id="post-60871-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="ghader has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>When you're talking about a reported bug at <a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a>: It depends.</p><ul><li>is the bug easy to reproduce?</li><li>is a capture file attached?</li><li>is the bug a security issue?</li><li>is there someone to pick up the bug...</li></ul><p>Bugs have been resolved after some minutes and bugs have been open for years.</p><p>Are you asking for a specific bug ID?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Apr '17, 02:04</strong></p><img src="https://secure.gravatar.com/avatar/11cda2a4be5391632a5b28af1927307b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Uli&#39;s gravatar image" /><p><span>Uli</span><br />
<span class="score" title="903 reputation points">903</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Uli has 16 accepted answers">29%</span></p></div></div><div id="comments-container-60871" class="comments-container"><span id="60872"></span><div id="comment-60872" class="comment"><div id="post-60872-score" class="comment-score"></div><div class="comment-text"><p>thanks very much. yes i am asking for <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=13592">Bug ID 13592</a>.</p></div><div id="comment-60872-info" class="comment-info"><span class="comment-age">(18 Apr '17, 02:18)</span> <span class="comment-user userinfo">ghader</span></div></div></div><div id="comment-tools-60871" class="comment-tools"></div><div class="clear"></div><div id="comment-60871-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

