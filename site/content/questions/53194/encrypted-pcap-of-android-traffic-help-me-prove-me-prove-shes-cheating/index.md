+++
type = "question"
title = "ENCRYPTED PCAP of Android traffic. Help me prove me prove shes cheating.."
description = '''Please help ne prove or clear her name b4 its to late. I have a few days worth of traffic capture of my soon to be wifes android phone on my PC. I used my windows 10 pc and made a access point for her phone to connect to. Lots of traffic to google.talk and she claims to know nothing. Only problem is...'''
date = "2016-06-04T00:27:00Z"
lastmod = "2016-06-04T04:28:00Z"
weight = 53194
keywords = [ "cheat", "encryption", "android", "cheating" ]
aliases = [ "/questions/53194" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ENCRYPTED PCAP of Android traffic. Help me prove me prove shes cheating..](/questions/53194/encrypted-pcap-of-android-traffic-help-me-prove-me-prove-shes-cheating)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53194-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53194-score" class="post-score" title="current number of votes">0</div><span id="post-53194-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please help ne prove or clear her name b4 its to late. I have a few days worth of traffic capture of my soon to be wifes android phone on my PC. I used my windows 10 pc and made a access point for her phone to connect to. Lots of traffic to google.talk and she claims to know nothing. Only problem is i cant decrypt the pcap and pcapng files.. i have access to the device that the traffic originanated from. Tried to copy some of the cert files from the droid and use them on my pc with no luck.. if i can find a login that shes hidden from me after asking, thats enough evidence for me to leave. Ill pay cash aka paypal if anyone could help me. Plz email me at <span class="__cf_email__" data-cfemail="a2c8c3d1d6c7c7d0d1e2c5cfc3cbce8cc1cdcf">[email protected]</span> and i can give u my cell so we could text if thats easier. Thank you so much!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cheat" rel="tag" title="see questions tagged &#39;cheat&#39;">cheat</span> <span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span> <span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-cheating" rel="tag" title="see questions tagged &#39;cheating&#39;">cheating</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '16, 00:27</strong></p><img src="https://secure.gravatar.com/avatar/66a992ca70c292175fd43c71fce6825b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasteers&#39;s gravatar image" /><p><span>Jasteers</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasteers has no accepted answers">0%</span></p></div></div><div id="comments-container-53194" class="comments-container"></div><div id="comment-tools-53194" class="comment-tools"></div><div class="clear"></div><div id="comment-53194-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53200"></span>

<div id="answer-container-53200" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53200-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53200-score" class="post-score" title="current number of votes">0</div><span id="post-53200-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>To decrypt the traffic you either need the private keys from the server (which I am certain you won't ever get access to), or the session keys from the android device (which are usually not stored anywhere after the communication has ended). So from my point of view there is little to zero chance to decrypt anything at all.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jun '16, 04:28</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-53200" class="comments-container"></div><div id="comment-tools-53200" class="comment-tools"></div><div class="clear"></div><div id="comment-53200-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

