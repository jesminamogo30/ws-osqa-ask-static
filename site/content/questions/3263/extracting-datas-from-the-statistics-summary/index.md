+++
type = "question"
title = "Extracting datas from the Statistics Summary"
description = '''Hello, i would like to extract the Packets Captured and Displayed from the Summary Panel (in statistics menu), copy/past function is not working.'''
date = "2011-04-01T03:06:00Z"
lastmod = "2011-04-04T11:00:00Z"
weight = 3263
keywords = [ "summary", "datas", "extracting" ]
aliases = [ "/questions/3263" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Extracting datas from the Statistics Summary](/questions/3263/extracting-datas-from-the-statistics-summary)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3263-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3263-score" class="post-score" title="current number of votes">0</div><span id="post-3263-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, i would like to extract the Packets Captured and Displayed from the Summary Panel (in statistics menu), copy/past function is not working.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-summary" rel="tag" title="see questions tagged &#39;summary&#39;">summary</span> <span class="post-tag tag-link-datas" rel="tag" title="see questions tagged &#39;datas&#39;">datas</span> <span class="post-tag tag-link-extracting" rel="tag" title="see questions tagged &#39;extracting&#39;">extracting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '11, 03:06</strong></p><img src="https://secure.gravatar.com/avatar/ee69756d892791b712b858e3d0c61d94?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chenry&#39;s gravatar image" /><p><span>chenry</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chenry has no accepted answers">0%</span></p></div></div><div id="comments-container-3263" class="comments-container"><span id="3327"></span><div id="comment-3327" class="comment"><div id="post-3327-score" class="comment-score"></div><div class="comment-text"><p>If capinfos doesn't do it for you, check out tcptrace.</p></div><div id="comment-3327-info" class="comment-info"><span class="comment-age">(04 Apr '11, 11:00)</span> <span class="comment-user userinfo">hansangb</span></div></div></div><div id="comment-tools-3263" class="comment-tools"></div><div class="clear"></div><div id="comment-3263-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="3272"></span>

<div id="answer-container-3272" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3272-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3272-score" class="post-score" title="current number of votes">0</div><span id="post-3272-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The ability to copy that information is not yet supported, but you can probably obtain a lot of what you want using <a href="http://www.wireshark.org/docs/wsug_html_chunked/AppToolscapinfos.html">capinfos</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Apr '11, 07:49</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-3272" class="comments-container"></div><div id="comment-tools-3272" class="comment-tools"></div><div class="clear"></div><div id="comment-3272-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="3320"></span>

<div id="answer-container-3320" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3320-score" class="post-score" title="current number of votes">0</div><span id="post-3320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Thanks for your answer, i tried capinfos but it doesnt measure the total size of data on the network (in/out) from a logfile and i need to be able to copy/past this info.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Apr '11, 00:57</strong></p><img src="https://secure.gravatar.com/avatar/ee69756d892791b712b858e3d0c61d94?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chenry&#39;s gravatar image" /><p><span>chenry</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chenry has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Apr '11, 01:10</strong> </span></p></div></div><div id="comments-container-3320" class="comments-container"></div><div id="comment-tools-3320" class="comment-tools"></div><div class="clear"></div><div id="comment-3320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

