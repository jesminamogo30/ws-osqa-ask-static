+++
type = "question"
title = "How can I capture RTP packets from youtube?"
description = '''I am not able to catch RTP packets from youtube. I have tried checking &quot;Try to decode RTP outside of conversions&quot;, but it didnt help. I tried decoding UDP packet as RTP. After decoding, I am not able to do RTP stream analysis.'''
date = "2013-02-15T03:15:00Z"
lastmod = "2013-02-16T02:04:00Z"
weight = 18651
keywords = [ "decode_rtp" ]
aliases = [ "/questions/18651" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can I capture RTP packets from youtube?](/questions/18651/how-can-i-capture-rtp-packets-from-youtube)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18651-score" class="post-score" title="current number of votes">0</div><span id="post-18651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am not able to catch RTP packets from youtube. I have tried checking "Try to decode RTP outside of conversions", but it didnt help. I tried decoding UDP packet as RTP. After decoding, I am not able to do RTP stream analysis.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode_rtp" rel="tag" title="see questions tagged &#39;decode_rtp&#39;">decode_rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '13, 03:15</strong></p><img src="https://secure.gravatar.com/avatar/8e3a429d923f9d07fcb5d10557b8ba85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Autotest&#39;s gravatar image" /><p><span>Autotest</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Autotest has no accepted answers">0%</span></p></div></div><div id="comments-container-18651" class="comments-container"></div><div id="comment-tools-18651" class="comment-tools"></div><div class="clear"></div><div id="comment-18651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18668"></span>

<div id="answer-container-18668" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18668-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18668-score" class="post-score" title="current number of votes">2</div><span id="post-18668-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As far as I know, Youtube is <strong>not</strong> using RTP anywhere (they stream their videos via HTTP), so that's the reason why you can't decode it.<br />
</p><p>If you think they use RTP, please post a link to that video.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Feb '13, 02:04</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Feb '13, 02:10</strong> </span></p></div></div><div id="comments-container-18668" class="comments-container"></div><div id="comment-tools-18668" class="comment-tools"></div><div class="clear"></div><div id="comment-18668-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

