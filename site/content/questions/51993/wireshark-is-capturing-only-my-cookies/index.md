+++
type = "question"
title = "wireshark is capturing only my cookies"
description = '''wireshark is capturing only my cookies'''
date = "2016-04-27T02:35:00Z"
lastmod = "2016-04-28T02:40:00Z"
weight = 51993
keywords = [ "capturing" ]
aliases = [ "/questions/51993" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark is capturing only my cookies](/questions/51993/wireshark-is-capturing-only-my-cookies)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51993-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51993-score" class="post-score" title="current number of votes">0</div><span id="post-51993-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>wireshark is capturing only my cookies</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capturing" rel="tag" title="see questions tagged &#39;capturing&#39;">capturing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Apr '16, 02:35</strong></p><img src="https://secure.gravatar.com/avatar/f91f5ea69c00ecef9eb912dfdc96890c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sanchet%20Kore&#39;s gravatar image" /><p><span>Sanchet Kore</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sanchet Kore has no accepted answers">0%</span></p></div></div><div id="comments-container-51993" class="comments-container"><span id="51997"></span><div id="comment-51997" class="comment"><div id="post-51997-score" class="comment-score"></div><div class="comment-text"><p>To get an answer it would help to provide more/detailed information. What's your question?</p></div><div id="comment-51997-info" class="comment-info"><span class="comment-age">(27 Apr '16, 03:38)</span> <span class="comment-user userinfo">Uli</span></div></div><span id="52016"></span><div id="comment-52016" class="comment"><div id="post-52016-score" class="comment-score"></div><div class="comment-text"><p>Send some cookies my way! ;)</p><p>No really, each post should have a clear, specific question in the title field. Please rephrase the title as a proper question.</p></div><div id="comment-52016-info" class="comment-info"><span class="comment-age">(27 Apr '16, 07:02)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="52036"></span><div id="comment-52036" class="comment"><div id="post-52036-score" class="comment-score"></div><div class="comment-text"><p>i mean that its only capturing my session</p></div><div id="comment-52036-info" class="comment-info"><span class="comment-age">(28 Apr '16, 02:29)</span> <span class="comment-user userinfo">Sanchet Kore</span></div></div><span id="52037"></span><div id="comment-52037" class="comment"><div id="post-52037-score" class="comment-score"></div><div class="comment-text"><p>that is its not showing other pc on my network</p></div><div id="comment-52037-info" class="comment-info"><span class="comment-age">(28 Apr '16, 02:30)</span> <span class="comment-user userinfo">Sanchet Kore</span></div></div></div><div id="comment-tools-51993" class="comment-tools"></div><div class="clear"></div><div id="comment-51993-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52040"></span>

<div id="answer-container-52040" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52040-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52040-score" class="post-score" title="current number of votes">0</div><span id="post-52040-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You probably want to look into <a href="https://wiki.wireshark.org/CaptureSetup">your capture setup</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '16, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-52040" class="comments-container"></div><div id="comment-tools-52040" class="comment-tools"></div><div class="clear"></div><div id="comment-52040-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

