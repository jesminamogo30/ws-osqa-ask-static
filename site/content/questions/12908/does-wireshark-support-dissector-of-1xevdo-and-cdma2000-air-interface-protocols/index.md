+++
type = "question"
title = "Does WireShark Support Dissector of 1xEVDO and CDMA2000 Air Interface protocols?"
description = '''Does any expert knows that if WireShark support Dissector of 1xEVDO and CDMA2000 Over the Air (OTA) Interface protocols? '''
date = "2012-07-23T01:21:00Z"
lastmod = "2012-07-23T01:21:00Z"
weight = 12908
keywords = [ "c2k", "cdma2000", "ota", "1xevdo" ]
aliases = [ "/questions/12908" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Does WireShark Support Dissector of 1xEVDO and CDMA2000 Air Interface protocols?](/questions/12908/does-wireshark-support-dissector-of-1xevdo-and-cdma2000-air-interface-protocols)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12908-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12908-score" class="post-score" title="current number of votes">0</div><span id="post-12908-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does any expert knows that if WireShark support Dissector of 1xEVDO and CDMA2000 Over the Air (OTA) Interface protocols?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-c2k" rel="tag" title="see questions tagged &#39;c2k&#39;">c2k</span> <span class="post-tag tag-link-cdma2000" rel="tag" title="see questions tagged &#39;cdma2000&#39;">cdma2000</span> <span class="post-tag tag-link-ota" rel="tag" title="see questions tagged &#39;ota&#39;">ota</span> <span class="post-tag tag-link-1xevdo" rel="tag" title="see questions tagged &#39;1xevdo&#39;">1xevdo</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jul '12, 01:21</strong></p><img src="https://secure.gravatar.com/avatar/3f941a275463946999dbd0130888c455?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tools_hpp&#39;s gravatar image" /><p><span>tools_hpp</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tools_hpp has no accepted answers">0%</span></p></div></div><div id="comments-container-12908" class="comments-container"></div><div id="comment-tools-12908" class="comment-tools"></div><div class="clear"></div><div id="comment-12908-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

