+++
type = "question"
title = "Save decrypted WPA packets to a new file?"
description = '''I captured my own wpa2 wifi traffic and successfully decrypted it following the tutorial But is there a way to save the decrypted version as a new pcap file? so other people can read the pcap without the encrptyion key'''
date = "2014-02-27T07:25:00Z"
lastmod = "2014-02-28T07:39:00Z"
weight = 30235
keywords = [ "decrypted", "save", "wpa" ]
aliases = [ "/questions/30235" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Save decrypted WPA packets to a new file?](/questions/30235/save-decrypted-wpa-packets-to-a-new-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30235-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30235-score" class="post-score" title="current number of votes">0</div><span id="post-30235-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I captured my own wpa2 wifi traffic and successfully decrypted it following the <a href="http://wiki.wireshark.org/HowToDecrypt802.11">tutorial</a> But is there a way to save the decrypted version as a new pcap file? so other people can read the pcap without the encrptyion key</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decrypted" rel="tag" title="see questions tagged &#39;decrypted&#39;">decrypted</span> <span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span> <span class="post-tag tag-link-wpa" rel="tag" title="see questions tagged &#39;wpa&#39;">wpa</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '14, 07:25</strong></p><img src="https://secure.gravatar.com/avatar/2c0b87f1604f0f1ed1b1380b124c2e45?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ken&#39;s gravatar image" /><p><span>Ken</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ken has no accepted answers">0%</span></p></div></div><div id="comments-container-30235" class="comments-container"></div><div id="comment-tools-30235" class="comment-tools"></div><div class="clear"></div><div id="comment-30235-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30271"></span>

<div id="answer-container-30271" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30271-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30271-score" class="post-score" title="current number of votes">1</div><span id="post-30271-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See my answer to a similar question</p><blockquote><p><a href="http://ask.wireshark.org/questions/23606/decrypting-browser-https-wrapped-into-stunnel-ssl">http://ask.wireshark.org/questions/23606/decrypting-browser-https-wrapped-into-stunnel-ssl</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Feb '14, 07:39</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-30271" class="comments-container"></div><div id="comment-tools-30271" class="comment-tools"></div><div class="clear"></div><div id="comment-30271-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

