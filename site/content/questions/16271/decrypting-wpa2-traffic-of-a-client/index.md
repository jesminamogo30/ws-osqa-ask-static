+++
type = "question"
title = "decrypting WPA2 Traffic of a client"
description = '''Hello, i&#x27;m trying to decrypt the packets of my android-smartphone which is connected to my AP. I setup the wpa-pwd in the decryption settings. The Decryption works with the wireshark example capture file, so i can see the decrypted Packets in that capture. The Problem is that i only get EAPOL Packet...'''
date = "2012-11-25T04:13:00Z"
lastmod = "2012-11-25T04:13:00Z"
weight = 16271
keywords = [ "decryption", "handshake", "4-way", "wpa2", "eapol" ]
aliases = [ "/questions/16271" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [decrypting WPA2 Traffic of a client](/questions/16271/decrypting-wpa2-traffic-of-a-client)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16271-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16271-score" class="post-score" title="current number of votes">0</div><span id="post-16271-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>i'm trying to decrypt the packets of my android-smartphone which is connected to my AP. I setup the wpa-pwd in the decryption settings. The Decryption works with the wireshark example capture file, so i can see the decrypted Packets in that capture.</p><p>The Problem is that i only get EAPOL Packets 1,2,3 when i capture the 4 Way Handshake between my smartphone and the AP, but the 4th is Missing. Does Wireshark need all 4 Packets before it starts Decryption? I read that only Packets 1 and 2 are needed for Decryption!? Is it just not decrypting because the fourth Packet is missing?</p><p>i'm using wireshark 1.8.1 on Backtrack5. Wifi Card is an Intel 5100agn.</p><p>-frank</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-handshake" rel="tag" title="see questions tagged &#39;handshake&#39;">handshake</span> <span class="post-tag tag-link-4-way" rel="tag" title="see questions tagged &#39;4-way&#39;">4-way</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span> <span class="post-tag tag-link-eapol" rel="tag" title="see questions tagged &#39;eapol&#39;">eapol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '12, 04:13</strong></p><img src="https://secure.gravatar.com/avatar/0e3b05d6b65733cf4533fa0753aa4e08?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="frank999&#39;s gravatar image" /><p><span>frank999</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="frank999 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Nov '12, 04:21</strong> </span></p></div></div><div id="comments-container-16271" class="comments-container"></div><div id="comment-tools-16271" class="comment-tools"></div><div class="clear"></div><div id="comment-16271-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

