+++
type = "question"
title = "Problem with NetScaler HA protocol not being shown in the wireshark 1.4.3 protocol stack?"
description = '''I have written a dissector for NetScaler HA protocol. The dissector was synced with the previous version of wireshark. Now with the latest 1.4.3 version, I do not see the name of my protocol ie NS HA in the protocol when I try to decode the packets. Similarly, I am currently writing a dissector for ...'''
date = "2011-01-31T01:10:00Z"
lastmod = "2011-01-31T01:34:00Z"
weight = 2027
keywords = [ "wireshark" ]
aliases = [ "/questions/2027" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Problem with NetScaler HA protocol not being shown in the wireshark 1.4.3 protocol stack?](/questions/2027/problem-with-netscaler-ha-protocol-not-being-shown-in-the-wireshark-143-protocol-stack)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2027-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2027-score" class="post-score" title="current number of votes">0</div><span id="post-2027-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have written a dissector for NetScaler HA protocol. The dissector was synced with the previous version of wireshark. Now with the latest 1.4.3 version, I do not see the name of my protocol ie NS HA in the protocol when I try to decode the packets. Similarly, I am currently writing a dissector for another NetScaler protocol named NS Node to Node Messages. I am facing the same problem with this new protocol. So has there been any changes made to the trunk?</p><p>Is there anything, anyone can help me with???</p><p>Thanks and Regards, Sidharth</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jan '11, 01:10</strong></p><img src="https://secure.gravatar.com/avatar/5a41ae1c710064aebdb9a4e0a1788d12?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sid&#39;s gravatar image" /><p><span>sid</span><br />
<span class="score" title="45 reputation points">45</span><span title="19 badges"><span class="badge1">●</span><span class="badgecount">19</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sid has no accepted answers">0%</span></p></div></div><div id="comments-container-2027" class="comments-container"><span id="2029"></span><div id="comment-2029" class="comment"><div id="post-2029-score" class="comment-score"></div><div class="comment-text"><blockquote><p>So has there been any changes made to the trunk?</p></blockquote><p>We haven't been sitting still, so yes there have been changes. A lot of changes.</p><p>The question is: what <strong>do</strong> you see?</p></div><div id="comment-2029-info" class="comment-info"><span class="comment-age">(31 Jan '11, 01:34)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-2027" class="comment-tools"></div><div class="clear"></div><div id="comment-2027-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

