+++
type = "question"
title = "keyboard overtyping"
description = '''Hi - I have a situation where what I am writing is being &quot;over-written&quot; while I am writing it. Let me give a quick examples: &quot;Hi guys how are you doing?&quot; as I am writing would be perhaps be changed to: &quot;Hi guts hoe are you doimg?&quot; Typically the changes are single-letter mistypes, so they appear like...'''
date = "2014-02-18T15:24:00Z"
lastmod = "2014-02-19T01:45:00Z"
weight = 29987
keywords = [ "origin", "unknown", "overtyping", "keyboard" ]
aliases = [ "/questions/29987" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [keyboard overtyping](/questions/29987/keyboard-overtyping)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29987-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29987-score" class="post-score" title="current number of votes">0</div><span id="post-29987-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi - I have a situation where what I am writing is being "over-written" while I am writing it.</p><p>Let me give a quick examples: "Hi guys how are you doing?" as I am writing would be perhaps be changed to: "Hi guts hoe are you doimg?" Typically the changes are single-letter mistypes, so they appear like I am making keyboard errors, but I am not making these errors. The keyboard input is being modified.</p><p>At first I thought this was mistypes, but the problem has become very bad &amp; seems like it is either someone using a program to mess with my input or has implemented some type of script to edit my keyboard input. Can anyone assist me to figure out what I need to monitor on wireshark to figure the locational origin of these edits?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-origin" rel="tag" title="see questions tagged &#39;origin&#39;">origin</span> <span class="post-tag tag-link-unknown" rel="tag" title="see questions tagged &#39;unknown&#39;">unknown</span> <span class="post-tag tag-link-overtyping" rel="tag" title="see questions tagged &#39;overtyping&#39;">overtyping</span> <span class="post-tag tag-link-keyboard" rel="tag" title="see questions tagged &#39;keyboard&#39;">keyboard</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '14, 15:24</strong></p><img src="https://secure.gravatar.com/avatar/031b2ba713acf64524a342a45414318d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nehalem&#39;s gravatar image" /><p><span>Nehalem</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nehalem has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Feb '14, 15:59</strong> </span></p></div></div><div id="comments-container-29987" class="comments-container"></div><div id="comment-tools-29987" class="comment-tools"></div><div class="clear"></div><div id="comment-29987-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29996"></span>

<div id="answer-container-29996" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29996-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29996-score" class="post-score" title="current number of votes">-1</div><span id="post-29996-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Whilst this is possibly some form of PEBCAK issue, Wireshark is unlikely to be able to help as your keyboard isn't connected to your PC by a network adaptor that Wireshark can capture network traffic on.</p><p>Have you tried another keyboard? Can you get someone else to try your keyboard?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Feb '14, 01:45</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Feb '14, 01:45</strong> </span></p></div></div><div id="comments-container-29996" class="comments-container"></div><div id="comment-tools-29996" class="comment-tools"></div><div class="clear"></div><div id="comment-29996-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

