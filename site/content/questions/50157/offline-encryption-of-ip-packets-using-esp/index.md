+++
type = "question"
title = "Offline encryption of IP packets using ESP"
description = '''I understand that we can use wireshark or tshark to decryptm, in offline mode, IPSec packets that are encrypted using ESP. Is it possible to do the reverse using tshark or another userspace (vs. kernel-space) application? In other words, is there any application that can be used to encrypt IP packet...'''
date = "2016-02-12T08:03:00Z"
lastmod = "2016-02-13T04:22:00Z"
weight = 50157
keywords = [ "encryption", "ipsec", "esp" ]
aliases = [ "/questions/50157" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Offline encryption of IP packets using ESP](/questions/50157/offline-encryption-of-ip-packets-using-esp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50157-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50157-score" class="post-score" title="current number of votes">0</div><span id="post-50157-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I understand that we can use wireshark or tshark to decryptm, in offline mode, IPSec packets that are encrypted using ESP. Is it possible to do the reverse using tshark or another userspace (vs. kernel-space) application? In other words, is there any application that can be used to encrypt IP packets (e.g., captured using wireshark), in offline mode, into IPSec packets using ESP protocol?</p><p>Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span> <span class="post-tag tag-link-ipsec" rel="tag" title="see questions tagged &#39;ipsec&#39;">ipsec</span> <span class="post-tag tag-link-esp" rel="tag" title="see questions tagged &#39;esp&#39;">esp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Feb '16, 08:03</strong></p><img src="https://secure.gravatar.com/avatar/13040e66084512afee5fc2fef2c48f06?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="derisavi&#39;s gravatar image" /><p><span>derisavi</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="derisavi has no accepted answers">0%</span></p></div></div><div id="comments-container-50157" class="comments-container"></div><div id="comment-tools-50157" class="comment-tools"></div><div class="clear"></div><div id="comment-50157-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50172"></span>

<div id="answer-container-50172" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50172-score" class="post-score" title="current number of votes">0</div><span id="post-50172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>To answer your question: There might be tools available to do that, but I don't know any. Wireshark can't do that!</p><p>What is the purpose of such a tool?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Feb '16, 04:22</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Feb '16, 04:24</strong> </span></p></div></div><div id="comments-container-50172" class="comments-container"></div><div id="comment-tools-50172" class="comment-tools"></div><div class="clear"></div><div id="comment-50172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

