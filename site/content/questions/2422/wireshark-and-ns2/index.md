+++
type = "question"
title = "wireshark and ns2"
description = '''Hi   I am julie...I wanted to do know if the packets captured using wireshark can be accessed and analysed using the network simulator NS2?'''
date = "2011-02-19T00:35:00Z"
lastmod = "2011-02-21T19:54:00Z"
weight = 2422
keywords = [ "plshelp" ]
aliases = [ "/questions/2422" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark and ns2](/questions/2422/wireshark-and-ns2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2422-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2422-score" class="post-score" title="current number of votes">0</div><span id="post-2422-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I am julie...I wanted to do know if the packets captured using wireshark can be accessed and analysed using the network simulator NS2?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-plshelp" rel="tag" title="see questions tagged &#39;plshelp&#39;">plshelp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '11, 00:35</strong></p><img src="https://secure.gravatar.com/avatar/e4260b26c333bc8ee43efde05b2b7f6c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="julie&#39;s gravatar image" /><p><span>julie</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="julie has no accepted answers">0%</span></p></div></div><div id="comments-container-2422" class="comments-container"></div><div id="comment-tools-2422" class="comment-tools"></div><div class="clear"></div><div id="comment-2422-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2465"></span>

<div id="answer-container-2465" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2465-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2465-score" class="post-score" title="current number of votes">1</div><span id="post-2465-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>A quick search suggests that the answer is yes.</p><p>See: http://www.isi.edu/nsnam/ns/ns-emulation.html</p><p>"Pcap/File Network Objects These objects are similar to the Pcap/BPF objects, except that network data is taken from a trace file rather than the live network. As such, the notion of promiscuous mode and the naming of a particular interface (available to the BPF objects) are not available for the file objects. In addition, the ability to create trace files is still under development. This facility will provide the ability to create tcpdump-compatible trace files. "</p><p>Wireshark writes capture files in pcap format (as the default).</p><p>I suggest you'll get much more and better info if you inquire on a ns2 mailing list.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Feb '11, 19:54</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-2465" class="comments-container"></div><div id="comment-tools-2465" class="comment-tools"></div><div class="clear"></div><div id="comment-2465-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

