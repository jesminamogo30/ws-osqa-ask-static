+++
type = "question"
title = "Fax Capture"
description = '''Is there a way to export fax tiff file image from .pcap captured file. In other words i am trying to backup all faxes that are passing on my network,and export the fax file later on. Is this feasible and how . NB:i am using sip protocol and fax protocol is bypass ,G711  i can use T38 if that will so...'''
date = "2011-04-01T03:47:00Z"
lastmod = "2011-04-01T03:47:00Z"
weight = 3266
keywords = [ "fax" ]
aliases = [ "/questions/3266" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Fax Capture](/questions/3266/fax-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3266-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3266-score" class="post-score" title="current number of votes">0</div><span id="post-3266-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to export fax tiff file image from .pcap captured file. In other words i am trying to backup all faxes that are passing on my network,and export the fax file later on. Is this feasible and how .</p><p>NB:i am using sip protocol and fax protocol is bypass ,G711 i can use T38 if that will solve the matter</p><p>regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fax" rel="tag" title="see questions tagged &#39;fax&#39;">fax</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '11, 03:47</strong></p><img src="https://secure.gravatar.com/avatar/ff8b5a9e23a2bebc9317315167347868?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Khaled%20Chehab&#39;s gravatar image" /><p><span>Khaled Chehab</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Khaled Chehab has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Apr '11, 03:55</strong> </span></p></div></div><div id="comments-container-3266" class="comments-container"></div><div id="comment-tools-3266" class="comment-tools"></div><div class="clear"></div><div id="comment-3266-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

