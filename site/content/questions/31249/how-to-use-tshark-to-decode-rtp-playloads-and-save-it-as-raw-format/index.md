+++
type = "question"
title = "How to use tshark to decode RTP playloads and save it as raw format"
description = '''Hi How to use tshark to decode RTP playloads from pcap file, and save it as raw format? '''
date = "2014-03-28T12:25:00Z"
lastmod = "2014-03-28T14:48:00Z"
weight = 31249
keywords = [ "playloads", "rtp" ]
aliases = [ "/questions/31249" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to use tshark to decode RTP playloads and save it as raw format](/questions/31249/how-to-use-tshark-to-decode-rtp-playloads-and-save-it-as-raw-format)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31249-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31249-score" class="post-score" title="current number of votes">0</div><span id="post-31249-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi How to use tshark to decode RTP playloads from pcap file, and save it as raw format?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-playloads" rel="tag" title="see questions tagged &#39;playloads&#39;">playloads</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Mar '14, 12:25</strong></p><img src="https://secure.gravatar.com/avatar/7d344b8411db10bd824cd120f1ee556a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kmmahendra&#39;s gravatar image" /><p><span>kmmahendra</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kmmahendra has no accepted answers">0%</span></p></div></div><div id="comments-container-31249" class="comments-container"></div><div id="comment-tools-31249" class="comment-tools"></div><div class="clear"></div><div id="comment-31249-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31259"></span>

<div id="answer-container-31259" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31259-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31259-score" class="post-score" title="current number of votes">0</div><span id="post-31259-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See my answers to similar questions:</p><blockquote><p><a href="http://ask.wireshark.org/questions/21193/extracting-rtp-payload-and-dumping-to-a-ts-file">http://ask.wireshark.org/questions/21193/extracting-rtp-payload-and-dumping-to-a-ts-file</a><br />
<a href="http://ask.wireshark.org/questions/10493/can-tshark-extract-voice-data-from-an-rtp-stream">http://ask.wireshark.org/questions/10493/can-tshark-extract-voice-data-from-an-rtp-stream</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Mar '14, 14:48</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-31259" class="comments-container"></div><div id="comment-tools-31259" class="comment-tools"></div><div class="clear"></div><div id="comment-31259-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

