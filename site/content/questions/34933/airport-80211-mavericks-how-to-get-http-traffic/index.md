+++
type = "question"
title = "Airport 802.11, Mavericks. How to get HTTP traffic"
description = '''I want to monitor all traffic on the wireless net from a single machine, running Mavericks. I have Wireshark correctly capturing 802.11 traffic on the wireless net, but all I get are 802.11, UDP, DNS and MDNS packets. I don&#x27;t see any HTTP traffic. How can I see the HTP requests?  I have Monitor made...'''
date = "2014-07-27T14:46:00Z"
lastmod = "2014-07-29T13:36:00Z"
weight = 34933
keywords = [ "macosx", "802.11" ]
aliases = [ "/questions/34933" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Airport 802.11, Mavericks. How to get HTTP traffic](/questions/34933/airport-80211-mavericks-how-to-get-http-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34933-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34933-score" class="post-score" title="current number of votes">0</div><span id="post-34933-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to monitor all traffic on the wireless net from a single machine, running Mavericks.</p><p>I have Wireshark correctly capturing 802.11 traffic on the wireless net, but all I get are 802.11, UDP, DNS and MDNS packets. I don't see any HTTP traffic. How can I see the HTP requests?</p><ul><li>I have Monitor made turned on in Prefs for 802.11</li><li>There is no encryption on the network right now, so I don't have to worry about that</li></ul><p>Anything special in the 802.11 prefs like "Vendor specific HT Elements" or "Assume packets have FCS"?</p><p>_mike</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jul '14, 14:46</strong></p><img src="https://secure.gravatar.com/avatar/684f6f3530e6092cd698abd48841a610?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nibeck&#39;s gravatar image" /><p><span>nibeck</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nibeck has no accepted answers">0%</span></p></div></div><div id="comments-container-34933" class="comments-container"><span id="34981"></span><div id="comment-34981" class="comment"><div id="post-34981-score" class="comment-score"></div><div class="comment-text"><p>So the DNS packets are unicast (i.e., they're not sent to a MAC address of ff:ff:ff:ff:ff:ff, for example)?</p></div><div id="comment-34981-info" class="comment-info"><span class="comment-age">(29 Jul '14, 13:36)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-34933" class="comment-tools"></div><div class="clear"></div><div id="comment-34933-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

