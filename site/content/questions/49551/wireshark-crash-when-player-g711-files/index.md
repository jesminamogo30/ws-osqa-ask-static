+++
type = "question"
title = "Wireshark crash when player G711 files"
description = '''Why my wireshark (Win10-64) crash after loading and try to play captured files such as SIP_CALL_RTP_G711 ?'''
date = "2016-01-27T07:12:00Z"
lastmod = "2016-01-27T07:34:00Z"
weight = 49551
keywords = [ "wireshark_crashed" ]
aliases = [ "/questions/49551" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark crash when player G711 files](/questions/49551/wireshark-crash-when-player-g711-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49551-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49551-score" class="post-score" title="current number of votes">0</div><span id="post-49551-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Why my wireshark (Win10-64) crash after loading and try to play captured files such as SIP_CALL_RTP_G711 ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark_crashed" rel="tag" title="see questions tagged &#39;wireshark_crashed&#39;">wireshark_crashed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '16, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/abdd58ac711f99659ffd39f398b9f425?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dario&#39;s gravatar image" /><p><span>Dario</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dario has no accepted answers">0%</span></p></div></div><div id="comments-container-49551" class="comments-container"></div><div id="comment-tools-49551" class="comment-tools"></div><div class="clear"></div><div id="comment-49551-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49554"></span>

<div id="answer-container-49554" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49554-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49554-score" class="post-score" title="current number of votes">0</div><span id="post-49554-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Because it's probably a bug, simple as that.</p><p>Please post bug reports with as much detail as possible at <a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a> instead of this forum.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jan '16, 07:34</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-49554" class="comments-container"></div><div id="comment-tools-49554" class="comment-tools"></div><div class="clear"></div><div id="comment-49554-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

