+++
type = "question"
title = "Silent Install"
description = '''how do I install Wireshark 2.0.1 without installing USBPcap in silent mode'''
date = "2016-01-29T06:30:00Z"
lastmod = "2016-01-29T06:30:00Z"
weight = 49623
keywords = [ "silentinst" ]
aliases = [ "/questions/49623" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Silent Install](/questions/49623/silent-install)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49623-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49623-score" class="post-score" title="current number of votes">0</div><span id="post-49623-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how do I install Wireshark 2.0.1 without installing USBPcap in silent mode</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-silentinst" rel="tag" title="see questions tagged &#39;silentinst&#39;">silentinst</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jan '16, 06:30</strong></p><img src="https://secure.gravatar.com/avatar/17570e9cf1c2f2e0e2d404feb9471bd6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="May8&#39;s gravatar image" /><p><span>May8</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="May8 has no accepted answers">0%</span></p></div></div><div id="comments-container-49623" class="comments-container"></div><div id="comment-tools-49623" class="comment-tools"></div><div class="clear"></div><div id="comment-49623-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

