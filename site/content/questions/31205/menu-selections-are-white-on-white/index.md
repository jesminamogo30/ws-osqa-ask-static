+++
type = "question"
title = "Menu selections are white on white"
description = '''My problem is the Wireshark menu selections are white on white (as in both text and background are white). Normally the menu selection in a drop-down menu will be different from the other possible selections, yet still be legible. Here is a picture of the problem below, notice that &quot;File&quot; is whited ...'''
date = "2014-03-26T18:11:00Z"
lastmod = "2014-03-26T18:11:00Z"
weight = 31205
keywords = [ "options", "menuselection", "preferences", "user" ]
aliases = [ "/questions/31205" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Menu selections are white on white](/questions/31205/menu-selections-are-white-on-white)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31205-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31205-score" class="post-score" title="current number of votes">0</div><span id="post-31205-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My problem is the Wireshark menu selections are white on white (as in both text and background are white). Normally the menu selection in a drop-down menu will be different from the other possible selections, yet still be legible. Here is a picture of the problem below, notice that "File" is whited out.<img src="https://osqa-ask.wireshark.org/upfiles/WiresharkUI_1.jpg" alt="alt text" /></p><p>You know the classic prank: change the new person's machine defaults to white text on a white background, well, this is the type of problem I'm having. I've tried Wireshark preferences and View -&gt; Coloring rules, but didn't find anything. This is such a basic problem I would've thought it simple to resolve. X Windows is working for other applications; for example GIMP.</p><p>Running version 1.10.6 on Mac OS X 10.9.2, and using X Windows System Xquartz 2.7.5. I uninstalled Wireshark and removed everything I could find before installing the latest version, yet menu selections are "whited out". Anyone got any ideas what may fix this issue?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-options" rel="tag" title="see questions tagged &#39;options&#39;">options</span> <span class="post-tag tag-link-menuselection" rel="tag" title="see questions tagged &#39;menuselection&#39;">menuselection</span> <span class="post-tag tag-link-preferences" rel="tag" title="see questions tagged &#39;preferences&#39;">preferences</span> <span class="post-tag tag-link-user" rel="tag" title="see questions tagged &#39;user&#39;">user</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '14, 18:11</strong></p><img src="https://secure.gravatar.com/avatar/7cf262b46b9cef2a33e37f7154c7afcd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="glen356&#39;s gravatar image" /><p><span>glen356</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="glen356 has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Mar '14, 14:02</strong> </span></p></div></div><div id="comments-container-31205" class="comments-container"></div><div id="comment-tools-31205" class="comment-tools"></div><div class="clear"></div><div id="comment-31205-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

