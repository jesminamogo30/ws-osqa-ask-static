+++
type = "question"
title = "Is Wireshark certified or meets any kind of standards?"
description = '''Hi, does Wireshark meet any standards or is certified by other institutions? '''
date = "2015-01-14T22:54:00Z"
lastmod = "2015-01-15T00:32:00Z"
weight = 39140
keywords = [ "certified", "standard" ]
aliases = [ "/questions/39140" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is Wireshark certified or meets any kind of standards?](/questions/39140/is-wireshark-certified-or-meets-any-kind-of-standards)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39140-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39140-score" class="post-score" title="current number of votes">0</div><span id="post-39140-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, does Wireshark meet any standards or is certified by other institutions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-certified" rel="tag" title="see questions tagged &#39;certified&#39;">certified</span> <span class="post-tag tag-link-standard" rel="tag" title="see questions tagged &#39;standard&#39;">standard</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '15, 22:54</strong></p><img src="https://secure.gravatar.com/avatar/d0dd92a449121517f8851d1782ec921f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TomLap&#39;s gravatar image" /><p><span>TomLap</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TomLap has no accepted answers">0%</span></p></div></div><div id="comments-container-39140" class="comments-container"></div><div id="comment-tools-39140" class="comment-tools"></div><div class="clear"></div><div id="comment-39140-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39142"></span>

<div id="answer-container-39142" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39142-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39142-score" class="post-score" title="current number of votes">0</div><span id="post-39142-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not that I know of. What kind of certification would you need?</p><p>However, Wireshark has certainly grown into THE "industry standard" for Network Troubleshooting, so it's a "standard by itself" ;-))</p><p>If you are looking for certification for yourself, check out this:</p><blockquote><p><a href="http://wiresharktraining.com/certification.html">http://wiresharktraining.com/certification.html</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jan '15, 00:32</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jan '15, 00:33</strong> </span></p></div></div><div id="comments-container-39142" class="comments-container"></div><div id="comment-tools-39142" class="comment-tools"></div><div class="clear"></div><div id="comment-39142-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

