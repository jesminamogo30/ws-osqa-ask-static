+++
type = "question"
title = "DCOM Decode not displaying the UUID in v1.4.3"
description = '''The UUID of DCOM packets are not displayed in the latest version of Wireshark. Older versions of Wireshark (v1.2.2 at least) properly display the UUID, but the newest version (v1.4.3) simply displays &quot;Transfer Syntax: Version 1.1 network data representation protocol&quot; instead of the UUID of the packe...'''
date = "2011-01-18T12:15:00Z"
lastmod = "2011-01-18T14:15:00Z"
weight = 1794
keywords = [ "uuid", "dcom" ]
aliases = [ "/questions/1794" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [DCOM Decode not displaying the UUID in v1.4.3](/questions/1794/dcom-decode-not-displaying-the-uuid-in-v143)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1794-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1794-score" class="post-score" title="current number of votes">0</div><span id="post-1794-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The UUID of DCOM packets are not displayed in the latest version of Wireshark. Older versions of Wireshark (v1.2.2 at least) properly display the UUID, but the newest version (v1.4.3) simply displays "Transfer Syntax: Version 1.1 network data representation protocol" instead of the UUID of the packet (8a885d04-1ceb-11c9-9fe8-08002b104860).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-uuid" rel="tag" title="see questions tagged &#39;uuid&#39;">uuid</span> <span class="post-tag tag-link-dcom" rel="tag" title="see questions tagged &#39;dcom&#39;">dcom</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '11, 12:15</strong></p><img src="https://secure.gravatar.com/avatar/08ab2feaa7e9e8cf0284cb150bff616a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="glenn%20aydell&#39;s gravatar image" /><p><span>glenn aydell</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="glenn aydell has no accepted answers">0%</span></p></div></div><div id="comments-container-1794" class="comments-container"><span id="1796"></span><div id="comment-1796" class="comment"><div id="post-1796-score" class="comment-score">1</div><div class="comment-text"><p>And what was the question?</p><p>I remind you this is a Question and Answer site. If you have a bug report to file please use <a href="https://bugs.wireshark.org/">Bugzilla</a> and provide all relevant information and capture file.</p></div><div id="comment-1796-info" class="comment-info"><span class="comment-age">(18 Jan '11, 14:15)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-1794" class="comment-tools"></div><div class="clear"></div><div id="comment-1794-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

