+++
type = "question"
title = "Decrypt Connect Direct Secure Plus"
description = '''I&#x27;m wondering if it is possible to decrypt IBM/Sterling COnnect Direct Secure Plus. I understand that the STS protocol is a variation of Diffie-Hellman and can&#x27;t be decrypted, but I&#x27;m wondering if decryption is possible if you choose the SSL/TLS option.'''
date = "2014-06-30T15:11:00Z"
lastmod = "2014-06-30T15:11:00Z"
weight = 34301
keywords = [ "plus", "secure", "decrypt", "connect", "direct" ]
aliases = [ "/questions/34301" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt Connect Direct Secure Plus](/questions/34301/decrypt-connect-direct-secure-plus)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34301-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34301-score" class="post-score" title="current number of votes">0</div><span id="post-34301-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm wondering if it is possible to decrypt IBM/Sterling COnnect Direct Secure Plus. I understand that the STS protocol is a variation of Diffie-Hellman and can't be decrypted, but I'm wondering if decryption is possible if you choose the SSL/TLS option.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-plus" rel="tag" title="see questions tagged &#39;plus&#39;">plus</span> <span class="post-tag tag-link-secure" rel="tag" title="see questions tagged &#39;secure&#39;">secure</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span> <span class="post-tag tag-link-connect" rel="tag" title="see questions tagged &#39;connect&#39;">connect</span> <span class="post-tag tag-link-direct" rel="tag" title="see questions tagged &#39;direct&#39;">direct</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jun '14, 15:11</strong></p><img src="https://secure.gravatar.com/avatar/9c70aa6e240b38bb391e6d39e033c4e5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Steve%20Fenter&#39;s gravatar image" /><p><span>Steve Fenter</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Steve Fenter has no accepted answers">0%</span></p></div></div><div id="comments-container-34301" class="comments-container"></div><div id="comment-tools-34301" class="comment-tools"></div><div class="clear"></div><div id="comment-34301-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

