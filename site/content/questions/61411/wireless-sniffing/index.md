+++
type = "question"
title = "Wireless sniffing"
description = '''I just started using wireshark; I would like to learn how to view, what my brother and sisters are browsing while we are connected with one wifi. I&#x27;ve read few questions regarding this and some of it contains aircap. Is it really needed? Can&#x27;t wincap do it by itself?'''
date = "2017-05-15T09:55:00Z"
lastmod = "2017-05-15T10:15:00Z"
weight = 61411
keywords = [ "wireshark" ]
aliases = [ "/questions/61411" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless sniffing](/questions/61411/wireless-sniffing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61411-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61411-score" class="post-score" title="current number of votes">0</div><span id="post-61411-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just started using wireshark; I would like to learn how to view, what my brother and sisters are browsing while we are connected with one wifi. I've read few questions regarding this and some of it contains aircap. Is it really needed? Can't wincap do it by itself?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '17, 09:55</strong></p><img src="https://secure.gravatar.com/avatar/eb24f082a0ffba3bc5d21efa914fdc86?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tyrant1&#39;s gravatar image" /><p><span>tyrant1</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tyrant1 has no accepted answers">0%</span></p></div></div><div id="comments-container-61411" class="comments-container"></div><div id="comment-tools-61411" class="comment-tools"></div><div class="clear"></div><div id="comment-61411-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61413"></span>

<div id="answer-container-61413" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61413-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61413-score" class="post-score" title="current number of votes">0</div><span id="post-61413-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>WinPcap cannot capture WiFi traffic in monitor mode which is necessary to capture the traffic of other users.</p><p>See the wiki page on <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">WLAN capture</a> for more info and solutions, e.g. boot into Linux.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '17, 10:15</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-61413" class="comments-container"></div><div id="comment-tools-61413" class="comment-tools"></div><div class="clear"></div><div id="comment-61413-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

