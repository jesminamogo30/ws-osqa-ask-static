+++
type = "question"
title = "what is WireShark Capture Speed?"
description = '''Hi, what is WireShark capture speed? how many packets per second?'''
date = "2015-08-12T01:44:00Z"
lastmod = "2015-08-13T00:37:00Z"
weight = 44992
keywords = [ "speed" ]
aliases = [ "/questions/44992" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [what is WireShark Capture Speed?](/questions/44992/what-is-wireshark-capture-speed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44992-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44992-score" class="post-score" title="current number of votes">0</div><span id="post-44992-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, what is WireShark capture speed? how many packets per second?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-speed" rel="tag" title="see questions tagged &#39;speed&#39;">speed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Aug '15, 01:44</strong></p><img src="https://secure.gravatar.com/avatar/856d7bf7f7092e5d126c256c374e0e1b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shark_search&#39;s gravatar image" /><p><span>shark_search</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shark_search has no accepted answers">0%</span></p></div></div><div id="comments-container-44992" class="comments-container"></div><div id="comment-tools-44992" class="comment-tools"></div><div class="clear"></div><div id="comment-44992-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44998"></span>

<div id="answer-container-44998" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44998-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44998-score" class="post-score" title="current number of votes">0</div><span id="post-44998-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It completely depends on your hardware. You cannot compare an old laptop against a brand new workstation equipped with dedicated high speed capture cards. E.g. 1GBit/s is hard to capture with normal PCs, while 100MBit/s is usually not a problem. 10G requires special hardware in any case (assuming the link is saturated). Wireshark is not the limiting factor, the capture hardware is.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Aug '15, 05:22</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-44998" class="comments-container"><span id="45035"></span><div id="comment-45035" class="comment"><div id="post-45035-score" class="comment-score"></div><div class="comment-text"><p>OK Thank You!</p></div><div id="comment-45035-info" class="comment-info"><span class="comment-age">(12 Aug '15, 22:57)</span> <span class="comment-user userinfo">shark_search</span></div></div><span id="45040"></span><div id="comment-45040" class="comment"><div id="post-45040-score" class="comment-score"></div><div class="comment-text"><p><span>@shark_search</span></p><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-45040-info" class="comment-info"><span class="comment-age">(13 Aug '15, 00:37)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-44998" class="comment-tools"></div><div class="clear"></div><div id="comment-44998-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

