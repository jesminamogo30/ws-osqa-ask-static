+++
type = "question"
title = "Sharkfest?"
description = '''Hello, I was thinking of going to sharkfest but I also have to justify the cost. My options are attend the conference or take the self paced class on wireshark university. I was looking for opinions from folks who may have done both of these to see which they think would be more valuable for someone...'''
date = "2017-04-13T10:31:00Z"
lastmod = "2017-04-13T14:06:00Z"
weight = 60812
keywords = [ "sharkfest" ]
aliases = [ "/questions/60812" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Sharkfest?](/questions/60812/sharkfest)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60812-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60812-score" class="post-score" title="current number of votes">0</div><span id="post-60812-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I was thinking of going to sharkfest but I also have to justify the cost. My options are attend the conference or take the self paced class on wireshark university. I was looking for opinions from folks who may have done both of these to see which they think would be more valuable for someone who's just getting into WS.</p><p>Thanks,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sharkfest" rel="tag" title="see questions tagged &#39;sharkfest&#39;">sharkfest</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Apr '17, 10:31</strong></p><img src="https://secure.gravatar.com/avatar/a6414c2ff8204ee9c4a3bc2a646c4644?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rock90&#39;s gravatar image" /><p><span>rock90</span><br />
<span class="score" title="21 reputation points">21</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rock90 has no accepted answers">0%</span></p></div></div><div id="comments-container-60812" class="comments-container"><span id="60818"></span><div id="comment-60818" class="comment"><div id="post-60818-score" class="comment-score"></div><div class="comment-text"><p>Do you mean the conference days only, or combined with Laura's class that runs before the talk tracks start?</p></div><div id="comment-60818-info" class="comment-info"><span class="comment-age">(13 Apr '17, 13:20)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="60819"></span><div id="comment-60819" class="comment"><div id="post-60819-score" class="comment-score"></div><div class="comment-text"><p>Jasper,</p><p>I did see that there is a class that runs before so lets include that in this conversation. Is it better to attend just her self paced study and skip the conference and her class or vice versa?</p><p>Ultimately, I would like to go to all 3 but if a guy could only do one which would be the most bang for my buck from a learning standpoint? I don't really care about dinners and networking as I just want to learn WS.</p></div><div id="comment-60819-info" class="comment-info"><span class="comment-age">(13 Apr '17, 13:30)</span> <span class="comment-user userinfo">rock90</span></div></div></div><div id="comment-tools-60812" class="comment-tools"></div><div class="clear"></div><div id="comment-60812-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60821"></span>

<div id="answer-container-60821" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60821-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60821-score" class="post-score" title="current number of votes">2</div><span id="post-60821-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I always recommend going for the instructor lead option, because you can't ask printed materials or videos any questions. Sometimes that's the difference between getting the idea and getting stuck. So if you can go to Laura's intro class plus the conference you have tons of opportunities to ask questions to trainers, speakers, and other conference attendees with analysis skills. And Sharkfest is an amazing conference in the way that it is small and not as anonymous as the big conferences. It's easy to mingle and you really can talk to anyone. The speakers, which are all packet analysis experts, are also always happy to help.</p><p>If you can only do one I would probably recommend Laura's class (or any instructor led Wireshark training), because it's a structured way to learn. Conference talks are great, too, of course, but they're 1-2 hour topics that are going to handle specific topics. Depending on your skill level you may get some great information, or it could go over your head. A look at the Agenda can give you an idea what's going to happen, and Youtube has a ton of talk videos from previous conferences, so you can check if that kind of thing suits you.</p><p>Working with books or video material is pretty dry, so it requires a lot of self discipline to get through. If you want to go that way I can highly recommend getting yourself some IT equipment (PC, Server, manageable switch, Capture Laptop) to experiment with real things and not just reading text all the time. I usually experiment a lot and when I don't understand what's happening I read up on what I need to know.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Apr '17, 14:06</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Apr '17, 04:53</strong> </span></p></div></div><div id="comments-container-60821" class="comments-container"></div><div id="comment-tools-60821" class="comment-tools"></div><div class="clear"></div><div id="comment-60821-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

