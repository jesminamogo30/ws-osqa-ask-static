+++
type = "question"
title = "Knocked Offline, Captured with WireShark"
description = '''Hello, I was recently playing a game and an angry player wanted to DDoS me. His friends informed me of his intentions, so I downloaded WireShark to try and catch him. However, I am not sure how to check what IP DDoS&#x27;d me. Could someone please teach me how to find out who did it? Greatly appreciated,...'''
date = "2015-02-23T17:45:00Z"
lastmod = "2015-02-23T18:36:00Z"
weight = 40037
keywords = [ "tutorials", "dos", "ddos", "help", "tutorial" ]
aliases = [ "/questions/40037" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Knocked Offline, Captured with WireShark](/questions/40037/knocked-offline-captured-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40037-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40037-score" class="post-score" title="current number of votes">0</div><span id="post-40037-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I was recently playing a game and an angry player wanted to DDoS me. His friends informed me of his intentions, so I downloaded WireShark to try and catch him. However, I am not sure how to check what IP DDoS'd me. Could someone please teach me how to find out who did it? Greatly appreciated, thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tutorials" rel="tag" title="see questions tagged &#39;tutorials&#39;">tutorials</span> <span class="post-tag tag-link-dos" rel="tag" title="see questions tagged &#39;dos&#39;">dos</span> <span class="post-tag tag-link-ddos" rel="tag" title="see questions tagged &#39;ddos&#39;">ddos</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span> <span class="post-tag tag-link-tutorial" rel="tag" title="see questions tagged &#39;tutorial&#39;">tutorial</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '15, 17:45</strong></p><img src="https://secure.gravatar.com/avatar/c0e5ad91ee237eae28a1cd456ab5e1ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AsianSensation&#39;s gravatar image" /><p><span>AsianSensation</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AsianSensation has no accepted answers">0%</span></p></div></div><div id="comments-container-40037" class="comments-container"></div><div id="comment-tools-40037" class="comment-tools"></div><div class="clear"></div><div id="comment-40037-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40038"></span>

<div id="answer-container-40038" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40038-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40038-score" class="post-score" title="current number of votes">0</div><span id="post-40038-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>DDOS stands for <em>Distributed</em> Denial of Service attack. That is, the attack would come from multiple IP addresses. Unless the other player controls a botnet, this was probably an idle threat. If you were the victim of a DDOS attack, you might be able to spot it in a Wireshark trace, but the IP addresses would be the bots that attacked you, not the IP address of the system that launched the attack.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Feb '15, 18:36</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-40038" class="comments-container"></div><div id="comment-tools-40038" class="comment-tools"></div><div class="clear"></div><div id="comment-40038-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

