+++
type = "question"
title = "export multiple HTTP objects, unfortunately they are called the same (filename)"
description = '''is it possible to have wireshark auto-rename files with same name, when saving all?'''
date = "2011-01-14T06:30:00Z"
lastmod = "2011-01-18T01:31:00Z"
weight = 1749
keywords = [ "objects", "http", "features", "file" ]
aliases = [ "/questions/1749" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [export multiple HTTP objects, unfortunately they are called the same (filename)](/questions/1749/export-multiple-http-objects-unfortunately-they-are-called-the-same-filename)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1749-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1749-score" class="post-score" title="current number of votes">0</div><span id="post-1749-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is it possible to have wireshark auto-rename files with same name, when saving all?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-objects" rel="tag" title="see questions tagged &#39;objects&#39;">objects</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-features" rel="tag" title="see questions tagged &#39;features&#39;">features</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '11, 06:30</strong></p><img src="https://secure.gravatar.com/avatar/e8124290f21890319b971ea7ab453959?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lukas&#39;s gravatar image" /><p><span>lukas</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lukas has no accepted answers">0%</span></p></div></div><div id="comments-container-1749" class="comments-container"></div><div id="comment-tools-1749" class="comment-tools"></div><div class="clear"></div><div id="comment-1749-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1785"></span>

<div id="answer-container-1785" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1785-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1785-score" class="post-score" title="current number of votes">0</div><span id="post-1785-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>ok, found a workaround, I was able to save these files by using Fiddler http://www.fiddler2.com/fiddler2/</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jan '11, 01:31</strong></p><img src="https://secure.gravatar.com/avatar/e8124290f21890319b971ea7ab453959?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lukas&#39;s gravatar image" /><p><span>lukas</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lukas has no accepted answers">0%</span></p></div></div><div id="comments-container-1785" class="comments-container"></div><div id="comment-tools-1785" class="comment-tools"></div><div class="clear"></div><div id="comment-1785-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

