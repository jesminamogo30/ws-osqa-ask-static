+++
type = "question"
title = "How to add &quot;Data rate&quot; columns to display on wireshark?"
description = '''I know how to add a new column but I don&#x27;t know the field name of data rate.  Thanks in advanced!'''
date = "2013-09-13T02:50:00Z"
lastmod = "2013-09-17T02:57:00Z"
weight = 24633
keywords = [ "ratetal", "data" ]
aliases = [ "/questions/24633" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [How to add "Data rate" columns to display on wireshark?](/questions/24633/how-to-add-data-rate-columns-to-display-on-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24633-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24633-score" class="post-score" title="current number of votes">0</div><span id="post-24633-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I know how to add a new column but I don't know the field name of data rate. Thanks in advanced!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ratetal" rel="tag" title="see questions tagged &#39;ratetal&#39;">ratetal</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Sep '13, 02:50</strong></p><img src="https://secure.gravatar.com/avatar/77775edc82a3c9748775152927d74605?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kaisan&#39;s gravatar image" /><p><span>kaisan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kaisan has no accepted answers">0%</span></p></div></div><div id="comments-container-24633" class="comments-container"><span id="24794"></span><div id="comment-24794" class="comment"><div id="post-24794-score" class="comment-score"></div><div class="comment-text"><p><img src="https://osqa-ask.wireshark.org/upfiles/Capture_9a.PNG" alt="Picture" /></p><p>I PrtSc the picture I saw and marked it yellow. Still waiting for the answer&lt;(__)&gt;</p></div><div id="comment-24794-info" class="comment-info"><span class="comment-age">(16 Sep '13, 20:24)</span> <span class="comment-user userinfo">kaisan</span></div></div><span id="24799"></span><div id="comment-24799" class="comment"><div id="post-24799-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Still waiting for the answer&lt;</p></blockquote><p>see the <strong>UPDATE</strong> in my answer.</p></div><div id="comment-24799-info" class="comment-info"><span class="comment-age">(17 Sep '13, 02:57)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-24633" class="comment-tools"></div><div class="clear"></div><div id="comment-24633-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="24634"></span>

<div id="answer-container-24634" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24634-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24634-score" class="post-score" title="current number of votes">0</div><span id="post-24634-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Where did you find this "Data rate" if it is not a field? Columns are generally fields shown for all packets (unless a packet doesn't contain that specific field).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Sep '13, 02:52</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></img></div></div><div id="comments-container-24634" class="comments-container"></div><div id="comment-tools-24634" class="comment-tools"></div><div class="clear"></div><div id="comment-24634-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="24635"></span>

<div id="answer-container-24635" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24635-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24635-score" class="post-score" title="current number of votes">0</div><span id="post-24635-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Find the field in the proto tree, then right click it and select "Apply as Column".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Sep '13, 02:54</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-24635" class="comments-container"></div><div id="comment-tools-24635" class="comment-tools"></div><div class="clear"></div><div id="comment-24635-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="24677"></span>

<div id="answer-container-24677" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24677-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24677-score" class="post-score" title="current number of votes">0</div><span id="post-24677-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I don't know the field name of data rate</p></blockquote><p>There is no general <strong>data rate</strong> field (at least I don't know one), with some minor exceptions, where a protocol provides some kind of negotiated data rate. See</p><blockquote><p>tshark -G fields | grep "data rate"<br />
tshark -G fields | find "data rate"</p></blockquote><p>So, if you are looking for one of those exceptions, please add details about the protocol you are interested in.</p><p>If you looking for a general <strong>data rate</strong> fields, i.e. number of bytes/second: Fields are properties of frames/packets. A <strong>data rate</strong> would be an information about the amount of data (bytes/frames) transmitted in a defined time interval. It would not make sense to add that information to a single packet as a field.</p><p>So, if you need information about the <strong>data rate</strong> over time in a conversation, I suggest to use IO graphs.</p><blockquote><p>Statistics -&gt; IO Graph</p></blockquote><p>That will show you the packets/bytes per tick (seconds) over the lifetime of the conversation.</p><blockquote><p><a href="http://www.wireshark.org/docs/wsug_html_chunked/ChStatIOGraphs.html">http://www.wireshark.org/docs/wsug_html_chunked/ChStatIOGraphs.html</a><br />
</p></blockquote><p><strong>== UPDATE ==</strong></p><p>According to the screenshot, I'd say that the "Data Rate" is actually the field <strong>radiotap.datarate</strong></p><p>So, here we go:</p><blockquote><p>Edit -&gt; Preferences -&gt; User Interface -&gt; Columns</p></blockquote><p>Click <strong>Add</strong> and then change the fields type to <strong>Custom</strong>. Insert the following into 'Field name:': <strong>radiotap.datarate</strong>. Finally rename the Column from 'New Column' to 'Data Rate'.</p><p>Hint: That field will only be there if a Radiotap header is available (wifi traffic in certain capture environments)!!</p><p>BTW: If there is a radiotap header, you can just open it and click on "Data Rate:". Then right click and choose 'Apply as column'.</p><p>See also:</p><blockquote><p><a href="http://packetlife.net/blog/2008/jun/27/adding-columns-to-wireshark/">http://packetlife.net/blog/2008/jun/27/adding-columns-to-wireshark/</a><br />
<a href="http://www.wireshark.org/docs/dfref/r/radiotap.html">http://www.wireshark.org/docs/dfref/r/radiotap.html</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Sep '13, 07:06</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Sep '13, 03:17</strong> </span></p></div></div><div id="comments-container-24677" class="comments-container"></div><div id="comment-tools-24677" class="comment-tools"></div><div class="clear"></div><div id="comment-24677-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

