+++
type = "question"
title = "UNIStim Packets"
description = '''I need to able to capture Unistim packets for VOIP calls across a WAN. Do I need a specific plug in or additional application on the WS app to do this. I see that UNIStim is a recognized protocol but my filters don&#x27;t allow it to be used. '''
date = "2016-11-22T13:18:00Z"
lastmod = "2016-11-22T14:54:00Z"
weight = 57554
keywords = [ "unistim" ]
aliases = [ "/questions/57554" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [UNIStim Packets](/questions/57554/unistim-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57554-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57554-score" class="post-score" title="current number of votes">0</div><span id="post-57554-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to able to capture Unistim packets for VOIP calls across a WAN. Do I need a specific plug in or additional application on the WS app to do this. I see that UNIStim is a recognized protocol but my filters don't allow it to be used.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-unistim" rel="tag" title="see questions tagged &#39;unistim&#39;">unistim</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Nov '16, 13:18</strong></p><img src="https://secure.gravatar.com/avatar/1df26a1672367c117ed1fa60a86fbc1a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Iamgb&#39;s gravatar image" /><p><span>Iamgb</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Iamgb has no accepted answers">0%</span></p></div></div><div id="comments-container-57554" class="comments-container"></div><div id="comment-tools-57554" class="comment-tools"></div><div class="clear"></div><div id="comment-57554-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57557"></span>

<div id="answer-container-57557" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57557-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57557-score" class="post-score" title="current number of votes">0</div><span id="post-57557-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You either have to set the UDP port used for UNIStim packets in the protocols preferences, or use 'Decode as' to define the UDP port traffic to be decoded as UNIStim.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Nov '16, 14:54</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-57557" class="comments-container"></div><div id="comment-tools-57557" class="comment-tools"></div><div class="clear"></div><div id="comment-57557-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

