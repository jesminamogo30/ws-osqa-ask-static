+++
type = "question"
title = "filter in WS for my unit"
description = '''how can i scan without 3x ip numbers? (filter 3x ip ) i will only scan my unit of there is onother ip number whats going out. thanks texi '''
date = "2012-01-26T08:17:00Z"
lastmod = "2012-01-26T08:17:00Z"
weight = 8624
keywords = [ "scan" ]
aliases = [ "/questions/8624" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [filter in WS for my unit](/questions/8624/filter-in-ws-for-my-unit)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8624-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8624-score" class="post-score" title="current number of votes">0</div><span id="post-8624-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how can i scan without 3x ip numbers? (filter 3x ip ) i will only scan my unit of there is onother ip number whats going out.</p><p>thanks texi<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-scan" rel="tag" title="see questions tagged &#39;scan&#39;">scan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jan '12, 08:17</strong></p><img src="https://secure.gravatar.com/avatar/cd6717ad9cabe70764b8ee880898f0b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="texi&#39;s gravatar image" /><p><span>texi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="texi has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-8624" class="comments-container"></div><div id="comment-tools-8624" class="comment-tools"></div><div class="clear"></div><div id="comment-8624-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

