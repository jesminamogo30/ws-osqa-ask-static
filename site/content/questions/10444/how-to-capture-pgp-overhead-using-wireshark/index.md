+++
type = "question"
title = "How to capture PGP overhead using wireshark"
description = '''Hello  Can anyone help me as I&#x27;m trying to find monitor PGP protocol  and capture its overhead, but I&#x27;m unable to do it  Please Help me  Thanks Vi'''
date = "2012-04-25T12:15:00Z"
lastmod = "2012-04-25T17:08:00Z"
weight = 10444
keywords = [ "capture", "overhead", "pgp" ]
aliases = [ "/questions/10444" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture PGP overhead using wireshark](/questions/10444/how-to-capture-pgp-overhead-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10444-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10444-score" class="post-score" title="current number of votes">0</div><span id="post-10444-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>Can anyone help me as I'm trying to find monitor PGP protocol and capture its overhead, but I'm unable to do it</p><p>Please Help me</p><p>Thanks Vi</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-overhead" rel="tag" title="see questions tagged &#39;overhead&#39;">overhead</span> <span class="post-tag tag-link-pgp" rel="tag" title="see questions tagged &#39;pgp&#39;">pgp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '12, 12:15</strong></p><img src="https://secure.gravatar.com/avatar/e5c5ba01ddd504c1cafce91882c22b4a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Viv&#39;s gravatar image" /><p><span>Viv</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Viv has no accepted answers">0%</span></p></div></div><div id="comments-container-10444" class="comments-container"></div><div id="comment-tools-10444" class="comment-tools"></div><div class="clear"></div><div id="comment-10444-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10450"></span>

<div id="answer-container-10450" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10450-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10450-score" class="post-score" title="current number of votes">0</div><span id="post-10450-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi,</p><p>may I ask what you mean by PGP 'protocol'? PGP is mainly a framework (and format) for e-mail encryption. There is no real network protocol involved, if one does not count encrypted mails sent via smtp.</p><p>BTW: You are not talking about 'OpenPGP HTTP Keyserver Protocol (HKP)', are you?</p><p>Regards Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Apr '12, 17:08</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span></p></div></div><div id="comments-container-10450" class="comments-container"></div><div id="comment-tools-10450" class="comment-tools"></div><div class="clear"></div><div id="comment-10450-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

