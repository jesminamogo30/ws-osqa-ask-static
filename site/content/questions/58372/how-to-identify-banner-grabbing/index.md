+++
type = "question"
title = "How to identify banner grabbing"
description = '''How do I identify banner grabbing ?  Would the below be consider banner grabbing ?  '''
date = "2016-12-27T11:33:00Z"
lastmod = "2016-12-27T16:29:00Z"
weight = 58372
keywords = [ "bannergrabbing" ]
aliases = [ "/questions/58372" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to identify banner grabbing](/questions/58372/how-to-identify-banner-grabbing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58372-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58372-score" class="post-score" title="current number of votes">0</div><span id="post-58372-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I identify banner grabbing ?</p><p>Would the below be consider banner grabbing ?</p><p><img src="https://osqa-ask.wireshark.org/upfiles/banner_grabbing.jpg" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/banner_grabbing2.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bannergrabbing" rel="tag" title="see questions tagged &#39;bannergrabbing&#39;">bannergrabbing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Dec '16, 11:33</strong></p><img src="https://secure.gravatar.com/avatar/149d6f8eb0595bad31c406551c9654a8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="doran_lum&#39;s gravatar image" /><p><span>doran_lum</span><br />
<span class="score" title="11 reputation points">11</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="doran_lum has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Dec '16, 12:09</strong> </span></p></div></div><div id="comments-container-58372" class="comments-container"><span id="58383"></span><div id="comment-58383" class="comment"><div id="post-58383-score" class="comment-score"></div><div class="comment-text"><p>Can you define what you mean by "banner grabbing"? The frame you highlighted seems to be a simple HTTP get, albeit with a long and complicated URL.</p></div><div id="comment-58383-info" class="comment-info"><span class="comment-age">(27 Dec '16, 16:29)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-58372" class="comment-tools"></div><div class="clear"></div><div id="comment-58372-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

