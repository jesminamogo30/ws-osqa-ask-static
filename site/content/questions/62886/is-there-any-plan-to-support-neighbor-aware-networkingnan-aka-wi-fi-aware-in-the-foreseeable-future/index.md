+++
type = "question"
title = "Is there any plan to support Neighbor Aware Networking(NAN) a.k.a Wi-Fi Aware in the foreseeable future?"
description = '''Is there any plan to support Neighbor Aware Networking(NAN) a.k.a Wi-Fi Aware in the foreseeable future?'''
date = "2017-07-19T08:34:00Z"
lastmod = "2017-07-19T09:53:00Z"
weight = 62886
keywords = [ "nan" ]
aliases = [ "/questions/62886" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Is there any plan to support Neighbor Aware Networking(NAN) a.k.a Wi-Fi Aware in the foreseeable future?](/questions/62886/is-there-any-plan-to-support-neighbor-aware-networkingnan-aka-wi-fi-aware-in-the-foreseeable-future)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62886-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62886-score" class="post-score" title="current number of votes">0</div><span id="post-62886-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any plan to support Neighbor Aware Networking(NAN) a.k.a Wi-Fi Aware in the foreseeable future?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nan" rel="tag" title="see questions tagged &#39;nan&#39;">nan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '17, 08:34</strong></p><img src="https://secure.gravatar.com/avatar/040822daebcc721fc61e466ed442c2f1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mokhsamsung&#39;s gravatar image" /><p><span>mokhsamsung</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mokhsamsung has no accepted answers">0%</span></p></div></div><div id="comments-container-62886" class="comments-container"></div><div id="comment-tools-62886" class="comment-tools"></div><div class="clear"></div><div id="comment-62886-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62893"></span>

<div id="answer-container-62893" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62893-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62893-score" class="post-score" title="current number of votes">1</div><span id="post-62893-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="mokhsamsung has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Protocol dissection is added to Wireshark when and if someone contributes code. I have not seen any communication indicating that someone is working to contribute a dissector for this protocol. You could raise an enhancement request in <a href="https://bugs.wireshark.org">bugzilla</a> providing links to protocol description and an example pcap trace.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jul '17, 09:53</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jul '17, 00:59</strong> </span></p></div></div><div id="comments-container-62893" class="comments-container"></div><div id="comment-tools-62893" class="comment-tools"></div><div class="clear"></div><div id="comment-62893-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

