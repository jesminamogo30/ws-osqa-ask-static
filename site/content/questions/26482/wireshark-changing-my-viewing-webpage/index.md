+++
type = "question"
title = "Wireshark changing my viewing webpage?"
description = '''When I access a specified site, http://zj.189.cn, well a Chinese-based site, I found the company name in all the pages is changed to its competitor. I first thought the site was hacked. But then I found it&#x27;s correct on my mobile phone. So it&#x27;s my computer&#x27;s problem. Sorry, the first software is wire...'''
date = "2013-10-28T09:27:00Z"
lastmod = "2013-10-30T08:41:00Z"
weight = 26482
keywords = [ "webpage", "change" ]
aliases = [ "/questions/26482" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark changing my viewing webpage?](/questions/26482/wireshark-changing-my-viewing-webpage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26482-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26482-score" class="post-score" title="current number of votes">0</div><span id="post-26482-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I access a specified site, <a href="http://zj.189.cn">http://zj.189.cn</a>, well a Chinese-based site, I found the company name in all the pages is changed to its competitor. I first thought the site was hacked. But then I found it's correct on my mobile phone. So it's my computer's problem. Sorry, the first software is wireshark. Now it come back normal. I don't know why, but it's changing the webpages. I reinstalled, and re-uninstalled, it's changed and returned normal.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-webpage" rel="tag" title="see questions tagged &#39;webpage&#39;">webpage</span> <span class="post-tag tag-link-change" rel="tag" title="see questions tagged &#39;change&#39;">change</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Oct '13, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/5977d2fd3a94fd059b662a7ac1ad2a96?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="liyuan_cn&#39;s gravatar image" /><p><span>liyuan_cn</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="liyuan_cn has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Oct '13, 12:43</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-26482" class="comments-container"></div><div id="comment-tools-26482" class="comment-tools"></div><div class="clear"></div><div id="comment-26482-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26546"></span>

<div id="answer-container-26546" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26546-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26546-score" class="post-score" title="current number of votes">0</div><span id="post-26546-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The fact that a page shows up differently on a PC than it does on your phone is completely normal. Many sites send a totally different page to a phone so that it can be properly formatted to the smaller screen.</p><p>As to why the competitor's name is showing up in the PC version, I can't say. I have far too little information to state that it has been hacked or anything else.<br />
</p><p>I am fairly certain it is not a Wireshark problem - Wireshark is showing you what it receives from the site. The fact that you are getting a different version on your phone means nothing.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Oct '13, 08:41</strong></p><img src="https://secure.gravatar.com/avatar/eb859ad26d92eb0902b45ba20a167917?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kpalmgren&#39;s gravatar image" /><p><span>kpalmgren</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kpalmgren has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-26546" class="comments-container"></div><div id="comment-tools-26546" class="comment-tools"></div><div class="clear"></div><div id="comment-26546-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

