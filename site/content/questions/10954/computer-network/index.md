+++
type = "question"
title = "[closed] computer network"
description = '''how to calculate rtt time to get the object from server?'''
date = "2012-05-12T00:33:00Z"
lastmod = "2012-05-12T00:33:00Z"
weight = 10954
keywords = [ "cn" ]
aliases = [ "/questions/10954" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] computer network](/questions/10954/computer-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10954-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10954-score" class="post-score" title="current number of votes">0</div><span id="post-10954-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to calculate rtt time to get the object from server?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cn" rel="tag" title="see questions tagged &#39;cn&#39;">cn</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 May '12, 00:33</strong></p><img src="https://secure.gravatar.com/avatar/ad200a3734c66a6146cf36d0edaa1561?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sanjit&#39;s gravatar image" /><p><span>sanjit</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sanjit has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>12 May '12, 02:57</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-10954" class="comments-container"></div><div id="comment-tools-10954" class="comment-tools"></div><div class="clear"></div><div id="comment-10954-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 12 May '12, 02:57

</div>

</div>

</div>

