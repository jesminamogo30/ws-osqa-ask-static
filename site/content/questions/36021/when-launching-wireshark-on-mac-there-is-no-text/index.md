+++
type = "question"
title = "When launching wireshark on mac there is no text"
description = '''When ever I start my wireshark on Mac OSX 10.9, I get wireshark with no text on it. Everything is blank. When I click where my interface should be I get a blank message. I have tried several wireshark versions and several xquartz versions. Any ideas? Please and thank you'''
date = "2014-09-05T04:09:00Z"
lastmod = "2014-09-12T04:00:00Z"
weight = 36021
keywords = [ "unknown", "xquartz", "wireshark", "blank" ]
aliases = [ "/questions/36021" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [When launching wireshark on mac there is no text](/questions/36021/when-launching-wireshark-on-mac-there-is-no-text)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36021-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36021-score" class="post-score" title="current number of votes">0</div><span id="post-36021-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When ever I start my wireshark on Mac OSX 10.9, I get wireshark with no text on it. Everything is blank. When I click where my interface should be I get a blank message. I have tried several wireshark versions and several xquartz versions. Any ideas?</p><p>Please and thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-unknown" rel="tag" title="see questions tagged &#39;unknown&#39;">unknown</span> <span class="post-tag tag-link-xquartz" rel="tag" title="see questions tagged &#39;xquartz&#39;">xquartz</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-blank" rel="tag" title="see questions tagged &#39;blank&#39;">blank</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Sep '14, 04:09</strong></p><img src="https://secure.gravatar.com/avatar/0047447019325a522f42197cf28bfdc2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MrTooTall&#39;s gravatar image" /><p><span>MrTooTall</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MrTooTall has no accepted answers">0%</span></p></div></div><div id="comments-container-36021" class="comments-container"><span id="36257"></span><div id="comment-36257" class="comment"><div id="post-36257-score" class="comment-score"></div><div class="comment-text"><p>Anyone? Please help me.</p></div><div id="comment-36257-info" class="comment-info"><span class="comment-age">(12 Sep '14, 04:00)</span> <span class="comment-user userinfo">MrTooTall</span></div></div></div><div id="comment-tools-36021" class="comment-tools"></div><div class="clear"></div><div id="comment-36021-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

