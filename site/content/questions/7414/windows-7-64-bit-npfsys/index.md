+++
type = "question"
title = "Windows 7 64 Bit (npf.sys)"
description = '''Hi, i try to capture the Win7 64 Bit Installation with the Repackaging Software Installshield. Now i have a problem with the npf.sys driver. it does not install with my new created msi package. How can i manually install the driver ? or how can i solve my problem ? Anyone an idea ? Greetings Marc '''
date = "2011-11-14T05:20:00Z"
lastmod = "2011-11-14T05:20:00Z"
weight = 7414
keywords = [ "not", "npf.sys", "installed" ]
aliases = [ "/questions/7414" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 7 64 Bit (npf.sys)](/questions/7414/windows-7-64-bit-npfsys)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7414-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7414-score" class="post-score" title="current number of votes">0</div><span id="post-7414-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>i try to capture the Win7 64 Bit Installation with the Repackaging Software Installshield.</p><p>Now i have a problem with the npf.sys driver. it does not install with my new created msi package. How can i manually install the driver ? or how can i solve my problem ?</p><p>Anyone an idea ?</p><p>Greetings Marc</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-npf.sys" rel="tag" title="see questions tagged &#39;npf.sys&#39;">npf.sys</span> <span class="post-tag tag-link-installed" rel="tag" title="see questions tagged &#39;installed&#39;">installed</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '11, 05:20</strong></p><img src="https://secure.gravatar.com/avatar/c773d8636497ff1cd2aa2751ecaf8543?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="senseless&#39;s gravatar image" /><p><span>senseless</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="senseless has no accepted answers">0%</span></p></div></div><div id="comments-container-7414" class="comments-container"></div><div id="comment-tools-7414" class="comment-tools"></div><div class="clear"></div><div id="comment-7414-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

