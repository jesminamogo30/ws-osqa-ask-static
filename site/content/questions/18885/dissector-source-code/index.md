+++
type = "question"
title = "Dissector source code"
description = '''If I write a dissector for my proprietary protocol and I give the dissectors to my customers do I have to make the source available?'''
date = "2013-02-26T11:12:00Z"
lastmod = "2013-02-26T11:23:00Z"
weight = 18885
keywords = [ "dissector", "license" ]
aliases = [ "/questions/18885" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Dissector source code](/questions/18885/dissector-source-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18885-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18885-score" class="post-score" title="current number of votes">1</div><span id="post-18885-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If I write a dissector for my proprietary protocol and I give the dissectors to my customers do I have to make the source available?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-license" rel="tag" title="see questions tagged &#39;license&#39;">license</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Feb '13, 11:12</strong></p><img src="https://secure.gravatar.com/avatar/f8e4486422661a127462a51c5ef6b555?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gmeloney&#39;s gravatar image" /><p><span>gmeloney</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gmeloney has no accepted answers">0%</span></p></div></div><div id="comments-container-18885" class="comments-container"></div><div id="comment-tools-18885" class="comment-tools"></div><div class="clear"></div><div id="comment-18885-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18887"></span>

<div id="answer-container-18887" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18887-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18887-score" class="post-score" title="current number of votes">1</div><span id="post-18887-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, as you are using or interfacing with code of Wireshark which is licensed according to GPL. So, you need to make your code available as well. See the following questions.</p><blockquote><p><code>http://ask.wireshark.org/questions/12371/wireshark-plugin-and-gpl-license</code><br />
</p><p><code>http://ask.wireshark.org/questions/15154/distributing-wireshark</code><br />
<code>http://ask.wireshark.org/questions/8798/commerical-use-of-wireshark</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Feb '13, 11:23</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-18887" class="comments-container"></div><div id="comment-tools-18887" class="comment-tools"></div><div class="clear"></div><div id="comment-18887-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

