+++
type = "question"
title = "Automate analysing packets from a pcap file(Live capture)"
description = '''Hi, I have some IP address space associated with my server. I have written a packet capture script to capture packets to that IP address space and it will dump those packets to a pcap file. I am now manually copying part of the pcap file to my computer and analyzing it. But I would like to automate ...'''
date = "2017-01-21T08:22:00Z"
lastmod = "2017-01-21T08:22:00Z"
weight = 58932
keywords = [ "graph", "pcap", "automate", "io", "script" ]
aliases = [ "/questions/58932" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Automate analysing packets from a pcap file(Live capture)](/questions/58932/automate-analysing-packets-from-a-pcap-filelive-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58932-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58932-score" class="post-score" title="current number of votes">1</div><span id="post-58932-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have some IP address space associated with my server. I have written a packet capture script to capture packets to that IP address space and it will dump those packets to a pcap file.</p><p>I am now manually copying part of the pcap file to my computer and analyzing it.</p><p>But I would like to automate analyzing this pcap file in live using wireshark and create an IO Graph for the live capture.Can someone guide me how to write a script to do the same?</p><p>Thanks in advance</p><p>Subin</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-automate" rel="tag" title="see questions tagged &#39;automate&#39;">automate</span> <span class="post-tag tag-link-io" rel="tag" title="see questions tagged &#39;io&#39;">io</span> <span class="post-tag tag-link-script" rel="tag" title="see questions tagged &#39;script&#39;">script</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '17, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/95e9674b7a67d58ada813e0c6bc38d84?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="subinjp&#39;s gravatar image" /><p><span>subinjp</span><br />
<span class="score" title="41 reputation points">41</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="subinjp has no accepted answers">0%</span></p></div></div><div id="comments-container-58932" class="comments-container"></div><div id="comment-tools-58932" class="comment-tools"></div><div class="clear"></div><div id="comment-58932-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

