+++
type = "question"
title = "Unable to add GeoIP files on OSX"
description = '''I am trying to a install the GeoIP files in wireshark 1.12.1 on OSX 10.9.5. I go to EDIT -&amp;gt; Preferences -&amp;gt; Name Resolution and I select edit next to GeoIP database directories: When the window opened the path area is empty. I click on NEW nothing happens except for the button becoming grayed o...'''
date = "2014-11-10T21:39:00Z"
lastmod = "2014-11-10T21:39:00Z"
weight = 37741
keywords = [ "geoip", "error", "osx" ]
aliases = [ "/questions/37741" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to add GeoIP files on OSX](/questions/37741/unable-to-add-geoip-files-on-osx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37741-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37741-score" class="post-score" title="current number of votes">0</div><span id="post-37741-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to a install the GeoIP files in wireshark 1.12.1 on OSX 10.9.5. I go to EDIT -&gt; Preferences -&gt; Name Resolution and I select edit next to GeoIP database directories: When the window opened the path area is empty. I click on NEW nothing happens except for the button becoming grayed out.</p><p>I checked out the wiki it provides paths for Linux and windows but not OSX.</p><p>Any suggestions?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-geoip" rel="tag" title="see questions tagged &#39;geoip&#39;">geoip</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span> <span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '14, 21:39</strong></p><img src="https://secure.gravatar.com/avatar/5e8eb5965aac554dd94d2d3fe9aa8dad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jeff%20in%20RTP&#39;s gravatar image" /><p><span>jeff in RTP</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jeff in RTP has no accepted answers">0%</span></p></div></div><div id="comments-container-37741" class="comments-container"></div><div id="comment-tools-37741" class="comment-tools"></div><div class="clear"></div><div id="comment-37741-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

