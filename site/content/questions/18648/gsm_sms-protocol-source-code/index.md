+++
type = "question"
title = "gsm_sms protocol source code"
description = '''Hi,  I would like to know how gsm_sms protocol is identified in a packet. I mean the low level interpretation of filter &quot;gsm_sms&quot;. I am trying to use this filter in jpcap and it does not support, hence i need to write the filter in java. Thanks for the reply.'''
date = "2013-02-14T23:48:00Z"
lastmod = "2013-02-14T23:48:00Z"
weight = 18648
keywords = [ "gsm_sms", "jpcap" ]
aliases = [ "/questions/18648" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [gsm\_sms protocol source code](/questions/18648/gsm_sms-protocol-source-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18648-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18648-score" class="post-score" title="current number of votes">0</div><span id="post-18648-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I would like to know how gsm_sms protocol is identified in a packet. I mean the low level interpretation of filter "gsm_sms". I am trying to use this filter in jpcap and it does not support, hence i need to write the filter in java.</p><p>Thanks for the reply.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gsm_sms" rel="tag" title="see questions tagged &#39;gsm_sms&#39;">gsm_sms</span> <span class="post-tag tag-link-jpcap" rel="tag" title="see questions tagged &#39;jpcap&#39;">jpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Feb '13, 23:48</strong></p><img src="https://secure.gravatar.com/avatar/e72c900f0d36147cbb9172392cd3d771?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srk&#39;s gravatar image" /><p><span>srk</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srk has no accepted answers">0%</span></p></div></div><div id="comments-container-18648" class="comments-container"></div><div id="comment-tools-18648" class="comment-tools"></div><div class="clear"></div><div id="comment-18648-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

