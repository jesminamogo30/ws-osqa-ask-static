+++
type = "question"
title = "Mechanism of filtering."
description = '''I am currently doing research on Security Metrics and trying to develop and implement some new metrics. Please suggest How and on what mechanism(logic) packet filtering is done.'''
date = "2014-06-17T21:13:00Z"
lastmod = "2014-06-18T09:45:00Z"
weight = 33916
keywords = [ "parameters" ]
aliases = [ "/questions/33916" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Mechanism of filtering.](/questions/33916/mechanism-of-filtering)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33916-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33916-score" class="post-score" title="current number of votes">0</div><span id="post-33916-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am currently doing research on Security Metrics and trying to develop and implement some new metrics. Please suggest How and on what mechanism(logic) packet filtering is done.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-parameters" rel="tag" title="see questions tagged &#39;parameters&#39;">parameters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jun '14, 21:13</strong></p><img src="https://secure.gravatar.com/avatar/044bd86525d4c5835c1cee1ab4e774c8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sweety%20divs&#39;s gravatar image" /><p><span>Sweety divs</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sweety divs has no accepted answers">0%</span></p></div></div><div id="comments-container-33916" class="comments-container"><span id="33930"></span><div id="comment-33930" class="comment"><div id="post-33930-score" class="comment-score"></div><div class="comment-text"><p>I'm sorry, but I don't understand what you are asking for. Can you please add more information?</p><p>When you say:</p><blockquote><p>trying to develop and implement <strong>some new metrics</strong></p></blockquote><p>what exactly do you mean by 'metrics'?</p><blockquote><p>How and on what mechanism(logic) packet filtering is done.</p></blockquote><p>Can you please add more to this? What are you particularly interested in?</p><ul><li>The internals of the dissection engine</li><li>the display filter syntax</li></ul></div><div id="comment-33930-info" class="comment-info"><span class="comment-age">(18 Jun '14, 09:45)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-33916" class="comment-tools"></div><div class="clear"></div><div id="comment-33916-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

