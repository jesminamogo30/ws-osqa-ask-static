+++
type = "question"
title = "Extracting data from MPEG Transport Stream to playback in VLC"
description = '''I am looking for a way to extract the data portion of a MPEG Transport Stream in a pcap file from the command line. The pcap file is very large 10 GB plus. I know how to extract the data from the Wireshark GUI. Follow UDP stream and use the save raw command. I just need a way to do that from the com...'''
date = "2015-07-29T13:40:00Z"
lastmod = "2015-07-29T13:40:00Z"
weight = 44607
keywords = [ "mpegts" ]
aliases = [ "/questions/44607" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Extracting data from MPEG Transport Stream to playback in VLC](/questions/44607/extracting-data-from-mpeg-transport-stream-to-playback-in-vlc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44607-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44607-score" class="post-score" title="current number of votes">0</div><span id="post-44607-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking for a way to extract the data portion of a MPEG Transport Stream in a pcap file from the command line. The pcap file is very large 10 GB plus.</p><p>I know how to extract the data from the Wireshark GUI. Follow UDP stream and use the save raw command. I just need a way to do that from the command line.</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mpegts" rel="tag" title="see questions tagged &#39;mpegts&#39;">mpegts</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jul '15, 13:40</strong></p><img src="https://secure.gravatar.com/avatar/862fdc2de62e268ebcead5bb6c4022af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Chuck%20Wanner&#39;s gravatar image" /><p><span>Chuck Wanner</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Chuck Wanner has no accepted answers">0%</span></p></div></div><div id="comments-container-44607" class="comments-container"></div><div id="comment-tools-44607" class="comment-tools"></div><div class="clear"></div><div id="comment-44607-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

