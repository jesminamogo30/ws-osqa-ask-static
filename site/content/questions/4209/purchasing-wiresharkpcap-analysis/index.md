+++
type = "question"
title = "Purchasing wireshark,Pcap analysis"
description = '''Hai, I am a network admin in an organisation having 15 servers and upto 350 client pc&#x27;s.Our network is totally unmanaged we have a few(4) l2 managed switches at the core levels We are facing slow network issues Can u analyse our network traffic captures and Tell us the tweaks.What will u be charging...'''
date = "2011-05-24T19:16:00Z"
lastmod = "2011-05-25T08:21:00Z"
weight = 4209
keywords = [ "pcap", "analysis" ]
aliases = [ "/questions/4209" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Purchasing wireshark,Pcap analysis](/questions/4209/purchasing-wiresharkpcap-analysis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4209-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4209-score" class="post-score" title="current number of votes">0</div><span id="post-4209-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hai, I am a network admin in an organisation having 15 servers and upto 350 client pc's.Our network is totally unmanaged we have a few(4) l2 managed switches at the core levels We are facing slow network issues Can u analyse our network traffic captures and Tell us the tweaks.What will u be charging us for and on what basis you are going to charge us.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 May '11, 19:16</strong></p><img src="https://secure.gravatar.com/avatar/b18ef69df90affac1f76127e75ef6ea3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sitaram&#39;s gravatar image" /><p><span>Sitaram</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sitaram has no accepted answers">0%</span></p></div></div><div id="comments-container-4209" class="comments-container"></div><div id="comment-tools-4209" class="comment-tools"></div><div class="clear"></div><div id="comment-4209-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4218"></span>

<div id="answer-container-4218" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4218-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4218-score" class="post-score" title="current number of votes">2</div><span id="post-4218-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is a community site, as such, people will help you with (small) issues voluntarily. No charge...</p><p>However, if you have major issues, you can hire someone to look at the problems in your network and analyze the capture files for you. I (amongst other people here) can help you with that. You can contact me by e-mail if you like. My e-mail address is in my user profile.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 May '11, 00:45</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-4218" class="comments-container"><span id="4232"></span><div id="comment-4232" class="comment"><div id="post-4232-score" class="comment-score">2</div><div class="comment-text"><p>SYNbit nailed it.</p><p>No capture files necessary. A big flat network with 15 servers and 350 clients will crawl because of broadcast traffic alone. You need at least 1 layer 3 device and probably a few VLANs.</p></div><div id="comment-4232-info" class="comment-info"><span class="comment-age">(25 May '11, 08:21)</span> <span class="comment-user userinfo">GeonJay</span></div></div></div><div id="comment-tools-4218" class="comment-tools"></div><div class="clear"></div><div id="comment-4218-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

