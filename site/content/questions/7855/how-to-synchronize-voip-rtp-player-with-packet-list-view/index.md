+++
type = "question"
title = "how to synchronize voip RTP player with packet list view?"
description = '''I do have some obvious errors in VOIP calls. Those are visible and sometimes only recognizable by listening (either in the RTP player or an external audio recording.  Is there a way to &quot;synchronize&quot; what I see/hear in the RTP player, with the actual packet list? Thanks Dan'''
date = "2011-12-08T15:42:00Z"
lastmod = "2011-12-08T15:42:00Z"
weight = 7855
keywords = [ "player", "rtp", "voip" ]
aliases = [ "/questions/7855" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to synchronize voip RTP player with packet list view?](/questions/7855/how-to-synchronize-voip-rtp-player-with-packet-list-view)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7855-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7855-score" class="post-score" title="current number of votes">0</div><span id="post-7855-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I do have some obvious errors in VOIP calls. Those are visible and sometimes only recognizable by listening (either in the RTP player or an external audio recording. Is there a way to "synchronize" what I see/hear in the RTP player, with the actual packet list? Thanks Dan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-player" rel="tag" title="see questions tagged &#39;player&#39;">player</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '11, 15:42</strong></p><img src="https://secure.gravatar.com/avatar/c1c349f269892c3b94a854a18a4d6b07?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="piuslor&#39;s gravatar image" /><p><span>piuslor</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="piuslor has one accepted answer">100%</span></p></div></div><div id="comments-container-7855" class="comments-container"></div><div id="comment-tools-7855" class="comment-tools"></div><div class="clear"></div><div id="comment-7855-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

