+++
type = "question"
title = "Issue with smb2 file info request"
description = '''Hello I have issue with storage delay a request(see attached trace) that get file size info get more than 1.5sec Looking on the trace i don&#x27;t see any delay form storage side but i do see what seems to be authentication issues , and what more abnormal is that the full request should be &#92;&#92;10.2.0.21&#92;da...'''
date = "2015-07-16T05:33:00Z"
lastmod = "2015-07-16T05:33:00Z"
weight = 44206
keywords = [ "smb2" ]
aliases = [ "/questions/44206" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Issue with smb2 file info request](/questions/44206/issue-with-smb2-file-info-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44206-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44206-score" class="post-score" title="current number of votes">0</div><span id="post-44206-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I have issue with storage delay a request(<a href="https://www.cloudshark.org/captures/9e5ad6da50f1">see attached trace</a>) that get file size info get more than 1.5sec</p><p>Looking on the trace i don't see any delay form storage side but i do see what seems to be authentication issues , and what more abnormal is that the full request should be <code>\\10.2.0.21\dalet\LSU\Proxy\A103979.mp4</code> and chopped to <code>smb.file \10.2.0.21\dalet</code> ,my question what could be that the request is passed chopped a behaviour we see on others servers.</p><p>So i would like if someone could try to explain and help to understand what i see on that trace that is wrong</p><p>Please advice</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-smb2" rel="tag" title="see questions tagged &#39;smb2&#39;">smb2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jul '15, 05:33</strong></p><img src="https://secure.gravatar.com/avatar/491b248bc5431fa4cfed4498e4633f51?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tbaror&#39;s gravatar image" /><p><span>tbaror</span><br />
<span class="score" title="10 reputation points">10</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tbaror has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Jul '15, 05:36</strong> </span></p></div></div><div id="comments-container-44206" class="comments-container"></div><div id="comment-tools-44206" class="comment-tools"></div><div class="clear"></div><div id="comment-44206-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

