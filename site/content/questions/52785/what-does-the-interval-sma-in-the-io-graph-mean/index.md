+++
type = "question"
title = "What does the interval sma in the I/O graph mean"
description = '''Hi, I am trying to understand the new I/O graph is wireshark 2.0.3. The smoothing section shows ### interval SMA, can someone please explain what that means? Thanks! Josh'''
date = "2016-05-19T12:44:00Z"
lastmod = "2016-05-19T16:08:00Z"
weight = 52785
keywords = [ "graph" ]
aliases = [ "/questions/52785" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What does the interval sma in the I/O graph mean](/questions/52785/what-does-the-interval-sma-in-the-io-graph-mean)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52785-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52785-score" class="post-score" title="current number of votes">0</div><span id="post-52785-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to understand the new I/O graph is wireshark 2.0.3. The smoothing section shows ### interval SMA, can someone please explain what that means?</p><p>Thanks!</p><p>Josh</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 May '16, 12:44</strong></p><img src="https://secure.gravatar.com/avatar/cddabebc9fed4bdb297994e5ab3040b3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="josh38jsa&#39;s gravatar image" /><p><span>josh38jsa</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="josh38jsa has no accepted answers">0%</span></p></div></div><div id="comments-container-52785" class="comments-container"></div><div id="comment-tools-52785" class="comment-tools"></div><div class="clear"></div><div id="comment-52785-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52796"></span>

<div id="answer-container-52796" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52796-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52796-score" class="post-score" title="current number of votes">0</div><span id="post-52796-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I just found it, its the simple moving average...I was trying to use it on data that was going all over the place in terms of very high and very low throughput...when I used an SMA of 1000, things looks very off...the lower the SMA the more like the actual data you will see.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 May '16, 16:08</strong></p><img src="https://secure.gravatar.com/avatar/cddabebc9fed4bdb297994e5ab3040b3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="josh38jsa&#39;s gravatar image" /><p><span>josh38jsa</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="josh38jsa has no accepted answers">0%</span></p></div></div><div id="comments-container-52796" class="comments-container"></div><div id="comment-tools-52796" class="comment-tools"></div><div class="clear"></div><div id="comment-52796-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

