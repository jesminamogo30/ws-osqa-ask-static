+++
type = "question"
title = "Wireshark 2.0 doesn&#x27;t support WLAN (802.11) capture?"
description = '''I&#x27;m using Wireshark 2.0 on Mac OS X (10.11.2) I want to use the &quot;monitor mode&quot; mentioned in this post(https://wiki.wireshark.org/CaptureSetup/WLAN#Turning_on_monitor_mode) However, I don&#x27;t find an option to set the &quot;monitor mode&quot;. Does anyone have ideas about this?'''
date = "2015-12-08T00:01:00Z"
lastmod = "2015-12-08T14:32:00Z"
weight = 48344
keywords = [ "wireless", "wifi" ]
aliases = [ "/questions/48344" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark 2.0 doesn't support WLAN (802.11) capture?](/questions/48344/wireshark-20-doesnt-support-wlan-80211-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48344-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48344-score" class="post-score" title="current number of votes">0</div><span id="post-48344-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using Wireshark 2.0 on Mac OS X (10.11.2)</p><p>I want to use the "monitor mode" mentioned in this post(<a href="https://wiki.wireshark.org/CaptureSetup/WLAN#Turning_on_monitor_mode)">https://wiki.wireshark.org/CaptureSetup/WLAN#Turning_on_monitor_mode)</a></p><p>However, I don't find an option to set the "monitor mode". Does anyone have ideas about this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '15, 00:01</strong></p><img src="https://secure.gravatar.com/avatar/6ea2bbebf39d8f4d9ba654471289d576?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hanfei%20Sun&#39;s gravatar image" /><p><span>Hanfei Sun</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hanfei Sun has no accepted answers">0%</span></p></div></div><div id="comments-container-48344" class="comments-container"></div><div id="comment-tools-48344" class="comment-tools"></div><div class="clear"></div><div id="comment-48344-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="48345"></span>

<div id="answer-container-48345" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48345-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48345-score" class="post-score" title="current number of votes">1</div><span id="post-48345-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Hanfei Sun has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you scrolled to the right side of the capture dialog?</p><p>As shown here: <a href="https://ask.wireshark.org/questions/42897/how-do-i-turn-on-monitor-mode-in-mac-os-x-with-wireshark-v199">https://ask.wireshark.org/questions/42897/how-do-i-turn-on-monitor-mode-in-mac-os-x-with-wireshark-v199</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Dec '15, 01:43</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Dec '15, 01:43</strong> </span></p></div></div><div id="comments-container-48345" class="comments-container"></div><div id="comment-tools-48345" class="comment-tools"></div><div class="clear"></div><div id="comment-48345-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="48346"></span>

<div id="answer-container-48346" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48346-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48346-score" class="post-score" title="current number of votes">0</div><span id="post-48346-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi,</p><p>I had the same problem, setting the checkbox "monitor mode" had no effekt on my ubuntu 14.04-system. So I tried the following:</p><p>Install aircrack-ng</p><pre><code># sudo apt-get install aircrack-ng</code></pre><p>Start airmon to see the WLAN-Interfaces</p><pre><code># sudo airmon-ng</code></pre><p>Start monitor mode on interface wlan0</p><pre><code># sudo airmon-ng start wlan0</code></pre><p>A new interface "mon0" appears. You can capture on this interface with Radio-Information like RSSI, Channel and Transmit-Datarate. You can apply this information as colums in the packet-list.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Dec '15, 01:59</strong></p><img src="https://secure.gravatar.com/avatar/5df15cb7413a11a5897331507b8d9631?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Uri&#39;s gravatar image" /><p><span>Uri</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Uri has no accepted answers">0%</span></p></div></div><div id="comments-container-48346" class="comments-container"><span id="48367"></span><div id="comment-48367" class="comment"><div id="post-48367-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I had the same problem, setting the checkbox "monitor mode" had no effekt on my ubuntu 14.04-system.</p></blockquote><p>Different problem - the low-level mechanism used for monitor mode is very different on Linux (which you're using) and OS X (which the person who asked the question is using).</p></div><div id="comment-48367-info" class="comment-info"><span class="comment-age">(08 Dec '15, 14:32)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-48346" class="comment-tools"></div><div class="clear"></div><div id="comment-48346-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

