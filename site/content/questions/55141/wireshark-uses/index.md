+++
type = "question"
title = "Wireshark uses"
description = '''What is the need for the wireshark software for decoder software?'''
date = "2016-08-27T08:41:00Z"
lastmod = "2016-08-29T08:25:00Z"
weight = 55141
keywords = [ "wireshark" ]
aliases = [ "/questions/55141" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark uses](/questions/55141/wireshark-uses)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55141-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55141-score" class="post-score" title="current number of votes">0</div><span id="post-55141-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What is the need for the wireshark software for decoder software?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Aug '16, 08:41</strong></p><img src="https://secure.gravatar.com/avatar/7bd196acc985677f19f6efd9e587eeed?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AbdulR&#39;s gravatar image" /><p><span>AbdulR</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AbdulR has no accepted answers">0%</span></p></div></div><div id="comments-container-55141" class="comments-container"><span id="55156"></span><div id="comment-55156" class="comment"><div id="post-55156-score" class="comment-score"></div><div class="comment-text"><p>Some more background information would help to understand the question.</p></div><div id="comment-55156-info" class="comment-info"><span class="comment-age">(29 Aug '16, 02:21)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-55141" class="comment-tools"></div><div class="clear"></div><div id="comment-55141-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55166"></span>

<div id="answer-container-55166" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55166-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55166-score" class="post-score" title="current number of votes">0</div><span id="post-55166-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The <a href="https://en.wikipedia.org/wiki/Wireshark">Wireshark Wikipedia article</a> summarizes what Wireshark is used for quite well:</p><p><em>It is used for network troubleshooting, analysis, software and communications protocol development, and education.</em></p><p>See also <a href="https://www.wireshark.org/about.html">Wireshark's About</a> page.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Aug '16, 08:25</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-55166" class="comments-container"></div><div id="comment-tools-55166" class="comment-tools"></div><div class="clear"></div><div id="comment-55166-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

