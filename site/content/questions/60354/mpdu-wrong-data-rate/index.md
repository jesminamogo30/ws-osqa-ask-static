+++
type = "question"
title = "MPDU wrong data rate"
description = '''Hello, i have a question, why are MPDU data frames are shown with different data rates? I think the data rate from first ID to last ID must be the same, also the bandwith is shown wrong in the middle of the frames, there must be 20 MHz not 40 MHz? Is there a bug? I have compiled Wireshark on Ubuntu ...'''
date = "2017-03-27T05:27:00Z"
lastmod = "2017-03-27T05:27:00Z"
weight = 60354
keywords = [ "ampdu" ]
aliases = [ "/questions/60354" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [MPDU wrong data rate](/questions/60354/mpdu-wrong-data-rate)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60354-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60354-score" class="post-score" title="current number of votes">0</div><span id="post-60354-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>i have a question, why are MPDU data frames are shown with different data rates? I think the data rate from first ID to last ID must be the same, also the bandwith is shown wrong in the middle of the frames, there must be 20 MHz not 40 MHz? Is there a bug? I have compiled Wireshark on Ubuntu Qt 5.6.1.</p><p>All data packets of MPDU: <img src="https://osqa-ask.wireshark.org/upfiles/All_data_packets_MPDU.png" alt="alt text" /></p><p>Last Frame of MPDU: <img src="https://osqa-ask.wireshark.org/upfiles/Last_Frame_MPDU.png" alt="alt text" /></p><p>Middle Frame of MPDU: <img src="https://osqa-ask.wireshark.org/upfiles/Middle_Frame_MPDU.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ampdu" rel="tag" title="see questions tagged &#39;ampdu&#39;">ampdu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Mar '17, 05:27</strong></p><img src="https://secure.gravatar.com/avatar/a2f4985698458696d92a0f17460c70a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hyrrican&#39;s gravatar image" /><p><span>Hyrrican</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hyrrican has no accepted answers">0%</span></p></img></div></div><div id="comments-container-60354" class="comments-container"></div><div id="comment-tools-60354" class="comment-tools"></div><div class="clear"></div><div id="comment-60354-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

