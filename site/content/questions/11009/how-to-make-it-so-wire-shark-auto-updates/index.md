+++
type = "question"
title = "how to make it so wire-shark auto updates"
description = '''how to make it so wire-shark auto updates'''
date = "2012-05-15T18:21:00Z"
lastmod = "2012-05-15T23:55:00Z"
weight = 11009
keywords = [ "wireshark" ]
aliases = [ "/questions/11009" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [how to make it so wire-shark auto updates](/questions/11009/how-to-make-it-so-wire-shark-auto-updates)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11009-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11009-score" class="post-score" title="current number of votes">0</div><span id="post-11009-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to make it so wire-shark auto updates</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '12, 18:21</strong></p><img src="https://secure.gravatar.com/avatar/b2a4006b4a0252f8be292c57acde97ff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wiresharkhelpers&#39;s gravatar image" /><p><span>wiresharkhel...</span><br />
<span class="score" title="30 reputation points">30</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wiresharkhelpers has no accepted answers">0%</span></p></div></div><div id="comments-container-11009" class="comments-container"></div><div id="comment-tools-11009" class="comment-tools"></div><div class="clear"></div><div id="comment-11009-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="11013"></span>

<div id="answer-container-11013" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11013-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11013-score" class="post-score" title="current number of votes">0</div><span id="post-11013-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="wiresharkhelpers has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If by "make it so Wireshark auto-updates" you mean "make it so that when new versions of Wireshark are released, they get installed automatically", you do it by installing Wireshark as supplied by a Linux distribution or *BSD that provides an update mechanism. <a href="http://Wireshark.org">Wireshark.org</a> doesn't currently offer an automatic or semi-automatic update mechanism for the versions you can download from it (Windows and OS X.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '12, 18:59</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-11013" class="comments-container"><span id="11018"></span><div id="comment-11018" class="comment"><div id="post-11018-score" class="comment-score"></div><div class="comment-text"><p>guess few software really do in 2012 -- i don't think even chrome can -- <a href="http://support.google.com/chrome/bin/answer.py?hl=en&amp;answer=95414">http://support.google.com/chrome/bin/answer.py?hl=en&amp;answer=95414</a></p></div><div id="comment-11018-info" class="comment-info"><span class="comment-age">(15 May '12, 22:55)</span> <span class="comment-user userinfo">wiresharkhel...</span></div></div></div><div id="comment-tools-11013" class="comment-tools"></div><div class="clear"></div><div id="comment-11013-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="11022"></span>

<div id="answer-container-11022" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11022-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11022-score" class="post-score" title="current number of votes">0</div><span id="post-11022-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No-one has volunteered to write the code and set up the infrastructure to support that yet. The updater idea is listed as item 11 in the general section of the <a href="http://wiki.wireshark.org/WishList">Wishlist</a>.</p><p>See the <a href="http://www.wireshark.org/develop.html">Get Involved</a> page on the website for details on how you can contribute to the project. If you don't want to write the code yourself, you may be able to sponsor someone else to do the work, if so send an email to the developers mailing list.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '12, 23:55</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-11022" class="comments-container"></div><div id="comment-tools-11022" class="comment-tools"></div><div class="clear"></div><div id="comment-11022-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

