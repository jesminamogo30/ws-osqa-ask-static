+++
type = "question"
title = "negative times in merged capture"
description = '''I have several voip captures which I merged using the Merge Packets Chronologically option, but when I open the merged file, and then go to telephony/voip calls I see negative start and stop times. What does this mean? '''
date = "2011-12-08T07:39:00Z"
lastmod = "2011-12-08T07:39:00Z"
weight = 7847
keywords = [ "negativetimes", "mergepcap" ]
aliases = [ "/questions/7847" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [negative times in merged capture](/questions/7847/negative-times-in-merged-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7847-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7847-score" class="post-score" title="current number of votes">0</div><span id="post-7847-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have several voip captures which I merged using the Merge Packets Chronologically option, but when I open the merged file, and then go to telephony/voip calls I see negative start and stop times. What does this mean?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-negativetimes" rel="tag" title="see questions tagged &#39;negativetimes&#39;">negativetimes</span> <span class="post-tag tag-link-mergepcap" rel="tag" title="see questions tagged &#39;mergepcap&#39;">mergepcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '11, 07:39</strong></p><img src="https://secure.gravatar.com/avatar/d8eb37ad8f02e6f71516f51844ccafc7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Joe%20Garry&#39;s gravatar image" /><p><span>Joe Garry</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Joe Garry has no accepted answers">0%</span></p></div></div><div id="comments-container-7847" class="comments-container"></div><div id="comment-tools-7847" class="comment-tools"></div><div class="clear"></div><div id="comment-7847-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

