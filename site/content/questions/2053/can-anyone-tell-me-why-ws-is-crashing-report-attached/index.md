+++
type = "question"
title = "Can anyone tell me why WS is crashing - report attached."
description = '''Problem signature:  Problem Event Name: APPCRASH  Application Name: wireshark.exe  Application Version: 1.4.3.35482  Application Timestamp: 4d2cad77  Fault Module Name: libglib-2.0-0.dll  Fault Module Version: 2.22.4.0  Fault Module Timestamp: 4b6f546c  Exception Code: 40000015  Exception Offset: 00...'''
date = "2011-01-31T17:08:00Z"
lastmod = "2011-02-01T09:18:00Z"
weight = 2053
keywords = [ "crash", "appcrash", "ws" ]
aliases = [ "/questions/2053" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can anyone tell me why WS is crashing - report attached.](/questions/2053/can-anyone-tell-me-why-ws-is-crashing-report-attached)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2053-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2053-score" class="post-score" title="current number of votes">0</div><span id="post-2053-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Problem signature: Problem Event Name: APPCRASH Application Name: wireshark.exe Application Version: 1.4.3.35482 Application Timestamp: 4d2cad77 Fault Module Name: libglib-2.0-0.dll Fault Module Version: 2.22.4.0 Fault Module Timestamp: 4b6f546c Exception Code: 40000015 Exception Offset: 000000000004e68a OS Version: 6.0.6001.2.1.0.400.8 Locale ID: 1033 Additional Information 1: 96d7 Additional Information 2: 3471587d5bc0fe617aeaaca56aa1e394 Additional Information 3: 53f3 Additional Information 4: caa04b5b6781523c776247284e4299bd</p><p>Read our privacy statement: http://go.microsoft.com/fwlink/?linkid=50163&amp;clcid=0x0409</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span> <span class="post-tag tag-link-appcrash" rel="tag" title="see questions tagged &#39;appcrash&#39;">appcrash</span> <span class="post-tag tag-link-ws" rel="tag" title="see questions tagged &#39;ws&#39;">ws</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jan '11, 17:08</strong></p><img src="https://secure.gravatar.com/avatar/6cb8cb0b369c36e17ad38d847f2fc8bd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Edincalif&#39;s gravatar image" /><p><span>Edincalif</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Edincalif has no accepted answers">0%</span></p></div></div><div id="comments-container-2053" class="comments-container"><span id="2054"></span><div id="comment-2054" class="comment"><div id="post-2054-score" class="comment-score"></div><div class="comment-text"><p>When do you get the crash ? when reading a particular capture file ? Is the file a large file ?</p><p>If the crash is repeatable (such as being caused by a particular capture file) please file a report at bugs.wireshark.org and attach the capture file. You can mark the attachment private if needed so that only the core developers will have access.</p><p>(The crash info doesn't really provide much useful information).</p></div><div id="comment-2054-info" class="comment-info"><span class="comment-age">(31 Jan '11, 17:23)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="2072"></span><div id="comment-2072" class="comment"><div id="post-2072-score" class="comment-score"></div><div class="comment-text"><p>Nothing special as far as I know. I can make it happen but sometimes the sequence fails and sometimes it does not. Could it be related to useing a 10GB NIC? someone told me they were having problems keeping up with this data stream. He was getting packets now showing up though, not crashes Here is another one, this looks like a null pointer.</p><p>Problem signature: Problem Event Name: APPCRASH Application Name: iperf.exe Application Version: 0.0.0.0 Application Timestamp: 3e6fae4a Fault Module Name: ntdll.dll Fault Module Version: 6.0.6001.18000 Fault Module Timestamp: 4791a783 Exception Code: c0000005 Exception Offset: 00060267 OS Version: 6.0.6001.2.1.0.400.8 Locale ID: 1033 Additional Information 1: fd00 Additional Information 2: ea6f5fe8924aaa756324d57f87834160 Additional Information 3: fd00 Additional Information 4: ea6f5fe8924aaa756324d57f87834160 Read our privacy statement: http://go.microsoft.com/fwlink/?linkid=50163&amp;clcid=0x0409</p></div><div id="comment-2072-info" class="comment-info"><span class="comment-age">(01 Feb '11, 09:18)</span> <span class="comment-user userinfo">Edincalif</span></div></div></div><div id="comment-tools-2053" class="comment-tools"></div><div class="clear"></div><div id="comment-2053-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

