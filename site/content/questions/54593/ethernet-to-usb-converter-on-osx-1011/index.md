+++
type = "question"
title = "Ethernet to USB converter on OSX 10.11"
description = '''I am using an ethernet to USB converter on OSX 10.11 with the latest wireshark but there is no USB option available in the menu, how can I read this data?'''
date = "2016-08-04T23:40:00Z"
lastmod = "2016-08-05T00:46:00Z"
weight = 54593
keywords = [ "macosx", "usb" ]
aliases = [ "/questions/54593" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Ethernet to USB converter on OSX 10.11](/questions/54593/ethernet-to-usb-converter-on-osx-1011)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54593-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54593-score" class="post-score" title="current number of votes">0</div><span id="post-54593-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using an ethernet to USB converter on OSX 10.11 with the latest wireshark but there is no USB option available in the menu, how can I read this data?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Aug '16, 23:40</strong></p><img src="https://secure.gravatar.com/avatar/94e15da82d56dbc37f290a43f8f1e887?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="theduduk&#39;s gravatar image" /><p><span>theduduk</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="theduduk has no accepted answers">0%</span></p></div></div><div id="comments-container-54593" class="comments-container"></div><div id="comment-tools-54593" class="comment-tools"></div><div class="clear"></div><div id="comment-54593-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54597"></span>

<div id="answer-container-54597" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54597-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54597-score" class="post-score" title="current number of votes">1</div><span id="post-54597-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>An "Ethernet to USB converter" is usually called a "USB Ethernet adapter". A USB Ethernet adapter will appear in most if not all OSes, including OS X, the same way any other Ethernet adapter will show up; there's nothing special about it being a USB adapter.</p><p>So just look for something named <code>en1</code> or <code>en2</code> or <code>en3</code> or <code>en4</code> or something such as that.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Aug '16, 00:46</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-54597" class="comments-container"></div><div id="comment-tools-54597" class="comment-tools"></div><div class="clear"></div><div id="comment-54597-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

