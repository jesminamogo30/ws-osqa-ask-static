+++
type = "question"
title = "Erro Message When Starting a Capture Using a WiFi (WLAN) connection"
description = '''Message received when trying to monitor a network. The capture session could not be initiated (failed to set hardware filter to promiscuous mode). Please check that &quot;&#92;Device&#92;NPF_{37AEC650-717D-42BF-AB23-4DFA1B1B9748}&quot; is the proper interface.'''
date = "2012-05-07T09:17:00Z"
lastmod = "2012-05-07T09:27:00Z"
weight = 10740
keywords = [ "message", "wifi", "for", "error" ]
aliases = [ "/questions/10740" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Erro Message When Starting a Capture Using a WiFi (WLAN) connection](/questions/10740/erro-message-when-starting-a-capture-using-a-wifi-wlan-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10740-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10740-score" class="post-score" title="current number of votes">0</div><span id="post-10740-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Message received when trying to monitor a network.</p><p>The capture session could not be initiated (failed to set hardware filter to promiscuous mode).</p><p>Please check that "\Device\NPF_{37AEC650-717D-42BF-AB23-4DFA1B1B9748}" is the proper interface.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-message" rel="tag" title="see questions tagged &#39;message&#39;">message</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-for" rel="tag" title="see questions tagged &#39;for&#39;">for</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 May '12, 09:17</strong></p><img src="https://secure.gravatar.com/avatar/f661de45733e702d850cdf97d07025e4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="screenburner&#39;s gravatar image" /><p><span>screenburner</span><br />
<span class="score" title="-1 reputation points">-1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="screenburner has no accepted answers">0%</span></p></div></div><div id="comments-container-10740" class="comments-container"></div><div id="comment-tools-10740" class="comment-tools"></div><div class="clear"></div><div id="comment-10740-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10741"></span>

<div id="answer-container-10741" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10741-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10741-score" class="post-score" title="current number of votes">0</div><span id="post-10741-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please check this question, with the same problem:</p><p><a href="http://ask.wireshark.org/questions/2647/failed-to-set-hardware-filter-to-promiscuous-mode-error-when-capture-attempted-on-a-wireless-adapter-on-windows">http://ask.wireshark.org/questions/2647/failed-to-set-hardware-filter-to-promiscuous-mode-error-when-capture-attempted-on-a-wireless-adapter-on-windows</a></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 May '12, 09:27</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-10741" class="comments-container"></div><div id="comment-tools-10741" class="comment-tools"></div><div class="clear"></div><div id="comment-10741-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

