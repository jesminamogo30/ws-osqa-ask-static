+++
type = "question"
title = "Home computer"
description = '''My roomate just announced he installed wireshark on our home network. He claims &quot;for security&quot; but from what? Anyways, is there a way to block him from reading my network traffic if we are on the same router? We have Comcast cable if that helps. What can I do o protect my privacy outside of asking h...'''
date = "2011-08-09T19:13:00Z"
lastmod = "2011-08-10T22:10:00Z"
weight = 5606
keywords = [ "encryption", "security" ]
aliases = [ "/questions/5606" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Home computer](/questions/5606/home-computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5606-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5606-score" class="post-score" title="current number of votes">0</div><span id="post-5606-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My roomate just announced he installed wireshark on our home network. He claims "for security" but from what? Anyways, is there a way to block him from reading my network traffic if we are on the same router? We have Comcast cable if that helps. What can I do o protect my privacy outside of asking him to remove the software?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span> <span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Aug '11, 19:13</strong></p><img src="https://secure.gravatar.com/avatar/06218536d955e0dd1e12a00b81b6f222?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="loki6279&#39;s gravatar image" /><p><span>loki6279</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="loki6279 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Aug '11, 20:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-5606" class="comments-container"><span id="5639"></span><div id="comment-5639" class="comment"><div id="post-5639-score" class="comment-score"></div><div class="comment-text"><p><strong>More details:</strong> Roomate is very computer savvy, we are both using wired connections to cable router, both running windows. Hes removed it from what he claims as of today (8/10) because He heard that "Comcast (ISP) will cancel service if they detect a packet sniffer on their network."</p></div><div id="comment-5639-info" class="comment-info"><span class="comment-age">(10 Aug '11, 20:42)</span> <span class="comment-user userinfo">loki6279</span></div></div><span id="5641"></span><div id="comment-5641" class="comment"><div id="post-5641-score" class="comment-score"></div><div class="comment-text"><p>Well, good for you! :)</p><p>In this case, Comcast would only suspend or terminate your service if you use Wireshark maliciously (e.g., sniffing for passwords). As it turns out, I just learned that I've violated <a href="http://www.comcast.com/Corporate/Customers/Policies/HighSpeedInternetAUP.html">Comcast's AUP</a> (easy to do if you're a power user), and I'm obligated to notify them so that they can close my account (<em>right...</em>).</p><p>I'm not sure how they'd detect Wireshark since it's a passive application. Someone would have to report you in order for Comcast to act.</p></div><div id="comment-5641-info" class="comment-info"><span class="comment-age">(10 Aug '11, 22:10)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-5606" class="comment-tools"></div><div class="clear"></div><div id="comment-5606-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="5608"></span>

<div id="answer-container-5608" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5608-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5608-score" class="post-score" title="current number of votes">2</div><span id="post-5608-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>First of all, "security reasons" can be that he wants to check if his own computer is communicating in ways he didn't authorize - meaning, checking if a trojan horse or botnet has taken over his PC without him knowing. It has to be said that very often those bad boy programs also do prohibit Wireshark to see what they're doing, so running it on an possibly infected PC doesn't always help.</p><p>If you're on one of these usual home routers it depends how you connect to it. If you both connect over WiFi he might be able to capture all data (unless he's running Windows and without an AirPCAP adapter, in which case he'll only see his own traffic). If you have a wired setup you'll all be connected to the integrated switch within the router, which means that your traffic is separated from his and he can't record anything you do except (mostly harmless) broadcast frames.</p><p>What you could do is make sure that you only login to important services using SSL encryption, meaning HTTPS etc. That way he might be able to record, but not able to decode.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Aug '11, 20:23</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-5608" class="comments-container"><span id="5610"></span><div id="comment-5610" class="comment"><div id="post-5610-score" class="comment-score"></div><div class="comment-text"><p>I don't know if I'd call this example a "security reason" because by that time, security might already have been breached. Wireshark would then be used only as a troubleshooting / post-analysis tool.</p><p>If he wants to secure his computer from viruses/worms and attacks (which might be what he meant), the obvious is to use anti-virus and firewall software.</p></div><div id="comment-5610-info" class="comment-info"><span class="comment-age">(09 Aug '11, 20:48)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-5608" class="comment-tools"></div><div class="clear"></div><div id="comment-5608-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="5611"></span>

<div id="answer-container-5611" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5611-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5611-score" class="post-score" title="current number of votes">0</div><span id="post-5611-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Depends on how savvy your roommate is. If your household is connected to the Internet via a switch, as is common these days, and if he has installed wireshark only on his own PC, then he will only be able to monitor his own traffic.</p><p>If, on the other hand, he has managed to insert a network tap or an older-style hub device into the path to the Internet, then he may be able to monitor all household traffic. This would require a higher than average level of network savvy on the part of your roommate, but it would not be particularly difficult.</p><p>If the latter, then you should prevail upon your roommate to remove wireshark and stop any monitoring. Short of that, you would have to set up some sort of encrypted tunnel (VPN), or provision a separate physical connection for your own traffic to the Internet.</p><p>The above assumes wired connections within the household. If you are using wireless, then you should also be using WPA2 encryption for the wireless connections themselves. (Note that WEP or WPA encryption are insufficient). This is simply good practice, and goes beyond anything your roommate might be doing. If your wireless connections are not encrypted in the first place, then you should also be worried about what your neighbors may be monitoring.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Aug '11, 20:58</strong></p><img src="https://secure.gravatar.com/avatar/b260fb38b621169269b5030f1ed6b766?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="griff&#39;s gravatar image" /><p><span>griff</span><br />
<span class="score" title="361 reputation points">361</span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="griff has 2 accepted answers">10%</span></p></div></div><div id="comments-container-5611" class="comments-container"></div><div id="comment-tools-5611" class="comment-tools"></div><div class="clear"></div><div id="comment-5611-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

