+++
type = "question"
title = "H.264 CBP level 1.3"
description = '''Wireshark seems to show a max bit rate of 2Mbps with CBP profile level 1.3, whereas it should be 768kbps as per standard. Did anyone else find the same?'''
date = "2012-05-09T17:04:00Z"
lastmod = "2012-05-10T22:22:00Z"
weight = 10859
keywords = [ "h.264", "sdp", "rtp" ]
aliases = [ "/questions/10859" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [H.264 CBP level 1.3](/questions/10859/h264-cbp-level-13)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10859-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10859-score" class="post-score" title="current number of votes">0</div><span id="post-10859-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark seems to show a max bit rate of 2Mbps with CBP profile level 1.3, whereas it should be 768kbps as per standard. Did anyone else find the same?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-h.264" rel="tag" title="see questions tagged &#39;h.264&#39;">h.264</span> <span class="post-tag tag-link-sdp" rel="tag" title="see questions tagged &#39;sdp&#39;">sdp</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '12, 17:04</strong></p><img src="https://secure.gravatar.com/avatar/1e5ba80037e29a8e5a58235b24f2447f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob3280&#39;s gravatar image" /><p><span>Bob3280</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob3280 has no accepted answers">0%</span></p></div></div><div id="comments-container-10859" class="comments-container"></div><div id="comment-tools-10859" class="comment-tools"></div><div class="clear"></div><div id="comment-10859-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10916"></span>

<div id="answer-container-10916" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10916-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10916-score" class="post-score" title="current number of votes">0</div><span id="post-10916-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Bob3280 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's a bug, a fix committed revision 42559.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 May '12, 22:22</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-10916" class="comments-container"></div><div id="comment-tools-10916" class="comment-tools"></div><div class="clear"></div><div id="comment-10916-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

