+++
type = "question"
title = "Ubuntu 12.04 monitoring error"
description = '''after I set up my group and permissions i tried running wireshark in monitoring mode. I checked the box next to monitoring mode in the preferences hit ok then apply then I revived an error box that said  Can&#x27;t open preferences file &quot;/home/reaper/.wireshark/preferences&quot;: Permission denied Then i had ...'''
date = "2013-01-27T00:12:00Z"
lastmod = "2013-01-29T19:23:00Z"
weight = 17976
keywords = [ "monior-mode", "error" ]
aliases = [ "/questions/17976" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Ubuntu 12.04 monitoring error](/questions/17976/ubuntu-1204-monitoring-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17976-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17976-score" class="post-score" title="current number of votes">0</div><span id="post-17976-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>after I set up my group and permissions i tried running wireshark in monitoring mode. I checked the box next to monitoring mode in the preferences hit ok then apply then I revived an error box that said Can't open preferences file "/home/reaper/.wireshark/preferences": Permission denied</p><p>Then i had to reset my connection because I wasn't able to surf the web afterwards. Any ideas how to fix this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-monior-mode" rel="tag" title="see questions tagged &#39;monior-mode&#39;">monior-mode</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '13, 00:12</strong></p><img src="https://secure.gravatar.com/avatar/94dc977e07ef128fbaa5b531a96dc724?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bigreaper&#39;s gravatar image" /><p><span>Bigreaper</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bigreaper has no accepted answers">0%</span></p></div></div><div id="comments-container-17976" class="comments-container"></div><div id="comment-tools-17976" class="comment-tools"></div><div class="clear"></div><div id="comment-17976-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18001"></span>

<div id="answer-container-18001" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18001-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18001-score" class="post-score" title="current number of votes">0</div><span id="post-18001-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yos should probably check the permissions on ""/home/reaper/.wireshark/preferences": Permission denied" That you lost your connection might be due to the card being set into monitoring mode by Wireshark and not reset when it couldn't read the "/home/reaper/.wireshark/preferences" which is a bug I suppose.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jan '13, 08:34</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-18001" class="comments-container"><span id="18068"></span><div id="comment-18068" class="comment"><div id="post-18068-score" class="comment-score"></div><div class="comment-text"><p>I don’t think there is a preferences file in the .wireshark folder I checked and all I see is recent and recent_common</p></div><div id="comment-18068-info" class="comment-info"><span class="comment-age">(29 Jan '13, 19:23)</span> <span class="comment-user userinfo">Bigreaper</span></div></div></div><div id="comment-tools-18001" class="comment-tools"></div><div class="clear"></div><div id="comment-18001-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

