+++
type = "question"
title = "I am looking for a Capture file of 802.1X handshake and EAP Can anyone help??"
description = '''I am looking for a Capture file of 802.1X handshake and EAP Can anyone help?'''
date = "2011-07-15T15:59:00Z"
lastmod = "2011-07-15T22:23:00Z"
weight = 5065
keywords = [ "security", "radius", "help", "eapol", "802.1x" ]
aliases = [ "/questions/5065" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [I am looking for a Capture file of 802.1X handshake and EAP Can anyone help??](/questions/5065/i-am-looking-for-a-capture-file-of-8021x-handshake-and-eap-can-anyone-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5065-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5065-score" class="post-score" title="current number of votes">0</div><span id="post-5065-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking for a Capture file of 802.1X handshake and EAP Can anyone help?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-radius" rel="tag" title="see questions tagged &#39;radius&#39;">radius</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span> <span class="post-tag tag-link-eapol" rel="tag" title="see questions tagged &#39;eapol&#39;">eapol</span> <span class="post-tag tag-link-802.1x" rel="tag" title="see questions tagged &#39;802.1x&#39;">802.1x</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jul '11, 15:59</strong></p><img src="https://secure.gravatar.com/avatar/b2ab100f04140997fe113b7878c664a6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sjohnson&#39;s gravatar image" /><p><span>sjohnson</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sjohnson has no accepted answers">0%</span></p></div></div><div id="comments-container-5065" class="comments-container"></div><div id="comment-tools-5065" class="comment-tools"></div><div class="clear"></div><div id="comment-5065-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5068"></span>

<div id="answer-container-5068" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5068-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5068-score" class="post-score" title="current number of votes">0</div><span id="post-5068-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Here are places to look for sample capture files:<br />
<a href="http://wiki.wireshark.org/SampleCaptures">http://wiki.wireshark.org/SampleCaptures</a><br />
<a href="https://www.openpacket.org/">https://www.openpacket.org/</a><br />
<a href="http://packetlife.net/captures/">http://packetlife.net/captures/</a><br />
<a href="http://www.pcapr.net/browse/protos">http://www.pcapr.net/browse/protos</a><br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jul '11, 22:23</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span> </br></br></p></div></div><div id="comments-container-5068" class="comments-container"></div><div id="comment-tools-5068" class="comment-tools"></div><div class="clear"></div><div id="comment-5068-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

