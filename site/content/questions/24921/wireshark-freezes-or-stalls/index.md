+++
type = "question"
title = "Wireshark freezes or stalls"
description = '''Does anyone know why the latest version (1.10.2) of wireshark did not work right? The software would function fine until I click to capture. Once it&#x27;s clicked, nothing happens. It does not matter how long I wait, it would not do anything until I have to kill the process. Any suggestion? I&#x27;m using wi...'''
date = "2013-09-18T08:33:00Z"
lastmod = "2013-09-18T08:44:00Z"
weight = 24921
keywords = [ "stall", "freezing", "wireshark" ]
aliases = [ "/questions/24921" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark freezes or stalls](/questions/24921/wireshark-freezes-or-stalls)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24921-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24921-score" class="post-score" title="current number of votes">0</div><span id="post-24921-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know why the latest version (1.10.2) of wireshark did not work right? The software would function fine until I click to capture. Once it's clicked, nothing happens. It does not matter how long I wait, it would not do anything until I have to kill the process. Any suggestion? I'm using wireshark under Windows Server 2008.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-stall" rel="tag" title="see questions tagged &#39;stall&#39;">stall</span> <span class="post-tag tag-link-freezing" rel="tag" title="see questions tagged &#39;freezing&#39;">freezing</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Sep '13, 08:33</strong></p><img src="https://secure.gravatar.com/avatar/7a096d30179ceb77d76d65c0eddc1d98?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Charlie&#39;s gravatar image" /><p><span>Charlie</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Charlie has no accepted answers">0%</span></p></div></div><div id="comments-container-24921" class="comments-container"><span id="24922"></span><div id="comment-24922" class="comment"><div id="post-24922-score" class="comment-score"></div><div class="comment-text"><p>There have been a few reports of odd happenings on W2K8. Are you connected to the server remotely or actually at the server console?</p></div><div id="comment-24922-info" class="comment-info"><span class="comment-age">(18 Sep '13, 08:44)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-24921" class="comment-tools"></div><div class="clear"></div><div id="comment-24921-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

