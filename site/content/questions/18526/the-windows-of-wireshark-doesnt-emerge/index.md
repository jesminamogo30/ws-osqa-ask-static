+++
type = "question"
title = "the windows of Wireshark doesn&#x27;t  emerge"
description = '''i am having a similar problem which is when i started the Wireshark another windows emerged asking me to choose the program to run the Wirshark'''
date = "2013-02-11T21:54:00Z"
lastmod = "2013-02-12T00:35:00Z"
weight = 18526
keywords = [ "mac" ]
aliases = [ "/questions/18526" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [the windows of Wireshark doesn't emerge](/questions/18526/the-windows-of-wireshark-doesnt-emerge)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18526-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18526-score" class="post-score" title="current number of votes">0</div><span id="post-18526-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i am having a similar problem which is when i started the Wireshark another windows emerged asking me to choose the program to run the Wirshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Feb '13, 21:54</strong></p><img src="https://secure.gravatar.com/avatar/46c378ef7cf3781d5650d2670f5891e5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bandar&#39;s gravatar image" /><p><span>bandar</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bandar has no accepted answers">0%</span></p></div></div><div id="comments-container-18526" class="comments-container"><span id="18529"></span><div id="comment-18529" class="comment"><div id="post-18529-score" class="comment-score"></div><div class="comment-text"><p>I think the OP's system is OS X of some form as they had originally posted the question as an "answer" to <a href="http://ask.wireshark.org/questions/9617/wireshark-wont-start-on-osx-lion">http://ask.wireshark.org/questions/9617/wireshark-wont-start-on-osx-lion</a></p></div><div id="comment-18529-info" class="comment-info"><span class="comment-age">(12 Feb '13, 00:35)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-18526" class="comment-tools"></div><div class="clear"></div><div id="comment-18526-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

