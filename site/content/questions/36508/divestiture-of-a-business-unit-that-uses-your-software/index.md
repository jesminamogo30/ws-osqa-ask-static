+++
type = "question"
title = "Divestiture of a business unit that uses your software"
description = '''Hi, My name is Bruce Decock and I was the CIO at LSI prior to it being acquired by Avago and I am now working on a divestiture of Avago&#x27;s network processor business unit to Intel. This business unit uses Wireshark software and I need to find out from you how to handle a consent for the BU to continu...'''
date = "2014-09-22T16:19:00Z"
lastmod = "2014-09-22T16:44:00Z"
weight = 36508
keywords = [ "divestiture", "software", "consent" ]
aliases = [ "/questions/36508" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Divestiture of a business unit that uses your software](/questions/36508/divestiture-of-a-business-unit-that-uses-your-software)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36508-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36508-score" class="post-score" title="current number of votes">0</div><span id="post-36508-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, My name is Bruce Decock and I was the CIO at LSI prior to it being acquired by Avago and I am now working on a divestiture of Avago's network processor business unit to Intel. This business unit uses Wireshark software and I need to find out from you how to handle a consent for the BU to continue to use this software for up to 9 months until they get fully transitioned to Intel, or an assignment of this license to Intel. Can you get back to me with a contact so we can set up a time to discuss this and how we can move it forward?</p><p>Thanks for your help, Bruce</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-divestiture" rel="tag" title="see questions tagged &#39;divestiture&#39;">divestiture</span> <span class="post-tag tag-link-software" rel="tag" title="see questions tagged &#39;software&#39;">software</span> <span class="post-tag tag-link-consent" rel="tag" title="see questions tagged &#39;consent&#39;">consent</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '14, 16:19</strong></p><img src="https://secure.gravatar.com/avatar/2927ff11dfd8ad03fdb2e77fad7a8e98?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bruce71&#39;s gravatar image" /><p><span>bruce71</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bruce71 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Sep '14, 16:36</strong> </span></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span></p></div></div><div id="comments-container-36508" class="comments-container"><span id="36509"></span><div id="comment-36509" class="comment"><div id="post-36509-score" class="comment-score"></div><div class="comment-text"><p>Are there any internal company-private Wireshark changes/modifications involved ?</p></div><div id="comment-36509-info" class="comment-info"><span class="comment-age">(22 Sep '14, 16:37)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-36508" class="comment-tools"></div><div class="clear"></div><div id="comment-36508-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36510"></span>

<div id="answer-container-36510" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36510-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36510-score" class="post-score" title="current number of votes">1</div><span id="post-36510-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is released under the GNU General Public License. The GPL doesn't place any restrictions on the use of the software (distribution is a different story). Neither I nor the Wireshark Foundation have signed any agreements with anyone including Avago, LSI, or Intel granting permission to use the software because none is needed. You're free to use it without anyone's consent.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Sep '14, 16:44</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-36510" class="comments-container"></div><div id="comment-tools-36510" class="comment-tools"></div><div class="clear"></div><div id="comment-36510-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

