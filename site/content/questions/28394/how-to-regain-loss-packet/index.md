+++
type = "question"
title = "how to regain loss packet"
description = '''Hello Sir, i have one dummy router format in extension .pkt is that possible i can see the sending &amp;amp; receiving of packet &amp;amp; if any how loss occur in sending packet how i re-transmit or resend that packet.'''
date = "2013-12-26T00:07:00Z"
lastmod = "2013-12-26T00:31:00Z"
weight = 28394
keywords = [ "regain", "of", "wireshark", "packet", "using" ]
aliases = [ "/questions/28394" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to regain loss packet](/questions/28394/how-to-regain-loss-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28394-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28394-score" class="post-score" title="current number of votes">0</div><span id="post-28394-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Sir, i have one dummy router format in extension .pkt is that possible i can see the sending &amp; receiving of packet &amp; if any how loss occur in sending packet how i re-transmit or resend that packet.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-regain" rel="tag" title="see questions tagged &#39;regain&#39;">regain</span> <span class="post-tag tag-link-of" rel="tag" title="see questions tagged &#39;of&#39;">of</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-using" rel="tag" title="see questions tagged &#39;using&#39;">using</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Dec '13, 00:07</strong></p><img src="https://secure.gravatar.com/avatar/2e76a0d7455b72bccbca6a636a31e564?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mohit11282&#39;s gravatar image" /><p><span>mohit11282</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mohit11282 has no accepted answers">0%</span></p></div></div><div id="comments-container-28394" class="comments-container"></div><div id="comment-tools-28394" class="comment-tools"></div><div class="clear"></div><div id="comment-28394-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28395"></span>

<div id="answer-container-28395" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28395-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28395-score" class="post-score" title="current number of votes">0</div><span id="post-28395-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>if any how loss occur in sending packet how i re-transmit or resend that packet.</p></blockquote><p>you can't, neither with Wireshark nor with any other network analysis tool, because what is not there (lost frames) cannot be re-created out of thin air.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Dec '13, 00:31</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-28395" class="comments-container"></div><div id="comment-tools-28395" class="comment-tools"></div><div class="clear"></div><div id="comment-28395-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

