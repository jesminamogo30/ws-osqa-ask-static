+++
type = "question"
title = "decode as wrong box? please help"
description = '''I want this box the first image to come up so I can choose AIM  but every time I have the packet selected and when I choose decode as all I get is this box (second image) '''
date = "2016-10-25T13:03:00Z"
lastmod = "2016-10-25T14:32:00Z"
weight = 56656
keywords = [ "decode", "ssl", "as", "packet" ]
aliases = [ "/questions/56656" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [decode as wrong box? please help](/questions/56656/decode-as-wrong-box-please-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56656-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56656-score" class="post-score" title="current number of votes">0</div><span id="post-56656-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want this box the first image to come up so I can choose AIM</p><p><img src="https://www.wireshark.org/docs/wsug_html_chunked/wsug_graphics/ws-decode-as.png" alt="alt text" /></p><p>but every time I have the packet selected and when I choose decode as all I get is this box (second image)</p><p><img src="https://www.wireshark.org/docs/wsug_html_chunked/wsug_graphics/ws-decode-as-show.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-as" rel="tag" title="see questions tagged &#39;as&#39;">as</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '16, 13:03</strong></p><img src="https://secure.gravatar.com/avatar/af1b81537798fe755b3734ba8a999f91?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="achalkley&#39;s gravatar image" /><p><span>achalkley</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="achalkley has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Oct '16, 13:17</strong> </span></p></div></div><div id="comments-container-56656" class="comments-container"></div><div id="comment-tools-56656" class="comment-tools"></div><div class="clear"></div><div id="comment-56656-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56659"></span>

<div id="answer-container-56659" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56659-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56659-score" class="post-score" title="current number of votes">0</div><span id="post-56659-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>These two are the same. The first image is how the Decode As dialog box looked in earlier versions of Wireshark; the second image is how the Decode As dialog box looks in recent versions of Wireshark.</p><p>In both images, Wireshark has been configured to dissect traffic over TCP port 3196 as AIM.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Oct '16, 14:32</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></img></div></div><div id="comments-container-56659" class="comments-container"></div><div id="comment-tools-56659" class="comment-tools"></div><div class="clear"></div><div id="comment-56659-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

