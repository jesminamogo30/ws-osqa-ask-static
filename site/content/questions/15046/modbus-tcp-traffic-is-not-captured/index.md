+++
type = "question"
title = "MODBUS TCP  traffic is not captured"
description = '''Running Modbus Slave and Modbus Master on one machine with Modbus TCP protocl enabled,Wireshark(Ver.1.8.1) captured any Modbus TCP traffic. Are there any checkups for this case?'''
date = "2012-10-17T01:53:00Z"
lastmod = "2012-10-17T02:27:00Z"
weight = 15046
keywords = [ "modbus", "tcp" ]
aliases = [ "/questions/15046" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [MODBUS TCP traffic is not captured](/questions/15046/modbus-tcp-traffic-is-not-captured)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15046-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15046-score" class="post-score" title="current number of votes">0</div><span id="post-15046-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Running Modbus Slave and Modbus Master on one machine with Modbus TCP protocl enabled,Wireshark(Ver.1.8.1) captured any Modbus TCP traffic. Are there any checkups for this case?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-modbus" rel="tag" title="see questions tagged &#39;modbus&#39;">modbus</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '12, 01:53</strong></p><img src="https://secure.gravatar.com/avatar/d1262cfef0f387af4bbfa433f9db5e78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="choejs&#39;s gravatar image" /><p><span>choejs</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="choejs has no accepted answers">0%</span></p></div></div><div id="comments-container-15046" class="comments-container"></div><div id="comment-tools-15046" class="comment-tools"></div><div class="clear"></div><div id="comment-15046-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15049"></span>

<div id="answer-container-15049" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15049-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15049-score" class="post-score" title="current number of votes">1</div><span id="post-15049-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Are you running on Windows? If so it's not easy to capture "loopback" traffic on the same machine. See the Wiki page on <a href="http://wiki.wireshark.org/CaptureSetup/Loopback">Loopback Capture</a> for more info.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Oct '12, 02:27</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-15049" class="comments-container"></div><div id="comment-tools-15049" class="comment-tools"></div><div class="clear"></div><div id="comment-15049-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

