+++
type = "question"
title = "Airopeek sniffer capture (.pkt) and tags"
description = '''Hi, The wireshark code doesn&#x27;t interpret all the tags present in the sniffer capture (.pkt). Where can I find the description of each tag present in such kind of file? Thanks in advance.'''
date = "2013-05-03T11:56:00Z"
lastmod = "2013-05-03T14:20:00Z"
weight = 20939
keywords = [ "airopeek", "wireshark" ]
aliases = [ "/questions/20939" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Airopeek sniffer capture (.pkt) and tags](/questions/20939/airopeek-sniffer-capture-pkt-and-tags)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20939-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20939-score" class="post-score" title="current number of votes">0</div><span id="post-20939-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>The wireshark code doesn't interpret all the tags present in the sniffer capture (.pkt). Where can I find the description of each tag present in such kind of file? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-airopeek" rel="tag" title="see questions tagged &#39;airopeek&#39;">airopeek</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 May '13, 11:56</strong></p><img src="https://secure.gravatar.com/avatar/cebfc4539e6936d382011a3add7e7c1b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Silwer%20star&#39;s gravatar image" /><p><span>Silwer star</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Silwer star has no accepted answers">0%</span></p></div></div><div id="comments-container-20939" class="comments-container"></div><div id="comment-tools-20939" class="comment-tools"></div><div class="clear"></div><div id="comment-20939-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20940"></span>

<div id="answer-container-20940" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20940-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20940-score" class="post-score" title="current number of votes">0</div><span id="post-20940-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please take a look at the PeekRdr Sample Application and the included documentation.</p><blockquote><p><a href="https://mypeek.wildpackets.com/view_solution.php?id=26">https://mypeek.wildpackets.com/view_solution.php?id=26</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 May '13, 12:09</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-20940" class="comments-container"><span id="20942"></span><div id="comment-20942" class="comment"><div id="post-20942-score" class="comment-score"></div><div class="comment-text"><p>Note that, to quote that page, "You must be a MyPeek member to download our extensibility solutions. Click here to learn more about the benefits of membership and find out how to sign up for free."</p><p>I don't know whether you end up, in effect, signing an NDA for the information you get from that documentation.</p><p>There's also <a href="http://www.varsanofiev.com/inside/airopeekv9.htm">a reverse-engineered list of tags</a> that was used for the "tagged *Peek" file reading code; it's not necessarily a complete list.</p></div><div id="comment-20942-info" class="comment-info"><span class="comment-age">(03 May '13, 13:11)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="20946"></span><div id="comment-20946" class="comment"><div id="post-20946-score" class="comment-score"></div><div class="comment-text"><p>yes, that's odd. You can create an account, but you cannot download PeekRdr without a support contract.</p><p>As the OP apparently has access to a *.pkt file, chances are that he/she also owns a copy of Airopeek and then he/she may also have a support contract. Then he/she could be able to download that file and tell us if there is any NDA ralated to the file format.</p></div><div id="comment-20946-info" class="comment-info"><span class="comment-age">(03 May '13, 14:20)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-20940" class="comment-tools"></div><div class="clear"></div><div id="comment-20940-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

