+++
type = "question"
title = "KeyTab - Kerberos"
description = '''Hi there ,  i am trying to review some examples on: http://wiki.wireshark.org/Kerberos but when i try to do either: a) add the keytab via the gui - the option no longer seems to be availble b) add using the -K from the command line - the command doesnt run sucessfully and just stops at the help sect...'''
date = "2011-11-14T02:29:00Z"
lastmod = "2011-11-21T12:21:00Z"
weight = 7408
keywords = [ "kerberos" ]
aliases = [ "/questions/7408" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [KeyTab - Kerberos](/questions/7408/keytab-kerberos)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7408-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7408-score" class="post-score" title="current number of votes">0</div><span id="post-7408-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there ,</p><p>i am trying to review some examples on: http://wiki.wireshark.org/Kerberos</p><p>but when i try to do either: a) add the keytab via the gui - the option no longer seems to be availble b) add using the -K from the command line - the command doesnt run sucessfully and just stops at the help section next to the -K (keytab).</p><p>I want to review some of the examples before i attempt to do this for myself.</p><p>can anyone help thanks Chris</p><hr /><p>ok so from XP i do get the menu in the GUI but i still cant get the examples to work.</p><p>Does anyone have a working solution - or any other examples ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-kerberos" rel="tag" title="see questions tagged &#39;kerberos&#39;">kerberos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '11, 02:29</strong></p><img src="https://secure.gravatar.com/avatar/1b822de19024c45d8f16a5cbe6487a5a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chrisbeams&#39;s gravatar image" /><p><span>chrisbeams</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chrisbeams has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Nov '11, 05:12</strong> </span></p></div></div><div id="comments-container-7408" class="comments-container"></div><div id="comment-tools-7408" class="comment-tools"></div><div class="clear"></div><div id="comment-7408-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7542"></span>

<div id="answer-container-7542" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7542-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7542-score" class="post-score" title="current number of votes">0</div><span id="post-7542-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I faced this issue with the 64bit version of Wireshark v1.6.4. Switched to the 32bit version and was able to decrypt Kerberos traffic by specifying the keytab on the GUI.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Nov '11, 12:21</strong></p><img src="https://secure.gravatar.com/avatar/3be3127a9dfaf7509f581a173f518106?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kvthanga&#39;s gravatar image" /><p><span>kvthanga</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kvthanga has no accepted answers">0%</span></p></div></div><div id="comments-container-7542" class="comments-container"></div><div id="comment-tools-7542" class="comment-tools"></div><div class="clear"></div><div id="comment-7542-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</hr>

</div>

</div>

