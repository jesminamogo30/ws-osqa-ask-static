+++
type = "question"
title = "Change package string and install directory during wireshark build process"
description = '''I am trying to build wireshark from source in ubuntu 12.04. I have successfully build it with changing the version by adding a version.conf file.Is it possible to change the package string and installation directory?If yes,please tell me the procedure.'''
date = "2013-09-05T07:25:00Z"
lastmod = "2013-09-06T09:03:00Z"
weight = 24379
keywords = [ "source", "build", "wireshark" ]
aliases = [ "/questions/24379" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Change package string and install directory during wireshark build process](/questions/24379/change-package-string-and-install-directory-during-wireshark-build-process)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24379-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24379-score" class="post-score" title="current number of votes">0</div><span id="post-24379-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to build wireshark from source in ubuntu 12.04. I have successfully build it with changing the version by adding a version.conf file.Is it possible to change the package string and installation directory?If yes,please tell me the procedure.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-source" rel="tag" title="see questions tagged &#39;source&#39;">source</span> <span class="post-tag tag-link-build" rel="tag" title="see questions tagged &#39;build&#39;">build</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Sep '13, 07:25</strong></p><img src="https://secure.gravatar.com/avatar/5aae92c75bcf159f9da5092d5e7e99a1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="swap&#39;s gravatar image" /><p><span>swap</span><br />
<span class="score" title="11 reputation points">11</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="swap has no accepted answers">0%</span></p></div></div><div id="comments-container-24379" class="comments-container"></div><div id="comment-tools-24379" class="comment-tools"></div><div class="clear"></div><div id="comment-24379-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24433"></span>

<div id="answer-container-24433" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24433-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24433-score" class="post-score" title="current number of votes">0</div><span id="post-24433-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You're looking for <a href="http://www.debian.org/doc/manuals/developers-reference/best-pkging-practices.html">Debian packaging guidelines</a>, and <code>configure --help</code> output.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Sep '13, 09:03</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-24433" class="comments-container"></div><div id="comment-tools-24433" class="comment-tools"></div><div class="clear"></div><div id="comment-24433-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

