+++
type = "question"
title = "COMO FAÇO E QUAIS FERRAMENTAS NECESSÁRIAS PARA TRADUZIR O PROGRAMA EM PORTUGUÊS BRASIL"
description = '''OLÁ Galera tudo bem ? gostaria de saber como faço para desenvolver a tradução do programa para a versão português brasil,se alguém poderia esta me auxiliando com nome de ferramentas etc...e os arquivos '''
date = "2014-09-19T05:16:00Z"
lastmod = "2014-09-20T16:04:00Z"
weight = 36440
keywords = [ "do", "programa", "tradução" ]
aliases = [ "/questions/36440" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [COMO FAÇO E QUAIS FERRAMENTAS NECESSÁRIAS PARA TRADUZIR O PROGRAMA EM PORTUGUÊS BRASIL](/questions/36440/como-faco-e-quais-ferramentas-necessarias-para-traduzir-o-programa-em-portugues-brasil)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36440-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36440-score" class="post-score" title="current number of votes">0</div><span id="post-36440-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>OLÁ Galera tudo bem ?</p><p>gostaria de saber como faço para desenvolver a tradução do programa para a versão português brasil,se alguém poderia esta me auxiliando com nome de ferramentas etc...e os arquivos<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-do" rel="tag" title="see questions tagged &#39;do&#39;">do</span> <span class="post-tag tag-link-programa" rel="tag" title="see questions tagged &#39;programa&#39;">programa</span> <span class="post-tag tag-link-tradução" rel="tag" title="see questions tagged &#39;tradução&#39;">tradução</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Sep '14, 05:16</strong></p><img src="https://secure.gravatar.com/avatar/6e22fda8c296626c2f08ffdf2fc924dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rudsoncamarda&#39;s gravatar image" /><p><span>rudsoncamarda</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rudsoncamarda has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-36440" class="comments-container"><span id="36443"></span><div id="comment-36443" class="comment"><div id="post-36443-score" class="comment-score"></div><div class="comment-text"><p>Google translate:</p><p>Title:</p><p>HOW DO AND WHAT TOOLS REQUIRED TO TRANSLATE THE PROGRAM IN PORTUGUESE BRAZIL</p><p>Text:</p><p>HELLO Galera okay ?</p><p>I wonder how do I develop a program translation for the Portuguese Brazil , this could be someone helping me with the name of tools and files etc ...</p></div><div id="comment-36443-info" class="comment-info"><span class="comment-age">(19 Sep '14, 06:07)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="36466"></span><div id="comment-36466" class="comment"><div id="post-36466-score" class="comment-score"></div><div class="comment-text"><p>Hm my bad, I was too quick dismissing this as irrelevant/SPAM. Sorry! :-)</p></div><div id="comment-36466-info" class="comment-info"><span class="comment-age">(19 Sep '14, 12:33)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-36440" class="comment-tools"></div><div class="clear"></div><div id="comment-36440-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36461"></span>

<div id="answer-container-36461" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36461-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36461-score" class="post-score" title="current number of votes">1</div><span id="post-36461-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Bom dia. If you are asking about translating Wireshark to Portuguese you can find instructions at the bottom of the file doc/README.qt in the Wireshark source code directory. Translation efforts are underway for several languages including Chinese, French, German, Japanese, and Polish.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Sep '14, 08:57</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-36461" class="comments-container"><span id="36486"></span><div id="comment-36486" class="comment"><div id="post-36486-score" class="comment-score"></div><div class="comment-text"><p>(Presumably Qt supports national variants of languages, so that, for example, the rest of the English-speaking world doesn't have to deal with "coloring" packets :-), and there can be separate Portuguese-for-Portugal and Portuguese-for-Brazil translations.)</p></div><div id="comment-36486-info" class="comment-info"><span class="comment-age">(20 Sep '14, 16:04)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-36461" class="comment-tools"></div><div class="clear"></div><div id="comment-36461-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

