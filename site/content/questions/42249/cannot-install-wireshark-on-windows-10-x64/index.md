+++
type = "question"
title = "Cannot install wireshark on windows 10 x64"
description = '''i tried install all version wireshark on win 10 x64 but not working. Please!'''
date = "2015-05-09T00:18:00Z"
lastmod = "2015-05-09T06:20:00Z"
weight = 42249
keywords = [ "windows", "install" ]
aliases = [ "/questions/42249" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot install wireshark on windows 10 x64](/questions/42249/cannot-install-wireshark-on-windows-10-x64)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42249-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42249-score" class="post-score" title="current number of votes">0</div><span id="post-42249-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i tried install all version wireshark on win 10 x64 but not working. Please!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '15, 00:18</strong></p><img src="https://secure.gravatar.com/avatar/38c076ac2fe0b4aef4e7038a054e28f7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="binh&#39;s gravatar image" /><p><span>binh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="binh has no accepted answers">0%</span></p></div></div><div id="comments-container-42249" class="comments-container"><span id="42252"></span><div id="comment-42252" class="comment"><div id="post-42252-score" class="comment-score"></div><div class="comment-text"><p>Could you clarify what issue you are facing? I installed Wireshark on Windows 10 x64 10074 without any problem.</p><p>Windows 10 versions starting from 10049 and prior to 10061 are not compatible with WinPcap so packet capture does not work. If you are using those releases please upgrade Windows.</p></div><div id="comment-42252-info" class="comment-info"><span class="comment-age">(09 May '15, 06:20)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-42249" class="comment-tools"></div><div class="clear"></div><div id="comment-42249-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

