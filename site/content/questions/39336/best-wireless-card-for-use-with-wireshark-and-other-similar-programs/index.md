+++
type = "question"
title = "Best Wireless card for use with Wireshark and other similar programs?"
description = '''Hi I&#x27;m using the latest version of Ubuntu 14.04 and want to learn how to use Wireshark and other software that uses the wireless card, zenmap etc... What would you suggest for a wireless card? I&#x27;m on a 2 year old HP laptop. Thanks. Other specs: hp 2000 3.8 gb memory intel pentium cpu 2020m @ 2.40GHz...'''
date = "2015-01-21T05:46:00Z"
lastmod = "2015-01-21T05:46:00Z"
weight = 39336
keywords = [ "hp" ]
aliases = [ "/questions/39336" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Best Wireless card for use with Wireshark and other similar programs?](/questions/39336/best-wireless-card-for-use-with-wireshark-and-other-similar-programs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39336-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39336-score" class="post-score" title="current number of votes">0</div><span id="post-39336-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I'm using the latest version of Ubuntu 14.04 and want to learn how to use Wireshark and other software that uses the wireless card, zenmap etc... What would you suggest for a wireless card? I'm on a 2 year old HP laptop. Thanks.</p><p>Other specs: hp 2000 3.8 gb memory intel pentium cpu 2020m @ 2.40GHz x2 graphics intet ivybridge mobile os type 64 bit free disk space 48Gb</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hp" rel="tag" title="see questions tagged &#39;hp&#39;">hp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '15, 05:46</strong></p><img src="https://secure.gravatar.com/avatar/09dae98a95c755f08fbc46360c3d34c3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="johnbaptiste&#39;s gravatar image" /><p><span>johnbaptiste</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="johnbaptiste has no accepted answers">0%</span></p></div></div><div id="comments-container-39336" class="comments-container"></div><div id="comment-tools-39336" class="comment-tools"></div><div class="clear"></div><div id="comment-39336-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

