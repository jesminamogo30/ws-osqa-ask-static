+++
type = "question"
title = "L2P and DSCP tags"
description = '''How to check L2P and DSCP tags in the packet. Which NIC supports the tagged packets? If driver is stripping off the tags then how to check the same?'''
date = "2012-10-04T05:57:00Z"
lastmod = "2012-10-04T05:57:00Z"
weight = 14707
keywords = [ "1.0.4", "version" ]
aliases = [ "/questions/14707" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [L2P and DSCP tags](/questions/14707/l2p-and-dscp-tags)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14707-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14707-score" class="post-score" title="current number of votes">0</div><span id="post-14707-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to check L2P and DSCP tags in the packet. Which NIC supports the tagged packets? If driver is stripping off the tags then how to check the same?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-1.0.4" rel="tag" title="see questions tagged &#39;1.0.4&#39;">1.0.4</span> <span class="post-tag tag-link-version" rel="tag" title="see questions tagged &#39;version&#39;">version</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Oct '12, 05:57</strong></p><img src="https://secure.gravatar.com/avatar/f57bc43f11cf21dbbe03d7f15d083de6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Maldini&#39;s gravatar image" /><p><span>Maldini</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Maldini has no accepted answers">0%</span></p></div></div><div id="comments-container-14707" class="comments-container"></div><div id="comment-tools-14707" class="comment-tools"></div><div class="clear"></div><div id="comment-14707-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

