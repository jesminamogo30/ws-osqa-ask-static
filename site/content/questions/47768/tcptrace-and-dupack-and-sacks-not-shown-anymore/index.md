+++
type = "question"
title = "tcptrace and dupack and sacks not shown anymore"
description = '''Hello, I recently installed Wireshark 2.0 on my MAC and I noticed dupacks and sacks option is not shown in the tcptrace graph anymore. This feature available in previous release is very useful to do tcp debugging. Is it something expected. Thanks.'''
date = "2015-11-19T10:40:00Z"
lastmod = "2016-03-10T20:18:00Z"
weight = 47768
keywords = [ "duplicateacks", "wireshark-2", "tcptrace", "sack" ]
aliases = [ "/questions/47768" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [tcptrace and dupack and sacks not shown anymore](/questions/47768/tcptrace-and-dupack-and-sacks-not-shown-anymore)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47768-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47768-score" class="post-score" title="current number of votes">1</div><span id="post-47768-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I recently installed Wireshark 2.0 on my MAC and I noticed dupacks and sacks option is not shown in the tcptrace graph anymore.</p><p>This feature available in previous release is very useful to do tcp debugging.</p><p>Is it something expected.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-duplicateacks" rel="tag" title="see questions tagged &#39;duplicateacks&#39;">duplicateacks</span> <span class="post-tag tag-link-wireshark-2" rel="tag" title="see questions tagged &#39;wireshark-2&#39;">wireshark-2</span> <span class="post-tag tag-link-tcptrace" rel="tag" title="see questions tagged &#39;tcptrace&#39;">tcptrace</span> <span class="post-tag tag-link-sack" rel="tag" title="see questions tagged &#39;sack&#39;">sack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Nov '15, 10:40</strong></p><img src="https://secure.gravatar.com/avatar/3d3a18a8eff62a0544b3bde0b3d8ff0c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rafdalb&#39;s gravatar image" /><p><span>rafdalb</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rafdalb has no accepted answers">0%</span></p></div></div><div id="comments-container-47768" class="comments-container"><span id="50812"></span><div id="comment-50812" class="comment"><div id="post-50812-score" class="comment-score"></div><div class="comment-text"><p>Agreed and quite understated. The extra annotations on the tcptrace graph are vital to understanding a packet capture.</p></div><div id="comment-50812-info" class="comment-info"><span class="comment-age">(10 Mar '16, 17:42)</span> <span class="comment-user userinfo">eggnet</span></div></div></div><div id="comment-tools-47768" class="comment-tools"></div><div class="clear"></div><div id="comment-47768-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50813"></span>

<div id="answer-container-50813" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50813-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50813-score" class="post-score" title="current number of votes">0</div><span id="post-50813-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12009">bug 12009</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Mar '16, 20:18</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-50813" class="comments-container"></div><div id="comment-tools-50813" class="comment-tools"></div><div class="clear"></div><div id="comment-50813-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

