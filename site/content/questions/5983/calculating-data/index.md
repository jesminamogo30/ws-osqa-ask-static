+++
type = "question"
title = "Calculating data"
description = '''I have a wireshark capture of data on a particular port of a smart switch. I want to be ab;e to calculate how much data went to or from a particular IP. Are there expressions in the filters etc that would enable me to do this.'''
date = "2011-08-30T19:21:00Z"
lastmod = "2011-08-30T22:10:00Z"
weight = 5983
keywords = [ "filters" ]
aliases = [ "/questions/5983" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Calculating data](/questions/5983/calculating-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5983-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5983-score" class="post-score" title="current number of votes">0</div><span id="post-5983-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a wireshark capture of data on a particular port of a smart switch. I want to be ab;e to calculate how much data went to or from a particular IP. Are there expressions in the filters etc that would enable me to do this.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filters" rel="tag" title="see questions tagged &#39;filters&#39;">filters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Aug '11, 19:21</strong></p><img src="https://secure.gravatar.com/avatar/ffbda7e4d22dfb67ff95044520f06b3f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kasper46&#39;s gravatar image" /><p><span>kasper46</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kasper46 has no accepted answers">0%</span></p></div></div><div id="comments-container-5983" class="comments-container"></div><div id="comment-tools-5983" class="comment-tools"></div><div class="clear"></div><div id="comment-5983-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5984"></span>

<div id="answer-container-5984" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5984-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5984-score" class="post-score" title="current number of votes">2</div><span id="post-5984-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look at Statistics ! Endpoints in Wireshark.</p><p>You'll get a table of packet counts, etc by endpoint for a selected type of endpoint (Ethernet, IPv4, etc0</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Aug '11, 19:40</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Aug '11, 19:40</strong> </span></p></div></div><div id="comments-container-5984" class="comments-container"><span id="5985"></span><div id="comment-5985" class="comment"><div id="post-5985-score" class="comment-score"></div><div class="comment-text"><p>Hi Bill</p><p>Thanks very much for that</p><p>Regards Wayne</p></div><div id="comment-5985-info" class="comment-info"><span class="comment-age">(30 Aug '11, 20:58)</span> <span class="comment-user userinfo">kasper46</span></div></div><span id="5987"></span><div id="comment-5987" class="comment"><div id="post-5987-score" class="comment-score"></div><div class="comment-text"><p>(converted your "answer" to a "comment", please see the FAQ for details. Also please click on the checkmark on the left if this is indeed an answer to your question)</p></div><div id="comment-5987-info" class="comment-info"><span class="comment-age">(30 Aug '11, 22:10)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-5984" class="comment-tools"></div><div class="clear"></div><div id="comment-5984-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

