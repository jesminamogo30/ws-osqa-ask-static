+++
type = "question"
title = "wireshark ipsec"
description = '''good afternoon! My server(windows 2012 r2 - Have White IP ) establishes a VPN connection. In the interface list I see only Ethernet (disable)? Ethernet 2 (work) my task is to monitor the packets with private addresses(10.0.0.x) How it to do ? Article I do not understand - https://wiki.wireshark.org/...'''
date = "2016-10-31T01:26:00Z"
lastmod = "2016-10-31T01:26:00Z"
weight = 56852
keywords = [ "windows", "ipsec" ]
aliases = [ "/questions/56852" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark ipsec](/questions/56852/wireshark-ipsec)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56852-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56852-score" class="post-score" title="current number of votes">0</div><span id="post-56852-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>good afternoon! My server(windows 2012 r2 - Have White IP ) establishes a VPN connection. In the interface list I see only Ethernet (disable)? Ethernet 2 (work) my task is to monitor the packets with private addresses(10.0.0.x)</p><p>How it to do ?</p><p>Article I do not understand - <a href="https://wiki.wireshark.org/ESP_Preferences">https://wiki.wireshark.org/ESP_Preferences</a> , I have another version In wireshark i can see only:</p><pre><code>Frame 138: 270 bytes on wire (2160 bits), 270 bytes captured (2160 bits) on interface 0
Ethernet II, Src: CiscoInc_17:e8:00 (00:15:c7:17:e8:00), Dst: SuperMic_64:81:a2 (0c:c4:7a:64:81:a2)
Internet Protocol Version 4, Src: &quot;WhiteIP2&quot;, Dst: &quot;WhiteIP1&quot;
Encapsulating Security Payload
    ESP SPI: 0xe5aca32c (3853296428)
    ESP Sequence: 2128</code></pre><p>i have PSK, remote and my IP (peer), 3des, SHA-1, Deffy-helman groupe 2, authen - Require inbound and outbound, author - NO</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-ipsec" rel="tag" title="see questions tagged &#39;ipsec&#39;">ipsec</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '16, 01:26</strong></p><img src="https://secure.gravatar.com/avatar/b53fb7a9f4f92759766440b93a4712fd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="friis&#39;s gravatar image" /><p><span>friis</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="friis has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Jan '17, 08:00</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-56852" class="comments-container"></div><div id="comment-tools-56852" class="comment-tools"></div><div class="clear"></div><div id="comment-56852-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

