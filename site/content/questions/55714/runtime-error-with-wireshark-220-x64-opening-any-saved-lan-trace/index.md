+++
type = "question"
title = "Runtime error with Wireshark 2.2.0 x64 opening any saved LAN trace."
description = '''I updated from Wireshark 2.0.5 x64 to Wireshark 2.2.0 x64 on both my production machines (Windows 8.1 x64 and Windows Server 2012 R2 x64), and on both machines Wireshark now hits a Microsoft C++ Runtime exception in libwireshark!dissect-ndr-nt-NTTIME+0x975e when opening any saved LAN trace I have, u...'''
date = "2016-09-21T07:34:00Z"
lastmod = "2016-10-06T15:40:00Z"
weight = 55714
keywords = [ "runtime", "bug", "2.2.0" ]
aliases = [ "/questions/55714" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Runtime error with Wireshark 2.2.0 x64 opening any saved LAN trace.](/questions/55714/runtime-error-with-wireshark-220-x64-opening-any-saved-lan-trace)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55714-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55714-score" class="post-score" title="current number of votes">0</div><span id="post-55714-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I updated from Wireshark 2.0.5 x64 to Wireshark 2.2.0 x64 on both my production machines (Windows 8.1 x64 and Windows Server 2012 R2 x64), and on both machines Wireshark now hits a Microsoft C++ Runtime exception in libwireshark!dissect-ndr-nt-NTTIME+0x975e when opening any saved LAN trace I have, using any method. (Pick from MRU list on Wireshark main display, double-click saved LAN trace file out of Windows Explorer, open saved LAN trace attachment directly from email, etc.) Opening Wireshark 2.2.0 without asking to open a LAN trace works fine.</p><p>I backed off to the Wireshark 2.0.6 x64 release on both machines and everything runs fine with this previous release. Have crash dumps and can file a bug, but just wanted to make sure its not something already known or worked around regarding the updated Microsoft runtime dependency, since I'm not seeing widespread reports from Wireshark 2.2.0 users.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-runtime" rel="tag" title="see questions tagged &#39;runtime&#39;">runtime</span> <span class="post-tag tag-link-bug" rel="tag" title="see questions tagged &#39;bug&#39;">bug</span> <span class="post-tag tag-link-2.2.0" rel="tag" title="see questions tagged &#39;2.2.0&#39;">2.2.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Sep '16, 07:34</strong></p><img src="https://secure.gravatar.com/avatar/6d91d48fe424d97d7c597bcd39c1022d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AlanA&#39;s gravatar image" /><p><span>AlanA</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AlanA has one accepted answer">50%</span></p></div></div><div id="comments-container-55714" class="comments-container"><span id="55716"></span><div id="comment-55716" class="comment"><div id="post-55716-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by LAN trace? A Wireshark capture from your LAN, or a capture generated by another tool?</p></div><div id="comment-55716-info" class="comment-info"><span class="comment-age">(21 Sep '16, 08:33)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-55714" class="comment-tools"></div><div class="clear"></div><div id="comment-55714-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="56206"></span>

<div id="answer-container-56206" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56206-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56206-score" class="post-score" title="current number of votes">0</div><span id="post-56206-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="AlanA has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Another engineer at my company entered this as Bug 12962, and it has been resolved in 2.2.1.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Oct '16, 15:40</strong></p><img src="https://secure.gravatar.com/avatar/6d91d48fe424d97d7c597bcd39c1022d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AlanA&#39;s gravatar image" /><p><span>AlanA</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AlanA has one accepted answer">50%</span></p></div></div><div id="comments-container-56206" class="comments-container"></div><div id="comment-tools-56206" class="comment-tools"></div><div class="clear"></div><div id="comment-56206-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="55719"></span>

<div id="answer-container-55719" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55719-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55719-score" class="post-score" title="current number of votes">0</div><span id="post-55719-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Downloading the PDB symbols from <a href="https://www.wireshark.org/download/win64/all-versions/">https://www.wireshark.org/download/win64/all-versions/</a> allowed me to identify that the crash is in the NCP protocol dissector, and indeed all the LAN traces I have been opening would have involved the NCP protocol. (Crash is actually at libwireshark!ncp2222-compile-dfilters+0x8e.) Will consult with the NCP dissector author and file as bug as needed.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Sep '16, 09:36</strong></p><img src="https://secure.gravatar.com/avatar/6d91d48fe424d97d7c597bcd39c1022d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AlanA&#39;s gravatar image" /><p><span>AlanA</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AlanA has one accepted answer">50%</span></p></div></div><div id="comments-container-55719" class="comments-container"><span id="55724"></span><div id="comment-55724" class="comment"><div id="post-55724-score" class="comment-score"></div><div class="comment-text"><p>If you do not <em>need</em> to dissect the NCP packets in particular (i.e. if they are not your focus but they just happen to be present in your traces), you may disable NCP through <code>Analysis -&gt; Enabled Protocols</code> after starting Wireshark without opening any trace. The "enabled protocols" settings survive Wireshark closure and re-opening, so once done, you can open your traces safely.</p></div><div id="comment-55724-info" class="comment-info"><span class="comment-age">(21 Sep '16, 11:59)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="55737"></span><div id="comment-55737" class="comment"><div id="post-55737-score" class="comment-score"></div><div class="comment-text"><p>If you haven't yet filed the bug, please attach a sample capture that causes the crash to the bug when you file it, if possible.</p><p>If you <em>have</em> filed the bug, and there isn't a sample capture that causes the crash attached to the bug, please attach one, if possible.</p></div><div id="comment-55737-info" class="comment-info"><span class="comment-age">(21 Sep '16, 17:54)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-55719" class="comment-tools"></div><div class="clear"></div><div id="comment-55719-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

