+++
type = "question"
title = "Unable to capture packets promiscuously on Wi-Fi on Windows"
description = '''I am trying to configure Wireshark to capture all packets on my WiFi network, however I am only packets to and from my computers, in addition to broadcast packets.  I am relatively new to Wireshark but I seem to remember when I used it on my old laptop, Wireshark had this functionality &quot;out of the b...'''
date = "2012-02-17T11:39:00Z"
lastmod = "2014-03-10T15:55:00Z"
weight = 9102
keywords = [ "capture" ]
aliases = [ "/questions/9102" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to capture packets promiscuously on Wi-Fi on Windows](/questions/9102/unable-to-capture-packets-promiscuously-on-wi-fi-on-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9102-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9102-score" class="post-score" title="current number of votes">0</div><span id="post-9102-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to configure Wireshark to capture all packets on my WiFi network, however I am only packets to and from my computers, in addition to broadcast packets.</p><p>I am relatively new to Wireshark but I seem to remember when I used it on my old laptop, Wireshark had this functionality "out of the box".</p><p>The interface I am using is a Intel Centrino Wireless-N 1030.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '12, 11:39</strong></p><img src="https://secure.gravatar.com/avatar/aa1dc58d539e48a74b168bab688234e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ethernetdan&#39;s gravatar image" /><p><span>ethernetdan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ethernetdan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Mar '12, 14:43</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-9102" class="comments-container"></div><div id="comment-tools-9102" class="comment-tools"></div><div class="clear"></div><div id="comment-9102-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="9103"></span>

<div id="answer-container-9103" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9103-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9103-score" class="post-score" title="current number of votes">0</div><span id="post-9103-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What is your OS? Some wireless card drivers on Windows can't handle promiscuous mode. See the <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">WLAN</a> capture page on the Wiki for more information.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Feb '12, 11:45</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-9103" class="comments-container"><span id="9105"></span><div id="comment-9105" class="comment"><div id="post-9105-score" class="comment-score"></div><div class="comment-text"><p>I am using Win7 x64. I looked through the page earlier but I was not able to see anything that could help resolve the issue. Is there a mirror or newer version of the list of supported hardware?, it seems the link on the page is dead. Also would running wireshark on a virtualized computer running linux work?</p></div><div id="comment-9105-info" class="comment-info"><span class="comment-age">(17 Feb '12, 11:51)</span> <span class="comment-user userinfo">ethernetdan</span></div></div><span id="9107"></span><div id="comment-9107" class="comment"><div id="post-9107-score" class="comment-score"></div><div class="comment-text"><p>Unfortunately the linux VM would still rely on the Windows card drivers. AFAIK the only guaranteed way to capture WLAN traffic on Windows is with an <a href="http://www.riverbed.com/us/products/cascade/wireshark_enhancements/airpcap.php">AirPCap</a> adaptor.</p></div><div id="comment-9107-info" class="comment-info"><span class="comment-age">(17 Feb '12, 12:17)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="9116"></span><div id="comment-9116" class="comment"><div id="post-9116-score" class="comment-score"></div><div class="comment-text"><p>Or with <a href="http://www.microsoft.com/download/en/details.aspx?id=4865">Microsoft Network Monitor</a>, which, I think, has its own drivers that, on Vista and Windows 7 (but <em>NOT</em> XP!), can use NDIS 6 (unlike WinPcap) and can thus use Native Wi-Fi (if your adapter's driver supports it) and thus can capture on Wi-Fi adapters in monitor mode. (Note that, on Windows, going into monitor mode disconnects you from your wireless network.)</p></div><div id="comment-9116-info" class="comment-info"><span class="comment-age">(18 Feb '12, 00:29)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="9499"></span><div id="comment-9499" class="comment"><div id="post-9499-score" class="comment-score"></div><div class="comment-text"><p>You've <a href="http://ask.wireshark.org/questions/9420/cannot-monitor-other-users-uni-cast-traffic-on-wlan">already asked that question</a>. No need to ask it again.</p></div><div id="comment-9499-info" class="comment-info"><span class="comment-age">(12 Mar '12, 14:39)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="9500"></span><div id="comment-9500" class="comment"><div id="post-9500-score" class="comment-score"></div><div class="comment-text"><p>As for the Linux VM, if the VM software you're using allows the virtual machine to access USB hardware on your machine (as, for example, VMware Fusion does), then if you have a USB Wi-Fi adapter that Linux supports in monitor mode (as I do), you could plug that in, have it connect to the virtual machine, and capture in monitor mode on <em>that</em> adapter (as I've done when developing and debugging the libpcap support for monitor mode on Linux and FreeBSD).</p></div><div id="comment-9500-info" class="comment-info"><span class="comment-age">(12 Mar '12, 14:42)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-9103" class="comment-tools"></div><div class="clear"></div><div id="comment-9103-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="30666"></span>

<div id="answer-container-30666" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30666-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30666-score" class="post-score" title="current number of votes">0</div><span id="post-30666-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is one way to capture WiFi packets under Windows with Wireshark. You have to install Acrylic WiFi software ( <a href="https://www.acrylicwifi.com/en/acrylic-wifi-free/">https://www.acrylicwifi.com/en/acrylic-wifi-free/</a> )</p><p>Acrylic WiFi installs an NDIS driver that captures wlan packets and also adds support to wireshark. Once Acrylic is installed you have to start wireshark as Administrator and select your NDIS WiFi interface</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Mar '14, 15:55</strong></p><img src="https://secure.gravatar.com/avatar/c601fc1140b59596c899c2386bd29460?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AcrylicWiFi&#39;s gravatar image" /><p><span>AcrylicWiFi</span><br />
<span class="score" title="9 reputation points">9</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AcrylicWiFi has no accepted answers">0%</span></p></div></div><div id="comments-container-30666" class="comments-container"></div><div id="comment-tools-30666" class="comment-tools"></div><div class="clear"></div><div id="comment-30666-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="15947"></span>

<div id="answer-container-15947" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15947-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15947-score" class="post-score" title="current number of votes">-1</div><span id="post-15947-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>AFAIR Wireshark (actually, WinPCAP) was never able to capture packets in promiscous (do you actually mean monitor?) mode on Windows. It was even not able to capture from WiFi interface at all for a long time, because "something is wrong with how windows network drivers work" (according to wipcap faq). At that time, every freaking sniffer was able to capture from Wifi interfaces (e.g., CommView) but not WinPCAP based sniffers.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Nov '12, 16:47</strong></p><img src="https://secure.gravatar.com/avatar/a00c8faaeb8a556d49ded6b3aa1eb51e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xpeh&#39;s gravatar image" /><p><span>xpeh</span><br />
<span class="score" title="-3 reputation points">-3</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xpeh has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Nov '12, 17:08</strong> </span></p></div></div><div id="comments-container-15947" class="comments-container"><span id="15987"></span><div id="comment-15987" class="comment"><div id="post-15987-score" class="comment-score"></div><div class="comment-text"><p>What the heck? Why -1?</p></div><div id="comment-15987-info" class="comment-info"><span class="comment-age">(16 Nov '12, 13:50)</span> <span class="comment-user userinfo">xpeh</span></div></div></div><div id="comment-tools-15947" class="comment-tools"></div><div class="clear"></div><div id="comment-15947-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

