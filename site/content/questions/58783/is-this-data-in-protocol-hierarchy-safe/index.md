+++
type = "question"
title = "Is this data in protocol hierarchy safe ?"
description = '''  Those two are the same Data. When I close that Transmission Control Protocol group that data disappears so it seems its under that.  But why is it not under Malformed Packet ? Is it also used by Hypertext Transfer Protocol which is why its under that. Hypertext Transfer Protocol is a subgroup of T...'''
date = "2017-01-15T11:19:00Z"
lastmod = "2017-01-16T00:20:00Z"
weight = 58783
keywords = [ "hierarchy" ]
aliases = [ "/questions/58783" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is this data in protocol hierarchy safe ?](/questions/58783/is-this-data-in-protocol-hierarchy-safe)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58783-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58783-score" class="post-score" title="current number of votes">0</div><span id="post-58783-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="http://i.imgur.com/82S6xpX.png" alt="alt text" /></p><p><img src="http://i.imgur.com/wCXFdxk.png" alt="alt text" /></p><p>Those two are the same Data. When I close that Transmission Control Protocol group that data disappears so it seems its under that. But why is it not under Malformed Packet ? Is it also used by Hypertext Transfer Protocol which is why its under that. Hypertext Transfer Protocol is a subgroup of Transmission Control Protocol here.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hierarchy" rel="tag" title="see questions tagged &#39;hierarchy&#39;">hierarchy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '17, 11:19</strong></p><img src="https://secure.gravatar.com/avatar/4066025cca7dd075cc2c5f0422518624?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sanan&#39;s gravatar image" /><p><span>Sanan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sanan has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jan '17, 11:22</strong> </span></p></div></div><div id="comments-container-58783" class="comments-container"><span id="58785"></span><div id="comment-58785" class="comment"><div id="post-58785-score" class="comment-score">1</div><div class="comment-text"><p>Why should it be under Malformed Packet? I think you try to read too much into it.</p></div><div id="comment-58785-info" class="comment-info"><span class="comment-age">(15 Jan '17, 11:56)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="58797"></span><div id="comment-58797" class="comment"><div id="post-58797-score" class="comment-score"></div><div class="comment-text"><p>If you look at the indenting, it is not under Hypertext Transfer Protocol, but instead under TCP. It is data that Wireshark has no dissector for, thus listing it as simply Data.</p><p>Is it "unsafe"? That depends.....it could be a legit proprietary protocol, but could also be something unsafe.</p></div><div id="comment-58797-info" class="comment-info"><span class="comment-age">(15 Jan '17, 20:48)</span> <span class="comment-user userinfo">Rooster_50</span></div></div><span id="58800"></span><div id="comment-58800" class="comment"><div id="post-58800-score" class="comment-score"></div><div class="comment-text"><p>This is the data associated with it, it doesn't look suspicious does it?</p><p><img src="http://i.imgur.com/002HuvQ.png" width="600" /> <img src="http://i.imgur.com/BzRgu9C.png" width="600" /></p></div><div id="comment-58800-info" class="comment-info"><span class="comment-age">(16 Jan '17, 00:20)</span> <span class="comment-user userinfo">Sanan</span></div></div></div><div id="comment-tools-58783" class="comment-tools"></div><div class="clear"></div><div id="comment-58783-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

