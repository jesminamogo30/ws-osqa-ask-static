+++
type = "question"
title = "How to save reports of Telephony-&gt;VoIP Calls"
description = '''I found a similar question on this topics, but I am asking again if anybody can provide me other solution.  When I select Telephony and then VoIP calls, a report of all VoIP calls (CDR) are found in an window, but I cannot save this report for future processing. How can I save this report? Is there ...'''
date = "2012-06-04T07:30:00Z"
lastmod = "2012-06-20T00:18:00Z"
weight = 11618
keywords = [ "voip" ]
aliases = [ "/questions/11618" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to save reports of Telephony-&gt;VoIP Calls](/questions/11618/how-to-save-reports-of-telephony-voip-calls)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11618-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11618-score" class="post-score" title="current number of votes">0</div><span id="post-11618-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I found a similar question on this topics, but I am asking again if anybody can provide me other solution. When I select Telephony and then VoIP calls, a report of all VoIP calls (CDR) are found in an window, but I cannot save this report for future processing. How can I save this report? Is there any other alternatives? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '12, 07:30</strong></p><img src="https://secure.gravatar.com/avatar/efb2948388e477e294806ea40cea1315?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Raihana&#39;s gravatar image" /><p><span>Raihana</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Raihana has no accepted answers">0%</span></p></div></div><div id="comments-container-11618" class="comments-container"><span id="11641"></span><div id="comment-11641" class="comment"><div id="post-11641-score" class="comment-score"></div><div class="comment-text"><p>Have you tried just copying the text and pasting it into a text file?</p></div><div id="comment-11641-info" class="comment-info"><span class="comment-age">(04 Jun '12, 16:52)</span> <span class="comment-user userinfo">inetdog</span></div></div><span id="11735"></span><div id="comment-11735" class="comment"><div id="post-11735-score" class="comment-score"></div><div class="comment-text"><p>Yes, I tried, but it does not allow to copy the text!</p></div><div id="comment-11735-info" class="comment-info"><span class="comment-age">(07 Jun '12, 03:03)</span> <span class="comment-user userinfo">Raihana</span></div></div><span id="11742"></span><div id="comment-11742" class="comment"><div id="post-11742-score" class="comment-score"></div><div class="comment-text"><p>Can you not select it or just not copy it?</p></div><div id="comment-11742-info" class="comment-info"><span class="comment-age">(07 Jun '12, 16:48)</span> <span class="comment-user userinfo">inetdog</span></div></div></div><div id="comment-tools-11618" class="comment-tools"></div><div class="clear"></div><div id="comment-11618-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12065"></span>

<div id="answer-container-12065" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12065-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12065-score" class="post-score" title="current number of votes">0</div><span id="post-12065-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Consult your voip provider, they will provide you hardcopy of all your records. <a href="http://www.whichvoip.com/voip/ip-pbx.htm">IP PBX Systems</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jun '12, 00:18</strong></p><img src="https://secure.gravatar.com/avatar/644783816a9e1072aee6c2f0abf39e94?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="themaverick&#39;s gravatar image" /><p><span>themaverick</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="themaverick has no accepted answers">0%</span></p></div></div><div id="comments-container-12065" class="comments-container"></div><div id="comment-tools-12065" class="comment-tools"></div><div class="clear"></div><div id="comment-12065-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

