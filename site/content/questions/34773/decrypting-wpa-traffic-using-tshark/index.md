+++
type = "question"
title = "Decrypting WPA traffic using TShark"
description = '''If we are using tshark how would we decrypt WPA traffic. I think we would need to use the -o wlan.enable_decryption:TRUE flag to enable decryption but I don&#x27;t see a preferences entry to enter the WPA password and ssid. In the wireshark client application it actually creates a file named 8021__keys s...'''
date = "2014-07-18T15:32:00Z"
lastmod = "2014-07-21T10:42:00Z"
weight = 34773
keywords = [ "decryption", "tshark" ]
aliases = [ "/questions/34773" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypting WPA traffic using TShark](/questions/34773/decrypting-wpa-traffic-using-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34773-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34773-score" class="post-score" title="current number of votes">0</div><span id="post-34773-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If we are using tshark how would we decrypt WPA traffic. I think we would need to use the -o wlan.enable_decryption:TRUE flag to enable decryption but I don't see a preferences entry to enter the WPA password and ssid. In the wireshark client application it actually creates a file named 8021__keys specifically with these key values "wpa-pwd","MyPassword:MySSID".<br />
Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '14, 15:32</strong></p><img src="https://secure.gravatar.com/avatar/d5dceed823c94e5e7f69b722b3cfc7c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jd45093&#39;s gravatar image" /><p><span>jd45093</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jd45093 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-34773" class="comments-container"></div><div id="comment-tools-34773" class="comment-tools"></div><div class="clear"></div><div id="comment-34773-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34802"></span>

<div id="answer-container-34802" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34802-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34802-score" class="post-score" title="current number of votes">0</div><span id="post-34802-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>So I figured out the answer to the question from this former QA <a href="http://ask.wireshark.org/questions/24249/decrypt-wpa-with-tshark">http://ask.wireshark.org/questions/24249/decrypt-wpa-with-tshark</a> Hope it helps :)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jul '14, 10:42</strong></p><img src="https://secure.gravatar.com/avatar/d5dceed823c94e5e7f69b722b3cfc7c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jd45093&#39;s gravatar image" /><p><span>jd45093</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jd45093 has no accepted answers">0%</span></p></div></div><div id="comments-container-34802" class="comments-container"></div><div id="comment-tools-34802" class="comment-tools"></div><div class="clear"></div><div id="comment-34802-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

