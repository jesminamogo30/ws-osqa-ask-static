+++
type = "question"
title = "How can I learn to use Wireshark"
description = '''WHAT TYPE OF BASIC KNOWLEDGE is REQUIRED TO USE WIRESHARK.I M NEW TO WIRESHARK.'''
date = "2014-04-26T09:26:00Z"
lastmod = "2014-04-26T12:33:00Z"
weight = 32189
keywords = [ "wireshark" ]
aliases = [ "/questions/32189" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can I learn to use Wireshark](/questions/32189/how-can-i-learn-to-use-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32189-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32189-score" class="post-score" title="current number of votes">0</div><span id="post-32189-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>WHAT TYPE OF BASIC KNOWLEDGE is REQUIRED TO USE WIRESHARK.I M NEW TO WIRESHARK.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Apr '14, 09:26</strong></p><img src="https://secure.gravatar.com/avatar/818fa3b6eb95625283ce42f755dc201a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RAJU%20SINGH&#39;s gravatar image" /><p><span>RAJU SINGH</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RAJU SINGH has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Apr '14, 11:54</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-32189" class="comments-container"></div><div id="comment-tools-32189" class="comment-tools"></div><div class="clear"></div><div id="comment-32189-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32190"></span>

<div id="answer-container-32190" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32190-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32190-score" class="post-score" title="current number of votes">0</div><span id="post-32190-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Speaking as a newbie. I would like to say a prerequisite along with basic knowledge would be patience,time and strong interest in networking.</p><p>In terms of knowledge</p><p>Basic networking. Knowing what an IP address, mac address, subnets,default gateways, routing, broadcast address and how they all work. TCP UDP how they work. Some knowledge of how data is sent over the network and having some basic 7 layer OSI understanding.</p><p>There are some great tutorials out on the web to install Wireshark and basic usage but one thing I really liked about Laura's book is putting the knowledge into a relevant context.</p><p>We visit web pages every day like www.Google.com which we take for granted and by using Wireshark and seeing what is actually happening in the background which haooens within milliseconds is truly amazing and humbling.</p><p>I don't know if this helps as I am still a beginner but I read the 101 and troubleshooting books and every day for the past days I am capturing packets on my network and spending 10-20 mins on Wireshark look at what is going on and trying to find one message or error or feature I don't understand and searching it :)</p><p>Cheers</p><p>Jazz PS the community is fantastic here!!!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Apr '14, 11:17</strong></p><img src="https://secure.gravatar.com/avatar/1de54471a0cc95cb8604b875d49fe7e9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yoyomonkey&#39;s gravatar image" /><p><span>yoyomonkey</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yoyomonkey has no accepted answers">0%</span></p></div></div><div id="comments-container-32190" class="comments-container"><span id="32199"></span><div id="comment-32199" class="comment"><div id="post-32199-score" class="comment-score"></div><div class="comment-text"><p><span>@yoyomonkey</span>: I deleted your duplicate answer.</p></div><div id="comment-32199-info" class="comment-info"><span class="comment-age">(26 Apr '14, 12:33)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-32190" class="comment-tools"></div><div class="clear"></div><div id="comment-32190-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

