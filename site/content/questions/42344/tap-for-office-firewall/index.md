+++
type = "question"
title = "TAP for office firewall?"
description = '''I&#x27;d like to start monitoring my office network traffic, and I purchased an old Linksys hub hoping that I could bridge my firewall&#x27;s LAN port and my laptop in order to grab everything in and out of my firewall. Problem is that I&#x27;m not seeing any TCP traffic at all, so my cheap hub has let me down. Ca...'''
date = "2015-05-12T13:50:00Z"
lastmod = "2015-05-13T10:59:00Z"
weight = 42344
keywords = [ "206" ]
aliases = [ "/questions/42344" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [TAP for office firewall?](/questions/42344/tap-for-office-firewall)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42344-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42344-score" class="post-score" title="current number of votes">0</div><span id="post-42344-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'd like to start monitoring my office network traffic, and I purchased an old Linksys hub hoping that I could bridge my firewall's LAN port and my laptop in order to grab everything in and out of my firewall. Problem is that I'm not seeing any TCP traffic at all, so my cheap hub has let me down. Can you guys recommend an affordable TAP? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-206" rel="tag" title="see questions tagged &#39;206&#39;">206</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 May '15, 13:50</strong></p><img src="https://secure.gravatar.com/avatar/fb088ac2aead612a2d6a82572cf8be51?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shartnado&#39;s gravatar image" /><p><span>Shartnado</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shartnado has no accepted answers">0%</span></p></div></div><div id="comments-container-42344" class="comments-container"></div><div id="comment-tools-42344" class="comment-tools"></div><div class="clear"></div><div id="comment-42344-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42347"></span>

<div id="answer-container-42347" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42347-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42347-score" class="post-score" title="current number of votes">0</div><span id="post-42347-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>A is nowaday no really good idea. Because a hub normaly supports only 10 MBit/s half duplex it may give some special hubs with 100 MBit/s but they even provide only half-duplex.</p><p>Better you use a cheap switch with a span port e.g. from DUALCOMM. Perhaps the recommended and cheapest solution to your Use Case.</p><p>TAPS provide the most precise capturing solution. You can use different vendors e.g. Garland, NetOptics or Cubro. If you need more precision ( no packet loss intitiated by the of capture equipment) then you should use a tap.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 May '15, 15:19</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-42347" class="comments-container"><span id="42355"></span><div id="comment-42355" class="comment"><div id="post-42355-score" class="comment-score"></div><div class="comment-text"><p>Thanks for the info!</p></div><div id="comment-42355-info" class="comment-info"><span class="comment-age">(13 May '15, 05:30)</span> <span class="comment-user userinfo">Shartnado</span></div></div><span id="42358"></span><div id="comment-42358" class="comment"><div id="post-42358-score" class="comment-score"></div><div class="comment-text"><p>a TAP is probably overkill.</p><p>For cheap switches with port mirroring see here:</p><blockquote><p><a href="https://ask.wireshark.org/questions/13892/port-mirror-switch">https://ask.wireshark.org/questions/13892/port-mirror-switch</a></p></blockquote></div><div id="comment-42358-info" class="comment-info"><span class="comment-age">(13 May '15, 05:41)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="42373"></span><div id="comment-42373" class="comment"><div id="post-42373-score" class="comment-score"></div><div class="comment-text"><p>Thanks, I'll check that out.</p></div><div id="comment-42373-info" class="comment-info"><span class="comment-age">(13 May '15, 10:59)</span> <span class="comment-user userinfo">Shartnado</span></div></div></div><div id="comment-tools-42347" class="comment-tools"></div><div class="clear"></div><div id="comment-42347-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

