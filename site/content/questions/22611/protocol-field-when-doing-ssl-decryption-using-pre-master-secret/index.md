+++
type = "question"
title = "Protocol Field  when doing SSL decryption using Pre-master-secret"
description = '''I am using the pre-master-secret to decrypt SSL web traffic. I can see the reassembled and decrypted packets just fine. It works great! Thanks for this feature, by the way. The negotiated version of TLS is TLSv1 for this session but I sometimes see TLSv1 in the protocol field and sometimes see SSL i...'''
date = "2013-07-03T07:39:00Z"
lastmod = "2013-07-03T07:39:00Z"
weight = 22611
keywords = [ "ssl", "decryption" ]
aliases = [ "/questions/22611" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Protocol Field when doing SSL decryption using Pre-master-secret](/questions/22611/protocol-field-when-doing-ssl-decryption-using-pre-master-secret)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22611-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22611-score" class="post-score" title="current number of votes">0</div><span id="post-22611-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using the pre-master-secret to decrypt SSL web traffic. I can see the reassembled and decrypted packets just fine. It works great! Thanks for this feature, by the way. The negotiated version of TLS is TLSv1 for this session but I sometimes see TLSv1 in the protocol field and sometimes see SSL in the protocol field in the same stream. The TLS that has been decrypted is shown as HTTP but the SSL segments of a reassembled PDU are show as either TLSv1 or SSL -- even though it is all supposed to be TLSv1. How is this protocol field determined? Thanks. Sally</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '13, 07:39</strong></p><img src="https://secure.gravatar.com/avatar/671720473b82bfdfafbcfe1f3f96de40?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="serrano&#39;s gravatar image" /><p><span>serrano</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="serrano has no accepted answers">0%</span></p></div></div><div id="comments-container-22611" class="comments-container"></div><div id="comment-tools-22611" class="comment-tools"></div><div class="clear"></div><div id="comment-22611-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

