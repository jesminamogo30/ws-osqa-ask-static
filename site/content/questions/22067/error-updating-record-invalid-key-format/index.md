+++
type = "question"
title = "error updating record: Invalid key format"
description = '''Hello! When I try to input a WPA-PSK key in the IEEE 802.11 decryption place, I get the message in the title. I have tried with ssid:password, and password only. Also, I have input a WEP key earlier (that worked), and even though I delete it and see it dissapear, it reappears if I close the window w...'''
date = "2013-06-14T10:05:00Z"
lastmod = "2013-06-14T10:05:00Z"
weight = 22067
keywords = [ "wpa-psk", "decryption", "wpa", "key", "bug" ]
aliases = [ "/questions/22067" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [error updating record: Invalid key format](/questions/22067/error-updating-record-invalid-key-format)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22067-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22067-score" class="post-score" title="current number of votes">0</div><span id="post-22067-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello! When I try to input a WPA-PSK key in the IEEE 802.11 decryption place, I get the message in the title. I have tried with ssid:password, and password only.</p><p>Also, I have input a WEP key earlier (that worked), and even though I delete it and see it dissapear, it reappears if I close the window with OK, and then reopen it. What should I?</p><p>Thank you, i really appreciate it!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wpa-psk" rel="tag" title="see questions tagged &#39;wpa-psk&#39;">wpa-psk</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-wpa" rel="tag" title="see questions tagged &#39;wpa&#39;">wpa</span> <span class="post-tag tag-link-key" rel="tag" title="see questions tagged &#39;key&#39;">key</span> <span class="post-tag tag-link-bug" rel="tag" title="see questions tagged &#39;bug&#39;">bug</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '13, 10:05</strong></p><img src="https://secure.gravatar.com/avatar/5a82ec592f9ae58b528b4ba33f30588f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cheesedoodal&#39;s gravatar image" /><p><span>cheesedoodal</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cheesedoodal has no accepted answers">0%</span></p></div></div><div id="comments-container-22067" class="comments-container"></div><div id="comment-tools-22067" class="comment-tools"></div><div class="clear"></div><div id="comment-22067-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

