+++
type = "question"
title = "How to analyze sip messages in wireshark logs through tcl automation"
description = '''Can anyone help me how to write tcl script for analysing sip messages in wireshark logs... If possible the code'''
date = "2013-07-18T04:16:00Z"
lastmod = "2013-07-18T04:49:00Z"
weight = 23100
keywords = [ "12ab3" ]
aliases = [ "/questions/23100" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to analyze sip messages in wireshark logs through tcl automation](/questions/23100/how-to-analyze-sip-messages-in-wireshark-logs-through-tcl-automation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23100-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23100-score" class="post-score" title="current number of votes">0</div><span id="post-23100-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can anyone help me how to write tcl script for analysing sip messages in wireshark logs... If possible the code</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-12ab3" rel="tag" title="see questions tagged &#39;12ab3&#39;">12ab3</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '13, 04:16</strong></p><img src="https://secure.gravatar.com/avatar/3556b977f0d31b4b7b6f5347800de71d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="megha&#39;s gravatar image" /><p><span>megha</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="megha has no accepted answers">0%</span></p></div></div><div id="comments-container-23100" class="comments-container"><span id="23101"></span><div id="comment-23101" class="comment"><div id="post-23101-score" class="comment-score"></div><div class="comment-text"><blockquote><p>for analysing sip messages</p></blockquote><ul><li>what are you trying to do?<br />
</li><li>how good is your knowledge of TCL, tshark and SIP?</li><li>is this a business/commerical request?</li></ul></div><div id="comment-23101-info" class="comment-info"><span class="comment-age">(18 Jul '13, 04:49)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-23100" class="comment-tools"></div><div class="clear"></div><div id="comment-23100-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

