+++
type = "question"
title = "How to decode traces from Shark Android app"
description = '''Hi all,  I am getting traces with Shark app in a Android device.  I start the capture with the default parameters (-vv -s 0) that means all the interfaces. I generate traffic such SIP,MSRP and sending some SMS and MMS... When I open the pcap file I only see traces under TCP protocol, how can I decod...'''
date = "2015-09-03T07:57:00Z"
lastmod = "2015-09-03T07:57:00Z"
weight = 45616
keywords = [ "decode", "android", "tcp" ]
aliases = [ "/questions/45616" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to decode traces from Shark Android app](/questions/45616/how-to-decode-traces-from-shark-android-app)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45616-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45616-score" class="post-score" title="current number of votes">0</div><span id="post-45616-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I am getting traces with Shark app in a Android device. I start the capture with the default parameters (-vv -s 0) that means all the interfaces. I generate traffic such SIP,MSRP and sending some SMS and MMS... When I open the pcap file I only see traces under TCP protocol, how can I decode/decrypt the packets in SIP, MSRP, SMPP or other protocols? How can I identify wich TCP packets belongs to each protocol? Here you have the link to the pcap file: <a href="http://www.filedropper.com/sharkdump1441290581">http://www.filedropper.com/sharkdump1441290581</a></p><p>Thank you very much in advance, Regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '15, 07:57</strong></p><img src="https://secure.gravatar.com/avatar/08317d6cd999a98c43fd85807066c6d8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nairda&#39;s gravatar image" /><p><span>Nairda</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nairda has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Sep '15, 08:23</strong> </span></p></div></div><div id="comments-container-45616" class="comments-container"></div><div id="comment-tools-45616" class="comment-tools"></div><div class="clear"></div><div id="comment-45616-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

