+++
type = "question"
title = "wireshark 1.12.4 fails to install os x 10.7.5"
description = '''wireshark 1.12.4 fails to install os x 10.7.5. tried several times :('''
date = "2015-03-05T12:26:00Z"
lastmod = "2015-03-05T17:39:00Z"
weight = 40299
keywords = [ "wireshark-1.12" ]
aliases = [ "/questions/40299" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark 1.12.4 fails to install os x 10.7.5](/questions/40299/wireshark-1124-fails-to-install-os-x-1075)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40299-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40299-score" class="post-score" title="current number of votes">0</div><span id="post-40299-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2015-03-05_at_12.17.21_PM.png" alt="alt text" />wireshark 1.12.4 fails to install os x 10.7.5. tried several times :(</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark-1.12" rel="tag" title="see questions tagged &#39;wireshark-1.12&#39;">wireshark-1.12</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '15, 12:26</strong></p><img src="https://secure.gravatar.com/avatar/96f616fac276e6b4215bbfa7b0ef5043?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="orca&#39;s gravatar image" /><p><span>orca</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="orca has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Mar '15, 12:31</strong> </span></p></div></div><div id="comments-container-40299" class="comments-container"></div><div id="comment-tools-40299" class="comment-tools"></div><div class="clear"></div><div id="comment-40299-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40300"></span>

<div id="answer-container-40300" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40300-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40300-score" class="post-score" title="current number of votes">0</div><span id="post-40300-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This post is way better kept over at the <a href="http://bugs.wireshark.org">bug tracker</a>, don't you think? Can you please open a bug over there? :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Mar '15, 12:44</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-40300" class="comments-container"><span id="40313"></span><div id="comment-40313" class="comment"><div id="post-40313-score" class="comment-score"></div><div class="comment-text"><p>Yes, please do. This isn't the sort of "what am I doing wrong here?" or "what do I need to do here?" questions for which this site was intended, this is more of a "what went wrong here and what's wrong with Wireshark?" question that's best addressed through the bug tracker, as it might not be something orca's doing wrong, it might be something the <em>Wireshark installer</em> is doing wrong.</p><p>In order to find that out, we'll probably have to ask them for more information, and the bug tracker's the best place to do that.</p></div><div id="comment-40313-info" class="comment-info"><span class="comment-age">(05 Mar '15, 17:39)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-40300" class="comment-tools"></div><div class="clear"></div><div id="comment-40300-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

