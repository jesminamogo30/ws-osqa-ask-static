+++
type = "question"
title = "Adding/appending packets comment in pcapng file"
description = '''Hi,  I would like to add comment inside the &quot;packet comments&quot; field of pcapng using a script.  Use case would be something like: if dns.ttl &amp;lt; 120 sec:  add_comments(&quot;Short TLL&quot;) I&#x27;ve been reading and it&#x27;s probably doable using a LUA script but before today I never wrote a line of LUA. I would lik...'''
date = "2013-05-22T20:35:00Z"
lastmod = "2013-05-23T05:27:00Z"
weight = 21388
keywords = [ "comment", "lua", "pcapng" ]
aliases = [ "/questions/21388" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Adding/appending packets comment in pcapng file](/questions/21388/addingappending-packets-comment-in-pcapng-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21388-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21388-score" class="post-score" title="current number of votes">0</div><span id="post-21388-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I would like to add comment inside the "packet comments" field of pcapng using a script.</p><p>Use case would be something like: if dns.ttl &lt; 120 sec: add_comments("Short TLL")</p><p>I've been reading and it's probably doable using a LUA script but before today I never wrote a line of LUA. I would like to know if it is indeed doable and if someone could send me in the right direction to get that done.</p><p>thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-comment" rel="tag" title="see questions tagged &#39;comment&#39;">comment</span> <span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-pcapng" rel="tag" title="see questions tagged &#39;pcapng&#39;">pcapng</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '13, 20:35</strong></p><img src="https://secure.gravatar.com/avatar/49ecbb0f1ab3ef9c285daca10bf6df89?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="x0r&#39;s gravatar image" /><p><span>x0r</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="x0r has no accepted answers">0%</span></p></div></div><div id="comments-container-21388" class="comments-container"><span id="21404"></span><div id="comment-21404" class="comment"><div id="post-21404-score" class="comment-score"></div><div class="comment-text"><p>I guess you want to highlight certain packets that way - have you thought about using coloring rules for something like this?</p></div><div id="comment-21404-info" class="comment-info"><span class="comment-age">(23 May '13, 05:27)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-21388" class="comment-tools"></div><div class="clear"></div><div id="comment-21388-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

