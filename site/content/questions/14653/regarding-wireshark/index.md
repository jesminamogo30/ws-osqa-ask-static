+++
type = "question"
title = "Regarding wireshark"
description = '''DESCRIBE THE STEPS TO DISPLAY A SINGLE CONVERSATION /TRACE FROM A NETWORK CAPTURE FILE using wireshark'''
date = "2012-10-02T21:57:00Z"
lastmod = "2012-10-04T02:25:00Z"
weight = 14653
keywords = [ "conversation", "single" ]
aliases = [ "/questions/14653" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Regarding wireshark](/questions/14653/regarding-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14653-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14653-score" class="post-score" title="current number of votes">-1</div><span id="post-14653-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>DESCRIBE THE STEPS TO DISPLAY A SINGLE CONVERSATION /TRACE FROM A NETWORK CAPTURE FILE using wireshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-conversation" rel="tag" title="see questions tagged &#39;conversation&#39;">conversation</span> <span class="post-tag tag-link-single" rel="tag" title="see questions tagged &#39;single&#39;">single</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Oct '12, 21:57</strong></p><img src="https://secure.gravatar.com/avatar/91abbe9723022eb7e582575cd2cd28cd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bhuma&#39;s gravatar image" /><p><span>bhuma</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bhuma has no accepted answers">0%</span></p></div></div><div id="comments-container-14653" class="comments-container"></div><div id="comment-tools-14653" class="comment-tools"></div><div class="clear"></div><div id="comment-14653-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14682"></span>

<div id="answer-container-14682" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14682-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14682-score" class="post-score" title="current number of votes">2</div><span id="post-14682-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yay, homework...</p><p>Take a look at Statistics -&gt; Conversations, play around with the tabs and sorting columns and using the popup menu. Figure out the rest by yourself, you should learn something ;-)</p><p>BTW your keyboard seems broken, the Caps lock key is stuck. Do not shout at people, it's rude, especially for someone trying to dodge his homework assignment and looking for help.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Oct '12, 09:55</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Oct '12, 09:56</strong> </span></p></div></div><div id="comments-container-14682" class="comments-container"><span id="14695"></span><div id="comment-14695" class="comment"><div id="post-14695-score" class="comment-score"></div><div class="comment-text"><p>I dare anyone to drink a few glasses of wine, then read, <em>"Yay, homework..."</em> and not laugh. You sir are awarded a point! :)</p></div><div id="comment-14695-info" class="comment-info"><span class="comment-age">(03 Oct '12, 19:51)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="14700"></span><div id="comment-14700" class="comment"><div id="post-14700-score" class="comment-score"></div><div class="comment-text"><p>Thank you, my pleaure :-)</p></div><div id="comment-14700-info" class="comment-info"><span class="comment-age">(04 Oct '12, 02:25)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-14682" class="comment-tools"></div><div class="clear"></div><div id="comment-14682-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

