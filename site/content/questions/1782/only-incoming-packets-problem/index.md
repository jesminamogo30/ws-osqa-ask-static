+++
type = "question"
title = "Only incoming packets problem"
description = '''Hi All, I&#x27;m trying to capture the packets on my wifi router at home and for some reason i can only see incoming packets for my remote PC, no outgoing packets. I&#x27;m running wireshark in promiscuous mode on latest ubuntu distro. And i put my wireless card into monitor mode before starting the capture. ...'''
date = "2011-01-18T00:48:00Z"
lastmod = "2011-01-18T04:47:00Z"
weight = 1782
keywords = [ "one-way" ]
aliases = [ "/questions/1782" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Only incoming packets problem](/questions/1782/only-incoming-packets-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1782-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1782-score" class="post-score" title="current number of votes">0</div><span id="post-1782-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I'm trying to capture the packets on my wifi router at home and for some reason i can only see incoming packets for my remote PC, no outgoing packets.</p><p>I'm running wireshark in promiscuous mode on latest ubuntu distro. And i put my wireless card into monitor mode before starting the capture.</p><p>My wireless card is Alfa AWUS036H with mac80211 driver.</p><p>Any help will be much appreciated</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-one-way" rel="tag" title="see questions tagged &#39;one-way&#39;">one-way</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '11, 00:48</strong></p><img src="https://secure.gravatar.com/avatar/36466285ce183ff6725247b40caac961?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Michael&#39;s gravatar image" /><p><span>Michael</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Michael has no accepted answers">0%</span></p></div></div><div id="comments-container-1782" class="comments-container"></div><div id="comment-tools-1782" class="comment-tools"></div><div class="clear"></div><div id="comment-1782-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1787"></span>

<div id="answer-container-1787" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1787-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1787-score" class="post-score" title="current number of votes">0</div><span id="post-1787-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Are you connected with the same Alfa Card to the wireless network you are monitoring ?</p><p>Because of Wi-Fi being half-duplex, your NIC can only send or recieve with the single antenna card. That might be an explanation why you are not able to monitor your own outgoing traffic, because your card might not be able to monitor its own packets simultaneously...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jan '11, 03:07</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jan '11, 03:07</strong> </span></p></div></div><div id="comments-container-1787" class="comments-container"><span id="1788"></span><div id="comment-1788" class="comment"><div id="post-1788-score" class="comment-score"></div><div class="comment-text"><p>I'm able to monitor my own outgoing/incoming traffic. The problem only with outgoing traffic of remote device/machine</p></div><div id="comment-1788-info" class="comment-info"><span class="comment-age">(18 Jan '11, 04:47)</span> <span class="comment-user userinfo">Michael</span></div></div></div><div id="comment-tools-1787" class="comment-tools"></div><div class="clear"></div><div id="comment-1787-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

