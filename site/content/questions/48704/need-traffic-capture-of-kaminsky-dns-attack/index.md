+++
type = "question"
title = "Need traffic Capture of Kaminsky DNS attack."
description = '''Hi I need traffic capture of Kaminsky DNS attack I tried to find it on google but was unable to find one, can anybody share or give me link of website where I can download that ? Thanks in advance'''
date = "2015-12-24T11:25:00Z"
lastmod = "2015-12-24T11:25:00Z"
weight = 48704
keywords = [ "attack", "malicious", "udp", "dns" ]
aliases = [ "/questions/48704" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Need traffic Capture of Kaminsky DNS attack.](/questions/48704/need-traffic-capture-of-kaminsky-dns-attack)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48704-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48704-score" class="post-score" title="current number of votes">0</div><span id="post-48704-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I need traffic capture of Kaminsky DNS attack I tried to find it on google but was unable to find one, can anybody share or give me link of website where I can download that ?</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-attack" rel="tag" title="see questions tagged &#39;attack&#39;">attack</span> <span class="post-tag tag-link-malicious" rel="tag" title="see questions tagged &#39;malicious&#39;">malicious</span> <span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Dec '15, 11:25</strong></p><img src="https://secure.gravatar.com/avatar/0032ac169dfa9b4487cca759adaf8097?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Muhammad%20Irshad&#39;s gravatar image" /><p><span>Muhammad Irshad</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Muhammad Irshad has no accepted answers">0%</span></p></div></div><div id="comments-container-48704" class="comments-container"></div><div id="comment-tools-48704" class="comment-tools"></div><div class="clear"></div><div id="comment-48704-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

