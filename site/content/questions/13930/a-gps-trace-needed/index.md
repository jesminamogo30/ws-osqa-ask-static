+++
type = "question"
title = "A-GPS Trace Needed"
description = '''Hi all, Does someone have A-GPS (MS-Based and MS-Assisted GPS) trace for Lb (BSC-SMLC) interface? Also if possible please send me RRLP and RRC traces. Thanks a lot. Mert'''
date = "2012-08-28T05:00:00Z"
lastmod = "2012-08-28T05:00:00Z"
weight = 13930
keywords = [ "ms-assisted", "ms-based", "gps" ]
aliases = [ "/questions/13930" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [A-GPS Trace Needed](/questions/13930/a-gps-trace-needed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13930-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13930-score" class="post-score" title="current number of votes">0</div><span id="post-13930-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>Does someone have A-GPS (MS-Based and MS-Assisted GPS) trace for Lb (BSC-SMLC) interface? Also if possible please send me RRLP and RRC traces.</p><p>Thanks a lot. Mert</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ms-assisted" rel="tag" title="see questions tagged &#39;ms-assisted&#39;">ms-assisted</span> <span class="post-tag tag-link-ms-based" rel="tag" title="see questions tagged &#39;ms-based&#39;">ms-based</span> <span class="post-tag tag-link-gps" rel="tag" title="see questions tagged &#39;gps&#39;">gps</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '12, 05:00</strong></p><img src="https://secure.gravatar.com/avatar/2584e08939fe45116233ebd47e9ad09e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="antialias&#39;s gravatar image" /><p><span>antialias</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="antialias has no accepted answers">0%</span></p></div></div><div id="comments-container-13930" class="comments-container"></div><div id="comment-tools-13930" class="comment-tools"></div><div class="clear"></div><div id="comment-13930-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

