+++
type = "question"
title = "Correct platform to develop on?"
description = '''We wish to build custom dissectors. We will be running Wireshark on windows. Does our development need to happen on a windows enviorment, or can it be done on Linux? thanks Steve'''
date = "2015-08-03T06:35:00Z"
lastmod = "2015-08-03T06:44:00Z"
weight = 44771
keywords = [ "development" ]
aliases = [ "/questions/44771" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Correct platform to develop on?](/questions/44771/correct-platform-to-develop-on)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44771-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44771-score" class="post-score" title="current number of votes">0</div><span id="post-44771-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We wish to build custom dissectors. We will be running Wireshark on windows. Does our development need to happen on a windows enviorment, or can it be done on Linux? thanks Steve</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '15, 06:35</strong></p><img src="https://secure.gravatar.com/avatar/575922bd3f5d72b465eac18a045acfd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="steve_merc&#39;s gravatar image" /><p><span>steve_merc</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="steve_merc has no accepted answers">0%</span></p></div></div><div id="comments-container-44771" class="comments-container"></div><div id="comment-tools-44771" class="comment-tools"></div><div class="clear"></div><div id="comment-44771-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44773"></span>

<div id="answer-container-44773" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44773-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44773-score" class="post-score" title="current number of votes">0</div><span id="post-44773-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can build on Windows or Linux or OSX, whatever you prefer. See the <a href="https://www.wireshark.org/docs/wsdg_html_chunked/">Wireshark Developers Guide</a> for complete instructions on setting up the build environment.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Aug '15, 06:43</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Aug '15, 06:44</strong> </span></p></div></div><div id="comments-container-44773" class="comments-container"><span id="44776"></span><div id="comment-44776" class="comment"><div id="post-44776-score" class="comment-score"></div><div class="comment-text"><p>Although if you want to create a Windows installer, then running the build on Windows is your only option.</p></div><div id="comment-44776-info" class="comment-info"><span class="comment-age">(03 Aug '15, 06:44)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-44773" class="comment-tools"></div><div class="clear"></div><div id="comment-44773-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

