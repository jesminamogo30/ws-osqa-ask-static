+++
type = "question"
title = "Network sniff"
description = '''I want to setup my laptop to monitor two ports on a Cisco Catalyst switch that have phone equipment connected to them. Can I do this with Wireshark installed on my laptop? Will it run on Win 7 Pro?'''
date = "2011-09-21T11:55:00Z"
lastmod = "2011-09-21T12:46:00Z"
weight = 6480
keywords = [ "sniffing" ]
aliases = [ "/questions/6480" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Network sniff](/questions/6480/network-sniff)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6480-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6480-score" class="post-score" title="current number of votes">0</div><span id="post-6480-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to setup my laptop to monitor two ports on a Cisco Catalyst switch that have phone equipment connected to them. Can I do this with Wireshark installed on my laptop? Will it run on Win 7 Pro?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Sep '11, 11:55</strong></p><img src="https://secure.gravatar.com/avatar/68ded6480a81132be50cf3ad87330b83?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="obnetadmin&#39;s gravatar image" /><p><span>obnetadmin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="obnetadmin has no accepted answers">0%</span></p></div></div><div id="comments-container-6480" class="comments-container"></div><div id="comment-tools-6480" class="comment-tools"></div><div class="clear"></div><div id="comment-6480-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6481"></span>

<div id="answer-container-6481" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6481-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6481-score" class="post-score" title="current number of votes">0</div><span id="post-6481-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, you could. Setup a span port on the Cisco, install Wireshark on your laptop, connect it to the span port and start capturing.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Sep '11, 12:46</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-6481" class="comments-container"></div><div id="comment-tools-6481" class="comment-tools"></div><div class="clear"></div><div id="comment-6481-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

