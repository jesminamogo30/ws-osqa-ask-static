+++
type = "question"
title = "USBPcap/KB3033929"
description = '''Hi, I installed the last version of WireShark (2.0.3) with USBPcap embeded. During the installation it requires the KB3033929 package. After that I Installed the KB3033929 and tried to install again USBPcapSetup-1.1.0.0-g794bf26-3.exe. But it gives the same message, KB3033929 required!!! KB3033929 i...'''
date = "2016-05-03T03:01:00Z"
lastmod = "2016-12-23T01:34:00Z"
weight = 52166
keywords = [ "kb3033929", "usbpcap" ]
aliases = [ "/questions/52166" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [USBPcap/KB3033929](/questions/52166/usbpcapkb3033929)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52166-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52166-score" class="post-score" title="current number of votes">0</div><span id="post-52166-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I installed the last version of WireShark (2.0.3) with USBPcap embeded. During the installation it requires the KB3033929 package. After that I Installed the KB3033929 and tried to install again USBPcapSetup-1.1.0.0-g794bf26-3.exe. But it gives the same message, KB3033929 required!!! KB3033929 is really installed, I tested it. Is there a way to install it manually? ( I can extract the files with 7zip...) Thanks!!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-kb3033929" rel="tag" title="see questions tagged &#39;kb3033929&#39;">kb3033929</span> <span class="post-tag tag-link-usbpcap" rel="tag" title="see questions tagged &#39;usbpcap&#39;">usbpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 May '16, 03:01</strong></p><img src="https://secure.gravatar.com/avatar/d227669d78eefc2bc442982e3d8d408b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jean-Marc&#39;s gravatar image" /><p><span>Jean-Marc</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jean-Marc has no accepted answers">0%</span></p></div></div><div id="comments-container-52166" class="comments-container"></div><div id="comment-tools-52166" class="comment-tools"></div><div class="clear"></div><div id="comment-52166-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="52168"></span>

<div id="answer-container-52168" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52168-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52168-score" class="post-score" title="current number of votes">0</div><span id="post-52168-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>USBPcap is an external project that Wireshark uses, we just bundle their installer. Maybe contact them direct: <a href="http://desowin.org/usbpcap/">http://desowin.org/usbpcap/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 May '16, 03:26</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-52168" class="comments-container"></div><div id="comment-tools-52168" class="comment-tools"></div><div class="clear"></div><div id="comment-52168-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="52172"></span>

<div id="answer-container-52172" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52172-score" class="post-score" title="current number of votes">0</div><span id="post-52172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The SHA-2 KB detection was added by Wireshark project as the driver is signed with a SHA-2 certificate.</p><p>USBPcap installer is using the following method to detect KB3033929 installation:</p><pre><code>%COMSPEC% /C wmic qfe get Hotfixid | findstr KB3033929</code></pre><p>As suggested <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11766">here</a>. If you execute the command manually and the KB is installed, in theory you should get the following output:</p><pre><code>KB3033929</code></pre><p>You could try to install it manually by unzipping the NSIS script, but it requires selecting the right .sys file... You might have more chance downloading USBPcap installer from <a href="https://anonsvn.wireshark.org/viewvc/trunk/packages/USBPcapSetup-1.1.0.0-g794bf26-3.exe?view=co&amp;revision=481&amp;root=Wireshark-win32-libs">here</a> (that's the one embedded in Wireshark 2.0.3 installer), right click on the file, select properties -&gt; compatibility -&gt; run as Windows Vista (this will skip ghr KB3033929 check that is run on Windows 7 only).</p><p>But really ensure that KB3033929 is installed. Otherwise it will probably make your USB root inoperative (you can use the system restore point created during installation to recover).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 May '16, 05:07</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-52172" class="comments-container"><span id="52174"></span><div id="comment-52174" class="comment"><div id="post-52174-score" class="comment-score"></div><div class="comment-text"><p>As suggested in another Question here, be sure to to enable the Remote Desktop service before you take the adventure of installing the USBPcap's USB driver. Unless you have a notebook or a very old PC with PS/2 keyboard and mouse connector, getting access to the PC without any USB peripheral working is close to impossible, i.e. you wouldn't even be able to choose the recovery point.</p></div><div id="comment-52174-info" class="comment-info"><span class="comment-age">(03 May '16, 05:54)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="52217"></span><div id="comment-52217" class="comment"><div id="post-52217-score" class="comment-score"></div><div class="comment-text"><p>The detect method gives the good answer:</p><p>C:&gt;%COMSPEC% /C wmic qfe get Hotfixid | findstr KB3033929</p><p>KB3033929</p><p>C:&gt;</p><p>But the KB is not detected:</p><p><img src="https://osqa-ask.wireshark.org/upfiles/KB3033029.png" alt="alt text" /></p><p>If I try the Vista compatibility mode, <strong>the install fails</strong>...</p><p><img src="https://osqa-ask.wireshark.org/upfiles/failed.png" alt="alt text" /></p><p>Some files are installed, but not the system files!</p><p><img src="https://osqa-ask.wireshark.org/upfiles/end_install.png" alt="alt text" /></p><p>And it does not work in WireShark!!!</p><p>I am on Windows 7 64 bits French.</p></div><div id="comment-52217-info" class="comment-info"><span class="comment-age">(04 May '16, 01:38)</span> <span class="comment-user userinfo">Jean-Marc</span></div></div><span id="52223"></span><div id="comment-52223" class="comment"><div id="post-52223-score" class="comment-score"></div><div class="comment-text"><p>So far The only time I saw an installation failing was when a previous USBPcap version was uninstalled, and not reboot happened before the next installation attempt. That said I reviewed the installer options, and saw that we provide different drivers for Vista and Win7 (I did not realize this when I thought about the compatibility mode workaround. This could be also the root cause of the installation failure.</p><p>For the KB detection, the only remaining issue I could think to is to confirm that the installed is run with administrator privileges. Could you please uninstall any previously failed USBPcap installation, reboot and try to install it again with administrator rights (without the Vista compatibility mode)? If it still fails I'm out of ideas why... :(</p></div><div id="comment-52223-info" class="comment-info"><span class="comment-age">(04 May '16, 04:37)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div><span id="52224"></span><div id="comment-52224" class="comment"><div id="post-52224-score" class="comment-score"></div><div class="comment-text"><p>OK there is actually another solution: use an earlier version of USBPcap installer that was not doing the KB installation check (the USBPcap code itself is the same):</p><ul><li><p>download the file from here: <a href="https://anonsvn.wireshark.org/viewvc/tags/2015-10-07/packages/USBPcapSetup-1.1.0.0-g794bf26.exe?view=co&amp;revision=473&amp;root=Wireshark-win32-libs">https://anonsvn.wireshark.org/viewvc/tags/2015-10-07/packages/USBPcapSetup-1.1.0.0-g794bf26.exe?view=co&amp;revision=473&amp;root=Wireshark-win32-libs</a></p></li><li><p>install it (it will not check the KB presence)</p></li><li><p>once installed, reboot</p></li><li><p>go to USBPcap installation folder</p></li><li><p>copy USBPcapCMD.exe file in C:\Program Files\Wireshark\extcap\ folder</p></li></ul></div><div id="comment-52224-info" class="comment-info"><span class="comment-age">(04 May '16, 05:54)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-52172" class="comment-tools"></div><div class="clear"></div><div id="comment-52172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="58313"></span>

<div id="answer-container-58313" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58313-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58313-score" class="post-score" title="current number of votes">0</div><span id="post-58313-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This error is actually a problem with Windows Management Instrumentation. Their command-line interface for the API misses, causing QFE not to report rightly. You can test for this problem--and search for solutions--by using this command in a Command Prompt:</p><p><code>wmic /TRACE:ON qfe get Hotfixid</code></p><p>The resulting log usually ends with Error 0x8007007e, "The specified module could not be found." Oddly enough, if you run <code>wbemtest</code> and connect to <code>root\cimv2</code> before executing the trace...you can enumerate instances of Win32_QuickFixEngineering and get a full list of Hotfixes popping out in the query results box.</p><p>So QFE runs fine, but the problem is with WMIC actually returning the output to the API for USBPcapSetup to see. I've tried rebuilding WMI from scratch, reinstalling, rebooting...you name it. The error persists. I think something is missing from texttable.xml. If anyone has a working WMIC, could you please post texttable.xml somewhere for me? I'd like to perma-kill this bug dead.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Dec '16, 01:34</strong></p><img src="https://secure.gravatar.com/avatar/2213b443a3e48a872abe4e978959c2b2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="boinkitsbroken&#39;s gravatar image" /><p><span>boinkitsbroken</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="boinkitsbroken has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Dec '16, 01:35</strong> </span></p></div></div><div id="comments-container-58313" class="comments-container"></div><div id="comment-tools-58313" class="comment-tools"></div><div class="clear"></div><div id="comment-58313-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

