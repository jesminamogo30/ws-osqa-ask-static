+++
type = "question"
title = "detecting static route update contents in a wireshark trace"
description = '''after disabling cdp on router and all its interfaces how can i check in a wireshark trace that what are the ip addresses contained in a static route update?'''
date = "2011-10-01T10:17:00Z"
lastmod = "2011-10-01T10:17:00Z"
weight = 6675
keywords = [ "investigating", "route", "static", "update" ]
aliases = [ "/questions/6675" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [detecting static route update contents in a wireshark trace](/questions/6675/detecting-static-route-update-contents-in-a-wireshark-trace)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6675-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6675-score" class="post-score" title="current number of votes">0</div><span id="post-6675-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>after disabling cdp on router and all its interfaces how can i check in a wireshark trace that what are the ip addresses contained in a static route update?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-investigating" rel="tag" title="see questions tagged &#39;investigating&#39;">investigating</span> <span class="post-tag tag-link-route" rel="tag" title="see questions tagged &#39;route&#39;">route</span> <span class="post-tag tag-link-static" rel="tag" title="see questions tagged &#39;static&#39;">static</span> <span class="post-tag tag-link-update" rel="tag" title="see questions tagged &#39;update&#39;">update</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Oct '11, 10:17</strong></p><img src="https://secure.gravatar.com/avatar/8adada5060ff890de45a6a98d496c26d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="manjeet&#39;s gravatar image" /><p><span>manjeet</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="manjeet has no accepted answers">0%</span></p></div></div><div id="comments-container-6675" class="comments-container"></div><div id="comment-tools-6675" class="comment-tools"></div><div class="clear"></div><div id="comment-6675-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

