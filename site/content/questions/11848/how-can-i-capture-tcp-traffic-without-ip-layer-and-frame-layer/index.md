+++
type = "question"
title = "how can i capture tcp traffic without ip layer and frame layer?"
description = '''Hi all  how can i capture tcp traffic without capturing ip layer and frame layer?  i want to see only tcp layer information execpt L2 and L3 layer information.  in this case, can i make it shows just tcp layer only.  i think, if it is possible, it seems so easy to analysis captured wireshark.  also ...'''
date = "2012-06-12T04:31:00Z"
lastmod = "2012-06-12T05:39:00Z"
weight = 11848
keywords = [ "export" ]
aliases = [ "/questions/11848" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [how can i capture tcp traffic without ip layer and frame layer?](/questions/11848/how-can-i-capture-tcp-traffic-without-ip-layer-and-frame-layer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11848-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11848-score" class="post-score" title="current number of votes">1</div><span id="post-11848-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all how can i capture tcp traffic without capturing ip layer and frame layer?</p><p>i want to see only tcp layer information execpt L2 and L3 layer information. in this case, can i make it shows just tcp layer only. i think, if it is possible, it seems so easy to analysis captured wireshark. also it will make exported txt file very short.</p><p>isn't there anyone knows about this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jun '12, 04:31</strong></p><img src="https://secure.gravatar.com/avatar/27e4d1e97303115b07caf9ba39267f2b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ray_Han&#39;s gravatar image" /><p><span>Ray_Han</span><br />
<span class="score" title="56 reputation points">56</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ray_Han has no accepted answers">0%</span></p></div></div><div id="comments-container-11848" class="comments-container"></div><div id="comment-tools-11848" class="comment-tools"></div><div class="clear"></div><div id="comment-11848-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11851"></span>

<div id="answer-container-11851" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11851-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11851-score" class="post-score" title="current number of votes">0</div><span id="post-11851-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Ray_Han has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Look at <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChAdvFollowTCPSection.html">follow tcp streams</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jun '12, 05:39</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-11851" class="comments-container"></div><div id="comment-tools-11851" class="comment-tools"></div><div class="clear"></div><div id="comment-11851-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

