+++
type = "question"
title = "From the capture determine the average size and average data rate of the HTTP packets? Explain what you need to do in Wireshark to obtain this automatically?"
description = '''I am new to wire shark and need help to some of the question to understand the topics What is the version of Web browser and that of the webserver? What languages the web browser can support? Provide a screenshot to indicate your answer on it. What is the elapsed time since a request for webpage is ...'''
date = "2015-04-15T19:28:00Z"
lastmod = "2015-04-15T23:23:00Z"
weight = 41469
keywords = [ "average", "filesize", "homework" ]
aliases = [ "/questions/41469" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [From the capture determine the average size and average data rate of the HTTP packets? Explain what you need to do in Wireshark to obtain this automatically?](/questions/41469/from-the-capture-determine-the-average-size-and-average-data-rate-of-the-http-packets-explain-what-you-need-to-do-in-wireshark-to-obtain-this-automatically)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41469-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41469-score" class="post-score" title="current number of votes">0</div><span id="post-41469-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am new to wire shark and need help to some of the question to understand the topics</p><p>What is the version of Web browser and that of the webserver? What languages the web browser can support? Provide a screenshot to indicate your answer on it.</p><p>What is the elapsed time since a request for webpage is made. Provide a screenshot to indicate your answer on it.</p><p>Is HTTP a reliable protocol? Justify your answer. How does the capture differ if you download a long HTML file (greater than 1 MB) from a webserver? Explain your answer.</p><p>Examine one of the captured HTTP packets and determine IP address and port number of the client and the IP address and port number of the server? Provide a screenshot to indicate your answer on it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-average" rel="tag" title="see questions tagged &#39;average&#39;">average</span> <span class="post-tag tag-link-filesize" rel="tag" title="see questions tagged &#39;filesize&#39;">filesize</span> <span class="post-tag tag-link-homework" rel="tag" title="see questions tagged &#39;homework&#39;">homework</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '15, 19:28</strong></p><img src="https://secure.gravatar.com/avatar/0e3391b1822698562b27e576d377240b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jinendra120&#39;s gravatar image" /><p><span>Jinendra120</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jinendra120 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Apr '15, 20:42</strong> </span></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span></p></div></div><div id="comments-container-41469" class="comments-container"><span id="41474"></span><div id="comment-41474" class="comment"><div id="post-41474-score" class="comment-score"></div><div class="comment-text"><p>Nice homework assignment.In other word: we're not going to do that for you, you have to show to do some research. Also teachers are known to monitor this site, so be aware of that.</p></div><div id="comment-41474-info" class="comment-info"><span class="comment-age">(15 Apr '15, 23:23)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-41469" class="comment-tools"></div><div class="clear"></div><div id="comment-41469-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

