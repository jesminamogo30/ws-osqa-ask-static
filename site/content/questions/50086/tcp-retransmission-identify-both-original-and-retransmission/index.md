+++
type = "question"
title = "TCP retransmission: identify both original and retransmission"
description = '''Hi, Finding retransmissions using tshark or wireshark seems to be quite simple, using the tcp.analysis.retransmission or tcp.analysis.fast_retransmission display filters. However, my question is with regard to the segments that are flagged by these filters. Do they flag both the initial transmission...'''
date = "2016-02-11T01:05:00Z"
lastmod = "2016-02-11T02:01:00Z"
weight = 50086
keywords = [ "tcp.analysis.flags", "tshark", "tcp" ]
aliases = [ "/questions/50086" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [TCP retransmission: identify both original and retransmission](/questions/50086/tcp-retransmission-identify-both-original-and-retransmission)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50086-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50086-score" class="post-score" title="current number of votes">0</div><span id="post-50086-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Finding retransmissions using tshark or wireshark seems to be quite simple, using the tcp.analysis.retransmission or tcp.analysis.fast_retransmission display filters. However, my question is with regard to the segments that are flagged by these filters. Do they flag both the initial transmission and the retransmission (and later ones) or do they tag only the first retransmitted segment (and later ones). I have not been able to find a reference for this.</p><p>Appreciate your help! /Jamie</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp.analysis.flags" rel="tag" title="see questions tagged &#39;tcp.analysis.flags&#39;">tcp.analysis.flags</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Feb '16, 01:05</strong></p><img src="https://secure.gravatar.com/avatar/37061b341c1052b08cea844320cb71a9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jamie_unix&#39;s gravatar image" /><p><span>Jamie_unix</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jamie_unix has no accepted answers">0%</span></p></div></div><div id="comments-container-50086" class="comments-container"><span id="50088"></span><div id="comment-50088" class="comment"><div id="post-50088-score" class="comment-score"></div><div class="comment-text"><p>My tests lead me to conclude that it tags only the retransmissions. So, was wondering if there is a filter with which I can flag both the originals and the retransmissions?</p></div><div id="comment-50088-info" class="comment-info"><span class="comment-age">(11 Feb '16, 01:18)</span> <span class="comment-user userinfo">Jamie_unix</span></div></div></div><div id="comment-tools-50086" class="comment-tools"></div><div class="clear"></div><div id="comment-50086-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50090"></span>

<div id="answer-container-50090" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50090-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50090-score" class="post-score" title="current number of votes">0</div><span id="post-50090-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Jamie_unix has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The original will not be tagged, only the retransmitted copies. You need to find the originals by searching for the sequence number found in the retransmitted packet.</p><p>For further reference about packet dependency filtering you might want to take a look at this:</p><p><a href="https://blog.packet-foo.com/2015/03/advanced-display-filtering/">https://blog.packet-foo.com/2015/03/advanced-display-filtering/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Feb '16, 01:43</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Feb '16, 01:44</strong> </span></p></div></div><div id="comments-container-50090" class="comments-container"><span id="50091"></span><div id="comment-50091" class="comment"><div id="post-50091-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jasper. I will look into that. Is there a quick way to get seq numbers of the retransmissions? By default it does not seem to show them.</p></div><div id="comment-50091-info" class="comment-info"><span class="comment-age">(11 Feb '16, 01:52)</span> <span class="comment-user userinfo">Jamie_unix</span></div></div><span id="50092"></span><div id="comment-50092" class="comment"><div id="post-50092-score" class="comment-score"></div><div class="comment-text"><p>Figuring that out was straight forward. just requires -T fields -e tcp.seq.</p></div><div id="comment-50092-info" class="comment-info"><span class="comment-age">(11 Feb '16, 02:01)</span> <span class="comment-user userinfo">Jamie_unix</span></div></div></div><div id="comment-tools-50090" class="comment-tools"></div><div class="clear"></div><div id="comment-50090-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

