+++
type = "question"
title = "Unable to edit my comments"
description = '''I can edit my comments, as long as I am logged in to Ask Wireshark. After logging out and in, I cannot edit my comments anymore. The only option I get is to delete them. BTW I still can edit|delete my answers.'''
date = "2011-05-05T06:44:00Z"
lastmod = "2011-05-05T07:46:00Z"
weight = 3937
keywords = [ "ask.wireshark.org" ]
aliases = [ "/questions/3937" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Unable to edit my comments](/questions/3937/unable-to-edit-my-comments)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3937-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3937-score" class="post-score" title="current number of votes">0</div><span id="post-3937-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can edit my comments, as long as I am logged in to Ask Wireshark.<br />
After logging out and in, I cannot edit my comments anymore. The only option I get is to delete them.<br />
<br />
BTW<br />
I still can edit|delete my answers.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '11, 06:44</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span> </br></br></p></div></div><div id="comments-container-3937" class="comments-container"></div><div id="comment-tools-3937" class="comment-tools"></div><div class="clear"></div><div id="comment-3937-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3939"></span>

<div id="answer-container-3939" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3939-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3939-score" class="post-score" title="current number of votes">2</div><span id="post-3939-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="joke has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's actually a feature: you only have 60 minutes to edit your comments. But one of the OSQA developers <a href="http://meta.osqa.net/questions/5665/feature-or-bug-cant-edit-comments-anymore">posted</a> that newer versions allow this to be configurable [by an admin].</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 May '11, 07:22</strong></p><img src="https://secure.gravatar.com/avatar/aa651167cb1d51fa9dca1212f1123bfa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bstn&#39;s gravatar image" /><p><span>bstn</span><br />
<span class="score" title="375 reputation points">375</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bstn has 4 accepted answers">14%</span> </br></br></p></div></div><div id="comments-container-3939" class="comments-container"><span id="3941"></span><div id="comment-3941" class="comment"><div id="post-3941-score" class="comment-score"></div><div class="comment-text"><p>Thank you.<br />
BTW<br />
"comments, after 60 minutes, are no longer editable, exept for moderators or administrators"<br />
So you can always ask Sake:)</p></div><div id="comment-3941-info" class="comment-info"><span class="comment-age">(05 May '11, 07:46)</span> <span class="comment-user userinfo">joke</span></div></div></div><div id="comment-tools-3939" class="comment-tools"></div><div class="clear"></div><div id="comment-3939-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

