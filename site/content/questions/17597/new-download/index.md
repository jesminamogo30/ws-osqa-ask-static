+++
type = "question"
title = "new download"
description = '''Looking to download a trial of your software to see if it will do what I need? will I be able to view traffic on a switch that I think im having problems with? can I use your software to monitor and capture information on a switch? please let me know Thank you.'''
date = "2013-01-11T06:52:00Z"
lastmod = "2013-01-11T07:37:00Z"
weight = 17597
keywords = [ "capture", "switch" ]
aliases = [ "/questions/17597" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [new download](/questions/17597/new-download)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17597-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17597-score" class="post-score" title="current number of votes">0</div><span id="post-17597-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Looking to download a trial of your software to see if it will do what I need? will I be able to view traffic on a switch that I think im having problems with? can I use your software to monitor and capture information on a switch? please let me know Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-switch" rel="tag" title="see questions tagged &#39;switch&#39;">switch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '13, 06:52</strong></p><img src="https://secure.gravatar.com/avatar/69d9e9e1cb3eb68cd1a6ffd386d4b386?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jcgvette&#39;s gravatar image" /><p><span>jcgvette</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jcgvette has no accepted answers">0%</span></p></div></div><div id="comments-container-17597" class="comments-container"></div><div id="comment-tools-17597" class="comment-tools"></div><div class="clear"></div><div id="comment-17597-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17598"></span>

<div id="answer-container-17598" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17598-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17598-score" class="post-score" title="current number of votes">0</div><span id="post-17598-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No need for a trial, Wireshark is free, both in terms of use, and ability to modify.</p><p>To capture traffic from a switch you will have to consider your switches capabilities, can it span or mirror a port? What model of switch is it?</p><p>See the <a href="http://wiki.wireshark.org/CaptureSetup">CaptureSetup</a> page on the Wiki for more info.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '13, 06:56</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-17598" class="comments-container"><span id="17601"></span><div id="comment-17601" class="comment"><div id="post-17601-score" class="comment-score"></div><div class="comment-text"><p>It's a HP V1910-16g smart switch it does port mirroring</p></div><div id="comment-17601-info" class="comment-info"><span class="comment-age">(11 Jan '13, 07:27)</span> <span class="comment-user userinfo">jcgvette</span></div></div><span id="17602"></span><div id="comment-17602" class="comment"><div id="post-17602-score" class="comment-score"></div><div class="comment-text"><p>Hopefully the rather fine manual that comes with your switch will tell you how to set up the mirroring, and then connect the PC with Wireshark installed on to the port where the traffic is being mirrored to.</p></div><div id="comment-17602-info" class="comment-info"><span class="comment-age">(11 Jan '13, 07:37)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-17598" class="comment-tools"></div><div class="clear"></div><div id="comment-17598-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

