+++
type = "question"
title = "Wireshark and Airpcap"
description = '''How can I interface airpcap with wireshark to analyse throughput, latency and PSNR?'''
date = "2014-06-17T12:34:00Z"
lastmod = "2014-06-17T12:34:00Z"
weight = 33905
keywords = [ "airpcap", "wireshark" ]
aliases = [ "/questions/33905" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark and Airpcap](/questions/33905/wireshark-and-airpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33905-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33905-score" class="post-score" title="current number of votes">0</div><span id="post-33905-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I interface airpcap with wireshark to analyse throughput, latency and PSNR?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-airpcap" rel="tag" title="see questions tagged &#39;airpcap&#39;">airpcap</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jun '14, 12:34</strong></p><img src="https://secure.gravatar.com/avatar/f0b8218b8e0cd71d4903ea4e675f01d0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="haykhusaiby&#39;s gravatar image" /><p><span>haykhusaiby</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="haykhusaiby has no accepted answers">0%</span></p></div></div><div id="comments-container-33905" class="comments-container"></div><div id="comment-tools-33905" class="comment-tools"></div><div class="clear"></div><div id="comment-33905-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

