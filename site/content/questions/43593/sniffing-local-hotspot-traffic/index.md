+++
type = "question"
title = "Sniffing local hotspot traffic"
description = '''Hi ! I would like to know if it was possible to use Shark (Android) to sniff the wi-fi traffic of the devices using the hotspot my phone broadcasts? I have tried and it seems that all the packages are &quot;RAW&quot;. I must not use the right parameters I believe. For information I am using a LG G2. Cheers!'''
date = "2015-06-26T09:33:00Z"
lastmod = "2015-06-26T09:33:00Z"
weight = 43593
keywords = [ "sniffing", "wifi", "hotspot" ]
aliases = [ "/questions/43593" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Sniffing local hotspot traffic](/questions/43593/sniffing-local-hotspot-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43593-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43593-score" class="post-score" title="current number of votes">0</div><span id="post-43593-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi !</p><p>I would like to know if it was possible to use Shark (Android) to sniff the wi-fi traffic of the devices using the hotspot my phone broadcasts? I have tried and it seems that all the packages are "RAW". I must not use the right parameters I believe. For information I am using a LG G2.</p><p>Cheers!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-hotspot" rel="tag" title="see questions tagged &#39;hotspot&#39;">hotspot</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '15, 09:33</strong></p><img src="https://secure.gravatar.com/avatar/55ee5a8f23ddfb6122246fdb8ceb286f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wireflo&#39;s gravatar image" /><p><span>Wireflo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wireflo has no accepted answers">0%</span></p></div></div><div id="comments-container-43593" class="comments-container"></div><div id="comment-tools-43593" class="comment-tools"></div><div class="clear"></div><div id="comment-43593-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

