+++
type = "question"
title = "Must WinPCap be installed on all interfaces that are looked at?"
description = '''Must WinPCap be installed on all interfaces that are looked at?'''
date = "2012-12-07T12:18:00Z"
lastmod = "2012-12-07T12:37:00Z"
weight = 16704
keywords = [ "winpcap" ]
aliases = [ "/questions/16704" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Must WinPCap be installed on all interfaces that are looked at?](/questions/16704/must-winpcap-be-installed-on-all-interfaces-that-are-looked-at)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16704-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16704-score" class="post-score" title="current number of votes">0</div><span id="post-16704-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Must WinPCap be installed on all interfaces that are looked at?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Dec '12, 12:18</strong></p><img src="https://secure.gravatar.com/avatar/316d026ef774fd19e972ffd4cb8a337e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dbar&#39;s gravatar image" /><p><span>dbar</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dbar has no accepted answers">0%</span></p></div></div><div id="comments-container-16704" class="comments-container"></div><div id="comment-tools-16704" class="comment-tools"></div><div class="clear"></div><div id="comment-16704-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16708"></span>

<div id="answer-container-16708" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16708-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16708-score" class="post-score" title="current number of votes">0</div><span id="post-16708-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No.</p><p><strong>UPDATE</strong></p><p>If you are talking about remote capturing (see <a href="http://ask.wireshark.org/questions/16703/what-blocks-the-addition-of-an-interface">your other question</a>), then: yes, you must install WinPcap on every remote system and start rpcad there.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Dec '12, 12:37</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Dec '12, 13:35</strong> </span></p></div></div><div id="comments-container-16708" class="comments-container"></div><div id="comment-tools-16708" class="comment-tools"></div><div class="clear"></div><div id="comment-16708-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

