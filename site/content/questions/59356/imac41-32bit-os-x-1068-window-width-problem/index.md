+++
type = "question"
title = "iMac4.1 32bit os X 10.6.8 Window width problem"
description = '''I just downloaded Wireshark v 2.1.10 to run on an early intel iMac with 10.6.8. Works ok except that right side of the window is way off the screen. I have tried various settings in Preferences and clicking the green screen button as well as dragging the screen way to the left and dragging the resiz...'''
date = "2017-02-12T10:55:00Z"
lastmod = "2017-02-12T10:55:00Z"
weight = 59356
keywords = [ "scaling", "x", "window", "10.6.8", "os" ]
aliases = [ "/questions/59356" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [iMac4.1 32bit os X 10.6.8 Window width problem](/questions/59356/imac41-32bit-os-x-1068-window-width-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59356-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59356-score" class="post-score" title="current number of votes">0</div><span id="post-59356-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just downloaded Wireshark v 2.1.10 to run on an early intel iMac with 10.6.8. Works ok except that right side of the window is way off the screen. I have tried various settings in Preferences and clicking the green screen button as well as dragging the screen way to the left and dragging the resize corner. I can resize vertically but not horizontally. Anybody seen this before?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-scaling" rel="tag" title="see questions tagged &#39;scaling&#39;">scaling</span> <span class="post-tag tag-link-x" rel="tag" title="see questions tagged &#39;x&#39;">x</span> <span class="post-tag tag-link-window" rel="tag" title="see questions tagged &#39;window&#39;">window</span> <span class="post-tag tag-link-10.6.8" rel="tag" title="see questions tagged &#39;10.6.8&#39;">10.6.8</span> <span class="post-tag tag-link-os" rel="tag" title="see questions tagged &#39;os&#39;">os</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Feb '17, 10:55</strong></p><img src="https://secure.gravatar.com/avatar/d439dff187b431b92e7226d8a9b2e303?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="perge&#39;s gravatar image" /><p><span>perge</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="perge has no accepted answers">0%</span></p></div></div><div id="comments-container-59356" class="comments-container"></div><div id="comment-tools-59356" class="comment-tools"></div><div class="clear"></div><div id="comment-59356-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

