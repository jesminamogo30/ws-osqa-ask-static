+++
type = "question"
title = "Runtime error during launching"
description = '''After installation I try to launch the application and a get this error: http://www.image-share.com/upload/814/188.jpg Already reboot the PC (WinXP 32bit P4 HT 3Ghz 1GB RAM) '''
date = "2011-07-29T10:53:00Z"
lastmod = "2011-07-29T12:17:00Z"
weight = 5362
keywords = [ "windows", "startup", "crash" ]
aliases = [ "/questions/5362" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Runtime error during launching](/questions/5362/runtime-error-during-launching)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5362-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5362-score" class="post-score" title="current number of votes">0</div><span id="post-5362-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After installation I try to launch the application and a get this error: http://www.image-share.com/upload/814/188.jpg</p><p>Already reboot the PC (WinXP 32bit P4 HT 3Ghz 1GB RAM) <img src="http://www.image-share.com/upload/814/188.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-startup" rel="tag" title="see questions tagged &#39;startup&#39;">startup</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jul '11, 10:53</strong></p><img src="https://secure.gravatar.com/avatar/42322ad2a9fb24dc78eae2a99be8be67?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gabriel%20Venturi&#39;s gravatar image" /><p><span>Gabriel Venturi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gabriel Venturi has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Jul '11, 12:00</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-5362" class="comments-container"></div><div id="comment-tools-5362" class="comment-tools"></div><div class="clear"></div><div id="comment-5362-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5363"></span>

<div id="answer-container-5363" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5363-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5363-score" class="post-score" title="current number of votes">1</div><span id="post-5363-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try re-installing Wireshark (and WinPcap) as indicated in the duplicates:</p><ul><li><a href="http://ask.wireshark.org/questions/5029/runtime-error-the-application-has-requested-that-the-runtime-to-terminate">Runtime Error! The Application has requested that the Runtime to Terminate</a></li><li><a href="http://ask.wireshark.org/questions/5207/windows-xp-c-runtime-error">Windows XP C++ runtime error</a></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jul '11, 12:17</strong></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="helloworld has 28 accepted answers">28%</span></p></div></div><div id="comments-container-5363" class="comments-container"></div><div id="comment-tools-5363" class="comment-tools"></div><div class="clear"></div><div id="comment-5363-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

