+++
type = "question"
title = "Please Help Find Stream Url"
description = '''Trying to find the stream url for a radio station. http://player.liquidcompass.net/WJQMFM Although it is obvious that I have not found it, I felt like I tried everything. I know I&#x27;m doing something wrong but I just have no idea. If someone can tell me the stream url as well as steps for finding it w...'''
date = "2016-07-03T00:01:00Z"
lastmod = "2016-07-03T00:01:00Z"
weight = 53792
keywords = [ "url", "stream" ]
aliases = [ "/questions/53792" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Please Help Find Stream Url](/questions/53792/please-help-find-stream-url)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53792-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53792-score" class="post-score" title="current number of votes">0</div><span id="post-53792-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Trying to find the stream url for a radio station.</p><p><a href="http://player.liquidcompass.net/WJQMFM">http://player.liquidcompass.net/WJQMFM</a></p><p>Although it is obvious that I have not found it, I felt like I tried everything. I know I'm doing something wrong but I just have no idea. If someone can tell me the stream url as well as steps for finding it would be gratefully appreciated.</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-url" rel="tag" title="see questions tagged &#39;url&#39;">url</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '16, 00:01</strong></p><img src="https://secure.gravatar.com/avatar/14f359587403bb0c4ed00e09aa8877d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GoBadgers608&#39;s gravatar image" /><p><span>GoBadgers608</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GoBadgers608 has no accepted answers">0%</span></p></div></div><div id="comments-container-53792" class="comments-container"></div><div id="comment-tools-53792" class="comment-tools"></div><div class="clear"></div><div id="comment-53792-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

