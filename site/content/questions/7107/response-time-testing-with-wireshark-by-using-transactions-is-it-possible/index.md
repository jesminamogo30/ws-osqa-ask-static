+++
type = "question"
title = "Response time testing with wireshark by using transaction&#x27;s  --is it possible ?"
description = '''Hi I am going to work on wireshark . I have some quiries here to ask .  I am working on migration project but client d&#x27;nt want to use loadrunner before migrating and after migrating for response time i mean they want to know latency between pre migration and after migration . Can we know response ti...'''
date = "2011-10-27T09:14:00Z"
lastmod = "2011-10-27T09:14:00Z"
weight = 7107
keywords = [ "testing", "response", "time" ]
aliases = [ "/questions/7107" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Response time testing with wireshark by using transaction's --is it possible ?](/questions/7107/response-time-testing-with-wireshark-by-using-transactions-is-it-possible)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7107-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7107-score" class="post-score" title="current number of votes">0</div><span id="post-7107-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I am going to work on wireshark . I have some quiries here to ask .</p><ol><li>I am working on migration project but client d'nt want to use loadrunner before migrating and after migrating for response time i mean they want to know latency between pre migration and after migration . Can we know response time for 3 0r 4 existing trxn's with wireshark ?please let me give answer if anybody knows .</li></ol><p>2.This is the tool for data analysing or for response time also ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-testing" rel="tag" title="see questions tagged &#39;testing&#39;">testing</span> <span class="post-tag tag-link-response" rel="tag" title="see questions tagged &#39;response&#39;">response</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '11, 09:14</strong></p><img src="https://secure.gravatar.com/avatar/3f1f52beb11f4200f22e10872231a100?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="response%20time%20testing&#39;s gravatar image" /><p><span>response tim...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="response time testing has no accepted answers">0%</span></p></div></div><div id="comments-container-7107" class="comments-container"></div><div id="comment-tools-7107" class="comment-tools"></div><div class="clear"></div><div id="comment-7107-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

