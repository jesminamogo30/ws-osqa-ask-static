+++
type = "question"
title = "Version of Wireshark to make .ts file"
description = '''I am looking for a version of Wireshark that will create a .ts MPEG video file from the UDP and PES packets for a multicast. The output from wireshark is a viewable video stream. Is that in the latest version or certain versions? I can&#x27;t seem to find a version with it. Are there extensitons I am not...'''
date = "2012-01-30T10:11:00Z"
lastmod = "2012-01-30T11:33:00Z"
weight = 8707
keywords = [ ".ts" ]
aliases = [ "/questions/8707" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Version of Wireshark to make .ts file](/questions/8707/version-of-wireshark-to-make-ts-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8707-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8707-score" class="post-score" title="current number of votes">0</div><span id="post-8707-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking for a version of Wireshark that will create a .ts MPEG video file from the UDP and PES packets for a multicast. The output from wireshark is a viewable video stream. Is that in the latest version or certain versions? I can't seem to find a version with it. Are there extensitons I am not getting?<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-.ts" rel="tag" title="see questions tagged &#39;.ts&#39;">.ts</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jan '12, 10:11</strong></p><img src="https://secure.gravatar.com/avatar/20f363437e2105dabc161957da6a20cb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="erictelcoguy&#39;s gravatar image" /><p><span>erictelcoguy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="erictelcoguy has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Jan '12, 11:11</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-8707" class="comments-container"></div><div id="comment-tools-8707" class="comment-tools"></div><div class="clear"></div><div id="comment-8707-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8711"></span>

<div id="answer-container-8711" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8711-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8711-score" class="post-score" title="current number of votes">0</div><span id="post-8711-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As far as I know, if anybody's written code to do that, they haven't contributed it to Wireshark; there's no code to do that in any of the standard releases.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jan '12, 11:10</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-8711" class="comments-container"><span id="8713"></span><div id="comment-8713" class="comment"><div id="post-8713-score" class="comment-score"></div><div class="comment-text"><p>I think there was someting like that back in the Ethereal days but the patch newer got accepted and the person making it newer came back with a fixed version. I think it was part of a university project. At some point it was available on the web - try google.</p></div><div id="comment-8713-info" class="comment-info"><span class="comment-age">(30 Jan '12, 11:33)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-8711" class="comment-tools"></div><div class="clear"></div><div id="comment-8711-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

