+++
type = "question"
title = "AirPcap Nx Radio+PPI"
description = '''I have a new AirPCap NX. If I enable Radio+PPI then I get the following error message when I try to capture with wireshark: Unable to set data link type on interface &#x27;&#92;.&#92;airpcap00&#x27; (IEEE802_11_RADIO is not one of the DLTs supported by this device). What does this mean and how do I correct it? Regard...'''
date = "2015-10-01T07:12:00Z"
lastmod = "2015-10-12T15:14:00Z"
weight = 46311
keywords = [ "radio+ppi", "airpcap", "error" ]
aliases = [ "/questions/46311" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [AirPcap Nx Radio+PPI](/questions/46311/airpcap-nx-radioppi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46311-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46311-score" class="post-score" title="current number of votes">0</div><span id="post-46311-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a new AirPCap NX. If I enable Radio+PPI then I get the following error message when I try to capture with wireshark:</p><p>Unable to set data link type on interface '\.\airpcap00' (IEEE802_11_RADIO is not one of the DLTs supported by this device).</p><p>What does this mean and how do I correct it?</p><p>Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-radio+ppi" rel="tag" title="see questions tagged &#39;radio+ppi&#39;">radio+ppi</span> <span class="post-tag tag-link-airpcap" rel="tag" title="see questions tagged &#39;airpcap&#39;">airpcap</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Oct '15, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/6de834f29d6ed5d01600dfb768d23e79?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PAC&#39;s gravatar image" /><p><span>PAC</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PAC has no accepted answers">0%</span></p></div></div><div id="comments-container-46311" class="comments-container"></div><div id="comment-tools-46311" class="comment-tools"></div><div class="clear"></div><div id="comment-46311-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46482"></span>

<div id="answer-container-46482" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46482-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46482-score" class="post-score" title="current number of votes">0</div><span id="post-46482-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>What does this mean and how do I correct it?</p></blockquote><p>Sounds like a good question for the AirPcap vendor support ;-)</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Oct '15, 15:14</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-46482" class="comments-container"></div><div id="comment-tools-46482" class="comment-tools"></div><div class="clear"></div><div id="comment-46482-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

