+++
type = "question"
title = "Having this code error after installing : 0xc000007b"
description = '''Hello everyone ! First, my computer have Windows 7 64bits. Just after installing Wireshark, when I try to start it, I get an error with the code 0xc000007b. Does anyone know where is the problem ? EDIT : I forgot to write it down but I downloaded the 64bit version on the website, so that should not ...'''
date = "2014-02-21T06:24:00Z"
lastmod = "2014-02-23T09:57:00Z"
weight = 30082
keywords = [ "windows7", "0xc000007b", "install", "error" ]
aliases = [ "/questions/30082" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Having this code error after installing : 0xc000007b](/questions/30082/having-this-code-error-after-installing-0xc000007b)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30082-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30082-score" class="post-score" title="current number of votes">0</div><span id="post-30082-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone !</p><p>First, my computer have Windows 7 64bits. Just after installing Wireshark, when I try to start it, I get an error with the code 0xc000007b. Does anyone know where is the problem ?</p><p>EDIT : I forgot to write it down but I downloaded the 64bit version on the website, so that should not be the problem.</p><p>EDIT2 : Ok, so I solved the problem. I uninstalled the 64bit version and just installed the 32bit one. It should probably due to a missing library. But I didn't know how to debug this so I choose the easy way :)</p><p>Sorry about the inconvenience and hope it would be usefull for someone else.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-0xc000007b" rel="tag" title="see questions tagged &#39;0xc000007b&#39;">0xc000007b</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Feb '14, 06:24</strong></p><img src="https://secure.gravatar.com/avatar/cc3434e7f0cd2d48e53c1919c7530367?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zankur&#39;s gravatar image" /><p><span>zankur</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zankur has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Feb '14, 07:09</strong> </span></p></div></div><div id="comments-container-30082" class="comments-container"><span id="30107"></span><div id="comment-30107" class="comment"><div id="post-30107-score" class="comment-score"></div><div class="comment-text"><p>Did you search for other similar questions and answers? For example:</p><ul><li><a href="http://ask.wireshark.org/questions/19977/how-can-i-fix-error-0xc000007b-on-startup">http://ask.wireshark.org/questions/19977/how-can-i-fix-error-0xc000007b-on-startup</a></li><li><a href="http://ask.wireshark.org/questions/16727/error-unable-to-open-wireshark">http://ask.wireshark.org/questions/16727/error-unable-to-open-wireshark</a></li></ul></div><div id="comment-30107-info" class="comment-info"><span class="comment-age">(23 Feb '14, 09:57)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-30082" class="comment-tools"></div><div class="clear"></div><div id="comment-30082-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

