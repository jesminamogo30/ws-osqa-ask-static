+++
type = "question"
title = "Capturing network packets using a device connected through USB"
description = '''I need to capture the packets in the network by connecting a USB device such as mobile phone to the PC. Can someone please tell me whether this is possible or not. If Yes how to do so?'''
date = "2014-02-11T00:02:00Z"
lastmod = "2014-02-11T00:46:00Z"
weight = 29663
keywords = [ "mobile", "packet-capture", "usb" ]
aliases = [ "/questions/29663" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing network packets using a device connected through USB](/questions/29663/capturing-network-packets-using-a-device-connected-through-usb)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29663-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29663-score" class="post-score" title="current number of votes">0</div><span id="post-29663-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to capture the packets in the network by connecting a USB device such as mobile phone to the PC. Can someone please tell me whether this is possible or not. If Yes how to do so?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span> <span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Feb '14, 00:02</strong></p><img src="https://secure.gravatar.com/avatar/57e272f8bacf9005a5cf2ebcd62dcde7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Swamy&#39;s gravatar image" /><p><span>Swamy</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Swamy has no accepted answers">0%</span></p></div></div><div id="comments-container-29663" class="comments-container"></div><div id="comment-tools-29663" class="comment-tools"></div><div class="clear"></div><div id="comment-29663-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29664"></span>

<div id="answer-container-29664" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29664-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29664-score" class="post-score" title="current number of votes">2</div><span id="post-29664-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It depends on the device and your operating system - and the "USB-connected" part is irrelevant.</p><p>Capturing on a USB-connected Ethernet adapter is, like capturing on a non-USB-connected Ethernet adapter, probably going to work on most OSes.</p><p>Capturing on a USB-connected Wi-Fi adapter is, like capturing on a non-USB-connected Wi-Fi adapter, <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">probably going to work better on some OSes than on others, and not as well as you might like on Windows</a> .</p><p>Capturing on a USB-connected mobile phone device is, like capturing on other PPP adapters, <a href="http://wiki.wireshark.org/CaptureSetup/PPP">going to work fairly well on UN*X and not so well on Windows</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Feb '14, 00:46</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-29664" class="comments-container"></div><div id="comment-tools-29664" class="comment-tools"></div><div class="clear"></div><div id="comment-29664-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

