+++
type = "question"
title = "Need help please - to much information!!!"
description = '''How do I? I have wireshark installed on my desktop windows pc (connected to router either wired or wireless) I have a asus wireless router AC2400 AC-RT87R I need to capture all in &amp;amp; out packets from a cell phone that connects to my router via wifi. I am able to determine the mac and ip address o...'''
date = "2015-10-13T13:16:00Z"
lastmod = "2015-10-13T13:36:00Z"
weight = 46510
keywords = [ "how" ]
aliases = [ "/questions/46510" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Need help please - to much information!!!](/questions/46510/need-help-please-to-much-information)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46510-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46510-score" class="post-score" title="current number of votes">0</div><span id="post-46510-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I?</p><p>I have wireshark installed on my desktop windows pc (connected to router either wired or wireless) I have a asus wireless router AC2400 AC-RT87R I need to capture all in &amp; out packets from a cell phone that connects to my router via wifi.</p><p>I am able to determine the mac and ip address of cell phone through the router software. How to I capture the packets from phone to wireshark running on my PC??? Since I am able to connect either wired or wireless whichever way is best / easiest. Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-how" rel="tag" title="see questions tagged &#39;how&#39;">how</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Oct '15, 13:16</strong></p><img src="https://secure.gravatar.com/avatar/b0e8b5cdc2fad65368d6cec7afe07474?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jimbosshark&#39;s gravatar image" /><p><span>jimbosshark</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jimbosshark has no accepted answers">0%</span></p></div></div><div id="comments-container-46510" class="comments-container"></div><div id="comment-tools-46510" class="comment-tools"></div><div class="clear"></div><div id="comment-46510-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46512"></span>

<div id="answer-container-46512" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46512-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46512-score" class="post-score" title="current number of votes">0</div><span id="post-46512-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wired will not work since your Asus Wireless router acts like a switch. So in that case, you cannot capture the traffic bridging from the WiFi interface to the Internet/WAN interface. For my information, read the following Wiki:</p><p><a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">https://wiki.wireshark.org/CaptureSetup/Ethernet</a></p><p>Now, your next step would be capturing the WiFi traffic. This is not as easy as it appears. Again, I point you to the Wireshark Wiki:</p><p><a href="https://wiki.wireshark.org/CaptureSetup/WLAN">https://wiki.wireshark.org/CaptureSetup/WLAN</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Oct '15, 13:36</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-46512" class="comments-container"></div><div id="comment-tools-46512" class="comment-tools"></div><div class="clear"></div><div id="comment-46512-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

