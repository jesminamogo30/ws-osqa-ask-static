+++
type = "question"
title = "Ack packets are getting dropped, causing a FTP session reset"
description = '''Hi All, I am facing a weird issue.I am trying to fetch the data between two FTP servers. While the data transfer Server is not receiving all ACK packets from client. Due to which the session is getting reset. There is a L3 device in between client and server. What could be the possible issue and how...'''
date = "2014-11-19T22:18:00Z"
lastmod = "2014-11-19T22:18:00Z"
weight = 37992
keywords = [ "rst", "ftp", "dup-ack", "ack", "tcp" ]
aliases = [ "/questions/37992" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ack packets are getting dropped, causing a FTP session reset](/questions/37992/ack-packets-are-getting-dropped-causing-a-ftp-session-reset)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37992-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37992-score" class="post-score" title="current number of votes">0</div><span id="post-37992-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I am facing a weird issue.I am trying to fetch the data between two FTP servers. While the data transfer Server is not receiving all ACK packets from client. Due to which the session is getting reset.</p><p>There is a L3 device in between client and server. What could be the possible issue and how can I check it.</p><p>Regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rst" rel="tag" title="see questions tagged &#39;rst&#39;">rst</span> <span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span> <span class="post-tag tag-link-dup-ack" rel="tag" title="see questions tagged &#39;dup-ack&#39;">dup-ack</span> <span class="post-tag tag-link-ack" rel="tag" title="see questions tagged &#39;ack&#39;">ack</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Nov '14, 22:18</strong></p><img src="https://secure.gravatar.com/avatar/162aaebbf145f20b18ece91428ba91e5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nitinc&#39;s gravatar image" /><p><span>nitinc</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nitinc has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Nov '14, 00:33</strong> </span></p></div></div><div id="comments-container-37992" class="comments-container"></div><div id="comment-tools-37992" class="comment-tools"></div><div class="clear"></div><div id="comment-37992-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

