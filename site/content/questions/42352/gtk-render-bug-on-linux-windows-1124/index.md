+++
type = "question"
title = "[closed] GTK render bug on Linux &amp; Windows, 1.12.4"
description = '''Hi, I&#x27;m reporting the following bug, in case it was not reported before. It happens exactly the same in Linux, GTK2 and GTK3, and in Windows, (no VM) for latest 1.12.4 . I did not try the QT version, since I don&#x27;t like the QT.  The bug is this: the whole gui window just disappears sometimes (randoml...'''
date = "2015-05-13T01:33:00Z"
lastmod = "2015-05-13T02:14:00Z"
weight = 42352
keywords = [ "gtk", "bug" ]
aliases = [ "/questions/42352" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] GTK render bug on Linux & Windows, 1.12.4](/questions/42352/gtk-render-bug-on-linux-windows-1124)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42352-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42352-score" class="post-score" title="current number of votes">0</div><span id="post-42352-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,<br />
I'm reporting the following bug, in case it was not reported before.<br />
It happens exactly the same in Linux, GTK2 and GTK3, and in Windows, (no VM) for latest 1.12.4 .<br />
I did not try the QT version, since I don't like the QT.<br />
</p><p>The bug is this: the whole gui window just disappears sometimes (randomly) during live capture.<br />
The workaround to make it appear again, is this: press F10, menu, move with cursors to Zoom options, select any.<br />
I repeat, it happens pretty randomly, so I can't provide any steps to reproduce it.<br />
But here a link to a screen shot, how it looks like under Windows XP:<br />
<a href="http://i59.tinypic.com/1zfpnuw.png" title="bug">bug</a><br />
</p><p>Greetings</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtk" rel="tag" title="see questions tagged &#39;gtk&#39;">gtk</span> <span class="post-tag tag-link-bug" rel="tag" title="see questions tagged &#39;bug&#39;">bug</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '15, 01:33</strong></p><img src="https://secure.gravatar.com/avatar/054a5bf4cea5a2e02665f19ce9dde42a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nikita&#39;s gravatar image" /><p><span>nikita</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nikita has no accepted answers">0%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>13 May '15, 02:15</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></br></p></div></div><div id="comments-container-42352" class="comments-container"></div><div id="comment-tools-42352" class="comment-tools"></div><div class="clear"></div><div id="comment-42352-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 13 May '15, 02:15

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42353"></span>

<div id="answer-container-42353" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42353-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42353-score" class="post-score" title="current number of votes">0</div><span id="post-42353-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Bugs should be reported as per the <a href="https://wiki.wireshark.org/ReportingBugs">Reporting Bugs</a> wiki page.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 May '15, 02:14</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span> </br></br></p></div></div><div id="comments-container-42353" class="comments-container"></div><div id="comment-tools-42353" class="comment-tools"></div><div class="clear"></div><div id="comment-42353-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

