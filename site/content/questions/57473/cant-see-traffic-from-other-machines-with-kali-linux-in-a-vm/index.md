+++
type = "question"
title = "Can&#x27;t see traffic from other machines with Kali Linux in a VM"
description = '''Hi @All.. Iam using wireshark in Kali from VM and iam not able to get the traffic from other devices connected in the same local area network.. even i configured VM as Bridge connection.. can anyone help me out here how to capture network trafic from other devices in the same network..  Appreciate f...'''
date = "2016-11-19T23:21:00Z"
lastmod = "2016-11-20T00:25:00Z"
weight = 57473
keywords = [ "virtual" ]
aliases = [ "/questions/57473" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't see traffic from other machines with Kali Linux in a VM](/questions/57473/cant-see-traffic-from-other-machines-with-kali-linux-in-a-vm)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57473-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57473-score" class="post-score" title="current number of votes">0</div><span id="post-57473-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi <span>@All</span>..</p><p>Iam using wireshark in Kali from VM and iam not able to get the traffic from other devices connected in the same local area network.. even i configured VM as Bridge connection.. can anyone help me out here how to capture network trafic from other devices in the same network..</p><p>Appreciate for your support...</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-virtual" rel="tag" title="see questions tagged &#39;virtual&#39;">virtual</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Nov '16, 23:21</strong></p><img src="https://secure.gravatar.com/avatar/a3f4087009ab2db4797532d6c49b8cef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Umesh%20Narayanan&#39;s gravatar image" /><p><span>Umesh Narayanan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Umesh Narayanan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>20 Nov '16, 00:23</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-57473" class="comments-container"><span id="57474"></span><div id="comment-57474" class="comment"><div id="post-57474-score" class="comment-score"></div><div class="comment-text"><p>So Kali Linux is running on a guest VM, and you're trying to capture from other <em>hosts</em>, rather than other guests on the same host, and you're not seeing traffic?</p><p>If so, what OS is the host machine running, and what VM software are you using? (VMware, Parallels, VirtualBox, etc.)</p></div><div id="comment-57474-info" class="comment-info"><span class="comment-age">(20 Nov '16, 00:25)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-57473" class="comment-tools"></div><div class="clear"></div><div id="comment-57473-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

