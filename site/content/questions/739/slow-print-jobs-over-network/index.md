+++
type = "question"
title = "Slow Print Jobs Over Network"
description = '''Hi all I wonder if you can help with something I have installed a new printer for someone and set it up as a shared printer then installed the machine on the other two computers, the machine that is directly connected to printer works fine, the others take 20 seconds to start the job, it does this f...'''
date = "2010-10-29T04:08:00Z"
lastmod = "2010-10-30T07:24:00Z"
weight = 739
keywords = [ "print", "slow" ]
aliases = [ "/questions/739" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Slow Print Jobs Over Network](/questions/739/slow-print-jobs-over-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-739-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-739-score" class="post-score" title="current number of votes">0</div><span id="post-739-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all I wonder if you can help with something I have installed a new printer for someone and set it up as a shared printer then installed the machine on the other two computers, the machine that is directly connected to printer works fine, the others take 20 seconds to start the job, it does this for every page, I have captured the action and it seems to fuss around trying to connect to something that shouldn't even exist \Dell01,Xcvport \Dell01hp3010 this fails and it is not until later in the trace at the 21 seconds mark when it tries \Dell01hp3010 without the xcvport thing that it connects and works.</p><p>I connected up old printer and it works fine I captured the old printer and it doesn't show this ,xcvport (it does have the comma in it).</p><p>I am not sure how to attach trace files here ???</p><p>Thanks for ideas.</p><p>Ray</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-print" rel="tag" title="see questions tagged &#39;print&#39;">print</span> <span class="post-tag tag-link-slow" rel="tag" title="see questions tagged &#39;slow&#39;">slow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '10, 04:08</strong></p><img src="https://secure.gravatar.com/avatar/1d67795c1484b0652b735a3827dcb9b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m0rph&#39;s gravatar image" /><p><span>m0rph</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m0rph has no accepted answers">0%</span></p></div></div><div id="comments-container-739" class="comments-container"></div><div id="comment-tools-739" class="comment-tools"></div><div class="clear"></div><div id="comment-739-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="744"></span>

<div id="answer-container-744" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-744-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-744-score" class="post-score" title="current number of votes">0</div><span id="post-744-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>sorted it I installed slightly diff print driver and it works fine now.</p><p>Thanks.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Oct '10, 07:24</strong></p><img src="https://secure.gravatar.com/avatar/1d67795c1484b0652b735a3827dcb9b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m0rph&#39;s gravatar image" /><p><span>m0rph</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m0rph has no accepted answers">0%</span></p></div></div><div id="comments-container-744" class="comments-container"></div><div id="comment-tools-744" class="comment-tools"></div><div class="clear"></div><div id="comment-744-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

