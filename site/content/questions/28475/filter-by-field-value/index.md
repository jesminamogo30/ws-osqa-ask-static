+++
type = "question"
title = "filter by field value"
description = '''I&#x27;ve used the following to filter by field value - udp port 8003 and udp[10] = 200; udp port 8002 and udp[8:4] = 1049. I recently attempted these with a newer version of Wireshark and they were disallowed. Can you help me update these? '''
date = "2013-12-30T08:22:00Z"
lastmod = "2013-12-30T09:14:00Z"
weight = 28475
keywords = [ "filterbyfield" ]
aliases = [ "/questions/28475" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [filter by field value](/questions/28475/filter-by-field-value)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28475-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28475-score" class="post-score" title="current number of votes">0</div><span id="post-28475-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've used the following to filter by field value - udp port 8003 and udp[10] = 200; udp port 8002 and udp[8:4] = 1049.</p><p>I recently attempted these with a newer version of Wireshark and they were disallowed. Can you help me update these?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filterbyfield" rel="tag" title="see questions tagged &#39;filterbyfield&#39;">filterbyfield</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Dec '13, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/33b15b3e7d4118063c4b8b73bc72d672?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mmaloney&#39;s gravatar image" /><p><span>mmaloney</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mmaloney has no accepted answers">0%</span></p></div></div><div id="comments-container-28475" class="comments-container"></div><div id="comment-tools-28475" class="comment-tools"></div><div class="clear"></div><div id="comment-28475-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28476"></span>

<div id="answer-container-28476" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28476-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28476-score" class="post-score" title="current number of votes">0</div><span id="post-28476-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Are you entering them as capture filters or as display filters? Wireshark 1.10.5 accepts both of these as legitimate capture filters.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Dec '13, 09:14</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-28476" class="comments-container"></div><div id="comment-tools-28476" class="comment-tools"></div><div class="clear"></div><div id="comment-28476-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

