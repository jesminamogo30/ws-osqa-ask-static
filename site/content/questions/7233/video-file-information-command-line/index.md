+++
type = "question"
title = "Video File Information Command Line"
description = '''I want to use Command Line Interface to pull out kind of Video File I am opening on my PC from Youtube.com or from dailymotion.com. Is there any way I can do with WireShark or tshark. But I want to use only command line Interface. No GUI please.'''
date = "2011-11-04T02:50:00Z"
lastmod = "2011-11-08T03:29:00Z"
weight = 7233
keywords = [ "tshark", "command-line" ]
aliases = [ "/questions/7233" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Video File Information Command Line](/questions/7233/video-file-information-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7233-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7233-score" class="post-score" title="current number of votes">0</div><span id="post-7233-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to use Command Line Interface to pull out kind of Video File I am opening on my PC from Youtube.com or from dailymotion.com. Is there any way I can do with WireShark or tshark. But I want to use only command line Interface. No GUI please.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-command-line" rel="tag" title="see questions tagged &#39;command-line&#39;">command-line</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Nov '11, 02:50</strong></p><img src="https://secure.gravatar.com/avatar/7d1a159f38bde1d7e11f9f4a4ed6604b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AnshumanG&#39;s gravatar image" /><p><span>AnshumanG</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AnshumanG has no accepted answers">0%</span></p></div></div><div id="comments-container-7233" class="comments-container"></div><div id="comment-tools-7233" class="comment-tools"></div><div class="clear"></div><div id="comment-7233-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7275"></span>

<div id="answer-container-7275" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7275-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7275-score" class="post-score" title="current number of votes">0</div><span id="post-7275-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>tshark does not have that functionality</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Nov '11, 03:29</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-7275" class="comments-container"></div><div id="comment-tools-7275" class="comment-tools"></div><div class="clear"></div><div id="comment-7275-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

