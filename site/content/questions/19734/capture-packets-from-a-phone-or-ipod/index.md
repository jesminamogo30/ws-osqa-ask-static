+++
type = "question"
title = "Capture Packets from a Phone or ipod"
description = '''Just got wireshark and i am currently trying it out. All the packets are going through my computer but not my phone (it is connected via wireless), however no packets are coming from that device. I want to find out how to view those packets'''
date = "2013-03-21T14:22:00Z"
lastmod = "2013-03-22T02:52:00Z"
weight = 19734
keywords = [ "phone", "packets" ]
aliases = [ "/questions/19734" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture Packets from a Phone or ipod](/questions/19734/capture-packets-from-a-phone-or-ipod)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19734-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19734-score" class="post-score" title="current number of votes">0</div><span id="post-19734-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Just got wireshark and i am currently trying it out. All the packets are going through my computer but not my phone (it is connected via wireless), however no packets are coming from that device. I want to find out how to view those packets</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-phone" rel="tag" title="see questions tagged &#39;phone&#39;">phone</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '13, 14:22</strong></p><img src="https://secure.gravatar.com/avatar/7588141ad28a6c0ec36c474565bf8a0f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Infamous&#39;s gravatar image" /><p><span>Infamous</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Infamous has no accepted answers">0%</span></p></div></div><div id="comments-container-19734" class="comments-container"><span id="19747"></span><div id="comment-19747" class="comment"><div id="post-19747-score" class="comment-score">1</div><div class="comment-text"><p>You're using your computer as a WiFi hotspot? How, what platform, etc, etc.?</p></div><div id="comment-19747-info" class="comment-info"><span class="comment-age">(22 Mar '13, 02:52)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-19734" class="comment-tools"></div><div class="clear"></div><div id="comment-19734-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

