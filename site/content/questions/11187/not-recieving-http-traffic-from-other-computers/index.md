+++
type = "question"
title = "not recieving http traffic from other computers"
description = '''Hi I am trying to run Wireshark on my home network to see traffic from several of my computers. I am running Wireshark on my laptop which is running Ubuntu 10.10, has a broadcom 43224 wireless adapter with b43 driver installed. My home network has WPA+WPA2 security mode. When I start capture in prom...'''
date = "2012-05-21T13:02:00Z"
lastmod = "2012-05-22T05:18:00Z"
weight = 11187
keywords = [ "capture", "promiscuous", "http" ]
aliases = [ "/questions/11187" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [not recieving http traffic from other computers](/questions/11187/not-recieving-http-traffic-from-other-computers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11187-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11187-score" class="post-score" title="current number of votes">0</div><span id="post-11187-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I am trying to run Wireshark on my home network to see traffic from several of my computers. I am running Wireshark on my laptop which is running Ubuntu 10.10, has a broadcom 43224 wireless adapter with b43 driver installed.</p><p>My home network has WPA+WPA2 security mode.</p><p>When I start capture in promiscuous mode I only get http traffic from my own laptop and not the other computers (that are also connected over wireless). I do get other protocols which seem to be just for broadcasting from the other computers.</p><p>Does anyone know what I am doing wrong?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 May '12, 13:02</strong></p><img src="https://secure.gravatar.com/avatar/8f19a0a6b0afe902c224e03a8eb38ece?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srose&#39;s gravatar image" /><p><span>srose</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srose has no accepted answers">0%</span></p></div></div><div id="comments-container-11187" class="comments-container"></div><div id="comment-tools-11187" class="comment-tools"></div><div class="clear"></div><div id="comment-11187-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11203"></span>

<div id="answer-container-11203" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11203-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11203-score" class="post-score" title="current number of votes">0</div><span id="post-11203-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you looked at the Wiki pages on <a href="http://wiki.wireshark.org/CaptureSetup">Capture setup</a> and <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">Wireless capture</a>?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 May '12, 01:49</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-11203" class="comments-container"><span id="11206"></span><div id="comment-11206" class="comment"><div id="post-11206-score" class="comment-score"></div><div class="comment-text"><p>Hi I have checked these out but can't find anything to help. Under Step 4: Capture traffic destined for machines other than your own I seem to meet all the requirements to do this</p></div><div id="comment-11206-info" class="comment-info"><span class="comment-age">(22 May '12, 05:18)</span> <span class="comment-user userinfo">srose</span></div></div></div><div id="comment-tools-11203" class="comment-tools"></div><div class="clear"></div><div id="comment-11203-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

