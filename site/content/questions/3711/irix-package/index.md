+++
type = "question"
title = "Irix Package"
description = '''Is there a free Irix package for Wireshark somewhere online?'''
date = "2011-04-25T13:18:00Z"
lastmod = "2011-04-25T13:18:00Z"
weight = 3711
keywords = [ "irix" ]
aliases = [ "/questions/3711" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Irix Package](/questions/3711/irix-package)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3711-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3711-score" class="post-score" title="current number of votes">0</div><span id="post-3711-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a free Irix package for Wireshark somewhere online?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-irix" rel="tag" title="see questions tagged &#39;irix&#39;">irix</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '11, 13:18</strong></p><img src="https://secure.gravatar.com/avatar/788f77e550e073ee4efad7cf3d9600ed?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="westwaswon&#39;s gravatar image" /><p><span>westwaswon</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="westwaswon has no accepted answers">0%</span></p></div></div><div id="comments-container-3711" class="comments-container"></div><div id="comment-tools-3711" class="comment-tools"></div><div class="clear"></div><div id="comment-3711-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

