+++
type = "question"
title = "Why is there No RTP, SIP or other VOIP (MagicJack) captured."
description = '''I installed OrkAudio and it worked fine. A few weeks later I installed WireShark. After installing WireShark OrkAudio was broken. WireShark does not capture any voip traffic when I use magicJack. I had successfully captured telephony in the past. Not sure if the past was an earler version of WireSha...'''
date = "2015-05-03T20:32:00Z"
lastmod = "2015-05-03T20:32:00Z"
weight = 42049
keywords = [ "sip", "packet-capture", "magicjack", "voip", "rtp" ]
aliases = [ "/questions/42049" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Why is there No RTP, SIP or other VOIP (MagicJack) captured.](/questions/42049/why-is-there-no-rtp-sip-or-other-voip-magicjack-captured)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42049-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42049-score" class="post-score" title="current number of votes">0</div><span id="post-42049-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I installed OrkAudio and it worked fine. A few weeks later I installed WireShark. After installing WireShark OrkAudio was broken. WireShark does not capture any voip traffic when I use magicJack. I had successfully captured telephony in the past. Not sure if the past was an earler version of WireShark or not. Win7, NIC in promiscuous mode, original (old style) MJ plugged into computer usb port. Uninstalling OrkAudio, WireShark, Pcap, and reinstalling OrkAudio and/or WireShark did not fix the problem with either program. I'm at a loss. I don't understand why WireShark does not see the voip traffic.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-magicjack" rel="tag" title="see questions tagged &#39;magicjack&#39;">magicjack</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 May '15, 20:32</strong></p><img src="https://secure.gravatar.com/avatar/7b929f60b6a56275f0cbb738b494ffcb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LoosingIt&#39;s gravatar image" /><p><span>LoosingIt</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LoosingIt has no accepted answers">0%</span></p></div></div><div id="comments-container-42049" class="comments-container"></div><div id="comment-tools-42049" class="comment-tools"></div><div class="clear"></div><div id="comment-42049-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

