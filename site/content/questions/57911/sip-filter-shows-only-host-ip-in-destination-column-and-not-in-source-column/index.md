+++
type = "question"
title = "SIP filter shows only host IP in destination column and not in source column"
description = '''I have set the filter to SIP and the SIP filter shows only host IP in destination column and not in source column. The outbound traffic for SIP is not being capture, never mind displayed.'''
date = "2016-12-06T12:08:00Z"
lastmod = "2016-12-06T20:39:00Z"
weight = 57911
keywords = [ "filter", "sip" ]
aliases = [ "/questions/57911" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SIP filter shows only host IP in destination column and not in source column](/questions/57911/sip-filter-shows-only-host-ip-in-destination-column-and-not-in-source-column)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57911-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57911-score" class="post-score" title="current number of votes">0</div><span id="post-57911-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have set the filter to SIP and the SIP filter shows only host IP in destination column and not in source column. The outbound traffic for SIP is not being capture, never mind displayed.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Dec '16, 12:08</strong></p><img src="https://secure.gravatar.com/avatar/961f51c5ec194c675b646ceeafadd62c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Werner%20G&#39;s gravatar image" /><p><span>Werner G</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Werner G has no accepted answers">0%</span></p></div></div><div id="comments-container-57911" class="comments-container"><span id="57918"></span><div id="comment-57918" class="comment"><div id="post-57918-score" class="comment-score"></div><div class="comment-text"><p>Is "ANY" outbound traffic being captured? If not, start here...</p><p><a href="https://ask.wireshark.org/questions/11714/only-inbound-traffic">https://ask.wireshark.org/questions/11714/only-inbound-traffic</a></p></div><div id="comment-57918-info" class="comment-info"><span class="comment-age">(06 Dec '16, 20:39)</span> <span class="comment-user userinfo">Rooster_50</span></div></div></div><div id="comment-tools-57911" class="comment-tools"></div><div class="clear"></div><div id="comment-57911-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

