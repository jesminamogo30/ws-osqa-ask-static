+++
type = "question"
title = "tshark -z expert report in JSON or EK format"
description = '''is it possible to export the expert report from tshark into JSON or elk delimiters? something like -T ?'''
date = "2016-11-21T06:05:00Z"
lastmod = "2016-11-21T06:09:00Z"
weight = 57522
keywords = [ "json", "elasticsearch", "tshark", "expert" ]
aliases = [ "/questions/57522" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [tshark -z expert report in JSON or EK format](/questions/57522/tshark-z-expert-report-in-json-or-ek-format)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57522-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57522-score" class="post-score" title="current number of votes">0</div><span id="post-57522-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is it possible to export the expert report from tshark into JSON or elk delimiters? something like -T ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-json" rel="tag" title="see questions tagged &#39;json&#39;">json</span> <span class="post-tag tag-link-elasticsearch" rel="tag" title="see questions tagged &#39;elasticsearch&#39;">elasticsearch</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-expert" rel="tag" title="see questions tagged &#39;expert&#39;">expert</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '16, 06:05</strong></p><img src="https://secure.gravatar.com/avatar/7cce78d738fa16cbb8d95bb5699d8a66?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zalabany&#39;s gravatar image" /><p><span>Zalabany</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zalabany has no accepted answers">0%</span></p></div></div><div id="comments-container-57522" class="comments-container"></div><div id="comment-tools-57522" class="comment-tools"></div><div class="clear"></div><div id="comment-57522-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57524"></span>

<div id="answer-container-57524" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57524-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57524-score" class="post-score" title="current number of votes">1</div><span id="post-57524-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No this is not supported for now.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Nov '16, 06:09</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-57524" class="comments-container"></div><div id="comment-tools-57524" class="comment-tools"></div><div class="clear"></div><div id="comment-57524-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

