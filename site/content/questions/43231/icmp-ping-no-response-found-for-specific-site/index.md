+++
type = "question"
title = "ICMP - ping - no response found for specific site"
description = '''Hello, When pingong specific site (HP.COM or any other Hp site) I get in Wireshark: 730 81.424930000 192.168.13.117 15.216.111.23 ICMP 74 Echo (ping) request id=0x0001, seq=265/2305, ttl=128 (no response found!) other site ping successfully. where to look!?'''
date = "2015-06-17T00:19:00Z"
lastmod = "2015-06-17T04:16:00Z"
weight = 43231
keywords = [ "icmp", "response", "no" ]
aliases = [ "/questions/43231" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ICMP - ping - no response found for specific site](/questions/43231/icmp-ping-no-response-found-for-specific-site)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43231-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43231-score" class="post-score" title="current number of votes">0</div><span id="post-43231-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>When pingong specific site (HP.COM or any other Hp site) I get in Wireshark: 730 81.424930000 192.168.13.117 15.216.111.23 ICMP 74 Echo (ping) request id=0x0001, seq=265/2305, ttl=128 (no response found!)</p><p>other site ping successfully.</p><p>where to look!?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-icmp" rel="tag" title="see questions tagged &#39;icmp&#39;">icmp</span> <span class="post-tag tag-link-response" rel="tag" title="see questions tagged &#39;response&#39;">response</span> <span class="post-tag tag-link-no" rel="tag" title="see questions tagged &#39;no&#39;">no</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jun '15, 00:19</strong></p><img src="https://secure.gravatar.com/avatar/1db506829385dc23f37e4d8bf44dc13d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yosiba&#39;s gravatar image" /><p><span>yosiba</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yosiba has no accepted answers">0%</span></p></div></div><div id="comments-container-43231" class="comments-container"></div><div id="comment-tools-43231" class="comment-tools"></div><div class="clear"></div><div id="comment-43231-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43237"></span>

<div id="answer-container-43237" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43237-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43237-score" class="post-score" title="current number of votes">0</div><span id="post-43237-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>where to look!?</p></blockquote><p>Nowhere. Those sites don't allow pings, meaning <strong>their</strong> Firewall blocks it. There is nothing you can do, except pinging other sites that allow pings (www.microsoft.com, www.apple.com, www.wireshark.org).</p><p>BTW: You can ping support.hp.com, but that site is hosted on the Akamai CDN.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jun '15, 04:16</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Jun '15, 04:18</strong> </span></p></div></div><div id="comments-container-43237" class="comments-container"></div><div id="comment-tools-43237" class="comment-tools"></div><div class="clear"></div><div id="comment-43237-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

