+++
type = "question"
title = "Is there an Install package for 32bit CentOS?"
description = '''Is there an Install package for 32bit CentOS 6? Please email jorgepalomo@hotmail.com .....need it for a class that already started. Thanks, jp'''
date = "2014-08-19T21:17:00Z"
lastmod = "2014-08-20T07:13:00Z"
weight = 35602
keywords = [ "32bit", "centos", "install", "6" ]
aliases = [ "/questions/35602" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is there an Install package for 32bit CentOS?](/questions/35602/is-there-an-install-package-for-32bit-centos)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35602-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35602-score" class="post-score" title="current number of votes">0</div><span id="post-35602-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there an Install package for 32bit CentOS 6? Please email <span class="__cf_email__" data-cfemail="6a0005180d0f1a0b060507052a02051e070b030644090507">[email protected]</span> .....need it for a class that already started. Thanks, jp</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-32bit" rel="tag" title="see questions tagged &#39;32bit&#39;">32bit</span> <span class="post-tag tag-link-centos" rel="tag" title="see questions tagged &#39;centos&#39;">centos</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-6" rel="tag" title="see questions tagged &#39;6&#39;">6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Aug '14, 21:17</strong></p><img src="https://secure.gravatar.com/avatar/5b534aae5555f144b72f1eb9fdf03506?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jpalomo&#39;s gravatar image" /><p><span>jpalomo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jpalomo has no accepted answers">0%</span></p></div></div><div id="comments-container-35602" class="comments-container"></div><div id="comment-tools-35602" class="comment-tools"></div><div class="clear"></div><div id="comment-35602-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35627"></span>

<div id="answer-container-35627" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35627-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35627-score" class="post-score" title="current number of votes">0</div><span id="post-35627-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes and no. CentOS comes with an old version of Wireshark: just <code>yum install wireshark-gnome</code>.</p><p>But if you want a modern version you have to build your own. Basic steps:</p><ol><li>Download the source tarball and expand it</li><li><code>./configure</code> with whatever options you want (you may need to install some dependencies to allow compilation)</li><li><code>make rpm-package</code> to build the RPM</li><li>Install the RPM</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Aug '14, 07:13</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Aug '14, 07:14</strong> </span></p></div></div><div id="comments-container-35627" class="comments-container"></div><div id="comment-tools-35627" class="comment-tools"></div><div class="clear"></div><div id="comment-35627-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

