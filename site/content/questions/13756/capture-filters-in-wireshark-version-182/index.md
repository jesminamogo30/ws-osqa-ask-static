+++
type = "question"
title = "Capture Filters in Wireshark Version 1.8.2"
description = '''I downloaded version 1.8.2 last week to assist at a client site where we needed to look at some activity on the network. One issue i noticed was when I went to try to build a capture filter, and then apply it, under Capture&amp;gt;Options, there is no longer a place to pick the Capture Filter. In other ...'''
date = "2012-08-20T08:22:00Z"
lastmod = "2012-08-20T11:39:00Z"
weight = 13756
keywords = [ "capture-filter" ]
aliases = [ "/questions/13756" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Capture Filters in Wireshark Version 1.8.2](/questions/13756/capture-filters-in-wireshark-version-182)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13756-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13756-score" class="post-score" title="current number of votes">0</div><span id="post-13756-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded version 1.8.2 last week to assist at a client site where we needed to look at some activity on the network. One issue i noticed was when I went to try to build a capture filter, and then apply it, under Capture&gt;Options, there is no longer a place to pick the Capture Filter. In other words, the Capture Filter field seems to be gone. I looked in the release notes, but there is no specific discussion about Capture Filters, other than the following:</p><p>New and Updated Capture File Support Ixia IxVeriWave, pcap-ng</p><p>How do we load a Capture Filter in 1.8.2?</p><p>Thank YOu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Aug '12, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/9e96b23e3495316e470ba9b487b82a73?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kmnruser&#39;s gravatar image" /><p><span>kmnruser</span><br />
<span class="score" title="26 reputation points">26</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kmnruser has no accepted answers">0%</span></p></div></div><div id="comments-container-13756" class="comments-container"></div><div id="comment-tools-13756" class="comment-tools"></div><div class="clear"></div><div id="comment-13756-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13757"></span>

<div id="answer-container-13757" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13757-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13757-score" class="post-score" title="current number of votes">1</div><span id="post-13757-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="kmnruser has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://ask.wireshark.org/questions/12221/how-to-chose-a-capture-filter-in-version-180-as-previous-in-the-capture-options-menu-of-version-16">this answer</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Aug '12, 08:44</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-13757" class="comments-container"><span id="13770"></span><div id="comment-13770" class="comment"><div id="post-13770-score" class="comment-score"></div><div class="comment-text"><p>Thank You, Jim!</p></div><div id="comment-13770-info" class="comment-info"><span class="comment-age">(20 Aug '12, 11:39)</span> <span class="comment-user userinfo">kmnruser</span></div></div></div><div id="comment-tools-13757" class="comment-tools"></div><div class="clear"></div><div id="comment-13757-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

