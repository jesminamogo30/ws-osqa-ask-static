+++
type = "question"
title = "wireless toolbar"
description = '''Hey All! why is my &#x27;wireless toolbar&#x27; grayed out / inactive? cheers'''
date = "2010-11-03T18:26:00Z"
lastmod = "2010-11-04T20:54:00Z"
weight = 802
keywords = [ "wireless_toolbar" ]
aliases = [ "/questions/802" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireless toolbar](/questions/802/wireless-toolbar)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-802-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-802-score" class="post-score" title="current number of votes">0</div><span id="post-802-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey All! why is my 'wireless toolbar' grayed out / inactive? cheers</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless_toolbar" rel="tag" title="see questions tagged &#39;wireless_toolbar&#39;">wireless_toolbar</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Nov '10, 18:26</strong></p><img src="https://secure.gravatar.com/avatar/52e66990382c85fe691832d89140ec3b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="clive&#39;s gravatar image" /><p><span>clive</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="clive has no accepted answers">0%</span></p></div></div><div id="comments-container-802" class="comments-container"></div><div id="comment-tools-802" class="comment-tools"></div><div class="clear"></div><div id="comment-802-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="810"></span>

<div id="answer-container-810" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-810-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-810-score" class="post-score" title="current number of votes">2</div><span id="post-810-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Um...we need a wee bit more information, I think. <em>smile</em></p><p>The first answer is that the wireless toolbar is greyed out if the active interface is not AirPcapped.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Nov '10, 20:24</strong></p><img src="https://secure.gravatar.com/avatar/11ea89c2fd5a5830c69d0574a51b8142?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wesmorgan1&#39;s gravatar image" /><p><span>wesmorgan1</span><br />
<span class="score" title="411 reputation points">411</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wesmorgan1 has 2 accepted answers">4%</span></p></div></div><div id="comments-container-810" class="comments-container"><span id="825"></span><div id="comment-825" class="comment"><div id="post-825-score" class="comment-score"></div><div class="comment-text"><p>thanks wes, I am so new to this that I do not even know what 'AirPcapped' means. I load the app, open the Microsoft interface and see all the comms. cheers clive</p></div><div id="comment-825-info" class="comment-info"><span class="comment-age">(04 Nov '10, 20:54)</span> <span class="comment-user userinfo">clive</span></div></div></div><div id="comment-tools-810" class="comment-tools"></div><div class="clear"></div><div id="comment-810-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

