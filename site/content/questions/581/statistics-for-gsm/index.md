+++
type = "question"
title = "statistics for GSM"
description = '''Hi, Is there a way to use statistics tool for GSM protocols ?'''
date = "2010-10-22T03:43:00Z"
lastmod = "2010-11-11T00:21:00Z"
weight = 581
keywords = [ "statistics", "gsm" ]
aliases = [ "/questions/581" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [statistics for GSM](/questions/581/statistics-for-gsm)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-581-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-581-score" class="post-score" title="current number of votes">0</div><span id="post-581-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Is there a way to use statistics tool for GSM protocols ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Oct '10, 03:43</strong></p><img src="https://secure.gravatar.com/avatar/641c4bff87dfe7930eb1c12b1f84ed52?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nuri&#39;s gravatar image" /><p><span>nuri</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nuri has no accepted answers">0%</span></p></div></div><div id="comments-container-581" class="comments-container"></div><div id="comment-tools-581" class="comment-tools"></div><div class="clear"></div><div id="comment-581-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="586"></span>

<div id="answer-container-586" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-586-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-586-score" class="post-score" title="current number of votes">0</div><span id="post-586-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>have a look at the menu: Telephony | GSM</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Oct '10, 08:13</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-586" class="comments-container"><span id="907"></span><div id="comment-907" class="comment"><div id="post-907-score" class="comment-score"></div><div class="comment-text"><p>Thanks for reply however tools under Telephony tab is not sufficient enough. Do you know more detailed statistic analyzing ways for GSM protocols?</p></div><div id="comment-907-info" class="comment-info"><span class="comment-age">(11 Nov '10, 00:21)</span> <span class="comment-user userinfo">nuri</span></div></div></div><div id="comment-tools-586" class="comment-tools"></div><div class="clear"></div><div id="comment-586-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

