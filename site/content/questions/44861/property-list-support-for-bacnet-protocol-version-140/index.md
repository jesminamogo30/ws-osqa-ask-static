+++
type = "question"
title = "Property List Support for BACnet protocol version 14.0"
description = '''Wireshark version 1.12.6 and version 1.99 both are not supporting the Response packets from the device with the property list support. It says RESERVED by ASHARAE . Can we get a wireshark release that supports BACnet protocol version 14.0?'''
date = "2015-08-05T02:23:00Z"
lastmod = "2015-08-05T11:12:00Z"
weight = 44861
keywords = [ "x64" ]
aliases = [ "/questions/44861" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Property List Support for BACnet protocol version 14.0](/questions/44861/property-list-support-for-bacnet-protocol-version-140)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44861-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44861-score" class="post-score" title="current number of votes">0</div><span id="post-44861-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark version 1.12.6 and version 1.99 both are not supporting the Response packets from the device with the property list support. It says RESERVED by ASHARAE . Can we get a wireshark release that supports BACnet protocol version 14.0?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-x64" rel="tag" title="see questions tagged &#39;x64&#39;">x64</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '15, 02:23</strong></p><img src="https://secure.gravatar.com/avatar/07c5616ebbf94206e850b4cf5827fc72?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ajanish&#39;s gravatar image" /><p><span>ajanish</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ajanish has no accepted answers">0%</span></p></div></div><div id="comments-container-44861" class="comments-container"></div><div id="comment-tools-44861" class="comment-tools"></div><div class="clear"></div><div id="comment-44861-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44863"></span>

<div id="answer-container-44863" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44863-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44863-score" class="post-score" title="current number of votes">0</div><span id="post-44863-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure, write the required code and submit your enhancement through <a href="https://code.wireshark.org/review">code.wireshark.org</a>. Once accepted it will be put out in the following 1.99 release.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Aug '15, 04:17</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-44863" class="comments-container"><span id="44879"></span><div id="comment-44879" class="comment"><div id="post-44879-score" class="comment-score"></div><div class="comment-text"><p>Or raise an enhancement request on the <a href="https://bugs.wireshark.org">Wireshark Bugzilla</a>. Note that implementing Bacnet 2012 will require access to the non-free <a href="http://www.techstreet.com/ashrae/products/1852610">protocol spec</a> at USD 170 so will have to wait for someone with access to the spec who can update the dissector.</p></div><div id="comment-44879-info" class="comment-info"><span class="comment-age">(05 Aug '15, 11:12)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-44863" class="comment-tools"></div><div class="clear"></div><div id="comment-44863-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

