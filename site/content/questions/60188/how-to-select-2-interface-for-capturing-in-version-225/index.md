+++
type = "question"
title = "how to select 2 interface for capturing in version 2.2.5"
description = '''In the older version we can select by ticking the interface we want to capture. But on verison 2.2.5 i cannot find it. Thanks'''
date = "2017-03-19T23:07:00Z"
lastmod = "2017-03-19T23:30:00Z"
weight = 60188
keywords = [ "multiple-interfaces" ]
aliases = [ "/questions/60188" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to select 2 interface for capturing in version 2.2.5](/questions/60188/how-to-select-2-interface-for-capturing-in-version-225)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60188-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60188-score" class="post-score" title="current number of votes">0</div><span id="post-60188-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In the older version we can select by ticking the interface we want to capture. But on verison 2.2.5 i cannot find it. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multiple-interfaces" rel="tag" title="see questions tagged &#39;multiple-interfaces&#39;">multiple-interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Mar '17, 23:07</strong></p><img src="https://secure.gravatar.com/avatar/1cdfcf42360e170692acec73ee437c60?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tdl&#39;s gravatar image" /><p><span>tdl</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tdl has no accepted answers">0%</span></p></div></div><div id="comments-container-60188" class="comments-container"></div><div id="comment-tools-60188" class="comment-tools"></div><div class="clear"></div><div id="comment-60188-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60190"></span>

<div id="answer-container-60190" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60190-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60190-score" class="post-score" title="current number of votes">0</div><span id="post-60190-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can just select multiple interfaces using Ctrl+Left Click and then start capturing.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/mult_int.JPG" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Mar '17, 23:30</strong></p><img src="https://secure.gravatar.com/avatar/1e22670f8d643ca08d658b80a6782932?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Packet_vlad&#39;s gravatar image" /><p><span>Packet_vlad</span><br />
<span class="score" title="436 reputation points">436</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Packet_vlad has 5 accepted answers">20%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Mar '17, 23:32</strong> </span></p></div></div><div id="comments-container-60190" class="comments-container"></div><div id="comment-tools-60190" class="comment-tools"></div><div class="clear"></div><div id="comment-60190-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

