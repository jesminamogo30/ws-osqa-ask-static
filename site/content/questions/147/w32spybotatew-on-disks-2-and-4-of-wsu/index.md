+++
type = "question"
title = "W32.spybot.ATEW on disks 2 and 4 of WSU"
description = '''My organization recently picked up the WireShark University disc set through a trusted retailer. I loaded disk 4 and scanned it with symantec. The tftp.pcap file is infected with W32.spybot.ATEW. Is this a threat?  Or is my A/V software detecting the signature of the spybot in the PCAP file and it&#x27;s...'''
date = "2010-09-16T05:35:00Z"
lastmod = "2010-09-16T12:35:00Z"
weight = 147
keywords = [ "w32.spybot.atew" ]
aliases = [ "/questions/147" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [W32.spybot.ATEW on disks 2 and 4 of WSU](/questions/147/w32spybotatew-on-disks-2-and-4-of-wsu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-147-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-147-score" class="post-score" title="current number of votes">0</div><span id="post-147-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My organization recently picked up the WireShark University disc set through a trusted retailer.</p><p>I loaded disk 4 and scanned it with symantec. The tftp.pcap file is infected with W32.spybot.ATEW.</p><p>Is this a threat?<br />
</p><p>Or is my A/V software detecting the signature of the spybot in the PCAP file and it's not a threat?</p><p>Disk 2 is also infected with W32.spybot.ATEW.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-w32.spybot.atew" rel="tag" title="see questions tagged &#39;w32.spybot.atew&#39;">w32.spybot.atew</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Sep '10, 05:35</strong></p><img src="https://secure.gravatar.com/avatar/4159802045d04a77a73882a24ccc9fd6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mdskier&#39;s gravatar image" /><p><span>mdskier</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mdskier has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-147" class="comments-container"></div><div id="comment-tools-147" class="comment-tools"></div><div class="clear"></div><div id="comment-147-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="150"></span>

<div id="answer-container-150" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-150-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-150-score" class="post-score" title="current number of votes">2</div><span id="post-150-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hello - we created the original DVD set and, assuming you have that original set, the DVDs are not infected with W32spybot - the trace file you refer to contains signatures of the communications to and from a bot-infected host. It is a .pcap file (trace file) and not an executable so loading it in Wireshark does not pose a risk.</p><p>Interesting that the majority of spyware detection tools do not have a problem while some scream bloody murder. Some even tag Wireshark as a "hacktool virus."</p><p>If you have any other thoughts/concerns, please post again.</p><p>Laura Chappell</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Sep '10, 08:07</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-150" class="comments-container"></div><div id="comment-tools-150" class="comment-tools"></div><div class="clear"></div><div id="comment-150-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="159"></span>

<div id="answer-container-159" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-159-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-159-score" class="post-score" title="current number of votes">0</div><span id="post-159-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Better safe then sorry.</p><p>Thanks Laura.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Sep '10, 12:22</strong></p><img src="https://secure.gravatar.com/avatar/4159802045d04a77a73882a24ccc9fd6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mdskier&#39;s gravatar image" /><p><span>mdskier</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mdskier has no accepted answers">0%</span></p></div></div><div id="comments-container-159" class="comments-container"><span id="160"></span><div id="comment-160" class="comment"><div id="post-160-score" class="comment-score"></div><div class="comment-text"><p>I agree!!!</p></div><div id="comment-160-info" class="comment-info"><span class="comment-age">(16 Sep '10, 12:35)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div></div><div id="comment-tools-159" class="comment-tools"></div><div class="clear"></div><div id="comment-159-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

