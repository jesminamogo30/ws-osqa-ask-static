+++
type = "question"
title = "Human readeble traffic TCP port 80,etc."
description = '''I start a new question then it didn&#x27;t work with old questiones, I can&#x27;t see answers of others users. So, in few words: I captured traffic of my smartphone p9 lite using commview and leaving my router without password. I&#x27;ve open on the smartphone different site, https and http. I have exported log fi...'''
date = "2017-06-28T05:46:00Z"
lastmod = "2017-06-28T05:46:00Z"
weight = 62354
keywords = [ "protocol", "websites", "tcp" ]
aliases = [ "/questions/62354" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Human readeble traffic TCP port 80,etc.](/questions/62354/human-readeble-traffic-tcp-port-80etc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62354-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62354-score" class="post-score" title="current number of votes">0</div><span id="post-62354-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I start a new question then it didn't work with old questiones, I can't see answers of others users. So, in few words: I captured traffic of my smartphone p9 lite using commview and leaving my router without password. I've open on the smartphone different site, https and http.</p><p>I have exported log file data from commview in .cap file that wireshark can open, I've run then wireshark and see all the lines, filter the TCP connection but nothing to do I can not read data in human readeble format, I've choosed in name resolutions properties the flag of "resolve network IP addresses" but no way to read the data. Most of tcp are of port 443 that I know it's https but there was also some port 80 but nothing to do. The question is why I see just TCP 443 data and some TCP 80 and why in wireshark I just see info in ASCII and not in human readeble format? thank you again</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-websites" rel="tag" title="see questions tagged &#39;websites&#39;">websites</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jun '17, 05:46</strong></p><img src="https://secure.gravatar.com/avatar/a64522e74524fdb3c6fa54d7fdc42275?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stevieraypis&#39;s gravatar image" /><p><span>stevieraypis</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stevieraypis has no accepted answers">0%</span></p></div></div><div id="comments-container-62354" class="comments-container"></div><div id="comment-tools-62354" class="comment-tools"></div><div class="clear"></div><div id="comment-62354-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

