+++
type = "question"
title = "Cant find Interface in Capture Tab"
description = '''I&#x27;ve clicked &quot;Capture&quot; on the top tab but cannot see an option that says &quot;Interface&quot;. I saw that a shortcut for it is ctrl + i, but that does not work either. How can i get this option to access it, and why is it not there for me?'''
date = "2016-12-17T18:37:00Z"
lastmod = "2016-12-17T19:29:00Z"
weight = 58200
keywords = [ "interface" ]
aliases = [ "/questions/58200" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Cant find Interface in Capture Tab](/questions/58200/cant-find-interface-in-capture-tab)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58200-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58200-score" class="post-score" title="current number of votes">0</div><span id="post-58200-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>I've clicked "Capture" on the top tab but cannot see an option that says "Interface". I saw that a shortcut for it is ctrl + i, but that does not work either. How can i get this option to access it, and why is it not there for me?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Dec '16, 18:37</strong></p><img src="https://secure.gravatar.com/avatar/bfc4b1a628694a812fe152aa71f666a9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BootTheSnoot&#39;s gravatar image" /><p><span>BootTheSnoot</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BootTheSnoot has no accepted answers">0%</span></p></div></div><div id="comments-container-58200" class="comments-container"></div><div id="comment-tools-58200" class="comment-tools"></div><div class="clear"></div><div id="comment-58200-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58201"></span>

<div id="answer-container-58201" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58201-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58201-score" class="post-score" title="current number of votes">1</div><span id="post-58201-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Ctrl-I was the shortcut for the interface list in Wireshark's legacy interface. In the new interface, Ctrl-I doesn't do anything and Interfaces is no longer a menu item on the Capture menu. You can see your interfaces on the main screen if there are no packets loaded into Wireshark, or on the Capture Options page. You can get to Capture Options from the icon toolbar (fourth icon from the left), or from the menus (Capture &gt; Options), or using the shortcut Ctrl-K.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Dec '16, 19:29</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-58201" class="comments-container"></div><div id="comment-tools-58201" class="comment-tools"></div><div class="clear"></div><div id="comment-58201-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

