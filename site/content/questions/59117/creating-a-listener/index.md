+++
type = "question"
title = "Creating a listener"
description = '''I am developing a customisation for Wireshark and I need to make a listener for it to work. What is the best way to do this?  From what I have read online so far it&#x27;s much easier to do in Lua but I am unsure as to how to allow my existing code to take the output from a Lua script or even how to get ...'''
date = "2017-01-28T10:04:00Z"
lastmod = "2017-01-28T10:04:00Z"
weight = 59117
keywords = [ "development", "lua", "code" ]
aliases = [ "/questions/59117" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Creating a listener](/questions/59117/creating-a-listener)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59117-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59117-score" class="post-score" title="current number of votes">0</div><span id="post-59117-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am developing a customisation for Wireshark and I need to make a listener for it to work.</p><p>What is the best way to do this?</p><p>From what I have read online so far it's much easier to do in Lua but I am unsure as to how to allow my existing code to take the output from a Lua script or even how to get started with Lua in this case.</p><p>If this is easier how would I go about embedding Lua scripts into my existing C++ code and if it is not easier how would I go about doing this using C++ and C?</p><p>Any help would be appreciated!</p><p>Thanks in advance!</p><p>ModuleMan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span> <span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-code" rel="tag" title="see questions tagged &#39;code&#39;">code</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jan '17, 10:04</strong></p><img src="https://secure.gravatar.com/avatar/3b7eb282c454b776eac0e960a3798043?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ModuleMan&#39;s gravatar image" /><p><span>ModuleMan</span><br />
<span class="score" title="21 reputation points">21</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ModuleMan has no accepted answers">0%</span></p></div></div><div id="comments-container-59117" class="comments-container"></div><div id="comment-tools-59117" class="comment-tools"></div><div class="clear"></div><div id="comment-59117-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

