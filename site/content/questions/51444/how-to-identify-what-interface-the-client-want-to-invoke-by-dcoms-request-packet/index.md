+++
type = "question"
title = "How to identify  what interface  the client want to invoke by DCOM‘s request packet"
description = '''I learn about the theory about communication of DCOM what is built on DCERPC. i am eagerto getting help from you. I would like to konw how to analysis of a request PDU of DCOM&#x27;S to get what the interface of the ORPC want to invoke （I find wireshark can do it）, by object UUID（IPID） or Context ID?'''
date = "2016-04-06T17:59:00Z"
lastmod = "2016-04-06T17:59:00Z"
weight = 51444
keywords = [ "interface", "identify" ]
aliases = [ "/questions/51444" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to identify what interface the client want to invoke by DCOM‘s request packet](/questions/51444/how-to-identify-what-interface-the-client-want-to-invoke-by-dcoms-request-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51444-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51444-score" class="post-score" title="current number of votes">0</div><span id="post-51444-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I learn about the theory about communication of DCOM what is built on DCERPC. i am eagerto getting help from you. I would like to konw how to analysis of a request PDU of DCOM'S to get what the interface of the ORPC want to invoke （I find wireshark can do it）, by object UUID（IPID） or Context ID?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-identify" rel="tag" title="see questions tagged &#39;identify&#39;">identify</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '16, 17:59</strong></p><img src="https://secure.gravatar.com/avatar/b93d30832785fbaf530b01535f49171d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bryan_ma&#39;s gravatar image" /><p><span>bryan_ma</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bryan_ma has no accepted answers">0%</span></p></div></div><div id="comments-container-51444" class="comments-container"></div><div id="comment-tools-51444" class="comment-tools"></div><div class="clear"></div><div id="comment-51444-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

