+++
type = "question"
title = "CPRI frame"
description = '''Does wireshark supports to analyze CPRI(commmon Public Radio Interface)frame?'''
date = "2014-11-12T00:39:00Z"
lastmod = "2014-11-14T00:21:00Z"
weight = 37773
keywords = [ "cpri" ]
aliases = [ "/questions/37773" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [CPRI frame](/questions/37773/cpri-frame)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37773-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37773-score" class="post-score" title="current number of votes">0</div><span id="post-37773-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does wireshark supports to analyze CPRI(commmon Public Radio Interface)frame?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cpri" rel="tag" title="see questions tagged &#39;cpri&#39;">cpri</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Nov '14, 00:39</strong></p><img src="https://secure.gravatar.com/avatar/f5f5e3055a264c5ac0be65a9c6511c69?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Abhieshek&#39;s gravatar image" /><p><span>Abhieshek</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Abhieshek has no accepted answers">0%</span></p></div></div><div id="comments-container-37773" class="comments-container"></div><div id="comment-tools-37773" class="comment-tools"></div><div class="clear"></div><div id="comment-37773-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37837"></span>

<div id="answer-container-37837" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37837-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37837-score" class="post-score" title="current number of votes">0</div><span id="post-37837-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No.</p><p>But there seems to be a vendor of "special equipment" who provides a tool to extract data from CPRI trace data to view it in Wireshark.</p><blockquote><p><a href="http://www.absoluteanalysis.com/documents/CRPI%20Data%20Extraction%20App%20Note.87.pdf">http://www.absoluteanalysis.com/documents/CRPI%20Data%20Extraction%20App%20Note.87.pdf</a></p></blockquote><p>I have no idea if that works or makes sense at all, so please have a look at the PDF and make up your mind.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Nov '14, 13:10</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-37837" class="comments-container"><span id="37852"></span><div id="comment-37852" class="comment"><div id="post-37852-score" class="comment-score"></div><div class="comment-text"><p>You are right,in fact there are other vendors also,like Sarokal,SmartDV(for FPGA verification only) which provides sniffing tools along with other capabilities &amp; need to be bought seperately.</p><p>If et al wireshark had the capability,it would have been really great &amp; useful,hence wanted to confirm.</p><p>Thank you for the confirmation.</p><p>Thanks, Abhieshek Deshpande.</p></div><div id="comment-37852-info" class="comment-info"><span class="comment-age">(14 Nov '14, 00:21)</span> <span class="comment-user userinfo">Abhieshek</span></div></div></div><div id="comment-tools-37837" class="comment-tools"></div><div class="clear"></div><div id="comment-37837-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

