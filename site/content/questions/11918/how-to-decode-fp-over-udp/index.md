+++
type = "question"
title = "[closed] How to decode FP over UDP?"
description = '''I am using Wireshark version 1.6.4. I have some FP packets in pcap  format. However I do not see FP protocol option in &quot;decode as&quot; list.  However Enabled protocols， shows FP protocolsenabled which indicates  Wireshark supports fp. I fill &quot;fp&quot; in filters,I can not get no Packets  filtered.Howerver,I ...'''
date = "2012-06-14T23:17:00Z"
lastmod = "2012-06-14T23:37:00Z"
weight = 11918
keywords = [ "fp", "udp", "umts-fp" ]
aliases = [ "/questions/11918" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to decode FP over UDP?](/questions/11918/how-to-decode-fp-over-udp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11918-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11918-score" class="post-score" title="current number of votes">0</div><span id="post-11918-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using Wireshark version 1.6.4. I have some FP packets in pcap format. However I do not see FP protocol option in "decode as" list. However Enabled protocols， shows FP protocolsenabled which indicates Wireshark supports fp. I fill "fp" in filters,I can not get no Packets filtered.Howerver,I see some packets over UDP transported from RNC.</p><p>Please tell me how can I decode packets as FP. Those sample fp packets are transported over UDP. Yet I do not see fp protocol in "decode as" list.</p><p>Thanks and regards,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fp" rel="tag" title="see questions tagged &#39;fp&#39;">fp</span> <span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-umts-fp" rel="tag" title="see questions tagged &#39;umts-fp&#39;">umts-fp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '12, 23:17</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>14 Jun '12, 23:36</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-11918" class="comments-container"><span id="11919"></span><div id="comment-11919" class="comment"><div id="post-11919-score" class="comment-score"></div><div class="comment-text"><p><a href="http://ask.wireshark.org/questions/11913/how-to-use-umts-fp-dissector">duplicate 1</a>, <a href="http://ask.wireshark.org/questions/11894/how-to-use-the-two-function-heur_dissect_xx-and-heur_dissector_add">duplicate 2</a></p></div><div id="comment-11919-info" class="comment-info"><span class="comment-age">(14 Jun '12, 23:37)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-11918" class="comment-tools"></div><div class="clear"></div><div id="comment-11918-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by helloworld 14 Jun '12, 23:36

</div>

</div>

</div>

