+++
type = "question"
title = "max interface for capturing"
description = '''Hi there, is there any limitation of interface which i can choose for capturing? At the moment we have capturing with 2 WLAN cards, anybody knows if we can 4 card or 6 cards? Best regards L2C'''
date = "2015-07-03T02:08:00Z"
lastmod = "2015-07-03T19:20:00Z"
weight = 43840
keywords = [ "interface" ]
aliases = [ "/questions/43840" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [max interface for capturing](/questions/43840/max-interface-for-capturing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43840-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43840-score" class="post-score" title="current number of votes">0</div><span id="post-43840-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there,</p><p>is there any limitation of interface which i can choose for capturing?</p><p>At the moment we have capturing with 2 WLAN cards, anybody knows if we can 4 card or 6 cards?</p><p>Best regards L2C</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '15, 02:08</strong></p><img src="https://secure.gravatar.com/avatar/c7c5b173f3e62a1973a03e00db3c13c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="L2C&#39;s gravatar image" /><p><span>L2C</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="L2C has no accepted answers">0%</span></p></div></div><div id="comments-container-43840" class="comments-container"></div><div id="comment-tools-43840" class="comment-tools"></div><div class="clear"></div><div id="comment-43840-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43857"></span>

<div id="answer-container-43857" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43857-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43857-score" class="post-score" title="current number of votes">1</div><span id="post-43857-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="L2C has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are no hardwired limits in Wireshark or libpcap/WinPcap (which it uses for capturing), and there probably aren't any hardwired limits in your OS. The main limit would probably be the performance of your hardware and the software you're using to capture.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jul '15, 19:20</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-43857" class="comments-container"></div><div id="comment-tools-43857" class="comment-tools"></div><div class="clear"></div><div id="comment-43857-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

