+++
type = "question"
title = "How to read a throughput graph"
description = '''How to read a throughput graph in Wireshark? I don&#x27;t understand at all. This is example my throughput graph.'''
date = "2011-02-06T23:50:00Z"
lastmod = "2011-02-07T18:43:00Z"
weight = 2180
keywords = [ "graph", "throughput" ]
aliases = [ "/questions/2180" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to read a throughput graph](/questions/2180/how-to-read-a-throughput-graph)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2180-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2180-score" class="post-score" title="current number of votes">0</div><span id="post-2180-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to read a throughput graph in Wireshark? I don't understand at all. This is example my throughput graph.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-throughput" rel="tag" title="see questions tagged &#39;throughput&#39;">throughput</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '11, 23:50</strong></p><img src="https://secure.gravatar.com/avatar/169d37dad20d51373e83b5c131960518?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SOFY&#39;s gravatar image" /><p><span>SOFY</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SOFY has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Feb '12, 18:59</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-2180" class="comments-container"><span id="2187"></span><div id="comment-2187" class="comment"><div id="post-2187-score" class="comment-score"></div><div class="comment-text"><p>Were's your example graph?</p></div><div id="comment-2187-info" class="comment-info"><span class="comment-age">(07 Feb '11, 05:05)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-2180" class="comment-tools"></div><div class="clear"></div><div id="comment-2180-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2209"></span>

<div id="answer-container-2209" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2209-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2209-score" class="post-score" title="current number of votes">2</div><span id="post-2209-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SOFY has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's just a plot of bytes per second over time. It's a snapshot of throughput at a particular instance.. Stevens graph (more useful IMO) is a plot of number of bytes being transferred over time.<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Feb '11, 18:43</strong></p><img src="https://secure.gravatar.com/avatar/63805f079ac429902641cad9d7cd69e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hansangb&#39;s gravatar image" /><p><span>hansangb</span><br />
<span class="score" title="791 reputation points">791</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hansangb has 7 accepted answers">12%</span> </br></p></div></div><div id="comments-container-2209" class="comments-container"></div><div id="comment-tools-2209" class="comment-tools"></div><div class="clear"></div><div id="comment-2209-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

