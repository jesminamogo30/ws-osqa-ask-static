+++
type = "question"
title = "Can I load Wireshark with a pre-specified view?"
description = '''Hi all, Is it possible to set up a Wireshark plugin to load with a pre-specified view (i.e. specific columns added) when using the plugin? E.g. if the plugin is a dissector and we want to make it so that when it is used as a decoder, additional columns are added to the view. Is this possible? Thank ...'''
date = "2017-05-11T13:19:00Z"
lastmod = "2017-05-11T13:19:00Z"
weight = 61359
keywords = [ "plugin", "view", "columns", "wireshark" ]
aliases = [ "/questions/61359" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can I load Wireshark with a pre-specified view?](/questions/61359/can-i-load-wireshark-with-a-pre-specified-view)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61359-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61359-score" class="post-score" title="current number of votes">0</div><span id="post-61359-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>Is it possible to set up a Wireshark plugin to load with a pre-specified view (i.e. specific columns added) when using the plugin? E.g. if the plugin is a dissector and we want to make it so that when it is used as a decoder, additional columns are added to the view.</p><p>Is this possible?</p><p>Thank you, Martin</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span> <span class="post-tag tag-link-view" rel="tag" title="see questions tagged &#39;view&#39;">view</span> <span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 May '17, 13:19</strong></p><img src="https://secure.gravatar.com/avatar/bb505f6832bb10125678c300fff66aae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mfcss&#39;s gravatar image" /><p><span>mfcss</span><br />
<span class="score" title="21 reputation points">21</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mfcss has no accepted answers">0%</span></p></div></div><div id="comments-container-61359" class="comments-container"></div><div id="comment-tools-61359" class="comment-tools"></div><div class="clear"></div><div id="comment-61359-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

