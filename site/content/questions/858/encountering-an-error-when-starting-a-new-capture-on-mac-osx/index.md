+++
type = "question"
title = "Encountering an error when starting a new capture on Mac OSX"
description = '''I am encountering an error &quot;You didn&#x27;t specify an interface on which to capture packets&quot; when clicking on the Start new Live capture.  How do I setup a new Interface? I might be missing something obvious.  Regards'''
date = "2010-11-08T11:01:00Z"
lastmod = "2012-04-25T09:30:00Z"
weight = 858
keywords = [ "interface", "start", "trace", "new" ]
aliases = [ "/questions/858" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Encountering an error when starting a new capture on Mac OSX](/questions/858/encountering-an-error-when-starting-a-new-capture-on-mac-osx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-858-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-858-score" class="post-score" title="current number of votes">0</div><span id="post-858-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am encountering an error "You didn't specify an interface on which to capture packets" when clicking on the Start new Live capture.</p><p>How do I setup a new Interface? I might be missing something obvious.</p><p>Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-start" rel="tag" title="see questions tagged &#39;start&#39;">start</span> <span class="post-tag tag-link-trace" rel="tag" title="see questions tagged &#39;trace&#39;">trace</span> <span class="post-tag tag-link-new" rel="tag" title="see questions tagged &#39;new&#39;">new</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '10, 11:01</strong></p><img src="https://secure.gravatar.com/avatar/6227e75fe0f5d95539083e556e4af7f4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="loganwol&#39;s gravatar image" /><p><span>loganwol</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="loganwol has no accepted answers">0%</span></p></div></div><div id="comments-container-858" class="comments-container"></div><div id="comment-tools-858" class="comment-tools"></div><div class="clear"></div><div id="comment-858-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="925"></span>

<div id="answer-container-925" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-925-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-925-score" class="post-score" title="current number of votes">0</div><span id="post-925-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Can you capture by clicking Start from the capture interfaces window? (Capture &gt; Interfaces - click Start next to the interface that appears to be seeing traffic." If no interfaces appear, then you can't capture traffic.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Nov '10, 17:20</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-925" class="comments-container"><span id="10442"></span><div id="comment-10442" class="comment"><div id="post-10442-score" class="comment-score"></div><div class="comment-text"><p>I am also having the same problem and yes the interface also doesn't appear, so any solution??</p></div><div id="comment-10442-info" class="comment-info"><span class="comment-age">(25 Apr '12, 09:30)</span> <span class="comment-user userinfo">b_kedar</span></div></div></div><div id="comment-tools-925" class="comment-tools"></div><div class="clear"></div><div id="comment-925-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

