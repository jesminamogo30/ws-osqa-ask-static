+++
type = "question"
title = "how do i sniff packet from other pc on same network over wireless connection"
description = '''hi, im a newbie to this program and currently doing project on packets and security. could someone help me how to set up to sniff packet sent and received from other pc that are connected in the same network that im in.. yeah, via a wireless network... =)'''
date = "2011-03-16T03:52:00Z"
lastmod = "2011-03-17T18:51:00Z"
weight = 2868
keywords = [ "capture", "setup" ]
aliases = [ "/questions/2868" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how do i sniff packet from other pc on same network over wireless connection](/questions/2868/how-do-i-sniff-packet-from-other-pc-on-same-network-over-wireless-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2868-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2868-score" class="post-score" title="current number of votes">0</div><span id="post-2868-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, im a newbie to this program and currently doing project on packets and security. could someone help me how to set up to sniff packet sent and received from other pc that are connected in the same network that im in.. yeah, via a wireless network... =)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-setup" rel="tag" title="see questions tagged &#39;setup&#39;">setup</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '11, 03:52</strong></p><img src="https://secure.gravatar.com/avatar/b6028afe69b402deeef65cd466399c0c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mr_T&#39;s gravatar image" /><p><span>mr_T</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mr_T has no accepted answers">0%</span></p></div></div><div id="comments-container-2868" class="comments-container"></div><div id="comment-tools-2868" class="comment-tools"></div><div class="clear"></div><div id="comment-2868-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2871"></span>

<div id="answer-container-2871" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2871-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2871-score" class="post-score" title="current number of votes">1</div><span id="post-2871-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See: <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Mar '11, 08:25</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-2871" class="comments-container"><span id="2884"></span><div id="comment-2884" class="comment"><div id="post-2884-score" class="comment-score"></div><div class="comment-text"><p>thanks jeff... but i have read it before... man, i read it 3 or 4 times already, but i just cant get it how to run it on windows... in the capture option, there is a button for wireless setting and remote setting, and those buttons are disabled.. what should i do?</p></div><div id="comment-2884-info" class="comment-info"><span class="comment-age">(16 Mar '11, 23:01)</span> <span class="comment-user userinfo">mr_T</span></div></div><span id="2903"></span><div id="comment-2903" class="comment"><div id="post-2903-score" class="comment-score"></div><div class="comment-text"><p>I'm not an expert on WLAN capturing, but IIRC the most important question is: what OS are you using? If it's Windows, chances are you're out of luck (unless you buy an AirPcap adapter). See the section about using Monitor mode.</p></div><div id="comment-2903-info" class="comment-info"><span class="comment-age">(17 Mar '11, 06:56)</span> <span class="comment-user userinfo">JeffMorriss ♦</span></div></div><span id="2917"></span><div id="comment-2917" class="comment"><div id="post-2917-score" class="comment-score"></div><div class="comment-text"><p>im using windows vista basic 32-bit... okay i'll try to get an adapter via my university... most probably i wont succeed but its worth a try... thanks again...</p></div><div id="comment-2917-info" class="comment-info"><span class="comment-age">(17 Mar '11, 18:51)</span> <span class="comment-user userinfo">mr_T</span></div></div></div><div id="comment-tools-2871" class="comment-tools"></div><div class="clear"></div><div id="comment-2871-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

