+++
type = "question"
title = "Need to see server to server exchange for SMTP setup"
description = '''I need to see server to server exchange for SMTP setup and cannot find a sample capture. Unfortunately, I do not have an environment available to capture.  Can anyone help out?  TIA. steve hammill'''
date = "2014-09-18T14:19:00Z"
lastmod = "2014-09-18T14:19:00Z"
weight = 36436
keywords = [ "smtp" ]
aliases = [ "/questions/36436" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Need to see server to server exchange for SMTP setup](/questions/36436/need-to-see-server-to-server-exchange-for-smtp-setup)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36436-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36436-score" class="post-score" title="current number of votes">0</div><span id="post-36436-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to see server to server exchange for SMTP setup and cannot find a sample capture. Unfortunately, I do not have an environment available to capture.</p><p>Can anyone help out? TIA.</p><p>steve hammill</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-smtp" rel="tag" title="see questions tagged &#39;smtp&#39;">smtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Sep '14, 14:19</strong></p><img src="https://secure.gravatar.com/avatar/308ff5d1bd8a883899839b5d6d731233?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="griz326&#39;s gravatar image" /><p><span>griz326</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="griz326 has no accepted answers">0%</span></p></div></div><div id="comments-container-36436" class="comments-container"></div><div id="comment-tools-36436" class="comment-tools"></div><div class="clear"></div><div id="comment-36436-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

