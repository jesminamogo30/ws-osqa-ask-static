+++
type = "question"
title = "decoding g729"
description = '''We have captured a call that we need to decode/play however it is in g729. is that possible to do in wireshark? if so how we&#x27;re using SCCP(skinny) for call control.  thanks. '''
date = "2011-09-01T16:45:00Z"
lastmod = "2011-09-02T02:29:00Z"
weight = 6052
keywords = [ "g279" ]
aliases = [ "/questions/6052" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [decoding g729](/questions/6052/decoding-g729)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6052-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6052-score" class="post-score" title="current number of votes">0</div><span id="post-6052-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We have captured a call that we need to decode/play however it is in g729. is that possible to do in wireshark? if so how we're using SCCP(skinny) for call control.</p><p>thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-g279" rel="tag" title="see questions tagged &#39;g279&#39;">g279</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Sep '11, 16:45</strong></p><img src="https://secure.gravatar.com/avatar/7723fe82b4c9ee32f818a60f963fa29f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jabrony99&#39;s gravatar image" /><p><span>jabrony99</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jabrony99 has no accepted answers">0%</span></p></div></div><div id="comments-container-6052" class="comments-container"></div><div id="comment-tools-6052" class="comment-tools"></div><div class="clear"></div><div id="comment-6052-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6057"></span>

<div id="answer-container-6057" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6057-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6057-score" class="post-score" title="current number of votes">2</div><span id="post-6057-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Since G.729 is a licensed codec it cannot incorporated in Wireshark without fees being paid. This is a problem not easy to circumvent.</p><p>Therefore have a look <a href="http://wiki.wireshark.org/HowToDecodeG729">here</a> for an alternative procedure.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Sep '11, 02:29</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-6057" class="comments-container"></div><div id="comment-tools-6057" class="comment-tools"></div><div class="clear"></div><div id="comment-6057-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

