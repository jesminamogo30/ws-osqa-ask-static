+++
type = "question"
title = "analysis h.248 protocol"
description = '''Hi every one  I want learn about analysis h.248 and V5 protocol any body can guide me what I must do ? thanks'''
date = "2015-10-08T01:21:00Z"
lastmod = "2015-10-08T01:21:00Z"
weight = 46415
keywords = [ "h.248" ]
aliases = [ "/questions/46415" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [analysis h.248 protocol](/questions/46415/analysis-h248-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46415-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46415-score" class="post-score" title="current number of votes">0</div><span id="post-46415-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi every one I want learn about analysis h.248 and V5 protocol any body can guide me what I must do ?</p><p>thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-h.248" rel="tag" title="see questions tagged &#39;h.248&#39;">h.248</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Oct '15, 01:21</strong></p><img src="https://secure.gravatar.com/avatar/c83cba973253d90fab778223d22973b3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="amir%20mohammadi&#39;s gravatar image" /><p><span>amir mohammadi</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="amir mohammadi has no accepted answers">0%</span></p></div></div><div id="comments-container-46415" class="comments-container"></div><div id="comment-tools-46415" class="comment-tools"></div><div class="clear"></div><div id="comment-46415-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

