+++
type = "question"
title = "How to zoom in on TCP stream graphs?"
description = '''Hi, Wireshark documentation, Laura Chappell&#x27;s DVD tutorial and other source all say that I can zoom in on a Statistics-&amp;gt;TCP Stream Graph-&amp;gt;Time Sequence Graph by left clicking my mouse. But this doesn&#x27;t work for me in Wireshark 1.4.4 on two different systems (a XP SP3 system and a Win 7 SP1 sys...'''
date = "2011-04-19T19:00:00Z"
lastmod = "2012-03-02T07:45:00Z"
weight = 3627
keywords = [ "zooming", "graph" ]
aliases = [ "/questions/3627" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How to zoom in on TCP stream graphs?](/questions/3627/how-to-zoom-in-on-tcp-stream-graphs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3627-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3627-score" class="post-score" title="current number of votes">0</div><span id="post-3627-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Wireshark documentation, Laura Chappell's DVD tutorial and other source all say that I can zoom in on a Statistics-&gt;TCP Stream Graph-&gt;Time Sequence Graph by left clicking my mouse. But this doesn't work for me in Wireshark 1.4.4 on two different systems (a XP SP3 system and a Win 7 SP1 system). Left clicking in the TCP stream graph has no effect. I've been unable to find any mouse click or key sequence which will zoom, nor have I had any luck in finding the answer to this question by surfing.</p><p>So am I missing something? Is this functionality broken? Is there some other mechanism for accomplishing this in recent Wireshark versions that's changed from earlier versions?</p><p>Regards,</p><p>John Fishbeck MSU Physical Plant Systems and Networking</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-zooming" rel="tag" title="see questions tagged &#39;zooming&#39;">zooming</span> <span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Apr '11, 19:00</strong></p><img src="https://secure.gravatar.com/avatar/c0e5aecff96efd80829b66218c4ce668?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pplant_guy&#39;s gravatar image" /><p><span>pplant_guy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pplant_guy has no accepted answers">0%</span></p></div></div><div id="comments-container-3627" class="comments-container"></div><div id="comment-tools-3627" class="comment-tools"></div><div class="clear"></div><div id="comment-3627-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="3628"></span>

<div id="answer-container-3628" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3628-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3628-score" class="post-score" title="current number of votes">0</div><span id="post-3628-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From the help:</p><pre><code>   Middle Mouse Button         zooms in (towards area under cursor)
   &lt;Shift&gt;-Middle Mouse Button    zooms out</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '11, 22:41</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-3628" class="comments-container"><span id="3629"></span><div id="comment-3629" class="comment"><div id="post-3629-score" class="comment-score">1</div><div class="comment-text"><p>And for those of us without a 3 button mouse: You can use the + and - keys to zoom in and out. Then use the cursor keys to move the view on the graph.</p></div><div id="comment-3629-info" class="comment-info"><span class="comment-age">(19 Apr '11, 23:10)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="9312"></span><div id="comment-9312" class="comment"><div id="post-9312-score" class="comment-score"></div><div class="comment-text"><p>Hint: + and - may only work on the general keyboard, not the numpad keys - at least on PCs with german keymaps it is like that.</p></div><div id="comment-9312-info" class="comment-info"><span class="comment-age">(02 Mar '12, 07:45)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-3628" class="comment-tools"></div><div class="clear"></div><div id="comment-3628-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="9310"></span>

<div id="answer-container-9310" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9310-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9310-score" class="post-score" title="current number of votes">0</div><span id="post-9310-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The + or - options do not work if you have a two button mouse. You can zoom with the wheel, but try to zoom out and you won't get it. Wireshark should improve this aspect.</p><p>Machines: Linux 2.6.33.20</p><p>Windows 7 64 bits Wireshark 1.6.4</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Mar '12, 07:02</strong></p><img src="https://secure.gravatar.com/avatar/46e7ed000d5c4c49d2f39a890e8aa0b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Josola&#39;s gravatar image" /><p><span>Josola</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Josola has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Mar '12, 07:06</strong> </span></p></div></div><div id="comments-container-9310" class="comments-container"></div><div id="comment-tools-9310" class="comment-tools"></div><div class="clear"></div><div id="comment-9310-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

