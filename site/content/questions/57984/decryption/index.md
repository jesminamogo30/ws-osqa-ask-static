+++
type = "question"
title = "DECRYPTION"
description = '''Hello guys, i want to know the information received from various packets but a lot of protocols like DNS ARP NBNS etc send their information in encrypted form so i want to know how to decrypt that information. i appreciate all the help i get thanks '''
date = "2016-12-09T19:33:00Z"
lastmod = "2016-12-09T20:01:00Z"
weight = 57984
keywords = [ "decryptioncal" ]
aliases = [ "/questions/57984" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [DECRYPTION](/questions/57984/decryption)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57984-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57984-score" class="post-score" title="current number of votes">0</div><span id="post-57984-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello guys,</p><p>i want to know the information received from various packets but a lot of protocols like DNS ARP NBNS etc send their information in encrypted form so i want to know how to decrypt that information.</p><p>i appreciate all the help i get thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryptioncal" rel="tag" title="see questions tagged &#39;decryptioncal&#39;">decryptioncal</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Dec '16, 19:33</strong></p><img src="https://secure.gravatar.com/avatar/bc57f6b93187a3c3e2438afbe84d6f60?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="beginner%20in%20hacking&#39;s gravatar image" /><p><span>beginner in ...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="beginner in hacking has no accepted answers">0%</span></p></div></div><div id="comments-container-57984" class="comments-container"></div><div id="comment-tools-57984" class="comment-tools"></div><div class="clear"></div><div id="comment-57984-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57985"></span>

<div id="answer-container-57985" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57985-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57985-score" class="post-score" title="current number of votes">1</div><span id="post-57985-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>ARP and NBNS packets aren't encrypted, so there's no decryption to be done.</p><p>They're not <em>text</em> packets, but that's why programs such as tcpdump, snoop, Sniffer, {Ether,Token,Airo,Omni}Peek, Wireshark, etc. exist - they read the <em>binary</em> packet data and display it in a form more easily understood by humans.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Dec '16, 20:01</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-57985" class="comments-container"></div><div id="comment-tools-57985" class="comment-tools"></div><div class="clear"></div><div id="comment-57985-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

