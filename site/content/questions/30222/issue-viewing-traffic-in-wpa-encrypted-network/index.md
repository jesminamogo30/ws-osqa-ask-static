+++
type = "question"
title = "Issue viewing traffic in WPA Encrypted network"
description = '''Hello,  I use Wireshark in Kali Linux, I&#x27;ve been having some problems viewing traffic of other computers inside of my own WPA encrypted network. I&#x27;ve added the WPA password, collected the EAPOL 4-Way handshake, as well as having the card in promiscuous mode. I can view DNS/ARP, SSDP (I&#x27;m unsure as t...'''
date = "2014-02-26T20:07:00Z"
lastmod = "2014-02-26T20:07:00Z"
weight = 30222
keywords = [ "sniffing", "http", "wpa2", "wpa", "sniff" ]
aliases = [ "/questions/30222" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Issue viewing traffic in WPA Encrypted network](/questions/30222/issue-viewing-traffic-in-wpa-encrypted-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30222-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30222-score" class="post-score" title="current number of votes">0</div><span id="post-30222-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I use Wireshark in Kali Linux, I've been having some problems viewing traffic of other computers inside of my own WPA encrypted network. I've added the WPA password, collected the EAPOL 4-Way handshake, as well as having the card in promiscuous mode. I can view DNS/ARP, SSDP (I'm unsure as to what this is), EAPoL, and HTTP NOTIFY (Not POST or GET). If I switch to monitor mode via either the settings or with an airmon-ng command, I can only see beacon packets from surrounding networks (lots of them). I have tried setting http.request.method == GET and have tried http.request.method == POST after doing a lot of internet usage on another laptop as well as the one with Wireshark running, with no packets of these types found. Because I still find packets with text (Not just encrypted jargon), I'm not sure if it's decrypting, or what could be happening under the hood. Are there any parameters I didn't explain in this question? Am I doing something wrong? Thank you for any help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span> <span class="post-tag tag-link-wpa" rel="tag" title="see questions tagged &#39;wpa&#39;">wpa</span> <span class="post-tag tag-link-sniff" rel="tag" title="see questions tagged &#39;sniff&#39;">sniff</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Feb '14, 20:07</strong></p><img src="https://secure.gravatar.com/avatar/38c66a8938ffedb1d78b28c23ae122ba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sypheren&#39;s gravatar image" /><p><span>Sypheren</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sypheren has no accepted answers">0%</span></p></div></div><div id="comments-container-30222" class="comments-container"></div><div id="comment-tools-30222" class="comment-tools"></div><div class="clear"></div><div id="comment-30222-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

