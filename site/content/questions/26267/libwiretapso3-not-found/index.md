+++
type = "question"
title = "libwiretap.so.3 not found"
description = '''I was trying to install wireshark 1.10.2 under Ubuntu 12.04 and encountered an error saying that libwiretap.so.3 can not open shared object file : No such file or directory. How shall I fix it?'''
date = "2013-10-21T18:06:00Z"
lastmod = "2013-10-22T18:03:00Z"
weight = 26267
keywords = [ "install" ]
aliases = [ "/questions/26267" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [libwiretap.so.3 not found](/questions/26267/libwiretapso3-not-found)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26267-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26267-score" class="post-score" title="current number of votes">0</div><span id="post-26267-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was trying to install wireshark 1.10.2 under Ubuntu 12.04 and encountered an error saying that libwiretap.so.3 can not open shared object file : No such file or directory. How shall I fix it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '13, 18:06</strong></p><img src="https://secure.gravatar.com/avatar/fcf0af2271932f936ad443e39ced807d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zeki88&#39;s gravatar image" /><p><span>zeki88</span><br />
<span class="score" title="36 reputation points">36</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zeki88 has one accepted answer">100%</span></p></div></div><div id="comments-container-26267" class="comments-container"><span id="26276"></span><div id="comment-26276" class="comment"><div id="post-26276-score" class="comment-score"></div><div class="comment-text"><p>How did you install wireshark 1.10.2?</p></div><div id="comment-26276-info" class="comment-info"><span class="comment-age">(22 Oct '13, 00:24)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-26267" class="comment-tools"></div><div class="clear"></div><div id="comment-26267-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26303"></span>

<div id="answer-container-26303" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26303-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26303-score" class="post-score" title="current number of votes">0</div><span id="post-26303-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Kurt Knochner has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Problem solved! After I uninstalled wireshark-1.6.7.1, I was able to run wireshark-1.10.2.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Oct '13, 18:03</strong></p><img src="https://secure.gravatar.com/avatar/fcf0af2271932f936ad443e39ced807d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zeki88&#39;s gravatar image" /><p><span>zeki88</span><br />
<span class="score" title="36 reputation points">36</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zeki88 has one accepted answer">100%</span></p></div></div><div id="comments-container-26303" class="comments-container"></div><div id="comment-tools-26303" class="comment-tools"></div><div class="clear"></div><div id="comment-26303-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

