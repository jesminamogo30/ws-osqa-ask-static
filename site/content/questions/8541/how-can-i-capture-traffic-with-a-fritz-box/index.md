+++
type = "question"
title = "How can i capture traffic with a fritz box?"
description = '''How can i capture wlan traffic with a fritz box by avm?'''
date = "2012-01-22T01:46:00Z"
lastmod = "2012-01-22T01:51:00Z"
weight = 8541
keywords = [ "avm", "fritz", "capture" ]
aliases = [ "/questions/8541" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can i capture traffic with a fritz box?](/questions/8541/how-can-i-capture-traffic-with-a-fritz-box)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8541-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8541-score" class="post-score" title="current number of votes">1</div><span id="post-8541-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can i capture wlan traffic with a fritz box by avm?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-avm" rel="tag" title="see questions tagged &#39;avm&#39;">avm</span> <span class="post-tag tag-link-fritz" rel="tag" title="see questions tagged &#39;fritz&#39;">fritz</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jan '12, 01:46</strong></p><img src="https://secure.gravatar.com/avatar/82430c9aeb3635c636e17c88c535774a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anon&#39;s gravatar image" /><p><span>Anon</span><br />
<span class="score" title="84 reputation points">84</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anon has one accepted answer">16%</span></p></div></div><div id="comments-container-8541" class="comments-container"></div><div id="comment-tools-8541" class="comment-tools"></div><div class="clear"></div><div id="comment-8541-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8542"></span>

<div id="answer-container-8542" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8542-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8542-score" class="post-score" title="current number of votes">3</div><span id="post-8542-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Jaap has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Most of the FRITZ!Boxes have a non-documentated page which enables a traffic capture in 'wireshark' format for different ports. Open the page in a browser with following URL: <a href="http://fritz.box/html/capture.html"><code>fritz.box/html/capture.html</code></a>, or replace <code>fritz.box</code> with your routers IP address (usually either <a href="http://192.168.178.1/html/capture.html">192.168.178.1</a> or <a href="http://192.168.178.254/html/capture.html">192.168.178.254</a>). There you can choose the ports you want to capture from. Just follow the on-screen instructions and after successful download, you can open the file directly in Wireshark.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jan '12, 01:51</strong></p><img src="https://secure.gravatar.com/avatar/82430c9aeb3635c636e17c88c535774a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anon&#39;s gravatar image" /><p><span>Anon</span><br />
<span class="score" title="84 reputation points">84</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anon has one accepted answer">16%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Jan '12, 01:21</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-8542" class="comments-container"></div><div id="comment-tools-8542" class="comment-tools"></div><div class="clear"></div><div id="comment-8542-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

