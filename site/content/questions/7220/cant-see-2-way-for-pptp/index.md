+++
type = "question"
title = "cant see 2 way for pptp"
description = '''Hi,  I am trying to measure pptp encrypted vpn traffic from my computer with wireshark. However, when I run it, I only see packets headed from one ip address to another ip address. There is no 2 directional flows that I see. It seems to only flow from my computer to the server.'''
date = "2011-11-03T08:48:00Z"
lastmod = "2011-11-03T09:04:00Z"
weight = 7220
keywords = [ "pptp" ]
aliases = [ "/questions/7220" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [cant see 2 way for pptp](/questions/7220/cant-see-2-way-for-pptp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7220-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7220-score" class="post-score" title="current number of votes">0</div><span id="post-7220-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am trying to measure pptp encrypted vpn traffic from my computer with wireshark. However, when I run it, I only see packets headed from one ip address to another ip address.</p><p>There is no 2 directional flows that I see. It seems to only flow from my computer to the server.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pptp" rel="tag" title="see questions tagged &#39;pptp&#39;">pptp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Nov '11, 08:48</strong></p><img src="https://secure.gravatar.com/avatar/2cd01b51309490ac1ee00ae615abd0f4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="desert1940fox&#39;s gravatar image" /><p><span>desert1940fox</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="desert1940fox has no accepted answers">0%</span></p></div></div><div id="comments-container-7220" class="comments-container"><span id="7221"></span><div id="comment-7221" class="comment"><div id="post-7221-score" class="comment-score"></div><div class="comment-text"><p>OS? PPTP software? Wireshark version? Capture interface you selected? Type of traffic you see? Mind you: we cannot guess this stuff.</p></div><div id="comment-7221-info" class="comment-info"><span class="comment-age">(03 Nov '11, 09:04)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-7220" class="comment-tools"></div><div class="clear"></div><div id="comment-7220-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

