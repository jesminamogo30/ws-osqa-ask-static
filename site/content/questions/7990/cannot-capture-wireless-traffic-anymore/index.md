+++
type = "question"
title = "Cannot capture wireless traffic anymore?"
description = '''Hi all, I have been using wireshark to analyze my wireless traffic for a while. The past couple of times I&#x27;ve tried to use it, my wireless card (an Atheros AR9285) doesn&#x27;t even show up on the interfaces list anymore. I re-installed WinPcap and wireshark (64-bit) but to no avail. Any ideas? J.W. Cali...'''
date = "2011-12-15T09:17:00Z"
lastmod = "2011-12-16T13:58:00Z"
weight = 7990
keywords = [ "wireless", "windows", "windows7", "wifi", "troubleshooting" ]
aliases = [ "/questions/7990" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot capture wireless traffic anymore?](/questions/7990/cannot-capture-wireless-traffic-anymore)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7990-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7990-score" class="post-score" title="current number of votes">0</div><span id="post-7990-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I have been using wireshark to analyze my wireless traffic for a while. The past couple of times I've tried to use it, my wireless card (an Atheros AR9285) doesn't even show up on the interfaces list anymore. I re-installed WinPcap and wireshark (64-bit) but to no avail. Any ideas?</p><p>J.W. California</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Dec '11, 09:17</strong></p><img src="https://secure.gravatar.com/avatar/89eba912ade87bb958b46b9c2c1d7d1f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Willows&#39;s gravatar image" /><p><span>Jim Willows</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Willows has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Dec '11, 13:58</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-7990" class="comments-container"><span id="8014"></span><div id="comment-8014" class="comment"><div id="post-8014-score" class="comment-score"></div><div class="comment-text"><p>Would it help to know I am using Windows 7 64-bit?</p></div><div id="comment-8014-info" class="comment-info"><span class="comment-age">(16 Dec '11, 09:58)</span> <span class="comment-user userinfo">Jim Willows</span></div></div><span id="8020"></span><div id="comment-8020" class="comment"><div id="post-8020-score" class="comment-score"></div><div class="comment-text"><p>This is a possible duplicate of <a href="http://ask.wireshark.org/questions/1281/npf-driver-problem-in-windows-7">NPF driver Problem in Windows 7</a>. More at Wireshark <a href="http://wiki.wireshark.org/CaptureSetup/CapturePrivileges#Windows">wiki on privileges</a>.</p></div><div id="comment-8020-info" class="comment-info"><span class="comment-age">(16 Dec '11, 13:58)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-7990" class="comment-tools"></div><div class="clear"></div><div id="comment-7990-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

