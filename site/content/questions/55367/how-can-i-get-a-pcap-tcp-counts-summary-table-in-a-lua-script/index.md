+++
type = "question"
title = "How can I get a PCAP TCP counts summary table in a Lua script?"
description = '''Hallo fellows How can I get a PCAP file summary table with API, I mean the only the count of the statistics like retransmissions ..etc. I believe there is command in Tshark can do it so fast, but I wonder if the API can provide the same or not  Thanks'''
date = "2016-09-07T01:20:00Z"
lastmod = "2016-09-07T06:31:00Z"
weight = 55367
keywords = [ "lua", "pcap", "tcp", "summary" ]
aliases = [ "/questions/55367" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I get a PCAP TCP counts summary table in a Lua script?](/questions/55367/how-can-i-get-a-pcap-tcp-counts-summary-table-in-a-lua-script)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55367-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55367-score" class="post-score" title="current number of votes">0</div><span id="post-55367-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hallo fellows</p><p>How can I get a PCAP file summary table with API, I mean the only the count of the statistics like retransmissions ..etc.</p><p>I believe there is command in Tshark can do it so fast, but I wonder if the API can provide the same or not Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-summary" rel="tag" title="see questions tagged &#39;summary&#39;">summary</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Sep '16, 01:20</strong></p><img src="https://secure.gravatar.com/avatar/7cce78d738fa16cbb8d95bb5699d8a66?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zalabany&#39;s gravatar image" /><p><span>Zalabany</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zalabany has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Sep '16, 15:29</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-55367" class="comments-container"><span id="55372"></span><div id="comment-55372" class="comment"><div id="post-55372-score" class="comment-score"></div><div class="comment-text"><p>what do you mean by 'the API'?</p></div><div id="comment-55372-info" class="comment-info"><span class="comment-age">(07 Sep '16, 06:26)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="55373"></span><div id="comment-55373" class="comment"><div id="post-55373-score" class="comment-score"></div><div class="comment-text"><p>I mean LUA API</p></div><div id="comment-55373-info" class="comment-info"><span class="comment-age">(07 Sep '16, 06:31)</span> <span class="comment-user userinfo">Zalabany</span></div></div></div><div id="comment-tools-55367" class="comment-tools"></div><div class="clear"></div><div id="comment-55367-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

