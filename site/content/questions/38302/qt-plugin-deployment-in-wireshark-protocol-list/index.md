+++
type = "question"
title = "Qt plugin deployment in wireshark protocol list"
description = '''Hi I have developed a Plugin(.dll) in qt and i want to add the particular plugin into the wireshark&#x27;s protocol list. I tried out by copying the dll file into the plugins folder or wireshark, it didn&#x27;t work out. Please any one give me some suggestions for installing the Plugins in wireshark. and how ...'''
date = "2014-12-03T04:04:00Z"
lastmod = "2014-12-03T04:29:00Z"
weight = 38302
keywords = [ "installation", "qt", "plugin" ]
aliases = [ "/questions/38302" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Qt plugin deployment in wireshark protocol list](/questions/38302/qt-plugin-deployment-in-wireshark-protocol-list)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38302-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38302-score" class="post-score" title="current number of votes">0</div><span id="post-38302-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I have developed a Plugin(.dll) in qt and i want to add the particular plugin into the wireshark's protocol list. I tried out by copying the dll file into the plugins folder or wireshark, it didn't work out.</p><p>Please any one give me some suggestions for installing the Plugins in wireshark. and how to code for wireshark.</p><p>Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span> <span class="post-tag tag-link-qt" rel="tag" title="see questions tagged &#39;qt&#39;">qt</span> <span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Dec '14, 04:04</strong></p><img src="https://secure.gravatar.com/avatar/d2eac198c88783636a4683ebc085f889?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kulasekaran&#39;s gravatar image" /><p><span>Kulasekaran</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kulasekaran has no accepted answers">0%</span></p></div></div><div id="comments-container-38302" class="comments-container"><span id="38303"></span><div id="comment-38303" class="comment"><div id="post-38303-score" class="comment-score"></div><div class="comment-text"><p>Hi, Im not sure what you mean by "in qt" have you developen a plugin that does something in the GUI? If it's a "normal" protocol plugin it should work to do as you have described if the plugin was developed with the same verion of Wireshark as the installed one. How did it "not work out"? Did you receive any error output? If you start wireshark from the build directory is your protocol available then? If not you may have done something wrong. Which in Wireshark version are you doing your development? Have you compared your Makefiles with other plugins?</p></div><div id="comment-38303-info" class="comment-info"><span class="comment-age">(03 Dec '14, 04:29)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-38302" class="comment-tools"></div><div class="clear"></div><div id="comment-38302-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

