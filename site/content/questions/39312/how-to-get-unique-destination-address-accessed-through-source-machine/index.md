+++
type = "question"
title = "How to get Unique destination address accessed through source machine"
description = '''Hi, I want to apply restriction on my system in such a way only a set of white listed sites should be accessible and nothing else. I have applied wire-shark filters but it gives me huge set of data and i only need unique site which are getting visited from that machine. Is there any way of analyzing...'''
date = "2015-01-20T01:07:00Z"
lastmod = "2015-01-20T07:06:00Z"
weight = 39312
keywords = [ "statistics" ]
aliases = [ "/questions/39312" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to get Unique destination address accessed through source machine](/questions/39312/how-to-get-unique-destination-address-accessed-through-source-machine)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39312-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39312-score" class="post-score" title="current number of votes">0</div><span id="post-39312-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I want to apply restriction on my system in such a way only a set of white listed sites should be accessible and nothing else. I have applied wire-shark filters but it gives me huge set of data and i only need unique site which are getting visited from that machine. Is there any way of analyzing the captured data. As I can capture all packets in which source is my IP address. But in that that there are many repeated records and tons of packets in which only a few are getting accessed. Please help me in analyzing the data.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jan '15, 01:07</strong></p><img src="https://secure.gravatar.com/avatar/10084d293a7869c0f7a248432d5d31d8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="salmansaifi&#39;s gravatar image" /><p><span>salmansaifi</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="salmansaifi has no accepted answers">0%</span></p></div></div><div id="comments-container-39312" class="comments-container"><span id="39315"></span><div id="comment-39315" class="comment"><div id="post-39315-score" class="comment-score"></div><div class="comment-text"><p>Are you using the right tool for the job?</p></div><div id="comment-39315-info" class="comment-info"><span class="comment-age">(20 Jan '15, 07:06)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-39312" class="comment-tools"></div><div class="clear"></div><div id="comment-39312-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

