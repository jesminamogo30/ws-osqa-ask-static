+++
type = "question"
title = "Compare 2 PCAP files"
description = '''Hello, I was wondering how can I compare 2 .pcap files ? I want to see differences as I would see if I just opened the .pcap files using Wireshark and put the two windows beside each other and check &quot;Data&quot; or &quot;Transmission Control Protocol&quot;. Thank you'''
date = "2016-02-25T05:33:00Z"
lastmod = "2016-07-21T09:48:00Z"
weight = 50507
keywords = [ "compare_two_traces" ]
aliases = [ "/questions/50507" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Compare 2 PCAP files](/questions/50507/compare-2-pcap-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50507-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50507-score" class="post-score" title="current number of votes">0</div><span id="post-50507-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I was wondering how can I compare 2 .pcap files ? I want to see differences as I would see if I just opened the .pcap files using Wireshark and put the two windows beside each other and check "Data" or "Transmission Control Protocol". Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-compare_two_traces" rel="tag" title="see questions tagged &#39;compare_two_traces&#39;">compare_two_traces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Feb '16, 05:33</strong></p><img src="https://secure.gravatar.com/avatar/14e6b46186312b23c8476dc9bd46e8bf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Norbi&#39;s gravatar image" /><p><span>Norbi</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Norbi has no accepted answers">0%</span></p></div></div><div id="comments-container-50507" class="comments-container"></div><div id="comment-tools-50507" class="comment-tools"></div><div class="clear"></div><div id="comment-50507-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54226"></span>

<div id="answer-container-54226" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54226-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54226-score" class="post-score" title="current number of votes">1</div><span id="post-54226-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As <span>@Jasper</span> mentioned in his answer to <a href="https://ask.wireshark.org/questions/51611/compare-two-captures-feature-missing-in-wireshark-20">this</a> question, you might want to take a look at TribeLab's <a href="https://community.tribelab.com/enrol/index.php?id=15">Workbench</a> tool.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jul '16, 09:48</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-54226" class="comments-container"></div><div id="comment-tools-54226" class="comment-tools"></div><div class="clear"></div><div id="comment-54226-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

