+++
type = "question"
title = "Wireshark 1.10.6 on Ubuntu 14.04 missing top bar / menus"
description = '''Hello, after loading up Ubuntu 14.04.03 LTS I installed Wireshark and the top bar which would have File/Edit/Capture/Analyze/etc is gone. I see it has been reported before. The solution was to remove appmenu-qt5. Tried that but the problem persists. Any help is appreciated.'''
date = "2017-01-19T04:22:00Z"
lastmod = "2017-01-20T10:31:00Z"
weight = 58881
keywords = [ "menus", "14.04", "missing", "ubuntu" ]
aliases = [ "/questions/58881" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 1.10.6 on Ubuntu 14.04 missing top bar / menus](/questions/58881/wireshark-1106-on-ubuntu-1404-missing-top-bar-menus)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58881-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58881-score" class="post-score" title="current number of votes">0</div><span id="post-58881-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, after loading up Ubuntu 14.04.03 LTS I installed Wireshark and the top bar which would have File/Edit/Capture/Analyze/etc is gone. I see it has been reported before. The solution was to remove appmenu-qt5. Tried that but the problem persists. Any help is appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-menus" rel="tag" title="see questions tagged &#39;menus&#39;">menus</span> <span class="post-tag tag-link-14.04" rel="tag" title="see questions tagged &#39;14.04&#39;">14.04</span> <span class="post-tag tag-link-missing" rel="tag" title="see questions tagged &#39;missing&#39;">missing</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '17, 04:22</strong></p><img src="https://secure.gravatar.com/avatar/d6872b1cae1da5fcd92837a89d05942c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tiger762&#39;s gravatar image" /><p><span>tiger762</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tiger762 has no accepted answers">0%</span></p></div></div><div id="comments-container-58881" class="comments-container"><span id="58923"></span><div id="comment-58923" class="comment"><div id="post-58923-score" class="comment-score"></div><div class="comment-text"><p>OK, I am the only one who has had this problem. Is there a workaround to get the "View" menu items? Would like to turn off packet bytes at the bottom of the screen, set time display format and name resolution options.</p></div><div id="comment-58923-info" class="comment-info"><span class="comment-age">(20 Jan '17, 10:31)</span> <span class="comment-user userinfo">tiger762</span></div></div></div><div id="comment-tools-58881" class="comment-tools"></div><div class="clear"></div><div id="comment-58881-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

