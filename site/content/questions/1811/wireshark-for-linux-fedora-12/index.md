+++
type = "question"
title = "Wireshark for linux (fedora 12)"
description = '''Hi, I have fedora 12 in my system. Can I download Wireshark for fedora 12 ? If yes, then please let me know the link as well as the steps of installation. Thanks Rke'''
date = "2011-01-19T07:42:00Z"
lastmod = "2011-04-05T05:21:00Z"
weight = 1811
keywords = [ "wireshark", "fedora", "linux" ]
aliases = [ "/questions/1811" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark for linux (fedora 12)](/questions/1811/wireshark-for-linux-fedora-12)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1811-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1811-score" class="post-score" title="current number of votes">0</div><span id="post-1811-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have fedora 12 in my system. Can I download Wireshark for fedora 12 ?</p><p>If yes, then please let me know the link as well as the steps of installation.</p><p>Thanks Rke</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-fedora" rel="tag" title="see questions tagged &#39;fedora&#39;">fedora</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '11, 07:42</strong></p><img src="https://secure.gravatar.com/avatar/24f8408c3d93fef7e0b9b0c8941a3f71?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rketest&#39;s gravatar image" /><p><span>rketest</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rketest has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jan '11, 12:25</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-1811" class="comments-container"></div><div id="comment-tools-1811" class="comment-tools"></div><div class="clear"></div><div id="comment-1811-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="1814"></span>

<div id="answer-container-1814" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1814-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1814-score" class="post-score" title="current number of votes">0</div><span id="post-1814-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes: Wireshark can be downloaded from the Fedora 12 repository.</p><p>Just use 'yum install wireshark' (or use packagekit or however you do Fedora installs).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jan '11, 08:26</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jan '11, 08:27</strong> </span></p></div></div><div id="comments-container-1814" class="comments-container"><span id="1816"></span><div id="comment-1816" class="comment"><div id="post-1816-score" class="comment-score"></div><div class="comment-text"><p>You may also want 'wireshark-gnome' which I think uses 'consolehelper' "to help console users run system programs" from a user (non-root) console.</p></div><div id="comment-1816-info" class="comment-info"><span class="comment-age">(19 Jan '11, 09:31)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="1826"></span><div id="comment-1826" class="comment"><div id="post-1826-score" class="comment-score"></div><div class="comment-text"><p>I used the same "yum install wireshrak" and also "yum install wireshrak-gnome". But both were not working.</p><p>It was showing "No wireshrak package present" etc</p></div><div id="comment-1826-info" class="comment-info"><span class="comment-age">(19 Jan '11, 22:11)</span> <span class="comment-user userinfo">rketest</span></div></div><span id="1829"></span><div id="comment-1829" class="comment"><div id="post-1829-score" class="comment-score"></div><div class="comment-text"><p>(I've converted your answer to a comment to conform to how this site works. Please see the FAQ for info).</p></div><div id="comment-1829-info" class="comment-info"><span class="comment-age">(20 Jan '11, 07:05)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="1830"></span><div id="comment-1830" class="comment"><div id="post-1830-score" class="comment-score"></div><div class="comment-text"><p>Re: I used the same "yum install wireshrak" ....</p><p>Hopefully "wireshrak" is just a typo and you used "wireshark".</p><p>Assuming that's the case, what do you get for the command</p><p>yum repolist all</p><p>Also: Have you successfully installed Fedora packages on your system? If so, how?</p></div><div id="comment-1830-info" class="comment-info"><span class="comment-age">(20 Jan '11, 07:27)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="1831"></span><div id="comment-1831" class="comment"><div id="post-1831-score" class="comment-score"></div><div class="comment-text"><p>Or you can try to squeeze an extra .5% of performance out of it by downloading the source, tweaking it for your system, voiding the warranty, and compiling/running. &lt;/sarcasm&gt;</p></div><div id="comment-1831-info" class="comment-info"><span class="comment-age">(20 Jan '11, 07:39)</span> <span class="comment-user userinfo">GeonJay</span></div></div></div><div id="comment-tools-1814" class="comment-tools"></div><div class="clear"></div><div id="comment-1814-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="3340"></span>

<div id="answer-container-3340" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3340-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3340-score" class="post-score" title="current number of votes">0</div><span id="post-3340-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>i have installed the wireshark using yum. i want to ask now how can i use it to take the traces of packets?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Apr '11, 05:21</strong></p><img src="https://secure.gravatar.com/avatar/e99b52669e12a7dbdc1114c12adddda5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shree%20Garg&#39;s gravatar image" /><p><span>Shree Garg</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shree Garg has no accepted answers">0%</span></p></div></div><div id="comments-container-3340" class="comments-container"></div><div id="comment-tools-3340" class="comment-tools"></div><div class="clear"></div><div id="comment-3340-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

