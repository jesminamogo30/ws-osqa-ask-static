+++
type = "question"
title = "Change highlight color or appearance of currently selected packet?"
description = '''New Surface Book, new Windows 10, new 64-bit version of Wireshark - Version 2.0.1 (v2.0.1-0-g59ea380 from master-2.0). The highlight for the currently selected packet adds a pale blue-green cast to whatever color is already there. Pretty subtle on a yellow or tan packet, totally invisible on a blue ...'''
date = "2016-02-25T14:30:00Z"
lastmod = "2016-02-25T14:30:00Z"
weight = 50517
keywords = [ "highlight", "coloring", "selected", "visibility" ]
aliases = [ "/questions/50517" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Change highlight color or appearance of currently selected packet?](/questions/50517/change-highlight-color-or-appearance-of-currently-selected-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50517-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50517-score" class="post-score" title="current number of votes">0</div><span id="post-50517-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>New Surface Book, new Windows 10, new 64-bit version of Wireshark - Version 2.0.1 (v2.0.1-0-g59ea380 from master-2.0). The highlight for the currently selected packet adds a pale blue-green cast to whatever color is already there. Pretty subtle on a yellow or tan packet, totally invisible on a blue or green packet, especially a dark one. No effect at all on the text color.</p><p>I guess this is in keeping with Win 10's (and Office 2016's!) flat, pale color scheme, and the trend on the web to make the actual content text as pale as possible. But it makes it completely impossible to glance up and see where the detail you are looking at came from!</p><p>I found <a href="https://ask.wireshark.org/questions/10134/change-selected-packet-colors">Change selected packet colors</a> which seems to suggest the selected packet at that time had its text color reversed. And that the actual colors are up to the OS.</p><p>Is there some way to permanently reconfigure the highlight mode or color? If not, since color alone seems to be so vulnerable to outside changes, might I suggest the highlight should involve a different font or weight or size? Or an outline around the whole line of the selected packet?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-highlight" rel="tag" title="see questions tagged &#39;highlight&#39;">highlight</span> <span class="post-tag tag-link-coloring" rel="tag" title="see questions tagged &#39;coloring&#39;">coloring</span> <span class="post-tag tag-link-selected" rel="tag" title="see questions tagged &#39;selected&#39;">selected</span> <span class="post-tag tag-link-visibility" rel="tag" title="see questions tagged &#39;visibility&#39;">visibility</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Feb '16, 14:30</strong></p><img src="https://secure.gravatar.com/avatar/8497d1e09444b92e8424122da294aa4b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LorenAmelang&#39;s gravatar image" /><p><span>LorenAmelang</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LorenAmelang has no accepted answers">0%</span></p></div></div><div id="comments-container-50517" class="comments-container"></div><div id="comment-tools-50517" class="comment-tools"></div><div class="clear"></div><div id="comment-50517-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

