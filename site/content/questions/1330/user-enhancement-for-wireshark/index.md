+++
type = "question"
title = "User enhancement for wireshark"
description = '''I have used Ethereal and Wireshark for many years and have hundreds of traces. I would like the ability to enter text notes attached to the trace to allow me to keep a record of the problem scenario and where in the trace the problem is recorded plus my diagnosis of the trace etc. I am unaware if th...'''
date = "2010-12-13T13:14:00Z"
lastmod = "2010-12-15T15:31:00Z"
weight = 1330
keywords = [ "enhancement" ]
aliases = [ "/questions/1330" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [User enhancement for wireshark](/questions/1330/user-enhancement-for-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1330-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1330-score" class="post-score" title="current number of votes">0</div><span id="post-1330-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have used Ethereal and Wireshark for many years and have hundreds of traces. I would like the ability to enter text notes attached to the trace to allow me to keep a record of the problem scenario and where in the trace the problem is recorded plus my diagnosis of the trace etc. I am unaware if this facility already exists but I for one would find it very useful.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-enhancement" rel="tag" title="see questions tagged &#39;enhancement&#39;">enhancement</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Dec '10, 13:14</strong></p><img src="https://secure.gravatar.com/avatar/f3e1cc1aa000d4c783afdf5b320d5462?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="farmerk&#39;s gravatar image" /><p><span>farmerk</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="farmerk has no accepted answers">0%</span></p></div></div><div id="comments-container-1330" class="comments-container"><span id="1333"></span><div id="comment-1333" class="comment"><div id="post-1333-score" class="comment-score"></div><div class="comment-text"><p>Yup, this was discussed during one of the sessions in Sharkfest. the other thing I would love to see is tabbed filtered traces (so if you filter on something, it'll be a tabbed window). With the option of preserving original frame numbers.</p></div><div id="comment-1333-info" class="comment-info"><span class="comment-age">(13 Dec '10, 18:33)</span> <span class="comment-user userinfo">hansangb</span></div></div></div><div id="comment-tools-1330" class="comment-tools"></div><div class="clear"></div><div id="comment-1330-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1338"></span>

<div id="answer-container-1338" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1338-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1338-score" class="post-score" title="current number of votes">0</div><span id="post-1338-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is a feature that is wanted by more people. The current default file format does not support this. However the pcap-ng file format does make this possible, it only needs to be implemented. This is on the <a href="http://wiki.wireshark.org/Development/PcapNg">wireshark pcap-ng wishlist</a> awaiting someone to write the code ;-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Dec '10, 01:25</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-1338" class="comments-container"><span id="1352"></span><div id="comment-1352" class="comment"><div id="post-1352-score" class="comment-score"></div><div class="comment-text"><p>so get busy, ya slacker! ;)</p></div><div id="comment-1352-info" class="comment-info"><span class="comment-age">(14 Dec '10, 18:57)</span> <span class="comment-user userinfo">hansangb</span></div></div><span id="1354"></span><div id="comment-1354" class="comment"><div id="post-1354-score" class="comment-score"></div><div class="comment-text"><p>I just thought it would be the best "first development project" for you actually... I'll be your mentor ;-)</p></div><div id="comment-1354-info" class="comment-info"><span class="comment-age">(14 Dec '10, 23:05)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="1366"></span><div id="comment-1366" class="comment"><div id="post-1366-score" class="comment-score"></div><div class="comment-text"><p>If I have to break out my Kernighan and Ritchie, god help us all! That'll be the start of the downfall of Wireshark! :) I'll leave that up to the professionals, how about that?</p></div><div id="comment-1366-info" class="comment-info"><span class="comment-age">(15 Dec '10, 15:31)</span> <span class="comment-user userinfo">hansangb</span></div></div></div><div id="comment-tools-1338" class="comment-tools"></div><div class="clear"></div><div id="comment-1338-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

