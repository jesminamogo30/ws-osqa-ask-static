+++
type = "question"
title = "Mac colorizing filter"
description = '''I am using the latest Wireshark version 1.8.3 on my mac along with X11 which makes it work. The issue that I am facing is that whenever i try to create a new colorizing rule, it crashes Wireshark and closes it. It doesn&#x27;t even ask for saving the capture and just shuts it down. If you know the soluti...'''
date = "2012-10-08T09:43:00Z"
lastmod = "2012-10-08T09:52:00Z"
weight = 14776
keywords = [ "mac", "coloring" ]
aliases = [ "/questions/14776" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Mac colorizing filter](/questions/14776/mac-colorizing-filter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14776-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14776-score" class="post-score" title="current number of votes">0</div><span id="post-14776-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using the latest Wireshark version 1.8.3 on my mac along with X11 which makes it work. The issue that I am facing is that whenever i try to create a new colorizing rule, it crashes Wireshark and closes it. It doesn't even ask for saving the capture and just shuts it down. If you know the solution to this problem, let me know.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-coloring" rel="tag" title="see questions tagged &#39;coloring&#39;">coloring</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Oct '12, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/4d550bae0a586af6378305b8f23f4447?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rttl_Snk&#39;s gravatar image" /><p><span>Rttl_Snk</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rttl_Snk has no accepted answers">0%</span></p></div></div><div id="comments-container-14776" class="comments-container"><span id="14777"></span><div id="comment-14777" class="comment"><div id="post-14777-score" class="comment-score"></div><div class="comment-text"><p>I am capturing 802.11 packets with internal nic card and trying to create a new coloring filter such wlan.fc.retry == 1.</p></div><div id="comment-14777-info" class="comment-info"><span class="comment-age">(08 Oct '12, 09:52)</span> <span class="comment-user userinfo">Rttl_Snk</span></div></div></div><div id="comment-tools-14776" class="comment-tools"></div><div class="clear"></div><div id="comment-14776-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

