+++
type = "question"
title = "Monitor wifi traffic."
description = '''Hi, I have Dlink-600L, Firmware Version 2.00, would I be able to monitor all the wifi traffic including the traffic from my cell phone using wifi, If yes then would wireshark installed on win7 be enough to achieve this thing, please let me know.'''
date = "2013-11-14T02:07:00Z"
lastmod = "2013-11-14T02:07:00Z"
weight = 26987
keywords = [ "wifi" ]
aliases = [ "/questions/26987" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Monitor wifi traffic.](/questions/26987/monitor-wifi-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26987-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26987-score" class="post-score" title="current number of votes">0</div><span id="post-26987-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have Dlink-600L, Firmware Version 2.00, would I be able to monitor all the wifi traffic including the traffic from my cell phone using wifi, If yes then would wireshark installed on win7 be enough to achieve this thing, please let me know.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '13, 02:07</strong></p><img src="https://secure.gravatar.com/avatar/54da1322edf1f20d8d84a541c97e248d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="faisal&#39;s gravatar image" /><p><span>faisal</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="faisal has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>14 Nov '13, 02:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-26987" class="comments-container"></div><div id="comment-tools-26987" class="comment-tools"></div><div class="clear"></div><div id="comment-26987-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

