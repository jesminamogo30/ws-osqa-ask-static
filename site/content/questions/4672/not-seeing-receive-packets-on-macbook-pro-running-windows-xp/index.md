+++
type = "question"
title = "Not seeing receive packets on Macbook Pro running Windows XP"
description = '''When attempting to do a packet capture with Wireshark running under Windows XP on my Macbook Pro, I only see packets sent from the device I am monitoring. I never see any packets sent to the device. If I use the Mac version of Wireshark I see both sent and received packets. For instance, when using ...'''
date = "2011-06-22T09:11:00Z"
lastmod = "2011-06-23T10:16:00Z"
weight = 4672
keywords = [ "macbook" ]
aliases = [ "/questions/4672" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Not seeing receive packets on Macbook Pro running Windows XP](/questions/4672/not-seeing-receive-packets-on-macbook-pro-running-windows-xp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4672-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4672-score" class="post-score" title="current number of votes">0</div><span id="post-4672-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When attempting to do a packet capture with Wireshark running under Windows XP on my Macbook Pro, I only see packets sent from the device I am monitoring. I never see any packets sent to the device. If I use the Mac version of Wireshark I see both sent and received packets.</p><p>For instance, when using another device to PING the monitored device, I only see the ICMP replies from the monitored device.</p><p>This is a bootcamp installation of XP and I am booting directly into the partition and not running under VM.<br />
</p><p>In addition, this behavior is consistent when using a Fluke Networks monitoring tool as well.</p><p>Has anyone else and any success in using Wireshark under Windows XP on a Macbook Pro?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-macbook" rel="tag" title="see questions tagged &#39;macbook&#39;">macbook</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jun '11, 09:11</strong></p><img src="https://secure.gravatar.com/avatar/0bcc34019dd1817a577840cecab87186?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rjenk&#39;s gravatar image" /><p><span>rjenk</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rjenk has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-4672" class="comments-container"><span id="4699"></span><div id="comment-4699" class="comment"><div id="post-4699-score" class="comment-score"></div><div class="comment-text"><p>Are you capturing on the Ethernet interface or the AirPort interface?</p></div><div id="comment-4699-info" class="comment-info"><span class="comment-age">(23 Jun '11, 10:16)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-4672" class="comment-tools"></div><div class="clear"></div><div id="comment-4672-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

