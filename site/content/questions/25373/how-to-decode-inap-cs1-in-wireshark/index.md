+++
type = "question"
title = "How to decode INAP CS1+ in wireshark"
description = '''I have captured a snoop trace between Ericsson CCN and SDP nodes and I am trying to open it in Wireshark. INAP packets are not getting decoded using wireshark. I have selected SSN 6-252 in INAP protocol preferences section but still no joy... Pl help if there is any alternate way to open the packet ...'''
date = "2013-09-30T05:41:00Z"
lastmod = "2013-09-30T05:41:00Z"
weight = 25373
keywords = [ "inap" ]
aliases = [ "/questions/25373" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to decode INAP CS1+ in wireshark](/questions/25373/how-to-decode-inap-cs1-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25373-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25373-score" class="post-score" title="current number of votes">0</div><span id="post-25373-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured a snoop trace between Ericsson CCN and SDP nodes and I am trying to open it in Wireshark. INAP packets are not getting decoded using wireshark.</p><p>I have selected SSN 6-252 in INAP protocol preferences section but still no joy...</p><p>Pl help if there is any alternate way to open the packet of INAP appln using wireshark...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-inap" rel="tag" title="see questions tagged &#39;inap&#39;">inap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '13, 05:41</strong></p><img src="https://secure.gravatar.com/avatar/7372280898346a581516a902d1bc82f3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ashu1983&#39;s gravatar image" /><p><span>ashu1983</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ashu1983 has no accepted answers">0%</span></p></div></div><div id="comments-container-25373" class="comments-container"></div><div id="comment-tools-25373" class="comment-tools"></div><div class="clear"></div><div id="comment-25373-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

