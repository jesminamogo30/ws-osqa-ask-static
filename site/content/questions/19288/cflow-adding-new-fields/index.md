+++
type = "question"
title = "CFLOW adding new fields"
description = '''Adding different fields to a CFLOW'''
date = "2013-03-07T13:19:00Z"
lastmod = "2013-03-07T14:05:00Z"
weight = 19288
keywords = [ "cflow" ]
aliases = [ "/questions/19288" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [CFLOW adding new fields](/questions/19288/cflow-adding-new-fields)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19288-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19288-score" class="post-score" title="current number of votes">0</div><span id="post-19288-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Adding different fields to a CFLOW</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cflow" rel="tag" title="see questions tagged &#39;cflow&#39;">cflow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Mar '13, 13:19</strong></p><img src="https://secure.gravatar.com/avatar/162021ed9fdca576a7b75816c5213062?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="CFLOWextension&#39;s gravatar image" /><p><span>CFLOWextension</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="CFLOWextension has no accepted answers">0%</span></p></div></div><div id="comments-container-19288" class="comments-container"><span id="19291"></span><div id="comment-19291" class="comment"><div id="post-19291-score" class="comment-score"></div><div class="comment-text"><p>What was the question?</p></div><div id="comment-19291-info" class="comment-info"><span class="comment-age">(07 Mar '13, 14:05)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-19288" class="comment-tools"></div><div class="clear"></div><div id="comment-19288-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

