+++
type = "question"
title = "What does a negative nmber of lost packet mean in RTP stream?"
description = '''A result of RTP stream captured by me is analyzed in Wireshark and the lot packets &amp;amp; lots rate are negative, what does &quot;negative&quot; mean here? Total RTP packets = 1074 (expected 1074) Lost RTP packets = -19 (-1.77%) Sequence errors = 77 '''
date = "2015-04-27T15:25:00Z"
lastmod = "2015-04-28T01:06:00Z"
weight = 41901
keywords = [ "rtp", "lost" ]
aliases = [ "/questions/41901" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [What does a negative nmber of lost packet mean in RTP stream?](/questions/41901/what-does-a-negative-nmber-of-lost-packet-mean-in-rtp-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41901-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41901-score" class="post-score" title="current number of votes">0</div><span id="post-41901-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>A result of RTP stream captured by me is analyzed in Wireshark and the lot packets &amp; lots rate are negative, what does "negative" mean here?</p><p>Total RTP packets = 1074 (expected 1074) <strong><em>Lost RTP packets = -19 (-1.77%)</em></strong> Sequence errors = 77</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-lost" rel="tag" title="see questions tagged &#39;lost&#39;">lost</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Apr '15, 15:25</strong></p><img src="https://secure.gravatar.com/avatar/d10e76912ae0a0d745f3451d29395d86?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DiveDave&#39;s gravatar image" /><p><span>DiveDave</span><br />
<span class="score" title="21 reputation points">21</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DiveDave has one accepted answer">100%</span></p></div></div><div id="comments-container-41901" class="comments-container"><span id="41902"></span><div id="comment-41902" class="comment"><div id="post-41902-score" class="comment-score"></div><div class="comment-text"><p>BTW, why it's a negative number?</p></div><div id="comment-41902-info" class="comment-info"><span class="comment-age">(27 Apr '15, 15:32)</span> <span class="comment-user userinfo">DiveDave</span></div></div></div><div id="comment-tools-41901" class="comment-tools"></div><div class="clear"></div><div id="comment-41901-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41911"></span>

<div id="answer-container-41911" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41911-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41911-score" class="post-score" title="current number of votes">0</div><span id="post-41911-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="DiveDave has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The RTP anallysis is geared towards Audio G711, for other streams the results may be wrong or misleading.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '15, 01:06</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-41911" class="comments-container"></div><div id="comment-tools-41911" class="comment-tools"></div><div class="clear"></div><div id="comment-41911-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

