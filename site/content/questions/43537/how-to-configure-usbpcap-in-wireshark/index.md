+++
type = "question"
title = "How to Configure USBPCAP in wireshark?"
description = '''Hi all, Thank you for your valuable support. I want to know how to Configure USBPCAP in wireshark and how to see the USB interface like 4g dongle in wireshark. Thanks and Regards, Sathish'''
date = "2015-06-25T00:06:00Z"
lastmod = "2015-06-25T08:13:00Z"
weight = 43537
keywords = [ "pcap", "usbpcap" ]
aliases = [ "/questions/43537" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to Configure USBPCAP in wireshark?](/questions/43537/how-to-configure-usbpcap-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43537-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43537-score" class="post-score" title="current number of votes">0</div><span id="post-43537-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>Thank you for your valuable support.</p><p>I want to know how to Configure USBPCAP in wireshark and how to see the USB interface like 4g dongle in wireshark.</p><p>Thanks and Regards, Sathish</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-usbpcap" rel="tag" title="see questions tagged &#39;usbpcap&#39;">usbpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jun '15, 00:06</strong></p><img src="https://secure.gravatar.com/avatar/7ba5607f38325cbf87766b918e1d76a8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sathish%20kannan&#39;s gravatar image" /><p><span>Sathish kannan</span><br />
<span class="score" title="6 reputation points">6</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sathish kannan has no accepted answers">0%</span></p></div></div><div id="comments-container-43537" class="comments-container"></div><div id="comment-tools-43537" class="comment-tools"></div><div class="clear"></div><div id="comment-43537-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43540"></span>

<div id="answer-container-43540" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43540-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43540-score" class="post-score" title="current number of votes">1</div><span id="post-43540-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I want to know how to Configure USBPCAP in wireshark and how to see the USB interface like 4g dongle in wireshark.</p></blockquote><p>It's all documented on the USBPcap site!</p><blockquote><p><a href="http://desowin.org/usbpcap/tour.html">http://desowin.org/usbpcap/tour.html</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jun '15, 04:02</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-43540" class="comments-container"><span id="43547"></span><div id="comment-43547" class="comment"><div id="post-43547-score" class="comment-score"></div><div class="comment-text"><p>Note that the next version of USBPcap (that should be out hopefully soon) combined with Wireshark 1.99.7 will allow selecting the USB devices and launching captures from the GUI. It will make USB capturing much more easy on Windows.</p></div><div id="comment-43547-info" class="comment-info"><span class="comment-age">(25 Jun '15, 08:13)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-43540" class="comment-tools"></div><div class="clear"></div><div id="comment-43540-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

