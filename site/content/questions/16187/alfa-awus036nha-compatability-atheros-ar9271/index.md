+++
type = "question"
title = "Alfa AWUS036NHA Compatability (Atheros AR9271)"
description = '''Having read somewhere that the AWUS036NHA was a good unit for monitoring in promiscuous mode, I bought one. But, I am not seeing anything on it at all. Not even the traffic in and out of this computer. The LAN and the Intel chipset both work as expected. This is on W7 64 bit. Am I missing something ...'''
date = "2012-11-21T19:15:00Z"
lastmod = "2012-11-22T03:03:00Z"
weight = 16187
keywords = [ "awus036nha", "atheros", "ar9271" ]
aliases = [ "/questions/16187" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Alfa AWUS036NHA Compatability (Atheros AR9271)](/questions/16187/alfa-awus036nha-compatability-atheros-ar9271)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16187-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16187-score" class="post-score" title="current number of votes">0</div><span id="post-16187-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Having read somewhere that the AWUS036NHA was a good unit for monitoring in promiscuous mode, I bought one.</p><p>But, I am not seeing anything on it at all. Not even the traffic in and out of this computer. The LAN and the Intel chipset both work as expected.</p><p>This is on W7 64 bit.</p><p>Am I missing something obvious in the configuration?</p><p>Might it work on XP? 32 bit? Linux?</p><p>Does anyone have one working?</p><p>Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-awus036nha" rel="tag" title="see questions tagged &#39;awus036nha&#39;">awus036nha</span> <span class="post-tag tag-link-atheros" rel="tag" title="see questions tagged &#39;atheros&#39;">atheros</span> <span class="post-tag tag-link-ar9271" rel="tag" title="see questions tagged &#39;ar9271&#39;">ar9271</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '12, 19:15</strong></p><img src="https://secure.gravatar.com/avatar/bb011c5e29824a1aea7b6ede20c4f6f4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GeoNomad&#39;s gravatar image" /><p><span>GeoNomad</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GeoNomad has no accepted answers">0%</span></p></div></div><div id="comments-container-16187" class="comments-container"></div><div id="comment-tools-16187" class="comment-tools"></div><div class="clear"></div><div id="comment-16187-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16197"></span>

<div id="answer-container-16197" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16197-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16197-score" class="post-score" title="current number of votes">0</div><span id="post-16197-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Monitor mode on Windows is "tricky" (means not possible in most cases).</p><blockquote><p><code>http://wiki.wireshark.org/CaptureSetup/WLAN</code><br />
</p></blockquote><p>I recommend <a href="http://www.backtrack-linux.org/">BackTrack</a>. There are a lot of tutorials how to use the Alfa with BackTrack. The BackTrack community is also quite active.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Nov '12, 03:03</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-16197" class="comments-container"></div><div id="comment-tools-16197" class="comment-tools"></div><div class="clear"></div><div id="comment-16197-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

