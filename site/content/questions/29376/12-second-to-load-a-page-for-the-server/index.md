+++
type = "question"
title = "12 second to load a page for the Server"
description = '''Dose anyone see and issue with the trace below other the 35 second FIN ACK, if there is a issue is the trace can you please enplane to me what i am missing please https://www.cloudshark.org/captures/e42f2386a047 your complaining of the browser takes to long to open the file on the server.'''
date = "2014-02-01T21:23:00Z"
lastmod = "2014-02-09T14:07:00Z"
weight = 29376
keywords = [ "delay_page" ]
aliases = [ "/questions/29376" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [12 second to load a page for the Server](/questions/29376/12-second-to-load-a-page-for-the-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29376-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29376-score" class="post-score" title="current number of votes">1</div><span id="post-29376-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dose anyone see and issue with the trace below other the 35 second FIN ACK, if there is a issue is the trace can you please enplane to me what i am missing please</p><p><a href="https://www.cloudshark.org/captures/e42f2386a047">https://www.cloudshark.org/captures/e42f2386a047</a></p><p>your complaining of the browser takes to long to open the file on the server.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delay_page" rel="tag" title="see questions tagged &#39;delay_page&#39;">delay_page</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '14, 21:23</strong></p><img src="https://secure.gravatar.com/avatar/b616f858ccbee3de56d053f1b002a757?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ernest%20Johnson&#39;s gravatar image" /><p><span>Ernest Johnson</span><br />
<span class="score" title="26 reputation points">26</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ernest Johnson has no accepted answers">0%</span></p></div></div><div id="comments-container-29376" class="comments-container"></div><div id="comment-tools-29376" class="comment-tools"></div><div class="clear"></div><div id="comment-29376-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="29377"></span>

<div id="answer-container-29377" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29377-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29377-score" class="post-score" title="current number of votes">2</div><span id="post-29377-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no issue in this trace besides the client <strong>NOT</strong> sending an HTTP request in the first session after TLS negotiation. <img src="https://osqa-ask.wireshark.org/upfiles/Selection_034.png" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Feb '14, 00:25</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></img></div></div><div id="comments-container-29377" class="comments-container"></div><div id="comment-tools-29377" class="comment-tools"></div><div class="clear"></div><div id="comment-29377-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="29392"></span>

<div id="answer-container-29392" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29392-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29392-score" class="post-score" title="current number of votes">2</div><span id="post-29392-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are two connection attempts in the capture file. The first one (tcp.stream eq 0) shows a gap of ~35 seconds, before the client closes the connection. The second connection (tcp.stream eq 1) <strong>does not show</strong> that gap.</p><p>I believe the following could have happened: There is a CRL dsitribution point in the cert of 195.35.91.51 (secure.worldpay.com): <a href="http://SVRSecure-G3-crl.verisign.com/SVRSecureG3.crl">http://SVRSecure-G3-crl.verisign.com/SVRSecureG3.crl</a> . Maybe your client tries to get that CRL (maybe even for the CAs) and is unable to fetch it for whatever reason. After some time it gives up as it realizes it cannot check the validity of that cert (behavior depends on the client TLS settings). During the second connection attempt it already knows it cannot get the CRL and thus it does not even try. As a result it continues with the HTTP request.</p><p>Please check other communication attempts of the client while it is accessing 195.35.91.51. Maybe you'll see the (failing) attempt to fetch the CRL in some way (http, ldap or OCSP).</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Feb '14, 03:33</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-29392" class="comments-container"><span id="29587"></span><div id="comment-29587" class="comment"><div id="post-29587-score" class="comment-score"></div><div class="comment-text"><p>Thanks you so much for showing me that, now it is on the Server guys and the people who added the Cert to the box Thanks again Kurt</p></div><div id="comment-29587-info" class="comment-info"><span class="comment-age">(09 Feb '14, 14:01)</span> <span class="comment-user userinfo">ejohnson7</span></div></div><span id="29588"></span><div id="comment-29588" class="comment"><div id="post-29588-score" class="comment-score"></div><div class="comment-text"><p>Tanks Kurt for the help, now it is not theme the network is good</p></div><div id="comment-29588-info" class="comment-info"><span class="comment-age">(09 Feb '14, 14:07)</span> <span class="comment-user userinfo">ejohnson7</span></div></div></div><div id="comment-tools-29392" class="comment-tools"></div><div class="clear"></div><div id="comment-29392-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

