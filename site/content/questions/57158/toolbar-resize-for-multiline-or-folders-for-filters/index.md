+++
type = "question"
title = "Toolbar resize for multiline or &quot;folders&quot; for filters"
description = '''Hi Experts I don&#x27;t know if what I&#x27;m asking if possible or not as I couldn&#x27;t find any information about it in internet. Due to my job, I have plenty of saved filters, so they don&#x27;t fit in the toolbar. I would like to be able to resize the toolbar to have 2 lines for filters or have &quot;folders&quot; to sort ...'''
date = "2016-11-08T07:31:00Z"
lastmod = "2017-01-10T06:11:00Z"
weight = 57158
keywords = [ "filter", "multiline", "resize", "toolbar" ]
aliases = [ "/questions/57158" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Toolbar resize for multiline or "folders" for filters](/questions/57158/toolbar-resize-for-multiline-or-folders-for-filters)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57158-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57158-score" class="post-score" title="current number of votes">0</div><span id="post-57158-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Experts</p><p>I don't know if what I'm asking if possible or not as I couldn't find any information about it in internet. Due to my job, I have plenty of saved filters, so they don't fit in the toolbar. I would like to be able to resize the toolbar to have 2 lines for filters or have "folders" to sort the filters based on the type of error or any other category (like bookmarks in the browser).</p><p>Anyone knows if it's possible?</p><p>Thanks in advance</p><p>Osito</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-multiline" rel="tag" title="see questions tagged &#39;multiline&#39;">multiline</span> <span class="post-tag tag-link-resize" rel="tag" title="see questions tagged &#39;resize&#39;">resize</span> <span class="post-tag tag-link-toolbar" rel="tag" title="see questions tagged &#39;toolbar&#39;">toolbar</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '16, 07:31</strong></p><img src="https://secure.gravatar.com/avatar/0e9b510379013638f59658b49d7d38cb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="osito&#39;s gravatar image" /><p><span>osito</span><br />
<span class="score" title="0 reputation points">0</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="osito has one accepted answer">50%</span></p></div></div><div id="comments-container-57158" class="comments-container"></div><div id="comment-tools-57158" class="comment-tools"></div><div class="clear"></div><div id="comment-57158-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58640"></span>

<div id="answer-container-58640" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58640-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58640-score" class="post-score" title="current number of votes">0</div><span id="post-58640-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, there's no such capability currently. If it's a feature you're interested in the place to request it would be <a href="https://bugs.wireshark.org">bugzilla</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jan '17, 06:11</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-58640" class="comments-container"></div><div id="comment-tools-58640" class="comment-tools"></div><div class="clear"></div><div id="comment-58640-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

