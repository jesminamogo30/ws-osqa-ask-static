+++
type = "question"
title = "How to block a site"
description = '''Hi, I think someone on my network is using wireshark to look at what I am doing on the web. How can I stop them from being able to do that. I do not have a clue how to use wireshark so please give basic step by step instructions. Thank you.'''
date = "2016-11-27T07:08:00Z"
lastmod = "2016-11-27T11:28:00Z"
weight = 57665
keywords = [ "block" ]
aliases = [ "/questions/57665" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to block a site](/questions/57665/how-to-block-a-site)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57665-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57665-score" class="post-score" title="current number of votes">0</div><span id="post-57665-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I think someone on my network is using wireshark to look at what I am doing on the web. How can I stop them from being able to do that. I do not have a clue how to use wireshark so please give basic step by step instructions. Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-block" rel="tag" title="see questions tagged &#39;block&#39;">block</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Nov '16, 07:08</strong></p><img src="https://secure.gravatar.com/avatar/09d1878614e350b3cd24d0d1515f4efd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cheesevampire&#39;s gravatar image" /><p><span>cheesevampire</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cheesevampire has no accepted answers">0%</span></p></div></div><div id="comments-container-57665" class="comments-container"><span id="57666"></span><div id="comment-57666" class="comment"><div id="post-57666-score" class="comment-score"></div><div class="comment-text"><p>you can't find them! this attack is native and you can't find them;</p></div><div id="comment-57666-info" class="comment-info"><span class="comment-age">(27 Nov '16, 09:09)</span> <span class="comment-user userinfo">Babyy</span></div></div></div><div id="comment-tools-57665" class="comment-tools"></div><div class="clear"></div><div id="comment-57665-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57667"></span>

<div id="answer-container-57667" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57667-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57667-score" class="post-score" title="current number of votes">1</div><span id="post-57667-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Surfing on the Internet is like talking to other people in the bus. Everybody can listen in on the conversation.</p><p>There are two options to increase your privacy: 1. Use SSL encryption. 2. Use a proxy server.</p><p><strong>SSL Encryption</strong></p><p>SSL encryption is used if the address line in your browser starts with https:// Note the character "s", which stands for SSL. Web sites used with http:// indicate plain text traffic: "Everybody" can read and even modify the data stream.</p><p>This comes with a caveat: If you use online banking, everybody can see the name of your bank, but not the actual balance on your account: Only the data is encrypted, but it is still easy to pick up the names of the web sites that you are visiting.</p><p><strong>Proxy Server</strong></p><p>This is, where a proxy server comes in: If you use a proxy server, <em>all</em> traffic will be directed to the proxy server. The proxy will make the request on your behalf and send you the results.</p><p>Needless to say, you should only use proxy servers, that you trust. The major antivirus companies offer some type of "Internet Security Suite", that offers optional proxy servers.</p><p>Some malicious pieces of software (Trojan horses, or "trojans" for short) would even go so far as to register an evil proxy server to intercept all your traffic. As these evil proxy servers are often run by criminal organizations they usually "only" pick up data that can be sold: Access to your online banking, credit card information, even the passwords for popular online games have a certain black market value.</p><p><strong>Rule out a malware infection</strong></p><p>If you think, that someone is following your Internet activities, please make sure that your computer is not infected with malware. Run a full anti virus scan with the most recent patterns. Take your computer to a specialist if you are not sure how to do this or how to read the virus scanner output.</p><p>Good hunting</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Nov '16, 11:28</strong></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packethunter has 8 accepted answers">8%</span></p></div></div><div id="comments-container-57667" class="comments-container"></div><div id="comment-tools-57667" class="comment-tools"></div><div class="clear"></div><div id="comment-57667-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

