+++
type = "question"
title = "Old Fashion Fax"
description = '''Does anyone know of a tool to use to analyze analog fax transmissions - T30.'''
date = "2014-01-31T08:57:00Z"
lastmod = "2014-01-31T14:17:00Z"
weight = 29354
keywords = [ "analyze", "fax", "t.30", "t30" ]
aliases = [ "/questions/29354" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Old Fashion Fax](/questions/29354/old-fashion-fax)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29354-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29354-score" class="post-score" title="current number of votes">0</div><span id="post-29354-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anyone know of a tool to use to analyze analog fax transmissions - T30.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analyze" rel="tag" title="see questions tagged &#39;analyze&#39;">analyze</span> <span class="post-tag tag-link-fax" rel="tag" title="see questions tagged &#39;fax&#39;">fax</span> <span class="post-tag tag-link-t.30" rel="tag" title="see questions tagged &#39;t.30&#39;">t.30</span> <span class="post-tag tag-link-t30" rel="tag" title="see questions tagged &#39;t30&#39;">t30</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jan '14, 08:57</strong></p><img src="https://secure.gravatar.com/avatar/21d862b1a2dcc43bf352c43a2eeeec25?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fax&#39;s gravatar image" /><p><span>fax</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fax has no accepted answers">0%</span></p></div></div><div id="comments-container-29354" class="comments-container"></div><div id="comment-tools-29354" class="comment-tools"></div><div class="clear"></div><div id="comment-29354-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29355"></span>

<div id="answer-container-29355" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29355-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29355-score" class="post-score" title="current number of votes">0</div><span id="post-29355-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well, this is really not related to Wireshark!</p><p>Why? Because you want to capture and analyze ISDN or analog signals, carrying the T.30 protocol.</p><p>So, you need a hardware T.30 analyzer.</p><p>How do you find one?</p><p>Answer (to your question): invest 15 seconds to google it: <a href="http://bit.ly/1ihrlGu">http://bit.ly/1ihrlGu</a></p><p>See also here: <a href="http://ask.wireshark.org/questions/22391/decode-packets-as-t30-messages-in-wireshark">http://ask.wireshark.org/questions/22391/decode-packets-as-t30-messages-in-wireshark</a></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Jan '14, 11:09</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-29355" class="comments-container"><span id="29356"></span><div id="comment-29356" class="comment"><div id="post-29356-score" class="comment-score"></div><div class="comment-text"><p>Kurt;</p><p>Thanks for the reply. tone not neccesary. I invested more than 3 hours searching and only found 1 real sotuion which is $7,500.00 just for the basics. Not knowing where Wireshark was going with R&amp;D and knowing that you could handle T.38 I decided to see if there was an option for this within WS.</p></div><div id="comment-29356-info" class="comment-info"><span class="comment-age">(31 Jan '14, 11:18)</span> <span class="comment-user userinfo">fax</span></div></div><span id="29357"></span><div id="comment-29357" class="comment"><div id="post-29357-score" class="comment-score"></div><div class="comment-text"><p>Never mind, just my kind of fun/humor ;-)) I hope my Google link was helpful.</p></div><div id="comment-29357-info" class="comment-info"><span class="comment-age">(31 Jan '14, 12:33)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="29359"></span><div id="comment-29359" class="comment"><div id="post-29359-score" class="comment-score"></div><div class="comment-text"><blockquote><p>where Wireshark was going with R&amp;D</p></blockquote><p>It's not going anywhere that involves developing hardware. :-) That's not what we do.</p><p>If somebody <em>else</em> develops a hardware device that can plug into a host and supply to software running on the host the "Binary coded signal functions and formats" described in section 5.3 of T.30 (well, I have some pre-publication version, so maybe the section number changed), then it might be possible to have Wireshark (or libpcap/WinPcap, with Wireshark using that) get those HDLC messages and analyze them. If a fax modem card can do that, great; if it takes a $7,500 hardware device, I guess with a suitably-modified version of Wireshark (and possibly libpcap) plus $7,500, you could do it.</p></div><div id="comment-29359-info" class="comment-info"><span class="comment-age">(31 Jan '14, 14:17)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-29355" class="comment-tools"></div><div class="clear"></div><div id="comment-29355-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

