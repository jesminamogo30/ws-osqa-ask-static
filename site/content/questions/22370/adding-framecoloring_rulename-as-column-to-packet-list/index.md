+++
type = "question"
title = "adding frame.coloring_rule.name as column to packet list"
description = '''I have a strange behaviour when adding the coloring rule name as a column in the packet list. The contents of the field is not always displayed initially. I need to reorder the columns a couple of times to get all frames&#x27; coloring rule name to display correctly.  Anybody else having the same problem...'''
date = "2013-06-26T11:41:00Z"
lastmod = "2013-06-26T21:41:00Z"
weight = 22370
keywords = [ "column", "coloring_rule.name" ]
aliases = [ "/questions/22370" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [adding frame.coloring\_rule.name as column to packet list](/questions/22370/adding-framecoloring_rulename-as-column-to-packet-list)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22370-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22370-score" class="post-score" title="current number of votes">0</div><span id="post-22370-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a strange behaviour when adding the coloring rule name as a column in the packet list.</p><p>The contents of the field is not always displayed initially. I need to reorder the columns a couple of times to get all frames' coloring rule name to display correctly. Anybody else having the same problem? Is this a known bug/issue?</p><p>I'm running 1.10 on Win7. Thanks <img src="https://osqa-ask.wireshark.org/upfiles/coloringrule.PNG" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-column" rel="tag" title="see questions tagged &#39;column&#39;">column</span> <span class="post-tag tag-link-coloring_rule.name" rel="tag" title="see questions tagged &#39;coloring_rule.name&#39;">coloring_rule.name</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '13, 11:41</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></img></div></div><div id="comments-container-22370" class="comments-container"></div><div id="comment-tools-22370" class="comment-tools"></div><div class="clear"></div><div id="comment-22370-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22372"></span>

<div id="answer-container-22372" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22372-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22372-score" class="post-score" title="current number of votes">2</div><span id="post-22372-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="mrEEde has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, it's a known issue, see <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5703">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5703</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jun '13, 12:15</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-22372" class="comments-container"><span id="22385"></span><div id="comment-22385" class="comment"><div id="post-22385-score" class="comment-score"></div><div class="comment-text"><p>Thank you for the link! Looks like the bug could do with some more voting ;-)</p></div><div id="comment-22385-info" class="comment-info"><span class="comment-age">(26 Jun '13, 21:41)</span> <span class="comment-user userinfo">mrEEde</span></div></div></div><div id="comment-tools-22372" class="comment-tools"></div><div class="clear"></div><div id="comment-22372-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

