+++
type = "question"
title = "WS version 2.2.1 - RTP streams packet loss value incorrect ?"
description = '''Hi,  When reviewing the voip-extension.pcapng trace from the Laura Chappell&#x27;s book, I notice a wrong value of packet loss in the RTP streams table. Is this a bug ? '''
date = "2016-10-12T05:36:00Z"
lastmod = "2016-10-12T23:49:00Z"
weight = 56315
keywords = [ "report", "bug" ]
aliases = [ "/questions/56315" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WS version 2.2.1 - RTP streams packet loss value incorrect ?](/questions/56315/ws-version-221-rtp-streams-packet-loss-value-incorrect)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56315-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56315-score" class="post-score" title="current number of votes">0</div><span id="post-56315-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, When reviewing the voip-extension.pcapng trace from the Laura Chappell's book, I notice a wrong value of packet loss in the RTP streams table. Is this a bug ? <img src="https://osqa-ask.wireshark.org/upfiles/2016-10-12_14-31-29.gif" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-report" rel="tag" title="see questions tagged &#39;report&#39;">report</span> <span class="post-tag tag-link-bug" rel="tag" title="see questions tagged &#39;bug&#39;">bug</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Oct '16, 05:36</strong></p><img src="https://secure.gravatar.com/avatar/eac75eef24254c1c9ee690951f6c4006?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thierryn&#39;s gravatar image" /><p><span>thierryn</span><br />
<span class="score" title="21 reputation points">21</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thierryn has no accepted answers">0%</span></p></img></div></div><div id="comments-container-56315" class="comments-container"><span id="56318"></span><div id="comment-56318" class="comment"><div id="post-56318-score" class="comment-score"></div><div class="comment-text"><p>If you can make it to <a href="https://sharkfesteurope.wireshark.org/">SharkFest Europe</a> we can have a look, with the author herself!</p><p>Else it's a good idea to <a href="https://bugs.wireshark.org/bugzilla/">file a bug report</a>.</p></div><div id="comment-56318-info" class="comment-info"><span class="comment-age">(12 Oct '16, 07:11)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="56319"></span><div id="comment-56319" class="comment"><div id="post-56319-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jaap,</p><p>I dit not know this bug report link. I'll attend Sharkfest next week :-)...</p><p>Thierry</p></div><div id="comment-56319-info" class="comment-info"><span class="comment-age">(12 Oct '16, 07:23)</span> <span class="comment-user userinfo">thierryn</span></div></div><span id="56320"></span><div id="comment-56320" class="comment"><div id="post-56320-score" class="comment-score"></div><div class="comment-text"><p>it seems this bug has already been reported and confirmed...</p></div><div id="comment-56320-info" class="comment-info"><span class="comment-age">(12 Oct '16, 07:28)</span> <span class="comment-user userinfo">thierryn</span></div></div><span id="56329"></span><div id="comment-56329" class="comment"><div id="post-56329-score" class="comment-score"></div><div class="comment-text"><p>Bug number?</p></div><div id="comment-56329-info" class="comment-info"><span class="comment-age">(12 Oct '16, 22:01)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="56332"></span><div id="comment-56332" class="comment"><div id="post-56332-score" class="comment-score"></div><div class="comment-text"><p>I think 10665</p></div><div id="comment-56332-info" class="comment-info"><span class="comment-age">(12 Oct '16, 23:49)</span> <span class="comment-user userinfo">thierryn</span></div></div></div><div id="comment-tools-56315" class="comment-tools"></div><div class="clear"></div><div id="comment-56315-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

