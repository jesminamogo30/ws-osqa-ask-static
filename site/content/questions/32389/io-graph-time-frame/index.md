+++
type = "question"
title = "IO graph time frame"
description = '''I have a capture that I&#x27;m attempting to narrow down to a specific time frame to graph. I already have the filter (see below) applied but when I apply the same filter in the IO graph, it&#x27;s not displaying any data. Essentially, I&#x27;m trying to narrow the time frame to set the tick interval down to .001 ...'''
date = "2014-05-02T04:53:00Z"
lastmod = "2014-05-02T04:53:00Z"
weight = 32389
keywords = [ "graph", "display-filter", "microburst", "time" ]
aliases = [ "/questions/32389" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [IO graph time frame](/questions/32389/io-graph-time-frame)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32389-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32389-score" class="post-score" title="current number of votes">0</div><span id="post-32389-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a capture that I'm attempting to narrow down to a specific time frame to graph. I already have the filter (see below) applied but when I apply the same filter in the IO graph, it's not displaying any data. Essentially, I'm trying to narrow the time frame to set the tick interval down to .001 second to try and find microbursts.</p><p><strong>(frame.time &gt;= "2014-05-01 09:56:32.841596") &amp;&amp; (frame.time &lt;= "May 1, 2014 10:06:59.370357000")</strong></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span> <span class="post-tag tag-link-microburst" rel="tag" title="see questions tagged &#39;microburst&#39;">microburst</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '14, 04:53</strong></p><img src="https://secure.gravatar.com/avatar/ff65c20787c07170ed0ced33dd0db3ed?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TheTreZuredOne&#39;s gravatar image" /><p><span>TheTreZuredOne</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TheTreZuredOne has no accepted answers">0%</span></p></div></div><div id="comments-container-32389" class="comments-container"></div><div id="comment-tools-32389" class="comment-tools"></div><div class="clear"></div><div id="comment-32389-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

