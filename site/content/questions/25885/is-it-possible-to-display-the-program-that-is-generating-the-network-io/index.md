+++
type = "question"
title = "Is it possible to display the program that is generating the network I/O?"
description = '''First of all, thank you. Wireshark is an incredible tool. I use it way too much. My wife thinks she is a computer widow...  One thing that would be a nice-to-have. For the packets originating on my computer, is there a way to determine which program (binary) is actually requesting the I/O, and displ...'''
date = "2013-10-10T08:10:00Z"
lastmod = "2013-10-10T14:29:00Z"
weight = 25885
keywords = [ "tcpview" ]
aliases = [ "/questions/25885" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to display the program that is generating the network I/O?](/questions/25885/is-it-possible-to-display-the-program-that-is-generating-the-network-io)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25885-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25885-score" class="post-score" title="current number of votes">0</div><span id="post-25885-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>First of all, thank you. Wireshark is an incredible tool. I use it way too much. My wife thinks she is a computer widow...<br />
</p><p>One thing that would be a nice-to-have. For the packets originating on my computer, is there a way to determine which program (binary) is actually requesting the I/O, and displaying that along with the normal Wireshark output? Kind of like Wireshark and TCPView combined...</p><p>Just an idea. It would be very help to know which program is generating the I/O, and the precise I/O that is being generated.</p><p>Once again, thanks for a fabulous program.</p><p>Jeff Kayser</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcpview" rel="tag" title="see questions tagged &#39;tcpview&#39;">tcpview</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Oct '13, 08:10</strong></p><img src="https://secure.gravatar.com/avatar/3c281b14d1ae47118a0cc2600eabe050?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffKayser&#39;s gravatar image" /><p><span>JeffKayser</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffKayser has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-25885" class="comments-container"></div><div id="comment-tools-25885" class="comment-tools"></div><div class="clear"></div><div id="comment-25885-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="25891"></span>

<div id="answer-container-25891" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25891-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25891-score" class="post-score" title="current number of votes">0</div><span id="post-25891-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not currently. It was one of the ideas for a <a href="http://wiki.wireshark.org/GSoC2013#Process_Information">Google Summer of Code project</a> but (AFAIK) the proposal never went anywhere.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '13, 11:25</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-25891" class="comments-container"></div><div id="comment-tools-25891" class="comment-tools"></div><div class="clear"></div><div id="comment-25891-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="25895"></span>

<div id="answer-container-25895" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25895-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25895-score" class="post-score" title="current number of votes">0</div><span id="post-25895-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See also the answers for a similar question.</p><blockquote><p><a href="http://ask.wireshark.org/questions/25838/captured-ip-address-but-which-application-using-it-in-pc">http://ask.wireshark.org/questions/25838/captured-ip-address-but-which-application-using-it-in-pc</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '13, 14:29</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-25895" class="comments-container"></div><div id="comment-tools-25895" class="comment-tools"></div><div class="clear"></div><div id="comment-25895-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

