+++
type = "question"
title = "Computer Network"
description = '''Suppose you visit a website using the browser. The IP address for the associated URL is not cached in your local host, so a DNS lookup is necessary to obtain the IP address. Suppose that your local DNS server is ip-srv1.vanderbilt.edu and the round-trip time (RTT) from your local DNS server is denot...'''
date = "2012-05-12T00:42:00Z"
lastmod = "2012-05-12T05:41:00Z"
weight = 10955
keywords = [ "computer", "dns" ]
aliases = [ "/questions/10955" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Computer Network](/questions/10955/computer-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10955-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10955-score" class="post-score" title="current number of votes">-1</div><span id="post-10955-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Suppose you visit a website using the browser. The IP address for the associated URL is not cached in your local host, so a DNS lookup is necessary to obtain the IP address. Suppose that your local DNS server is <a href="http://ip-srv1.vanderbilt.edu">ip-srv1.vanderbilt.edu</a> and the round-trip time (RTT) from your local DNS server is denoted as RTT_dns_local. The local DNS server performs iterative queries to a Root DNS server, a TLD DNS server and an Authorative DNS server. The round-trip time from the local DNS server to these DNS servers are denotd as RTT_dns_root, RTT_dns_tld, and RTT_dns_auth, respectively. Suppose that the round-trip time from your local host to this website is RTT_web. Further suppose that the web pass you visit is short, so that its transmission time of the page can be omitted. The web page references 8 small image objects hosted on the web site. Lets assume zero transmission time for the image objects as well. how much time elapse from when you click on the link until the web page is fully displayed (with all image objects) on your browser?</p><p>Consider the following scenarios and provide your solutions respectively.</p><ul><li>The browsing is using persistent HTTP. Please help me about this question.</li></ul></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-computer" rel="tag" title="see questions tagged &#39;computer&#39;">computer</span> <span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 May '12, 00:42</strong></p><img src="https://secure.gravatar.com/avatar/ad200a3734c66a6146cf36d0edaa1561?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sanjit&#39;s gravatar image" /><p><span>sanjit</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sanjit has no accepted answers">0%</span></p></div></div><div id="comments-container-10955" class="comments-container"><span id="10960"></span><div id="comment-10960" class="comment"><div id="post-10960-score" class="comment-score">1</div><div class="comment-text"><p>Another Homework assignment?!</p></div><div id="comment-10960-info" class="comment-info"><span class="comment-age">(12 May '12, 04:18)</span> <span class="comment-user userinfo">Landi</span></div></div><span id="10961"></span><div id="comment-10961" class="comment"><div id="post-10961-score" class="comment-score"></div><div class="comment-text"><p>The wording says it all...</p></div><div id="comment-10961-info" class="comment-info"><span class="comment-age">(12 May '12, 05:41)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-10955" class="comment-tools"></div><div class="clear"></div><div id="comment-10955-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10956"></span>

<div id="answer-container-10956" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10956-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10956-score" class="post-score" title="current number of votes">1</div><span id="post-10956-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The question cannot be answered as this is unknown:</p><ul><li>does the browser load the images in parallel or sequentially</li><li>how much CPU cycles does it take to render the images in the browser</li><li>how much CPU cycles are available for the browser, meaning how high is the load on the PC</li><li>how long is one CPU cycle</li></ul><p>BTW: don't you think your teacher/professor will also read this forum and detect your attempt to cheat? ;-))</p><p>EDIT: As the question is all about DNS, I recommend reading/watching this:</p><blockquote><p><code>http://www.howstuffworks.com/dns.htm</code><br />
<code>http://www.youtube.com/watch?v=XKoomsRMyCU</code></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 May '12, 01:00</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 May '12, 02:12</strong> </span></p></div></div><div id="comments-container-10956" class="comments-container"></div><div id="comment-tools-10956" class="comment-tools"></div><div class="clear"></div><div id="comment-10956-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

