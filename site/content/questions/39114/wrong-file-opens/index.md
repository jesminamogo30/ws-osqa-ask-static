+++
type = "question"
title = "Wrong file opens"
description = '''Hi, We are using Wireshark version 1.8.10 on a Linux machine (2.6.32-431.20.3.el6.x86_64). When using the file-&amp;gt;open dialog and selecting a trace file, Wireshark opens a completely different file? Has anyone seen anything like this before? Cheers, JT'''
date = "2015-01-14T00:02:00Z"
lastmod = "2015-01-15T00:47:00Z"
weight = 39114
keywords = [ "open", "file" ]
aliases = [ "/questions/39114" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wrong file opens](/questions/39114/wrong-file-opens)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39114-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39114-score" class="post-score" title="current number of votes">0</div><span id="post-39114-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>We are using Wireshark version 1.8.10 on a Linux machine (2.6.32-431.20.3.el6.x86_64). When using the file-&gt;open dialog and selecting a trace file, Wireshark opens a completely different file? Has anyone seen anything like this before?</p><p>Cheers, JT</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-open" rel="tag" title="see questions tagged &#39;open&#39;">open</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '15, 00:02</strong></p><img src="https://secure.gravatar.com/avatar/366f7e85b34a9120662fe75dbf63e84f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JTW&#39;s gravatar image" /><p><span>JTW</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JTW has no accepted answers">0%</span></p></div></div><div id="comments-container-39114" class="comments-container"></div><div id="comment-tools-39114" class="comment-tools"></div><div class="clear"></div><div id="comment-39114-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39144"></span>

<div id="answer-container-39144" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39144-score" class="post-score" title="current number of votes">0</div><span id="post-39144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark 1.8.10 is pretty old and there have been a lot of bug fixes since then. I'd suggest to use a more recent version of Wireshark (if possible on your system) or to run Wireshark with -r from the CLI.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jan '15, 00:47</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-39144" class="comments-container"></div><div id="comment-tools-39144" class="comment-tools"></div><div class="clear"></div><div id="comment-39144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

