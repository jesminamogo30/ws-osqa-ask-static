+++
type = "question"
title = "[closed] network issue about fsk ack"
description = '''No. ms Time Source Destination Protocol Length Frame is marked Info time delta 4872 0.298604 298.604 58.718061 10.211.135.98 172.19.98.20 TCP 216 FALSE sbl &amp;gt; 10820 [PSH, ACK] Seq=153133 Ack=173997 Win=64635 Len=162 4872 3751 0.253973 253.973 43.625935 10.211.135.98 172.19.98.20 TCP 188 FALSE sbl ...'''
date = "2011-12-04T23:30:00Z"
lastmod = "2011-12-06T03:52:00Z"
weight = 7763
keywords = [ "performance", "tcp" ]
aliases = [ "/questions/7763" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] network issue about fsk ack](/questions/7763/network-issue-about-fsk-ack)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7763-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7763-score" class="post-score" title="current number of votes">0</div><span id="post-7763-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><pre><code>No.  ms          Time                Source          Destination     Protocol   Length  Frame is marked Info    time delta
4872    0.298604    298.604 58.718061   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=153133 Ack=173997 Win=64635 Len=162  4872
3751    0.253973    253.973 43.625935   10.211.135.98   172.19.98.20    TCP 188 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=117741 Ack=133473 Win=65355 Len=134  3751
1619    0.253584    253.584 19.216507   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=49367 Ack=56011 Win=64275 Len=162    1619
5012    0.245084    245.084 60.667425   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=157043 Ack=178497 Win=64455 Len=162  5012
4045    0.244999    244.999 47.633918   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=125561 Ack=142473 Win=65175 Len=162  4045
3816    0.233447    233.447 44.6228 10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=118981 Ack=134913 Win=65355 Len=162  3816
3881    0.22657 226.57  45.627073   10.211.135.98   172.19.98.20    TCP 188 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=120897 Ack=137073 Win=64635 Len=134  3881
4352    0.222741    222.741 51.644643   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=135453 Ack=153633 Win=65355 Len=162  4352
4697    0.21845 218.45  56.638219   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=146003 Ack=165615 Win=65115 Len=162  4697
3956    0.212175    212.175 46.627235   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=122757 Ack=139233 Win=65355 Len=162  3956
603 0.212087    212.087 7.462898    10.211.135.98   172.19.98.10    TCP 210 FALSE   netarx &gt; 10810 [PSH, ACK] Seq=11145 Ack=11529 Win=65067 Len=156 603
5395    0.208185    208.185 64.673954   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=166985 Ack=190017 Win=64995 Len=162  5395
4449    0.206069    206.069 52.646409   10.211.135.98   172.19.98.20    TCP 188 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=138385 Ack=157053 Win=64815 Len=134  4449
4571    0.204386    204.386 54.652477   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=141583 Ack=160755 Win=65535 Len=162  4571
4927    0.20387 203.87  59.665059   10.211.135.98   172.19.98.20    TCP 188 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=154725 Ack=175797 Win=64275 Len=134  4927
4253    0.20293 202.93  50.63814    10.211.135.98   172.19.98.20    TCP 188 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=132001 Ack=150033 Win=65175 Len=134  4253
115 0.201825    201.825 1.263388    10.211.135.98   172.19.98.10    TCP 210 FALSE   netarx &gt; 10810 [PSH, ACK] Seq=1829 Ack=2029 Win=64755 Len=156   115
628 0.201617    201.617 7.854474    10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=21225 Ack=23863 Win=64635 Len=162    628
2048    0.198806    198.806 24.565566   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=60637 Ack=68713 Win=64815 Len=162    2048
4509    0.195633    195.633 53.649592   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=140083 Ack=159033 Win=64275 Len=162  4509
4122    0.194995    194.995 48.635535   10.211.135.98   172.19.98.20    TCP 188 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=128315 Ack=145533 Win=65115 Len=134  4122
4592    0.191885    191.885 55.177294   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=142365 Ack=161655 Win=64635 Len=162  4592
5044    0.188757    188.757 61.185544   10.211.135.98   172.19.98.10    TCP 210 FALSE   netarx &gt; 10810 [PSH, ACK] Seq=100717 Ack=101781 Win=64911 Len=156   5044
1538    0.183816    183.816 18.119034   10.211.135.98   172.19.98.10    TCP 210 FALSE   netarx &gt; 10810 [PSH, ACK] Seq=31337 Ack=31889 Win=64443 Len=156 1538
1971    0.183516    183.516 23.562454   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=58939 Ack=66733 Win=65355 Len=162    1971
4795    0.182948    182.948 57.620751   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=150229 Ack=170397 Win=65073 Len=162  4795
4723    0.181979    181.979 57.102364   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=147137 Ack=166875 Win=65355 Len=162  4723
3786    0.181946    181.946 44.15498    10.211.135.98   172.19.98.10    TCP 210 FALSE   netarx &gt; 10810 [PSH, ACK] Seq=76245 Ack=77193 Win=65535 Len=156 3786
3089    0.180513    180.513 36.149076   10.211.135.98   172.19.98.20    TCP 188 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=96061 Ack=107977 Win=65355 Len=134   3089
1868    0.17808 178.08  22.537495   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=55213 Ack=62593 Win=64995 Len=162    1868
2141    0.177606    177.606 25.567659   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=63089 Ack=71593 Win=64815 Len=162    2141
2391    0.177177    177.177 28.128197   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=70471 Ack=79975 Win=64995 Len=162    2391
4172    0.17704 177.04  49.635788   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=129555 Ack=146973 Win=65175 Len=162  4172
3602    0.17556 175.56  42.146046   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=111669 Ack=126633 Win=65253 Len=162  3602
2245    0.175144    175.144 26.547862   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=66435 Ack=75373 Win=65115 Len=162    2245
3418    0.17512 175.12  40.154368   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=106619 Ack=120849 Win=65175 Len=162  3418
2926    0.174591    174.591 34.138991   10.211.135.98   172.19.98.20    TCP 188 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=90855 Ack=102037 Win=65535 Len=134   2926
1252    0.168838    168.838 15.115938   10.211.135.98   172.19.98.10    TCP 210 FALSE   netarx &gt; 10810 [PSH, ACK] Seq=24785 Ack=25337 Win=65379 Len=156 1252
2750    0.16869 168.69  32.136587   10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=83579 Ack=94477 Win=64215 Len=162    2750
3650    0.166877    166.877 42.58566    10.211.135.98   172.19.98.20    TCP 216 FALSE   sbl &gt; 10820 [PSH, ACK] Seq=114099 Ack=129333 Win=64935 Len=162  3650
1692    0.166559    166.559 20.121693   10.211.135.98   172.19.98.10    TCP 210 FALSE   netarx &gt; 10810 [PSH, ACK] Seq=34925 Ack=35477 Win=65067 Len=156 1692</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-performance" rel="tag" title="see questions tagged &#39;performance&#39;">performance</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '11, 23:30</strong></p><img src="https://secure.gravatar.com/avatar/ad926725faf36607ffbd9349f38dabf7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="drawat&#39;s gravatar image" /><p><span>drawat</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="drawat has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Feb '12, 19:07</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-7763" class="comments-container"><span id="7764"></span><div id="comment-7764" class="comment"><div id="post-7764-score" class="comment-score"></div><div class="comment-text"><p>this is my trading TAP server, and i set it to nagle algorithm off , is it issue on my netowrk</p></div><div id="comment-7764-info" class="comment-info"><span class="comment-age">(04 Dec '11, 23:36)</span> <span class="comment-user userinfo">drawat</span></div></div></div><div id="comment-tools-7763" class="comment-tools"></div><div class="clear"></div><div id="comment-7763-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question not applicable to be handled through free online Q&A service." by Jaap 06 Dec '11, 04:47

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7766"></span>

<div id="answer-container-7766" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7766-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7766-score" class="post-score" title="current number of votes">0</div><span id="post-7766-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You are not really helping us to help you I'm afraid.</p><ul><li><p>What do you perceive as the issue? High delta times? Please explain what you find unusual and why you find it unusual, as each protocol can behave differently, please also tell what you would have expected.</p></li><li><p>Posting only the frames with high delta times is not useful as all the context of the frames is gone. Please post the whole capture on <a href="http://www.cloudshark.org">www.cloudshark.org</a></p></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Dec '11, 01:03</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-7766" class="comments-container"><span id="7790"></span><div id="comment-7790" class="comment"><div id="post-7790-score" class="comment-score"></div><div class="comment-text"><p>actually i am working for stock trading firm and my network TAP broadcast and interactive maximum data packet size is 525 byte, every machine has mtu 1500. As i set on TAP SERVER nagle algorithm off (i read it from website, if we do the nagle algorithm off on TAP OR PC , small packet dont wait for buffer full ack) my questing is, 1- is my network behave is good 2- is psk ack working good or not 3- i just want to do my packet dont wait for a while to communicate server 4-is my mtu is ok or not kindy help to fast communication between server and client without delay</p></div><div id="comment-7790-info" class="comment-info"><span class="comment-age">(06 Dec '11, 00:53)</span> <span class="comment-user userinfo">drawat</span></div></div><span id="7796"></span><div id="comment-7796" class="comment"><div id="post-7796-score" class="comment-score"></div><div class="comment-text"><p>Those questions are typically answered by a network consultant who will look thoroughly at your network design and specific traffic on your network.</p><p>The scope of this site is to help people use wireshark in better ways to solve their own issues. That is done by answering specific questions. You questions are a nit like going to a doctor and asking "Am I healthy?" without letting him do any physical tests.</p></div><div id="comment-7796-info" class="comment-info"><span class="comment-age">(06 Dec '11, 03:52)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-7766" class="comment-tools"></div><div class="clear"></div><div id="comment-7766-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

