+++
type = "question"
title = "How does airmon-ng work?"
description = '''I am just curious, what happens after sudo airmon-ng start wlan0 is typed?'''
date = "2014-05-02T08:35:00Z"
lastmod = "2014-05-02T08:47:00Z"
weight = 32427
keywords = [ "airmon" ]
aliases = [ "/questions/32427" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How does airmon-ng work?](/questions/32427/how-does-airmon-ng-work)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32427-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32427-score" class="post-score" title="current number of votes">0</div><span id="post-32427-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am just curious, what happens after <code>sudo airmon-ng start wlan0</code> is typed?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-airmon" rel="tag" title="see questions tagged &#39;airmon&#39;">airmon</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '14, 08:35</strong></p><img src="https://secure.gravatar.com/avatar/eeca75356089d0569c63dfc514d7f19d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tttttttttttt2&#39;s gravatar image" /><p><span>tttttttttttt2</span><br />
<span class="score" title="34 reputation points">34</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tttttttttttt2 has no accepted answers">0%</span></p></div></div><div id="comments-container-32427" class="comments-container"></div><div id="comment-tools-32427" class="comment-tools"></div><div class="clear"></div><div id="comment-32427-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32429"></span>

<div id="answer-container-32429" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32429-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32429-score" class="post-score" title="current number of votes">0</div><span id="post-32429-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="tttttttttttt2 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>While this isn't a Wireshark question it pretty much does the same as capturing with Wireshark I guess, except that airmon-ng will additionally activate monitor mode on the WiFi card.</p><p>Monitor mode is not the same as promiscuous mode (which Wireshark uses), and is required on WiFi cards to be able to record all frames and not only the ones for the PC the capture runs on.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 May '14, 08:47</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-32429" class="comments-container"></div><div id="comment-tools-32429" class="comment-tools"></div><div class="clear"></div><div id="comment-32429-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

