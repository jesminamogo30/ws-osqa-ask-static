+++
type = "question"
title = "Can anyone give me the functional syntax and its description to create a dissector?"
description = '''HI, Currently I put myself to write a dissector, for that first I have to understand the various &quot;functional syntax and its descriptions&quot; involved in dissector creation. Can any one give me the functional syntax for writing a dissector? I read through the &quot;http://www.wireshark.org/docs/wsdg_html_chu...'''
date = "2011-11-28T06:19:00Z"
lastmod = "2011-11-29T08:44:00Z"
weight = 7673
keywords = [ "development", "dissector", "plugin" ]
aliases = [ "/questions/7673" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can anyone give me the functional syntax and its description to create a dissector?](/questions/7673/can-anyone-give-me-the-functional-syntax-and-its-description-to-create-a-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7673-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7673-score" class="post-score" title="current number of votes">0</div><span id="post-7673-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI, Currently I put myself to write a dissector, for that first I have to understand the various "functional syntax and its descriptions" involved in dissector creation. Can any one give me the functional syntax for writing a dissector?</p><p>I read through the "<a href="http://www.wireshark.org/docs/wsdg_html_chunked/ChDissectAdd.html">http://www.wireshark.org/docs/wsdg_html_chunked/ChDissectAdd.html</a>" link, but I need the syntax and description.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Nov '11, 06:19</strong></p><img src="https://secure.gravatar.com/avatar/01febacc45af8ecf743c4f575d428326?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JK7&#39;s gravatar image" /><p><span>JK7</span><br />
<span class="score" title="31 reputation points">31</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JK7 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Nov '11, 10:33</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-7673" class="comments-container"><span id="7699"></span><div id="comment-7699" class="comment"><div id="post-7699-score" class="comment-score"></div><div class="comment-text"><p>Can you be more specific? Dissectors are written in C, so the "functional syntax" of a dissector is the same as the C programming language. The developer's guide chapter you link to provides a fairly thorough summary of how to create a dissector; if it is not enough information, you could also check <code>doc/README.developer</code> in the Wireshark sources.</p></div><div id="comment-7699-info" class="comment-info"><span class="comment-age">(29 Nov '11, 08:44)</span> <span class="comment-user userinfo">multipleinte...</span></div></div></div><div id="comment-tools-7673" class="comment-tools"></div><div class="clear"></div><div id="comment-7673-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

