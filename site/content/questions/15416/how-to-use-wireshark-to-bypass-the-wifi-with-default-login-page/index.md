+++
type = "question"
title = "how to use wireshark to bypass the wifi with default login page?"
description = '''i can connect to public wifi here with no password but theres a login page wherever i go to browse the net. u need an account but i have no account as it is expensive. how can i use wireshark to this?'''
date = "2012-10-31T03:37:00Z"
lastmod = "2012-10-31T05:26:00Z"
weight = 15416
keywords = [ "login", "page" ]
aliases = [ "/questions/15416" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [how to use wireshark to bypass the wifi with default login page?](/questions/15416/how-to-use-wireshark-to-bypass-the-wifi-with-default-login-page)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15416-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15416-score" class="post-score" title="current number of votes">-1</div><span id="post-15416-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i can connect to public wifi here with no password but theres a login page wherever i go to browse the net. u need an account but i have no account as it is expensive. how can i use wireshark to this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-login" rel="tag" title="see questions tagged &#39;login&#39;">login</span> <span class="post-tag tag-link-page" rel="tag" title="see questions tagged &#39;page&#39;">page</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '12, 03:37</strong></p><img src="https://secure.gravatar.com/avatar/3c3f59efa2ce21d8a432477c93c27ad8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="FierceX&#39;s gravatar image" /><p><span>FierceX</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="FierceX has no accepted answers">0%</span></p></div></div><div id="comments-container-15416" class="comments-container"><span id="15420"></span><div id="comment-15420" class="comment"><div id="post-15420-score" class="comment-score"></div><div class="comment-text"><p>are you really asking us to help you cheating your provider by telling you how to steal wifi accounts? I wonder if there is any country on earth where that is legal...</p></div><div id="comment-15420-info" class="comment-info"><span class="comment-age">(31 Oct '12, 05:26)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-15416" class="comment-tools"></div><div class="clear"></div><div id="comment-15416-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="15417"></span>

<div id="answer-container-15417" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15417-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15417-score" class="post-score" title="current number of votes">1</div><span id="post-15417-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is just a diagnostic tool.</p><p>Unless the Internet provider has botched configuration payment and authentication data will be encrypted in plain text.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Oct '12, 04:21</strong></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packethunter has 8 accepted answers">8%</span></p></div></div><div id="comments-container-15417" class="comments-container"></div><div id="comment-tools-15417" class="comment-tools"></div><div class="clear"></div><div id="comment-15417-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="15419"></span>

<div id="answer-container-15419" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15419-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15419-score" class="post-score" title="current number of votes">0</div><span id="post-15419-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is a network protocol analyser and cannot modify the packets sent by your system to steal wifi usage from a provider.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Oct '12, 04:27</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Oct '12, 06:49</strong> </span></p></div></div><div id="comments-container-15419" class="comments-container"></div><div id="comment-tools-15419" class="comment-tools"></div><div class="clear"></div><div id="comment-15419-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

