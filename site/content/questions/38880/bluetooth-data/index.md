+++
type = "question"
title = "bluetooth data"
description = '''is it possible to capture only the data that is being transferred using bluetooth? how can we identify the data that is being transferred via bluetooth connection?'''
date = "2015-01-04T10:06:00Z"
lastmod = "2015-01-05T08:31:00Z"
weight = 38880
keywords = [ "bluetooth" ]
aliases = [ "/questions/38880" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [bluetooth data](/questions/38880/bluetooth-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38880-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38880-score" class="post-score" title="current number of votes">0</div><span id="post-38880-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is it possible to capture only the data that is being transferred using bluetooth? how can we identify the data that is being transferred via bluetooth connection?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '15, 10:06</strong></p><img src="https://secure.gravatar.com/avatar/4c3a8cc615cdd2c1c6602f545c97a091?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wizard&#39;s gravatar image" /><p><span>wizard</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wizard has no accepted answers">0%</span></p></div></div><div id="comments-container-38880" class="comments-container"></div><div id="comment-tools-38880" class="comment-tools"></div><div class="clear"></div><div id="comment-38880-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38892"></span>

<div id="answer-container-38892" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38892-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38892-score" class="post-score" title="current number of votes">1</div><span id="post-38892-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's an article on the <a href="http://wiki.wireshark.org/CaptureSetup/Bluetooth">Wireshark Wiki</a> on how to capture Bluetooth traffic.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jan '15, 08:31</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-38892" class="comments-container"></div><div id="comment-tools-38892" class="comment-tools"></div><div class="clear"></div><div id="comment-38892-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

