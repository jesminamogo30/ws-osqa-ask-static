+++
type = "question"
title = "Media Delivery Index"
description = '''What is a good method for looking at UDP packets (video) to find out of order packets, burst, and/or MDI? I am looking for a stream starvation source.  Thanks'''
date = "2011-06-20T08:23:00Z"
lastmod = "2011-06-20T08:23:00Z"
weight = 4637
keywords = [ "video" ]
aliases = [ "/questions/4637" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Media Delivery Index](/questions/4637/media-delivery-index)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4637-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4637-score" class="post-score" title="current number of votes">0</div><span id="post-4637-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What is a good method for looking at UDP packets (video) to find out of order packets, burst, and/or MDI? I am looking for a stream starvation source.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-video" rel="tag" title="see questions tagged &#39;video&#39;">video</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jun '11, 08:23</strong></p><img src="https://secure.gravatar.com/avatar/39d3ba8f9ff4f18f52c346c88cb49b89?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tnjpatton&#39;s gravatar image" /><p><span>tnjpatton</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tnjpatton has no accepted answers">0%</span></p></div></div><div id="comments-container-4637" class="comments-container"></div><div id="comment-tools-4637" class="comment-tools"></div><div class="clear"></div><div id="comment-4637-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

