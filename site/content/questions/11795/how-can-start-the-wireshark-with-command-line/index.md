+++
type = "question"
title = "How can start the wireshark with command line"
description = '''i have XP i go to cmd &quot;C:&#92;Program Files&#92;Wireshark&#92;wireshark.exe&quot; thanks'''
date = "2012-06-10T01:52:00Z"
lastmod = "2012-06-10T03:40:00Z"
weight = 11795
keywords = [ "qa" ]
aliases = [ "/questions/11795" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can start the wireshark with command line](/questions/11795/how-can-start-the-wireshark-with-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11795-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11795-score" class="post-score" title="current number of votes">0</div><span id="post-11795-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i have XP i go to cmd "C:\Program Files\Wireshark\wireshark.exe"</p><p>thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-qa" rel="tag" title="see questions tagged &#39;qa&#39;">qa</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jun '12, 01:52</strong></p><img src="https://secure.gravatar.com/avatar/f370a012dbbdc3e4bd969497e9fcc1b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="artur1979&#39;s gravatar image" /><p><span>artur1979</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="artur1979 has no accepted answers">0%</span></p></div></div><div id="comments-container-11795" class="comments-container"><span id="11796"></span><div id="comment-11796" class="comment"><div id="post-11796-score" class="comment-score"></div><div class="comment-text"><p>This is not a question. What is it do you want to know?</p></div><div id="comment-11796-info" class="comment-info"><span class="comment-age">(10 Jun '12, 03:40)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-11795" class="comment-tools"></div><div class="clear"></div><div id="comment-11795-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

