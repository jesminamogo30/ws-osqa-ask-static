+++
type = "question"
title = "The time used to compile is so long after I edit the rlc code."
description = '''I&#x27;m a developer in the wireshark. I&#x27;m fixing the FP MAC RLC dissector bug in XP. The time used to compile is so long after I edit the rlc code. How to fixed it?'''
date = "2013-02-28T19:09:00Z"
lastmod = "2013-03-01T09:53:00Z"
weight = 19001
keywords = [ "compile", "rlc" ]
aliases = [ "/questions/19001" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [The time used to compile is so long after I edit the rlc code.](/questions/19001/the-time-used-to-compile-is-so-long-after-i-edit-the-rlc-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19001-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19001-score" class="post-score" title="current number of votes">0</div><span id="post-19001-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm a developer in the wireshark.</p><p>I'm fixing the FP MAC RLC dissector bug in XP.</p><p>The time used to compile is so long after I edit the rlc code.</p><p>How to fixed it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-compile" rel="tag" title="see questions tagged &#39;compile&#39;">compile</span> <span class="post-tag tag-link-rlc" rel="tag" title="see questions tagged &#39;rlc&#39;">rlc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Feb '13, 19:09</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-19001" class="comments-container"><span id="19041"></span><div id="comment-19041" class="comment"><div id="post-19041-score" class="comment-score"></div><div class="comment-text"><blockquote><p>The time used to compile is so long after I edit the rlc code.</p></blockquote><p>do you mind to share your knowledge with us and tell us <strong>what</strong> you have changed and <strong>which</strong> file takes so long to compile?</p><p>Otherwise we would have to throw some chicken bones to figure it out ;-))</p></div><div id="comment-19041-info" class="comment-info"><span class="comment-age">(01 Mar '13, 09:53)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-19001" class="comment-tools"></div><div class="clear"></div><div id="comment-19001-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

