+++
type = "question"
title = "MPLS EXP bits have been renamed &quot;Traffic Class&quot;"
description = '''Just to draw to decode developers attention, the MPLS &quot;Experimental&quot; bits have now formally been renamed since 2009 in RFC5462. This RFC formally calls these 3 bits the &quot;Traffic class&quot; bits.'''
date = "2011-06-24T04:25:00Z"
lastmod = "2011-06-24T06:54:00Z"
weight = 4723
keywords = [ "renamed", "mpls", "experimental", "rfc5462" ]
aliases = [ "/questions/4723" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [MPLS EXP bits have been renamed "Traffic Class"](/questions/4723/mpls-exp-bits-have-been-renamed-traffic-class)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4723-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4723-score" class="post-score" title="current number of votes">0</div><span id="post-4723-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Just to draw to decode developers attention, the MPLS "Experimental" bits have now formally been renamed since 2009 in RFC5462. This RFC formally calls these 3 bits the "Traffic class" bits.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-renamed" rel="tag" title="see questions tagged &#39;renamed&#39;">renamed</span> <span class="post-tag tag-link-mpls" rel="tag" title="see questions tagged &#39;mpls&#39;">mpls</span> <span class="post-tag tag-link-experimental" rel="tag" title="see questions tagged &#39;experimental&#39;">experimental</span> <span class="post-tag tag-link-rfc5462" rel="tag" title="see questions tagged &#39;rfc5462&#39;">rfc5462</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jun '11, 04:25</strong></p><img src="https://secure.gravatar.com/avatar/77cf155dbc856a4e2dbba0b1c3482a34?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="idtchris&#39;s gravatar image" /><p><span>idtchris</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="idtchris has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Feb '12, 22:05</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-4723" class="comments-container"></div><div id="comment-tools-4723" class="comment-tools"></div><div class="clear"></div><div id="comment-4723-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4728"></span>

<div id="answer-container-4728" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4728-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4728-score" class="post-score" title="current number of votes">0</div><span id="post-4728-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Thanks. Could you file a <a href="https://bugs.wireshark.org">bug report</a> on this, with a sample capture file to verify the implementation?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jun '11, 06:54</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-4728" class="comments-container"></div><div id="comment-tools-4728" class="comment-tools"></div><div class="clear"></div><div id="comment-4728-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

