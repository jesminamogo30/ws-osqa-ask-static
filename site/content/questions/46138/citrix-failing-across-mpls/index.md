+++
type = "question"
title = "Citrix Failing across MPLS"
description = '''Hi All, I have attached 2 captures from routers at each end of the conversation. Basically I have two devices. One is a Citrix Portal (10.44.137.31) and the other is a PC (172.18.53.51). Log into the Citrix portal via http, click an ICON which should load up the PC - it tries and then I get connecti...'''
date = "2015-09-25T01:26:00Z"
lastmod = "2015-09-25T08:00:00Z"
weight = 46138
keywords = [ "mpls", "citrix" ]
aliases = [ "/questions/46138" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Citrix Failing across MPLS](/questions/46138/citrix-failing-across-mpls)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46138-score" class="post-score" title="current number of votes">0</div><span id="post-46138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I have attached 2 captures from routers at each end of the conversation. Basically I have two devices. One is a Citrix Portal (10.44.137.31) and the other is a PC (172.18.53.51). Log into the Citrix portal via http, click an ICON which should load up the PC - it tries and then I get connection errors. This is across MPLS.</p><p>Is anything standing out from the captures at all? I see ECN flags, CWR flags but not to sure if this is relevant. Not to sure how to attach the actual capture files? Is this possible? I've attached screenshots for now.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Wire-ServerSide.JPG" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/Wire-ClientSide.JPG" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mpls" rel="tag" title="see questions tagged &#39;mpls&#39;">mpls</span> <span class="post-tag tag-link-citrix" rel="tag" title="see questions tagged &#39;citrix&#39;">citrix</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Sep '15, 01:26</strong></p><img src="https://secure.gravatar.com/avatar/a28bceae0effe0ec1130bab7cb87a4e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="exit12&#39;s gravatar image" /><p><span>exit12</span><br />
<span class="score" title="11 reputation points">11</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="exit12 has no accepted answers">0%</span></p></img></div></div><div id="comments-container-46138" class="comments-container"><span id="46161"></span><div id="comment-46161" class="comment"><div id="post-46161-score" class="comment-score"></div><div class="comment-text"><p>can you upload captures,would be easier to analyse.</p></div><div id="comment-46161-info" class="comment-info"><span class="comment-age">(25 Sep '15, 05:36)</span> <span class="comment-user userinfo">kishan pandey</span></div></div><span id="46169"></span><div id="comment-46169" class="comment"><div id="post-46169-score" class="comment-score"></div><div class="comment-text"><p>Hi There,</p><p>I can't see where I can upload actual capture files.</p><p>Where is the option? Thanks</p></div><div id="comment-46169-info" class="comment-info"><span class="comment-age">(25 Sep '15, 07:11)</span> <span class="comment-user userinfo">exit12</span></div></div><span id="46172"></span><div id="comment-46172" class="comment"><div id="post-46172-score" class="comment-score"></div><div class="comment-text"><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>, Google Drive or DropBox?</p></div><div id="comment-46172-info" class="comment-info"><span class="comment-age">(25 Sep '15, 08:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-46138" class="comment-tools"></div><div class="clear"></div><div id="comment-46138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

