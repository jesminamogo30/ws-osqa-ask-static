+++
type = "question"
title = "Like as langrabber"
description = '''Hello!! I like the software lan grabber. The software captures all packages and analyzes all content. The software knows all data files and saves based on filters. You can save all you want, jpeg, flv, wmv, html, css and a lot of other supported files. But the software is older and have a lot of bug...'''
date = "2013-02-04T18:53:00Z"
lastmod = "2013-02-04T22:22:00Z"
weight = 18299
keywords = [ "lan", "langrabber", "grabber" ]
aliases = [ "/questions/18299" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Like as langrabber](/questions/18299/like-as-langrabber)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18299-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18299-score" class="post-score" title="current number of votes">0</div><span id="post-18299-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello!!</p><p>I like the software <a href="http://www.skygrabber.com/en/langrabber.php">lan grabber</a>. The software captures all packages and analyzes all content. The software knows all data files and saves based on filters.</p><p>You can save all you want, jpeg, flv, wmv, html, css and a lot of other supported files.</p><p>But the software is older and have a lot of bugs running in Windows 8. With Wireshark, is it possible to do the same?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lan" rel="tag" title="see questions tagged &#39;lan&#39;">lan</span> <span class="post-tag tag-link-langrabber" rel="tag" title="see questions tagged &#39;langrabber&#39;">langrabber</span> <span class="post-tag tag-link-grabber" rel="tag" title="see questions tagged &#39;grabber&#39;">grabber</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Feb '13, 18:53</strong></p><img src="https://secure.gravatar.com/avatar/50929948178c332396d813262478af60?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LKS45&#39;s gravatar image" /><p><span>LKS45</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LKS45 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Feb '13, 19:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-18299" class="comments-container"></div><div id="comment-tools-18299" class="comment-tools"></div><div class="clear"></div><div id="comment-18299-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18305"></span>

<div id="answer-container-18305" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18305-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18305-score" class="post-score" title="current number of votes">0</div><span id="post-18305-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark can do some analysis, and as a result save certain content, but it's not intended to, and has limited support. You should look into other tools, specifically made for the purpose, such as <a href="http://tcpxtract.sourceforge.net/">tcpxtract</a> and <a href="http://chaosreader.sourceforge.net/">chaosreader</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Feb '13, 22:22</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-18305" class="comments-container"></div><div id="comment-tools-18305" class="comment-tools"></div><div class="clear"></div><div id="comment-18305-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

