+++
type = "question"
title = "Detected RTP with G.711 PCMU, but all 100% packet loss or wrong sequence nr. How to decode it ?"
description = '''Hello This captured file (Please check packet in this link https://www.cloudshark.org/captures/0074bdf3ac05 ) has lot of RTP packets. But when analyzed, The session seems all broken with wrong sequence number and packet loss 100%. Any idea how to solve it ?? Thanks. Any suggestion please.'''
date = "2015-05-12T06:15:00Z"
lastmod = "2015-05-13T07:50:00Z"
weight = 42326
keywords = [ "rtp", "voip" ]
aliases = [ "/questions/42326" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Detected RTP with G.711 PCMU, but all 100% packet loss or wrong sequence nr. How to decode it ?](/questions/42326/detected-rtp-with-g711-pcmu-but-all-100-packet-loss-or-wrong-sequence-nr-how-to-decode-it)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42326-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42326-score" class="post-score" title="current number of votes">0</div><span id="post-42326-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello This captured file (Please check packet in this link <a href="https://www.cloudshark.org/captures/0074bdf3ac05">https://www.cloudshark.org/captures/0074bdf3ac05</a> ) has lot of RTP packets. But when analyzed, The session seems all broken with wrong sequence number and packet loss 100%. Any idea how to solve it ?? Thanks. Any suggestion please.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 May '15, 06:15</strong></p><img src="https://secure.gravatar.com/avatar/4ec917e3556fb6d9c03cc0e39ec7732a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shas&#39;s gravatar image" /><p><span>Shas</span><br />
<span class="score" title="1 reputation points">1</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shas has no accepted answers">0%</span></p></div></div><div id="comments-container-42326" class="comments-container"><span id="42360"></span><div id="comment-42360" class="comment"><div id="post-42360-score" class="comment-score"></div><div class="comment-text"><p>I'm not sure what codec this is, or how the RTP headers are composed. There's also STUN like traffic here. Maybe if you could explain the context of this capture things would make more sense.</p></div><div id="comment-42360-info" class="comment-info"><span class="comment-age">(13 May '15, 07:09)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="42368"></span><div id="comment-42368" class="comment"><div id="post-42368-score" class="comment-score"></div><div class="comment-text"><p><span>@Jaap</span>, Thanks for the comment.After hours hours of struggle, I, now realised that it should be due to some kind of security measures from the voip app provider. could you check this post please ?? <a href="https://ask.wireshark.org/questions/42367/understanding-sipsdp-file-in-context-of-opus-codec-cannot-decode-opus">https://ask.wireshark.org/questions/42367/understanding-sipsdp-file-in-context-of-opus-codec-cannot-decode-opus</a></p></div><div id="comment-42368-info" class="comment-info"><span class="comment-age">(13 May '15, 07:50)</span> <span class="comment-user userinfo">Shas</span></div></div></div><div id="comment-tools-42326" class="comment-tools"></div><div class="clear"></div><div id="comment-42326-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

