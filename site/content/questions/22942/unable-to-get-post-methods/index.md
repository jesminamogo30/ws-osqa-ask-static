+++
type = "question"
title = "Unable to get post methods"
description = '''When I start capturing network Im not able to get POST methods except mine. However its able to track other active IP on the Network. And when I use the program from other computer, same thing happens.'''
date = "2013-07-14T00:44:00Z"
lastmod = "2013-07-14T04:02:00Z"
weight = 22942
keywords = [ "post", "get" ]
aliases = [ "/questions/22942" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to get post methods](/questions/22942/unable-to-get-post-methods)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22942-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22942-score" class="post-score" title="current number of votes">0</div><span id="post-22942-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I start capturing network Im not able to get POST methods except mine. However its able to track other active IP on the Network. And when I use the program from other computer, same thing happens.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-post" rel="tag" title="see questions tagged &#39;post&#39;">post</span> <span class="post-tag tag-link-get" rel="tag" title="see questions tagged &#39;get&#39;">get</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jul '13, 00:44</strong></p><img src="https://secure.gravatar.com/avatar/e8e1b5e09c2c82cc9287704750ad9938?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rishabh%20Goyal&#39;s gravatar image" /><p><span>Rishabh Goyal</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rishabh Goyal has no accepted answers">0%</span></p></div></div><div id="comments-container-22942" class="comments-container"><span id="22944"></span><div id="comment-22944" class="comment"><div id="post-22944-score" class="comment-score"></div><div class="comment-text"><p>Where are you putting your sniffer(capture)? What type of sniffing option are you using: arp cache poisoning, tap, port mirroring or what?</p></div><div id="comment-22944-info" class="comment-info"><span class="comment-age">(14 Jul '13, 03:25)</span> <span class="comment-user userinfo">Edmond</span></div></div></div><div id="comment-tools-22942" class="comment-tools"></div><div class="clear"></div><div id="comment-22942-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22948"></span>

<div id="answer-container-22948" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22948-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22948-score" class="post-score" title="current number of votes">0</div><span id="post-22948-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Long story short (<strong>simplified</strong>): In a regular network (switched or Wifi/wlan) you will only see traffic to/from your own system and broadcast/multicast traffic.</p><p>Please read the Wiki article about <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">Ethernet Capture Setup</a> (or <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">Wlan Capture Setup</a>). That will explain why you see only your own traffic, unless you prepare the environment for proper network traffic capturing.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jul '13, 04:02</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Jul '13, 04:04</strong> </span></p></div></div><div id="comments-container-22948" class="comments-container"></div><div id="comment-tools-22948" class="comment-tools"></div><div class="clear"></div><div id="comment-22948-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

