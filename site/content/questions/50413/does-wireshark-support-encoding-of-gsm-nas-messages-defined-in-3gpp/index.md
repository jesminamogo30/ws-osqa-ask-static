+++
type = "question"
title = "Does wireshark support encoding of GSM / NAS messages defined in 3GPP"
description = '''Hi, I am new to wireshark. As I understand it supports decoding of almost all 3GPP protocols. But does it support encoding also? Thanks, Sub_k23'''
date = "2016-02-22T10:44:00Z"
lastmod = "2016-02-22T14:28:00Z"
weight = 50413
keywords = [ "nas", "gsm", "encoder" ]
aliases = [ "/questions/50413" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Does wireshark support encoding of GSM / NAS messages defined in 3GPP](/questions/50413/does-wireshark-support-encoding-of-gsm-nas-messages-defined-in-3gpp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50413-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50413-score" class="post-score" title="current number of votes">0</div><span id="post-50413-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am new to wireshark.</p><p>As I understand it supports decoding of almost all 3GPP protocols. But does it support encoding also?</p><p>Thanks, Sub_k23</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nas" rel="tag" title="see questions tagged &#39;nas&#39;">nas</span> <span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span> <span class="post-tag tag-link-encoder" rel="tag" title="see questions tagged &#39;encoder&#39;">encoder</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '16, 10:44</strong></p><img src="https://secure.gravatar.com/avatar/69f8125456c09eebfa867570e5b2f2d6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sub_k23&#39;s gravatar image" /><p><span>sub_k23</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sub_k23 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Feb '16, 10:45</strong> </span></p></div></div><div id="comments-container-50413" class="comments-container"><span id="50418"></span><div id="comment-50418" class="comment"><div id="post-50418-score" class="comment-score"></div><div class="comment-text"><p>What specifically is your goal?</p></div><div id="comment-50418-info" class="comment-info"><span class="comment-age">(22 Feb '16, 14:28)</span> <span class="comment-user userinfo">Quadratic</span></div></div></div><div id="comment-tools-50413" class="comment-tools"></div><div class="clear"></div><div id="comment-50413-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50414"></span>

<div id="answer-container-50414" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50414-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50414-score" class="post-score" title="current number of votes">0</div><span id="post-50414-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No. Wireshark is only a monitoring and analysing tool, not an encoder.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Feb '16, 10:50</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Feb '16, 10:50</strong> </span></p></div></div><div id="comments-container-50414" class="comments-container"></div><div id="comment-tools-50414" class="comment-tools"></div><div class="clear"></div><div id="comment-50414-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

