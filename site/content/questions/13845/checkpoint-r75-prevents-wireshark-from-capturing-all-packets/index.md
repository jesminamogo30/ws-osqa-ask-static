+++
type = "question"
title = "Checkpoint R75 prevents Wireshark from capturing ALL packets"
description = '''Hi, This isn&#x27;t a question, its an answer to my own question. I had problem capturing packets onsite with Windows 7 64bit, however other PCs from colleagues (XP 32 bit, 7 32bit and 7 64bit) worked fine. I tried everything:  - Reinstalling Wireshark (with the NPF set to start on boot)  - Trying MS Net...'''
date = "2012-08-23T06:13:00Z"
lastmod = "2012-08-27T04:27:00Z"
weight = 13845
keywords = [ "windows", "checkpoint", "promiscuous", "7" ]
aliases = [ "/questions/13845" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Checkpoint R75 prevents Wireshark from capturing ALL packets](/questions/13845/checkpoint-r75-prevents-wireshark-from-capturing-all-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13845-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13845-score" class="post-score" title="current number of votes">0</div><span id="post-13845-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>This isn't a question, its an answer to my own question. I had problem capturing packets onsite with Windows 7 64bit, however other PCs from colleagues (XP 32 bit, 7 32bit and 7 64bit) worked fine. I tried everything: - Reinstalling Wireshark (with the NPF set to start on boot) - Trying MS Network Monitor instead - Checking McAffe didn't have any firewall functions - Installing latest Intel network drivers - Checking intel supports promisbious mode - Checking Windows 7 supports promiscious mode</p><p>The issue turned out to be Checkpoint Endpoint Security R75 (the VPN client), once uninstalled (and rebooted) it worked first time.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-checkpoint" rel="tag" title="see questions tagged &#39;checkpoint&#39;">checkpoint</span> <span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-7" rel="tag" title="see questions tagged &#39;7&#39;">7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '12, 06:13</strong></p><img src="https://secure.gravatar.com/avatar/f397f08f9b07a84e99a97041586ca533?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="piethief&#39;s gravatar image" /><p><span>piethief</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="piethief has no accepted answers">0%</span></p></div></div><div id="comments-container-13845" class="comments-container"></div><div id="comment-tools-13845" class="comment-tools"></div><div class="clear"></div><div id="comment-13845-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13911"></span>

<div id="answer-container-13911" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13911-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13911-score" class="post-score" title="current number of votes">0</div><span id="post-13911-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>... like <a href="http://wiki.wireshark.org/CaptureSetup/InterferingSoftware">many others</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Aug '12, 04:27</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-13911" class="comments-container"></div><div id="comment-tools-13911" class="comment-tools"></div><div class="clear"></div><div id="comment-13911-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

