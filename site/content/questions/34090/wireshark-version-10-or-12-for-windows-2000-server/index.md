+++
type = "question"
title = "Wireshark version 1.0 or 1.2 for Windows 2000 server"
description = '''Where can I find WireShark version 1.0 or 1.2 for an older Windows 2000 server? Thanks.'''
date = "2014-06-23T11:45:00Z"
lastmod = "2014-06-23T11:49:00Z"
weight = 34090
keywords = [ "of", "version", "wirehshark", "older" ]
aliases = [ "/questions/34090" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark version 1.0 or 1.2 for Windows 2000 server](/questions/34090/wireshark-version-10-or-12-for-windows-2000-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34090-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34090-score" class="post-score" title="current number of votes">0</div><span id="post-34090-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Where can I find WireShark version 1.0 or 1.2 for an older Windows 2000 server? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-of" rel="tag" title="see questions tagged &#39;of&#39;">of</span> <span class="post-tag tag-link-version" rel="tag" title="see questions tagged &#39;version&#39;">version</span> <span class="post-tag tag-link-wirehshark" rel="tag" title="see questions tagged &#39;wirehshark&#39;">wirehshark</span> <span class="post-tag tag-link-older" rel="tag" title="see questions tagged &#39;older&#39;">older</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jun '14, 11:45</strong></p><img src="https://secure.gravatar.com/avatar/e970be066144fd0a71ce5180546d3aef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wireshark100&#39;s gravatar image" /><p><span>wireshark100</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wireshark100 has no accepted answers">0%</span></p></div></div><div id="comments-container-34090" class="comments-container"></div><div id="comment-tools-34090" class="comment-tools"></div><div class="clear"></div><div id="comment-34090-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34091"></span>

<div id="answer-container-34091" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34091-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34091-score" class="post-score" title="current number of votes">0</div><span id="post-34091-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can find a list of download mirrors under "Go Spelunking" section at <a href="http://www.wireshark.org/download.html">http://www.wireshark.org/download.html</a>. Click on your nearest mirror and navigate to win32/all-versions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jun '14, 11:49</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-34091" class="comments-container"></div><div id="comment-tools-34091" class="comment-tools"></div><div class="clear"></div><div id="comment-34091-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

