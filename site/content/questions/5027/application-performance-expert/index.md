+++
type = "question"
title = "Application performance expert"
description = '''Whare can I find Application performance experts? Our company needs experts that can help us identify application performance issues and guide us with the creation of new applications that will scale over a large WAN infrastructure. I know Shaunra offers such services... any other good references ?'''
date = "2011-07-13T08:46:00Z"
lastmod = "2011-07-14T04:43:00Z"
weight = 5027
keywords = [ "application", "expert", "performance" ]
aliases = [ "/questions/5027" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Application performance expert](/questions/5027/application-performance-expert)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5027-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5027-score" class="post-score" title="current number of votes">0</div><span id="post-5027-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Whare can I find Application performance experts? Our company needs experts that can help us identify application performance issues and guide us with the creation of new applications that will scale over a large WAN infrastructure. I know Shaunra offers such services... any other good references ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-application" rel="tag" title="see questions tagged &#39;application&#39;">application</span> <span class="post-tag tag-link-expert" rel="tag" title="see questions tagged &#39;expert&#39;">expert</span> <span class="post-tag tag-link-performance" rel="tag" title="see questions tagged &#39;performance&#39;">performance</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jul '11, 08:46</strong></p><img src="https://secure.gravatar.com/avatar/0918771ad16362fe29f809dfd3f958c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="setamp&#39;s gravatar image" /><p><span>setamp</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="setamp has no accepted answers">0%</span></p></div></div><div id="comments-container-5027" class="comments-container"></div><div id="comment-tools-5027" class="comment-tools"></div><div class="clear"></div><div id="comment-5027-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5043"></span>

<div id="answer-container-5043" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5043-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5043-score" class="post-score" title="current number of votes">0</div><span id="post-5043-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>OpNet offers an onsite engineering service, and their products are pretty good at homing in on performance issues. Plus their ACE product uses WireShark as the decoder engine.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jul '11, 04:43</strong></p><img src="https://secure.gravatar.com/avatar/9e493496d59bb4ce33c37cd6e7a26a4d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GeonJay&#39;s gravatar image" /><p><span>GeonJay</span><br />
<span class="score" title="470 reputation points">470</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="bronze">●</span><span class="badgecount">22</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GeonJay has 2 accepted answers">5%</span></p></div></div><div id="comments-container-5043" class="comments-container"></div><div id="comment-tools-5043" class="comment-tools"></div><div class="clear"></div><div id="comment-5043-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

