+++
type = "question"
title = "Unable to open two captures simultaneously for review on MAC OS X"
description = '''I have checked the various settings / preferences and am unable to determine how to view multiple files at same time on Wireshark 1.11.3. I was able to do this on previous versions. This issue is limited to my MAC OS X 10.9.1 Mavericks. TYIA'''
date = "2014-02-24T08:38:00Z"
lastmod = "2014-02-24T10:02:00Z"
weight = 30133
keywords = [ "osx" ]
aliases = [ "/questions/30133" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to open two captures simultaneously for review on MAC OS X](/questions/30133/unable-to-open-two-captures-simultaneously-for-review-on-mac-os-x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30133-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30133-score" class="post-score" title="current number of votes">0</div><span id="post-30133-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have checked the various settings / preferences and am unable to determine how to view multiple files at same time on Wireshark 1.11.3. I was able to do this on previous versions. This issue is limited to my MAC OS X 10.9.1 Mavericks.</p><p>TYIA</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Feb '14, 08:38</strong></p><img src="https://secure.gravatar.com/avatar/f0544d103e1ba2171828adf2d8cc551b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tysonjordanali&#39;s gravatar image" /><p><span>tysonjordanali</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tysonjordanali has no accepted answers">0%</span></p></div></div><div id="comments-container-30133" class="comments-container"><span id="30137"></span><div id="comment-30137" class="comment"><div id="post-30137-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by "view multiple files at the same time"? Do you mean open two instances of Wireshark? Or do you mean open a file in a file set (i.e., following the Filename_00001_20050604101530.pcap pattern) and see the files in the File-&gt;File set-&gt;List Files dialog box?</p><p>I can do both of those things with 1.11.3 on Mavericks, though opening two instance of Wireshark requires opening the second one from the command line instead of clicking the Applications-&gt;Wireshark app icon (but that's always been true as far as I know).</p></div><div id="comment-30137-info" class="comment-info"><span class="comment-age">(24 Feb '14, 10:02)</span> <span class="comment-user userinfo">Hadriel</span></div></div></div><div id="comment-tools-30133" class="comment-tools"></div><div class="clear"></div><div id="comment-30133-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

