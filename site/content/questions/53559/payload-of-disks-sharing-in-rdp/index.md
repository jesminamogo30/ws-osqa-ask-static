+++
type = "question"
title = "Payload of DISKS sharing in RDP?"
description = '''It is possible to allow in RDP sharing of disks. My question is what it the payload of this action? How to I measure this? To be exact, i&#x27;m talking only about the option being enabled in the client and server, no actual file transfer is in progress. http://ultraimg.com/images/mstsc_2016-06-19_11-51-...'''
date = "2016-06-19T01:51:00Z"
lastmod = "2016-06-19T01:51:00Z"
weight = 53559
keywords = [ "rdp" ]
aliases = [ "/questions/53559" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Payload of DISKS sharing in RDP?](/questions/53559/payload-of-disks-sharing-in-rdp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53559-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53559-score" class="post-score" title="current number of votes">0</div><span id="post-53559-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>It is possible to allow in RDP sharing of disks. My question is what it the payload of this action? How to I measure this?</p><p>To be exact, i'm talking only about the option being enabled in the client and server, no actual file transfer is in progress.</p><p><a href="http://ultraimg.com/images/mstsc_2016-06-19_11-51-21.png">http://ultraimg.com/images/mstsc_2016-06-19_11-51-21.png</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rdp" rel="tag" title="see questions tagged &#39;rdp&#39;">rdp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jun '16, 01:51</strong></p><img src="https://secure.gravatar.com/avatar/0fe5d3f427d0d4d50ba3925e3ab6cadd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="worselatus&#39;s gravatar image" /><p><span>worselatus</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="worselatus has no accepted answers">0%</span></p></div></div><div id="comments-container-53559" class="comments-container"></div><div id="comment-tools-53559" class="comment-tools"></div><div class="clear"></div><div id="comment-53559-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

