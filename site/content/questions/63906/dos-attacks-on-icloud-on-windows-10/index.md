+++
type = "question"
title = "DOS attacks on iCloud on Windows 10"
description = '''I have notice that after installing iCloud on Windows 10, I seem to be getting almost a denial-of-service attack from their servers with these incorrect frame check sequence packets hitting my machine at high frequency. I first noticed it because I was having trouble with streaming media hanging and...'''
date = "2017-10-14T20:31:00Z"
lastmod = "2017-10-14T20:31:00Z"
weight = 63906
keywords = [ "windows", "dos", "icloud" ]
aliases = [ "/questions/63906" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [DOS attacks on iCloud on Windows 10](/questions/63906/dos-attacks-on-icloud-on-windows-10)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63906-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63906-score" class="post-score" title="current number of votes">0</div><span id="post-63906-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have notice that after installing iCloud on Windows 10, I seem to be getting almost a denial-of-service attack from their servers with these incorrect frame check sequence packets hitting my machine at high frequency.</p><p>I first noticed it because I was having trouble with streaming media hanging and pausing while the buffers were struggling to fill themselves up with data.</p><p>As soon as I kill the iCloud drive client, the 'attack' (so to speak) goes away.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-dos" rel="tag" title="see questions tagged &#39;dos&#39;">dos</span> <span class="post-tag tag-link-icloud" rel="tag" title="see questions tagged &#39;icloud&#39;">icloud</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Oct '17, 20:31</strong></p><img src="https://secure.gravatar.com/avatar/3ed8c428a22d0b49bc0dfffc23e4b440?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JBaker5&#39;s gravatar image" /><p><span>JBaker5</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JBaker5 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>14 Oct '17, 22:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-63906" class="comments-container"></div><div id="comment-tools-63906" class="comment-tools"></div><div class="clear"></div><div id="comment-63906-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

