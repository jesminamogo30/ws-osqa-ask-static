+++
type = "question"
title = "help router traffic"
description = '''why doesnt my wireshark show all router traffic? It only shows the traffic to my specific device. '''
date = "2017-01-06T01:42:00Z"
lastmod = "2017-01-06T16:14:00Z"
weight = 58555
keywords = [ "router", "traffic", "wireshark" ]
aliases = [ "/questions/58555" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [help router traffic](/questions/58555/help-router-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58555-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58555-score" class="post-score" title="current number of votes">0</div><span id="post-58555-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>why doesnt my wireshark show all router traffic? It only shows the traffic to my specific device.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jan '17, 01:42</strong></p><img src="https://secure.gravatar.com/avatar/a3d2237388c875a243a326eae9942fb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AnonStrous&#39;s gravatar image" /><p><span>AnonStrous</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AnonStrous has no accepted answers">0%</span></p></div></div><div id="comments-container-58555" class="comments-container"></div><div id="comment-tools-58555" class="comment-tools"></div><div class="clear"></div><div id="comment-58555-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58556"></span>

<div id="answer-container-58556" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58556-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58556-score" class="post-score" title="current number of votes">1</div><span id="post-58556-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please have a look here:</p><p><a href="https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-1-ethernet-basics/">https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-1-ethernet-basics/</a></p><p>And here:</p><p><a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">https://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jan '17, 02:01</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-58556" class="comments-container"><span id="58557"></span><div id="comment-58557" class="comment"><div id="post-58557-score" class="comment-score"></div><div class="comment-text"><p>Ok, I read it and still have no idea how to capture traffic from the other devices on my network lol. I'm new to all this so a lot of what I read was hieroglyphics pretty much.</p></div><div id="comment-58557-info" class="comment-info"><span class="comment-age">(06 Jan '17, 03:00)</span> <span class="comment-user userinfo">AnonStrous</span></div></div><span id="58559"></span><div id="comment-58559" class="comment"><div id="post-58559-score" class="comment-score"></div><div class="comment-text"><p>Well I am sorry, but I can't explain it easier as the guys have done it in there articles.</p></div><div id="comment-58559-info" class="comment-info"><span class="comment-age">(06 Jan '17, 03:14)</span> <span class="comment-user userinfo">Christian_R</span></div></div><span id="58571"></span><div id="comment-58571" class="comment"><div id="post-58571-score" class="comment-score"></div><div class="comment-text"><p>Please take this in a positive way. Since you are "new to all of this", it may be beneficial to learn basic networking fundamentals and to understand the filtering, forwarding, and flooding properties of L2 network switches to fully grasp why you are not able to plug into a switch port and not see "All" of your router's traffic.</p><p>This page may shed some light on things for you... <a href="http://vishalp-network.blogspot.com/2010/05/switches.html">http://vishalp-network.blogspot.com/2010/05/switches.html</a></p></div><div id="comment-58571-info" class="comment-info"><span class="comment-age">(06 Jan '17, 16:14)</span> <span class="comment-user userinfo">Rooster_50</span></div></div></div><div id="comment-tools-58556" class="comment-tools"></div><div class="clear"></div><div id="comment-58556-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

