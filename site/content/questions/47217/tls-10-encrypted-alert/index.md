+++
type = "question"
title = "TLS 1.0 encrypted alert"
description = '''I have two way ssl communication between client(garbled-ip77.66.0.240, .net app) host(147.0.0.10, proxy GW). Can somebody help me out how I can troubleshoot?  anon.pcap'''
date = "2015-11-04T01:48:00Z"
lastmod = "2015-11-17T21:55:00Z"
weight = 47217
keywords = [ "tlsv1", "encrypt", "pcap", "error" ]
aliases = [ "/questions/47217" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [TLS 1.0 encrypted alert](/questions/47217/tls-10-encrypted-alert)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47217-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47217-score" class="post-score" title="current number of votes">0</div><span id="post-47217-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have two way ssl communication between client(garbled-ip77.66.0.240, .net app) host(147.0.0.10, proxy GW). Can somebody help me out how I can troubleshoot?</p><p><img src="https://drive.google.com/open?id=0B-wmB5QnnUr-TXBaNFJKZFlkWms" alt="Wireshark Screenshot" /></p><p><a href="https://drive.google.com/open?id=0B-wmB5QnnUr-UV9lcGdjRmdHNDg">anon.pcap</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tlsv1" rel="tag" title="see questions tagged &#39;tlsv1&#39;">tlsv1</span> <span class="post-tag tag-link-encrypt" rel="tag" title="see questions tagged &#39;encrypt&#39;">encrypt</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Nov '15, 01:48</strong></p><img src="https://secure.gravatar.com/avatar/7a74bcce2d8fd530cb92ff086805a521?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rhansen&#39;s gravatar image" /><p><span>rhansen</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rhansen has one accepted answer">100%</span></p></img></div></div><div id="comments-container-47217" class="comments-container"></div><div id="comment-tools-47217" class="comment-tools"></div><div class="clear"></div><div id="comment-47217-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47696"></span>

<div id="answer-container-47696" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47696-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47696-score" class="post-score" title="current number of votes">0</div><span id="post-47696-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="rhansen has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well this turned out to be the client (3rd. party) doing something wrong. It helped them to debug thier code for why it spend 10 sec</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Nov '15, 21:55</strong></p><img src="https://secure.gravatar.com/avatar/7a74bcce2d8fd530cb92ff086805a521?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rhansen&#39;s gravatar image" /><p><span>rhansen</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rhansen has one accepted answer">100%</span></p></div></div><div id="comments-container-47696" class="comments-container"></div><div id="comment-tools-47696" class="comment-tools"></div><div class="clear"></div><div id="comment-47696-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

