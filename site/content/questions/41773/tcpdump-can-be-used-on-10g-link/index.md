+++
type = "question"
title = "TCPDUMP can be used on 10G link?"
description = '''Hi, I would just like to understand that TCPDUMP can capture full packets on 10G line rate? In the case of tshark, it is only able to capture packets uptp 100MB/S speed. TCPDUMP has also this kind of limits or it can capture full packets at line rate? Thanks,'''
date = "2015-04-24T04:18:00Z"
lastmod = "2015-04-24T05:23:00Z"
weight = 41773
keywords = [ "capture", "tcpdump", "speed" ]
aliases = [ "/questions/41773" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [TCPDUMP can be used on 10G link?](/questions/41773/tcpdump-can-be-used-on-10g-link)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41773-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41773-score" class="post-score" title="current number of votes">0</div><span id="post-41773-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I would just like to understand that TCPDUMP can capture full packets on 10G line rate? In the case of tshark, it is only able to capture packets uptp 100MB/S speed. TCPDUMP has also this kind of limits or it can capture full packets at line rate?</p><p>Thanks,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-tcpdump" rel="tag" title="see questions tagged &#39;tcpdump&#39;">tcpdump</span> <span class="post-tag tag-link-speed" rel="tag" title="see questions tagged &#39;speed&#39;">speed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '15, 04:18</strong></p><img src="https://secure.gravatar.com/avatar/904e19785874be39705426e578c4c029?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aditi&#39;s gravatar image" /><p><span>Aditi</span><br />
<span class="score" title="16 reputation points">16</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aditi has no accepted answers">0%</span></p></div></div><div id="comments-container-41773" class="comments-container"></div><div id="comment-tools-41773" class="comment-tools"></div><div class="clear"></div><div id="comment-41773-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41774"></span>

<div id="answer-container-41774" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41774-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41774-score" class="post-score" title="current number of votes">1</div><span id="post-41774-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This performance question has nothing to do with the program you run, but everything with the platform you run it on. Stick tcpdump on a 6 MHz 80286 and you won't handle 100 Mb/s as well. Stick tshark on a screaming fast processor, with ultra high speed peripherals and unlimited memory it can easily tackle the problem.</p><p>So the question is what are your requirements? Pure capture and storage? or wire speed analysis as well? And if so, what needs to be analyzed? What program will give you the features you need? Can tcpdump do the job? Go for it. Need more detailed analysis? Maybe something else then. Need statistical analysis? Then you may need something else.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Apr '15, 05:23</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-41774" class="comments-container"></div><div id="comment-tools-41774" class="comment-tools"></div><div class="clear"></div><div id="comment-41774-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

