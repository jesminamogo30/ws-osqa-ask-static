+++
type = "question"
title = "Wireshark crash when do some menu operation on Wireshark  using Pipe file to feed data"
description = '''I use wireshark pipe mode to launch wireshark, &quot;wireshark -k -i pipeFileName&quot;. when do some menu operation, wireshark crashed.  Edit --&amp;gt; Configuration Profiles (Do some change and Click OK or Apply) Edit --&amp;gt; Preferences (Do some change and Click OK or Apply) Tools --&amp;gt; Firewall ACL rules (Sa...'''
date = "2011-01-19T21:17:00Z"
lastmod = "2011-01-19T21:17:00Z"
weight = 1825
keywords = [ "pipe", "crash" ]
aliases = [ "/questions/1825" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark crash when do some menu operation on Wireshark using Pipe file to feed data](/questions/1825/wireshark-crash-when-do-some-menu-operation-on-wireshark-using-pipe-file-to-feed-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1825-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1825-score" class="post-score" title="current number of votes">0</div><span id="post-1825-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use wireshark pipe mode to launch wireshark, "wireshark -k -i pipeFileName". when do some menu operation, wireshark crashed. Edit --&gt; Configuration Profiles (Do some change and Click OK or Apply) Edit --&gt; Preferences (Do some change and Click OK or Apply) Tools --&gt; Firewall ACL rules (Save rules) Any help is appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pipe" rel="tag" title="see questions tagged &#39;pipe&#39;">pipe</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '11, 21:17</strong></p><img src="https://secure.gravatar.com/avatar/9f94dd6c84b70b9abb80a22546d09710?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="brenthuang&#39;s gravatar image" /><p><span>brenthuang</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="brenthuang has no accepted answers">0%</span></p></div></div><div id="comments-container-1825" class="comments-container"></div><div id="comment-tools-1825" class="comment-tools"></div><div class="clear"></div><div id="comment-1825-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

