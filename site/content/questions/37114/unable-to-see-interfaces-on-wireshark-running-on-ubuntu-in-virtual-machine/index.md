+++
type = "question"
title = "Unable to see interfaces on wireshark running on Ubuntu in virtual machine"
description = '''Hi, I am trying to capture the traffic using wireshark on Ubuntu OS which is running in virtual machine. However I&#x27;m not able to see any local interfaces in wireshark. In VM I am able to see one wired connection. I have bridged eth interface of host machine with virtual machine, so if i can see one ...'''
date = "2014-10-16T10:07:00Z"
lastmod = "2014-10-16T11:13:00Z"
weight = 37114
keywords = [ "wireshark" ]
aliases = [ "/questions/37114" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to see interfaces on wireshark running on Ubuntu in virtual machine](/questions/37114/unable-to-see-interfaces-on-wireshark-running-on-ubuntu-in-virtual-machine)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37114-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37114-score" class="post-score" title="current number of votes">0</div><span id="post-37114-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to capture the traffic using wireshark on Ubuntu OS which is running in virtual machine. However I'm not able to see any local interfaces in wireshark. In VM I am able to see one wired connection. I have bridged eth interface of host machine with virtual machine, so if i can see one eth interface on VM. Also, I am able to access internet.</p><p>But still this eth interface is not visible in wireshark's interface list.</p><p>Could you please help me out.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Oct '14, 10:07</strong></p><img src="https://secure.gravatar.com/avatar/f3e09017420ed9d6dff1c1d94e37469d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vikrant07&#39;s gravatar image" /><p><span>vikrant07</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vikrant07 has no accepted answers">0%</span></p></div></div><div id="comments-container-37114" class="comments-container"></div><div id="comment-tools-37114" class="comment-tools"></div><div class="clear"></div><div id="comment-37114-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37115"></span>

<div id="answer-container-37115" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37115-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37115-score" class="post-score" title="current number of votes">0</div><span id="post-37115-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://wiki.wireshark.org/CaptureSetup/CapturePrivileges">http://wiki.wireshark.org/CaptureSetup/CapturePrivileges</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Oct '14, 11:13</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-37115" class="comments-container"></div><div id="comment-tools-37115" class="comment-tools"></div><div class="clear"></div><div id="comment-37115-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

