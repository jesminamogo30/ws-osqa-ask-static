+++
type = "question"
title = "Record incoming/outgoing traffic on a PC on a network"
description = '''Its been quite awhile since I&#x27;ve used a packet sniffer. Can Wireshark or any other tool record traffic speeds for different websites on the same PC at the same time? With Time/Date stamps?'''
date = "2014-12-10T12:43:00Z"
lastmod = "2014-12-10T17:29:00Z"
weight = 38515
keywords = [ "traffic-analysis", "connection.speed" ]
aliases = [ "/questions/38515" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Record incoming/outgoing traffic on a PC on a network](/questions/38515/record-incomingoutgoing-traffic-on-a-pc-on-a-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38515-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38515-score" class="post-score" title="current number of votes">0</div><span id="post-38515-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Its been quite awhile since I've used a packet sniffer. Can Wireshark or any other tool record traffic speeds for different websites on the same PC at the same time? With Time/Date stamps?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic-analysis" rel="tag" title="see questions tagged &#39;traffic-analysis&#39;">traffic-analysis</span> <span class="post-tag tag-link-connection.speed" rel="tag" title="see questions tagged &#39;connection.speed&#39;">connection.speed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Dec '14, 12:43</strong></p><img src="https://secure.gravatar.com/avatar/97fe80d9d0d6caa4a83feacfd33a3b39?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KenJebsen&#39;s gravatar image" /><p><span>KenJebsen</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KenJebsen has no accepted answers">0%</span></p></div></div><div id="comments-container-38515" class="comments-container"></div><div id="comment-tools-38515" class="comment-tools"></div><div class="clear"></div><div id="comment-38515-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38517"></span>

<div id="answer-container-38517" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38517-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38517-score" class="post-score" title="current number of votes">0</div><span id="post-38517-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes. It's possible. run several Wireshark apps or something like that such as Omnipeek and use filter(s) that makes you doing capture traffic what you want to get. It is important your PC performance when you do capturing much traffic at the same time.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Dec '14, 17:29</strong></p><img src="https://secure.gravatar.com/avatar/d1b99abccfc95e814611bf145088ffe0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sarancha&#39;s gravatar image" /><p><span>sarancha</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sarancha has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Dec '14, 17:32</strong> </span></p></div></div><div id="comments-container-38517" class="comments-container"></div><div id="comment-tools-38517" class="comment-tools"></div><div class="clear"></div><div id="comment-38517-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

