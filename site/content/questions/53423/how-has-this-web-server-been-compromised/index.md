+++
type = "question"
title = "How has this web server been compromised?"
description = '''I am trying to figure out how the server has been compromised. I been looking at this for a good two hours and I cannot figure it out.  here is the capture. https://filetea.me/t1s4L5WXp89TdmH7ztELgiAnQ here is another link to the file https://file.io/84OHis'''
date = "2016-06-13T21:31:00Z"
lastmod = "2016-06-13T21:31:00Z"
weight = 53423
keywords = [ "compromise", "homework", "server" ]
aliases = [ "/questions/53423" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How has this web server been compromised?](/questions/53423/how-has-this-web-server-been-compromised)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53423-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53423-score" class="post-score" title="current number of votes">0</div><span id="post-53423-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to figure out how the server has been compromised. I been looking at this for a good two hours and I cannot figure it out.</p><p>here is the capture.</p><p><a href="https://filetea.me/t1s4L5WXp89TdmH7ztELgiAnQ">https://filetea.me/t1s4L5WXp89TdmH7ztELgiAnQ</a></p><p>here is another link to the file</p><p><a href="https://file.io/84OHis">https://file.io/84OHis</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-compromise" rel="tag" title="see questions tagged &#39;compromise&#39;">compromise</span> <span class="post-tag tag-link-homework" rel="tag" title="see questions tagged &#39;homework&#39;">homework</span> <span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '16, 21:31</strong></p><img src="https://secure.gravatar.com/avatar/ac5e219068ca87abc15164f475cd7be6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Benq357&#39;s gravatar image" /><p><span>Benq357</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Benq357 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Jun '16, 22:19</strong> </span></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span></p></div></div><div id="comments-container-53423" class="comments-container"></div><div id="comment-tools-53423" class="comment-tools"></div><div class="clear"></div><div id="comment-53423-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

