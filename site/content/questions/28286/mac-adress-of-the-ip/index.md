+++
type = "question"
title = "Mac adress of the IP"
description = '''Hi, I have this captured packet file and I need to find the mac adress of 192.168.1.13 I find it difficult. Can anyone help please?'''
date = "2013-12-19T04:23:00Z"
lastmod = "2013-12-19T05:24:00Z"
weight = 28286
keywords = [ "wireshark" ]
aliases = [ "/questions/28286" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Mac adress of the IP](/questions/28286/mac-adress-of-the-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28286-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28286-score" class="post-score" title="current number of votes">0</div><span id="post-28286-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have this captured packet file and I need to find the mac adress of 192.168.1.13 I find it difficult. Can anyone help please?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Dec '13, 04:23</strong></p><img src="https://secure.gravatar.com/avatar/b14aca76389ce1d6229a9e30f4f59829?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="saanwer&#39;s gravatar image" /><p><span>saanwer</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="saanwer has no accepted answers">0%</span></p></div></div><div id="comments-container-28286" class="comments-container"></div><div id="comment-tools-28286" class="comment-tools"></div><div class="clear"></div><div id="comment-28286-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28288"></span>

<div id="answer-container-28288" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28288-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28288-score" class="post-score" title="current number of votes">1</div><span id="post-28288-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>maybe <a href="http://ask.wireshark.org/questions/15824/display-mac-address-in-the-packet-list">this</a> can help you</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Dec '13, 05:24</strong></p><img src="https://secure.gravatar.com/avatar/29c0629c406e2304ba6371aefcede460?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="straw&#39;s gravatar image" /><p><span>straw</span><br />
<span class="score" title="31 reputation points">31</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="straw has no accepted answers">0%</span></p></div></div><div id="comments-container-28288" class="comments-container"></div><div id="comment-tools-28288" class="comment-tools"></div><div class="clear"></div><div id="comment-28288-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

