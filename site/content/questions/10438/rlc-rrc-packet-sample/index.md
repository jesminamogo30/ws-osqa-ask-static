+++
type = "question"
title = "RLC &amp; RRC packet sample"
description = '''how do i get sample capture of RLC and RRC packet in 3g (UTMS) technology? is any body upload it?'''
date = "2012-04-25T07:28:00Z"
lastmod = "2012-04-25T07:28:00Z"
weight = 10438
keywords = [ "rrc", "sample_packet", "utms", "3g", "rlc" ]
aliases = [ "/questions/10438" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [RLC & RRC packet sample](/questions/10438/rlc-rrc-packet-sample)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10438-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10438-score" class="post-score" title="current number of votes">0</div><span id="post-10438-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how do i get sample capture of RLC and RRC packet in 3g (UTMS) technology? is any body upload it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rrc" rel="tag" title="see questions tagged &#39;rrc&#39;">rrc</span> <span class="post-tag tag-link-sample_packet" rel="tag" title="see questions tagged &#39;sample_packet&#39;">sample_packet</span> <span class="post-tag tag-link-utms" rel="tag" title="see questions tagged &#39;utms&#39;">utms</span> <span class="post-tag tag-link-3g" rel="tag" title="see questions tagged &#39;3g&#39;">3g</span> <span class="post-tag tag-link-rlc" rel="tag" title="see questions tagged &#39;rlc&#39;">rlc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '12, 07:28</strong></p><img src="https://secure.gravatar.com/avatar/93a4c56b6418d23362d6e787c2f9b1e0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="souhal67&#39;s gravatar image" /><p><span>souhal67</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="souhal67 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Apr '12, 08:20</strong> </span></p></div></div><div id="comments-container-10438" class="comments-container"></div><div id="comment-tools-10438" class="comment-tools"></div><div class="clear"></div><div id="comment-10438-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

