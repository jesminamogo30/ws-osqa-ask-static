+++
type = "question"
title = "[closed] Facebook cookie"
description = '''Hi I want a working way to steal facebook cookies and use it to hack facebook account  thank you '''
date = "2013-11-19T12:54:00Z"
lastmod = "2013-11-19T16:37:00Z"
weight = 27112
keywords = [ "facebook" ]
aliases = [ "/questions/27112" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Facebook cookie](/questions/27112/facebook-cookie)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27112-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27112-score" class="post-score" title="current number of votes">0</div><span id="post-27112-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I want a working way to steal facebook cookies and use it to hack facebook account</p><p>thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-facebook" rel="tag" title="see questions tagged &#39;facebook&#39;">facebook</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Nov '13, 12:54</strong></p><img src="https://secure.gravatar.com/avatar/4149adc173c55eca0158e659cf6f0862?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mahmoud%20Qeshreh&#39;s gravatar image" /><p><span>Mahmoud Qeshreh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mahmoud Qeshreh has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>19 Nov '13, 13:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-27112" class="comments-container"><span id="27120"></span><div id="comment-27120" class="comment"><div id="post-27120-score" class="comment-score">1</div><div class="comment-text"><blockquote><p>and use it to <strong>hack facebook account</strong></p></blockquote><p>you won't be able to do that!</p><p>Reason: You have to ask how to get a cookie out of a HTTP(S) stream. I wonder <strong>how you are going to hack an account</strong> at that knowledge level !?!</p><p>I recommend to read (<strong>at least</strong>) the following books</p><ul><li><a href="http://www.amazon.com/TCP-Illustrated-Vol-Addison-Wesley-Professional/dp/0201633469/ref=sr_1_4?s=books&amp;ie=UTF8&amp;qid=1384902631&amp;sr=1-4">TCP/IP Illustrated, Vol. 1:</a></li><li><a href="http://www.amazon.com/TCP-IP-Illustrated-Implementation-Vol/dp/020163354X/ref=sr_1_5?s=books&amp;ie=UTF8&amp;qid=1384902631&amp;sr=1-5">TCP/IP Illustrated: The Implementation, Vol. 2</a></li><li><a href="http://www.amazon.com/TCP-Illustrated-Vol-Transactions-Protocols/dp/0201634953/ref=sr_1_6?s=books&amp;ie=UTF8&amp;qid=1384902631&amp;sr=1-6">TCP/IP Illustrated, Vol. 3: TCP for Transactions, HTTP, NNTP, and the UNIX Domain Protocols</a></li></ul><p>then something about web application design. Just random picks at amazon, NO recommendation!</p><ul><li><a href="http://www.amazon.com/Building-Scalable-Web-Sites-Applications/dp/0596102356/ref=sr_1_13?s=books&amp;ie=UTF8&amp;qid=1384902735&amp;sr=1-13&amp;keywords=web+application+design">Building Scalable Web Sites:</a></li><li><a href="http://www.amazon.com/Getting-Real-Smarter-Successful-Application/dp/0578012812/ref=sr_1_22?s=books&amp;ie=UTF8&amp;qid=1384902760&amp;sr=1-22&amp;keywords=web+application+design">Getting Real: The Smarter, Faster, Easier Way to Build a Successful Web Application</a></li><li><a href="http://www.amazon.com/Express-Application-Development-Hage-Yaapa/dp/1849696543/ref=sr_1_27?s=books&amp;ie=UTF8&amp;qid=1384902760&amp;sr=1-27&amp;keywords=web+application+design">Express Web Application Development</a></li></ul><p>and FINALLY something about web application 'hacking'. Just random picks at amazon, NO recommendation!</p><ul><li><a href="http://www.amazon.com/Web-Application-Hackers-Handbook-Discovering/dp/0470170778/ref=sr_1_20?s=books&amp;ie=UTF8&amp;qid=1384902760&amp;sr=1-20&amp;keywords=web+application+design">The Web Application Hacker's Handbook: Discovering and Exploiting Security Flaws</a></li></ul><p>ONLY <strong>after</strong> you have studied all that you may come back and ask further questions! Good luck!</p></div><div id="comment-27120-info" class="comment-info"><span class="comment-age">(19 Nov '13, 15:19)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-27112" class="comment-tools"></div><div class="clear"></div><div id="comment-27112-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jasper 19 Nov '13, 13:03

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27113"></span>

<div id="answer-container-27113" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27113-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27113-score" class="post-score" title="current number of votes">3</div><span id="post-27113-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Congratulations. You've managed to ask a question nobody will help you with.</p><p><strong>Edit:</strong> well, Kurt does. But he's nice and helps everyone :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Nov '13, 13:02</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Nov '13, 15:31</strong> </span></p></div></div><div id="comments-container-27113" class="comments-container"><span id="27123"></span><div id="comment-27123" class="comment"><div id="post-27123-score" class="comment-score"></div><div class="comment-text"><p>Yep. I can't help myself, but that's how I am ;-)))</p></div><div id="comment-27123-info" class="comment-info"><span class="comment-age">(19 Nov '13, 15:32)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="27125"></span><div id="comment-27125" class="comment"><div id="post-27125-score" class="comment-score"></div><div class="comment-text"><p>Furthermore my comment was meant to be slightly ironic, as I 'struggled' to implement a proxy policy that allows only a subset of Facebook and we were capturing and analyzing that damn Facebook code for the last two days to implement it ;-))</p></div><div id="comment-27125-info" class="comment-info"><span class="comment-age">(19 Nov '13, 15:53)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="27127"></span><div id="comment-27127" class="comment"><div id="post-27127-score" class="comment-score"></div><div class="comment-text"><p>It was just alright. Some people need a high five. In the face. With a TCP Illustrated book. :-)</p></div><div id="comment-27127-info" class="comment-info"><span class="comment-age">(19 Nov '13, 16:37)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-27113" class="comment-tools"></div><div class="clear"></div><div id="comment-27113-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

