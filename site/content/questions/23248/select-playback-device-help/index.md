+++
type = "question"
title = "Select playback device - help?"
description = '''Hey folks. I have absolutely zero experience with this stuff, and I can&#x27;t manage to find any tutorials. My end goal is replay a single captured packet I saved with wireshark (managed that just fine, saved as .pcap). I&#x27;m trying to do this with playcap. I open the file and it prompts me to select a pl...'''
date = "2013-07-22T12:14:00Z"
lastmod = "2013-07-22T14:30:00Z"
weight = 23248
keywords = [ "playcap", "playback", "wireshark" ]
aliases = [ "/questions/23248" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Select playback device - help?](/questions/23248/select-playback-device-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23248-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23248-score" class="post-score" title="current number of votes">0</div><span id="post-23248-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey folks. I have absolutely zero experience with this stuff, and I can't manage to find any tutorials.</p><p>My end goal is replay a single captured packet I saved with wireshark (managed that just fine, saved as .pcap). I'm trying to do this with playcap. I open the file and it prompts me to select a playback device. Is this "playback device" some sort of hardware, or a program?</p><p>I apologize if this is a really stupid question, but the only devices listed are Windows and Hamachi for me; I've searched hard and found a few screenshots of other people at this dialog, and it shows various capture devices and such, and I'm just really not sure what I need to replay this packet.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-playcap" rel="tag" title="see questions tagged &#39;playcap&#39;">playcap</span> <span class="post-tag tag-link-playback" rel="tag" title="see questions tagged &#39;playback&#39;">playback</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jul '13, 12:14</strong></p><img src="https://secure.gravatar.com/avatar/3f9d5f02fe141edb5e1bffbbb77a419a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="justanewbie&#39;s gravatar image" /><p><span>justanewbie</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="justanewbie has no accepted answers">0%</span></p></div></div><div id="comments-container-23248" class="comments-container"></div><div id="comment-tools-23248" class="comment-tools"></div><div class="clear"></div><div id="comment-23248-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23252"></span>

<div id="answer-container-23252" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23252-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23252-score" class="post-score" title="current number of votes">1</div><span id="post-23252-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I open the file and it prompts me to select a playback device. Is this "playback device" some sort of hardware, or a program?</p></blockquote><p>The <strong>playback device</strong> is the network interface where you want the tool to send that packet out (playback).</p><blockquote><p><a href="http://www.lovemytool.com/blog/2009/12/playcap_playback_for_wireshark_capture_files_by_joke_snelders.html">http://www.lovemytool.com/blog/2009/12/playcap_playback_for_wireshark_capture_files_by_joke_snelders.html</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jul '13, 14:30</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Jul '13, 14:30</strong> </span></p></div></div><div id="comments-container-23252" class="comments-container"></div><div id="comment-tools-23252" class="comment-tools"></div><div class="clear"></div><div id="comment-23252-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

