+++
type = "question"
title = "I cant remember the password for my wireshark"
description = '''My customer do not remember the password for wireshark access, does any one knows how to reset it?'''
date = "2012-03-09T17:34:00Z"
lastmod = "2012-03-09T18:44:00Z"
weight = 9463
keywords = [ "password" ]
aliases = [ "/questions/9463" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [I cant remember the password for my wireshark](/questions/9463/i-cant-remember-the-password-for-my-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9463-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9463-score" class="post-score" title="current number of votes">0</div><span id="post-9463-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My customer do not remember the password for wireshark access, does any one knows how to reset it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Mar '12, 17:34</strong></p><img src="https://secure.gravatar.com/avatar/7a45379d503d4ac586957edde6d869a0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fmoralesgt&#39;s gravatar image" /><p><span>fmoralesgt</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fmoralesgt has no accepted answers">0%</span></p></div></div><div id="comments-container-9463" class="comments-container"></div><div id="comment-tools-9463" class="comment-tools"></div><div class="clear"></div><div id="comment-9463-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9464"></span>

<div id="answer-container-9464" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9464-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9464-score" class="post-score" title="current number of votes">4</div><span id="post-9464-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark, as shipped by the Wireshark developers, does not require a password to access it.</p><p>Either somebody provided your customer with a version of Wireshark modified to require a password, somebody configured their system to require a password to run Wireshark, or the password is actually a password to get sufficient privileges to do traffic capture with Wireshark. We can't help you with the first two of those, as we had nothing to do with that; if it's a password to get sufficient privileges, it's probably the root password for the machine, if it's a UN\<em>X of some sort (Mac OS X, Linux, Solaris, \</em>BSD, etc.), or an administrator password, if it's Windows.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Mar '12, 18:44</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-9464" class="comments-container"></div><div id="comment-tools-9464" class="comment-tools"></div><div class="clear"></div><div id="comment-9464-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

