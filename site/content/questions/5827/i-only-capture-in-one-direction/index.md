+++
type = "question"
title = "I only capture in one direction"
description = '''Hi; I configure a switch Catalyst in monitor mode to capture some packets, with my laptop I only capture packets in one direction; I use another laptop with same Wireshark version and I capture traffic in both directions, I can check this with a Ping. In my laptop I just see ICMP (Echo) Requests w/o...'''
date = "2011-08-23T12:50:00Z"
lastmod = "2011-08-23T17:54:00Z"
weight = 5827
keywords = [ "capture" ]
aliases = [ "/questions/5827" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [I only capture in one direction](/questions/5827/i-only-capture-in-one-direction)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5827-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5827-score" class="post-score" title="current number of votes">0</div><span id="post-5827-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi; I configure a switch Catalyst in monitor mode to capture some packets, with my laptop I only capture packets in one direction; I use another laptop with same Wireshark version and I capture traffic in both directions, I can check this with a Ping. In my laptop I just see ICMP (Echo) Requests w/o any filter but when I use the another one I see Echo Requests and Replies.</p><p>So I don't know what else I must change in the original laptop to receive and capture traffic in both directions.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '11, 12:50</strong></p><img src="https://secure.gravatar.com/avatar/2412e1d7f9df1329df800e753b34433a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joseavanegas&#39;s gravatar image" /><p><span>joseavanegas</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joseavanegas has no accepted answers">0%</span></p></div></div><div id="comments-container-5827" class="comments-container"><span id="5831"></span><div id="comment-5831" class="comment"><div id="post-5831-score" class="comment-score"></div><div class="comment-text"><p>I assume you're plugging the two laptops into the same port on the Catalyst switch.</p><p>Are you capturing in promiscuous mode on both laptops?</p><p>What versions of which operating systems are running on the two laptops?</p></div><div id="comment-5831-info" class="comment-info"><span class="comment-age">(23 Aug '11, 17:54)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-5827" class="comment-tools"></div><div class="clear"></div><div id="comment-5827-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

