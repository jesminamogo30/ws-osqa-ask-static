+++
type = "question"
title = "How to set default Time Display Format ?"
description = '''The &quot;Time Display Format&quot; is always set to &quot;Seconds since beginning of capture&quot;. Even after I change it to something else, e.g. &quot;Time of Day&quot;. When launching the WS next time, it seems to default back to &quot;Seconds since beginning of capture&quot; again. How do I set to use a default time format, which wil...'''
date = "2016-12-22T16:02:00Z"
lastmod = "2016-12-23T13:32:00Z"
weight = 58306
keywords = [ "timestamps", "qtgui" ]
aliases = [ "/questions/58306" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to set default Time Display Format ?](/questions/58306/how-to-set-default-time-display-format)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58306-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58306-score" class="post-score" title="current number of votes">0</div><span id="post-58306-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The "Time Display Format" is always set to "Seconds since beginning of capture".</p><p>Even after I change it to something else, e.g. "Time of Day".</p><p>When launching the WS next time, it seems to default back to "Seconds since beginning of capture" again.</p><p>How do I set to use a default time format, which will show next time WS is launched, until changed again ?</p><p>Thanks &amp; Regards!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamps" rel="tag" title="see questions tagged &#39;timestamps&#39;">timestamps</span> <span class="post-tag tag-link-qtgui" rel="tag" title="see questions tagged &#39;qtgui&#39;">qtgui</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Dec '16, 16:02</strong></p><img src="https://secure.gravatar.com/avatar/6a77a1580d878771c51f9a2c50483e38?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LLouco&#39;s gravatar image" /><p><span>LLouco</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LLouco has no accepted answers">0%</span></p></div></div><div id="comments-container-58306" class="comments-container"><span id="58308"></span><div id="comment-58308" class="comment"><div id="post-58308-score" class="comment-score"></div><div class="comment-text"><p>Which version of Wireshark are you using? 2.0.x, 2.2.x, or a development version?</p></div><div id="comment-58308-info" class="comment-info"><span class="comment-age">(22 Dec '16, 20:12)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="58324"></span><div id="comment-58324" class="comment"><div id="post-58324-score" class="comment-score"></div><div class="comment-text"><p>I'm using: Version 2.2.3 (v2.2.3-0-g57531cd).</p></div><div id="comment-58324-info" class="comment-info"><span class="comment-age">(23 Dec '16, 10:25)</span> <span class="comment-user userinfo">LLouco</span></div></div><span id="58325"></span><div id="comment-58325" class="comment"><div id="post-58325-score" class="comment-score"></div><div class="comment-text"><p>On what version of what operating system are you running Wireshark? I cannot reproduce the</p><blockquote><p>Even after I change it to something else, e.g. "Time of Day".</p><p>When launching the WS next time, it seems to default back to "Seconds since beginning of capture" again.</p></blockquote><p>behavior on macOS Sierra - if I change the time format from the View menu, close Wireshark, and relaunch it, the new time format is used.</p></div><div id="comment-58325-info" class="comment-info"><span class="comment-age">(23 Dec '16, 13:04)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="58326"></span><div id="comment-58326" class="comment"><div id="post-58326-score" class="comment-score"></div><div class="comment-text"><p>Same thing for me with v2.2.3 on Windows 7. Wireshark retains the change and uses the new time format on relaunch.</p></div><div id="comment-58326-info" class="comment-info"><span class="comment-age">(23 Dec '16, 13:32)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div></div><div id="comment-tools-58306" class="comment-tools"></div><div class="clear"></div><div id="comment-58306-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

