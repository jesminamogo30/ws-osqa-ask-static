+++
type = "question"
title = "How do I use Wireshark to get the API response on a test transaction that uses authorize.net to process the payment?"
description = '''I am trying to integrate authorize.net into my website and am having trouble connecting to authorize.net using the AIM integration. How can I use wireshark to troubleshoot the API resonse from authorize.net in order to determine why transactions arent being processed?'''
date = "2016-07-24T19:11:00Z"
lastmod = "2016-07-24T19:11:00Z"
weight = 54279
keywords = [ "api", "response", "https" ]
aliases = [ "/questions/54279" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I use Wireshark to get the API response on a test transaction that uses authorize.net to process the payment?](/questions/54279/how-do-i-use-wireshark-to-get-the-api-response-on-a-test-transaction-that-uses-authorizenet-to-process-the-payment)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54279-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54279-score" class="post-score" title="current number of votes">0</div><span id="post-54279-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to integrate authorize.net into my website and am having trouble connecting to authorize.net using the AIM integration. How can I use wireshark to troubleshoot the API resonse from authorize.net in order to determine why transactions arent being processed?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-api" rel="tag" title="see questions tagged &#39;api&#39;">api</span> <span class="post-tag tag-link-response" rel="tag" title="see questions tagged &#39;response&#39;">response</span> <span class="post-tag tag-link-https" rel="tag" title="see questions tagged &#39;https&#39;">https</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jul '16, 19:11</strong></p><img src="https://secure.gravatar.com/avatar/25d4808d288f4008b5da2f3a35b87e2d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Doug&#39;s gravatar image" /><p><span>Doug</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Doug has no accepted answers">0%</span></p></div></div><div id="comments-container-54279" class="comments-container"></div><div id="comment-tools-54279" class="comment-tools"></div><div class="clear"></div><div id="comment-54279-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

