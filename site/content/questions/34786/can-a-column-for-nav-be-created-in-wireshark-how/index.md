+++
type = "question"
title = "Can a column for NAV be created in wireshark? How?"
description = '''Good day everyone. I want to analyse some packets using wireshark. I really want to see their network allocation vector (NAV). Is there any way i can be able to creat a column to show the corresponding NAV for each packet? Thanks. Maigana.'''
date = "2014-07-20T01:55:00Z"
lastmod = "2014-07-20T02:01:00Z"
weight = 34786
keywords = [ "nav" ]
aliases = [ "/questions/34786" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can a column for NAV be created in wireshark? How?](/questions/34786/can-a-column-for-nav-be-created-in-wireshark-how)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34786-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34786-score" class="post-score" title="current number of votes">0</div><span id="post-34786-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good day everyone. I want to analyse some packets using wireshark. I really want to see their network allocation vector (NAV). Is there any way i can be able to creat a column to show the corresponding NAV for each packet?</p><p>Thanks. Maigana.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nav" rel="tag" title="see questions tagged &#39;nav&#39;">nav</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jul '14, 01:55</strong></p><img src="https://secure.gravatar.com/avatar/e3778ae8a621767b52cdf5a8052a93c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Maigana&#39;s gravatar image" /><p><span>Maigana</span><br />
<span class="score" title="11 reputation points">11</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Maigana has no accepted answers">0%</span></p></div></div><div id="comments-container-34786" class="comments-container"></div><div id="comment-tools-34786" class="comment-tools"></div><div class="clear"></div><div id="comment-34786-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34787"></span>

<div id="answer-container-34787" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34787-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34787-score" class="post-score" title="current number of votes">0</div><span id="post-34787-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can create columns for any kind of field you see in the decode, so if you can find a field like that in a decoded frame you can right click it and select "Apply as column".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jul '14, 02:01</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-34787" class="comments-container"></div><div id="comment-tools-34787" class="comment-tools"></div><div class="clear"></div><div id="comment-34787-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

