+++
type = "question"
title = "Port Numbers"
description = '''I&#x27;m a little bit confused. I know about well-known and registered port numbers but never heard standard or non-standard ports. I will consider well-known and registered ports to be standard and the rest non-standard. Does this is correct or I have been missing something else? Thank you in advance. W...'''
date = "2014-07-23T15:27:00Z"
lastmod = "2014-07-25T02:50:00Z"
weight = 34859
keywords = [ "101" ]
aliases = [ "/questions/34859" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Port Numbers](/questions/34859/port-numbers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34859-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34859-score" class="post-score" title="current number of votes">0</div><span id="post-34859-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm a little bit confused. I know about well-known and registered port numbers but never heard standard or non-standard ports. I will consider well-known and registered ports to be standard and the rest non-standard. Does this is correct or I have been missing something else?</p><p>Thank you in advance.</p><p>Wireshark User</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-101" rel="tag" title="see questions tagged &#39;101&#39;">101</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jul '14, 15:27</strong></p><img src="https://secure.gravatar.com/avatar/9ee914a9fba57513c2612c9d2735a862?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cyverzek&#39;s gravatar image" /><p><span>cyverzek</span><br />
<span class="score" title="10 reputation points">10</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cyverzek has no accepted answers">0%</span></p></div></div><div id="comments-container-34859" class="comments-container"></div><div id="comment-tools-34859" class="comment-tools"></div><div class="clear"></div><div id="comment-34859-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34861"></span>

<div id="answer-container-34861" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34861-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34861-score" class="post-score" title="current number of votes">1</div><span id="post-34861-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If HTTP is transmitted via port 80, then it's the <strong>standard</strong> port, as 'everybody' is using HTTP on that port. It's also the <strong>well-known</strong> and the <strong>'registered'</strong> port. If you run your web server on port 8000 it's a <strong>non-standard</strong> port. In that case you'll have to add the port to the URL in the browser, like: <a href="http://server:8000/">http://server:8000/</a></p><p>The same applies for all other protocols (FTP, SMTP, etc.)</p><p>So, you can say: the <strong>standard</strong> port is the same as the <strong>well-known</strong> port and the <strong>'registered'</strong> port.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jul '14, 16:24</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-34861" class="comments-container"><span id="34898"></span><div id="comment-34898" class="comment"><div id="post-34898-score" class="comment-score"></div><div class="comment-text"><p>Thank you Kurt!</p></div><div id="comment-34898-info" class="comment-info"><span class="comment-age">(24 Jul '14, 14:35)</span> <span class="comment-user userinfo">cyverzek</span></div></div><span id="34907"></span><div id="comment-34907" class="comment"><div id="post-34907-score" class="comment-score"></div><div class="comment-text"><p><span>@cyverzek</span>: You awarded, one extra karma point to me. I'm not sure if that is what you really wanted to do. If you want to mark the question as answered, please follow the instructions below.</p><p>Hint: If a supplied answer resolves your question can you please "accept" it by clicking the checkmark icon next to it. This highlights good answers for the benefit of subsequent users with the same or similar questions. For extra points you can up vote the answer (thumb up).</p></div><div id="comment-34907-info" class="comment-info"><span class="comment-age">(25 Jul '14, 02:50)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-34861" class="comment-tools"></div><div class="clear"></div><div id="comment-34861-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

