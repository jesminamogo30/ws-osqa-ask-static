+++
type = "question"
title = "Some questions @ Wireshark Capture"
description = '''Hi Pls kindly help to see,  Is the frame an outgoing or an incoming frame? Identify the source IP address of the network-layer header in the frame? Identify the destination IP address of the network-layer header in the frame? Calculate the total number of bytes in the whole frame? Estimate the numbe...'''
date = "2013-09-20T04:42:00Z"
lastmod = "2013-09-20T09:07:00Z"
weight = 25009
keywords = [ "helpdesk", "homework", "wireshark" ]
aliases = [ "/questions/25009" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Some questions @ Wireshark Capture](/questions/25009/some-questions-wireshark-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25009-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25009-score" class="post-score" title="current number of votes">-2</div><span id="post-25009-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Pls kindly help to see,</p><ol><li>Is the frame an outgoing or an incoming frame?</li><li>Identify the source IP address of the network-layer header in the frame?</li><li>Identify the destination IP address of the network-layer header in the frame?</li><li>Calculate the total number of bytes in the whole frame?</li><li>Estimate the number of bytes in the Ethernet (data-link layer) header?</li><li>Estimate the number of bytes in the IP header?</li><li>Estimate the number of bytes in the TCP header?</li><li>What is the total bytes in the message (at the application layer)?</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-helpdesk" rel="tag" title="see questions tagged &#39;helpdesk&#39;">helpdesk</span> <span class="post-tag tag-link-homework" rel="tag" title="see questions tagged &#39;homework&#39;">homework</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Sep '13, 04:42</strong></p><img src="https://secure.gravatar.com/avatar/7611ebfee5f30257914005afa9b730f1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Johnson&#39;s gravatar image" /><p><span>Johnson</span><br />
<span class="score" title="9 reputation points">9</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Johnson has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Sep '13, 08:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-25009" class="comments-container"><span id="25013"></span><div id="comment-25013" class="comment"><div id="post-25013-score" class="comment-score">2</div><div class="comment-text"><p>Mr. Johnson, aren't you supposed to do your homework by yourself?</p><p>BTW: It's well known that teachers monitor this site to trap those who try to cheat. So, be warned ;-)</p><p>Regards<br />
Kurt</p></div><div id="comment-25013-info" class="comment-info"><span class="comment-age">(20 Sep '13, 04:49)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="25022"></span><div id="comment-25022" class="comment"><div id="post-25022-score" class="comment-score">1</div><div class="comment-text"><p>Hint: even if you remove the link to the screenshot, it's still there in the history.</p><blockquote><p><a href="http://ask.wireshark.org/revisions/25009/">http://ask.wireshark.org/revisions/25009/</a></p></blockquote></div><div id="comment-25022-info" class="comment-info"><span class="comment-age">(20 Sep '13, 05:20)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="25027"></span><div id="comment-25027" class="comment"><div id="post-25027-score" class="comment-score"></div><div class="comment-text"><p>oh, that's mean, spreading panic... :-)</p></div><div id="comment-25027-info" class="comment-info"><span class="comment-age">(20 Sep '13, 06:48)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="25028"></span><div id="comment-25028" class="comment"><div id="post-25028-score" class="comment-score"></div><div class="comment-text"><pre><code>From: :-)   --&gt;&gt;   To: =8-O</code></pre></div><div id="comment-25028-info" class="comment-info"><span class="comment-age">(20 Sep '13, 06:50)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="25031"></span><div id="comment-25031" class="comment"><div id="post-25031-score" class="comment-score">3</div><div class="comment-text"><p>In the future, please direct all homework questions to the <a href="http://www.wireshark.org/tools/helpdesk.html">Wireshark! Live! Helpdesk</a>.</p></div><div id="comment-25031-info" class="comment-info"><span class="comment-age">(20 Sep '13, 07:25)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="25036"></span><div id="comment-25036" class="comment not_top_scorer"><div id="post-25036-score" class="comment-score"></div><div class="comment-text"><p>cool feature!!</p><p>Finally all those newbys get <strong>direct access</strong> to the most talented Wireshark experts. Maybe I'm going to ask one or two questions myself ;-))</p></div><div id="comment-25036-info" class="comment-info"><span class="comment-age">(20 Sep '13, 07:35)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="25048"></span><div id="comment-25048" class="comment not_top_scorer"><div id="post-25048-score" class="comment-score"></div><div class="comment-text"><p>Wow, nice, finally, an expert I can ask! :-D</p></div><div id="comment-25048-info" class="comment-info"><span class="comment-age">(20 Sep '13, 09:07)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-25009" class="comment-tools"><span class="comments-showing"> showing 5 of 7 </span> <a href="#" class="show-all-comments-link">show 2 more comments</a></div><div class="clear"></div><div id="comment-25009-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25014"></span>

<div id="answer-container-25014" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25014-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25014-score" class="post-score" title="current number of votes">1</div><span id="post-25014-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yay, homework.</p><ol><li>can't tell - where was it captured, and what do you consider incoming or outgoing? It's a question of perspective. A switch considers a frame incoming while the server sending it considers it to be outgoing. So what is your point of origin?</li><li>seriously? You can read that easily. It's on the screen.</li><li>same</li><li>why calculate? it's in the length column</li><li>estimate? Its Ethernet II, so 14 bytes. Give or take. Well, not really. It's 14.</li><li>at least 20</li><li>same</li><li>look at the length given in the TCP protocol header summary line in the decode</li></ol><p>Sorry for making some fun of this, but you should find these things out yourself, or you'll be in trouble if someone (your teacher/professor) starts asking simple questions...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Sep '13, 04:49</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span> </br></p></div></div><div id="comments-container-25014" class="comments-container"></div><div id="comment-tools-25014" class="comment-tools"></div><div class="clear"></div><div id="comment-25014-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

