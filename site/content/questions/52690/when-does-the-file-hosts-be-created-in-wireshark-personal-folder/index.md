+++
type = "question"
title = "when does the file &quot;hosts&quot; be created in Wireshark Personal Folder ?"
description = '''On my computer, I have a hosts file in my personal preference folder. I&#x27;ve shown it to a colleague that has a quite new installed version of Wireshark and the file does not exist on his computer. Sames situation for 3 other people I&#x27;ve asked to test. Is this file automatically created when I create ...'''
date = "2016-05-17T12:52:00Z"
lastmod = "2016-05-18T07:11:00Z"
weight = 52690
keywords = [ "hosts", "file" ]
aliases = [ "/questions/52690" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [when does the file "hosts" be created in Wireshark Personal Folder ?](/questions/52690/when-does-the-file-hosts-be-created-in-wireshark-personal-folder)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52690-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52690-score" class="post-score" title="current number of votes">0</div><span id="post-52690-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>On my computer, I have a hosts file in my personal preference folder. I've shown it to a colleague that has a quite new installed version of Wireshark and the file does not exist on his computer. Sames situation for 3 other people I've asked to test.</p><p>Is this file automatically created when I create for example a new profile ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hosts" rel="tag" title="see questions tagged &#39;hosts&#39;">hosts</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 May '16, 12:52</strong></p><img src="https://secure.gravatar.com/avatar/eac75eef24254c1c9ee690951f6c4006?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thierryn&#39;s gravatar image" /><p><span>thierryn</span><br />
<span class="score" title="21 reputation points">21</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thierryn has no accepted answers">0%</span></p></div></div><div id="comments-container-52690" class="comments-container"></div><div id="comment-tools-52690" class="comment-tools"></div><div class="clear"></div><div id="comment-52690-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52724"></span>

<div id="answer-container-52724" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52724-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52724-score" class="post-score" title="current number of votes">2</div><span id="post-52724-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="thierryn has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That file is not created by Wireshark. Someone must have manually created the file at some point--maybe in the distant past.</p><p>(The file is read by Wireshark and if it's in a profile then if you copy that profile presumably it would be copied to the new profile. But Wireshark doesn't create it.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 May '16, 07:11</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-52724" class="comments-container"></div><div id="comment-tools-52724" class="comment-tools"></div><div class="clear"></div><div id="comment-52724-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

