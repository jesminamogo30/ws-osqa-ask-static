+++
type = "question"
title = "Getting wireshark working on os x mountain lion"
description = '''Hi, I&#x27;m a new OS X Mountain Lion user, and I installed Xquartz like I was told to for an X server, but Wireshark keeps invoking the X11.app, which prompts me to install Xquartz. I feel that I must be doing something wrong. Help appreciated.'''
date = "2012-11-30T21:17:00Z"
lastmod = "2012-12-01T09:24:00Z"
weight = 16471
keywords = [ "osx", "xquartz", "wireshark" ]
aliases = [ "/questions/16471" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Getting wireshark working on os x mountain lion](/questions/16471/getting-wireshark-working-on-os-x-mountain-lion)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16471-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16471-score" class="post-score" title="current number of votes">0</div><span id="post-16471-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I'm a new OS X Mountain Lion user, and I installed Xquartz like I was told to for an X server, but Wireshark keeps invoking the X11.app, which prompts me to install Xquartz. I feel that I must be doing something wrong.</p><p>Help appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-xquartz" rel="tag" title="see questions tagged &#39;xquartz&#39;">xquartz</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Nov '12, 21:17</strong></p><img src="https://secure.gravatar.com/avatar/9a4ae17201525474ec26e5d89b66477e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="msoulier&#39;s gravatar image" /><p><span>msoulier</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="msoulier has no accepted answers">0%</span></p></div></div><div id="comments-container-16471" class="comments-container"></div><div id="comment-tools-16471" class="comment-tools"></div><div class="clear"></div><div id="comment-16471-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16473"></span>

<div id="answer-container-16473" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16473-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16473-score" class="post-score" title="current number of votes">0</div><span id="post-16473-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You might have to log out and log in again (or possibly reboot) after installing Xquartz (I don't remember offhand).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Nov '12, 23:52</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-16473" class="comments-container"><span id="16479"></span><div id="comment-16479" class="comment"><div id="post-16479-score" class="comment-score"></div><div class="comment-text"><p>Yeah I tried that, it didn't help.</p></div><div id="comment-16479-info" class="comment-info"><span class="comment-age">(01 Dec '12, 09:24)</span> <span class="comment-user userinfo">msoulier</span></div></div></div><div id="comment-tools-16473" class="comment-tools"></div><div class="clear"></div><div id="comment-16473-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

