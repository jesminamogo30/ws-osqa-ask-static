+++
type = "question"
title = "Does the 64-bit version of Wireshark support SNMP OID resolution"
description = '''I have Wireshark on a Windows Server 2003 machine (32-bit) and Windows 7 Professional machine (64-bit). I have enabled the option to resolve OIDs on the 32-bit one but I am not able to do so on the 64-bit one. The preferences section for Name Resolution only shows NA instead.'''
date = "2011-01-21T10:52:00Z"
lastmod = "2011-01-23T03:06:00Z"
weight = 1852
keywords = [ "name-resolving", "snmp", "64-bit" ]
aliases = [ "/questions/1852" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Does the 64-bit version of Wireshark support SNMP OID resolution](/questions/1852/does-the-64-bit-version-of-wireshark-support-snmp-oid-resolution)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1852-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1852-score" class="post-score" title="current number of votes">0</div><span id="post-1852-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have Wireshark on a Windows Server 2003 machine (32-bit) and Windows 7 Professional machine (64-bit). I have enabled the option to resolve OIDs on the 32-bit one but I am not able to do so on the 64-bit one. The preferences section for <strong>Name Resolution</strong> only shows <strong>NA</strong> instead.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-name-resolving" rel="tag" title="see questions tagged &#39;name-resolving&#39;">name-resolving</span> <span class="post-tag tag-link-snmp" rel="tag" title="see questions tagged &#39;snmp&#39;">snmp</span> <span class="post-tag tag-link-64-bit" rel="tag" title="see questions tagged &#39;64-bit&#39;">64-bit</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '11, 10:52</strong></p><img src="https://secure.gravatar.com/avatar/de9e22c123217a4211147bd618d9ec0e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vivek&#39;s gravatar image" /><p><span>Vivek</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vivek has no accepted answers">0%</span></p></div></div><div id="comments-container-1852" class="comments-container"></div><div id="comment-tools-1852" class="comment-tools"></div><div class="clear"></div><div id="comment-1852-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1886"></span>

<div id="answer-container-1886" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1886-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1886-score" class="post-score" title="current number of votes">0</div><span id="post-1886-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The Windows 64 bits version does not support SNMP OID resolution as the package needed to do it - libsmi isn't available for Windows 64 Bits.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jan '11, 03:06</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-1886" class="comments-container"></div><div id="comment-tools-1886" class="comment-tools"></div><div class="clear"></div><div id="comment-1886-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

