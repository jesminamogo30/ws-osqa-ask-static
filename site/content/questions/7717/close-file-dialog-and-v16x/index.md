+++
type = "question"
title = "Close File Dialog and v.1.6x"
description = '''On a Windows XP, SP2 machine I am getting this hung Close File issue. The only way around it is to define the filename beforehand and have it record multiple files of a specific size. Then when I am finished, I use TaskManager to kill Wireshark. Pretty tedious &amp;amp; not very pretty.'''
date = "2011-11-30T08:17:00Z"
lastmod = "2011-11-30T14:30:00Z"
weight = 7717
keywords = [ "capture" ]
aliases = [ "/questions/7717" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Close File Dialog and v.1.6x](/questions/7717/close-file-dialog-and-v16x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7717-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7717-score" class="post-score" title="current number of votes">0</div><span id="post-7717-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>On a Windows XP, SP2 machine I am getting this hung Close File issue. The only way around it is to define the filename beforehand and have it record multiple files of a specific size. Then when I am finished, I use TaskManager to kill Wireshark. Pretty tedious &amp; not very pretty.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Nov '11, 08:17</strong></p><img src="https://secure.gravatar.com/avatar/33ac0ea40fe77b58643888b0d78424e9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="VictorD&#39;s gravatar image" /><p><span>VictorD</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="VictorD has no accepted answers">0%</span></p></div></div><div id="comments-container-7717" class="comments-container"></div><div id="comment-tools-7717" class="comment-tools"></div><div class="clear"></div><div id="comment-7717-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7720"></span>

<div id="answer-container-7720" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7720-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7720-score" class="post-score" title="current number of votes">1</div><span id="post-7720-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's sounds like <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3046">bug 3046</a>, which should be fixed in the next (1.6.5) stable release.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Nov '11, 14:30</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Nov '11, 18:37</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-7720" class="comments-container"></div><div id="comment-tools-7720" class="comment-tools"></div><div class="clear"></div><div id="comment-7720-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

