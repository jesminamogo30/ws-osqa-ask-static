+++
type = "question"
title = "Where I should start to learn WIRESHARK ? [Basic Question]"
description = '''Where I should start to learn WireShark ? What all are I should want know to learn WireShark ?'''
date = "2013-11-28T01:04:00Z"
lastmod = "2013-11-28T02:15:00Z"
weight = 27522
keywords = [ "beginner", "wireshark" ]
aliases = [ "/questions/27522" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Where I should start to learn WIRESHARK ? \[Basic Question\]](/questions/27522/where-i-should-start-to-learn-wireshark-basic-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27522-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27522-score" class="post-score" title="current number of votes">0</div><span id="post-27522-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Where I should start to learn WireShark ?</p><p>What all are I should want know to learn WireShark ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-beginner" rel="tag" title="see questions tagged &#39;beginner&#39;">beginner</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Nov '13, 01:04</strong></p><img src="https://secure.gravatar.com/avatar/7b7cc95bf3e350f7f5805d72f5667914?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kumar%20P&#39;s gravatar image" /><p><span>Kumar P</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kumar P has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Nov '13, 01:05</strong> </span></p></div></div><div id="comments-container-27522" class="comments-container"></div><div id="comment-tools-27522" class="comment-tools"></div><div class="clear"></div><div id="comment-27522-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27525"></span>

<div id="answer-container-27525" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27525-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27525-score" class="post-score" title="current number of votes">0</div><span id="post-27525-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Take a look here: <a href="http://ask.wireshark.org/questions/19980/how-to-study-to-use-wireshark">http://ask.wireshark.org/questions/19980/how-to-study-to-use-wireshark</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Nov '13, 02:15</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-27525" class="comments-container"></div><div id="comment-tools-27525" class="comment-tools"></div><div class="clear"></div><div id="comment-27525-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

