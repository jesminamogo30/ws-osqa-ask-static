+++
type = "question"
title = "capture filter expression always invalid"
description = '''I have just installed Version 2.2.3 (v2.2.3-0-g57531cd) on my MBP running OSX 10.12.2. When I run wireshark, it refuses to accept any filter expression I enter. For example, I enter &#x27;host 192.0.2.1&#x27; (one of the default examples) and the filter window remains red. It is utterly broken. The same filte...'''
date = "2017-01-03T04:21:00Z"
lastmod = "2017-01-03T08:18:00Z"
weight = 58475
keywords = [ "filter", "capture", "macosx", "sierra" ]
aliases = [ "/questions/58475" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [capture filter expression always invalid](/questions/58475/capture-filter-expression-always-invalid)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58475-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58475-score" class="post-score" title="current number of votes">0</div><span id="post-58475-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have just installed Version 2.2.3 (v2.2.3-0-g57531cd) on my MBP running OSX 10.12.2.</p><p>When I run wireshark, it refuses to accept any filter expression I enter. For example, I enter 'host 192.0.2.1' (one of the default examples) and the filter window remains red. It is utterly broken.</p><p>The same filter expression on my other Mac running Wireshark version 2.0.0 on OSX 10.11.6 works exactly as expected -- green background.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-sierra" rel="tag" title="see questions tagged &#39;sierra&#39;">sierra</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jan '17, 04:21</strong></p><img src="https://secure.gravatar.com/avatar/48ec40781f38cfc1bbbd81895aa73fe8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="et01267&#39;s gravatar image" /><p><span>et01267</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="et01267 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Jan '17, 04:23</strong> </span></p></div></div><div id="comments-container-58475" class="comments-container"><span id="58478"></span><div id="comment-58478" class="comment"><div id="post-58478-score" class="comment-score"></div><div class="comment-text"><p>Do you have an interface selected?</p></div><div id="comment-58478-info" class="comment-info"><span class="comment-age">(03 Jan '17, 05:58)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-58475" class="comment-tools"></div><div class="clear"></div><div id="comment-58475-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58482"></span>

<div id="answer-container-58482" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58482-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58482-score" class="post-score" title="current number of votes">0</div><span id="post-58482-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No interface was selected before typing the capture filter, as discussed in bug <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=13287">13287</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jan '17, 08:18</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-58482" class="comments-container"></div><div id="comment-tools-58482" class="comment-tools"></div><div class="clear"></div><div id="comment-58482-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

