+++
type = "question"
title = "WPA decryption"
description = '''Hi I want to decrypt my WPA packets which i gathered by sniffing other computer from my network with airodump-ng. The problem is that even after I have the 4-way handshake packets (they are correct) i don&#x27;t know how to extract the PSK so i can put it in the IEEE 802.11 wireshark preferences. However...'''
date = "2013-07-28T07:01:00Z"
lastmod = "2013-07-28T07:01:00Z"
weight = 23413
keywords = [ "decryption", "802.11", "wpa" ]
aliases = [ "/questions/23413" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WPA decryption](/questions/23413/wpa-decryption)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23413-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23413-score" class="post-score" title="current number of votes">0</div><span id="post-23413-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I want to decrypt my WPA packets which i gathered by sniffing other computer from my network with airodump-ng.</p><p>The problem is that even after I have the 4-way handshake packets (they are correct) i don't know how to extract the PSK so i can put it in the IEEE 802.11 wireshark preferences. However i created the "raw" PSK but after i typed it into WPA decrytpion keys and enabled the decryption it still doesnt want to work. Wireshark is doing the decrytpion but it doesn't changes anything i still see only 802.11 protocol packets (I have about 20k of them).</p><ul><li>WPA-PSK:ee2b63f6068bdb1b7935ca7a5c5e3a5303c56f2ab3a60e8f130fbea1e305010d "raw" psk</li><li>password: london84</li><li>essid: virginmedia8083545</li></ul><p>Do i need diffrent PSK evry time i need to decrypt some packets ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-wpa" rel="tag" title="see questions tagged &#39;wpa&#39;">wpa</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jul '13, 07:01</strong></p><img src="https://secure.gravatar.com/avatar/9a10efb1f456b13fad4b82c1aed2601a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="uMaikelos&#39;s gravatar image" /><p><span>uMaikelos</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="uMaikelos has no accepted answers">0%</span></p></div></div><div id="comments-container-23413" class="comments-container"></div><div id="comment-tools-23413" class="comment-tools"></div><div class="clear"></div><div id="comment-23413-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

