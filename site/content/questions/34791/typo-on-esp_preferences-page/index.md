+++
type = "question"
title = "Typo on ESP_Preferences page?"
description = '''Hello, Is there a typo on this page: http://wiki.wireshark.org/ESP_Preferences in this section? * Start of section* [IP1][ESP1][ENCRYPTION1] with [ENCRYPTION1]=[IP2][ESP2][ENCRYPTION2] and [ENCRYPTION2]=ICMP IP1 is IP header from SGW1 to N2 ENCRYPTION2 is aes-cbc IP2 is IP header from N1 to N2 ENCRY...'''
date = "2014-07-20T08:34:00Z"
lastmod = "2014-07-20T08:34:00Z"
weight = 34791
keywords = [ "esp_preferences" ]
aliases = [ "/questions/34791" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Typo on ESP\_Preferences page?](/questions/34791/typo-on-esp_preferences-page)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34791-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34791-score" class="post-score" title="current number of votes">0</div><span id="post-34791-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, Is there a typo on this page:</p><p><a href="http://wiki.wireshark.org/ESP_Preferences">http://wiki.wireshark.org/ESP_Preferences</a></p><p>in this section?</p><p><strong><em>* Start of section</em></strong>*</p><p>[IP1][ESP1][ENCRYPTION1]</p><p>with [ENCRYPTION1]=[IP2][ESP2][ENCRYPTION2] and [ENCRYPTION2]=ICMP</p><p>IP1 is IP header from SGW1 to N2 ENCRYPTION2 is aes-cbc IP2 is IP header from N1 to N2 ENCRYPTION2 is des-cbc</p><p><strong><em>* End of section</em></strong>*</p><p>Shouldn't the line "ENCRYPTION2 is aes-cbc" read "ENCRYPTION1 is aes-cbc" since that's the protocol used between SGW1 and N2?</p><p>Just wanted to make sure about this for better understanding.</p><p>AA</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-esp_preferences" rel="tag" title="see questions tagged &#39;esp_preferences&#39;">esp_preferences</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jul '14, 08:34</strong></p><img src="https://secure.gravatar.com/avatar/83dc9147927bdcd2f250432da434dcb3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aa77&#39;s gravatar image" /><p><span>aa77</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aa77 has no accepted answers">0%</span></p></div></div><div id="comments-container-34791" class="comments-container"></div><div id="comment-tools-34791" class="comment-tools"></div><div class="clear"></div><div id="comment-34791-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

