+++
type = "question"
title = "Decoding TD.35"
description = '''Can someone explain how to decode TD.35 data format or someone tell where can i find the specification of the TD.35 data format? The following is the sample data, 30272017111544749011353876999672 0021 353876999672 0000801208061845300000000000000000000000000000 30272017111544749011353876999673 0021 3...'''
date = "2013-04-20T00:03:00Z"
lastmod = "2013-04-24T04:10:00Z"
weight = 20649
keywords = [ "ss7", "td.35", "sigtran" ]
aliases = [ "/questions/20649" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding TD.35](/questions/20649/decoding-td35)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20649-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20649-score" class="post-score" title="current number of votes">0</div><span id="post-20649-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can someone explain how to decode TD.35 data format or someone tell where can i find the specification of the TD.35 data format?</p><p>The following is the sample data,</p><pre><code>30272017111544749011353876999672      0021    353876999672   0000801208061845300000000000000000000000000000
30272017111544749011353876999673      0021    353876999673   0000801208061754310000000000000000000000000000</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ss7" rel="tag" title="see questions tagged &#39;ss7&#39;">ss7</span> <span class="post-tag tag-link-td.35" rel="tag" title="see questions tagged &#39;td.35&#39;">td.35</span> <span class="post-tag tag-link-sigtran" rel="tag" title="see questions tagged &#39;sigtran&#39;">sigtran</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '13, 00:03</strong></p><img src="https://secure.gravatar.com/avatar/b2940a37e14d31283e43c55dc07a1fea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Manoj%20G&#39;s gravatar image" /><p><span>Manoj G</span><br />
<span class="score" title="40 reputation points">40</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Manoj G has 2 accepted answers">33%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Apr '13, 00:06</strong> </span></p></div></div><div id="comments-container-20649" class="comments-container"></div><div id="comment-tools-20649" class="comment-tools"></div><div class="clear"></div><div id="comment-20649-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="20668"></span>

<div id="answer-container-20668" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20668-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20668-score" class="post-score" title="current number of votes">0</div><span id="post-20668-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As far as I can tell (with a few minutes of searching), the TD.35 / NRTRDE specifications aren't available to the public, at least not without asking someone for them. You could try the GSMA (gsma.com) or Syniverse (syniverse.com), who apparently created TD.35. It's probably available as a download to GSMA members, but unless your employer is already a member that's not very helpful. (They only have corporate memberships, and they're pricy.)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Apr '13, 20:38</strong></p><img src="https://secure.gravatar.com/avatar/513295189ee24340b8de5822e630c627?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mwojcik&#39;s gravatar image" /><p><span>mwojcik</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mwojcik has no accepted answers">0%</span></p></div></div><div id="comments-container-20668" class="comments-container"></div><div id="comment-tools-20668" class="comment-tools"></div><div class="clear"></div><div id="comment-20668-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20652"></span>

<div id="answer-container-20652" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20652-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20652-score" class="post-score" title="current number of votes">-1</div><span id="post-20652-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Can someone explain how to decode TD.35 data format</p></blockquote><p>AFIAK wireshark is unable to dissect TD.35.</p><blockquote><p>or someone tell where can i find the specification of the TD.35 data format?</p></blockquote><p>I'm sure you will find it via google: TD.35 specs</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Apr '13, 05:44</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Apr '13, 05:51</strong> </span></p></div></div><div id="comments-container-20652" class="comments-container"><span id="20666"></span><div id="comment-20666" class="comment"><div id="post-20666-score" class="comment-score"></div><div class="comment-text"><p><span>@Kurt</span>: Thank you. I did it before posting this question itself. I feel it is not good to post any question in forums without proper search. May be I'm bad in search!</p></div><div id="comment-20666-info" class="comment-info"><span class="comment-age">(20 Apr '13, 19:03)</span> <span class="comment-user userinfo">Manoj G</span></div></div><span id="20676"></span><div id="comment-20676" class="comment"><div id="post-20676-score" class="comment-score"></div><div class="comment-text"><p>As <span>@mwojcik</span> said: The specs are probably not available publicly :-(</p></div><div id="comment-20676-info" class="comment-info"><span class="comment-age">(21 Apr '13, 07:47)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="20760"></span><div id="comment-20760" class="comment"><div id="post-20760-score" class="comment-score"></div><div class="comment-text"><p><span></span><span>@Manoj G</span>: What's wrong with my answer?</p></div><div id="comment-20760-info" class="comment-info"><span class="comment-age">(24 Apr '13, 04:10)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-20652" class="comment-tools"></div><div class="clear"></div><div id="comment-20652-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

