+++
type = "question"
title = "I can&#x27;t figure out why i can&#x27;t capture 802.11 encrypted packets"
description = '''Hi, i&#x27;m on ubuntu 12.04 and i have an issue regarding the capture of 802.11 Wpa encrypted packets. Testing my card i have found that IF the network is unencrypted i can pick up all the packets from all the clients.  IF the network is encrypted i can catch EAPOL authentication of other clients and no...'''
date = "2013-03-26T01:53:00Z"
lastmod = "2013-03-26T01:53:00Z"
weight = 19832
keywords = [ "capture", "promiscuous", "802.11", "wpa" ]
aliases = [ "/questions/19832" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [I can't figure out why i can't capture 802.11 encrypted packets](/questions/19832/i-cant-figure-out-why-i-cant-capture-80211-encrypted-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19832-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19832-score" class="post-score" title="current number of votes">0</div><span id="post-19832-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, i'm on ubuntu 12.04 and i have an issue regarding the capture of 802.11 Wpa encrypted packets. Testing my card i have found that IF the network is unencrypted i can pick up all the packets from all the clients. IF the network is encrypted i can catch EAPOL authentication of other clients and nothing else. My problem is not decrypting the traffic, because i've found that i can decrypt the test file from the Wireshark wiki, my problem is that if the network is encrypted i can't capture packets AT ALL. I see only my local traffic, EAPOL, some ARP packets and broadcasting stuff.</p><p>Please someone help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-wpa" rel="tag" title="see questions tagged &#39;wpa&#39;">wpa</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '13, 01:53</strong></p><img src="https://secure.gravatar.com/avatar/5bef33870a99e72004999df7f6ebfc82?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BlueStarry&#39;s gravatar image" /><p><span>BlueStarry</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BlueStarry has no accepted answers">0%</span></p></div></div><div id="comments-container-19832" class="comments-container"></div><div id="comment-tools-19832" class="comment-tools"></div><div class="clear"></div><div id="comment-19832-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

