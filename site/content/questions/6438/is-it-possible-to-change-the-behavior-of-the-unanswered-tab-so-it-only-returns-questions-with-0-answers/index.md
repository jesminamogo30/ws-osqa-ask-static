+++
type = "question"
title = "Is it possible to change the behavior of the &quot;unanswered&quot; tab so it only returns questions with 0 answers?"
description = '''Basically, I am wondering if it&#x27;s possible if we implement what dougvk apparently already has in his answer to this question at OSQA. While we&#x27;re at it, I would like to be able to sort by views as well. dougvk has apparently found a solution for that as well, which he provides in the same answer.'''
date = "2011-09-18T17:49:00Z"
lastmod = "2011-09-18T17:49:00Z"
weight = 6438
keywords = [ "sort", "meta", "ask.wireshark.org", "unanswered", "view" ]
aliases = [ "/questions/6438" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to change the behavior of the "unanswered" tab so it only returns questions with 0 answers?](/questions/6438/is-it-possible-to-change-the-behavior-of-the-unanswered-tab-so-it-only-returns-questions-with-0-answers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6438-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6438-score" class="post-score" title="current number of votes">0</div><span id="post-6438-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Basically, I am wondering if it's possible if we implement what <a href="http://meta.osqa.net/users/932/dougvk">dougvk</a> apparently already has in his answer to <a href="http://meta.osqa.net/questions/6103/best-way-to-modify-unanswered-question-sorting">this</a> question at <a href="http://www.osqa.net/">OSQA</a>.</p><p>While we're at it, I would like to be able to sort by views as well. dougvk has apparently found a solution for that as well, which he provides in the same answer.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sort" rel="tag" title="see questions tagged &#39;sort&#39;">sort</span> <span class="post-tag tag-link-meta" rel="tag" title="see questions tagged &#39;meta&#39;">meta</span> <span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span> <span class="post-tag tag-link-unanswered" rel="tag" title="see questions tagged &#39;unanswered&#39;">unanswered</span> <span class="post-tag tag-link-view" rel="tag" title="see questions tagged &#39;view&#39;">view</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Sep '11, 17:49</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>19 Sep '11, 05:32</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-6438" class="comments-container"></div><div id="comment-tools-6438" class="comment-tools"></div><div class="clear"></div><div id="comment-6438-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

