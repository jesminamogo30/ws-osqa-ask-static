+++
type = "question"
title = "Apache Struts Vulnarabilities"
description = '''Apache Struts is a popular web application framework. There have been several Critical vulnerabilities (3 this year) being found across wide range of Struts versions. A new Apache Struts vulnerability was discovered and announced this week. All versions of struts 2.1-2.3 and 2.5-2.5.12 are vulnerabl...'''
date = "2017-09-21T05:04:00Z"
lastmod = "2017-09-21T06:09:00Z"
weight = 63620
keywords = [ "apache" ]
aliases = [ "/questions/63620" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Apache Struts Vulnarabilities](/questions/63620/apache-struts-vulnarabilities)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63620-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63620-score" class="post-score" title="current number of votes">0</div><span id="post-63620-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Apache Struts is a popular web application framework. There have been several Critical vulnerabilities (3 this year) being found across wide range of Struts versions. A new Apache Struts vulnerability was discovered and announced this week. All versions of struts 2.1-2.3 and 2.5-2.5.12 are vulnerable. The official Apache info is located here: <a href="https://struts.apache.org/docs/s2-052.html">https://struts.apache.org/docs/s2-052.html</a><br />
As of right now, the only remediation is to upgrade to the latest version that was released on 9/20 - 2.3.34 or 2.5.13.</p><p>Could you pls confirm to me asap that Wireshark and all application(s) used by Wireshark are using Struts or not. If any of them are using Struts could you pls supply the version?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-apache" rel="tag" title="see questions tagged &#39;apache&#39;">apache</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Sep '17, 05:04</strong></p><img src="https://secure.gravatar.com/avatar/4fc43c83d14e6cb53bf36dd8013dbcf1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="profke&#39;s gravatar image" /><p><span>profke</span><br />
<span class="score" title="10 reputation points">10</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="profke has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-63620" class="comments-container"></div><div id="comment-tools-63620" class="comment-tools"></div><div class="clear"></div><div id="comment-63620-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63621"></span>

<div id="answer-container-63621" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63621-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63621-score" class="post-score" title="current number of votes">1</div><span id="post-63621-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark doesn't use Struts, it's entirely self-supporting.</p><p>As to other applications that "use" Wireshark, (considering it's not a framework or server for others to build on), who knows.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Sep '17, 05:10</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-63621" class="comments-container"><span id="63623"></span><div id="comment-63623" class="comment"><div id="post-63623-score" class="comment-score"></div><div class="comment-text"><p>Thank you!</p></div><div id="comment-63623-info" class="comment-info"><span class="comment-age">(21 Sep '17, 06:09)</span> <span class="comment-user userinfo">profke</span></div></div></div><div id="comment-tools-63621" class="comment-tools"></div><div class="clear"></div><div id="comment-63621-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

