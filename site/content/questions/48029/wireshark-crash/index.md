+++
type = "question"
title = "Wireshark Crash"
description = '''I have a 6.3GB trace file of my machine copying a 10GB file to my nas, when I open the trace file wireshark crashes but not immediately, after a few mins. I doubt the spec of my machine has anything to do with it, I have 32GB of RAM. Just wondering if I am missing something, hoping someone has an id...'''
date = "2015-11-27T09:39:00Z"
lastmod = "2015-11-27T09:51:00Z"
weight = 48029
keywords = [ "crash" ]
aliases = [ "/questions/48029" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Crash](/questions/48029/wireshark-crash)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48029-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48029-score" class="post-score" title="current number of votes">0</div><span id="post-48029-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a 6.3GB trace file of my machine copying a 10GB file to my nas, when I open the trace file wireshark crashes but not immediately, after a few mins.</p><p>I doubt the spec of my machine has anything to do with it, I have 32GB of RAM.</p><p>Just wondering if I am missing something, hoping someone has an idea ?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Nov '15, 09:39</strong></p><img src="https://secure.gravatar.com/avatar/1d67795c1484b0652b735a3827dcb9b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m0rph&#39;s gravatar image" /><p><span>m0rph</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m0rph has no accepted answers">0%</span></p></div></div><div id="comments-container-48029" class="comments-container"></div><div id="comment-tools-48029" class="comment-tools"></div><div class="clear"></div><div id="comment-48029-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48031"></span>

<div id="answer-container-48031" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48031-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48031-score" class="post-score" title="current number of votes">0</div><span id="post-48031-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is likely to be an out of memory issue. See the relevant <a href="https://wiki.wireshark.org/KnownBugs/OutOfMemory">page</a> on the wiki.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Nov '15, 09:51</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-48031" class="comments-container"></div><div id="comment-tools-48031" class="comment-tools"></div><div class="clear"></div><div id="comment-48031-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

