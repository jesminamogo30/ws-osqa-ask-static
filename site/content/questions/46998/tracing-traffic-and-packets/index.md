+++
type = "question"
title = "Tracing traffic and packets"
description = '''I have a web server in the could that is making requests to an internal server behind a few firewalls. All traffic is being pushed through this VPN tunnel. As such this traffic is being dropped and/or seriously lagged. I am looking for some software to listen and determine when and where the interru...'''
date = "2015-10-27T13:06:00Z"
lastmod = "2015-10-27T13:09:00Z"
weight = 46998
keywords = [ "packets" ]
aliases = [ "/questions/46998" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Tracing traffic and packets](/questions/46998/tracing-traffic-and-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46998-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46998-score" class="post-score" title="current number of votes">0</div><span id="post-46998-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a web server in the could that is making requests to an internal server behind a few firewalls. All traffic is being pushed through this VPN tunnel. As such this traffic is being dropped and/or seriously lagged. I am looking for some software to listen and determine when and where the interruptions are occurring. Will this software do that and how easy is it to set up? Any help would be greatly appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '15, 13:06</strong></p><img src="https://secure.gravatar.com/avatar/a07d5f2a02be2ffda33e0d289df57191?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jaykapalczynski&#39;s gravatar image" /><p><span>jaykapalczynski</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jaykapalczynski has no accepted answers">0%</span></p></div></div><div id="comments-container-46998" class="comments-container"><span id="46999"></span><div id="comment-46999" class="comment"><div id="post-46999-score" class="comment-score"></div><div class="comment-text"><p>I am being told that Blue Coat Proxy is interfering with HTTPS / SSL traffic and trying to see if there is a way that I can verify</p></div><div id="comment-46999-info" class="comment-info"><span class="comment-age">(27 Oct '15, 13:09)</span> <span class="comment-user userinfo">jaykapalczynski</span></div></div></div><div id="comment-tools-46998" class="comment-tools"></div><div class="clear"></div><div id="comment-46998-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

