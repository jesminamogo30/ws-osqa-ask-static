+++
type = "question"
title = "dumpcap outfile save location"
description = '''C:&#92;Program Files&#92;Wireshark&amp;gt;dumpcap -i &#92;Device&#92;NPF_{9E7AF7D7-A7CE-4350-882B-7223F2B9A333} -b duration:10 -b files:10 -a duration:200 -w outfile.pcap Capturing on &#92;Device&#92;NPF_{9E7AF7D7-A7CE-4350-882B-7223F2B9A333} File: outfile_00001_20130222164538.pcap Packets: 25 File: outfile_00002_2013022216454...'''
date = "2013-02-22T03:24:00Z"
lastmod = "2013-02-22T03:41:00Z"
weight = 18809
keywords = [ "outfile", "tshark", "automated", "dumpcap", "location" ]
aliases = [ "/questions/18809" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [dumpcap outfile save location](/questions/18809/dumpcap-outfile-save-location)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18809-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18809-score" class="post-score" title="current number of votes">0</div><span id="post-18809-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><pre><code>C:\Program Files\Wireshark&gt;dumpcap -i \Device\NPF_{9E7AF7D7-A7CE-4350-882B-7223F2B9A333} -b duration:10 -b files:10 -a duration:200 -w outfile.pcap
Capturing on \Device\NPF_{9E7AF7D7-A7CE-4350-882B-7223F2B9A333}
File: outfile_00001_20130222164538.pcap
Packets: 25 File: outfile_00002_20130222164548.pcap
Packets: 44 File: outfile_00003_20130222164558.pcap
Packets: 72 File: outfile_00004_20130222164608.pcap
Packets: 104 File: outfile_00005_20130222164618.pcap
Packets: 118 File: outfile_00006_20130222164628.pcap
Packets: 141 File: outfile_00007_20130222164638.pcap
Packets: 204 File: outfile_00008_20130222164648.pcap
Packets: 251 File: outfile_00009_20130222164658.pcap
Packets: 278 File: outfile_00010_20130222164708.pcap
Packets: 381 File: outfile_00011_20130222164718.pcap
Packets: 513 File: outfile_00012_20130222164728.pcap
Packets: 607 File: outfile_00013_20130222164738.pcap
Packets: 672 File: outfile_00014_20130222164748.pcap
Packets: 707 File: outfile_00015_20130222164758.pcap
Packets: 740 File: outfile_00016_20130222164808.pcap
Packets: 768 File: outfile_00017_20130222164818.pcap
Packets: 855 File: outfile_00018_20130222164828.pcap
Packets: 884 File: outfile_00019_20130222164838.pcap
Packets: 899 File: outfile_00020_20130222164848.pcap
Packets captured: 914
Packets received/dropped on interface \Device\NPF_{9E7AF7D7-A7CE-4350-882B-7223F2B9A333}: 914/0 (100.0%)</code></pre><p><strong>Where are the outfile_000&amp;&amp;_20130222&amp;&amp;&amp;&amp;&amp;&amp;.pcap saved</strong></p><p>I just don't seem to find them.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-outfile" rel="tag" title="see questions tagged &#39;outfile&#39;">outfile</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-automated" rel="tag" title="see questions tagged &#39;automated&#39;">automated</span> <span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span> <span class="post-tag tag-link-location" rel="tag" title="see questions tagged &#39;location&#39;">location</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '13, 03:24</strong></p><img src="https://secure.gravatar.com/avatar/0752849c661ba26916da497635abb5c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AshwinSethi&#39;s gravatar image" /><p><span>AshwinSethi</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AshwinSethi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Feb '13, 03:36</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-18809" class="comments-container"></div><div id="comment-tools-18809" class="comment-tools"></div><div class="clear"></div><div id="comment-18809-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18810"></span>

<div id="answer-container-18810" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18810-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18810-score" class="post-score" title="current number of votes">1</div><span id="post-18810-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="AshwinSethi has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would guess nowhere. From the prompt, you're running the capture from within the Wireshark "Program Files" directory and a normal user account doesn't have write permission in that directory so the files aren't written.</p><p>Either run the command from within a directory to which you do have write permission, or change the output file parameter to include a path that you can write to, e.g. <code>c:\temp\outfile.pcap</code></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Feb '13, 03:34</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-18810" class="comments-container"><span id="18812"></span><div id="comment-18812" class="comment"><div id="post-18812-score" class="comment-score"></div><div class="comment-text"><p>Logical enough. Got the pcap files. Thanks a lot</p></div><div id="comment-18812-info" class="comment-info"><span class="comment-age">(22 Feb '13, 03:41)</span> <span class="comment-user userinfo">AshwinSethi</span></div></div></div><div id="comment-tools-18810" class="comment-tools"></div><div class="clear"></div><div id="comment-18810-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

