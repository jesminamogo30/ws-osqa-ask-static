+++
type = "question"
title = "Wireshark not working with NVIDIA nForce adapter on Windows Vista"
description = '''lookng for help with nvidia nforce mcp network adapter driver not working with wire shark ,plzs help me  goin mad looking for help'''
date = "2014-04-18T09:43:00Z"
lastmod = "2014-04-18T13:54:00Z"
weight = 31970
keywords = [ "drivers", "nvidia" ]
aliases = [ "/questions/31970" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not working with NVIDIA nForce adapter on Windows Vista](/questions/31970/wireshark-not-working-with-nvidia-nforce-adapter-on-windows-vista)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31970-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31970-score" class="post-score" title="current number of votes">0</div><span id="post-31970-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>lookng for help with nvidia nforce mcp network adapter driver not working with wire shark ,plzs help me goin mad looking for help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-drivers" rel="tag" title="see questions tagged &#39;drivers&#39;">drivers</span> <span class="post-tag tag-link-nvidia" rel="tag" title="see questions tagged &#39;nvidia&#39;">nvidia</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '14, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/1c2d1c18802227ce0042a5844abf392f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sjs&#39;s gravatar image" /><p><span>sjs</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sjs has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Apr '14, 13:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-31970" class="comments-container"><span id="31978"></span><div id="comment-31978" class="comment"><div id="post-31978-score" class="comment-score"></div><div class="comment-text"><p>In <a href="http://ask.wireshark.org/questions/31969/nvidia-nforce-mcp-network-adapter-driver">your other question</a> you said "no interface"; is that the problem you're having? Does "no interface" mean that Wireshark doesn't show any network interfaces?</p></div><div id="comment-31978-info" class="comment-info"><span class="comment-age">(18 Apr '14, 13:54)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-31970" class="comment-tools"></div><div class="clear"></div><div id="comment-31970-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31973"></span>

<div id="answer-container-31973" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31973-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31973-score" class="post-score" title="current number of votes">0</div><span id="post-31973-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>NIC card is supposed to function in promicious mode, check weather Nvidia supports it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Apr '14, 12:08</strong></p><img src="https://secure.gravatar.com/avatar/ee0dd9b5ea44e7f8db0040e34109a712?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hardshah4&#39;s gravatar image" /><p><span>hardshah4</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hardshah4 has no accepted answers">0%</span></p></div></div><div id="comments-container-31973" class="comments-container"></div><div id="comment-tools-31973" class="comment-tools"></div><div class="clear"></div><div id="comment-31973-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

