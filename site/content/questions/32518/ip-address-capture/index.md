+++
type = "question"
title = "ip address capture"
description = ''' so lets say I type google, I am getting a lot of reads, which one is google ip address? http://imgur.com/I5y8yrI'''
date = "2014-05-04T19:07:00Z"
lastmod = "2014-05-04T20:23:00Z"
weight = 32518
keywords = [ "wireshark" ]
aliases = [ "/questions/32518" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ip address capture](/questions/32518/ip-address-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32518-score" class="post-score" title="current number of votes">0</div><span id="post-32518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="http://imgur.com/I5y8yrI" alt="alt text" /></p><p>so lets say I type google, I am getting a lot of reads, which one is google ip address?</p><p><a href="http://imgur.com/I5y8yrI">http://imgur.com/I5y8yrI</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 May '14, 19:07</strong></p><img src="https://secure.gravatar.com/avatar/ee6742b82568eba6c2e3f29d81ddd472?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ay7aga14&#39;s gravatar image" /><p><span>ay7aga14</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ay7aga14 has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 May '14, 19:08</strong> </span></p></div></div><div id="comments-container-32518" class="comments-container"></div><div id="comment-tools-32518" class="comment-tools"></div><div class="clear"></div><div id="comment-32518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32519"></span>

<div id="answer-container-32519" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32519-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32519-score" class="post-score" title="current number of votes">0</div><span id="post-32519-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is actually an interesting question now, since when that exercise was written Google was probably still defaulting to http instead of https. Which means that the whole communication used to be unencrypted and easy to track down. So unless you can find a DNS name resolution packet that tells you what IP the server of Google has in your trace you may have trouble finding it (if network name resolution doesn't help you - try enabling it in the "View" menu).</p><p>Best way to do it in the sense of what the book whats you to do is to call a web page that is not using https yet, otherwise you can't answer some of the questions, like for the URL, anyway.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 May '14, 20:23</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-32519" class="comments-container"></div><div id="comment-tools-32519" class="comment-tools"></div><div class="clear"></div><div id="comment-32519-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

