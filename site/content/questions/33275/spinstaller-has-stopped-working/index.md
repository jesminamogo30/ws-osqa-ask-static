+++
type = "question"
title = "[closed] SPInstaller has stopped working"
description = '''In the guide http://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html When I try to install Microsoft Visual C++ 2010 Service Pack 1 Compiler Update for the Windows SDK 7.1, It gives an error: SPInstaller has stopped working. Please help me solve the issue..'''
date = "2014-06-02T06:49:00Z"
lastmod = "2014-06-02T09:40:00Z"
weight = 33275
keywords = [ "sp1", "vc2010", "wireshark" ]
aliases = [ "/questions/33275" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] SPInstaller has stopped working](/questions/33275/spinstaller-has-stopped-working)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33275-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33275-score" class="post-score" title="current number of votes">0</div><span id="post-33275-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In the guide <a href="http://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html">http://www.wireshark.org/docs/wsdg_html_chunked/ChSetupWin32.html</a></p><p>When I try to install Microsoft Visual C++ 2010 Service Pack 1 Compiler Update for the Windows SDK 7.1, It gives an error: SPInstaller has stopped working. Please help me solve the issue..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sp1" rel="tag" title="see questions tagged &#39;sp1&#39;">sp1</span> <span class="post-tag tag-link-vc2010" rel="tag" title="see questions tagged &#39;vc2010&#39;">vc2010</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '14, 06:49</strong></p><img src="https://secure.gravatar.com/avatar/a9a254ac482208f766093c0f9c144364?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aman&#39;s gravatar image" /><p><span>aman</span><br />
<span class="score" title="36 reputation points">36</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aman has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>02 Jun '14, 09:40</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-33275" class="comments-container"><span id="33290"></span><div id="comment-33290" class="comment"><div id="post-33290-score" class="comment-score"></div><div class="comment-text"><p>This is not a Wireshark issue, this is a problem with your machine and the Visual Studio installation and as such is not an appropriate question for this site</p></div><div id="comment-33290-info" class="comment-info"><span class="comment-age">(02 Jun '14, 09:40)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-33275" class="comment-tools"></div><div class="clear"></div><div id="comment-33275-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 02 Jun '14, 09:40

</div>

</div>

</div>

