+++
type = "question"
title = "Printer Information"
description = '''I would like to collect printer information of a range of ip addresses.  Is that possible'''
date = "2016-02-21T08:50:00Z"
lastmod = "2016-02-22T03:47:00Z"
weight = 50385
keywords = [ "windows" ]
aliases = [ "/questions/50385" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Printer Information](/questions/50385/printer-information)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50385-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50385-score" class="post-score" title="current number of votes">0</div><span id="post-50385-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to collect printer information of a range of ip addresses.</p><p>Is that possible</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Feb '16, 08:50</strong></p><img src="https://secure.gravatar.com/avatar/36eb5e52be7bab7d2705949828a87696?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nightwing2099&#39;s gravatar image" /><p><span>Nightwing2099</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nightwing2099 has no accepted answers">0%</span></p></div></div><div id="comments-container-50385" class="comments-container"><span id="50400"></span><div id="comment-50400" class="comment"><div id="post-50400-score" class="comment-score"></div><div class="comment-text"><p>What sort of printer information? Wireshark is a packet capture and analysis tool, so you'll need something to generate the traffic to those IP addresses for Wireshark to capture and analyse.</p></div><div id="comment-50400-info" class="comment-info"><span class="comment-age">(22 Feb '16, 03:47)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-50385" class="comment-tools"></div><div class="clear"></div><div id="comment-50385-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

