+++
type = "question"
title = "From Godaddy"
description = '''I recently have been having issues with the speed of uploading files to an FTP. My site is hosted with GoDaddy. They sent me this link to follow and test. http://products.secureserver.net/hosting/wireshark/ When I try to follow the instructions on the link, I get to step #3, but I get an error that ...'''
date = "2011-05-10T19:15:00Z"
lastmod = "2011-05-10T19:39:00Z"
weight = 4025
keywords = [ "ftp", "godaddy" ]
aliases = [ "/questions/4025" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [From Godaddy](/questions/4025/from-godaddy)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4025-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4025-score" class="post-score" title="current number of votes">0</div><span id="post-4025-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I recently have been having issues with the speed of uploading files to an FTP. My site is hosted with GoDaddy. They sent me this link to follow and test. http://products.secureserver.net/hosting/wireshark/</p><p>When I try to follow the instructions on the link, I get to step #3, but I get an error that there are no interfaces available. How can I fix this so I can move on to the next step?</p><p>Thanks, D</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span> <span class="post-tag tag-link-godaddy" rel="tag" title="see questions tagged &#39;godaddy&#39;">godaddy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 May '11, 19:15</strong></p><img src="https://secure.gravatar.com/avatar/c01faafcf228fb68702e60c3be9e3b9e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="davel160&#39;s gravatar image" /><p><span>davel160</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="davel160 has no accepted answers">0%</span></p></div></div><div id="comments-container-4025" class="comments-container"></div><div id="comment-tools-4025" class="comment-tools"></div><div class="clear"></div><div id="comment-4025-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4026"></span>

<div id="answer-container-4026" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4026-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4026-score" class="post-score" title="current number of votes">0</div><span id="post-4026-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you're running on Mac OS X, see this <a href="http://ask.wireshark.org/questions/578/mac-os-cant-detect-any-interface">post</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 May '11, 19:39</strong></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="helloworld has 28 accepted answers">28%</span></p></div></div><div id="comments-container-4026" class="comments-container"></div><div id="comment-tools-4026" class="comment-tools"></div><div class="clear"></div><div id="comment-4026-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

