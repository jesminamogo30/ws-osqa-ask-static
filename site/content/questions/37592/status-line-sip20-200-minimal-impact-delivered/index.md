+++
type = "question"
title = "[closed] Status-Line: SIP/2.0 200 Minimal Impact - delivered"
description = '''I just saw this in response to a BYE: Status-Line: SIP/2.0 200 Minimal Impact - delivered What does it mean?'''
date = "2014-11-05T07:35:00Z"
lastmod = "2014-11-05T07:35:00Z"
weight = 37592
keywords = [ "200ok" ]
aliases = [ "/questions/37592" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Status-Line: SIP/2.0 200 Minimal Impact - delivered](/questions/37592/status-line-sip20-200-minimal-impact-delivered)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37592-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37592-score" class="post-score" title="current number of votes">0</div><span id="post-37592-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just saw this in response to a BYE: Status-Line: SIP/2.0 200 Minimal Impact - delivered What does it mean?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-200ok" rel="tag" title="see questions tagged &#39;200ok&#39;">200ok</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Nov '14, 07:35</strong></p><img src="https://secure.gravatar.com/avatar/73432f7a83d11c742ebd52ebb846e2a8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DrCohen&#39;s gravatar image" /><p><span>DrCohen</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DrCohen has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>05 Nov '14, 08:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-37592" class="comments-container"></div><div id="comment-tools-37592" class="comment-tools"></div><div class="clear"></div><div id="comment-37592-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 05 Nov '14, 08:53

</div>

</div>

</div>

