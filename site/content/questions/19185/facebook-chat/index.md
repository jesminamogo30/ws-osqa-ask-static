+++
type = "question"
title = "facebook chat"
description = '''Hi guys. I&#x27;ve sniffed some packet right now. What&#x27;s the filter to use to read facebook chat? thanks'''
date = "2013-03-05T13:55:00Z"
lastmod = "2015-02-28T16:14:00Z"
weight = 19185
keywords = [ "facebook" ]
aliases = [ "/questions/19185" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [facebook chat](/questions/19185/facebook-chat)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19185-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19185-score" class="post-score" title="current number of votes">0</div><span id="post-19185-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys. I've sniffed some packet right now. What's the filter to use to read facebook chat? thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-facebook" rel="tag" title="see questions tagged &#39;facebook&#39;">facebook</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '13, 13:55</strong></p><img src="https://secure.gravatar.com/avatar/4892f11d5ec0b05def85ac94b69e8107?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rax%20Ozen&#39;s gravatar image" /><p><span>Rax Ozen</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rax Ozen has no accepted answers">0%</span></p></div></div><div id="comments-container-19185" class="comments-container"></div><div id="comment-tools-19185" class="comment-tools"></div><div class="clear"></div><div id="comment-19185-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19187"></span>

<div id="answer-container-19187" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19187-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19187-score" class="post-score" title="current number of votes">0</div><span id="post-19187-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>AFIAK Facebook does a redirect to HTTPS, so the traffic is encrypted and you won't be able to read the communication. If you want to analyze HTTPS traffic (without access to the crypto keys) you need a plugin for the browser (e.g. <a href="https://addons.mozilla.org/de/firefox/addon/firebug/">Firebug</a>) and/or some ssl intercepting proxy (e.g. <a href="http://www.fiddler2.com/fiddler2/">Fiddler2</a>).</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Mar '13, 14:04</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Mar '13, 14:06</strong> </span></p></div></div><div id="comments-container-19187" class="comments-container"><span id="40156"></span><div id="comment-40156" class="comment"><div id="post-40156-score" class="comment-score"></div><div class="comment-text"><p>Hi Kurt. does this work? can you provide any more detail</p><p>The problem i have is that i think one of my children is getting themselves into something they really should not be. So that i know one way or the other i need to stealthily look at what is going on and if my fears are correct i can deal with that as stealthily!</p><p>They are using an android phone which i have no access to, any help greatly appreciated.</p></div><div id="comment-40156-info" class="comment-info"><span class="comment-age">(28 Feb '15, 16:14)</span> <span class="comment-user userinfo">new2geeky</span></div></div></div><div id="comment-tools-19187" class="comment-tools"></div><div class="clear"></div><div id="comment-19187-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

