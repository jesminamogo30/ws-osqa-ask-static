+++
type = "question"
title = "ZoneAlarm warning for 64-bit Windows installer"
description = '''Dear sirs, Zone alarm reports this file as a &#x27;zombie&#x27;. Regards C Gilbert'''
date = "2011-05-10T08:41:00Z"
lastmod = "2011-05-14T11:03:00Z"
weight = 4019
keywords = [ "zonealarm", "win64", "installer" ]
aliases = [ "/questions/4019" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [ZoneAlarm warning for 64-bit Windows installer](/questions/4019/zonealarm-warning-for-64-bit-windows-installer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4019-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4019-score" class="post-score" title="current number of votes">0</div><span id="post-4019-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear sirs,</p><p>Zone alarm reports this file as a 'zombie'.</p><p>Regards</p><p>C Gilbert</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-zonealarm" rel="tag" title="see questions tagged &#39;zonealarm&#39;">zonealarm</span> <span class="post-tag tag-link-win64" rel="tag" title="see questions tagged &#39;win64&#39;">win64</span> <span class="post-tag tag-link-installer" rel="tag" title="see questions tagged &#39;installer&#39;">installer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 May '11, 08:41</strong></p><img src="https://secure.gravatar.com/avatar/5ce4ca87827644143401b752d38f35b9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SwissBob&#39;s gravatar image" /><p><span>SwissBob</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SwissBob has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 May '11, 15:13</strong> </span></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span></p></div></div><div id="comments-container-4019" class="comments-container"><span id="4088"></span><div id="comment-4088" class="comment"><div id="post-4088-score" class="comment-score"></div><div class="comment-text"><p>Has ZoneAlarm updated itself since May 10? If so, is it still reporting a problem with the Wireshark installer?</p></div><div id="comment-4088-info" class="comment-info"><span class="comment-age">(14 May '11, 11:03)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-4019" class="comment-tools"></div><div class="clear"></div><div id="comment-4019-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="4020"></span>

<div id="answer-container-4020" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4020-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4020-score" class="post-score" title="current number of votes">0</div><span id="post-4020-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is a common false positive <a href="http://forum.zonealarm.com/showthread.php?t=74477&amp;highlight=zombie">reported</a> by ZoneAlarm users. The issue is most likely with ZoneAlarm, not the Wireshark installer.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 May '11, 09:19</strong></p><img src="https://secure.gravatar.com/avatar/aa651167cb1d51fa9dca1212f1123bfa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bstn&#39;s gravatar image" /><p><span>bstn</span><br />
<span class="score" title="375 reputation points">375</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bstn has 4 accepted answers">14%</span></p></div></div><div id="comments-container-4020" class="comments-container"></div><div id="comment-tools-4020" class="comment-tools"></div><div class="clear"></div><div id="comment-4020-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="4021"></span>

<div id="answer-container-4021" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4021-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4021-score" class="post-score" title="current number of votes">0</div><span id="post-4021-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Can you provide a few more details? Specifically:</p><ul><li>The exact error reported. Did ZoneAlarm trigger on the installer itself or one of the files contained in the installer?</li><li>The version of Wireshark.</li><li>The version of ZoneAlarm.</li><li>The version of ZoneAlarm's DAT file.</li></ul><p>If possible can you <a href="http://www.zonealarm.com/security/en-us/forms/spyware-reporting-form.htm">submit the installer to ZoneAlarm for manual verification</a>?</p><p>So far every antivirus warning we've encountered has been a <a href="http://wiki.wireshark.org/FalsePositives">false positive</a> but I'd like to verify that this is the case here as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 May '11, 09:32</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 May '11, 11:11</strong> </span></p></div></div><div id="comments-container-4021" class="comments-container"><span id="4022"></span><div id="comment-4022" class="comment"><div id="post-4022-score" class="comment-score"></div><div class="comment-text"><p>First ZA Extreme Security 9.3.037.000 reports it cannot verify the file.</p><p>The advanced scan reports:</p><p>wireshark-win64-1.4.6 is malicious, secondly that "The file is zombie software that silently terminates itself etc etc, can execute malicious commands"</p></div><div id="comment-4022-info" class="comment-info"><span class="comment-age">(10 May '11, 10:29)</span> <span class="comment-user userinfo">SwissBob</span></div></div></div><div id="comment-tools-4021" class="comment-tools"></div><div class="clear"></div><div id="comment-4021-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

