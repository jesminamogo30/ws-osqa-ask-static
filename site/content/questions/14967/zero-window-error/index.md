+++
type = "question"
title = "Zero window error"
description = '''I am getting a zero window error from a copier, I&#x27;m sorry 2 copiers and 1 printer, this did not occur before changes were made to the customers network. They replaced the nic in the printer to resolve the problem. my question is what could have caused the failure of the 3 nics if they are infact the...'''
date = "2012-10-12T11:58:00Z"
lastmod = "2012-10-13T10:25:00Z"
weight = 14967
keywords = [ "copier", "zero-window", "nic" ]
aliases = [ "/questions/14967" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Zero window error](/questions/14967/zero-window-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14967-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14967-score" class="post-score" title="current number of votes">0</div><span id="post-14967-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am getting a zero window error from a copier, I'm sorry 2 copiers and 1 printer, this did not occur before changes were made to the customers network. They replaced the nic in the printer to resolve the problem. my question is what could have caused the failure of the 3 nics if they are infact the cause of the zero window error<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-copier" rel="tag" title="see questions tagged &#39;copier&#39;">copier</span> <span class="post-tag tag-link-zero-window" rel="tag" title="see questions tagged &#39;zero-window&#39;">zero-window</span> <span class="post-tag tag-link-nic" rel="tag" title="see questions tagged &#39;nic&#39;">nic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Oct '12, 11:58</strong></p><img src="https://secure.gravatar.com/avatar/0b840538df3618c627e84f54bf238f85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aPauling&#39;s gravatar image" /><p><span>aPauling</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aPauling has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-14967" class="comments-container"></div><div id="comment-tools-14967" class="comment-tools"></div><div class="clear"></div><div id="comment-14967-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="14981"></span>

<div id="answer-container-14981" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14981-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14981-score" class="post-score" title="current number of votes">1</div><span id="post-14981-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I can send you a network analysis report from an IT company that says I need to replace the printers</p></blockquote><p>just an idea. Did they upgrade the network (where the printers are located) from 100 MBit to gigabit? That could cause an older printer to get "overwhelmed" by the amount of data coming through the gigabit network and thus leading to a full buffer problem much often than before (slow CPU, small buffer).</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Oct '12, 01:34</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-14981" class="comments-container"></div><div id="comment-tools-14981" class="comment-tools"></div><div class="clear"></div><div id="comment-14981-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="14971"></span>

<div id="answer-container-14971" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14971-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14971-score" class="post-score" title="current number of votes">0</div><span id="post-14971-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>"Zero Window" is not really an error, it is more of a "stop signal". You could argue that a signal like that is quite unwanted (just like an error) :-)</p><p>As I already commented in the older post you also asked about this I have seen printers use zero window signaling to slow down/stop transmission from print servers/workstations while they're busy waking up and spinning up their internal mechanical parts. When ready, the zero window is pulled up to signal that the sender may continue. It's like a "wait a second, need to get running first... okay, send".</p><p>Without a tracefile I can't check what is really happening, but it is kind of surprising that replacing a NIC would help here, since the TCP window signaling is not something that depends on the NIC, but the TCP stack.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Oct '12, 14:10</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-14971" class="comments-container"><span id="14973"></span><div id="comment-14973" class="comment"><div id="post-14973-score" class="comment-score"></div><div class="comment-text"><p>I can send you a network analysis report from an IT company that says I need to replace the printers</p></div><div id="comment-14973-info" class="comment-info"><span class="comment-age">(12 Oct '12, 15:13)</span> <span class="comment-user userinfo">aPauling</span></div></div></div><div id="comment-tools-14971" class="comment-tools"></div><div class="clear"></div><div id="comment-14971-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="14989"></span>

<div id="answer-container-14989" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14989-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14989-score" class="post-score" title="current number of votes">0</div><span id="post-14989-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As <span>@Jasper</span> said, zero window from printers as a signaling mechanism is fairly normal. As <span>@Kurt</span> said, a modern PC on a fast network can throw data at an older printer faster than the printer can accept it.</p><p>In all this discussion of zero window packets, there is one thing you haven't mentioned: Is printing working normally?</p><p>If the printers are working the way they always have, and you're happy with the print output speed, then zero window can be considered a cosmetic error and disregarded. If the printers are working the way they always have, but you're <em>not</em> happy with the print output speed, then you've outgrown these printers and you need to upgrade to newer, faster printers.</p><p>If, as we suspect, this is just a case of older, slower printers on a newer, faster network, then this is what you should see:</p><p>First, a fairly steady stream of data to the printer, but with the printer's remaining window size continuously decreasing as it receives data faster then it can process it. At some point, the printer's receive buffer will fill, it will send a zero window, and data transfer will stop. As the printer continues printing, it will keep pulling data from the receive buffer, and when there is room for more incoming data, it will send a window update and data transfer will resume. The data transfer will start and stop like this repeatedly until all the data has been transferred. However, the print output should run more or less continuously. Without Wireshark, you would be unable to tell that the data transfer starts and stops.</p><p>This process of data transfer stopping and starting--zero window followed by window update--is a normal part of TCP operation when a slower device with limited buffer space is receiving data from a faster device.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Oct '12, 10:25</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-14989" class="comments-container"></div><div id="comment-tools-14989" class="comment-tools"></div><div class="clear"></div><div id="comment-14989-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

