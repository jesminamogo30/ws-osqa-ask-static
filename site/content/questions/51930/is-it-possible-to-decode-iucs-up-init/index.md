+++
type = "question"
title = "Is it possible to decode IuCS-UP INIT?"
description = '''I tried to decode with wireshark IuCS-UP messages to find what Mode Version used by CN but was unsuccessful. Is it possible to decode IuCS-UP INIT? br, Murat'''
date = "2016-04-25T10:50:00Z"
lastmod = "2016-04-25T10:50:00Z"
weight = 51930
keywords = [ "iusc-up" ]
aliases = [ "/questions/51930" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to decode IuCS-UP INIT?](/questions/51930/is-it-possible-to-decode-iucs-up-init)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51930-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51930-score" class="post-score" title="current number of votes">0</div><span id="post-51930-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I tried to decode with wireshark IuCS-UP messages to find what Mode Version used by CN but was unsuccessful. Is it possible to decode IuCS-UP INIT?</p><p>br, Murat</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iusc-up" rel="tag" title="see questions tagged &#39;iusc-up&#39;">iusc-up</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '16, 10:50</strong></p><img src="https://secure.gravatar.com/avatar/bf3da28b352020f95f0961db3f35e4ae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zhumatay&#39;s gravatar image" /><p><span>Zhumatay</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zhumatay has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jul '16, 08:27</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-51930" class="comments-container"></div><div id="comment-tools-51930" class="comment-tools"></div><div class="clear"></div><div id="comment-51930-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

