+++
type = "question"
title = "Wireshark Capture time for packet in/out of eth"
description = '''I ran wireshark to listen to two network interfaces eth0 and eth1. Eth0 is assigned a public IP address where eth1 is assigned a private IP Address for local network. The VM functions as a Firewall/IDS, and the requirement here is to measure the processing time of each appliance.  Is there any way o...'''
date = "2017-02-17T14:05:00Z"
lastmod = "2017-02-17T14:05:00Z"
weight = 59518
keywords = [ "traffic-analysis", "packet-capture", "traffic", "network" ]
aliases = [ "/questions/59518" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Capture time for packet in/out of eth](/questions/59518/wireshark-capture-time-for-packet-inout-of-eth)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59518-score" class="post-score" title="current number of votes">0</div><span id="post-59518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I ran wireshark to listen to two network interfaces eth0 and eth1. Eth0 is assigned a public IP address where eth1 is assigned a private IP Address for local network. The VM functions as a Firewall/IDS, and the requirement here is to measure the processing time of each appliance.</p><p>Is there any way of capturing packets inside the FW and the timestamp of packets IN/OUT?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic-analysis" rel="tag" title="see questions tagged &#39;traffic-analysis&#39;">traffic-analysis</span> <span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '17, 14:05</strong></p><img src="https://secure.gravatar.com/avatar/44832c1ee7e9ff26685092163ab7d3c3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eronad&#39;s gravatar image" /><p><span>eronad</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eronad has no accepted answers">0%</span></p></div></div><div id="comments-container-59518" class="comments-container"></div><div id="comment-tools-59518" class="comment-tools"></div><div class="clear"></div><div id="comment-59518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

