+++
type = "question"
title = "Wireshark capture between virtual Hyper-V and Physical machine"
description = '''Hello everyone. I have setup a Hyper-V machine in a Windows Server 2012 Standard edition. The Hyper-V resides on a physical server version of Windows Server 2012. I use the physical machine for DHCP, DNS, AD, and so forth, a very clean system. I have installed Wireshark on the Hyper-V version. I wan...'''
date = "2014-04-11T14:19:00Z"
lastmod = "2014-04-11T14:19:00Z"
weight = 31761
keywords = [ "machine", "capture", "hyper", "physical", "to" ]
aliases = [ "/questions/31761" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark capture between virtual Hyper-V and Physical machine](/questions/31761/wireshark-capture-between-virtual-hyper-v-and-physical-machine)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31761-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31761-score" class="post-score" title="current number of votes">0</div><span id="post-31761-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone.</p><p>I have setup a Hyper-V machine in a Windows Server 2012 Standard edition. The Hyper-V resides on a physical server version of Windows Server 2012. I use the physical machine for DHCP, DNS, AD, and so forth, a very clean system. I have installed Wireshark on the Hyper-V version. I want to trace packets using Wireshark installed on my Hyper V version to the physical DNS machine. Is this possible to use another interface as the capture card. I tried to setup remote interface but I am not sure if this would be the way to go. Is what I am trying to do possible? If so can someone lead me in the correct direction to set this up. I am still a noob when it comes to Wireshark, so go easy on me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-machine" rel="tag" title="see questions tagged &#39;machine&#39;">machine</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-hyper" rel="tag" title="see questions tagged &#39;hyper&#39;">hyper</span> <span class="post-tag tag-link-physical" rel="tag" title="see questions tagged &#39;physical&#39;">physical</span> <span class="post-tag tag-link-to" rel="tag" title="see questions tagged &#39;to&#39;">to</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Apr '14, 14:19</strong></p><img src="https://secure.gravatar.com/avatar/ec0d4e751c395815341839ac5791ec34?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dschnizzy&#39;s gravatar image" /><p><span>dschnizzy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dschnizzy has no accepted answers">0%</span></p></div></div><div id="comments-container-31761" class="comments-container"></div><div id="comment-tools-31761" class="comment-tools"></div><div class="clear"></div><div id="comment-31761-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

