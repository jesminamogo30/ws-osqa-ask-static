+++
type = "question"
title = "No &quot;Resolve Name&quot; on 2.0.1 release"
description = '''I recently installed 2.0.1 version. I captured traffic network. When I go to TCP from package list, then Destination, I right click mouse button but I don&#x27;t see &quot;As Resolve Name&quot; option?'''
date = "2016-02-19T09:28:00Z"
lastmod = "2016-02-19T09:43:00Z"
weight = 50345
keywords = [ "wireshark.2.0.1" ]
aliases = [ "/questions/50345" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No "Resolve Name" on 2.0.1 release](/questions/50345/no-resolve-name-on-201-release)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50345-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50345-score" class="post-score" title="current number of votes">0</div><span id="post-50345-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I recently installed 2.0.1 version. I captured traffic network. When I go to TCP from package list, then Destination, I right click mouse button but I don't see "As Resolve Name" option?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark.2.0.1" rel="tag" title="see questions tagged &#39;wireshark.2.0.1&#39;">wireshark.2.0.1</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '16, 09:28</strong></p><img src="https://secure.gravatar.com/avatar/26b149596a364ccfc9aebae03ca3a9c7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vanvo&#39;s gravatar image" /><p><span>vanvo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vanvo has no accepted answers">0%</span></p></div></div><div id="comments-container-50345" class="comments-container"></div><div id="comment-tools-50345" class="comment-tools"></div><div class="clear"></div><div id="comment-50345-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50346"></span>

<div id="answer-container-50346" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50346-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50346-score" class="post-score" title="current number of votes">0</div><span id="post-50346-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not yet done for the Qt version. See the Wiki page <a href="https://wiki.wireshark.org/Development/QtShark">QtShark</a> for info on what menu items are still to be ported over.</p><p>If on Windows, you'll also have the legacy version available, Wireshark Legacy (or wireshark-gtk.exe) if you really need the older UI.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Feb '16, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-50346" class="comments-container"></div><div id="comment-tools-50346" class="comment-tools"></div><div class="clear"></div><div id="comment-50346-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

