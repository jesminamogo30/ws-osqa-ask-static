+++
type = "question"
title = "what does mean &quot;POTENTIAL BROWSER&quot;"
description = '''sometimes wireshark had capture packets telling that one host is &quot;potential browser&quot; what does it mean?'''
date = "2015-09-07T08:51:00Z"
lastmod = "2015-09-08T08:13:00Z"
weight = 45667
keywords = [ "hosts", "browser" ]
aliases = [ "/questions/45667" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [what does mean "POTENTIAL BROWSER"](/questions/45667/what-does-mean-potential-browser)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45667-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45667-score" class="post-score" title="current number of votes">0</div><span id="post-45667-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>sometimes wireshark had capture packets telling that one host is "potential browser" what does it mean?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hosts" rel="tag" title="see questions tagged &#39;hosts&#39;">hosts</span> <span class="post-tag tag-link-browser" rel="tag" title="see questions tagged &#39;browser&#39;">browser</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Sep '15, 08:51</strong></p><img src="https://secure.gravatar.com/avatar/cd87e170a7a2ddbf79b06c3fb8b0c141?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jicelle&#39;s gravatar image" /><p><span>Jicelle</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jicelle has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Sep '15, 08:50</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-45667" class="comments-container"><span id="45675"></span><div id="comment-45675" class="comment"><div id="post-45675-score" class="comment-score"></div><div class="comment-text"><p>I can't find that string in the source code. Can you please provide a screenshot of that message?</p></div><div id="comment-45675-info" class="comment-info"><span class="comment-age">(07 Sep '15, 16:18)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="45692"></span><div id="comment-45692" class="comment"><div id="post-45692-score" class="comment-score"></div><div class="comment-text"><p>I guess it's 'potential browser' ?</p></div><div id="comment-45692-info" class="comment-info"><span class="comment-age">(07 Sep '15, 23:32)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-45667" class="comment-tools"></div><div class="clear"></div><div id="comment-45667-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45696"></span>

<div id="answer-container-45696" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45696-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45696-score" class="post-score" title="current number of votes">0</div><span id="post-45696-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Jicelle has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I guess it's 'potential browser' ?</p></blockquote><p>sounds better ;-)</p><p>In that case, <span>@Jicelle</span>, please read the following to learn how a 'potential browser' is defined.</p><blockquote><p><a href="https://technet.microsoft.com/en-us/library/cc737661(v=ws.10).aspx">https://technet.microsoft.com/en-us/library/cc737661(v=ws.10).aspx</a><br />
<a href="http://www.onlamp.com/pub/a/onlamp/excerpt/samba_chap7/index2.html">http://www.onlamp.com/pub/a/onlamp/excerpt/samba_chap7/index2.html</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Sep '15, 05:01</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-45696" class="comments-container"><span id="45699"></span><div id="comment-45699" class="comment"><div id="post-45699-score" class="comment-score"></div><div class="comment-text"><p>that's right it's 'potential browser' thank you very much <a href="https://ask.wireshark.org/users/2593/kurt-knochner"></a><span>@Kurt Knochner</span></p></div><div id="comment-45699-info" class="comment-info"><span class="comment-age">(08 Sep '15, 06:50)</span> <span class="comment-user userinfo">Jicelle</span></div></div><span id="45705"></span><div id="comment-45705" class="comment"><div id="post-45705-score" class="comment-score"></div><div class="comment-text"><p>Actually it was <span>@Jaap</span> who figured that out ;-) So, your thanks should go to him...</p></div><div id="comment-45705-info" class="comment-info"><span class="comment-age">(08 Sep '15, 08:13)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-45696" class="comment-tools"></div><div class="clear"></div><div id="comment-45696-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

