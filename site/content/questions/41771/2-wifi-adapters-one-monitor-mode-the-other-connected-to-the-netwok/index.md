+++
type = "question"
title = "2 wifi adapters one monitor mode,  the other connected to the netwok"
description = '''I&#x27;m wondering if it&#x27;s possible to have one wifi adapter connected to the network and another wifi adapter in monitor mode/airmon-ng.   I have one PC in my house on my network that i am using to connect too with teamviewer. Connected by a cheap WiFi Mini WLAN 802.11n/g/b. for the second wifi adapter ...'''
date = "2015-04-24T03:39:00Z"
lastmod = "2015-04-24T03:39:00Z"
weight = 41771
keywords = [ "airmon-ng", "monitor-mode" ]
aliases = [ "/questions/41771" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [2 wifi adapters one monitor mode, the other connected to the netwok](/questions/41771/2-wifi-adapters-one-monitor-mode-the-other-connected-to-the-netwok)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41771-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41771-score" class="post-score" title="current number of votes">0</div><span id="post-41771-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm wondering if it's possible to have one wifi adapter connected to the network and another wifi adapter in monitor mode/airmon-ng.<br />
</p><p>I have one PC in my house on my network that i am using to connect too with teamviewer. Connected by a cheap WiFi Mini WLAN 802.11n/g/b. for the second wifi adapter on that pc i have the Alfa 1000mW Wifi AWUS036H. Ultimately what im trying to do is to keep the connection to the network so i can remotely control it without loosing the connection due to the service network-manager down command. can anyone share with me how this works and or have some sort of solution as to how this is done. If it's even possible. Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-airmon-ng" rel="tag" title="see questions tagged &#39;airmon-ng&#39;">airmon-ng</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '15, 03:39</strong></p><img src="https://secure.gravatar.com/avatar/3a819cbd9a2b0dbb90ef1bc177da51f3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yipsy&#39;s gravatar image" /><p><span>Yipsy</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yipsy has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-41771" class="comments-container"></div><div id="comment-tools-41771" class="comment-tools"></div><div class="clear"></div><div id="comment-41771-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

