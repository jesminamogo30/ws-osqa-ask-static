+++
type = "question"
title = "Find password for Linksys spa2102"
description = '''Does anybody have any idea how I can find the password for a Linksys spa2102. I have tried to use the spa2102-r sniff.pcap. and the sniff w static ip.pcap. but they have not shown the user name and password. I am using a new fritzbox router so would this be stopping me from having access to this inf...'''
date = "2014-09-03T10:04:00Z"
lastmod = "2014-09-03T10:04:00Z"
weight = 35966
keywords = [ "spa2102", "linksys" ]
aliases = [ "/questions/35966" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Find password for Linksys spa2102](/questions/35966/find-password-for-linksys-spa2102)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35966-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35966-score" class="post-score" title="current number of votes">0</div><span id="post-35966-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does anybody have any idea how I can find the password for a Linksys spa2102. I have tried to use the spa2102-r sniff.pcap. and the sniff w static ip.pcap. but they have not shown the user name and password. I am using a new fritzbox router so would this be stopping me from having access to this information. I would be grateful if anybody could tell how to set up the wireshark to find this data for me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-spa2102" rel="tag" title="see questions tagged &#39;spa2102&#39;">spa2102</span> <span class="post-tag tag-link-linksys" rel="tag" title="see questions tagged &#39;linksys&#39;">linksys</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '14, 10:04</strong></p><img src="https://secure.gravatar.com/avatar/befd5769464ed8ff459b3581d9d07d52?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ducmon&#39;s gravatar image" /><p><span>ducmon</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ducmon has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Sep '14, 16:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-35966" class="comments-container"></div><div id="comment-tools-35966" class="comment-tools"></div><div class="clear"></div><div id="comment-35966-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

