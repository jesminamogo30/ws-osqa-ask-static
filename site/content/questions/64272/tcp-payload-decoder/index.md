+++
type = "question"
title = "[closed] TCP payload decoder"
description = '''Dear wireshark help team, I have a TCP payload packet and I would like to decode the data that is in there, how do I do this ?  The data in ASCII format is as follows: ScEn@XhVPk;uPe|&#x27;?f(A/:($N_C4;G4xc!X4F&amp;gt;@2w3 Thanks in advance.'''
date = "2017-10-27T03:21:00Z"
lastmod = "2017-10-27T05:12:00Z"
weight = 64272
keywords = [ "decode" ]
aliases = [ "/questions/64272" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] TCP payload decoder](/questions/64272/tcp-payload-decoder)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64272-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64272-score" class="post-score" title="current number of votes">0</div><span id="post-64272-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear wireshark help team,</p><p>I have a TCP payload packet and I would like to decode the data that is in there, how do I do this ?</p><p>The data in ASCII format is as follows: <span class="__cf_email__" data-cfemail="b2e1d1f7dcf2eadae4e2d9">[email protected]</span>;uPe|'?f(A/:($N_C4;G4xc!X4F&gt;@2w3</p><p>Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Oct '17, 03:21</strong></p><img src="https://secure.gravatar.com/avatar/235d9d30ff6ba8c12a67c760609b8644?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ToasTer86&#39;s gravatar image" /><p><span>ToasTer86</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ToasTer86 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>27 Oct '17, 05:13</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-64272" class="comments-container"><span id="64273"></span><div id="comment-64273" class="comment"><div id="post-64273-score" class="comment-score"></div><div class="comment-text"><p>I doubt we can help you here - to decode TCP payloads you should at least know what protocol this is. If it's anything using SSL/TLS -&gt; forget it. If its proprietary -&gt; forget it.</p></div><div id="comment-64273-info" class="comment-info"><span class="comment-age">(27 Oct '17, 03:38)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="64274"></span><div id="comment-64274" class="comment"><div id="post-64274-score" class="comment-score"></div><div class="comment-text"><p>it is just HTTP I am sending out, but I think my embedded configuration is not correct, thats why it is jibberish.</p></div><div id="comment-64274-info" class="comment-info"><span class="comment-age">(27 Oct '17, 03:40)</span> <span class="comment-user userinfo">ToasTer86</span></div></div><span id="64275"></span><div id="comment-64275" class="comment"><div id="post-64275-score" class="comment-score"></div><div class="comment-text"><p>"jibberish". So how would you expect this data to be 'decoded' then?</p></div><div id="comment-64275-info" class="comment-info"><span class="comment-age">(27 Oct '17, 04:06)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="64279"></span><div id="comment-64279" class="comment"><div id="post-64279-score" class="comment-score"></div><div class="comment-text"><p>Well I just figured out that the messages was not encoded but that what I am sending is not correct, so sorry for posting this question.</p></div><div id="comment-64279-info" class="comment-info"><span class="comment-age">(27 Oct '17, 04:48)</span> <span class="comment-user userinfo">ToasTer86</span></div></div><span id="64280"></span><div id="comment-64280" class="comment"><div id="post-64280-score" class="comment-score"></div><div class="comment-text"><p>Okay, thanks for your clarification.</p></div><div id="comment-64280-info" class="comment-info"><span class="comment-age">(27 Oct '17, 05:12)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-64272" class="comment-tools"></div><div class="clear"></div><div id="comment-64272-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Problem is not reproducible or outdated" by Jaap 27 Oct '17, 05:12

</div>

</div>

</div>

