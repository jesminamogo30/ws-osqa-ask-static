+++
type = "question"
title = "installing offline"
description = '''hi, i hv downloaded wireshark source code and libraray files and skipped nmake -f makefile.nmake setup as i did download the libraries myself , but wen i tried to run &quot;nmake -f makefile.nmake all&quot; i am getting the error as dont know how to stop match686.onj does anybody has any idea on this, plase h...'''
date = "2011-06-03T00:21:00Z"
lastmod = "2011-06-03T17:43:00Z"
weight = 4358
keywords = [ "source", "install" ]
aliases = [ "/questions/4358" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [installing offline](/questions/4358/installing-offline)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4358-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4358-score" class="post-score" title="current number of votes">0</div><span id="post-4358-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, i hv downloaded wireshark source code and libraray files and skipped nmake -f makefile.nmake setup as i did download the libraries myself , but wen i tried to run "<strong>nmake -f makefile.nmake all</strong>" i am getting the error as <strong>dont know how to stop match686.onj</strong> does anybody has any idea on this, plase help to resolve this issue.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-source" rel="tag" title="see questions tagged &#39;source&#39;">source</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jun '11, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/257c9f9e498193d7ddde57090efe094a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sagu072&#39;s gravatar image" /><p><span>sagu072</span><br />
<span class="score" title="35 reputation points">35</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sagu072 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Jun '11, 07:37</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-4358" class="comments-container"></div><div id="comment-tools-4358" class="comment-tools"></div><div class="clear"></div><div id="comment-4358-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4359"></span>

<div id="answer-container-4359" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4359-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4359-score" class="post-score" title="current number of votes">2</div><span id="post-4359-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can't skip steps in the build procedure! It's great that you've downloaded the libraries yourself, but they have to be unpacked and prepared before you can build.</p><p>Please follow the build steps unless you really know what you're doing. The fact you've to ask here begs to differ.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jun '11, 02:50</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-4359" class="comments-container"><span id="4360"></span><div id="comment-4360" class="comment"><div id="post-4360-score" class="comment-score"></div><div class="comment-text"><p>i have unpacked the files to path according to the config and makefile.nmake file, after that its giving error, once i hv all the libraries i thought its not necessary to download them again.</p></div><div id="comment-4360-info" class="comment-info"><span class="comment-age">(03 Jun '11, 03:16)</span> <span class="comment-user userinfo">sagu072</span></div></div><span id="4369"></span><div id="comment-4369" class="comment"><div id="post-4369-score" class="comment-score">1</div><div class="comment-text"><p>Did you run "nmake -f makefile.nmake setup"? If not, do so, <em>REGARDLESS</em> of whether you've downloaded the libraries yourself. We don't guarantee that the build process will work if you customize it by, for example, downloading the libraries yourself; if you want to do the build your way, rather than our way, you'll have to solve build problems yourself.</p></div><div id="comment-4369-info" class="comment-info"><span class="comment-age">(03 Jun '11, 17:43)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-4359" class="comment-tools"></div><div class="clear"></div><div id="comment-4359-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

