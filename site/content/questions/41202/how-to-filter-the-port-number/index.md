+++
type = "question"
title = "How to filter the port number"
description = '''Hi i search the port number that is different each other. i should measure the port number per 10 seconds using the IO graph but, i don&#x27;t know how to write the filtering section in IO graph. please help!'''
date = "2015-04-05T21:33:00Z"
lastmod = "2015-04-06T03:07:00Z"
weight = 41202
keywords = [ "analyze" ]
aliases = [ "/questions/41202" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to filter the port number](/questions/41202/how-to-filter-the-port-number)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41202-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41202-score" class="post-score" title="current number of votes">0</div><span id="post-41202-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi i search the port number that is different each other. i should measure the port number per 10 seconds using the IO graph but, i don't know how to write the filtering section in IO graph.</p><p>please help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analyze" rel="tag" title="see questions tagged &#39;analyze&#39;">analyze</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Apr '15, 21:33</strong></p><img src="https://secure.gravatar.com/avatar/89da163c13027a6536ccdc883adead9e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Soong&#39;s gravatar image" /><p><span>Soong</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Soong has no accepted answers">0%</span></p></div></div><div id="comments-container-41202" class="comments-container"><span id="41208"></span><div id="comment-41208" class="comment"><div id="post-41208-score" class="comment-score"></div><div class="comment-text"><p>can you please add more details, as it's unclear what you are asking for. An example might help to understand what you mean by: "I search the port number that <strong>is different each other</strong>."</p></div><div id="comment-41208-info" class="comment-info"><span class="comment-age">(06 Apr '15, 03:07)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-41202" class="comment-tools"></div><div class="clear"></div><div id="comment-41202-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

