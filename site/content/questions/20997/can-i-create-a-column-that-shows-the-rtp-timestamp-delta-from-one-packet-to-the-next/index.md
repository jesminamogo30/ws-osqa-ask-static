+++
type = "question"
title = "Can I create a column that shows the RTP Timestamp delta from one packet to the next"
description = '''I&#x27;d like to create a column that shows the change in time stamps from one RTP packet to the next. Is this possible in wireshark? This is a screenshot of the RTP timestamp column I would like another column that displays the difference between these values. So in this case, the values in the new colu...'''
date = "2013-05-06T17:15:00Z"
lastmod = "2013-05-06T17:15:00Z"
weight = 20997
keywords = [ "timestamp", "rtp" ]
aliases = [ "/questions/20997" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can I create a column that shows the RTP Timestamp delta from one packet to the next](/questions/20997/can-i-create-a-column-that-shows-the-rtp-timestamp-delta-from-one-packet-to-the-next)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20997-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20997-score" class="post-score" title="current number of votes">0</div><span id="post-20997-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'd like to create a column that shows the change in time stamps from one RTP packet to the next. Is this possible in wireshark?</p><p><a href="http://pasteboard.co/2ePt9Qw4.png">This is a screenshot of the RTP timestamp column</a></p><p>I would like another column that displays the difference between these values. So in this case, the values in the new column would be 160.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamp" rel="tag" title="see questions tagged &#39;timestamp&#39;">timestamp</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 May '13, 17:15</strong></p><img src="https://secure.gravatar.com/avatar/d7951e5f68ad9562c47212f4e651edda?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="carleeto&#39;s gravatar image" /><p><span>carleeto</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="carleeto has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 May '13, 17:17</strong> </span></p></div></div><div id="comments-container-20997" class="comments-container"></div><div id="comment-tools-20997" class="comment-tools"></div><div class="clear"></div><div id="comment-20997-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

