+++
type = "question"
title = "Mobile packets not able to be captured in wireshark"
description = '''Hi,I have installed Wire shark in my system.The system is connected to a router and my mobile is connected to the same router via WiFi.An evaluation board is also connected to the router via Ethernet.An application from my mobile will be sending packets to the evaluation board.But i could not captur...'''
date = "2013-06-26T22:32:00Z"
lastmod = "2013-06-27T02:24:00Z"
weight = 22387
keywords = [ "mobile", "capture", "router", "wifi", "wireshark" ]
aliases = [ "/questions/22387" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Mobile packets not able to be captured in wireshark](/questions/22387/mobile-packets-not-able-to-be-captured-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22387-score" class="post-score" title="current number of votes">0</div><span id="post-22387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,I have installed Wire shark in my system.The system is connected to a router and my mobile is connected to the same router via WiFi.An evaluation board is also connected to the router via Ethernet.An application from my mobile will be sending packets to the evaluation board.But i could not capture those packets send from the mobile.Please help out.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jun '13, 22:32</strong></p><img src="https://secure.gravatar.com/avatar/31ce65441be1fd185623891cde49b52a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jithin&#39;s gravatar image" /><p><span>jithin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jithin has no accepted answers">0%</span></p></div></div><div id="comments-container-22387" class="comments-container"></div><div id="comment-tools-22387" class="comment-tools"></div><div class="clear"></div><div id="comment-22387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22396"></span>

<div id="answer-container-22396" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22396-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22396-score" class="post-score" title="current number of votes">0</div><span id="post-22396-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the Wiki page on <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">Ethernet capturing setup</a>, and pay attention to the bit about switched networks.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '13, 02:24</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-22396" class="comments-container"></div><div id="comment-tools-22396" class="comment-tools"></div><div class="clear"></div><div id="comment-22396-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

