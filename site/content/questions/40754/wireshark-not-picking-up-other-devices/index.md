+++
type = "question"
title = "Wireshark not picking up other devices."
description = '''I have installed wireshark on my MacBook Pro. It runs fine expect for the fact it&#x27;s only picking up my MacBook, not any of the other devices running through my router. I saw an answer for this involving switches on the FAQ section. But, I do not understand any of it. Would there be a way that someon...'''
date = "2015-03-20T14:06:00Z"
lastmod = "2015-03-22T06:34:00Z"
weight = 40754
keywords = [ "wireless", "router", "netgear" ]
aliases = [ "/questions/40754" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not picking up other devices.](/questions/40754/wireshark-not-picking-up-other-devices)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40754-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40754-score" class="post-score" title="current number of votes">0</div><span id="post-40754-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have installed wireshark on my MacBook Pro. It runs fine expect for the fact it's only picking up my MacBook, not any of the other devices running through my router. I saw an answer for this involving switches on the FAQ section. But, I do not understand any of it. Would there be a way that someone could help me in basic terms. I am a bit over my head here. My router is a netgear n900. Thanks so much for any help.<br />
</p><p>Kylabgeek.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-netgear" rel="tag" title="see questions tagged &#39;netgear&#39;">netgear</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '15, 14:06</strong></p><img src="https://secure.gravatar.com/avatar/d4b066fa77ebc786b3dd193d272fc74c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kylabgeek&#39;s gravatar image" /><p><span>Kylabgeek</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kylabgeek has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-40754" class="comments-container"></div><div id="comment-tools-40754" class="comment-tools"></div><div class="clear"></div><div id="comment-40754-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40767"></span>

<div id="answer-container-40767" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40767-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40767-score" class="post-score" title="current number of votes">0</div><span id="post-40767-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I assume you are using a wireless connection. Your interface can either actively participate in a conversation (managed mode) or listen to all the conversations on a specific channel (monitor mode). If you want to know how to enable monitor mode on Mac OS X please read <a href="https://wiki.wireshark.org/CaptureSetup/WLAN#Mac_OS_X">this</a>. After you accomplished the previous step you will see a lot of wireless packets (management, control, data) and the data packets will be encrypted (I hope so). Since you know the password of your AP you can type it in Wireshark by following <a href="https://wiki.wireshark.org/HowToDecrypt802.11">this</a> guide. What you do next with the decrypted traffic is up to you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Mar '15, 06:34</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div></div><div id="comments-container-40767" class="comments-container"></div><div id="comment-tools-40767" class="comment-tools"></div><div class="clear"></div><div id="comment-40767-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

