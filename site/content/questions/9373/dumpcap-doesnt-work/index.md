+++
type = "question"
title = "dumpcap doesn&#x27;t work"
description = '''hi! i&#x27;m korean. i wonder you can understand what i&#x27;m saying.. i&#x27;m installing the wireshark program based on window 7. when i&#x27;m staring this program, windows error message is showed by dumpcap error. i don&#x27;t know this problem. please answer my question to resolve it.. thank you'''
date = "2012-03-05T17:15:00Z"
lastmod = "2012-03-05T21:25:00Z"
weight = 9373
keywords = [ "dumpcap", "error" ]
aliases = [ "/questions/9373" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [dumpcap doesn't work](/questions/9373/dumpcap-doesnt-work)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9373-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9373-score" class="post-score" title="current number of votes">0</div><span id="post-9373-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi! i'm korean. i wonder you can understand what i'm saying.. i'm installing the wireshark program based on window 7. when i'm staring this program, windows error message is showed by dumpcap error. i don't know this problem. please answer my question to resolve it.. thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '12, 17:15</strong></p><img src="https://secure.gravatar.com/avatar/a37d6faec59a841457dc538119849c77?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Young&#39;s gravatar image" /><p><span>Young</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Young has no accepted answers">0%</span></p></div></div><div id="comments-container-9373" class="comments-container"><span id="9384"></span><div id="comment-9384" class="comment"><div id="post-9384-score" class="comment-score"></div><div class="comment-text"><p>What does the error message say? It might indicate why dumpcap failed; we'd have to know exactly what happened in order to help.</p></div><div id="comment-9384-info" class="comment-info"><span class="comment-age">(05 Mar '12, 21:25)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-9373" class="comment-tools"></div><div class="clear"></div><div id="comment-9373-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

