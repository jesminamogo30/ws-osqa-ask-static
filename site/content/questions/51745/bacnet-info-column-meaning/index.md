+++
type = "question"
title = "BACnet info column meaning"
description = '''What is the meaning of &quot;Unconfirmed-REQ i-Am device, 570042&quot;? Can anyone explains this to me? I have been searching for it online, but couldn&#x27;t get an answer to it. Thank you and appreciate it.'''
date = "2016-04-18T02:11:00Z"
lastmod = "2016-04-18T09:29:00Z"
weight = 51745
keywords = [ "col_info", "bacnet" ]
aliases = [ "/questions/51745" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [BACnet info column meaning](/questions/51745/bacnet-info-column-meaning)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51745-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51745-score" class="post-score" title="current number of votes">0</div><span id="post-51745-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What is the meaning of "Unconfirmed-REQ i-Am device, 570042"?</p><p>Can anyone explains this to me? I have been searching for it online, but couldn't get an answer to it.</p><p>Thank you and appreciate it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-col_info" rel="tag" title="see questions tagged &#39;col_info&#39;">col_info</span> <span class="post-tag tag-link-bacnet" rel="tag" title="see questions tagged &#39;bacnet&#39;">bacnet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '16, 02:11</strong></p><img src="https://secure.gravatar.com/avatar/61fdd46eabae505cde8b585db3df146c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kingsley&#39;s gravatar image" /><p><span>kingsley</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kingsley has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Apr '16, 09:30</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-51745" class="comments-container"><span id="51754"></span><div id="comment-51754" class="comment"><div id="post-51754-score" class="comment-score"></div><div class="comment-text"><p>You will need to supply much more information that this if you expect anyone to have any chance at being able to help you. If you can post a capture file somewhere (cloudshark, dropbox, ...), then that might be your best chance at getting some help. A screenshot <em>might</em> be enough.</p></div><div id="comment-51754-info" class="comment-info"><span class="comment-age">(18 Apr '16, 08:31)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-51745" class="comment-tools"></div><div class="clear"></div><div id="comment-51745-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51757"></span>

<div id="answer-container-51757" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51757-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51757-score" class="post-score" title="current number of votes">0</div><span id="post-51757-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well, as far as I can tell, this question pertains to the BACnet protocol. Here are a few resources that may or may not help you better understand the protocol:</p><ul><li><a href="https://wiki.wireshark.org/Protocols/bacnet">https://wiki.wireshark.org/Protocols/bacnet</a></li><li><a href="https://en.wikipedia.org/wiki/BACnet">https://en.wikipedia.org/wiki/BACnet</a></li><li><a href="http://www.bacnet.org/">http://www.bacnet.org/</a></li></ul></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Apr '16, 09:29</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-51757" class="comments-container"></div><div id="comment-tools-51757" class="comment-tools"></div><div class="clear"></div><div id="comment-51757-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

