+++
type = "question"
title = "How do I add a pdml tool that could benefit others"
description = '''I have written an app in VB.Net that can carry out two functions: 1) Create a raw binary of all the payload data contained in a .pdml file 2) Create a text file with the specific field data from all packets in a .pdml file by specifying the search string. The application has been added to sourceforg...'''
date = "2012-09-13T02:25:00Z"
lastmod = "2012-09-13T02:25:00Z"
weight = 14228
keywords = [ "pdml", "export" ]
aliases = [ "/questions/14228" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I add a pdml tool that could benefit others](/questions/14228/how-do-i-add-a-pdml-tool-that-could-benefit-others)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14228-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14228-score" class="post-score" title="current number of votes">0</div><span id="post-14228-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have written an app in <a href="http://VB.Net">VB.Net</a> that can carry out two functions:</p><p>1) Create a raw binary of all the payload data contained in a .pdml file 2) Create a text file with the specific field data from all packets in a .pdml file by specifying the search string.</p><p>The application has been added to sourceforge at the following address:</p><p><a href="http://sourceforge.net/projects/pdmlpayloadcopy/">link text</a> <a href="http://sourceforge.net/projects/pdmlpayloadcopy">http://sourceforge.net/projects/pdmlpayloadcopy</a></p><p>It was instrumental in decoding a UDP stream contained raw PCM data, hopefully others will find it useful too.</p><p>The description from the sourceforge page is as follows:</p><hr /><p>I required a method of saving the UDP packet stream from a .pdml file created using Wireshark. I could find no method in Wireshark to export the data bytes from more than one packet, a online search was also unsuccessful. The UDP packets contained an audio stream in raw PCM format.</p><p>As a result I have created the pdml payload copy application using <a href="http://VB.Net">VB.Net</a> (V2.0) which stores all the hex data in the .pdml <a href="http://file.to">file.to</a> a new file. Using the program I was able to load the raw data file created into Audacity to playback the transmitted audio.</p><p>I have successfully used the application to analyse a .pdml file containing over 20,000 packets (over 300Mb in size). The application can store the byte data for a maximum of 100,000 packets.</p><p>Version 1.1 (12/09/12) Added option to select any field contained in .pdml file (enter required search string), which will output the requested data in text format.</p><p>Requires <a href="http://VB.Net">VB.Net</a> 2.0 runtime files.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pdml" rel="tag" title="see questions tagged &#39;pdml&#39;">pdml</span> <span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Sep '12, 02:25</strong></p><img src="https://secure.gravatar.com/avatar/aca92b1346da932886779daed561c95f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="time2innov8&#39;s gravatar image" /><p><span>time2innov8</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="time2innov8 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Sep '12, 02:26</strong> </span></p></div></div><div id="comments-container-14228" class="comments-container"></div><div id="comment-tools-14228" class="comment-tools"></div><div class="clear"></div><div id="comment-14228-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

