+++
type = "question"
title = "WireShark bundle !without! WinPCAP"
description = '''Dear Developers, Is the WireShark installer officially available without WinPCap and other 3rd party libraries for download? We would like to introduce the tool at the company but without the capturing functionality. We only need it to open and analyze PCAP files. Thank you, Attila'''
date = "2012-12-12T08:28:00Z"
lastmod = "2012-12-12T08:45:00Z"
weight = 16812
keywords = [ "winpcap", "without", "wireshark" ]
aliases = [ "/questions/16812" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark bundle !without! WinPCAP](/questions/16812/wireshark-bundle-without-winpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16812-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16812-score" class="post-score" title="current number of votes">0</div><span id="post-16812-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Developers,</p><p>Is the WireShark installer officially available without WinPCap and other 3rd party libraries for download? We would like to introduce the tool at the company but without the capturing functionality. We only need it to open and analyze PCAP files.</p><p>Thank you, Attila</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-without" rel="tag" title="see questions tagged &#39;without&#39;">without</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Dec '12, 08:28</strong></p><img src="https://secure.gravatar.com/avatar/5cb5d4debcf6cfd9cfcf5f601fe10f20?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nemmas&#39;s gravatar image" /><p><span>nemmas</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nemmas has no accepted answers">0%</span></p></div></div><div id="comments-container-16812" class="comments-container"></div><div id="comment-tools-16812" class="comment-tools"></div><div class="clear"></div><div id="comment-16812-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16816"></span>

<div id="answer-container-16816" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16816-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16816-score" class="post-score" title="current number of votes">1</div><span id="post-16816-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please check the portable version of Wireshark. After you unpacked it, you should be able to customize it to your needs.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Dec '12, 08:45</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Dec '12, 08:46</strong> </span></p></div></div><div id="comments-container-16816" class="comments-container"></div><div id="comment-tools-16816" class="comment-tools"></div><div class="clear"></div><div id="comment-16816-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

