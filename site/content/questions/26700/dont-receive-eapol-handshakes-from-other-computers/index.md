+++
type = "question"
title = "Don&#x27;t receive eapol handshakes from other computers"
description = '''Hello! I am using wireshark at MacOSX with a Tenda W311M (802.11n) on a WPA2 network. I can see the traffic from my test system (like UDP), but I don&#x27;t monitor any eapol hand shakes (except for my own two way handshakes that come along time to time). I can see the gratuitious arp when I connect anot...'''
date = "2013-11-06T15:07:00Z"
lastmod = "2013-11-06T15:07:00Z"
weight = 26700
keywords = [ "eapol", "macosx", "wpa2", "handshake" ]
aliases = [ "/questions/26700" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Don't receive eapol handshakes from other computers](/questions/26700/dont-receive-eapol-handshakes-from-other-computers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26700-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26700-score" class="post-score" title="current number of votes">0</div><span id="post-26700-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello!</p><p>I am using wireshark at MacOSX with a Tenda W311M (802.11n) on a WPA2 network. I can see the traffic from my test system (like UDP), but I don't monitor any eapol hand shakes (except for my own two way handshakes that come along time to time). I can see the gratuitious arp when I connect another computer, everything as expected, but no eapol hand shakes... Of course I set the the WPA Key in wireshark.</p><p>Thanks for any help! Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-eapol" rel="tag" title="see questions tagged &#39;eapol&#39;">eapol</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span> <span class="post-tag tag-link-handshake" rel="tag" title="see questions tagged &#39;handshake&#39;">handshake</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '13, 15:07</strong></p><img src="https://secure.gravatar.com/avatar/b97f6d2d52bff3777f77d8207e9ccd88?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Motzart&#39;s gravatar image" /><p><span>Motzart</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Motzart has no accepted answers">0%</span></p></div></div><div id="comments-container-26700" class="comments-container"></div><div id="comment-tools-26700" class="comment-tools"></div><div class="clear"></div><div id="comment-26700-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

