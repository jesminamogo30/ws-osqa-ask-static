+++
type = "question"
title = "RTP definition of forward (fwd) and reverse (rev)?"
description = '''Hi,  Please can you tell me what is the definition of forward and reverse with regards to RTP analysis?  Cheers Femto.'''
date = "2012-09-14T03:32:00Z"
lastmod = "2012-09-16T05:00:00Z"
weight = 14265
keywords = [ "rtp" ]
aliases = [ "/questions/14265" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [RTP definition of forward (fwd) and reverse (rev)?](/questions/14265/rtp-definition-of-forward-fwd-and-reverse-rev)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14265-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14265-score" class="post-score" title="current number of votes">0</div><span id="post-14265-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Please can you tell me what is the definition of forward and reverse with regards to RTP analysis? Cheers Femto.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '12, 03:32</strong></p><img src="https://secure.gravatar.com/avatar/ab6c24a4e811ab33b43f3fe8cdee84a2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Femto&#39;s gravatar image" /><p><span>Femto</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Femto has no accepted answers">0%</span></p></div></div><div id="comments-container-14265" class="comments-container"></div><div id="comment-tools-14265" class="comment-tools"></div><div class="clear"></div><div id="comment-14265-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14302"></span>

<div id="answer-container-14302" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14302-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14302-score" class="post-score" title="current number of votes">2</div><span id="post-14302-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Femto has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>RTP in itself has no forward and reverse direction, the sessions are totally independent. What binds them is the context in which they are used. For instance, in a VoIP call it's the initiators' end point that is usually tagged as producing the forward stream.</p><p>So what is the forward stream in Wireshark RTP analysis depends on what you select as such.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Sep '12, 05:00</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-14302" class="comments-container"></div><div id="comment-tools-14302" class="comment-tools"></div><div class="clear"></div><div id="comment-14302-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

