+++
type = "question"
title = "Reconstruction of GVSP video stream"
description = '''Can GVSP raw video stream be reconstructed from captured packets, in order to replay it with media player? '''
date = "2017-05-22T07:43:00Z"
lastmod = "2017-05-22T07:43:00Z"
weight = 61539
keywords = [ "gvsp", "videostream", "reconstruction" ]
aliases = [ "/questions/61539" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Reconstruction of GVSP video stream](/questions/61539/reconstruction-of-gvsp-video-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61539-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61539-score" class="post-score" title="current number of votes">0</div><span id="post-61539-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can GVSP raw video stream be reconstructed from captured packets, in order to replay it with media player?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gvsp" rel="tag" title="see questions tagged &#39;gvsp&#39;">gvsp</span> <span class="post-tag tag-link-videostream" rel="tag" title="see questions tagged &#39;videostream&#39;">videostream</span> <span class="post-tag tag-link-reconstruction" rel="tag" title="see questions tagged &#39;reconstruction&#39;">reconstruction</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '17, 07:43</strong></p><img src="https://secure.gravatar.com/avatar/0ef8ae815336d7286e62519880cb3368?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="matilda38111&#39;s gravatar image" /><p><span>matilda38111</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="matilda38111 has no accepted answers">0%</span></p></div></div><div id="comments-container-61539" class="comments-container"></div><div id="comment-tools-61539" class="comment-tools"></div><div class="clear"></div><div id="comment-61539-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

