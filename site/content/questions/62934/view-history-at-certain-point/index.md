+++
type = "question"
title = "view history at certain point"
description = '''I need to review activity at certain point from previous day, 11pm to 1 am. Thats when my network goes down every day. '''
date = "2017-07-20T12:49:00Z"
lastmod = "2017-07-20T18:16:00Z"
weight = 62934
keywords = [ "history" ]
aliases = [ "/questions/62934" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [view history at certain point](/questions/62934/view-history-at-certain-point)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62934-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62934-score" class="post-score" title="current number of votes">0</div><span id="post-62934-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to review activity at certain point from previous day, 11pm to 1 am. Thats when my network goes down every day.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-history" rel="tag" title="see questions tagged &#39;history&#39;">history</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jul '17, 12:49</strong></p><img src="https://secure.gravatar.com/avatar/371770feff33dffa1724f1faa413c400?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ac031601&#39;s gravatar image" /><p><span>ac031601</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ac031601 has no accepted answers">0%</span></p></div></div><div id="comments-container-62934" class="comments-container"></div><div id="comment-tools-62934" class="comment-tools"></div><div class="clear"></div><div id="comment-62934-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62940"></span>

<div id="answer-container-62940" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62940-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62940-score" class="post-score" title="current number of votes">0</div><span id="post-62940-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Thats when my network goes down every day.</p></blockquote><p>So I suggest you get a capture starting before you go down, through the event in question, starting before 11. Trick is to do the things you normally do that make you think the network is down - maybe ping a router or something?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jul '17, 18:16</strong></p><img src="https://secure.gravatar.com/avatar/0a47ef51dd9c9996d194a4983295f5a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob%20Jones&#39;s gravatar image" /><p><span>Bob Jones</span><br />
<span class="score" title="1014 reputation points"><span>1.0k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob Jones has 19 accepted answers">21%</span></p></div></div><div id="comments-container-62940" class="comments-container"></div><div id="comment-tools-62940" class="comment-tools"></div><div class="clear"></div><div id="comment-62940-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

