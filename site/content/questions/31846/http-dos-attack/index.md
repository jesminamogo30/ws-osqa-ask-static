+++
type = "question"
title = "http DOS attack"
description = '''&quot;transmission control protocol,src port: cognex-dataman(44444),dst port: http (80) ,seq:229,ack:1,len:0&quot; what is cognex-dataman(44444)?? is it evidence of http DOS attack?? if no thn any technique for capture live http DOS attack.??'''
date = "2014-04-15T13:07:00Z"
lastmod = "2014-04-16T04:45:00Z"
weight = 31846
keywords = [ "dos", "attack", "http" ]
aliases = [ "/questions/31846" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [http DOS attack](/questions/31846/http-dos-attack)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31846-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31846-score" class="post-score" title="current number of votes">0</div><span id="post-31846-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>"transmission control protocol,src port: cognex-dataman(44444),dst port: http (80) ,seq:229,ack:1,len:0"</p><p>what is cognex-dataman(44444)?? is it evidence of http DOS attack?? if no thn any technique for capture live http DOS attack.??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dos" rel="tag" title="see questions tagged &#39;dos&#39;">dos</span> <span class="post-tag tag-link-attack" rel="tag" title="see questions tagged &#39;attack&#39;">attack</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '14, 13:07</strong></p><img src="https://secure.gravatar.com/avatar/aa52d89e7a99661c042a9b0ed266d70a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="john6&#39;s gravatar image" /><p><span>john6</span><br />
<span class="score" title="7 reputation points">7</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="john6 has no accepted answers">0%</span></p></div></div><div id="comments-container-31846" class="comments-container"></div><div id="comment-tools-31846" class="comment-tools"></div><div class="clear"></div><div id="comment-31846-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31847"></span>

<div id="answer-container-31847" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31847-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31847-score" class="post-score" title="current number of votes">0</div><span id="post-31847-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, it is not evidence of a DOS attack, it is just the well known name for TCP port 44444.</p><p>To capture a live DOS attack (I guess with "live" you mean a real one) you need (surprise :-)) an actual DOS attack and a Wireshark capturing at the victim.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '14, 13:13</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-31847" class="comments-container"><span id="31853"></span><div id="comment-31853" class="comment"><div id="post-31853-score" class="comment-score"></div><div class="comment-text"><p>thn what can i do??help me or give me more suggestion...this is my project &amp; i have no inough time.</p><p>thanx...</p></div><div id="comment-31853-info" class="comment-info"><span class="comment-age">(15 Apr '14, 14:32)</span> <span class="comment-user userinfo">john6</span></div></div><span id="31875"></span><div id="comment-31875" class="comment"><div id="post-31875-score" class="comment-score"></div><div class="comment-text"><p>Project? What kind of project and why not enough time?</p></div><div id="comment-31875-info" class="comment-info"><span class="comment-age">(16 Apr '14, 04:42)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="31876"></span><div id="comment-31876" class="comment"><div id="post-31876-score" class="comment-score"></div><div class="comment-text"><p>Maybe you can use the DDoS Sample Trace that I uploaded to Cloudshark a while ago, see <a href="https://www.cloudshark.org/captures/ba85949942a0">https://www.cloudshark.org/captures/ba85949942a0</a></p></div><div id="comment-31876-info" class="comment-info"><span class="comment-age">(16 Apr '14, 04:45)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-31847" class="comment-tools"></div><div class="clear"></div><div id="comment-31847-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

