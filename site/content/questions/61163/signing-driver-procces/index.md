+++
type = "question"
title = "[closed] Signing driver procces"
description = '''Hello, How is the signing procces to the driver to filter driver? Thanks'''
date = "2017-05-02T09:41:00Z"
lastmod = "2017-05-02T10:14:00Z"
weight = 61163
keywords = [ "signing", "npf", "driver" ]
aliases = [ "/questions/61163" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Signing driver procces](/questions/61163/signing-driver-procces)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61163-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61163-score" class="post-score" title="current number of votes">0</div><span id="post-61163-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>How is the signing procces to the driver to filter driver?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-signing" rel="tag" title="see questions tagged &#39;signing&#39;">signing</span> <span class="post-tag tag-link-npf" rel="tag" title="see questions tagged &#39;npf&#39;">npf</span> <span class="post-tag tag-link-driver" rel="tag" title="see questions tagged &#39;driver&#39;">driver</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '17, 09:41</strong></p><img src="https://secure.gravatar.com/avatar/a7880ab48781ce9c9cdc482dcf48be3c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LuisRueda&#39;s gravatar image" /><p><span>LuisRueda</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LuisRueda has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 May '17, 10:15</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-61163" class="comments-container"><span id="61168"></span><div id="comment-61168" class="comment"><div id="post-61168-score" class="comment-score"></div><div class="comment-text"><p>This isn't a Wireshark question. Presumably you're talking about a capture driver for Wireshark, either WinPcap or npcap, in which case you'd get more support at the npcap website.</p></div><div id="comment-61168-info" class="comment-info"><span class="comment-age">(02 May '17, 10:14)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-61163" class="comment-tools"></div><div class="clear"></div><div id="comment-61163-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 02 May '17, 10:14

</div>

</div>

</div>

