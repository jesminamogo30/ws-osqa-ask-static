+++
type = "question"
title = "Ip address on wireshark pc"
description = '''When I mirror a port, does it matter what ip address I configure on the wireshark pc?'''
date = "2011-08-18T09:48:00Z"
lastmod = "2011-08-18T18:26:00Z"
weight = 5744
keywords = [ "ip", "mirroring", "address" ]
aliases = [ "/questions/5744" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ip address on wireshark pc](/questions/5744/ip-address-on-wireshark-pc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5744-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5744-score" class="post-score" title="current number of votes">0</div><span id="post-5744-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I mirror a port, does it matter what ip address I configure on the wireshark pc?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-mirroring" rel="tag" title="see questions tagged &#39;mirroring&#39;">mirroring</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Aug '11, 09:48</strong></p><img src="https://secure.gravatar.com/avatar/785c9267d7c8326509443de827180e4d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="spudster2011&#39;s gravatar image" /><p><span>spudster2011</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="spudster2011 has no accepted answers">0%</span></p></div></div><div id="comments-container-5744" class="comments-container"><span id="5756"></span><div id="comment-5756" class="comment"><div id="post-5756-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by "mirror a port"? Do you mean that you're configuring a switch port as a mirror port and plugging your Wireshark PC into that port to try to capture traffic going through the switch?</p></div><div id="comment-5756-info" class="comment-info"><span class="comment-age">(18 Aug '11, 18:26)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-5744" class="comment-tools"></div><div class="clear"></div><div id="comment-5744-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

