+++
type = "question"
title = "can I use my black berry as modem and Stil receive packets??"
description = '''Can use my BB as modem and Stil receive packets only interface&#x27;s showing is Intel 8267lm-3 gigbite network conection and tap win 32 adpter v9?'''
date = "2015-05-02T15:23:00Z"
lastmod = "2015-05-02T15:23:00Z"
weight = 42024
keywords = [ "capture", "blackberry" ]
aliases = [ "/questions/42024" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [can I use my black berry as modem and Stil receive packets??](/questions/42024/can-i-use-my-black-berry-as-modem-and-stil-receive-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42024-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42024-score" class="post-score" title="current number of votes">0</div><span id="post-42024-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can use my BB as modem and Stil receive packets only interface's showing is Intel 8267lm-3 gigbite network conection and tap win 32 adpter v9?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-blackberry" rel="tag" title="see questions tagged &#39;blackberry&#39;">blackberry</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '15, 15:23</strong></p><img src="https://secure.gravatar.com/avatar/a310ff513069b89f6c43cebf253e4e9e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hoergo%20Fails&#39;s gravatar image" /><p><span>Hoergo Fails</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hoergo Fails has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 May '15, 03:18</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-42024" class="comments-container"></div><div id="comment-tools-42024" class="comment-tools"></div><div class="clear"></div><div id="comment-42024-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

