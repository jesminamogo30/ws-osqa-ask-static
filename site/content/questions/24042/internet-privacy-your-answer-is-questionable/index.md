+++
type = "question"
title = "[closed] Internet privacy your answer is questionable"
description = '''I need a program that tells me, who is looking at my usage on the internet or basic operating usage, i will pay for it.'''
date = "2013-08-25T14:46:00Z"
lastmod = "2013-08-26T02:04:00Z"
weight = 24042
keywords = [ "__security_cookie", "internet" ]
aliases = [ "/questions/24042" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Internet privacy your answer is questionable](/questions/24042/internet-privacy-your-answer-is-questionable)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24042-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24042-score" class="post-score" title="current number of votes">-3</div><span id="post-24042-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need a program that tells me, who is looking at my usage on the internet or basic operating usage, i will pay for it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-__security_cookie" rel="tag" title="see questions tagged &#39;__security_cookie&#39;">__security_cookie</span> <span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Aug '13, 14:46</strong></p><img src="https://secure.gravatar.com/avatar/9fd462c7174c065326a740e39dd53553?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ldavidw10&#39;s gravatar image" /><p><span>ldavidw10</span><br />
<span class="score" title="-2 reputation points">-2</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ldavidw10 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>26 Aug '13, 05:54</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-24042" class="comments-container"><span id="24043"></span><div id="comment-24043" class="comment"><div id="post-24043-score" class="comment-score"></div><div class="comment-text"><p>I am pist off at internet providers that keep my computer from operating at its maximum because they are looking at all my actions on the keyboard, i do not mind them looking and collecting data at my internet actions but what i do on my daily programs i really thing that is to much. I want to restrict access to my letters and personal data, i do have a programmer that will do it but the bugger is on a surfing holiday, who will do such a program will make a few bucks like Bill Gates did (Bill Gates competition where on a fishing trip)and Bill made his start to his fortune, My email is <span class="__cf_email__" data-cfemail="a2cec6c3d4c7c6d59392e2c5cfc3cbce8cc1cdcf">[email protected]</span><br />
</p></div><div id="comment-24043-info" class="comment-info"><span class="comment-age">(25 Aug '13, 14:58)</span> <span class="comment-user userinfo">ldavidw10</span></div></div><span id="24050"></span><div id="comment-24050" class="comment"><div id="post-24050-score" class="comment-score"></div><div class="comment-text"><p>Not a Wireshark question. Use the search engine of your choice for the phrase "Internet Meter". There are many free and paid for options.</p></div><div id="comment-24050-info" class="comment-info"><span class="comment-age">(26 Aug '13, 02:04)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-24042" class="comment-tools"></div><div class="clear"></div><div id="comment-24042-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by multipleinterfaces 26 Aug '13, 05:54

</div>

</div>

</div>

