+++
type = "question"
title = "PS3 set up?"
description = '''I want to set this up to monitor traffic coming from my PS3 as it has come to my attention that it is being used at odd hours and I want to find out where it&#x27;s going at that time. Is it possible to do this with this software and how would I go about it?'''
date = "2014-03-25T08:16:00Z"
lastmod = "2014-03-28T08:39:00Z"
weight = 31153
keywords = [ "ps3" ]
aliases = [ "/questions/31153" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [PS3 set up?](/questions/31153/ps3-set-up)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31153-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31153-score" class="post-score" title="current number of votes">0</div><span id="post-31153-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to set this up to monitor traffic coming from my PS3 as it has come to my attention that it is being used at odd hours and I want to find out where it's going at that time. Is it possible to do this with this software and how would I go about it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ps3" rel="tag" title="see questions tagged &#39;ps3&#39;">ps3</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '14, 08:16</strong></p><img src="https://secure.gravatar.com/avatar/d0e8f59416fdb29720956f3ff3ed2731?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KingSlime80&#39;s gravatar image" /><p><span>KingSlime80</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KingSlime80 has no accepted answers">0%</span></p></div></div><div id="comments-container-31153" class="comments-container"></div><div id="comment-tools-31153" class="comment-tools"></div><div class="clear"></div><div id="comment-31153-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31188"></span>

<div id="answer-container-31188" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31188-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31188-score" class="post-score" title="current number of votes">0</div><span id="post-31188-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is it possible to do this with this software and how would I go about it?</p></blockquote><p>sure. See the following link</p><blockquote><p><a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></blockquote><p>and my answer/comments in a very similar question.</p><blockquote><p><a href="http://ask.wireshark.org/questions/31092/intercept-smtp-fromto-a-dvr">http://ask.wireshark.org/questions/31092/intercept-smtp-fromto-a-dvr</a></p></blockquote><p>especially the part about the hub and/or cheap switch with port mirroring.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Mar '14, 09:10</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-31188" class="comments-container"><span id="31247"></span><div id="comment-31247" class="comment"><div id="post-31247-score" class="comment-score"></div><div class="comment-text"><p>Thank you very much for the advice, I'll let you know if it worked once I am able to go through the information. :)</p></div><div id="comment-31247-info" class="comment-info"><span class="comment-age">(28 Mar '14, 08:39)</span> <span class="comment-user userinfo">KingSlime80</span></div></div></div><div id="comment-tools-31188" class="comment-tools"></div><div class="clear"></div><div id="comment-31188-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

