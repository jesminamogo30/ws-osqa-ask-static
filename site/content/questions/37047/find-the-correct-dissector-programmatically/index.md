+++
type = "question"
title = "Find the correct dissector programmatically"
description = '''I am developing a project which uses wireshark dissector. It gets the hex string and decode the LTE, GSM or WCDMA message, but I always need to select the dissector manually. I would like to know if there is any way of finding the right dissector programmatically. Besides, is there any list with all...'''
date = "2014-10-14T19:56:00Z"
lastmod = "2014-10-14T19:56:00Z"
weight = 37047
keywords = [ "wcdma", "dissector", "gsm", "lte", "wireshark" ]
aliases = [ "/questions/37047" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Find the correct dissector programmatically](/questions/37047/find-the-correct-dissector-programmatically)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37047-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37047-score" class="post-score" title="current number of votes">0</div><span id="post-37047-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am developing a project which uses wireshark dissector. It gets the hex string and decode the LTE, GSM or WCDMA message, but I always need to select the dissector manually. I would like to know if there is any way of finding the right dissector programmatically. Besides, is there any list with all LTE, GSM and WCDMA dissectors?</p><p>Thanks in Advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wcdma" rel="tag" title="see questions tagged &#39;wcdma&#39;">wcdma</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span> <span class="post-tag tag-link-lte" rel="tag" title="see questions tagged &#39;lte&#39;">lte</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Oct '14, 19:56</strong></p><img src="https://secure.gravatar.com/avatar/4de92f5cbacdafb96fdad81319ad4341?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lsilva&#39;s gravatar image" /><p><span>lsilva</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lsilva has no accepted answers">0%</span></p></div></div><div id="comments-container-37047" class="comments-container"></div><div id="comment-tools-37047" class="comment-tools"></div><div class="clear"></div><div id="comment-37047-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

