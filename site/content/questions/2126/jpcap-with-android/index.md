+++
type = "question"
title = "JPCAP with android"
description = '''I have written a small app to sniff packets. It uses the JPCAP lib. It functions properly, however, I am having trouble porting it to my android emulator. Has anyone tried using JPCAP with android? Forrest'''
date = "2011-02-03T07:04:00Z"
lastmod = "2011-05-27T06:49:00Z"
weight = 2126
keywords = [ "sniffing", "android", "jpcap" ]
aliases = [ "/questions/2126" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [JPCAP with android](/questions/2126/jpcap-with-android)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2126-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2126-score" class="post-score" title="current number of votes">0</div><span id="post-2126-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have written a small app to sniff packets. It uses the JPCAP lib. It functions properly, however, I am having trouble porting it to my android emulator. Has anyone tried using JPCAP with android?</p><p>Forrest</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-jpcap" rel="tag" title="see questions tagged &#39;jpcap&#39;">jpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '11, 07:04</strong></p><img src="https://secure.gravatar.com/avatar/511929d1f29f82399a9a0e933729ba77?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Forrest&#39;s gravatar image" /><p><span>Forrest</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Forrest has no accepted answers">0%</span></p></div></div><div id="comments-container-2126" class="comments-container"><span id="2131"></span><div id="comment-2131" class="comment"><div id="post-2131-score" class="comment-score"></div><div class="comment-text"><p>No....but I'm really interested in your success!</p></div><div id="comment-2131-info" class="comment-info"><span class="comment-age">(03 Feb '11, 10:08)</span> <span class="comment-user userinfo">GeonJay</span></div></div><span id="2132"></span><div id="comment-2132" class="comment"><div id="post-2132-score" class="comment-score"></div><div class="comment-text"><p>If I figure it out I'll post it. It could just be the emulator does not have network interfaces and therefore blows-up. I am getting a htc incredible soon to experiment on.</p></div><div id="comment-2132-info" class="comment-info"><span class="comment-age">(03 Feb '11, 10:17)</span> <span class="comment-user userinfo">Forrest</span></div></div></div><div id="comment-tools-2126" class="comment-tools"></div><div class="clear"></div><div id="comment-2126-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4254"></span>

<div id="answer-container-4254" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4254-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4254-score" class="post-score" title="current number of votes">-1</div><span id="post-4254-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I was trying to make a simple packet sniffer application in Android using Jpcap. I have added the jpcap.jar in my project. i.e. Java Build Path --&gt; Libraries --&gt; Add External Jars :</p><p>But still am not able to run my application. its getting this following error and get killed.</p><p>java.lang.UnsatisfiedLinkError: Couldn't load jpcap: findLibrary returned null.</p><p>Can anyone help me to resolve this issue:</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 May '11, 03:32</strong></p><img src="https://secure.gravatar.com/avatar/f7a4b3bc8f5d0f969e31bd03ba722d8c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vijay&#39;s gravatar image" /><p><span>Vijay</span><br />
<span class="score" title="0 reputation points">0</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vijay has no accepted answers">0%</span></p></div></div><div id="comments-container-4254" class="comments-container"><span id="4257"></span><div id="comment-4257" class="comment"><div id="post-4257-score" class="comment-score"></div><div class="comment-text"><p>Don't hijack someone else's question to ask one of your own.</p></div><div id="comment-4257-info" class="comment-info"><span class="comment-age">(27 May '11, 06:49)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-4254" class="comment-tools"></div><div class="clear"></div><div id="comment-4254-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

