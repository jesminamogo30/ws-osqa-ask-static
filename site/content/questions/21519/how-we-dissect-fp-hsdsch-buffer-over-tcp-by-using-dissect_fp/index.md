+++
type = "question"
title = "How we dissect FP-HSDSCH buffer over tcp by using dissect_fp()"
description = '''Hi i have a buffer of Dounlink Fp-HSDSCH buffer , want to dissect this buffer using dissect_fp() over tcp function, but it gives error&quot;per-packet info was not persent&quot;'''
date = "2013-05-28T06:29:00Z"
lastmod = "2013-05-28T06:29:00Z"
weight = 21519
keywords = [ "dissect_fp", "fp", "umts-fp" ]
aliases = [ "/questions/21519" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How we dissect FP-HSDSCH buffer over tcp by using dissect\_fp()](/questions/21519/how-we-dissect-fp-hsdsch-buffer-over-tcp-by-using-dissect_fp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21519-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21519-score" class="post-score" title="current number of votes">0</div><span id="post-21519-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi i have a buffer of Dounlink Fp-HSDSCH buffer , want to dissect this buffer using dissect_fp() over tcp function, but it gives error"per-packet info was not persent"</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissect_fp" rel="tag" title="see questions tagged &#39;dissect_fp&#39;">dissect_fp</span> <span class="post-tag tag-link-fp" rel="tag" title="see questions tagged &#39;fp&#39;">fp</span> <span class="post-tag tag-link-umts-fp" rel="tag" title="see questions tagged &#39;umts-fp&#39;">umts-fp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 May '13, 06:29</strong></p><img src="https://secure.gravatar.com/avatar/5fa560f4e928cb2036f850a4fa37c869?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ranjeet%20Nag&#39;s gravatar image" /><p><span>Ranjeet Nag</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ranjeet Nag has no accepted answers">0%</span></p></div></div><div id="comments-container-21519" class="comments-container"></div><div id="comment-tools-21519" class="comment-tools"></div><div class="clear"></div><div id="comment-21519-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

