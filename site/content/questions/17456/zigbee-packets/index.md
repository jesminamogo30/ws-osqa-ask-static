+++
type = "question"
title = "ZigBee packets"
description = '''Hi all, I have digi s2b module, which is used as coordinator for the network. Also, I have XBP24 module, which is used as transceiver. I want to capture the packets between the two modules. I search on the net, but I found that each sniffer is compatible with its hardware. Can anybody help with this...'''
date = "2013-01-04T12:37:00Z"
lastmod = "2013-01-05T05:16:00Z"
weight = 17456
keywords = [ "zigbee" ]
aliases = [ "/questions/17456" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ZigBee packets](/questions/17456/zigbee-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17456-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17456-score" class="post-score" title="current number of votes">0</div><span id="post-17456-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I have digi s2b module, which is used as coordinator for the network. Also, I have XBP24 module, which is used as transceiver. I want to capture the packets between the two modules. I search on the net, but I found that each sniffer is compatible with its hardware. Can anybody help with this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-zigbee" rel="tag" title="see questions tagged &#39;zigbee&#39;">zigbee</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '13, 12:37</strong></p><img src="https://secure.gravatar.com/avatar/f2dfa8bcebf8ec5f011087a006805573?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill&#39;s gravatar image" /><p><span>Bill</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jan '13, 15:19</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-17456" class="comments-container"></div><div id="comment-tools-17456" class="comment-tools"></div><div class="clear"></div><div id="comment-17456-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17474"></span>

<div id="answer-container-17474" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17474-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17474-score" class="post-score" title="current number of votes">0</div><span id="post-17474-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi There, In uour 802.15.4 based networks (ZigBee as well as RF4CE and 6LoWPAN) we use the Perytons analyzer - <a href="http://www.perytons.com">www.perytons.com</a>. They provide a 60 days free evaluation of their software and they are compatible with a large number of front-end manyfacturers.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jan '13, 05:16</strong></p><img src="https://secure.gravatar.com/avatar/0169872b21ad8da114515f99ee7eeef6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AviDrugi&#39;s gravatar image" /><p><span>AviDrugi</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AviDrugi has no accepted answers">0%</span></p></div></div><div id="comments-container-17474" class="comments-container"></div><div id="comment-tools-17474" class="comment-tools"></div><div class="clear"></div><div id="comment-17474-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

