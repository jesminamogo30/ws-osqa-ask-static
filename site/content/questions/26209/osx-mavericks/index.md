+++
type = "question"
title = "OSX Mavericks"
description = '''Hi, Where can i find updates for Wireshark running under OSX Mavericks? Current version is: 1.11.0 r52474 Qt 5.1.1 Thnx Loe'''
date = "2013-10-19T02:30:00Z"
lastmod = "2013-10-19T16:11:00Z"
weight = 26209
keywords = [ "mavericks" ]
aliases = [ "/questions/26209" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [OSX Mavericks](/questions/26209/osx-mavericks)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26209-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26209-score" class="post-score" title="current number of votes">0</div><span id="post-26209-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Where can i find updates for Wireshark running under OSX Mavericks? Current version is: <strong>1.11.0 r52474 Qt 5.1.1</strong></p><p>Thnx Loe</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mavericks" rel="tag" title="see questions tagged &#39;mavericks&#39;">mavericks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Oct '13, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/bbba1b5736eafd024fe8d8a4325807db?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Loe%20Walter&#39;s gravatar image" /><p><span>Loe Walter</span><br />
<span class="score" title="41 reputation points">41</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Loe Walter has no accepted answers">0%</span></p></div></div><div id="comments-container-26209" class="comments-container"></div><div id="comment-tools-26209" class="comment-tools"></div><div class="clear"></div><div id="comment-26209-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26220"></span>

<div id="answer-container-26220" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26220-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26220-score" class="post-score" title="current number of votes">0</div><span id="post-26220-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>We do not guarantee that 1.11.0 - or any development build - will work properly on all versions of the OSes we support. There are currently some issues with Qt 5.1.x and Mavericks; if you want to run Wireshark on Mavericks, you will have to run one of the official releases, such as 1.10.2, or figure out how to work around those issues and build Qt and Wireshark yourself. I suggest running one of the official releases.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Oct '13, 16:11</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-26220" class="comments-container"></div><div id="comment-tools-26220" class="comment-tools"></div><div class="clear"></div><div id="comment-26220-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

