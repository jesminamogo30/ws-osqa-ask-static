+++
type = "question"
title = "WSGD. Should Wireshark always be restarted after changes in .wsgd .fdesc?"
description = '''Question is about wsgd.free.fr.  Should Wireshark always be restarted after changes in .wsgd .fdesc? May be there is a way to reload only generic.dll?'''
date = "2016-01-18T07:33:00Z"
lastmod = "2016-01-18T10:50:00Z"
weight = 49322
keywords = [ "dissector", "wsgd" ]
aliases = [ "/questions/49322" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [WSGD. Should Wireshark always be restarted after changes in .wsgd .fdesc?](/questions/49322/wsgd-should-wireshark-always-be-restarted-after-changes-in-wsgd-fdesc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49322-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49322-score" class="post-score" title="current number of votes">0</div><span id="post-49322-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Question is about <a href="http://wsgd.free.fr/index.html">wsgd.free.fr</a>. Should Wireshark always be restarted after changes in .wsgd .fdesc?<br />
May be there is a way to reload only generic.dll?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-wsgd" rel="tag" title="see questions tagged &#39;wsgd&#39;">wsgd</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '16, 07:33</strong></p><img src="https://secure.gravatar.com/avatar/680680d82143e4d33dc569913e5ef8c5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kyb&#39;s gravatar image" /><p><span>kyb</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kyb has one accepted answer">100%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Jan '16, 07:33</strong> </span></p></div></div><div id="comments-container-49322" class="comments-container"></div><div id="comment-tools-49322" class="comment-tools"></div><div class="clear"></div><div id="comment-49322-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49329"></span>

<div id="answer-container-49329" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49329-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49329-score" class="post-score" title="current number of votes">2</div><span id="post-49329-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="kyb has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark should always be restarted after changes in .wsgd .fdesc. Wireshark does not provide any way to change some dissector definitions/stuff without restarting.</p><p>Wireshark does not provide any way to reload a dll without restarting.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jan '16, 10:50</strong></p><img src="https://secure.gravatar.com/avatar/f69baac47dbc58c404e4356eb7a3b191?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wsgd&#39;s gravatar image" /><p><span>wsgd</span><br />
<span class="score" title="91 reputation points">91</span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wsgd has 2 accepted answers">100%</span></p></div></div><div id="comments-container-49329" class="comments-container"></div><div id="comment-tools-49329" class="comment-tools"></div><div class="clear"></div><div id="comment-49329-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

