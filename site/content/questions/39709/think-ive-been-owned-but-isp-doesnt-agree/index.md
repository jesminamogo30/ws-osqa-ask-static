+++
type = "question"
title = "Think i&#x27;ve been owned, but isp doesn&#x27;t agree"
description = '''I started noticing my computer acting strange in June of 2014 and started investigating the cause, after months of getting nowhere I thought i&#x27;d post here to let someone with more experience have a look at my capture... what my suspicion was that it was maybe a reverse proxy or something. I&#x27;ve learn...'''
date = "2015-02-09T04:53:00Z"
lastmod = "2015-02-09T15:04:00Z"
weight = 39709
keywords = [ "rootkit", "winston", "localhost", "loopback" ]
aliases = [ "/questions/39709" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Think i've been owned, but isp doesn't agree](/questions/39709/think-ive-been-owned-but-isp-doesnt-agree)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39709-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39709-score" class="post-score" title="current number of votes">0</div><span id="post-39709-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I started noticing my computer acting strange in June of 2014 and started investigating the cause, after months of getting nowhere I thought i'd post here to let someone with more experience have a look at my capture... what my suspicion was that it was maybe a reverse proxy or something. I've learned a lot over the last 6-7 months but I've come to realize that no matter, I've barely scratched the surface and should really stick to pulling wrenches and let someone else do what they do... :) I've uploaded a capture.<br />
<a href="https://www.cloudshark.org/captures/36c6a2600ab7">https://www.cloudshark.org/captures/36c6a2600ab7</a></p><p>If someone might have a look maybe they can say, everything looks normal, or have you tried?</p><p>Thanks in advance, if you need any help with a compressor rebuild, or a stationary diesel engine, i'd be happy to return the favor :) ...</p><p>Frank</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rootkit" rel="tag" title="see questions tagged &#39;rootkit&#39;">rootkit</span> <span class="post-tag tag-link-winston" rel="tag" title="see questions tagged &#39;winston&#39;">winston</span> <span class="post-tag tag-link-localhost" rel="tag" title="see questions tagged &#39;localhost&#39;">localhost</span> <span class="post-tag tag-link-loopback" rel="tag" title="see questions tagged &#39;loopback&#39;">loopback</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Feb '15, 04:53</strong></p><img src="https://secure.gravatar.com/avatar/78c2a2c7f8125d9d584dcdfd0a387f89?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="frank%20hero&#39;s gravatar image" /><p><span>frank hero</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="frank hero has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-39709" class="comments-container"></div><div id="comment-tools-39709" class="comment-tools"></div><div class="clear"></div><div id="comment-39709-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39722"></span>

<div id="answer-container-39722" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39722-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39722-score" class="post-score" title="current number of votes">0</div><span id="post-39722-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is nothing in the capture file that makes me believe you have been owned. There are a lot of ARP request every 10 seconds from your Cisco router, but that looks like "normal" operation of that device, trying to probe/map the local network.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Feb '15, 15:04</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-39722" class="comments-container"></div><div id="comment-tools-39722" class="comment-tools"></div><div class="clear"></div><div id="comment-39722-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

