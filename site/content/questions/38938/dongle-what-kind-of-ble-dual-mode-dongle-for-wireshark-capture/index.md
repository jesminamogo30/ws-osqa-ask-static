+++
type = "question"
title = "[Dongle] What kind of BLE Dual mode Dongle for Wireshark capture?"
description = '''Hi,  What kind of Bluetooth dongle (support BT dual mode) for Wireshark real-time capture Thanks.'''
date = "2015-01-08T02:25:00Z"
lastmod = "2015-01-10T07:56:00Z"
weight = 38938
keywords = [ "ble", "wireshark", "dongle", "bluetooth" ]
aliases = [ "/questions/38938" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[Dongle\] What kind of BLE Dual mode Dongle for Wireshark capture?](/questions/38938/dongle-what-kind-of-ble-dual-mode-dongle-for-wireshark-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38938-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38938-score" class="post-score" title="current number of votes">0</div><span id="post-38938-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, What kind of Bluetooth dongle (support BT dual mode) for Wireshark real-time capture</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ble" rel="tag" title="see questions tagged &#39;ble&#39;">ble</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-dongle" rel="tag" title="see questions tagged &#39;dongle&#39;">dongle</span> <span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jan '15, 02:25</strong></p><img src="https://secure.gravatar.com/avatar/cd72396e8302134e574d2ee23253e8ce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RHo&#39;s gravatar image" /><p><span>RHo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RHo has no accepted answers">0%</span></p></div></div><div id="comments-container-38938" class="comments-container"></div><div id="comment-tools-38938" class="comment-tools"></div><div class="clear"></div><div id="comment-38938-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39025"></span>

<div id="answer-container-39025" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39025-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39025-score" class="post-score" title="current number of votes">0</div><span id="post-39025-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Almost all, expect dongles that not provide standard HCI interface. But there are some restrictions: 1. Only Linux support real-time capturing for Bluetooth. 2. Linux with kernel that support Bluetooth is needed. And support your device (maybe it is new or special and it is not supported yet)</p><p>If you have USB interfaces in Wireshark you probably should see Bluetooth traffic over Bluetooth USB dongles (almost all are standard and should work).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jan '15, 07:56</strong></p><img src="https://secure.gravatar.com/avatar/6eabf35b1168a8242bb2d69db18a8a7c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Micha%C5%82%20%C5%81ab%C4%99dzki&#39;s gravatar image" /><p><span>Michał Łabędzki</span><br />
<span class="score" title="41 reputation points">41</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Michał Łabędzki has one accepted answer">8%</span></p></div></div><div id="comments-container-39025" class="comments-container"></div><div id="comment-tools-39025" class="comment-tools"></div><div class="clear"></div><div id="comment-39025-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

