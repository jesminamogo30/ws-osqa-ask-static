+++
type = "question"
title = "error loading some MIB from Cisco"
description = '''Hi all, I&#x27;m trying to load the Cisco wireless MIBs into Wireshark. Some of them load no problems, but two that I&#x27;m interested in give a &quot;stopped processing module ... MIB due to errors to prevent potential crash in libsmi. These are the particular MIB&#x27;s: CISCO-LWAPP-DOT11-CLIENT-MIB CISCO-LWAPP-ROGU...'''
date = "2015-03-31T23:10:00Z"
lastmod = "2015-04-01T01:39:00Z"
weight = 41075
keywords = [ "load", "smnp", "mib", "cisco", "error" ]
aliases = [ "/questions/41075" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [error loading some MIB from Cisco](/questions/41075/error-loading-some-mib-from-cisco)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41075-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41075-score" class="post-score" title="current number of votes">0</div><span id="post-41075-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I'm trying to load the Cisco wireless MIBs into Wireshark. Some of them load no problems, but two that I'm interested in give a "stopped processing module ... MIB due to errors to prevent potential crash in libsmi. These are the particular MIB's: CISCO-LWAPP-DOT11-CLIENT-MIB CISCO-LWAPP-ROGUE-MIB</p><p>I run then through one of the online MIB analyzers, and they say that I'm missing dependencies such as: CISCO-SMI</p><p>But I'm not I have those mibs in the wireshark dir and they have loaded correctly, other MIBs that have loaded rely on these and they have loaded as well. I've checked all other syntax in the mibs and it looks ok.</p><p>How can I troubleshoot this further?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-load" rel="tag" title="see questions tagged &#39;load&#39;">load</span> <span class="post-tag tag-link-smnp" rel="tag" title="see questions tagged &#39;smnp&#39;">smnp</span> <span class="post-tag tag-link-mib" rel="tag" title="see questions tagged &#39;mib&#39;">mib</span> <span class="post-tag tag-link-cisco" rel="tag" title="see questions tagged &#39;cisco&#39;">cisco</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Mar '15, 23:10</strong></p><img src="https://secure.gravatar.com/avatar/8f11004fd32ec7c6e14445f328ac5555?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sossie07&#39;s gravatar image" /><p><span>sossie07</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sossie07 has no accepted answers">0%</span></p></div></div><div id="comments-container-41075" class="comments-container"><span id="41085"></span><div id="comment-41085" class="comment"><div id="post-41085-score" class="comment-score"></div><div class="comment-text"><p>Can you reduce your set of MIBs to the minimum required to cause the issue and then list them in your question? That might allow someone to look into the issue.</p></div><div id="comment-41085-info" class="comment-info"><span class="comment-age">(01 Apr '15, 01:39)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-41075" class="comment-tools"></div><div class="clear"></div><div id="comment-41075-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

