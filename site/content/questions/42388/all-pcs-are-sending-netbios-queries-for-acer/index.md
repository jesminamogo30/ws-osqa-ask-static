+++
type = "question"
title = "All PCs are sending NETBIOS queries for &quot;ACER&quot;"
description = '''For months now, my Android phoneppe never stod showing the &quot;Downloading&quot; indicator on WiFi icon, even while it was locked, with no apps running and without using network resources. So, I supposed that one of my devices on my network is broadcasting packets all the time, and I installed Wireshark on ...'''
date = "2015-05-14T04:28:00Z"
lastmod = "2015-05-15T06:33:00Z"
weight = 42388
keywords = [ "netbios" ]
aliases = [ "/questions/42388" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [All PCs are sending NETBIOS queries for "ACER"](/questions/42388/all-pcs-are-sending-netbios-queries-for-acer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42388-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42388-score" class="post-score" title="current number of votes">0</div><span id="post-42388-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>For months now, my Android phoneppe never stod showing the "Downloading" indicator on WiFi icon, even while it was locked, with no apps running and without using network resources. So, I supposed that one of my devices on my network is broadcasting packets all the time, and I installed Wireshark on my machines to see what's happening (1 Win7 Laptop, 2 Win7 PCs working as servers, 1 Win8.1 Laptop).</p><p>"Name query NB ACER&lt;1c&gt;"</p><p>All of my Windows machines broadcast this message every 2 seconds. And yes, my Win7 Laptop is named "ACER", but when others send 1 packet per 2 seconds, my ACER Laptop is sending "Name query NB ACER&lt;1c&gt;" 10 times per 2 seconds! It queries it's own name, five more times than my other devices.</p><p>Is there a way to find the reason that these packets are being send all the time and killing my phone's battery?</p><p>Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-netbios" rel="tag" title="see questions tagged &#39;netbios&#39;">netbios</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 May '15, 04:28</strong></p><img src="https://secure.gravatar.com/avatar/55de38947f1c424610d52d0c0265aa67?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Manos%20Zoumpoulakis&#39;s gravatar image" /><p><span>Manos Zoumpo...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Manos Zoumpoulakis has no accepted answers">0%</span></p></div></div><div id="comments-container-42388" class="comments-container"></div><div id="comment-tools-42388" class="comment-tools"></div><div class="clear"></div><div id="comment-42388-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42413"></span>

<div id="answer-container-42413" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42413-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42413-score" class="post-score" title="current number of votes">0</div><span id="post-42413-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you don't need NetBIOS you can disable it under the adapter properties &gt; IPv4 &gt; Advanced &gt; WINS.</p><p>The queries can be generated by the OS (PID 4 - System) or by some software that you have installed. Check with:</p><blockquote><p>netstat -ano -p UDP | findstr 137</p></blockquote><p>If it is System look at your network locations / sharing profile.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '15, 06:33</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div></div><div id="comments-container-42413" class="comments-container"></div><div id="comment-tools-42413" class="comment-tools"></div><div class="clear"></div><div id="comment-42413-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

