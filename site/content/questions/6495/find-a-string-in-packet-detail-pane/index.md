+++
type = "question"
title = "find a string in packet detail pane"
description = '''Sorry if I missed this answer in my search... After finding a packet, the packet details section can be quite long. Is there a way to find a string in the detail panel and use a next button to advance through the details. I ended up opening my trace in Notepad++ so that I could do this!'''
date = "2011-09-22T14:49:00Z"
lastmod = "2011-09-22T14:49:00Z"
weight = 6495
keywords = [ "find", "details" ]
aliases = [ "/questions/6495" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [find a string in packet detail pane](/questions/6495/find-a-string-in-packet-detail-pane)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6495-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6495-score" class="post-score" title="current number of votes">0</div><span id="post-6495-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Sorry if I missed this answer in my search...</p><p>After finding a packet, the packet details section can be quite long. Is there a way to find a string in the detail panel and use a next button to advance through the details. I ended up opening my trace in Notepad++ so that I could do this!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-find" rel="tag" title="see questions tagged &#39;find&#39;">find</span> <span class="post-tag tag-link-details" rel="tag" title="see questions tagged &#39;details&#39;">details</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '11, 14:49</strong></p><img src="https://secure.gravatar.com/avatar/1d0783b76ade2c066f498e483886cfc2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chaptor&#39;s gravatar image" /><p><span>chaptor</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chaptor has no accepted answers">0%</span></p></div></div><div id="comments-container-6495" class="comments-container"></div><div id="comment-tools-6495" class="comment-tools"></div><div class="clear"></div><div id="comment-6495-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

