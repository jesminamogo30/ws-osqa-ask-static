+++
type = "question"
title = "Cisco WLC can you capture trace of a VLAN within the controller"
description = '''Cisco WLC version 8.0.121.0. I need to capture a trace from within the controller of a VLAN. It is wireless to wireless (both roaming) so a capture at the AP or on entry into the switch will not help. Can this be done and, if so, how?)'''
date = "2016-01-06T09:01:00Z"
lastmod = "2016-01-08T07:43:00Z"
weight = 48915
keywords = [ "cisco369", "wlc" ]
aliases = [ "/questions/48915" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Cisco WLC can you capture trace of a VLAN within the controller](/questions/48915/cisco-wlc-can-you-capture-trace-of-a-vlan-within-the-controller)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48915-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48915-score" class="post-score" title="current number of votes">0</div><span id="post-48915-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Cisco WLC version 8.0.121.0. I need to capture a trace from within the controller of a VLAN. It is wireless to wireless (both roaming) so a capture at the AP or on entry into the switch will not help. Can this be done and, if so, how?)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cisco369" rel="tag" title="see questions tagged &#39;cisco369&#39;">cisco369</span> <span class="post-tag tag-link-wlc" rel="tag" title="see questions tagged &#39;wlc&#39;">wlc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jan '16, 09:01</strong></p><img src="https://secure.gravatar.com/avatar/7e8faa0e739f3b2cdc87b71c1bc88036?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mikew&#39;s gravatar image" /><p><span>mikew</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mikew has no accepted answers">0%</span></p></div></div><div id="comments-container-48915" class="comments-container"></div><div id="comment-tools-48915" class="comment-tools"></div><div class="clear"></div><div id="comment-48915-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48972"></span>

<div id="answer-container-48972" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48972-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48972-score" class="post-score" title="current number of votes">0</div><span id="post-48972-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>A quick google search returns these:</p><blockquote><p><a href="http://what-when-how.com/deploying-and-troubleshooting-cisco-wireless-lan-controllers/troubleshooting-on-the-wlc-cisco-wireless-lan-controllers-part-1/">http://what-when-how.com/deploying-and-troubleshooting-cisco-wireless-lan-controllers/troubleshooting-on-the-wlc-cisco-wireless-lan-controllers-part-1/</a></p><p><a href="http://www.cisco.com/c/en/us/td/docs/wireless/controller/7-4/configuration/guides/consolidated/b_cg74_CONSOLIDATED/b_cg74_CONSOLIDATED_chapter_01101111.html">http://www.cisco.com/c/en/us/td/docs/wireless/controller/7-4/configuration/guides/consolidated/b_cg74_CONSOLIDATED/b_cg74_CONSOLIDATED_chapter_01101111.html</a></p></blockquote><p>But, seriously: this is the Wireshark Q&amp;A site, not a Cisco forum ;-) So, please ask the vendor support of Cisco. They should be able to answer you WLC specific questions.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jan '16, 07:43</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-48972" class="comments-container"></div><div id="comment-tools-48972" class="comment-tools"></div><div class="clear"></div><div id="comment-48972-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

