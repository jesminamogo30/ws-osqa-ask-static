+++
type = "question"
title = "trying to extract H264 on TCP and UDP"
description = '''hi, so here the story (will try to make it short and describe as possible). i have a DVR program that allow me to watch my cameras, i captured with wireshark the communication of the stream for couple of seconds, now i would like to extract the stream from the capture, the problem is the protocol is...'''
date = "2017-01-14T15:04:00Z"
lastmod = "2017-01-19T04:26:00Z"
weight = 58766
keywords = [ "h264", "udp", "rtp", "tcp" ]
aliases = [ "/questions/58766" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [trying to extract H264 on TCP and UDP](/questions/58766/trying-to-extract-h264-on-tcp-and-udp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58766-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58766-score" class="post-score" title="current number of votes">0</div><span id="post-58766-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, so here the story (will try to make it short and describe as possible). i have a DVR program that allow me to watch my cameras, i captured with wireshark the communication of the stream for couple of seconds, now i would like to extract the stream from the capture, the problem is the protocol is TCP (some times udp depend on the app/software), i know its a H264 encoding because the first packet when previewing a camera contain H264</p><blockquote><p>1111t d(<code>[email protected]^F1Y(</code> H264</p></blockquote><p>i tried to change the protocol hoping that would help me but it did not (maybe i did something wrong idk hopefull u could tell me what should i do) since there is no H264 i picked rtsp Decode as&gt;Transport&gt;rtsp , u guess it right i could not export it to view it.</p><p>help would be great. thanks alot.</p><p>heres a link to pastbin to see the actual packets (exported without summary lines and changed the dst ip and macs for privacy) <a href="http://pastebin.com/hMPWHjTH">http://pastebin.com/hMPWHjTH</a></p><p>and if u want to download the whole txt (1mb) heres the link <a href="http://www115.zippyshare.com/v/bKR5FDp2/file.html">http://www115.zippyshare.com/v/bKR5FDp2/file.html</a></p><p>if u need any additional info please tell me i will glad to provide it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-h264" rel="tag" title="see questions tagged &#39;h264&#39;">h264</span> <span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '17, 15:04</strong></p><img src="https://secure.gravatar.com/avatar/be87b4f738f50483c14962d8f8f525c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kol&#39;s gravatar image" /><p><span>kol</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kol has no accepted answers">0%</span></p></div></div><div id="comments-container-58766" class="comments-container"><span id="58882"></span><div id="comment-58882" class="comment"><div id="post-58882-score" class="comment-score"></div><div class="comment-text"><p>no one as tried what i am trying to achive here?</p><p>could some one give a hand on that :o?</p></div><div id="comment-58882-info" class="comment-info"><span class="comment-age">(19 Jan '17, 04:26)</span> <span class="comment-user userinfo">kol</span></div></div></div><div id="comment-tools-58766" class="comment-tools"></div><div class="clear"></div><div id="comment-58766-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

