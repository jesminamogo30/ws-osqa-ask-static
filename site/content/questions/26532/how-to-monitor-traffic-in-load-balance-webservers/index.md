+++
type = "question"
title = "How to monitor traffic in load balance webservers?"
description = '''We have 8 webservers that manage in load balancing mode. I install wireshark on one webserver to monitor traffic in my network..I wonder if wireshark can monitor also the traffic to other webservers and how to analyze traffic in webserver.'''
date = "2013-10-30T04:06:00Z"
lastmod = "2013-11-03T03:28:00Z"
weight = 26532
keywords = [ "webserver" ]
aliases = [ "/questions/26532" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to monitor traffic in load balance webservers?](/questions/26532/how-to-monitor-traffic-in-load-balance-webservers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26532-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26532-score" class="post-score" title="current number of votes">0</div><span id="post-26532-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We have 8 webservers that manage in load balancing mode. I install wireshark on one webserver to monitor traffic in my network..I wonder if wireshark can monitor also the traffic to other webservers and how to analyze traffic in webserver.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-webserver" rel="tag" title="see questions tagged &#39;webserver&#39;">webserver</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Oct '13, 04:06</strong></p><img src="https://secure.gravatar.com/avatar/a42962da254360f0bfc5fd52402789f5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rayden&#39;s gravatar image" /><p><span>rayden</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rayden has no accepted answers">0%</span></p></div></div><div id="comments-container-26532" class="comments-container"></div><div id="comment-tools-26532" class="comment-tools"></div><div class="clear"></div><div id="comment-26532-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26635"></span>

<div id="answer-container-26635" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26635-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26635-score" class="post-score" title="current number of votes">0</div><span id="post-26635-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please see: <a href="http://wiki.wireshark.org/CaptureSetup">http://wiki.wireshark.org/CaptureSetup</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Nov '13, 03:28</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-26635" class="comments-container"></div><div id="comment-tools-26635" class="comment-tools"></div><div class="clear"></div><div id="comment-26635-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

