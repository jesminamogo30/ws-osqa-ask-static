+++
type = "question"
title = "ISCSI:Multiple connections in a tcp session"
description = '''I am new to iscsi protocol.Came across an aspect of it in RFC stating multiple iscsi connection support in same TCP Session.How to identify all these multiple connections? In HTTP World we can relate it to multiple GET requests within a same TCP Session.How can one relate this in iscsi world?'''
date = "2013-03-26T15:39:00Z"
lastmod = "2013-04-08T16:35:00Z"
weight = 19856
keywords = [ "iscsi" ]
aliases = [ "/questions/19856" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [ISCSI:Multiple connections in a tcp session](/questions/19856/iscsimultiple-connections-in-a-tcp-session)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19856-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19856-score" class="post-score" title="current number of votes">0</div><span id="post-19856-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am new to iscsi protocol.Came across an aspect of it in RFC stating multiple iscsi connection support in same TCP Session.How to identify all these multiple connections? In HTTP World we can relate it to multiple GET requests within a same TCP Session.How can one relate this in iscsi world?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iscsi" rel="tag" title="see questions tagged &#39;iscsi&#39;">iscsi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '13, 15:39</strong></p><img src="https://secure.gravatar.com/avatar/2b038237e64839261fcc88e9fdef2b68?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krishnayeddula&#39;s gravatar image" /><p><span>krishnayeddula</span><br />
<span class="score" title="629 reputation points">629</span><span title="35 badges"><span class="badge1">●</span><span class="badgecount">35</span></span><span title="41 badges"><span class="silver">●</span><span class="badgecount">41</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krishnayeddula has 3 accepted answers">6%</span></p></div></div><div id="comments-container-19856" class="comments-container"><span id="20220"></span><div id="comment-20220" class="comment"><div id="post-20220-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Came across an aspect of it in RFC stating multiple iscsi connection support in same TCP Session.</p></blockquote><p>can you please add a reference to the RFC where you found that statement?</p></div><div id="comment-20220-info" class="comment-info"><span class="comment-age">(08 Apr '13, 16:26)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="20221"></span><div id="comment-20221" class="comment"><div id="post-20221-score" class="comment-score"></div><div class="comment-text"><p>Page.no 37:</p><p>Portal Groups - iSCSI supports multiple connections within the same session; some implementations will have the ability to combine connections in a session across multiple Network Portals.</p></div><div id="comment-20221-info" class="comment-info"><span class="comment-age">(08 Apr '13, 16:35)</span> <span class="comment-user userinfo">krishnayeddula</span></div></div></div><div id="comment-tools-19856" class="comment-tools"></div><div class="clear"></div><div id="comment-19856-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

