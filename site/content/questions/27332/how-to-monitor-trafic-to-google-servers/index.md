+++
type = "question"
title = "How to monitor trafic to google servers?"
description = '''I can no longer use google search . It says that my network is sending automatic requests. I discovered wireshark and I want to ask if I can use it to know what computer (lan ip) is sending all this queries to google. Maybe it has a virus or something. If It&#x27;s possible to do this, how do I do it ? I...'''
date = "2013-11-25T01:41:00Z"
lastmod = "2013-11-26T04:41:00Z"
weight = 27332
keywords = [ "trafic", "google" ]
aliases = [ "/questions/27332" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How to monitor trafic to google servers?](/questions/27332/how-to-monitor-trafic-to-google-servers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27332-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27332-score" class="post-score" title="current number of votes">1</div><span id="post-27332-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can no longer use google search . It says that my network is sending automatic requests. I discovered wireshark and I want to ask if I can use it to know what computer (lan ip) is sending all this queries to google. Maybe it has a virus or something.</p><p>If It's possible to do this, how do I do it ? Im not a network specialist.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-trafic" rel="tag" title="see questions tagged &#39;trafic&#39;">trafic</span> <span class="post-tag tag-link-google" rel="tag" title="see questions tagged &#39;google&#39;">google</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '13, 01:41</strong></p><img src="https://secure.gravatar.com/avatar/6570c8c95d4937063a9379282fc5b7b9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Loling%20Stones&#39;s gravatar image" /><p><span>Loling Stones</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Loling Stones has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Nov '13, 04:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-27332" class="comments-container"></div><div id="comment-tools-27332" class="comment-tools"></div><div class="clear"></div><div id="comment-27332-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="27346"></span>

<div id="answer-container-27346" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27346-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27346-score" class="post-score" title="current number of votes">0</div><span id="post-27346-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's possible for this to happen when you are using a proxy server; therefore, what I would suggest you do first is connect to Google through another internet connection to further see if it is the LAN that's actually causing this. I am still new to Wireshark as well, so I'd like to know the answer to your question as well. (:</p><p><strong>Good Luck!</strong></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Nov '13, 06:15</strong></p><img src="https://secure.gravatar.com/avatar/84b505ab399a12b3ad3715f0bdaf5652?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="GasShark&#39;s gravatar image" /><p><span>GasShark</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="GasShark has no accepted answers">0%</span></p></div></div><div id="comments-container-27346" class="comments-container"></div><div id="comment-tools-27346" class="comment-tools"></div><div class="clear"></div><div id="comment-27346-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="27350"></span>

<div id="answer-container-27350" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27350-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27350-score" class="post-score" title="current number of votes">0</div><span id="post-27350-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Here is what google recommends to identify the problem.</p><blockquote><p><a href="https://support.google.com/websearch/answer/86640">https://support.google.com/websearch/answer/86640</a></p></blockquote><p><strong>UPDATE</strong></p><blockquote><p>And what google recommends (installing a few malware /antivirus and scaning for malicios software) is not viable</p></blockquote><p>O.K. then you need to capture the traffic in front of your internet router (LAN side). Please read the following wiki how to do that</p><blockquote><p><a href="http://wiki.wireshark.org/CaptureSetup/Ethernet#Capture_using_a_monitor_mode_of_the_switch">http://wiki.wireshark.org/CaptureSetup/Ethernet#Capture_using_a_monitor_mode_of_the_switch</a></p></blockquote><p>You will need to mirror/monitor the switch port where your internet router is connected to. If you don't have a switch with port mirroring, these are your options:</p><ul><li>buy a small (cheap) switch with that capabilities: <a href="http://ask.wireshark.org/questions/13892/port-mirror-switch">http://ask.wireshark.org/questions/13892/port-mirror-switch</a></li><li>try to capture packets <strong>on</strong> your internet router if it supports that</li><li>use a PC with two interfaces in bridge mode to capture traffic in front of your internet router <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet#Capture_using_a_machine-in-the-middle">http://wiki.wireshark.org/CaptureSetup/Ethernet#Capture_using_a_machine-in-the-middle</a></li></ul><p>As soon as you can capture traffic, you need to run that capture for some time (maybe a few minutes, maybe a few hours, maybe days - if the offending device is currently switched off).</p><p><strong>BEWARE</strong>: You cannot simply run Wireshark for several hours/days, as your system will run out of RAM. Instead, please use <a href="http://www.wireshark.org/docs/man-pages/dumpcap.html">dumpcap</a> to capture only traffic to google servers (based on IP ranges). Please read the Wireshark docs to figure out how that works!</p><blockquote><p><a href="http://www.wireshark.org/docs/">http://www.wireshark.org/docs/</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Nov '13, 06:43</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Nov '13, 04:46</strong> </span></p></div></div><div id="comments-container-27350" class="comments-container"><span id="27412"></span><div id="comment-27412" class="comment"><div id="post-27412-score" class="comment-score"></div><div class="comment-text"><p>The problem exists on all lan's devices, not just a computer. And what google recommends (installing a few malware /antivirus and scaning for malicios software) is not viable if for example you got 50 pcs (i dont have that many) in the network. Also a lot of pcs got top-end antivirus software, so it can be other reason for the high requests sent to google.</p><p>I was able to bypass this issue using different google servers (not the one given by DNS), but It would be nice to know how to use Wireshark to find the root of the problem.</p></div><div id="comment-27412-info" class="comment-info"><span class="comment-age">(26 Nov '13, 04:16)</span> <span class="comment-user userinfo">Loling Stones</span></div></div><span id="27416"></span><div id="comment-27416" class="comment"><div id="post-27416-score" class="comment-score"></div><div class="comment-text"><p>see the <strong>UPDATE</strong> in my answer.</p></div><div id="comment-27416-info" class="comment-info"><span class="comment-age">(26 Nov '13, 04:41)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-27350" class="comment-tools"></div><div class="clear"></div><div id="comment-27350-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

