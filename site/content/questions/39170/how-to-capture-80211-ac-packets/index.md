+++
type = "question"
title = "How to capture 802.11 ac packets?"
description = '''Hi, I am a research student and I am stuck!I need to capture 802.11ac packets with the radiotap header on a linux machine. I have a TP-Link 802.11ac enabled router, a linux laptop with 802.11ac card, and I am trying to capture from a desktop in monitor mode. But am unable to capture even the 802.11n...'''
date = "2015-01-15T10:31:00Z"
lastmod = "2015-01-15T10:31:00Z"
weight = 39170
keywords = [ "radiotap", "802.11ac", "802.11n" ]
aliases = [ "/questions/39170" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture 802.11 ac packets?](/questions/39170/how-to-capture-80211-ac-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39170-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39170-score" class="post-score" title="current number of votes">0</div><span id="post-39170-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am a research student and I am stuck!I need to capture 802.11ac packets with the radiotap header on a linux machine. I have a TP-Link 802.11ac enabled router, a linux laptop with 802.11ac card, and I am trying to capture from a desktop in monitor mode. But am unable to capture even the 802.11n packets as the HT field is always 0 and the packets are 802.11g. Can anyone please help me with that.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-radiotap" rel="tag" title="see questions tagged &#39;radiotap&#39;">radiotap</span> <span class="post-tag tag-link-802.11ac" rel="tag" title="see questions tagged &#39;802.11ac&#39;">802.11ac</span> <span class="post-tag tag-link-802.11n" rel="tag" title="see questions tagged &#39;802.11n&#39;">802.11n</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '15, 10:31</strong></p><img src="https://secure.gravatar.com/avatar/41ae0380b039f8a29c5b55a0ff8d5900?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vkshah&#39;s gravatar image" /><p><span>vkshah</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vkshah has no accepted answers">0%</span></p></div></div><div id="comments-container-39170" class="comments-container"></div><div id="comment-tools-39170" class="comment-tools"></div><div class="clear"></div><div id="comment-39170-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

