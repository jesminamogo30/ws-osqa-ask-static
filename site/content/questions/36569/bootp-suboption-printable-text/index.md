+++
type = "question"
title = "bootp suboption printable text"
description = '''Does tshark have option 82 suboptions filter to display printable text? Applying circuit-id and remote-id will have hex value. In Wireshark we can analyze these hex value correlates to printable text.  What is the tshark filter to get option 82 printable text? Here is the example of data obtained fr...'''
date = "2014-09-24T10:54:00Z"
lastmod = "2014-09-24T10:54:00Z"
weight = 36569
keywords = [ "dhcp", "bootp" ]
aliases = [ "/questions/36569" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [bootp suboption printable text](/questions/36569/bootp-suboption-printable-text)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36569-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36569-score" class="post-score" title="current number of votes">0</div><span id="post-36569-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does tshark have option 82 suboptions filter to display printable text?</p><p>Applying circuit-id and remote-id will have hex value. In Wireshark we can analyze these hex value correlates to printable text.</p><p>What is the tshark filter to get option 82 printable text? Here is the example of data obtained from wireshark. How it's done in tshark?</p><p>Applied as filter: bootp.option.agent_information_option.agent_remote_id == 36:34:3a:37:36:3a:62:61:3a:39:37:3a:34:33:3a:34:38</p><p>Value: 36:34:3a:37:36:3a:62:61:3a:39:37:3a:34:33:3a:34:38</p><p>offset hex text: 0000 36 34 3a 37 36 3a 62 61 3a 39 37 3a 34 33 3a 34 46:67:ab:97:34:8 0010 38 4</p><p>Offset hex: 0000 36 34 3a 37 36 3a 62 61 3a 39 37 3a 34 33 3a 34 0010 38</p><p>printable text: 46:67:ba:79:43:84</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dhcp" rel="tag" title="see questions tagged &#39;dhcp&#39;">dhcp</span> <span class="post-tag tag-link-bootp" rel="tag" title="see questions tagged &#39;bootp&#39;">bootp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '14, 10:54</strong></p><img src="https://secure.gravatar.com/avatar/726ec604efa32511a5dc081e70b5b4d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tterminal&#39;s gravatar image" /><p><span>tterminal</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tterminal has no accepted answers">0%</span></p></div></div><div id="comments-container-36569" class="comments-container"></div><div id="comment-tools-36569" class="comment-tools"></div><div class="clear"></div><div id="comment-36569-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

