+++
type = "question"
title = "PDML field data"
description = '''Good evening all I&#x27;m writing because I&#x27;m sure I can have good helps and indication from the community. As first I must apologize because I&#x27;m not at all a network expert and not at all a WireShark guy. I&#x27;m a data analyst and I&#x27;m currently working for a Telco which want to get value of of data package...'''
date = "2014-06-02T06:01:00Z"
lastmod = "2014-06-02T07:13:00Z"
weight = 33272
keywords = [ "pdml", "decoding" ]
aliases = [ "/questions/33272" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [PDML field data](/questions/33272/pdml-field-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33272-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33272-score" class="post-score" title="current number of votes">0</div><span id="post-33272-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good evening all I'm writing because I'm sure I can have good helps and indication from the community. As first I must apologize because I'm not at all a network expert and not at all a WireShark guy. I'm a data analyst and I'm currently working for a Telco which want to get value of of data packages. They have trouble in get this data because they don't own the network infrastructure (political issue) but they captured some stream that I passed though WireShark. I was able to export PDML but I'm wondering that the most valuable information is in the 'data' field which comes in hexadecimal. Is there a way to apply an additional 'decoding' on this part of the PDML message?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pdml" rel="tag" title="see questions tagged &#39;pdml&#39;">pdml</span> <span class="post-tag tag-link-decoding" rel="tag" title="see questions tagged &#39;decoding&#39;">decoding</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '14, 06:01</strong></p><img src="https://secure.gravatar.com/avatar/17085755ee37863ef8454ce1353e40f5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sebastiano&#39;s gravatar image" /><p><span>Sebastiano</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sebastiano has no accepted answers">0%</span></p></div></div><div id="comments-container-33272" class="comments-container"><span id="33280"></span><div id="comment-33280" class="comment"><div id="post-33280-score" class="comment-score"></div><div class="comment-text"><p>The question is to open ended to answer, if you have TCP or UDP and then &lt;data&gt; you would have to know what protocol runs atop of UDP or TCP and do "decode as" or set the protocol preferenses accordingly.</p><p>"Telco which want to get value of of data packages" what type of data packets?</p></div><div id="comment-33280-info" class="comment-info"><span class="comment-age">(02 Jun '14, 07:13)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-33272" class="comment-tools"></div><div class="clear"></div><div id="comment-33272-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

