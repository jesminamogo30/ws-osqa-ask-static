+++
type = "question"
title = "Questions about analyze capture"
description = '''hello i new in wireshark and i have some questions about analyze a capture 1.how to find the average data bit rate of (in kbps) in capture 2.find how much different mac addresses in capture 3.find how many different ip addresses in capture 4.find how many different private ip addresses in capture 5....'''
date = "2016-12-28T03:11:00Z"
lastmod = "2016-12-28T08:17:00Z"
weight = 58395
keywords = [ "wireshark" ]
aliases = [ "/questions/58395" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Questions about analyze capture](/questions/58395/questions-about-analyze-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58395-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58395-score" class="post-score" title="current number of votes">0</div><span id="post-58395-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello</p><p>i new in wireshark and i have some questions about analyze a capture</p><p>1.how to find the average data bit rate of (in kbps) in capture</p><p>2.find how much different mac addresses in capture</p><p>3.find how many different ip addresses in capture</p><p>4.find how many different private ip addresses in capture</p><p>5.find how many different domain host name names in http traffic</p><p>6.find how many different http user-agent in capture</p><p>7.find how many png filea are contained in the capture</p><p>thanks for your help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Dec '16, 03:11</strong></p><img src="https://secure.gravatar.com/avatar/388a83f0dc0e0a2b82d8b32831407399?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="new-shark&#39;s gravatar image" /><p><span>new-shark</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="new-shark has no accepted answers">0%</span></p></div></div><div id="comments-container-58395" class="comments-container"><span id="58401"></span><div id="comment-58401" class="comment"><div id="post-58401-score" class="comment-score"></div><div class="comment-text"><p>Looks like a homework question, your classes should have given you the basic info to allow you to find the answers.</p><p>What have you tried?</p></div><div id="comment-58401-info" class="comment-info"><span class="comment-age">(28 Dec '16, 05:34)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="58405"></span><div id="comment-58405" class="comment"><div id="post-58405-score" class="comment-score"></div><div class="comment-text"><p>haha no i learn alone, to pass a exam for some course. i just stuck on this questions</p><p>i try to to use the filter, statistics and analyze but the results do not match the answers (american exam)</p><p>thanks for your comment :)</p></div><div id="comment-58405-info" class="comment-info"><span class="comment-age">(28 Dec '16, 08:17)</span> <span class="comment-user userinfo">new-shark</span></div></div></div><div id="comment-tools-58395" class="comment-tools"></div><div class="clear"></div><div id="comment-58395-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

