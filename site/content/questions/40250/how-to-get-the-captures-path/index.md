+++
type = "question"
title = "How to get the capture&#x27;s path?"
description = '''Hi, I am trying to get the path of the opened pcap file from a Lua dissector in order to write a text file there inside. So how could I get this path? Is there any non-method function?'''
date = "2015-03-04T02:13:00Z"
lastmod = "2015-06-27T22:38:00Z"
weight = 40250
keywords = [ "lua", "dissector", "pcap", "capture-file", "path" ]
aliases = [ "/questions/40250" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to get the capture's path?](/questions/40250/how-to-get-the-captures-path)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40250-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40250-score" class="post-score" title="current number of votes">0</div><span id="post-40250-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am trying to get the path of the opened pcap file from a Lua dissector in order to write a text file there inside. So how could I get this path? Is there any non-method function?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-capture-file" rel="tag" title="see questions tagged &#39;capture-file&#39;">capture-file</span> <span class="post-tag tag-link-path" rel="tag" title="see questions tagged &#39;path&#39;">path</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Mar '15, 02:13</strong></p><img src="https://secure.gravatar.com/avatar/c24621f6ca1c24900a51c41e96c6e303?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="FakerAlgebraico&#39;s gravatar image" /><p><span>FakerAlgebraico</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="FakerAlgebraico has no accepted answers">0%</span></p></div></div><div id="comments-container-40250" class="comments-container"></div><div id="comment-tools-40250" class="comment-tools"></div><div class="clear"></div><div id="comment-40250-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43624"></span>

<div id="answer-container-43624" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43624-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43624-score" class="post-score" title="current number of votes">1</div><span id="post-43624-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="FakerAlgebraico has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>[This is an old question, but I didn't see it until now.]</p><p>There is no current method to do that that I know of, but it's a good suggestion for an enhancement - so I've submitted bug <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11316">11316</a> to track it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '15, 22:38</strong></p><img src="https://secure.gravatar.com/avatar/d02f20c18a7742ec73a666f1974bf6dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hadriel&#39;s gravatar image" /><p><span>Hadriel</span><br />
<span class="score" title="2652 reputation points"><span>2.7k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="39 badges"><span class="bronze">●</span><span class="badgecount">39</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hadriel has 30 accepted answers">18%</span></p></div></div><div id="comments-container-43624" class="comments-container"></div><div id="comment-tools-43624" class="comment-tools"></div><div class="clear"></div><div id="comment-43624-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

