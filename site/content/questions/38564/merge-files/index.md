+++
type = "question"
title = "merge files"
description = '''I have big files to capture and i try to merge them but the files cannot open due to large files.. i seriously need help.. i want to display all those files in one io graph.. is it possible without merging them? '''
date = "2014-12-14T18:50:00Z"
lastmod = "2014-12-15T03:56:00Z"
weight = 38564
keywords = [ "large", "merge", "files" ]
aliases = [ "/questions/38564" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [merge files](/questions/38564/merge-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38564-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38564-score" class="post-score" title="current number of votes">0</div><span id="post-38564-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have big files to capture and i try to merge them but the files cannot open due to large files.. i seriously need help.. i want to display all those files in one io graph.. is it possible without merging them?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-large" rel="tag" title="see questions tagged &#39;large&#39;">large</span> <span class="post-tag tag-link-merge" rel="tag" title="see questions tagged &#39;merge&#39;">merge</span> <span class="post-tag tag-link-files" rel="tag" title="see questions tagged &#39;files&#39;">files</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Dec '14, 18:50</strong></p><img src="https://secure.gravatar.com/avatar/9b7893f0e3c3a52a2da99dd9d366627b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="miz_green&#39;s gravatar image" /><p><span>miz_green</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="miz_green has no accepted answers">0%</span></p></div></div><div id="comments-container-38564" class="comments-container"></div><div id="comment-tools-38564" class="comment-tools"></div><div class="clear"></div><div id="comment-38564-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38566"></span>

<div id="answer-container-38566" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38566-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38566-score" class="post-score" title="current number of votes">0</div><span id="post-38566-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, if you have a merged file that large that Wireshark won't open it, you can't IO graph anything, at least not with Wireshark. That would be a task for commercial tools like <a href="http://www.riverbed.com/products/performance-management-control/network-performance-management/High-Speed-Packet-Analysis.html">Pilot</a> (now renamed to "SteelCentral Packet Analyzer") I'm afraid.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Dec '14, 03:56</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-38566" class="comments-container"></div><div id="comment-tools-38566" class="comment-tools"></div><div class="clear"></div><div id="comment-38566-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

