+++
type = "question"
title = "wireshark v/s NS2 simulator"
description = '''what is the main difference between the use of WireShark and NS2? or how can we differentiate functioning of WireShark and NS2 ?'''
date = "2013-06-14T02:30:00Z"
lastmod = "2013-09-11T19:12:00Z"
weight = 22044
keywords = [ "wireshark" ]
aliases = [ "/questions/22044" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark v/s NS2 simulator](/questions/22044/wireshark-vs-ns2-simulator)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22044-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22044-score" class="post-score" title="current number of votes">0</div><span id="post-22044-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what is the main difference between the use of WireShark and NS2? or how can we differentiate functioning of WireShark and NS2 ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '13, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/e5bc502b2954fe6308a47c5f8d2fdead?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Smit&#39;s gravatar image" /><p><span>Smit</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Smit has no accepted answers">0%</span></p></div></div><div id="comments-container-22044" class="comments-container"><span id="24597"></span><div id="comment-24597" class="comment"><div id="post-24597-score" class="comment-score"></div><div class="comment-text"><p>can me to colaboration cisco boson netsim and wreshark? and what are the advantages of incorporation that I do this?</p></div><div id="comment-24597-info" class="comment-info"><span class="comment-age">(11 Sep '13, 19:09)</span> <span class="comment-user userinfo">Rizal Khadar...</span></div></div><span id="24598"></span><div id="comment-24598" class="comment"><div id="post-24598-score" class="comment-score"></div><div class="comment-text"><p>and if can do it, can you(admin) send me program of wireshark in my email? thank you abaout you(admin) attention :)</p></div><div id="comment-24598-info" class="comment-info"><span class="comment-age">(11 Sep '13, 19:12)</span> <span class="comment-user userinfo">Rizal Khadar...</span></div></div></div><div id="comment-tools-22044" class="comment-tools"></div><div class="clear"></div><div id="comment-22044-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22046"></span>

<div id="answer-container-22046" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22046-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22046-score" class="post-score" title="current number of votes">0</div><span id="post-22046-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>ns2 is a network <strong>simulator</strong>, meaning you can build virtual networks with it. Wireshark is a <strong>network analysis</strong> tool, so it will read network capture files and help you to analyze network problems. ns2 is also able to generate network capture files of the traffic in the virtual network.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jun '13, 03:03</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-22046" class="comments-container"></div><div id="comment-tools-22046" class="comment-tools"></div><div class="clear"></div><div id="comment-22046-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

