+++
type = "question"
title = "Can re-transmissions from a single ip identify the source of a problem?"
description = '''WE have a very large enterprise application located in a data-center in Boston. Users are all over the State (WAN MPLS). In analyzing a WireSherk Trace at several clients, the source of all of the transmissions was a load balancer (software CISCO 6509) that front end three Apache servers that distri...'''
date = "2013-10-02T14:44:00Z"
lastmod = "2013-10-02T14:44:00Z"
weight = 25557
keywords = [ "retransmissions" ]
aliases = [ "/questions/25557" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can re-transmissions from a single ip identify the source of a problem?](/questions/25557/can-re-transmissions-from-a-single-ip-identify-the-source-of-a-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25557-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25557-score" class="post-score" title="current number of votes">0</div><span id="post-25557-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>WE have a very large enterprise application located in a data-center in Boston. Users are all over the State (WAN MPLS). In analyzing a WireSherk Trace at several clients, the source of all of the transmissions was a load balancer (software CISCO 6509) that front end three Apache servers that distribute the database request into a multi-tier environment. All the retransmission are indeed retransmissions from this one load balancer device. The retransmissions coming from this load-balancer represent about 1.5% of all the traffic in this capture.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/10-2-2013_4-24-22_PM.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-retransmissions" rel="tag" title="see questions tagged &#39;retransmissions&#39;">retransmissions</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Oct '13, 14:44</strong></p><img src="https://secure.gravatar.com/avatar/16c80ca493c77f3486cbb7ff38cc5d3d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zoberist&#39;s gravatar image" /><p><span>Zoberist</span><br />
<span class="score" title="0 reputation points">0</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zoberist has no accepted answers">0%</span></p></img></div></div><div id="comments-container-25557" class="comments-container"></div><div id="comment-tools-25557" class="comment-tools"></div><div class="clear"></div><div id="comment-25557-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

