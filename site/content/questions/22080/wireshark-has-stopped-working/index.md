+++
type = "question"
title = "wireshark has stopped working"
description = '''hi, when I start wireshark my wi-fi interfaces is not displayed and when I check and apply local interfaces in interface options it is showing &quot;wireshark has stopped working&quot;(A problem caused by the program to stop working correctly. windows will close the program and notify you if solution is avail...'''
date = "2013-06-14T22:57:00Z"
lastmod = "2013-06-21T20:27:00Z"
weight = 22080
keywords = [ "interfaces" ]
aliases = [ "/questions/22080" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark has stopped working](/questions/22080/wireshark-has-stopped-working)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22080-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22080-score" class="post-score" title="current number of votes">0</div><span id="post-22080-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi,</p><p>when I start wireshark my wi-fi interfaces is not displayed and when I check and apply local interfaces in interface options it is showing "wireshark has stopped working"(A problem caused by the program to stop working correctly. windows will close the program and notify you if solution is available). can I know what problem is this? and how to overcome this problem? I tried with reinstallation also.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '13, 22:57</strong></p><img src="https://secure.gravatar.com/avatar/cc0f627200f978a2b79eac84ddbfca6f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="monisha&#39;s gravatar image" /><p><span>monisha</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="monisha has no accepted answers">0%</span></p></div></div><div id="comments-container-22080" class="comments-container"><span id="22238"></span><div id="comment-22238" class="comment"><div id="post-22238-score" class="comment-score"></div><div class="comment-text"><p>You neglected to provide enough information to allow anyone to help you. What version of Wireshark are you using? What is your OS? Basically, as a first step, run Wireshark and copy all the information from Help -&gt; About.</p><p>Also, did you specify any special preferences? Wireshark -&gt; Help -&gt; About -&gt; Folders will tell you the location of your preference files, as identified by the "Personal configuration" link. Double-click to open the folder. Temporarily move all preferences, if any, somewhere else and start with a pristine folder. Try Wireshark now to see if there is any behavioral difference. If so, try to narrow down the preference that is causing the crash; if not, we can try to take it from there ...</p></div><div id="comment-22238-info" class="comment-info"><span class="comment-age">(21 Jun '13, 20:27)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-22080" class="comment-tools"></div><div class="clear"></div><div id="comment-22080-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

