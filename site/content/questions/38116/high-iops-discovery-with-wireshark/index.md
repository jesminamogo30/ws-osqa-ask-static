+++
type = "question"
title = "High iops discovery with wireshark"
description = '''Would it be possible to use Wireshark to determine problems with NAS or filesystem iops using Wireshark since there is a coloration to drive iops from data being set over TCP/IP? I have an ERP system that uses a file based data base so that transactions go across smb. If I had an established baselin...'''
date = "2014-11-24T20:15:00Z"
lastmod = "2014-11-24T20:15:00Z"
weight = 38116
keywords = [ "iops", "drive", "tcp", "smb" ]
aliases = [ "/questions/38116" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [High iops discovery with wireshark](/questions/38116/high-iops-discovery-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38116-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38116-score" class="post-score" title="current number of votes">0</div><span id="post-38116-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Would it be possible to use Wireshark to determine problems with NAS or filesystem iops using Wireshark since there is a coloration to drive iops from data being set over TCP/IP? I have an ERP system that uses a file based data base so that transactions go across smb. If I had an established baseline for on operation to open and close a transition could I establish problems with drive iops latency over tcp/ip connectivity issues?</p><p>Tim</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iops" rel="tag" title="see questions tagged &#39;iops&#39;">iops</span> <span class="post-tag tag-link-drive" rel="tag" title="see questions tagged &#39;drive&#39;">drive</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-smb" rel="tag" title="see questions tagged &#39;smb&#39;">smb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '14, 20:15</strong></p><img src="https://secure.gravatar.com/avatar/befa1a780be263ceb21ab112600f6ff9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="av8r&#39;s gravatar image" /><p><span>av8r</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="av8r has no accepted answers">0%</span></p></div></div><div id="comments-container-38116" class="comments-container"></div><div id="comment-tools-38116" class="comment-tools"></div><div class="clear"></div><div id="comment-38116-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

