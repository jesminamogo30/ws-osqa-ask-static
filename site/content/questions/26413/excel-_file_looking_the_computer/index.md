+++
type = "question"
title = "excel _file_looking_the_computer"
description = '''https://www.cloudshark.org/captures/2daa822a7a25 I have a user that who states that see is attempting to attach a Excel file to here e-mail outlook  The user states that the computer just clocks an sh can’t do anything. The folder where the attachment is on a folder in a datacenter, and this is a V-...'''
date = "2013-10-25T13:06:00Z"
lastmod = "2013-10-26T12:14:00Z"
weight = 26413
keywords = [ "capture", "excel" ]
aliases = [ "/questions/26413" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [excel \_file\_looking\_the\_computer](/questions/26413/excel-_file_looking_the_computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26413-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26413-score" class="post-score" title="current number of votes">0</div><span id="post-26413-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><a href="https://www.cloudshark.org/captures/2daa822a7a25">https://www.cloudshark.org/captures/2daa822a7a25</a></p><p>I have a user that who states that see is attempting to attach a Excel file to here e-mail outlook The user states that the computer just clocks an sh can’t do anything.</p><p>The folder where the attachment is on a folder in a datacenter, and this is a V-filer on the storage device.</p><p>I have uploaded a copy of the trace, and I have looked through this track and can’t find anything that would cause this in the trace. I JUST DON’T UNDERSTAND what is going on here Can someone please help with issue thanks?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-excel" rel="tag" title="see questions tagged &#39;excel&#39;">excel</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '13, 13:06</strong></p><img src="https://secure.gravatar.com/avatar/530b55f3fcb17b760aabdf113d9318aa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ejohnson7&#39;s gravatar image" /><p><span>ejohnson7</span><br />
<span class="score" title="11 reputation points">11</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ejohnson7 has no accepted answers">0%</span></p></div></div><div id="comments-container-26413" class="comments-container"><span id="26414"></span><div id="comment-26414" class="comment"><div id="post-26414-score" class="comment-score"></div><div class="comment-text"><p>the two computer in question are 10.97.247.50 &lt; --- &gt; 10.184.193.205, als I am seeing a small window size on in some of the Captures also I just dont get it</p></div><div id="comment-26414-info" class="comment-info"><span class="comment-age">(25 Oct '13, 13:11)</span> <span class="comment-user userinfo">ejohnson7</span></div></div><span id="26428"></span><div id="comment-26428" class="comment"><div id="post-26428-score" class="comment-score"></div><div class="comment-text"><p>The capture file isn't too useful to me without the TCP 3-way handshake. Also, if you're able to, it might help to capture at both the client and server.</p></div><div id="comment-26428-info" class="comment-info"><span class="comment-age">(26 Oct '13, 12:14)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-26413" class="comment-tools"></div><div class="clear"></div><div id="comment-26413-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

