+++
type = "question"
title = "RTMP Unknown (0x0)"
description = '''Im investigating an issue where our clients are having issues streaming from bbc.co.uk. The issue might not be exclusivly bbc.co.uk but this is where we have noticed it and been using for testing.  Quite often the stream fails to play at all or buffers alot. This appears to happen at a time when the...'''
date = "2016-01-12T07:35:00Z"
lastmod = "2016-01-12T10:35:00Z"
weight = 49127
keywords = [ "rtmp" ]
aliases = [ "/questions/49127" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [RTMP Unknown (0x0)](/questions/49127/rtmp-unknown-0x0)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49127-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49127-score" class="post-score" title="current number of votes">0</div><span id="post-49127-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Im investigating an issue where our clients are having issues streaming from bbc.co.uk. The issue might not be exclusivly bbc.co.uk but this is where we have noticed it and been using for testing.<br />
</p><p>Quite often the stream fails to play at all or buffers alot. This appears to happen at a time when their is a higher volume of activity on the network/proxy servers.<br />
</p><p>I have notice from packet captures that during the times where it fails to play we see RTMP unkown (0x0) packets.<img src="https://osqa-ask.wireshark.org/upfiles/packet_capture.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtmp" rel="tag" title="see questions tagged &#39;rtmp&#39;">rtmp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jan '16, 07:35</strong></p><img src="https://secure.gravatar.com/avatar/9d6c56d430fefa11a9d3908b1bb47c21?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Colin%20Bell&#39;s gravatar image" /><p><span>Colin Bell</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Colin Bell has no accepted answers">0%</span> </br></br></p></img></div></div><div id="comments-container-49127" class="comments-container"><span id="49136"></span><div id="comment-49136" class="comment"><div id="post-49136-score" class="comment-score"></div><div class="comment-text"><blockquote><p><strong>higher volume of activity</strong> on the network/<strong>proxy servers</strong></p></blockquote><p>What kind of proxies are these and what are they doing to the RTMP traffic (AV scanning, etc.)?</p></div><div id="comment-49136-info" class="comment-info"><span class="comment-age">(12 Jan '16, 10:35)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-49127" class="comment-tools"></div><div class="clear"></div><div id="comment-49127-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

