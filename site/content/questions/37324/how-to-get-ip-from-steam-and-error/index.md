+++
type = "question"
title = "How to get ip from steam and error."
description = '''Well I used to do it easy. You type &quot;classicstun&quot; into filter and call them and their ip appears but now It won&#x27;t appear unless they accept. is there a way around this?. I also been getting an error in wireshark saying TCP DUP ACK. Does this mean someone is DDosing me or something? my internet is co...'''
date = "2014-10-23T22:16:00Z"
lastmod = "2014-10-26T15:32:00Z"
weight = 37324
keywords = [ "steamclient" ]
aliases = [ "/questions/37324" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to get ip from steam and error.](/questions/37324/how-to-get-ip-from-steam-and-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37324-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37324-score" class="post-score" title="current number of votes">0</div><span id="post-37324-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Well I used to do it easy. You type "classicstun" into filter and call them and their ip appears but now It won't appear unless they accept. is there a way around this?. I also been getting an error in wireshark saying TCP DUP ACK. Does this mean someone is DDosing me or something? my internet is completley fine</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-steamclient" rel="tag" title="see questions tagged &#39;steamclient&#39;">steamclient</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Oct '14, 22:16</strong></p><img src="https://secure.gravatar.com/avatar/6ca1605467b8a95cad016bbffe5c1d10?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="john8734&#39;s gravatar image" /><p><span>john8734</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="john8734 has no accepted answers">0%</span></p></div></div><div id="comments-container-37324" class="comments-container"><span id="37359"></span><div id="comment-37359" class="comment"><div id="post-37359-score" class="comment-score"></div><div class="comment-text"><p>I'm sorry, but I honestly don't understand what you are asking for, as the following scentence does not make any sense for me.</p><blockquote><p>call them and their ip appears but now It won't appear unless they accept. is there a way around this?.</p></blockquote><p>If you need/want help, please add some <strong>techincal</strong> information, like</p><ul><li>your OS and OS version</li><li>your Wirshark version</li><li><strong>what</strong> are you <strong>exactly</strong> trying to do</li><li>what protocols are you using</li><li>can you provide a capture file (I mean pcap, not a screenshot)</li></ul><p>Regards<br />
Kurt</p></div><div id="comment-37359-info" class="comment-info"><span class="comment-age">(26 Oct '14, 15:32)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-37324" class="comment-tools"></div><div class="clear"></div><div id="comment-37324-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

