+++
type = "question"
title = "ipv4 colums at main window"
description = '''Hallo , what I have to do to show the ipv4 in the main window. I have only the mac addresse as src or dest address, I think it is a smal thing for the community. thanks Götz'''
date = "2017-04-23T09:54:00Z"
lastmod = "2017-04-23T11:05:00Z"
weight = 60989
keywords = [ "ipv4" ]
aliases = [ "/questions/60989" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ipv4 colums at main window](/questions/60989/ipv4-colums-at-main-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60989-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60989-score" class="post-score" title="current number of votes">0</div><span id="post-60989-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hallo ,</p><p>what I have to do to show the ipv4 in the main window. I have only the mac addresse as src or dest address, I think it is a smal thing for the community.</p><p>thanks</p><p>Götz</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipv4" rel="tag" title="see questions tagged &#39;ipv4&#39;">ipv4</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Apr '17, 09:54</strong></p><img src="https://secure.gravatar.com/avatar/27c1620987e88c1c8ee33a7c583f375f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="macosx&#39;s gravatar image" /><p><span>macosx</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="macosx has no accepted answers">0%</span></p></div></div><div id="comments-container-60989" class="comments-container"></div><div id="comment-tools-60989" class="comment-tools"></div><div class="clear"></div><div id="comment-60989-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60991"></span>

<div id="answer-container-60991" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60991-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60991-score" class="post-score" title="current number of votes">0</div><span id="post-60991-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I assume you're interested in having the packet's IPv4 source and destination addresses displayed in the packet list pane? Those fields will be displayed in the Source and Destination columns for all IPv4 packets. Perhaps the packet you're looking at is not an IPv4 packet, perhaps it's an ARP packet, for example, in which case there is no IPv4 source and destination addresses to display. What is the protocol indicated for the packet that you're looking at? If you know it's an IPv4 packet, then perhaps the IPv4 dissector is disabled, in which case, you will have to enable it first by checking the <code>Analyze -&gt; Enabled Protocols -&gt; IPv4</code> box.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Apr '17, 11:05</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-60991" class="comments-container"></div><div id="comment-tools-60991" class="comment-tools"></div><div class="clear"></div><div id="comment-60991-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

