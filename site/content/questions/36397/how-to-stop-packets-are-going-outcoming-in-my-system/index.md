+++
type = "question"
title = "[closed] How to stop packets are going-out/coming-in my system ?"
description = '''I am trying to do some specific works with Wireshark for my project. Google couldn&#x27;t help me! So I put it there. Is it possible to stop (get and destroy them) some specific packets (like: IMAP traffics) from going-out/coming-in my system ? And is it also possible to get some specific packets (like: ...'''
date = "2014-09-17T04:03:00Z"
lastmod = "2014-09-17T04:47:00Z"
weight = 36397
keywords = [ "packet-capture", "imap", "packet", "wireshark" ]
aliases = [ "/questions/36397" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [\[closed\] How to stop packets are going-out/coming-in my system ?](/questions/36397/how-to-stop-packets-are-going-outcoming-in-my-system)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36397-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36397-score" class="post-score" title="current number of votes">0</div><span id="post-36397-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to do some specific works with Wireshark for my project. Google couldn't help me! So I put it there.</p><p>Is it possible to stop (get and destroy them) some specific packets (like: IMAP traffics) from going-out/coming-in my system ?</p><p>And is it also possible to get some specific packets (like: IMAP traffics), modify them and then, let them continue their way ... ? Imagine, I am receiving an email from YAHOO email-service (some IMAP packets are coming-in my system from Yahoo servers actually); Now, I have to get them (before being received), modify them and THEN let them to be received ? Is it possible ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-imap" rel="tag" title="see questions tagged &#39;imap&#39;">imap</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Sep '14, 04:03</strong></p><img src="https://secure.gravatar.com/avatar/3e400a04bfee6fe63d87f97d3584cb8d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hamed%20Shams&#39;s gravatar image" /><p><span>Hamed Shams</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hamed Shams has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>17 Sep '14, 04:47</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-36397" class="comments-container"></div><div id="comment-tools-36397" class="comment-tools"></div><div class="clear"></div><div id="comment-36397-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 17 Sep '14, 04:47

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36398"></span>

<div id="answer-container-36398" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36398-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36398-score" class="post-score" title="current number of votes">0</div><span id="post-36398-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Hamed Shams has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark cannot help you here. Look into iptables, ettercap or the like.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Sep '14, 04:47</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-36398" class="comments-container"></div><div id="comment-tools-36398" class="comment-tools"></div><div class="clear"></div><div id="comment-36398-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

