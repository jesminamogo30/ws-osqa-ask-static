+++
type = "question"
title = "Unique Wi-Fi probe packets"
description = '''Hi There, I always detected a probe packets from various device but I dont see a probe response. Attached the file at we transfer ; Download link  http://we.tl/V4Yywkm5ES  I kind of feeling this packet something unique inside. Let me know.'''
date = "2015-08-01T02:02:00Z"
lastmod = "2015-08-03T09:34:00Z"
weight = 44722
keywords = [ "packets" ]
aliases = [ "/questions/44722" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unique Wi-Fi probe packets](/questions/44722/unique-wi-fi-probe-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44722-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44722-score" class="post-score" title="current number of votes">0</div><span id="post-44722-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi There,</p><p>I always detected a probe packets from various device but I dont see a probe response. Attached the file at we transfer ;</p><p>Download link <a href="http://we.tl/V4Yywkm5ES">http://we.tl/V4Yywkm5ES</a></p><p>I kind of feeling this packet something unique inside.</p><p>Let me know.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Aug '15, 02:02</strong></p><img src="https://secure.gravatar.com/avatar/9b7c129577c67f2c99ed567977a59b22?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NewMill&#39;s gravatar image" /><p><span>NewMill</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NewMill has no accepted answers">0%</span></p></div></div><div id="comments-container-44722" class="comments-container"></div><div id="comment-tools-44722" class="comment-tools"></div><div class="clear"></div><div id="comment-44722-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44748"></span>

<div id="answer-container-44748" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44748-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44748-score" class="post-score" title="current number of votes">0</div><span id="post-44748-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That Probe Request is from a device that supports P2P or better known as WiFi Direct. It is trying to make a connection using P2P. Only devices that support P2P will respond.</p><p>See other post here:</p><p><a href="https://ask.wireshark.org/questions/44718/what-is-the-device-about-wi-fiall-p2p-in-pcap-file">https://ask.wireshark.org/questions/44718/what-is-the-device-about-wi-fiall-p2p-in-pcap-file</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Aug '15, 17:23</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-44748" class="comments-container"><span id="44753"></span><div id="comment-44753" class="comment"><div id="post-44753-score" class="comment-score"></div><div class="comment-text"><ol><li>Client ( Devices ) sent probe requests ( Broadcast) but none of Access<br />
point send Probe Response 2.There is specific probe request to SSID=Samly, but this SSID not visible<br />
on the flight. ( where is the SSID ? ) . By right, for SSID=Samly should<br />
be a Beacon Frames in which enable Laptop, Phone or other clients can see<br />
the Access Point but on our case we dont see Beacon Frames. I kind of feeling something strange on this.</li></ol></div><div id="comment-44753-info" class="comment-info"><span class="comment-age">(02 Aug '15, 23:56)</span> <span class="comment-user userinfo">NewMill</span></div></div><span id="44792"></span><div id="comment-44792" class="comment"><div id="post-44792-score" class="comment-score"></div><div class="comment-text"><p>Are there any Beacons with the SSID that you are trying to connect to? (SSID=Samly)</p></div><div id="comment-44792-info" class="comment-info"><span class="comment-age">(03 Aug '15, 09:34)</span> <span class="comment-user userinfo">Amato_C</span></div></div></div><div id="comment-tools-44748" class="comment-tools"></div><div class="clear"></div><div id="comment-44748-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

