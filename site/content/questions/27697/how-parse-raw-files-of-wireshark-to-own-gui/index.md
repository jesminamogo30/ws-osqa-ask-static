+++
type = "question"
title = "How parse raw files of wireshark to own GUI ?"
description = '''Hi everyone! Somebody tried to parse raw files of wireshark? You can read and parse the file and display the most?'''
date = "2013-12-03T04:15:00Z"
lastmod = "2013-12-03T21:09:00Z"
weight = 27697
keywords = [ "parsing" ]
aliases = [ "/questions/27697" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How parse raw files of wireshark to own GUI ?](/questions/27697/how-parse-raw-files-of-wireshark-to-own-gui)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27697-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27697-score" class="post-score" title="current number of votes">0</div><span id="post-27697-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone! Somebody tried to parse raw files of wireshark? You can read and parse the file and display the most?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-parsing" rel="tag" title="see questions tagged &#39;parsing&#39;">parsing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Dec '13, 04:15</strong></p><img src="https://secure.gravatar.com/avatar/0f2d15c8c5fb64794cf0125c807d1399?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Larush&#39;s gravatar image" /><p><span>Larush</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Larush has no accepted answers">0%</span></p></div></div><div id="comments-container-27697" class="comments-container"></div><div id="comment-tools-27697" class="comment-tools"></div><div class="clear"></div><div id="comment-27697-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27704"></span>

<div id="answer-container-27704" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27704-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27704-score" class="post-score" title="current number of votes">1</div><span id="post-27704-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The "raw" file is pcap or pcap-ng a binary format containing the raw frame as seen by the capture device roughly speaking. Several capture application can read pcap(-ng) files. Not sure what you are realy asking. Yes you can create your own application to read libpcap files and display that in a GUI but it's a lot of work.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Dec '13, 05:47</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-27704" class="comments-container"><span id="27742"></span><div id="comment-27742" class="comment"><div id="post-27742-score" class="comment-score"></div><div class="comment-text"><p>because I do not like GUI wireshark, think to use it only for the formation of the raw file (for example, every 10 seconds). A different program (may be own) to read files and display them on the screen of the content may by block 10 seconds.</p></div><div id="comment-27742-info" class="comment-info"><span class="comment-age">(03 Dec '13, 21:09)</span> <span class="comment-user userinfo">Larush</span></div></div></div><div id="comment-tools-27704" class="comment-tools"></div><div class="clear"></div><div id="comment-27704-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

