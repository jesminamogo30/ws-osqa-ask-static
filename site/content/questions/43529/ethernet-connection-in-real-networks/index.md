+++
type = "question"
title = "Ethernet Connection in Real Networks"
description = '''Regarding the Ethernet connection between a host machine and a router/Switch. Does the Ethernet protocol keeps using CSMA/CD since nowadays we have only point to point connection it does not use that mechanism since it is a waste of resources and we do not have any shared medium as before. In additi...'''
date = "2015-06-24T17:17:00Z"
lastmod = "2015-06-25T03:47:00Z"
weight = 43529
keywords = [ "llc", "ethernet", "dix" ]
aliases = [ "/questions/43529" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Ethernet Connection in Real Networks](/questions/43529/ethernet-connection-in-real-networks)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43529-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43529-score" class="post-score" title="current number of votes">0</div><span id="post-43529-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Regarding the Ethernet connection between a host machine and a router/Switch. Does the Ethernet protocol keeps using CSMA/CD since nowadays we have only point to point connection it does not use that mechanism since it is a waste of resources and we do not have any shared medium as before.</p><p>In addition, what is the most common used encapsulation mode for Ethernet is it with Dix (Ethernet-II) or LLC?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-llc" rel="tag" title="see questions tagged &#39;llc&#39;">llc</span> <span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-dix" rel="tag" title="see questions tagged &#39;dix&#39;">dix</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jun '15, 17:17</strong></p><img src="https://secure.gravatar.com/avatar/566cfe38b17a31f0dc825c86538cf3d4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hany%20Assasa&#39;s gravatar image" /><p><span>Hany Assasa</span><br />
<span class="score" title="21 reputation points">21</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hany Assasa has no accepted answers">0%</span></p></div></div><div id="comments-container-43529" class="comments-container"></div><div id="comment-tools-43529" class="comment-tools"></div><div class="clear"></div><div id="comment-43529-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43539"></span>

<div id="answer-container-43539" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43539-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43539-score" class="post-score" title="current number of votes">2</div><span id="post-43539-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Hany Assasa has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Does the Ethernet protocol keeps using CSMA/CD</p></blockquote><p>Yes, if the link is in half-duplex mode. In full-duplex mode you don't need it, as you said. Hence it's not being used in full-duplex mode.</p><p>Today the most common frame format is Ethernet II (DIX).</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jun '15, 03:47</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Jun '15, 03:55</strong> </span></p></div></div><div id="comments-container-43539" class="comments-container"></div><div id="comment-tools-43539" class="comment-tools"></div><div class="clear"></div><div id="comment-43539-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

