+++
type = "question"
title = "Column Sort Order"
description = '''The ability to sort the column order by clicking on the column header seems to have disappeared. Sort options on right mouse-click are greyed out also. Tried searching &#x27;help&#x27; and &#x27;faq&#x27;s&#x27; also but not show any results.'''
date = "2013-09-24T03:40:00Z"
lastmod = "2013-09-24T03:41:00Z"
weight = 25154
keywords = [ "column", "sort", "order" ]
aliases = [ "/questions/25154" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Column Sort Order](/questions/25154/column-sort-order)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25154-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25154-score" class="post-score" title="current number of votes">0</div><span id="post-25154-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The ability to sort the column order by clicking on the column header seems to have disappeared. Sort options on right mouse-click are greyed out also. Tried searching 'help' and 'faq's' also but not show any results.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-column" rel="tag" title="see questions tagged &#39;column&#39;">column</span> <span class="post-tag tag-link-sort" rel="tag" title="see questions tagged &#39;sort&#39;">sort</span> <span class="post-tag tag-link-order" rel="tag" title="see questions tagged &#39;order&#39;">order</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '13, 03:40</strong></p><img src="https://secure.gravatar.com/avatar/42d11708d83d0ffd208af4f1ff8e598b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Beans&#39;s gravatar image" /><p><span>Beans</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Beans has no accepted answers">0%</span></p></div></div><div id="comments-container-25154" class="comments-container"><span id="25155"></span><div id="comment-25155" class="comment"><div id="post-25155-score" class="comment-score"></div><div class="comment-text"><p>Which version and OS, and do you have a screenshot?</p></div><div id="comment-25155-info" class="comment-info"><span class="comment-age">(24 Sep '13, 03:41)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-25154" class="comment-tools"></div><div class="clear"></div><div id="comment-25154-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

