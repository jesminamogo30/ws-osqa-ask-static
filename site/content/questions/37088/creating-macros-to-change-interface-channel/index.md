+++
type = "question"
title = "Creating macros to change interface channel"
description = '''Hi,  I looked through the site and couldn&#x27;t find any similar question. There are macros for display filters but is there a way to crate a shortcut key or macro for accessing the wireless settings box for changing the sniffer channel? Thanks'''
date = "2014-10-15T17:42:00Z"
lastmod = "2014-10-15T17:42:00Z"
weight = 37088
keywords = [ "interface", "macro", "sniffer", "channel" ]
aliases = [ "/questions/37088" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Creating macros to change interface channel](/questions/37088/creating-macros-to-change-interface-channel)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37088-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37088-score" class="post-score" title="current number of votes">0</div><span id="post-37088-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I looked through the site and couldn't find any similar question. There are macros for display filters but is there a way to crate a shortcut key or macro for accessing the wireless settings box for changing the sniffer channel?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-macro" rel="tag" title="see questions tagged &#39;macro&#39;">macro</span> <span class="post-tag tag-link-sniffer" rel="tag" title="see questions tagged &#39;sniffer&#39;">sniffer</span> <span class="post-tag tag-link-channel" rel="tag" title="see questions tagged &#39;channel&#39;">channel</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '14, 17:42</strong></p><img src="https://secure.gravatar.com/avatar/8674f48b611630bf552aa74b41c87ebb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zathurian&#39;s gravatar image" /><p><span>zathurian</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zathurian has no accepted answers">0%</span></p></div></div><div id="comments-container-37088" class="comments-container"></div><div id="comment-tools-37088" class="comment-tools"></div><div class="clear"></div><div id="comment-37088-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

