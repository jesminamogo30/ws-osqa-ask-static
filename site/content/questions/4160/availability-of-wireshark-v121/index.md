+++
type = "question"
title = "Availability of Wireshark v1.2.1"
description = '''Is Wireshark v1.2.1 available for download? If not, is currently available v1.2.16 fully compatible with v1.2.1?'''
date = "2011-05-20T07:15:00Z"
lastmod = "2011-05-20T08:11:00Z"
weight = 4160
keywords = [ "download", "compatibility" ]
aliases = [ "/questions/4160" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Availability of Wireshark v1.2.1](/questions/4160/availability-of-wireshark-v121)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4160-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4160-score" class="post-score" title="current number of votes">0</div><span id="post-4160-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is Wireshark v1.2.1 available for download? If not, is currently available v1.2.16 fully compatible with v1.2.1?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-compatibility" rel="tag" title="see questions tagged &#39;compatibility&#39;">compatibility</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 May '11, 07:15</strong></p><img src="https://secure.gravatar.com/avatar/27c1eaebb792caedcf6d1cdc86212e70?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MAC&#39;s gravatar image" /><p><span>MAC</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MAC has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>27 May '11, 20:49</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-4160" class="comments-container"></div><div id="comment-tools-4160" class="comment-tools"></div><div class="clear"></div><div id="comment-4160-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="4161"></span>

<div id="answer-container-4161" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4161-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4161-score" class="post-score" title="current number of votes">2</div><span id="post-4161-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The various 1.2... releases were done to fix <em>many</em> bugs; AFAIK there were no functionality changes.</p><p>So if by "fully compatible with v1.2.1" you mean bug fixes only, that should indeed be the case.</p><p>What is the reason you wish to continue using Wireshark 1.2... ? The latest Wireshark(stable) 1.4 release contains improvements and added functionality.</p><p>See:</p><p><a href="http://wiki.wireshark.org/Development/ReleasePolicy">http://wiki.wireshark.org/Development/ReleasePolicy</a></p><p><a href="http://wiki.wireshark.org/Development/ReleaseNumbers">http://wiki.wireshark.org/Development/ReleaseNumbers</a></p><p>You can also review the release notes for the various 1.2... releases (starting in July 2009) at</p><p><a href="http://www.wireshark.org/docs/relnotes/">http://www.wireshark.org/docs/relnotes/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 May '11, 07:31</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 May '11, 07:59</strong> </span></p></div></div><div id="comments-container-4161" class="comments-container"></div><div id="comment-tools-4161" class="comment-tools"></div><div class="clear"></div><div id="comment-4161-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="4163"></span>

<div id="answer-container-4163" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4163-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4163-score" class="post-score" title="current number of votes">2</div><span id="post-4163-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Start <a href="http://www.wireshark.org/download/">here</a> and traverse the branch that you seek. There's an all-versions directory in there where all older releases can be found.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 May '11, 08:11</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-4163" class="comments-container"></div><div id="comment-tools-4163" class="comment-tools"></div><div class="clear"></div><div id="comment-4163-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

