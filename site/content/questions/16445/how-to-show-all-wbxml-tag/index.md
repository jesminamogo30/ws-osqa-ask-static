+++
type = "question"
title = "How to show all wbxml tag?"
description = '''Many wbxml tags are not shown on Wireshark. (but still many other tags are shown) It just show &quot;Requested token code page not defined for this content type&quot; How to solve this problem.'''
date = "2012-11-29T17:59:00Z"
lastmod = "2012-11-29T23:05:00Z"
weight = 16445
keywords = [ "tag", "wbxml" ]
aliases = [ "/questions/16445" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to show all wbxml tag?](/questions/16445/how-to-show-all-wbxml-tag)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16445-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16445-score" class="post-score" title="current number of votes">0</div><span id="post-16445-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Many wbxml tags are not shown on Wireshark. (but still many other tags are shown)</p><p>It just show "Requested token code page not defined for this content type"</p><p>How to solve this problem.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tag" rel="tag" title="see questions tagged &#39;tag&#39;">tag</span> <span class="post-tag tag-link-wbxml" rel="tag" title="see questions tagged &#39;wbxml&#39;">wbxml</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Nov '12, 17:59</strong></p><img src="https://secure.gravatar.com/avatar/4a676d7e4803b4c76e93fff20f54fd78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="edgar&#39;s gravatar image" /><p><span>edgar</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="edgar has no accepted answers">0%</span></p></div></div><div id="comments-container-16445" class="comments-container"></div><div id="comment-tools-16445" class="comment-tools"></div><div class="clear"></div><div id="comment-16445-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16453"></span>

<div id="answer-container-16453" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16453-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16453-score" class="post-score" title="current number of votes">0</div><span id="post-16453-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>By adding code to the wbmxl dissector I suspect.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Nov '12, 23:05</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-16453" class="comments-container"></div><div id="comment-tools-16453" class="comment-tools"></div><div class="clear"></div><div id="comment-16453-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

