+++
type = "question"
title = "So how do you work out the switch port your on?"
description = '''Hi So i&#x27;m a beginner. In a home network, how do you find out what switch port your PC is on? The wiring was done awhile ago and goes under floor boards etc so cant physically trace the cables. Thanks.'''
date = "2013-07-04T05:33:00Z"
lastmod = "2013-07-04T05:55:00Z"
weight = 22644
keywords = [ "home", "switch", "port", "network" ]
aliases = [ "/questions/22644" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [So how do you work out the switch port your on?](/questions/22644/so-how-do-you-work-out-the-switch-port-your-on)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22644-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22644-score" class="post-score" title="current number of votes">0</div><span id="post-22644-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>So i'm a beginner. In a home network, how do you find out what switch port your PC is on?</p><p>The wiring was done awhile ago and goes under floor boards etc so cant physically trace the cables.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-home" rel="tag" title="see questions tagged &#39;home&#39;">home</span> <span class="post-tag tag-link-switch" rel="tag" title="see questions tagged &#39;switch&#39;">switch</span> <span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jul '13, 05:33</strong></p><img src="https://secure.gravatar.com/avatar/fee9209421058d6b99de7bc42f012526?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sean87&#39;s gravatar image" /><p><span>Sean87</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sean87 has no accepted answers">0%</span></p></div></div><div id="comments-container-22644" class="comments-container"></div><div id="comment-tools-22644" class="comment-tools"></div><div class="clear"></div><div id="comment-22644-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22645"></span>

<div id="answer-container-22645" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22645-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22645-score" class="post-score" title="current number of votes">2</div><span id="post-22645-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Here are your options:</p><ul><li>Unmanaged switch: Disconnect the cables on the switch (one by one) until the link status LED on your PCs NIC if switched off. That cable is the one connected to your PC.</li><li>Managed switch: Connect to the switch (Web GUI or telnet/ssh) and search the MAC address of your PC (ipconfig /all will show you the MAC address). It depends on the switch vendor and model how and where to find the MAC address.</li></ul><p>As you are in a home network, you (most certainly) have an unmanaged switch, which means, you cannot lookup the MAC address of you PC on the switch and thus I recommend the "cable unplug method"</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jul '13, 05:55</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jul '13, 06:32</strong> </span></p></div></div><div id="comments-container-22645" class="comments-container"></div><div id="comment-tools-22645" class="comment-tools"></div><div class="clear"></div><div id="comment-22645-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

