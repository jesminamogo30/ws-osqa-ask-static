+++
type = "question"
title = "make visual studio project from wireshark sources"
description = '''Is it possible to create a visual studio project (or may be solution) from wireshark sources and use all advantages of VS to debug and edit sources.'''
date = "2012-10-18T09:53:00Z"
lastmod = "2012-10-18T12:34:00Z"
weight = 15087
keywords = [ "visual-studio" ]
aliases = [ "/questions/15087" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [make visual studio project from wireshark sources](/questions/15087/make-visual-studio-project-from-wireshark-sources)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15087-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15087-score" class="post-score" title="current number of votes">0</div><span id="post-15087-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to create a visual studio project (or may be solution) from wireshark sources and use all advantages of VS to debug and edit sources.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-visual-studio" rel="tag" title="see questions tagged &#39;visual-studio&#39;">visual-studio</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Oct '12, 09:53</strong></p><img src="https://secure.gravatar.com/avatar/c227eddf476777e0417b105709456368?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vensan%20vega&#39;s gravatar image" /><p><span>vensan vega</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vensan vega has no accepted answers">0%</span></p></div></div><div id="comments-container-15087" class="comments-container"></div><div id="comment-tools-15087" class="comment-tools"></div><div class="clear"></div><div id="comment-15087-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15089"></span>

<div id="answer-container-15089" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15089-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15089-score" class="post-score" title="current number of votes">0</div><span id="post-15089-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can use VS to debug and edit the sources as it is. What we don't have is a Visual Studio project that ties it all together so that you can just click on the solution.</p><p>For more on using VS to debug Wireshark, see Bill's answer to a very similar question <a href="http://ask.wireshark.org/questions/8660/wireshark-building-and-debugging-on-visual-c-or-visual-studio">here</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Oct '12, 12:34</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-15089" class="comments-container"></div><div id="comment-tools-15089" class="comment-tools"></div><div class="clear"></div><div id="comment-15089-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

