+++
type = "question"
title = "decode UDP mpeg2 TS"
description = '''Hi all, is there any plugin which can decode a UDP MPEG2 TS so you can see the payload? I have wireshark 1.6.1 and it&#x27;s decoding MPEG-1 Audio. Video and the PSI information would be nice too. Any chance to get this?'''
date = "2011-05-23T07:52:00Z"
lastmod = "2011-05-23T07:52:00Z"
weight = 4187
keywords = [ "mpeg2", "ts" ]
aliases = [ "/questions/4187" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [decode UDP mpeg2 TS](/questions/4187/decode-udp-mpeg2-ts)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4187-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4187-score" class="post-score" title="current number of votes">0</div><span id="post-4187-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>is there any plugin which can decode a UDP MPEG2 TS so you can see the payload? I have wireshark 1.6.1 and it's decoding MPEG-1 Audio. Video and the PSI information would be nice too. Any chance to get this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mpeg2" rel="tag" title="see questions tagged &#39;mpeg2&#39;">mpeg2</span> <span class="post-tag tag-link-ts" rel="tag" title="see questions tagged &#39;ts&#39;">ts</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '11, 07:52</strong></p><img src="https://secure.gravatar.com/avatar/3b38afeb8636ceb7855acc5d0f906fd0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sebastian%20S&#39;s gravatar image" /><p><span>Sebastian S</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sebastian S has no accepted answers">0%</span></p></div></div><div id="comments-container-4187" class="comments-container"></div><div id="comment-tools-4187" class="comment-tools"></div><div class="clear"></div><div id="comment-4187-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

