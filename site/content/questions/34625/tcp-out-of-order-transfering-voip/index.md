+++
type = "question"
title = "tcp out -of-order transfering voip"
description = '''trying transfer data fom 17.80 to 16.85 '''
date = "2014-07-13T12:11:00Z"
lastmod = "2014-07-13T21:47:00Z"
weight = 34625
keywords = [ "daap" ]
aliases = [ "/questions/34625" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [tcp out -of-order transfering voip](/questions/34625/tcp-out-of-order-transfering-voip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34625-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34625-score" class="post-score" title="current number of votes">0</div><span id="post-34625-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>trying transfer data fom 17.80 to 16.85 <img src="https://osqa-ask.wireshark.org/upfiles/tcpout.gif" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-daap" rel="tag" title="see questions tagged &#39;daap&#39;">daap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jul '14, 12:11</strong></p><img src="https://secure.gravatar.com/avatar/1c9414c24d1f140d3ba4e08711441bcd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Maor%20Avrahami&#39;s gravatar image" /><p><span>Maor Avrahami</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Maor Avrahami has no accepted answers">0%</span></p></img></div></div><div id="comments-container-34625" class="comments-container"><span id="34626"></span><div id="comment-34626" class="comment"><div id="post-34626-score" class="comment-score"></div><div class="comment-text"><p>"trying transfer data fom 17.80 to 16.85 "</p><p>What is your question? It is close to impossible to answer a question like "why are there so many out of order packets? " with a screenshot alone.</p><p>Please upload it to <a href="https://appliance.cloudshark.org/upload/">https://appliance.cloudshark.org/upload/</a></p></div><div id="comment-34626-info" class="comment-info"><span class="comment-age">(13 Jul '14, 21:47)</span> <span class="comment-user userinfo">mrEEde</span></div></div></div><div id="comment-tools-34625" class="comment-tools"></div><div class="clear"></div><div id="comment-34625-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

