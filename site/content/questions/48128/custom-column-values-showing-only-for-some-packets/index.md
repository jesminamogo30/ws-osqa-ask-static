+++
type = "question"
title = "Custom column values showing only for some packets"
description = '''Newbie with a postdissector LUA script, to extract and consolidate field values from a family (or suite) of related custom protocols. The compiled decoder for the said protocol suite was developed by an unknown developer for WS 1.6.6. Using the postdissector to consolidate fields from the protocols,...'''
date = "2015-12-01T05:02:00Z"
lastmod = "2015-12-01T05:02:00Z"
weight = 48128
keywords = [ "lua", "problem", "postdissector", "custom_column" ]
aliases = [ "/questions/48128" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Custom column values showing only for some packets](/questions/48128/custom-column-values-showing-only-for-some-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48128-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48128-score" class="post-score" title="current number of votes">0</div><span id="post-48128-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Newbie with a postdissector LUA script, to extract and consolidate field values from a family (or suite) of related custom protocols. The compiled decoder for the said protocol suite was developed by an unknown developer for WS 1.6.6.</p><p>Using the postdissector to consolidate fields from the protocols, to minimize the number of columns required for the Packet List and CSV export file. Also extracted additional (pseudo) fields from the undecoded packet data.</p><p>The extracted fields and their values are showing normally in the Packet Details window.</p><p>My problem is that only a few of the packets in the Packet List window show the values for the extracted fields which I added as custom columns.</p><p>Usually, only one row will show extracted values for every page of the Packet List.</p><p>If I add a column from the suite protocol decoder, the corresponding LUA extracted column will then have all the values shown.</p><p>Please kindly advice possible causes of and ways to resolve this problem.</p><p>TIA.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-problem" rel="tag" title="see questions tagged &#39;problem&#39;">problem</span> <span class="post-tag tag-link-postdissector" rel="tag" title="see questions tagged &#39;postdissector&#39;">postdissector</span> <span class="post-tag tag-link-custom_column" rel="tag" title="see questions tagged &#39;custom_column&#39;">custom_column</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Dec '15, 05:02</strong></p><img src="https://secure.gravatar.com/avatar/7e50c86ac4ee2038257acc83ccb1ce21?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="juandering&#39;s gravatar image" /><p><span>juandering</span><br />
<span class="score" title="11 reputation points">11</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="juandering has one accepted answer">100%</span></p></div></div><div id="comments-container-48128" class="comments-container"></div><div id="comment-tools-48128" class="comment-tools"></div><div class="clear"></div><div id="comment-48128-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

