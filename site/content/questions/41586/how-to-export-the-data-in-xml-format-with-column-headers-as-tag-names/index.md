+++
type = "question"
title = "How to export the data in xml format with column headers as tag names"
description = '''When I export the data into xml format, I get something like this  Since all the tags are named as &quot;section&quot;, it is not easy to parse this data. WHen I checked the box &quot;include column headings&quot;, it includes the first record that describes the structure of the document. This is not what I need. Is th...'''
date = "2015-04-20T00:15:00Z"
lastmod = "2015-04-20T03:22:00Z"
weight = 41586
keywords = [ "xml", "export" ]
aliases = [ "/questions/41586" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to export the data in xml format with column headers as tag names](/questions/41586/how-to-export-the-data-in-xml-format-with-column-headers-as-tag-names)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41586-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41586-score" class="post-score" title="current number of votes">0</div><span id="post-41586-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I export the data into xml format, I get something like this</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Capture_oF9tGYT.PNG" alt="alt text" /></p><p>Since all the tags are named as "section", it is not easy to parse this data.</p><p>WHen I checked the box "include column headings", it includes the first record that describes the structure of the document. This is not what I need. Is there a way to label the tags as actual column headers?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xml" rel="tag" title="see questions tagged &#39;xml&#39;">xml</span> <span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '15, 00:15</strong></p><img src="https://secure.gravatar.com/avatar/2d3358e5b4a4507b418f0f015c5377bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hvs&#39;s gravatar image" /><p><span>hvs</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hvs has no accepted answers">0%</span></p></img></div></div><div id="comments-container-41586" class="comments-container"></div><div id="comment-tools-41586" class="comment-tools"></div><div class="clear"></div><div id="comment-41586-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41596"></span>

<div id="answer-container-41596" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41596-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41596-score" class="post-score" title="current number of votes">0</div><span id="post-41596-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Then use a different format. You've selected PSML, while PDML provides all you need, and more.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Apr '15, 03:22</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-41596" class="comments-container"></div><div id="comment-tools-41596" class="comment-tools"></div><div class="clear"></div><div id="comment-41596-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

