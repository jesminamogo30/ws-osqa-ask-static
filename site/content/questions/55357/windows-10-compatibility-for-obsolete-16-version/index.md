+++
type = "question"
title = "windows 10 compatibility for obsolete 1.6 version"
description = ''' from which version wireshark supports windows 10 ? Is the wireshark (1.6.5) currently or at a future date, vendor supported running in a locally hosted VM? Is the wireshark (1.6.5) currently or at a future date, vendor supported running as a Virtualized app (Citrix Xenapp, VMWare ThinApp, Microsoft...'''
date = "2016-09-06T13:47:00Z"
lastmod = "2016-09-06T14:11:00Z"
weight = 55357
keywords = [ "win", "1.6", "os", "10" ]
aliases = [ "/questions/55357" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [windows 10 compatibility for obsolete 1.6 version](/questions/55357/windows-10-compatibility-for-obsolete-16-version)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55357-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55357-score" class="post-score" title="current number of votes">0</div><span id="post-55357-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><ol><li>from which version wireshark supports windows 10 ?</li><li>Is the wireshark (1.6.5) currently or at a future date, vendor supported running in a locally hosted VM?</li><li>Is the wireshark (1.6.5) currently or at a future date, vendor supported running as a Virtualized app (Citrix Xenapp, VMWare ThinApp, Microsoft AppV)</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-win" rel="tag" title="see questions tagged &#39;win&#39;">win</span> <span class="post-tag tag-link-1.6" rel="tag" title="see questions tagged &#39;1.6&#39;">1.6</span> <span class="post-tag tag-link-os" rel="tag" title="see questions tagged &#39;os&#39;">os</span> <span class="post-tag tag-link-10" rel="tag" title="see questions tagged &#39;10&#39;">10</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Sep '16, 13:47</strong></p><img src="https://secure.gravatar.com/avatar/277084326b74dd64bd543405f9838f07?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="inergi&#39;s gravatar image" /><p><span>inergi</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="inergi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Sep '16, 14:13</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-55357" class="comments-container"></div><div id="comment-tools-55357" class="comment-tools"></div><div class="clear"></div><div id="comment-55357-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55359"></span>

<div id="answer-container-55359" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55359-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55359-score" class="post-score" title="current number of votes">0</div><span id="post-55359-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The first release to support Windows 10 was I think 1.12.</p><p>Wireshark 1.6 is obsolete and support ended June 2013. As this was 2 years before the release of Windows 10, it may work or it may not, you'll have to test yourself.</p><p>The Wireshark project does no testing for use as a virtualized app, you'll have to test yourself.</p><p>There's more information on release and end of life dates on the Wiki <a href="https://wiki.wireshark.org/Development/LifeCycle">Lifecycle page</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Sep '16, 14:11</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Sep '16, 14:12</strong> </span></p></div></div><div id="comments-container-55359" class="comments-container"></div><div id="comment-tools-55359" class="comment-tools"></div><div class="clear"></div><div id="comment-55359-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

