+++
type = "question"
title = "Protocol Hierarchy"
description = '''I loaded the development Version 1.99.1 (v1.99.1-0-g4c229ca from master) for macbook. The application seems to work fine and I can capture packets. My problem is that &quot;protocol hierarchy&quot; is greyed out and I would like to view these statistics.'''
date = "2015-02-02T13:08:00Z"
lastmod = "2015-02-02T15:01:00Z"
weight = 39579
keywords = [ "hierarchy", "protocol" ]
aliases = [ "/questions/39579" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Protocol Hierarchy](/questions/39579/protocol-hierarchy)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39579-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39579-score" class="post-score" title="current number of votes">0</div><span id="post-39579-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I loaded the development Version 1.99.1 (v1.99.1-0-g4c229ca from master) for macbook. The application seems to work fine and I can capture packets. My problem is that "protocol hierarchy" is greyed out and I would like to view these statistics.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hierarchy" rel="tag" title="see questions tagged &#39;hierarchy&#39;">hierarchy</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Feb '15, 13:08</strong></p><img src="https://secure.gravatar.com/avatar/c1a9e5b393e0c7787b640b71034d7843?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gem_ctio&#39;s gravatar image" /><p><span>gem_ctio</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gem_ctio has no accepted answers">0%</span></p></div></div><div id="comments-container-39579" class="comments-container"></div><div id="comment-tools-39579" class="comment-tools"></div><div class="clear"></div><div id="comment-39579-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39585"></span>

<div id="answer-container-39585" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39585-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39585-score" class="post-score" title="current number of votes">0</div><span id="post-39585-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The new QT version isn't yet feature complete. There are more recent versions available from the <a href="https://www.wireshark.org/download/automated/osx/">automated downloads section</a> that might have that feature enabled, but if that doesn't fix it then you'll need to revert to the older GTK based 1.12.x versions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Feb '15, 14:46</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-39585" class="comments-container"><span id="39586"></span><div id="comment-39586" class="comment"><div id="post-39586-score" class="comment-score"></div><div class="comment-text"><p>Ok I uninstalled the 1.99.1 and put this one on below and it still has the feature greyed out. I did try the stable 1.12 version but I also needed to download and configure XQuartz and I'm really rusty on my unix command line...so when I did this I had no idea what I was doing. Thank you for the help I really appreciate it.</p><p>Version 1.99.2-959-g37a2cd0 (v1.99.2rc0-959-g37a2cd0 from unknown)</p></div><div id="comment-39586-info" class="comment-info"><span class="comment-age">(02 Feb '15, 15:01)</span> <span class="comment-user userinfo">gem_ctio</span></div></div></div><div id="comment-tools-39585" class="comment-tools"></div><div class="clear"></div><div id="comment-39585-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

