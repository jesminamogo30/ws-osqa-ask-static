+++
type = "question"
title = "packet transmitted with heartbeat failure"
description = '''In the wireshark interface details pan in the 802.3 (Ethernet) tab what is the &quot; packet transmitted with heartbeat failure&quot;? And with I am findings some unusual traffic let me know what is that one. '''
date = "2014-07-28T00:53:00Z"
lastmod = "2014-07-28T00:53:00Z"
weight = 34938
keywords = [ "failure", "with", "transmitted", "packet", "heartbeat" ]
aliases = [ "/questions/34938" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [packet transmitted with heartbeat failure](/questions/34938/packet-transmitted-with-heartbeat-failure)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34938-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34938-score" class="post-score" title="current number of votes">0</div><span id="post-34938-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In the wireshark interface details pan in the 802.3 (Ethernet) tab what is the " packet transmitted with heartbeat failure"? And with I am findings some unusual traffic let me know what is that one.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-failure" rel="tag" title="see questions tagged &#39;failure&#39;">failure</span> <span class="post-tag tag-link-with" rel="tag" title="see questions tagged &#39;with&#39;">with</span> <span class="post-tag tag-link-transmitted" rel="tag" title="see questions tagged &#39;transmitted&#39;">transmitted</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-heartbeat" rel="tag" title="see questions tagged &#39;heartbeat&#39;">heartbeat</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jul '14, 00:53</strong></p><img src="https://secure.gravatar.com/avatar/be97c677a49b8e1534d3556295d08376?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AdiTech&#39;s gravatar image" /><p><span>AdiTech</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AdiTech has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-34938" class="comments-container"></div><div id="comment-tools-34938" class="comment-tools"></div><div class="clear"></div><div id="comment-34938-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

