+++
type = "question"
title = "Need identify top/most network users or traffic source or IP-How to configure"
description = '''I need to know how to capture highest network traffic originator or source on my network i have configured wire shark to monitor all my Internet traffic from that need to know who’s the Top internet users (IP) or highest internet access most visited sites can it possible from wire shark how can I co...'''
date = "2011-07-06T02:44:00Z"
lastmod = "2011-07-08T08:05:00Z"
weight = 4923
keywords = [ "traffic" ]
aliases = [ "/questions/4923" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Need identify top/most network users or traffic source or IP-How to configure](/questions/4923/need-identify-topmost-network-users-or-traffic-source-or-ip-how-to-configure)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4923-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4923-score" class="post-score" title="current number of votes">0</div><span id="post-4923-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to know how to capture highest network traffic originator or source on my network i have configured wire shark to monitor all my Internet traffic from that need to know who’s the Top internet users (IP) or highest internet access most visited sites can it possible from wire shark how can I configure that in this and i need this fro ongoing traffic or like filter</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jul '11, 02:44</strong></p><img src="https://secure.gravatar.com/avatar/badce01421139befb4a3a2dbc1252962?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thusitha&#39;s gravatar image" /><p><span>thusitha</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thusitha has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Jul '11, 20:12</strong> </span></p></div></div><div id="comments-container-4923" class="comments-container"></div><div id="comment-tools-4923" class="comment-tools"></div><div class="clear"></div><div id="comment-4923-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4939"></span>

<div id="answer-container-4939" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4939-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4939-score" class="post-score" title="current number of votes">1</div><span id="post-4939-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="thusitha has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you tried the Statistics/Endpoint Option? If you select the IP tab you can sort by packets or bytes and see which nodes have the most RX/TX values. Or are you looking for something else?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Jul '11, 05:21</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-4939" class="comments-container"><span id="4949"></span><div id="comment-4949" class="comment"><div id="post-4949-score" class="comment-score"></div><div class="comment-text"><p>Great that works thanks for that I got some part of result</p><p>But</p><p>What i was looking at is Presently My company we have 3M internet lease line link everyday i can see link is full of traffic and cannot identify who utilize this link i mean TOP internet users to witch site. So what I’m looking is Most internet user(IP) or everyday access site for that i have enabled monitoring only for this Internet link connected Interface from Firewall.</p></div><div id="comment-4949-info" class="comment-info"><span class="comment-age">(08 Jul '11, 05:34)</span> <span class="comment-user userinfo">thusitha</span></div></div><span id="4950"></span><div id="comment-4950" class="comment"><div id="post-4950-score" class="comment-score"></div><div class="comment-text"><p>For that you could use Statistics/Conversations, and sort by the IP or TCP tab to see which IPs have the most packets/bytes going in and out, and by looking at the bps column you'll see the average throughput as well. If you found something interesting, use the context sensitive popup menu to filter on any selected communication to see the packets it is made of.</p></div><div id="comment-4950-info" class="comment-info"><span class="comment-age">(08 Jul '11, 06:07)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="4951"></span><div id="comment-4951" class="comment"><div id="post-4951-score" class="comment-score"></div><div class="comment-text"><p>Thanks That works this is what neaded Thank you very much</p></div><div id="comment-4951-info" class="comment-info"><span class="comment-age">(08 Jul '11, 06:49)</span> <span class="comment-user userinfo">thusitha</span></div></div><span id="4954"></span><div id="comment-4954" class="comment"><div id="post-4954-score" class="comment-score"></div><div class="comment-text"><p>You're welcome. You might want to accept the answer then, which will remove it from the list of open questions ;-)</p></div><div id="comment-4954-info" class="comment-info"><span class="comment-age">(08 Jul '11, 08:05)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-4939" class="comment-tools"></div><div class="clear"></div><div id="comment-4939-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

