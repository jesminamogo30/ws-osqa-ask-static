+++
type = "question"
title = "wireshark with Wifi Device"
description = '''Hello everyone , I want to use wireshark to capture packets from a mobile device (Tablet or Telephone) in a WIFI network. My goal is to successfully capture packets and data via a mobile device.  For example: access to a live streaming site with a phone and successfully capture packets and data with...'''
date = "2017-06-16T13:58:00Z"
lastmod = "2017-06-19T05:53:00Z"
weight = 62063
keywords = [ "wifi", "wifidevices", "wireshark" ]
aliases = [ "/questions/62063" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark with Wifi Device](/questions/62063/wireshark-with-wifi-device)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62063-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62063-score" class="post-score" title="current number of votes">0</div><span id="post-62063-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone , I want to use wireshark to capture packets from a mobile device (Tablet or Telephone) in a WIFI network. My goal is to successfully capture packets and data via a mobile device. For example: access to a live streaming site with a phone and successfully capture packets and data with wireshark. Please can Someone help me and advise me .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-wifidevices" rel="tag" title="see questions tagged &#39;wifidevices&#39;">wifidevices</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jun '17, 13:58</strong></p><img src="https://secure.gravatar.com/avatar/78a54b63becb43c965642d5d7cc59cba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alielkamel&#39;s gravatar image" /><p><span>alielkamel</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alielkamel has no accepted answers">0%</span></p></div></div><div id="comments-container-62063" class="comments-container"></div><div id="comment-tools-62063" class="comment-tools"></div><div class="clear"></div><div id="comment-62063-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62102"></span>

<div id="answer-container-62102" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62102-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62102-score" class="post-score" title="current number of votes">0</div><span id="post-62102-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Reference: <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">https://wiki.wireshark.org/CaptureSetup/WLAN</a></p><p>This link will get you going in the right direction. You may need to buy hardware depending on what you need to do. Also search here - there are plenty of questions relating to the details of what you are trying to do.</p><p>Good luck.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jun '17, 09:31</strong></p><img src="https://secure.gravatar.com/avatar/0a47ef51dd9c9996d194a4983295f5a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob%20Jones&#39;s gravatar image" /><p><span>Bob Jones</span><br />
<span class="score" title="1014 reputation points"><span>1.0k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob Jones has 19 accepted answers">21%</span></p></div></div><div id="comments-container-62102" class="comments-container"><span id="62120"></span><div id="comment-62120" class="comment"><div id="post-62120-score" class="comment-score"></div><div class="comment-text"><p>thank you for your answer :)</p></div><div id="comment-62120-info" class="comment-info"><span class="comment-age">(19 Jun '17, 05:53)</span> <span class="comment-user userinfo">alielkamel</span></div></div></div><div id="comment-tools-62102" class="comment-tools"></div><div class="clear"></div><div id="comment-62102-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

