+++
type = "question"
title = "Windows XP C++ runtime error"
description = '''hi, when I try to run wireshark in windows XP, it gives me the following error: Microsoft Visual C++ Runtime Library Program: /path-to-wireshark/wireshark.exe This Apllication has requested the runtime to terminate it in an unusual way. Please contact the support team. What should I do?'''
date = "2011-07-25T03:48:00Z"
lastmod = "2011-07-25T06:47:00Z"
weight = 5207
keywords = [ "windows", "startup", "crash" ]
aliases = [ "/questions/5207" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Windows XP C++ runtime error](/questions/5207/windows-xp-c-runtime-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5207-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5207-score" class="post-score" title="current number of votes">0</div><span id="post-5207-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi,</p><p>when I try to run wireshark in windows XP, it gives me the following error:</p><p>Microsoft Visual C++ Runtime Library</p><p>Program: /path-to-wireshark/wireshark.exe</p><p>This Apllication has requested the runtime to terminate it in an unusual way. Please contact the support team.</p><p>What should I do?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-startup" rel="tag" title="see questions tagged &#39;startup&#39;">startup</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jul '11, 03:48</strong></p><img src="https://secure.gravatar.com/avatar/022ac760748b4bf8ad137149041fdf25?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pangd&#39;s gravatar image" /><p><span>pangd</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pangd has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Jul '11, 12:05</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-5207" class="comments-container"><span id="5208"></span><div id="comment-5208" class="comment"><div id="post-5208-score" class="comment-score"></div><div class="comment-text"><p>Wireshark version? XP Service Pack?</p><p>Is this your first install of Wireshark or are you installing over a previous version?</p></div><div id="comment-5208-info" class="comment-info"><span class="comment-age">(25 Jul '11, 04:19)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="5209"></span><div id="comment-5209" class="comment"><div id="post-5209-score" class="comment-score"></div><div class="comment-text"><p>Th latest wireshark version with Microsoft Winsows XP Home Edition.</p><p>It's the first time.</p></div><div id="comment-5209-info" class="comment-info"><span class="comment-age">(25 Jul '11, 04:50)</span> <span class="comment-user userinfo">pangd</span></div></div><span id="5211"></span><div id="comment-5211" class="comment"><div id="post-5211-score" class="comment-score"></div><div class="comment-text"><p>Try a re-install of wireshark?</p></div><div id="comment-5211-info" class="comment-info"><span class="comment-age">(25 Jul '11, 05:30)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="5213"></span><div id="comment-5213" class="comment"><div id="post-5213-score" class="comment-score"></div><div class="comment-text"><p>Already tried.</p></div><div id="comment-5213-info" class="comment-info"><span class="comment-age">(25 Jul '11, 05:54)</span> <span class="comment-user userinfo">pangd</span></div></div><span id="5214"></span><div id="comment-5214" class="comment"><div id="post-5214-score" class="comment-score"></div><div class="comment-text"><p>This isn't an answer, but a further comment to my previous comment. It helps others if you keep "answers" for actual answers.</p></div><div id="comment-5214-info" class="comment-info"><span class="comment-age">(25 Jul '11, 06:17)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="5215"></span><div id="comment-5215" class="comment not_top_scorer"><div id="post-5215-score" class="comment-score"></div><div class="comment-text"><p>Yes I know, my mistake.</p></div><div id="comment-5215-info" class="comment-info"><span class="comment-age">(25 Jul '11, 06:18)</span> <span class="comment-user userinfo">pangd</span></div></div><span id="5216"></span><div id="comment-5216" class="comment not_top_scorer"><div id="post-5216-score" class="comment-score"></div><div class="comment-text"><p>You haven't yet stated the XP Service Pack, and I'm guessing by "Th latest wireshark version" you mean 1.6.1?</p><p>To see the XP Service Pack, right click "My Computer" and select "Properties". The Service Pack is listed on "the General" tab.</p></div><div id="comment-5216-info" class="comment-info"><span class="comment-age">(25 Jul '11, 06:20)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-5207" class="comment-tools"><span class="comments-showing"> showing 5 of 7 </span> <a href="#" class="show-all-comments-link">show 2 more comments</a></div><div class="clear"></div><div id="comment-5207-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5219"></span>

<div id="answer-container-5219" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5219-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5219-score" class="post-score" title="current number of votes">0</div><span id="post-5219-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I managed to do it.</p><p>Just reinstall the whole package.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jul '11, 06:47</strong></p><img src="https://secure.gravatar.com/avatar/022ac760748b4bf8ad137149041fdf25?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pangd&#39;s gravatar image" /><p><span>pangd</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pangd has no accepted answers">0%</span></p></div></div><div id="comments-container-5219" class="comments-container"></div><div id="comment-tools-5219" class="comment-tools"></div><div class="clear"></div><div id="comment-5219-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

