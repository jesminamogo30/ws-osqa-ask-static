+++
type = "question"
title = "wireshark on suse 10"
description = '''Hello, Before reading, please attention, my processor is 64-bit and so my oS is Suse_10 for 64 bit. I am trying to install wireshark on suse_10(64bit). The host does not have internet access. Therefore, I need to have all the files needed for installation; that is even the dependencies. This is to b...'''
date = "2011-02-19T12:33:00Z"
lastmod = "2011-02-20T21:24:00Z"
weight = 2431
keywords = [ "local", "suse", "64-bit", "install", "wireshark" ]
aliases = [ "/questions/2431" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark on suse 10](/questions/2431/wireshark-on-suse-10)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2431-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2431-score" class="post-score" title="current number of votes">0</div><span id="post-2431-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Before reading, please attention, my processor is 64-bit and so my oS is Suse_10 for 64 bit.</p><p>I am trying to install wireshark on suse_10(64bit). The host does not have internet access. Therefore, I need to have all the files needed for installation; that is even the dependencies. This is to be able to install wireshark on a host with no internet connection. (Zypper, Yast would complain when no connection. It would try t go to net and find the dependencies.)</p><p>Currently, when I execute the rpm command like 'rpm -i wireshark-devel-1.2.8-2.8.x86_64.rpm', of course I get the errors that say, briefly, dependencies needed.</p><p>So, what I am looking for is the answer if I can make a "package" which has everything, and everything needed to be able to perform an flawless installation.</p><p>And of yes, could you please tell me how?</p><p>I would really appreciate your help.</p><p>Or, a friend talked about "aptoncd". This works for ubuntu. This software can make the package I want, if I understood my friend correctly. But, what software can I use for "suse_10".</p><p>If I can find the software, I am thinking about to install a Suse_10 to my computer, and then using this program I would be able to make a package (that has everything needed in it).</p><p>Then, I would take this package to the host that I am trying to install wireshark on.</p><p>Can you please help?</p><p>thank you in advance..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-local" rel="tag" title="see questions tagged &#39;local&#39;">local</span> <span class="post-tag tag-link-suse" rel="tag" title="see questions tagged &#39;suse&#39;">suse</span> <span class="post-tag tag-link-64-bit" rel="tag" title="see questions tagged &#39;64-bit&#39;">64-bit</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '11, 12:33</strong></p><img src="https://secure.gravatar.com/avatar/f8deb079242bcf9cb848c2de41d36b6d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sudo1234&#39;s gravatar image" /><p><span>sudo1234</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sudo1234 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Jun '12, 20:09</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-2431" class="comments-container"><span id="2447"></span><div id="comment-2447" class="comment"><div id="post-2447-score" class="comment-score"></div><div class="comment-text"><p>If you have an installation disc with all the packages on you could use that otherwise get the individual packages listed in the developers manual and install them.</p></div><div id="comment-2447-info" class="comment-info"><span class="comment-age">(20 Feb '11, 21:24)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-2431" class="comment-tools"></div><div class="clear"></div><div id="comment-2431-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

