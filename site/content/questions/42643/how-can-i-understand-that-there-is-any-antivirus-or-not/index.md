+++
type = "question"
title = "how can i understand that there is any antivirus or not?"
description = '''I have the following pic. this is a result of wireshark. 192.168.115.238 is attacker. i want to know that is there antvirus that finished the conversation? in a better question by analyzing wireshark results how can i understand that there is any antivirus or not?  thanks for consideration'''
date = "2015-05-24T13:45:00Z"
lastmod = "2015-05-24T13:45:00Z"
weight = 42643
keywords = [ "malware", "wireshark" ]
aliases = [ "/questions/42643" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how can i understand that there is any antivirus or not?](/questions/42643/how-can-i-understand-that-there-is-any-antivirus-or-not)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42643-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42643-score" class="post-score" title="current number of votes">0</div><span id="post-42643-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have the following pic. this is a result of wireshark. 192.168.115.238 is attacker. i want to know that is there antvirus that finished the conversation? in a better question by analyzing wireshark results how can i understand that there is any antivirus or not?</p><p><img src="https://osqa-ask.wireshark.org/upfiles/anti.jpg" alt="alt text" /></p><p>thanks for consideration</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-malware" rel="tag" title="see questions tagged &#39;malware&#39;">malware</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 May '15, 13:45</strong></p><img src="https://secure.gravatar.com/avatar/280c3f22ec8de7b919785632284f0935?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="samira&#39;s gravatar image" /><p><span>samira</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="samira has no accepted answers">0%</span></p></img></div></div><div id="comments-container-42643" class="comments-container"></div><div id="comment-tools-42643" class="comment-tools"></div><div class="clear"></div><div id="comment-42643-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

