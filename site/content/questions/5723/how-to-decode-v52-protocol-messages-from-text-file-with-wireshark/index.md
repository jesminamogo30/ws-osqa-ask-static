+++
type = "question"
title = "How to decode V5.2 protocol messages from text file with Wireshark?"
description = '''I have a text file with captured data from V5.2 link. I want use Wireshark to analyse this files. How I can do this task? Here is a part of my text file: 12:46:23 15.12.05 SYS 0 FC E1 FC E1 01 01 77 13 00 63 06 12:46:23 15.12.05 LE 0 FC E3 FE E3 7F 08 62 C2 06 12:46:24 15.12.05 LE 0 FC E9 FE E9 7F D...'''
date = "2011-08-17T02:39:00Z"
lastmod = "2012-10-11T05:50:00Z"
weight = 5723
keywords = [ "v5.2" ]
aliases = [ "/questions/5723" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to decode V5.2 protocol messages from text file with Wireshark?](/questions/5723/how-to-decode-v52-protocol-messages-from-text-file-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5723-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5723-score" class="post-score" title="current number of votes">0</div><span id="post-5723-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a text file with captured data from V5.2 link. I want use Wireshark to analyse this files. How I can do this task? Here is a part of my text file:</p><pre><code>12:46:23 15.12.05 SYS 0 FC E1 FC E1 01 01 77 13 00 63 06
12:46:23 15.12.05 LE 0 FC E3 FE E3 7F 08 62 C2 06
12:46:24 15.12.05 LE 0 FC E9 FE E9 7F D6 43 7F 07
12:46:24 15.12.05 LE 0 FC E5 FE E5 7F 42 7D 1F 07
12:46:24 15.12.05 LE 0 FC E1 FE E1 7F CE 68 90 07
12:46:24 15.12.05 AN 0 FC E3 FE E3 73 64 A8 61 07
12:46:24 15.12.05 AN 0 FC E9 FE E9 73 BA 89 A6 07
12:46:24 15.12.05 AN 0 FC E5 FE E5 73 2E B7 42 07
12:46:24 15.12.05 AN 0 FC E1 FE E1 73 A2 A2 9B 07
12:46:24 15.12.05 LE 0 FC E3 FE E3 00 00 48 FC E3 12 21 01 86 56 C0 F0 09
12:46:24 15.12.05 AN 0 FC E3 FE E3 01 02 94 35 BA 06
12:46:24 15.12.05 AN 0 FC E3 FC E3 00 03 48 FC E3 13 21 01 86 1F FD FD 09
12:46:24 15.12.05 AN 0 FC E3 FC E3 02 03 48 FC E3 12 21 01 87 22 01 80 23 03 00 00 05 C4 10 97 0A
12:46:24 15.12.05 AN 0 FC E3 FC E3 04 03 48 FC E3 12 21 01 86 41 DE 07 0A
12:46:24 15.12.05 LE 0 FC E3 FC E3 01 03 6B 1D 7F 06
12:46:24 15.12.05 LE 0 FC E3 FC E3 01 05 5D 78 D0 06
12:46:24 15.12.05 LE 0 FC E3 FC E3 01 07 4F 5B A9 06</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-v5.2" rel="tag" title="see questions tagged &#39;v5.2&#39;">v5.2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Aug '11, 02:39</strong></p><img src="https://secure.gravatar.com/avatar/941120f82b82a809f440e65b36a352b8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AlEx&#39;s gravatar image" /><p><span>AlEx</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AlEx has no accepted answers">0%</span></p></div></div><div id="comments-container-5723" class="comments-container"><span id="14924"></span><div id="comment-14924" class="comment"><div id="post-14924-score" class="comment-score"></div><div class="comment-text"><p>Hi AlEx,<br />
</p><p>Did you get answer to your question. I am trying to find the same answer.</p></div><div id="comment-14924-info" class="comment-info"><span class="comment-age">(11 Oct '12, 05:50)</span> <span class="comment-user userinfo">rkuppili</span></div></div></div><div id="comment-tools-5723" class="comment-tools"></div><div class="clear"></div><div id="comment-5723-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

