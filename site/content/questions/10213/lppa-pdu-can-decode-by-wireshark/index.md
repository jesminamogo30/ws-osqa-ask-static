+++
type = "question"
title = "LPPa PDU can decode by wireshark ?"
description = '''Dear All:  Is the Wireshark can decode the LPPa-PDU in LPPa Transport Messages of S1AP protocol ? Anyone known it or give an example packet? '''
date = "2012-04-17T00:21:00Z"
lastmod = "2012-04-17T17:42:00Z"
weight = 10213
keywords = [ "lppa" ]
aliases = [ "/questions/10213" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [LPPa PDU can decode by wireshark ?](/questions/10213/lppa-pdu-can-decode-by-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10213-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10213-score" class="post-score" title="current number of votes">0</div><span id="post-10213-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear All: Is the Wireshark can decode the LPPa-PDU in LPPa Transport Messages of S1AP protocol ? Anyone known it or give an example packet?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lppa" rel="tag" title="see questions tagged &#39;lppa&#39;">lppa</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Apr '12, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/44af37a9a43e184510ec139669f5d6e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kwind&#39;s gravatar image" /><p><span>kwind</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kwind has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Apr '12, 00:22</strong> </span></p></div></div><div id="comments-container-10213" class="comments-container"></div><div id="comment-tools-10213" class="comment-tools"></div><div class="clear"></div><div id="comment-10213-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10217"></span>

<div id="answer-container-10217" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10217-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10217-score" class="post-score" title="current number of votes">1</div><span id="post-10217-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It should be possible with the development version 1.7.1 or a recent buildbot build.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Apr '12, 08:25</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-10217" class="comments-container"><span id="10226"></span><div id="comment-10226" class="comment"><div id="post-10226-score" class="comment-score"></div><div class="comment-text"><p>Thank you very much.</p></div><div id="comment-10226-info" class="comment-info"><span class="comment-age">(17 Apr '12, 17:42)</span> <span class="comment-user userinfo">kwind</span></div></div></div><div id="comment-tools-10217" class="comment-tools"></div><div class="clear"></div><div id="comment-10217-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

