+++
type = "question"
title = "OSX Lion release?"
description = '''Do you know if your latest Snow Leopard compatible release is also compatible with OSX Lion, or do you have a projected release date for WS for Lion?'''
date = "2011-08-11T09:36:00Z"
lastmod = "2011-11-19T11:18:00Z"
weight = 5652
keywords = [ "osx", "mac", "lion" ]
aliases = [ "/questions/5652" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [OSX Lion release?](/questions/5652/osx-lion-release)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5652-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5652-score" class="post-score" title="current number of votes">0</div><span id="post-5652-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Do you know if your latest Snow Leopard compatible release is also compatible with OSX Lion, or do you have a projected release date for WS for Lion?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-lion" rel="tag" title="see questions tagged &#39;lion&#39;">lion</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Aug '11, 09:36</strong></p><img src="https://secure.gravatar.com/avatar/1fc8e3f08e91bd12a8787c217e8ca461?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mpreissner&#39;s gravatar image" /><p><span>mpreissner</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mpreissner has no accepted answers">0%</span></p></div></div><div id="comments-container-5652" class="comments-container"></div><div id="comment-tools-5652" class="comment-tools"></div><div class="clear"></div><div id="comment-5652-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="5654"></span>

<div id="answer-container-5654" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5654-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5654-score" class="post-score" title="current number of votes">1</div><span id="post-5654-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's mostly compatible with Lion, although if you scroll the packet list with a scroll-ball (rather than by dragging the scrollbar) or possibly with a trackpad, some rows in the packet list will show up as blank; if you move the mouse over them, they draw normally. This appears to be a GTK+ bug that shows up with the presumably-newer version of X11 in Lion; upgrading the GTK+ in the OS X buildbots should make this go away.</p><p>There's no reason to have a separate build for Lion.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Aug '11, 10:39</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-5654" class="comments-container"><span id="5716"></span><div id="comment-5716" class="comment"><div id="post-5716-score" class="comment-score"></div><div class="comment-text"><p>I agree with Guy. I've played with various versions of X11 that will work with Lion, but I haven't noted the GTK issue.</p></div><div id="comment-5716-info" class="comment-info"><span class="comment-age">(16 Aug '11, 12:37)</span> <span class="comment-user userinfo">GeonJay</span></div></div><span id="5810"></span><div id="comment-5810" class="comment"><div id="post-5810-score" class="comment-score">1</div><div class="comment-text"><p>That UI refresh issue is really annoying on Lion. If it's possible to spin up a new build at some point to take care of this issue, it would be much appreciated.</p></div><div id="comment-5810-info" class="comment-info"><span class="comment-age">(22 Aug '11, 16:11)</span> <span class="comment-user userinfo">H_B</span></div></div><span id="5859"></span><div id="comment-5859" class="comment"><div id="post-5859-score" class="comment-score"></div><div class="comment-text"><p>Im a new user and trying to test it on oxs lion, The app opens give a message about x11 then closes</p></div><div id="comment-5859-info" class="comment-info"><span class="comment-age">(24 Aug '11, 17:34)</span> <span class="comment-user userinfo">bharned3</span></div></div><span id="5860"></span><div id="comment-5860" class="comment"><div id="post-5860-score" class="comment-score"></div><div class="comment-text"><p>You can ask a new question of your own by clicking <strong>ask a question</strong> above (top-right).</p><p>(In fact, you should <em>always</em> ask new questions that way. You should only post an answer if you're answering the original question, even if the answer is, say, "you can't do that with Wireshark"; everything else should be a comment. -Guy Harris)</p></div><div id="comment-5860-info" class="comment-info"><span class="comment-age">(24 Aug '11, 17:48)</span> <span class="comment-user userinfo">helloworld</span></div></div><span id="7508"></span><div id="comment-7508" class="comment"><div id="post-7508-score" class="comment-score"></div><div class="comment-text"><p>I agree with H_B, please release a version for Lion that fixes this issue. It's almost impossible to use.</p></div><div id="comment-7508-info" class="comment-info"><span class="comment-age">(18 Nov '11, 11:20)</span> <span class="comment-user userinfo">administraitor</span></div></div></div><div id="comment-tools-5654" class="comment-tools"></div><div class="clear"></div><div id="comment-5654-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="7518"></span>

<div id="answer-container-7518" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7518-score" class="post-score" title="current number of votes">0</div><span id="post-7518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The 1.6.4 build is built with GTK+ 2.24.5, so it should work. Try 1.6.4 or later builds.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Nov '11, 11:18</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-7518" class="comments-container"></div><div id="comment-tools-7518" class="comment-tools"></div><div class="clear"></div><div id="comment-7518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

