+++
type = "question"
title = "How can i setup wireshark to display name for particular field in header?"
description = '''Hi, I have a capture (TLV-[Type Length Value] capture) which shows all fields as Hex value.I want wireshark to display those fields as appropriate name instead of Hex value.Can anyone help me how to solve this one?'''
date = "2011-09-04T09:04:00Z"
lastmod = "2011-09-04T09:04:00Z"
weight = 6073
keywords = [ "tlv", "plugin" ]
aliases = [ "/questions/6073" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can i setup wireshark to display name for particular field in header?](/questions/6073/how-can-i-setup-wireshark-to-display-name-for-particular-field-in-header)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6073-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6073-score" class="post-score" title="current number of votes">0</div><span id="post-6073-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have a capture (TLV-[Type Length Value] capture) which shows all fields as Hex value.I want wireshark to display those fields as appropriate name instead of Hex value.Can anyone help me how to solve this one?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tlv" rel="tag" title="see questions tagged &#39;tlv&#39;">tlv</span> <span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Sep '11, 09:04</strong></p><img src="https://secure.gravatar.com/avatar/01febacc45af8ecf743c4f575d428326?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JK7&#39;s gravatar image" /><p><span>JK7</span><br />
<span class="score" title="31 reputation points">31</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JK7 has no accepted answers">0%</span></p></div></div><div id="comments-container-6073" class="comments-container"></div><div id="comment-tools-6073" class="comment-tools"></div><div class="clear"></div><div id="comment-6073-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

