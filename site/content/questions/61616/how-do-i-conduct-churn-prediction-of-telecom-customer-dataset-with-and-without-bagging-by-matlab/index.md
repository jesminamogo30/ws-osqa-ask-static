+++
type = "question"
title = "[closed] How do I conduct churn prediction of telecom customer dataset with and without bagging by Matlab?"
description = '''I want to identify graphically the global error in my telecom training and test data set with up to 120 outputs by comparing the result when I apply bagging to logistic regression, linear regression, linear discriminant analysis(LDA), classification tree, neural net and global additive model(GAM) an...'''
date = "2017-05-25T00:18:00Z"
lastmod = "2017-05-25T02:33:00Z"
weight = 61616
keywords = [ "telecom", "statistics", "networking", "matlab", "stackoverflow" ]
aliases = [ "/questions/61616" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How do I conduct churn prediction of telecom customer dataset with and without bagging by Matlab?](/questions/61616/how-do-i-conduct-churn-prediction-of-telecom-customer-dataset-with-and-without-bagging-by-matlab)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61616-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61616-score" class="post-score" title="current number of votes">0</div><span id="post-61616-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to identify graphically the global error in my telecom training and test data set with up to 120 outputs by comparing the result when I apply bagging to logistic regression, linear regression, linear discriminant analysis(LDA), classification tree, neural net and global additive model(GAM) and without bagging (a smoothed global error).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-telecom" rel="tag" title="see questions tagged &#39;telecom&#39;">telecom</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-networking" rel="tag" title="see questions tagged &#39;networking&#39;">networking</span> <span class="post-tag tag-link-matlab" rel="tag" title="see questions tagged &#39;matlab&#39;">matlab</span> <span class="post-tag tag-link-stackoverflow" rel="tag" title="see questions tagged &#39;stackoverflow&#39;">stackoverflow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 May '17, 00:18</strong></p><img src="https://secure.gravatar.com/avatar/2684ca6915e0a949c2442e7ca10cad91?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="moronto&#39;s gravatar image" /><p><span>moronto</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="moronto has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>25 May '17, 02:33</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-61616" class="comments-container"><span id="61619"></span><div id="comment-61619" class="comment"><div id="post-61619-score" class="comment-score"></div><div class="comment-text"><p>Unfortunately this doesn't seem to be a Wireshark question so is off-topic for this site.</p></div><div id="comment-61619-info" class="comment-info"><span class="comment-age">(25 May '17, 02:33)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-61616" class="comment-tools"></div><div class="clear"></div><div id="comment-61616-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 25 May '17, 02:33

</div>

</div>

</div>

