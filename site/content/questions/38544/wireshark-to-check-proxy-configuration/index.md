+++
type = "question"
title = "Wireshark to check proxy configuration"
description = '''I have proxychains set up and checked its working with nmap I have very little experience of wireshark  I used it to check my configuration and its showing my source ip of my wifi does this mean I have a leak and my proxychains set up is incorrect?enter code here'''
date = "2014-12-12T12:56:00Z"
lastmod = "2014-12-12T12:56:00Z"
weight = 38544
keywords = [ "proxy-server", "wireshark" ]
aliases = [ "/questions/38544" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark to check proxy configuration](/questions/38544/wireshark-to-check-proxy-configuration)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38544-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38544-score" class="post-score" title="current number of votes">0</div><span id="post-38544-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have proxychains set up and checked its working with nmap I have very little experience of wireshark I used it to check my configuration and its showing my source ip of my wifi does this mean I have a leak and my proxychains set up is incorrect?<code>enter code here</code></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-proxy-server" rel="tag" title="see questions tagged &#39;proxy-server&#39;">proxy-server</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Dec '14, 12:56</strong></p><img src="https://secure.gravatar.com/avatar/7b0875989d3a0a7213ec8528dab57a3d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Newbie111&#39;s gravatar image" /><p><span>Newbie111</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Newbie111 has no accepted answers">0%</span></p></div></div><div id="comments-container-38544" class="comments-container"></div><div id="comment-tools-38544" class="comment-tools"></div><div class="clear"></div><div id="comment-38544-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

