+++
type = "question"
title = "voice quality"
description = '''I am having voip quality of service issues what should I be looking for , one way audio and choppy voice'''
date = "2014-10-02T10:57:00Z"
lastmod = "2014-12-04T04:57:00Z"
weight = 36798
keywords = [ "voip" ]
aliases = [ "/questions/36798" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [voice quality](/questions/36798/voice-quality)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36798-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36798-score" class="post-score" title="current number of votes">0</div><span id="post-36798-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am having voip quality of service issues what should I be looking for , one way audio and choppy voice</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Oct '14, 10:57</strong></p><img src="https://secure.gravatar.com/avatar/07b90c1820bd26614862d5305f7f8c0a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cdf0010&#39;s gravatar image" /><p><span>cdf0010</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cdf0010 has no accepted answers">0%</span></p></div></div><div id="comments-container-36798" class="comments-container"></div><div id="comment-tools-36798" class="comment-tools"></div><div class="clear"></div><div id="comment-36798-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="37191"></span>

<div id="answer-container-37191" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37191-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37191-score" class="post-score" title="current number of votes">0</div><span id="post-37191-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Bad VoIP quality is usually a symptom of packet loss. Often that's due to not enough bandwidth somewhere between you and wherever the VoIP is terminated.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Oct '14, 05:07</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-37191" class="comments-container"></div><div id="comment-tools-37191" class="comment-tools"></div><div class="clear"></div><div id="comment-37191-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="38321"></span>

<div id="answer-container-38321" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38321-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38321-score" class="post-score" title="current number of votes">0</div><span id="post-38321-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As Jeff said, bandwidth could be an issue. You can use online tools such as <a href="http://telecom.toshiba.com/VIPedge-Assessor/">this</a> to test your connectivity.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Dec '14, 04:57</strong></p><img src="https://secure.gravatar.com/avatar/de0a6baa62fec7a382b9cce5776dbf2e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Liam&#39;s gravatar image" /><p><span>Liam</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Liam has no accepted answers">0%</span></p></div></div><div id="comments-container-38321" class="comments-container"></div><div id="comment-tools-38321" class="comment-tools"></div><div class="clear"></div><div id="comment-38321-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

