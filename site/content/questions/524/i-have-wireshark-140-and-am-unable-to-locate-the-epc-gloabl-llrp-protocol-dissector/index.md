+++
type = "question"
title = "I have wireshark 1.4.0 and am unable to locate the EPC gloabl llrp protocol dissector."
description = '''I see an example on the website of using the dissector but cannot figure our how to enable it. Any help would be greatly appreciated. What i see are the tcp conversation with no decoding as the dissectoru would provide. Thanks.'''
date = "2010-10-18T05:40:00Z"
lastmod = "2010-10-19T11:12:00Z"
weight = 524
keywords = [ "llrp", "global", "epc", "dissector" ]
aliases = [ "/questions/524" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [I have wireshark 1.4.0 and am unable to locate the EPC gloabl llrp protocol dissector.](/questions/524/i-have-wireshark-140-and-am-unable-to-locate-the-epc-gloabl-llrp-protocol-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-524-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-524-score" class="post-score" title="current number of votes">0</div><span id="post-524-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I see an example on the website of using the dissector but cannot figure our how to enable it. Any help would be greatly appreciated. What i see are the tcp conversation with no decoding as the dissectoru would provide. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-llrp" rel="tag" title="see questions tagged &#39;llrp&#39;">llrp</span> <span class="post-tag tag-link-global" rel="tag" title="see questions tagged &#39;global&#39;">global</span> <span class="post-tag tag-link-epc" rel="tag" title="see questions tagged &#39;epc&#39;">epc</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Oct '10, 05:40</strong></p><img src="https://secure.gravatar.com/avatar/1141c2eae0303ff15f3e5dafe86c3658?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jwhoffman&#39;s gravatar image" /><p><span>jwhoffman</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jwhoffman has no accepted answers">0%</span></p></div></div><div id="comments-container-524" class="comments-container"></div><div id="comment-tools-524" class="comment-tools"></div><div class="clear"></div><div id="comment-524-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="529"></span>

<div id="answer-container-529" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-529-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-529-score" class="post-score" title="current number of votes">0</div><span id="post-529-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi, It looks like the dissector never made it into Wireshark. <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=1957">See bug 1957</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Oct '10, 23:08</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Oct '10, 11:34</strong> </span></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span></p></div></div><div id="comments-container-529" class="comments-container"><span id="530"></span><div id="comment-530" class="comment"><div id="post-530-score" class="comment-score"></div><div class="comment-text"><p>How can it be added back in?</p></div><div id="comment-530-info" class="comment-info"><span class="comment-age">(19 Oct '10, 05:26)</span> <span class="comment-user userinfo">jwhoffman</span></div></div><span id="539"></span><div id="comment-539" class="comment"><div id="post-539-score" class="comment-score"></div><div class="comment-text"><p>By fixing the problem reported in the bug :-)</p></div><div id="comment-539-info" class="comment-info"><span class="comment-age">(19 Oct '10, 11:12)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-529" class="comment-tools"></div><div class="clear"></div><div id="comment-529-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

