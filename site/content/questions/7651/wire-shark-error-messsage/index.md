+++
type = "question"
title = "Wire shark error messsage"
description = '''When i am going to save my captured packets, it show the following error message, Microsoft Visual C++ runtime library (This application has requested the runtime to terminate it in an unusual way. Please contact the application&#x27;s support team for more information) , This is  the error message.. Pls...'''
date = "2011-11-26T06:36:00Z"
lastmod = "2011-12-05T12:38:00Z"
weight = 7651
keywords = [ "message", "error" ]
aliases = [ "/questions/7651" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wire shark error messsage](/questions/7651/wire-shark-error-messsage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7651-score" class="post-score" title="current number of votes">0</div><span id="post-7651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When i am going to save my captured packets, it show the following error message, Microsoft Visual C++ runtime library (This application has requested the runtime to terminate it in an unusual way. Please contact the application's support team for more information) , This is the error message.. Pls help me to over come this..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-message" rel="tag" title="see questions tagged &#39;message&#39;">message</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Nov '11, 06:36</strong></p><img src="https://secure.gravatar.com/avatar/bc78c927dc9edffbfa1942f94d18d0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="akbarsha&#39;s gravatar image" /><p><span>akbarsha</span><br />
<span class="score" title="5 reputation points">5</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="akbarsha has no accepted answers">0%</span></p></div></div><div id="comments-container-7651" class="comments-container"><span id="7652"></span><div id="comment-7652" class="comment"><div id="post-7652-score" class="comment-score">1</div><div class="comment-text"><p>This is probably a Wireshark bug.</p><p>Are you able to cause the crash consistently ?</p><p>What version of Wireshark are you using?</p></div><div id="comment-7652-info" class="comment-info"><span class="comment-age">(26 Nov '11, 06:40)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="7776"></span><div id="comment-7776" class="comment"><div id="post-7776-score" class="comment-score"></div><div class="comment-text"><p>im using 1.6.2... i can't solve this problem.. anyway it always shows the error msg whenever im goin to save the packets that i captured...plz help me..</p></div><div id="comment-7776-info" class="comment-info"><span class="comment-age">(05 Dec '11, 12:38)</span> <span class="comment-user userinfo">akbarsha</span></div></div></div><div id="comment-tools-7651" class="comment-tools"></div><div class="clear"></div><div id="comment-7651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

