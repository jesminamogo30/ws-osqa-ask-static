+++
type = "question"
title = "libgpg-error-0.dll: CantPackException: TLS callbacks are not supported	upx"
description = '''when i build wireshark, i got this error Error 1 FilesAppWiresharklibgpg-error-0.dll: CantPackException: TLS callbacks are not supported upx packaging what is that?'''
date = "2011-06-27T00:25:00Z"
lastmod = "2011-06-27T00:25:00Z"
weight = 4763
keywords = [ "libgpg" ]
aliases = [ "/questions/4763" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [libgpg-error-0.dll: CantPackException: TLS callbacks are not supported upx](/questions/4763/libgpg-error-0dll-cantpackexception-tls-callbacks-are-not-supported-upx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4763-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4763-score" class="post-score" title="current number of votes">0</div><span id="post-4763-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when i build wireshark, i got this error</p><p>Error 1 FilesAppWiresharklibgpg-error-0.dll: CantPackException: TLS callbacks are not supported upx packaging</p><p>what is that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-libgpg" rel="tag" title="see questions tagged &#39;libgpg&#39;">libgpg</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jun '11, 00:25</strong></p><img src="https://secure.gravatar.com/avatar/1f74fa27562ae0f98b96fa46254bdd79?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="boomsic&#39;s gravatar image" /><p><span>boomsic</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="boomsic has no accepted answers">0%</span></p></div></div><div id="comments-container-4763" class="comments-container"></div><div id="comment-tools-4763" class="comment-tools"></div><div class="clear"></div><div id="comment-4763-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

