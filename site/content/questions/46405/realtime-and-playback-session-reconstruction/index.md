+++
type = "question"
title = "realtime and playback session reconstruction"
description = '''Hello Forum Does Wireshark support realtime and playback session reconstruction (as the user sees it in his webbrowser, including HTML, Ajax or other dynamic techniques) ? Thank you very much for every feedback! Joe'''
date = "2015-10-07T10:19:00Z"
lastmod = "2015-12-02T10:32:00Z"
weight = 46405
keywords = [ "session", "reconstruction" ]
aliases = [ "/questions/46405" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [realtime and playback session reconstruction](/questions/46405/realtime-and-playback-session-reconstruction)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46405-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46405-score" class="post-score" title="current number of votes">0</div><span id="post-46405-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Forum</p><p>Does Wireshark support realtime and playback session reconstruction (as the user sees it in his webbrowser, including HTML, Ajax or other dynamic techniques) ?</p><p>Thank you very much for every feedback!</p><p>Joe</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-session" rel="tag" title="see questions tagged &#39;session&#39;">session</span> <span class="post-tag tag-link-reconstruction" rel="tag" title="see questions tagged &#39;reconstruction&#39;">reconstruction</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '15, 10:19</strong></p><img src="https://secure.gravatar.com/avatar/c08acf577aad3b14e932ee8f48cf7d20?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joseph123&#39;s gravatar image" /><p><span>joseph123</span><br />
<span class="score" title="11 reputation points">11</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joseph123 has no accepted answers">0%</span></p></div></div><div id="comments-container-46405" class="comments-container"></div><div id="comment-tools-46405" class="comment-tools"></div><div class="clear"></div><div id="comment-46405-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46418"></span>

<div id="answer-container-46418" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46418-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46418-score" class="post-score" title="current number of votes">1</div><span id="post-46418-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No. Wireshark, via the aid of various other libraries, captures packets (or opens capture files) for analysis in the Wireshark UI. It does not replay captured traffic, no display the reconstructed webpage. there are other tools for that.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Oct '15, 03:46</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-46418" class="comments-container"><span id="48205"></span><div id="comment-48205" class="comment"><div id="post-48205-score" class="comment-score"></div><div class="comment-text"><p>Thank you very much for the help/feedback!</p><p>Ok I understand... what applications are you thingking about when it comes to session reconstruction? (e.g. I tried Xplico 2 years ago.. but it was not the thing i was looking for ....)</p><p>Any feedback is apprecaited very much. Thank you! Joe</p></div><div id="comment-48205-info" class="comment-info"><span class="comment-age">(02 Dec '15, 10:32)</span> <span class="comment-user userinfo">joseph123</span></div></div></div><div id="comment-tools-46418" class="comment-tools"></div><div class="clear"></div><div id="comment-46418-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

