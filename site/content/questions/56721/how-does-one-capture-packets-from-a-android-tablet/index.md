+++
type = "question"
title = "How does one capture packets from a Android tablet"
description = '''I very new to Wireshark and would appreciate any help. The unit im trying to capture from is a Samsung Galaxy Tab 4.'''
date = "2016-10-26T18:58:00Z"
lastmod = "2016-10-27T02:41:00Z"
weight = 56721
keywords = [ "android", "4", "tab" ]
aliases = [ "/questions/56721" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How does one capture packets from a Android tablet](/questions/56721/how-does-one-capture-packets-from-a-android-tablet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56721-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56721-score" class="post-score" title="current number of votes">0</div><span id="post-56721-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I very new to Wireshark and would appreciate any help. The unit im trying to capture from is a Samsung Galaxy Tab 4.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-4" rel="tag" title="see questions tagged &#39;4&#39;">4</span> <span class="post-tag tag-link-tab" rel="tag" title="see questions tagged &#39;tab&#39;">tab</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '16, 18:58</strong></p><img src="https://secure.gravatar.com/avatar/3fcdfdf20b4df94c62f224f2946c252e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sundancekid57&#39;s gravatar image" /><p><span>sundancekid57</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sundancekid57 has no accepted answers">0%</span></p></div></div><div id="comments-container-56721" class="comments-container"></div><div id="comment-tools-56721" class="comment-tools"></div><div class="clear"></div><div id="comment-56721-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56731"></span>

<div id="answer-container-56731" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56731-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56731-score" class="post-score" title="current number of votes">0</div><span id="post-56731-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you dont root the device, then tPacketCapture app can be used (it starts a VPN service on top since only root user can access the captures), it has a lot of limitations such as DNS traffic can not be captured just to mention one.</p><p>If you root the device, then you can run the normal tcpdump binary or any app wrapping the tcpdump binary (shark for root for instance).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Oct '16, 02:41</strong></p><img src="https://secure.gravatar.com/avatar/1e84fb88c367cef82ae127b8c164750e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yhzs8&#39;s gravatar image" /><p><span>yhzs8</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yhzs8 has no accepted answers">0%</span></p></div></div><div id="comments-container-56731" class="comments-container"></div><div id="comment-tools-56731" class="comment-tools"></div><div class="clear"></div><div id="comment-56731-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

