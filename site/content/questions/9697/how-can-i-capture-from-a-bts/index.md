+++
type = "question"
title = "How can I capture from a BTS"
description = '''hi all, i am a newbie for this tool, my question is i have BTS physically with me i want to capture the packets where exactly it is routed ? so i don&#x27;t know how to start! if anyone can help with this it will be great! thanking you in advance!'''
date = "2012-03-22T05:16:00Z"
lastmod = "2012-03-22T05:16:00Z"
weight = 9697
keywords = [ "capture", "gsm" ]
aliases = [ "/questions/9697" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I capture from a BTS](/questions/9697/how-can-i-capture-from-a-bts)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9697-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9697-score" class="post-score" title="current number of votes">0</div><span id="post-9697-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi all, i am a newbie for this tool, my question is i have BTS physically with me i want to capture the packets where exactly it is routed ? so i don't know how to start! if anyone can help with this it will be great! thanking you in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-gsm" rel="tag" title="see questions tagged &#39;gsm&#39;">gsm</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Mar '12, 05:16</strong></p><img src="https://secure.gravatar.com/avatar/303cb542cdfc8af61444e79be47290e5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="anil&#39;s gravatar image" /><p><span>anil</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="anil has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>22 Mar '12, 05:52</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-9697" class="comments-container"></div><div id="comment-tools-9697" class="comment-tools"></div><div class="clear"></div><div id="comment-9697-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

