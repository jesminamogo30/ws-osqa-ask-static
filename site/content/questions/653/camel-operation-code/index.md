+++
type = "question"
title = "CAMEL Operation Code"
description = '''I&#x27;m sending a CAMEL Invoke ContinueWithArgument message with an opCode of 56 but the wireshark trace shows &#x27;invoke Unknown CAMEL (56)&#x27;. What is the correct opcode for this message? '''
date = "2010-10-26T07:11:00Z"
lastmod = "2010-10-26T11:26:00Z"
weight = 653
keywords = [ "tag", "opcode", "continuewithargument", "camel" ]
aliases = [ "/questions/653" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [CAMEL Operation Code](/questions/653/camel-operation-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-653-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-653-score" class="post-score" title="current number of votes">0</div><span id="post-653-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm sending a CAMEL Invoke ContinueWithArgument message with an opCode of 56 but the wireshark trace shows 'invoke Unknown CAMEL (56)'. What is the correct opcode for this message?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tag" rel="tag" title="see questions tagged &#39;tag&#39;">tag</span> <span class="post-tag tag-link-opcode" rel="tag" title="see questions tagged &#39;opcode&#39;">opcode</span> <span class="post-tag tag-link-continuewithargument" rel="tag" title="see questions tagged &#39;continuewithargument&#39;">continuewithargument</span> <span class="post-tag tag-link-camel" rel="tag" title="see questions tagged &#39;camel&#39;">camel</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '10, 07:11</strong></p><img src="https://secure.gravatar.com/avatar/500a1903ac7d35a475f37da84357748f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dbartlett2010&#39;s gravatar image" /><p><span>dbartlett2010</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dbartlett2010 has no accepted answers">0%</span></p></div></div><div id="comments-container-653" class="comments-container"></div><div id="comment-tools-653" class="comment-tools"></div><div class="clear"></div><div id="comment-653-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="681"></span>

<div id="answer-container-681" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-681-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-681-score" class="post-score" title="current number of votes">0</div><span id="post-681-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><h1 id="define-opcode_continuewithargument-88">define opcode_continueWithArgument 88</h1></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Oct '10, 11:26</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-681" class="comments-container"></div><div id="comment-tools-681" class="comment-tools"></div><div class="clear"></div><div id="comment-681-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

