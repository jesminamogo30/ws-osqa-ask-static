+++
type = "question"
title = "Wireshark capture question"
description = '''Hello,  Can someone help to figure out what methods to use to find ip address of the person implementing the teardrop attack and the ip address of the one who is being attacked? Also, how would I figure out the byte numbers that are being replaced by the teardrop attack.  The capture that I am using...'''
date = "2012-02-09T22:07:00Z"
lastmod = "2012-02-09T22:07:00Z"
weight = 8942
keywords = [ "capture", "attack", "udp", "packet", "tcp" ]
aliases = [ "/questions/8942" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark capture question](/questions/8942/wireshark-capture-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8942-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8942-score" class="post-score" title="current number of votes">0</div><span id="post-8942-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, Can someone help to figure out what methods to use to find ip address of the person implementing the teardrop attack and the ip address of the one who is being attacked? Also, how would I figure out the byte numbers that are being replaced by the teardrop attack.</p><p>The capture that I am using is the sample teardrop that is found from wiki wireshark; <a href="http://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;do=view&amp;target=teardrop.cap.">http://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;do=view&amp;target=teardrop.cap.</a></p><p>Any help would be great. Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-attack" rel="tag" title="see questions tagged &#39;attack&#39;">attack</span> <span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Feb '12, 22:07</strong></p><img src="https://secure.gravatar.com/avatar/50f1321c53aa65c3d80f41fe52798e1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jason007&#39;s gravatar image" /><p><span>Jason007</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jason007 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Feb '12, 20:41</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-8942" class="comments-container"></div><div id="comment-tools-8942" class="comment-tools"></div><div class="clear"></div><div id="comment-8942-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

