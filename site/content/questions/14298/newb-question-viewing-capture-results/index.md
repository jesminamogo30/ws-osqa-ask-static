+++
type = "question"
title = "Newb question; viewing capture results"
description = '''Hello I&#x27;m taking a networking class and have been searching all weekend for my answer and I can&#x27;t figure out how to do it. I was told to do a 1 minute capture. I did that. I had like 604 packets captured. How do I find out which source address was used the most? How do I find that break down? Also i...'''
date = "2012-09-15T19:36:00Z"
lastmod = "2012-09-16T22:35:00Z"
weight = 14298
keywords = [ "source", "traffic", "results", "packet" ]
aliases = [ "/questions/14298" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Newb question; viewing capture results](/questions/14298/newb-question-viewing-capture-results)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14298-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14298-score" class="post-score" title="current number of votes">0</div><span id="post-14298-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello I'm taking a networking class and have been searching all weekend for my answer and I can't figure out how to do it.</p><p>I was told to do a 1 minute capture. I did that. I had like 604 packets captured. How do I find out which source address was used the most? How do I find that break down?</p><p>Also is asks: b. What sort of traffic is using the most network resources? How do I sort that out too?</p><p>Thanks, I know its probably something simple, but I just couldn't find it on the web.</p><p>David</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-source" rel="tag" title="see questions tagged &#39;source&#39;">source</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-results" rel="tag" title="see questions tagged &#39;results&#39;">results</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Sep '12, 19:36</strong></p><img src="https://secure.gravatar.com/avatar/7380c70c69b1739023ddd6d6e3872034?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dstrausser83&#39;s gravatar image" /><p><span>dstrausser83</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dstrausser83 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Sep '12, 19:37</strong> </span></p></div></div><div id="comments-container-14298" class="comments-container"></div><div id="comment-tools-14298" class="comment-tools"></div><div class="clear"></div><div id="comment-14298-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14300"></span>

<div id="answer-container-14300" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14300-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14300-score" class="post-score" title="current number of votes">1</div><span id="post-14300-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>take a look at the statistics menu, especially the Endpoint statistics and the protocol hierarchy. With the help of those two you should be able to answer your questions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Sep '12, 03:04</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-14300" class="comments-container"><span id="14304"></span><div id="comment-14304" class="comment"><div id="post-14304-score" class="comment-score"></div><div class="comment-text"><p>Thank you.</p><p>[Bill Meier: Converted to a comment in keeping with the format of <a href="http://ask.wireshrk.org">ask.wireshrk.org</a>. please see the FAQ].</p></div><div id="comment-14304-info" class="comment-info"><span class="comment-age">(16 Sep '12, 09:29)</span> <span class="comment-user userinfo">dstrausser83</span></div></div><span id="14313"></span><div id="comment-14313" class="comment"><div id="post-14313-score" class="comment-score"></div><div class="comment-text"><p>You can find more information about statistics in the <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChStatistics.html">Wireshark User's Guide</a></p></div><div id="comment-14313-info" class="comment-info"><span class="comment-age">(16 Sep '12, 22:35)</span> <span class="comment-user userinfo">joke</span></div></div></div><div id="comment-tools-14300" class="comment-tools"></div><div class="clear"></div><div id="comment-14300-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

