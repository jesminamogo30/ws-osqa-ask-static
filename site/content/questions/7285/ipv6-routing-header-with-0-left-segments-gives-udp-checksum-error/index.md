+++
type = "question"
title = "IPv6 Routing Header with 0 left segments gives UDP checksum error"
description = '''A captured frame is stating the UDP checksum is in error. The frame is IPv6 and contains a type-2 routing header. The &quot;Left Segments&quot; is 0. Since there isn&#x27;t a segment left, shouldn&#x27;t the checksum be based on the L3 IP destination address and not the address in the routing header? I&#x27;m using Wireshar...'''
date = "2011-11-08T10:00:00Z"
lastmod = "2011-11-08T10:00:00Z"
weight = 7285
keywords = [ "header", "type-2", "routing", "ipv6" ]
aliases = [ "/questions/7285" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [IPv6 Routing Header with 0 left segments gives UDP checksum error](/questions/7285/ipv6-routing-header-with-0-left-segments-gives-udp-checksum-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7285-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7285-score" class="post-score" title="current number of votes">0</div><span id="post-7285-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>A captured frame is stating the UDP checksum is in error. The frame is IPv6 and contains a type-2 routing header. The "Left Segments" is 0. Since there isn't a segment left, shouldn't the checksum be based on the L3 IP destination address and not the address in the routing header? I'm using Wireshark version 1.6.3 on my MAC.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-header" rel="tag" title="see questions tagged &#39;header&#39;">header</span> <span class="post-tag tag-link-type-2" rel="tag" title="see questions tagged &#39;type-2&#39;">type-2</span> <span class="post-tag tag-link-routing" rel="tag" title="see questions tagged &#39;routing&#39;">routing</span> <span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '11, 10:00</strong></p><img src="https://secure.gravatar.com/avatar/a7ebcfc9938ca262a77295f0cc716985?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mayn&#39;s gravatar image" /><p><span>mayn</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mayn has no accepted answers">0%</span></p></div></div><div id="comments-container-7285" class="comments-container"></div><div id="comment-tools-7285" class="comment-tools"></div><div class="clear"></div><div id="comment-7285-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

