+++
type = "question"
title = "No traffic at all!"
description = '''Running on Mac OS X 10.9 I&#x27;m a beginner trying to teach myself some networking. I&#x27;ve just installed wireshark, but can&#x27;t detect any traffic. In the interfaces dialogue box, it claims there&#x27;s no packets or traffic anywhere, and when I start a capture, nothing is detected. Which to me seems impossible...'''
date = "2015-07-18T03:50:00Z"
lastmod = "2015-07-18T03:50:00Z"
weight = 44280
keywords = [ "newbie", "traffic", "troubleshooting", "capture-setup" ]
aliases = [ "/questions/44280" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No traffic at all!](/questions/44280/no-traffic-at-all)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44280-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44280-score" class="post-score" title="current number of votes">0</div><span id="post-44280-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Running on Mac OS X 10.9</p><p>I'm a beginner trying to teach myself some networking. I've just installed wireshark, but can't detect any traffic. In the interfaces dialogue box, it claims there's no packets or traffic anywhere, and when I start a capture, nothing is detected. Which to me seems impossible, as I'm browsing the internet right now. It seems related to a mystery that's sprung up on terminal - since about a week ago, whenever I enter 'ifconfig' into the mac terminal, all the interfaces claim to be 'inactive' and give no IP address (except lo0, of course, which has the usual IP address). Again, how can that be as I'm surfing the net? I'm using an o2 usb dongle, which HAS shown an active en1 connection on ifconfig in the past.</p><p>Completely confused.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-newbie" rel="tag" title="see questions tagged &#39;newbie&#39;">newbie</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span> <span class="post-tag tag-link-capture-setup" rel="tag" title="see questions tagged &#39;capture-setup&#39;">capture-setup</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '15, 03:50</strong></p><img src="https://secure.gravatar.com/avatar/a00adba94026bb50ea47ec28b3e92ce2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Crane&#39;s gravatar image" /><p><span>Crane</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Crane has no accepted answers">0%</span></p></div></div><div id="comments-container-44280" class="comments-container"></div><div id="comment-tools-44280" class="comment-tools"></div><div class="clear"></div><div id="comment-44280-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

