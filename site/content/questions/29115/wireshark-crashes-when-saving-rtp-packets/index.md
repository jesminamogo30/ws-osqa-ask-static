+++
type = "question"
title = "wireshark crashes when saving RTP packets"
description = '''I am transmitting video via vlc to the client-pc. in paralel, im capture the rtp packets. but when I want to save the pcap file, wireshark crashes. the stream is about 30 seconds. if someone knows what am I doing wrong, I will be happy for some guidness. thanks!'''
date = "2014-01-23T03:24:00Z"
lastmod = "2014-01-23T05:10:00Z"
weight = 29115
keywords = [ "wireshark_crashed", "crash", "videostream", "video", "rtp" ]
aliases = [ "/questions/29115" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark crashes when saving RTP packets](/questions/29115/wireshark-crashes-when-saving-rtp-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29115-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29115-score" class="post-score" title="current number of votes">0</div><span id="post-29115-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am transmitting video via vlc to the client-pc. in paralel, im capture the rtp packets.</p><p>but when I want to save the pcap file, wireshark crashes.</p><p>the stream is about 30 seconds.</p><p>if someone knows what am I doing wrong, I will be happy for some guidness.</p><p>thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark_crashed" rel="tag" title="see questions tagged &#39;wireshark_crashed&#39;">wireshark_crashed</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span> <span class="post-tag tag-link-videostream" rel="tag" title="see questions tagged &#39;videostream&#39;">videostream</span> <span class="post-tag tag-link-video" rel="tag" title="see questions tagged &#39;video&#39;">video</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jan '14, 03:24</strong></p><img src="https://secure.gravatar.com/avatar/19382512e78f5e497f0dfbd170a09837?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dr%20seuss&#39;s gravatar image" /><p><span>dr seuss</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dr seuss has no accepted answers">0%</span></p></div></div><div id="comments-container-29115" class="comments-container"><span id="29116"></span><div id="comment-29116" class="comment"><div id="post-29116-score" class="comment-score"></div><div class="comment-text"><p>What is your</p><ul><li>OS and OS version</li><li>Wireshark version</li></ul><p>What is the crash message?</p></div><div id="comment-29116-info" class="comment-info"><span class="comment-age">(23 Jan '14, 03:27)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="29117"></span><div id="comment-29117" class="comment"><div id="post-29117-score" class="comment-score"></div><div class="comment-text"><p>my OS is windows 7 the wireshark is 1.10.5</p><p>there is no message, when i hit the save button, it just stucks..</p></div><div id="comment-29117-info" class="comment-info"><span class="comment-age">(23 Jan '14, 03:32)</span> <span class="comment-user userinfo">dr seuss</span></div></div><span id="29118"></span><div id="comment-29118" class="comment"><div id="post-29118-score" class="comment-score"></div><div class="comment-text"><p>Is there any Security Software on the system?</p></div><div id="comment-29118-info" class="comment-info"><span class="comment-age">(23 Jan '14, 04:04)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="29119"></span><div id="comment-29119" class="comment"><div id="post-29119-score" class="comment-score"></div><div class="comment-text"><p>firewall. but i can see the RTP packets on the client, with wireshark. I guess that the transmision is o.k</p></div><div id="comment-29119-info" class="comment-info"><span class="comment-age">(23 Jan '14, 04:08)</span> <span class="comment-user userinfo">dr seuss</span></div></div><span id="29120"></span><div id="comment-29120" class="comment"><div id="post-29120-score" class="comment-score"></div><div class="comment-text"><p>What about AV and Endpoint Security that blocks file write access?</p></div><div id="comment-29120-info" class="comment-info"><span class="comment-age">(23 Jan '14, 05:10)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29115" class="comment-tools"></div><div class="clear"></div><div id="comment-29115-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

