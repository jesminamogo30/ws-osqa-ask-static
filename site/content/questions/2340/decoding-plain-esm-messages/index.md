+++
type = "question"
title = "Decoding plain ESM messages"
description = '''How can plain ESM messages using the NAS-EPS protocol of Wireshark Version 1.4.3 is be decoded successfully? Feeding in a plain ESM information request (e.g. 0x0201D9) as it is the decoder complains that it needs to be integrity protected. If the same message is put into a ESM container within an EM...'''
date = "2011-02-15T01:21:00Z"
lastmod = "2011-02-15T11:16:00Z"
weight = 2340
keywords = [ "esm" ]
aliases = [ "/questions/2340" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding plain ESM messages](/questions/2340/decoding-plain-esm-messages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2340-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2340-score" class="post-score" title="current number of votes">0</div><span id="post-2340-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can plain ESM messages using the NAS-EPS protocol of Wireshark Version 1.4.3 is be decoded successfully? Feeding in a plain ESM information request (e.g. 0x0201D9) as it is the decoder complains that it needs to be integrity protected. If the same message is put into a ESM container within an EMM message it is decoded successfully.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-esm" rel="tag" title="see questions tagged &#39;esm&#39;">esm</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '11, 01:21</strong></p><img src="https://secure.gravatar.com/avatar/5e907875013d8ca66bef5ce443b83567?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nabla&#39;s gravatar image" /><p><span>nabla</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nabla has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-2340" class="comments-container"><span id="2352"></span><div id="comment-2352" class="comment"><div id="post-2352-score" class="comment-score"></div><div class="comment-text"><p>Hi, Try it on a development build. LTE protocols are much more updated in trunk.</p></div><div id="comment-2352-info" class="comment-info"><span class="comment-age">(15 Feb '11, 11:16)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-2340" class="comment-tools"></div><div class="clear"></div><div id="comment-2340-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

