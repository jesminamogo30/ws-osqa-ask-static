+++
type = "question"
title = "extract qr.png"
description = '''how can i extract a picture from a capture like this Bob: begin 664 qrcode.png Bob: MB5!.1PT*&amp;amp;@H````-24A$4@```7(```%R`0````#`7VRD```&quot;BTE$051XG.V: Bob: MS6WC,!&quot;%OUD2R%$&quot;4H!+H3O8DH*4M!U(I;@#Z6B`PML#2&amp;lt;?Q(@F&quot;R#]:S!P$ Bob: M_7R&#x27;!Q!#SKR1B&amp;gt;_$^.M;.#COO//..^^&#92;&#92;Q_Q5B-BUB&#92;&amp;amp;&amp;lt;[VS_=R^[6^HQ_F5 Bob...'''
date = "2016-10-08T07:14:00Z"
lastmod = "2016-10-14T14:37:00Z"
weight = 56232
keywords = [ "picture", "png" ]
aliases = [ "/questions/56232" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [extract qr.png](/questions/56232/extract-qrpng)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56232-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56232-score" class="post-score" title="current number of votes">0</div><span id="post-56232-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how can i extract a picture from a capture like this</p><pre><code>Bob: begin 664 qrcode.png
Bob: MB5!.1PT*&amp;@H````[email protected]```7(```%R`0````#`7VRD```&quot;BTE$051XG.V:
Bob: MS6WC,!&quot;%OUD2R%$&quot;4H!+H3O8DH*4M!U(I;@#Z6B`PML#2&lt;?Q(@F&quot;R#]:S!P$
Bob: M_7R&#39;!Q!#SKR1B&gt;_$^.M;.#COO//..^^\\Q_Q5B-BUB\&amp;&lt;[VS_=R^[6^HQ_F5
Bob: M^21)FH!T&gt;*HE5[T+DB2]YZ^MQ_F5^;EFJ`86LWV78=P=#0`SB[?6X_QU&gt;+-=
Bob: M!EA,`Z#ASGJ&lt;7Y&gt;7IB&quot;8(T&quot;0[&gt;^LQ_F?\6WC[03,`/-SUKC+$5BBF.&#39;&lt;`GDT
Bob: M_&lt;Y_$&gt;?%$T&amp;DZ&gt;+2OB5)&amp;AY-O_-?A-Y&#39;&gt;QVDH9-J87T*7]^-\35_TU1KJ=(D
Bob: M06BMT12DH&lt;N&gt;O]OD+_.WK&quot;6==&#39;9)\O7=*$];U&gt;IOU%6&gt;0MF:Z]).P??G;?*U
Bob: M?DZ&#39;F(U.&amp;+P5S$O4:$&amp;,NZ/I-GJ&lt;7Y=O^5L&gt;:[email protected]#H\NE,KNGL^;LUON2O
Bob: MI:E&#39;X^\)F&amp;,&amp;2A++Z&quot;8,PJWT.&#39;\5?NRAF-`O.AIT&amp;;,&gt;-,P1:5KLMGJ&lt;7Y=/
Bob: M4]V06RPF&#39;=I4&lt;.Q#V\(?5+_S&#39;\1I,+08\&quot;1&amp;&quot;WH;$@K*[NWUU3;YRX9(4J89
Bob: M&amp;KG-CSKWKS;-VYYZUC):A&#39;2(G_+7UN/\6OS)O\I()YNJF)3-\RA#!N^/-LR/
Bob: M/9C9D_3:+U:&amp;_.D02Q%][email protected]]J&#39;[G/^&gt;K`YFQ/6W2\+JKZ7P&#39;/&lt;ZOQ%=_HSZ%
Bob: M3/I32V&gt;#F&amp;%^SC#W&gt;/V\9?[LU(5RX$K-Z:CNQRWU.+\2?S;_+9OT`,WN*[email protected]
Bob: M:J?D_=$6^&lt;L^R)(`9JH=G88EBOG9_W_^;_C%&amp;&#39;=&#39;LY=I,49[4JFN]_?2X_P/
Bob: M^,O_)S5:D$&amp;D9&gt;UB2EK&lt;G]PF_\__DR7:KW45F?#S=YM\R=^W%O?BKCYVI]&gt;/
Bob: AIM]YYYUWWGGGM\C_!6O6TL=I,[S%`````$E%3D2N0F&quot;&quot;
Bob: `
Bob: end</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-picture" rel="tag" title="see questions tagged &#39;picture&#39;">picture</span> <span class="post-tag tag-link-png" rel="tag" title="see questions tagged &#39;png&#39;">png</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Oct '16, 07:14</strong></p><img src="https://secure.gravatar.com/avatar/f344a1ac12cdf5ec4ec055941ff3d255?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kpoeyer&#39;s gravatar image" /><p><span>kpoeyer</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kpoeyer has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Oct '16, 08:14</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-56232" class="comments-container"><span id="56237"></span><div id="comment-56237" class="comment"><div id="post-56237-score" class="comment-score"></div><div class="comment-text"><p>How was that data captured, and what was the protocol?</p></div><div id="comment-56237-info" class="comment-info"><span class="comment-age">(08 Oct '16, 10:48)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="56238"></span><div id="comment-56238" class="comment"><div id="post-56238-score" class="comment-score"></div><div class="comment-text"><p>this is a grab for a challenge, i have to answer 4 questions, 1. a ip adress 2. a time stamp 3 a location in a qr.png image</p><p>i can send you the whole capture if you want</p><p>kind regards Kees</p></div><div id="comment-56238-info" class="comment-info"><span class="comment-age">(08 Oct '16, 10:58)</span> <span class="comment-user userinfo">kpoeyer</span></div></div><span id="56250"></span><div id="comment-56250" class="comment"><div id="post-56250-score" class="comment-score">1</div><div class="comment-text"><p>Look for uudecode.</p><p>52.079798, 4.317066 (q=52.079798,4.317066)</p></div><div id="comment-56250-info" class="comment-info"><span class="comment-age">(09 Oct '16, 04:00)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-56232" class="comment-tools"></div><div class="clear"></div><div id="comment-56232-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="56393"></span>

<div id="answer-container-56393" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56393-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56393-score" class="post-score" title="current number of votes">0</div><span id="post-56393-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This used to be some work but it isn't anymore. The option is under File and Export Objects, I'd guess it's from an HTTP stream. Pick the protocol and WireShark will show you everything that's available to parse out.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Oct '16, 14:37</strong></p><img src="https://secure.gravatar.com/avatar/0ca762ea8fec4622f09ecc44fc10384c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Keseymour&#39;s gravatar image" /><p><span>Keseymour</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Keseymour has no accepted answers">0%</span></p></div></div><div id="comments-container-56393" class="comments-container"></div><div id="comment-tools-56393" class="comment-tools"></div><div class="clear"></div><div id="comment-56393-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

