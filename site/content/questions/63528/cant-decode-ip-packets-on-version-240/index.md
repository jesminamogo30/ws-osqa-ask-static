+++
type = "question"
title = "can&#x27;t decode IP packets on version 2.4.0"
description = '''when we install wireshark Version 2.4.0 on a laptop, we open a pcap file but only ethernet header can be decode in details, we can not found any IP header information. even we uninstall and install, restart laptop again the problem is exist.  but we install same version on another laptop, everything...'''
date = "2017-08-28T04:12:00Z"
lastmod = "2017-08-28T04:51:00Z"
weight = 63528
keywords = [ "installer" ]
aliases = [ "/questions/63528" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can't decode IP packets on version 2.4.0](/questions/63528/cant-decode-ip-packets-on-version-240)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63528-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63528-score" class="post-score" title="current number of votes">0</div><span id="post-63528-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when we install wireshark Version 2.4.0 on a laptop, we open a pcap file but only ethernet header can be decode in details, we can not found any IP header information. even we uninstall and install, restart laptop again the problem is exist. but we install same version on another laptop, everything is ok and we can found ip header in details. we compared the OS and it's same version(win7) between 2 laptop. what's the issue ? is there have some old configuration which not deleted when uninstall wireshark? or is ir lost some dll when we install wireshark ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installer" rel="tag" title="see questions tagged &#39;installer&#39;">installer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '17, 04:12</strong></p><img src="https://secure.gravatar.com/avatar/853d7093103a60a3b0083b42b705b99e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="neil_hao&#39;s gravatar image" /><p><span>neil_hao</span><br />
<span class="score" title="26 reputation points">26</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="neil_hao has no accepted answers">0%</span></p></div></div><div id="comments-container-63528" class="comments-container"></div><div id="comment-tools-63528" class="comment-tools"></div><div class="clear"></div><div id="comment-63528-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63531"></span>

<div id="answer-container-63531" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63531-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63531-score" class="post-score" title="current number of votes">1</div><span id="post-63531-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>More than likely a local configuration issue.</p><p>From the Help -&gt; About Wireshark, "Folders" dialog, click on the "Personal configuration" link and move all the files and directories found there to a temporary spot leaving nothing behind. Stop and restart Wireshark.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Aug '17, 04:51</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-63531" class="comments-container"></div><div id="comment-tools-63531" class="comment-tools"></div><div class="clear"></div><div id="comment-63531-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

