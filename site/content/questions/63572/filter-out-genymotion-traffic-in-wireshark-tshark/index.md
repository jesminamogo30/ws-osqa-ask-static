+++
type = "question"
title = "Filter out Genymotion traffic in Wireshark (tshark)"
description = '''I have multiple Genymotion emulator, which I want to capture their traffic on the host system which they are running. Here is the system setting:  The host is assigned a valid static IP. Genymotions network mode is set to NAT, Since by the static IP assigned to machine It&#x27;s not possible to have Brid...'''
date = "2017-09-07T04:40:00Z"
lastmod = "2017-09-07T04:40:00Z"
weight = 63572
keywords = [ "traffic-analysis", "capture-filter", "sniffng", "virtualbox" ]
aliases = [ "/questions/63572" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Filter out Genymotion traffic in Wireshark (tshark)](/questions/63572/filter-out-genymotion-traffic-in-wireshark-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63572-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63572-score" class="post-score" title="current number of votes">0</div><span id="post-63572-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have multiple Genymotion emulator, which I want to capture their traffic on the host system which they are running.</p><p>Here is the system setting:</p><ul><li>The host is assigned a valid static IP.</li><li>Genymotions network mode is set to NAT, Since by the static IP assigned to machine It's not possible to have Bridge mode. So All the genymotion get same IP address as the host machine while in Wireshark. Also all have the same inner IP assigned.</li></ul><p>So, Is it possible to filter out the traffic using each devices mac address? Is there any solution to have the separated traffic?</p><p>PS: Genymotion is run on the top of Virtualbox.</p><p>Thank you in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic-analysis" rel="tag" title="see questions tagged &#39;traffic-analysis&#39;">traffic-analysis</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-sniffng" rel="tag" title="see questions tagged &#39;sniffng&#39;">sniffng</span> <span class="post-tag tag-link-virtualbox" rel="tag" title="see questions tagged &#39;virtualbox&#39;">virtualbox</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Sep '17, 04:40</strong></p><img src="https://secure.gravatar.com/avatar/1595a24111dff7d0376d456e91895399?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zahra&#39;s gravatar image" /><p><span>Zahra</span><br />
<span class="score" title="31 reputation points">31</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zahra has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Sep '17, 09:47</strong> </span></p></div></div><div id="comments-container-63572" class="comments-container"></div><div id="comment-tools-63572" class="comment-tools"></div><div class="clear"></div><div id="comment-63572-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

