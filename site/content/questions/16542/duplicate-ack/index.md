+++
type = "question"
title = "Duplicate Ack"
description = '''What is the assumption when analyzing the Dup Ack to know whether if packets are actually getting dropped on the network? A single transaction is showing 20% of Dup Ack but no retransmissions. '''
date = "2012-12-04T07:13:00Z"
lastmod = "2012-12-04T07:24:00Z"
weight = 16542
keywords = [ "dup", "ack" ]
aliases = [ "/questions/16542" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Duplicate Ack](/questions/16542/duplicate-ack)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16542-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16542-score" class="post-score" title="current number of votes">0</div><span id="post-16542-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What is the assumption when analyzing the Dup Ack to know whether if packets are actually getting dropped on the network? A single transaction is showing 20% of Dup Ack but no retransmissions.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dup" rel="tag" title="see questions tagged &#39;dup&#39;">dup</span> <span class="post-tag tag-link-ack" rel="tag" title="see questions tagged &#39;ack&#39;">ack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '12, 07:13</strong></p><img src="https://secure.gravatar.com/avatar/9d629f265392eaf7b61f921e25f9f730?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ws2006&#39;s gravatar image" /><p><span>ws2006</span><br />
<span class="score" title="1 reputation points">1</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ws2006 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-16542" class="comments-container"><span id="16543"></span><div id="comment-16543" class="comment"><div id="post-16543-score" class="comment-score"></div><div class="comment-text"><p><span>@ws2006</span>: I just spotted that this is question number 10 you are asking here and you didn't accept any of the answers that the community gave to you in your previous threads. Please consider voting / accepting answers and by that being fair to others spending time to help you</p></div><div id="comment-16543-info" class="comment-info"><span class="comment-age">(04 Dec '12, 07:24)</span> <span class="comment-user userinfo">Landi</span></div></div></div><div id="comment-tools-16542" class="comment-tools"></div><div class="clear"></div><div id="comment-16542-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

