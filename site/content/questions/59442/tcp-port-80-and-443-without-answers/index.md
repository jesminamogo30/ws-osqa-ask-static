+++
type = "question"
title = "TCP Port 80 and 443 without answers"
description = '''Hello, I am having problems in my network to access the web pages port 80, 443 and 8080. I have little experience in the package analysis, but I think that in the capture that I share in this link  https://www.cloudshark.org/captures/6387219e135d  the packages are requested to port 80, 443 and 8080 ...'''
date = "2017-02-15T12:26:00Z"
lastmod = "2017-02-15T12:26:00Z"
weight = 59442
keywords = [ "tcp" ]
aliases = [ "/questions/59442" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TCP Port 80 and 443 without answers](/questions/59442/tcp-port-80-and-443-without-answers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59442-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59442-score" class="post-score" title="current number of votes">0</div><span id="post-59442-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am having problems in my network to access the web pages port 80, 443 and 8080.</p><p>I have little experience in the package analysis, but I think that in the capture that I share in this link</p><p><a href="https://www.cloudshark.org/captures/6387219e135d">https://www.cloudshark.org/captures/6387219e135d</a></p><p>the packages are requested to port 80, 443 and 8080 but there is no answer, ¿ Is it right ?, could someone give me their opinion ?.</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '17, 12:26</strong></p><img src="https://secure.gravatar.com/avatar/f60c43bbf8c474bf020f7d7f93da24b5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hyxion&#39;s gravatar image" /><p><span>Hyxion</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hyxion has no accepted answers">0%</span></p></div></div><div id="comments-container-59442" class="comments-container"></div><div id="comment-tools-59442" class="comment-tools"></div><div class="clear"></div><div id="comment-59442-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

