+++
type = "question"
title = "WifiKill Android app kicking all users off wireless network"
description = '''Has anyone come across people wreaking havoc with the WiFiKill Android app on your network and know how to find them using WireShark? I know it can&#x27;t be hard, but am not sure how to go about it. Thanks for any help!'''
date = "2015-03-16T15:03:00Z"
lastmod = "2015-03-24T12:21:00Z"
weight = 40623
keywords = [ "android", "wifikill", "app" ]
aliases = [ "/questions/40623" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WifiKill Android app kicking all users off wireless network](/questions/40623/wifikill-android-app-kicking-all-users-off-wireless-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40623-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40623-score" class="post-score" title="current number of votes">0</div><span id="post-40623-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Has anyone come across people wreaking havoc with the WiFiKill Android app on your network and know how to find them using WireShark? I know it can't be hard, but am not sure how to go about it. Thanks for any help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-wifikill" rel="tag" title="see questions tagged &#39;wifikill&#39;">wifikill</span> <span class="post-tag tag-link-app" rel="tag" title="see questions tagged &#39;app&#39;">app</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '15, 15:03</strong></p><img src="https://secure.gravatar.com/avatar/7737cd35ee7a3c442c5ea5d50f4765b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="moob&#39;s gravatar image" /><p><span>moob</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="moob has no accepted answers">0%</span></p></div></div><div id="comments-container-40623" class="comments-container"></div><div id="comment-tools-40623" class="comment-tools"></div><div class="clear"></div><div id="comment-40623-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40635"></span>

<div id="answer-container-40635" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40635-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40635-score" class="post-score" title="current number of votes">0</div><span id="post-40635-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sees like it's using ARP spoofing, which you can read about <a href="http://www.academia.edu/5648727/ARP_Spoofing-_Analysis_using_Wireshark_on_2_different_OS_LINUX_and_WINDOWS">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Mar '15, 06:39</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-40635" class="comments-container"><span id="40811"></span><div id="comment-40811" class="comment"><div id="post-40811-score" class="comment-score"></div><div class="comment-text"><p>Thanks! Read the article and still can't quite find them. Has anyone dealt with this before?</p></div><div id="comment-40811-info" class="comment-info"><span class="comment-age">(24 Mar '15, 12:21)</span> <span class="comment-user userinfo">moob</span></div></div></div><div id="comment-tools-40635" class="comment-tools"></div><div class="clear"></div><div id="comment-40635-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

