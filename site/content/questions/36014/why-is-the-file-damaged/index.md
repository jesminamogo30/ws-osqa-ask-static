+++
type = "question"
title = "Why is the file damaged?"
description = '''I downloaded this and it says the file is damaged and won&#x27;t run.'''
date = "2014-09-04T21:53:00Z"
lastmod = "2014-09-05T01:15:00Z"
weight = 36014
keywords = [ "damaged", "file" ]
aliases = [ "/questions/36014" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why is the file damaged?](/questions/36014/why-is-the-file-damaged)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36014-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36014-score" class="post-score" title="current number of votes">0</div><span id="post-36014-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded this and it says the file is damaged and won't run.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-damaged" rel="tag" title="see questions tagged &#39;damaged&#39;">damaged</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Sep '14, 21:53</strong></p><img src="https://secure.gravatar.com/avatar/f11657f5f13e92c9bdb6b7ea5bc71a8e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="heartunes&#39;s gravatar image" /><p><span>heartunes</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="heartunes has no accepted answers">0%</span></p></div></div><div id="comments-container-36014" class="comments-container"><span id="36017"></span><div id="comment-36017" class="comment"><div id="post-36017-score" class="comment-score"></div><div class="comment-text"><p>I got my computer certificate in 1986 on Apple IIe and IIc; as well as IBM. So Windows 8.1 Pro is more than I can understand from ignorance. Maybe the connection to the WiFi here is not so good &amp; it corrupted or damaged the file, I don't know. I just want youtube videos to buffer faster &amp; will download anything I think might help. I use Moxilla Firefox as a browser. I don't know what this program does, but someone on youtube said to come here as it would give me a better WiFi connection or learn how to connect to any WiFi or something like that. Forgive me for not knowing more. Ask me about medications, and I can tell you that being a nurse of 30 years. lol Thanks for help, if anyone gives me any. I shall try to download this again &amp; see what happens. ;)</p></div><div id="comment-36017-info" class="comment-info"><span class="comment-age">(05 Sep '14, 00:02)</span> <span class="comment-user userinfo">heartunes</span></div></div></div><div id="comment-tools-36014" class="comment-tools"></div><div class="clear"></div><div id="comment-36014-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36019"></span>

<div id="answer-container-36019" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36019-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36019-score" class="post-score" title="current number of votes">0</div><span id="post-36019-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately Wireshark is unlikely to help you, as it is a tool for detailed examination of network traffic and requires technical knowledge to interpret the display. By itself, Wireshark won't fix anything. Think of it as a Cat scan machine in your field.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Sep '14, 01:15</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-36019" class="comments-container"></div><div id="comment-tools-36019" class="comment-tools"></div><div class="clear"></div><div id="comment-36019-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

