+++
type = "question"
title = "Not able to install wireshark on windows 7"
description = '''I am trying to install wireshark on windows 7 buts its giving message &quot; A previous version of winpcap is detected on this system and cannot be removed because it is in use by another application. Please close all winpcap related applicaton and run the installer&quot; But I dont know about winpcap related...'''
date = "2014-09-07T00:58:00Z"
lastmod = "2014-09-07T03:50:00Z"
weight = 36050
keywords = [ "winpcap" ]
aliases = [ "/questions/36050" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Not able to install wireshark on windows 7](/questions/36050/not-able-to-install-wireshark-on-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36050-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36050-score" class="post-score" title="current number of votes">0</div><span id="post-36050-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to install wireshark on windows 7 buts its giving message " A previous version of winpcap is detected on this system and cannot be removed because it is in use by another application. Please close all winpcap related applicaton and run the installer"</p><p>But I dont know about winpcap related applications</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Sep '14, 00:58</strong></p><img src="https://secure.gravatar.com/avatar/f1e450e63272ccd1343c6315746854b7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vartika%20Agrawal&#39;s gravatar image" /><p><span>Vartika Agrawal</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vartika Agrawal has no accepted answers">0%</span></p></div></div><div id="comments-container-36050" class="comments-container"></div><div id="comment-tools-36050" class="comment-tools"></div><div class="clear"></div><div id="comment-36050-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36053"></span>

<div id="answer-container-36053" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36053-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36053-score" class="post-score" title="current number of votes">0</div><span id="post-36053-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try running the installer as administrator.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Sep '14, 03:50</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-36053" class="comments-container"></div><div id="comment-tools-36053" class="comment-tools"></div><div class="clear"></div><div id="comment-36053-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

