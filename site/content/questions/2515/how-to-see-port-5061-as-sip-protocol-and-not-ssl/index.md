+++
type = "question"
title = "How to see port 5061 as SIP protocol and not SSL"
description = '''Hello, i want to see transport being sent to/from port 5061 as SIP (when i filter), and not as SSL (in order to simplfy view, i don&#x27;t want to start searching aoo TCP packets) Is there a way? thanks'''
date = "2011-02-23T05:06:00Z"
lastmod = "2011-02-23T07:34:00Z"
weight = 2515
keywords = [ "sip", "5061", "tcp" ]
aliases = [ "/questions/2515" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to see port 5061 as SIP protocol and not SSL](/questions/2515/how-to-see-port-5061-as-sip-protocol-and-not-ssl)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2515-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2515-score" class="post-score" title="current number of votes">0</div><span id="post-2515-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, i want to see transport being sent to/from port 5061 as SIP (when i filter), and not as SSL (in order to simplfy view, i don't want to start searching aoo TCP packets)</p><p>Is there a way? thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-5061" rel="tag" title="see questions tagged &#39;5061&#39;">5061</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '11, 05:06</strong></p><img src="https://secure.gravatar.com/avatar/386f90e90ff5551b1d9abcb771e89b94?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hk76&#39;s gravatar image" /><p><span>hk76</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hk76 has no accepted answers">0%</span></p></div></div><div id="comments-container-2515" class="comments-container"></div><div id="comment-tools-2515" class="comment-tools"></div><div class="clear"></div><div id="comment-2515-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2521"></span>

<div id="answer-container-2521" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2521-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2521-score" class="post-score" title="current number of votes">0</div><span id="post-2521-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you try using the popup menu on one of the packets and selecting "Decode As" -&gt; "Transport" -&gt; "SIP"? I have no trace to verify this, but it should work.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Feb '11, 07:34</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-2521" class="comments-container"></div><div id="comment-tools-2521" class="comment-tools"></div><div class="clear"></div><div id="comment-2521-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

