+++
type = "question"
title = "[closed] View traffic in and out similtaniously"
description = '''If a machine has 2 network adapters and an ethernet frame leaves on one, addressed for the other. Is it possible to captur both instances of the frame in one instance of wireshark? the default behaviour of the psudo interface capturing both interfaces seems only to be to record one instance of the f...'''
date = "2012-10-10T23:58:00Z"
lastmod = "2012-10-10T23:58:00Z"
weight = 14912
keywords = [ "multiple_interfaces" ]
aliases = [ "/questions/14912" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] View traffic in and out similtaniously](/questions/14912/view-traffic-in-and-out-similtaniously)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14912-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14912-score" class="post-score" title="current number of votes">0</div><span id="post-14912-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If a machine has 2 network adapters and an ethernet frame leaves on one, addressed for the other. Is it possible to captur both instances of the frame in one instance of wireshark? the default behaviour of the psudo interface capturing both interfaces seems only to be to record one instance of the frome?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multiple_interfaces" rel="tag" title="see questions tagged &#39;multiple_interfaces&#39;">multiple_interfaces</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Oct '12, 23:58</strong></p><img src="https://secure.gravatar.com/avatar/60074dc48073fb4204f469af5ca2b439?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Urumiko&#39;s gravatar image" /><p><span>Urumiko</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Urumiko has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>11 Oct '12, 05:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-14912" class="comments-container"></div><div id="comment-tools-14912" class="comment-tools"></div><div class="clear"></div><div id="comment-14912-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question: http://ask.wireshark.org/questions/14817/monitoring-2-interfaces-on-the-same-machine" by Kurt Knochner 11 Oct '12, 05:22

</div>

</div>

</div>

