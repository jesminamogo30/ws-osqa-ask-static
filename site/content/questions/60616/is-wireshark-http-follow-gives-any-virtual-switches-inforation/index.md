+++
type = "question"
title = "is  Wireshark http follow gives any VIRTUAL SWITCHES INFORATION"
description = '''is wireshark http follow option gives any switches information using mininet ?? I want to find out which switches have maximum load in my custom topology while i ping between two host. '''
date = "2017-04-06T09:22:00Z"
lastmod = "2017-04-06T09:22:00Z"
weight = 60616
keywords = [ "switches", "information" ]
aliases = [ "/questions/60616" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [is Wireshark http follow gives any VIRTUAL SWITCHES INFORATION](/questions/60616/is-wireshark-http-follow-gives-any-virtual-switches-inforation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60616-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60616-score" class="post-score" title="current number of votes">0</div><span id="post-60616-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is wireshark http follow option gives any switches information using mininet ?? I want to find out which switches have maximum load in my custom topology while i ping between two host.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-switches" rel="tag" title="see questions tagged &#39;switches&#39;">switches</span> <span class="post-tag tag-link-information" rel="tag" title="see questions tagged &#39;information&#39;">information</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '17, 09:22</strong></p><img src="https://secure.gravatar.com/avatar/44748226de71145e36fde26bfdd367b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rainy&#39;s gravatar image" /><p><span>Rainy</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rainy has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Apr '17, 09:34</strong> </span></p></div></div><div id="comments-container-60616" class="comments-container"></div><div id="comment-tools-60616" class="comment-tools"></div><div class="clear"></div><div id="comment-60616-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

