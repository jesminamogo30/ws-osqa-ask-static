+++
type = "question"
title = "Time in capture and time in RTP Player different"
description = '''I performed a capture. The Time says 2016-09-30 for the packet I highlighted. I clicked Telephony &amp;gt; RTP &amp;gt; Stream Analysis. Then I clicked on Play Streams and clicked Time of Day. The time listed on the graph says 2018-06-15. Why are they different? Thanks'''
date = "2016-09-30T13:20:00Z"
lastmod = "2016-10-01T00:51:00Z"
weight = 56032
keywords = [ "rtp_player", "timestamp", "time", "rtp", "wireshark" ]
aliases = [ "/questions/56032" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Time in capture and time in RTP Player different](/questions/56032/time-in-capture-and-time-in-rtp-player-different)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56032-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56032-score" class="post-score" title="current number of votes">0</div><span id="post-56032-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I performed a capture. The Time says 2016-09-30 for the packet I highlighted. I clicked Telephony &gt; RTP &gt; Stream Analysis. Then I clicked on Play Streams and clicked Time of Day. The time listed on the graph says 2018-06-15. Why are they different?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp_player" rel="tag" title="see questions tagged &#39;rtp_player&#39;">rtp_player</span> <span class="post-tag tag-link-timestamp" rel="tag" title="see questions tagged &#39;timestamp&#39;">timestamp</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '16, 13:20</strong></p><img src="https://secure.gravatar.com/avatar/d3fb68c9f98f61bffa72da220b68a4c6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="carljong2015&#39;s gravatar image" /><p><span>carljong2015</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="carljong2015 has no accepted answers">0%</span></p></div></div><div id="comments-container-56032" class="comments-container"><span id="56039"></span><div id="comment-56039" class="comment"><div id="post-56039-score" class="comment-score"></div><div class="comment-text"><p>That sounds like a bug, yet I cannot reproduce it on my Wireshark 2.2.0 on 64-bit Windows 10. Can you provide details about your Wireshark version and operating system, and can you reproduce that bug on each start of Wireshark, opening the same capture file? If yes, can you publish the file? Does it happen with several VoIP captures or just with this single one?</p></div><div id="comment-56039-info" class="comment-info"><span class="comment-age">(01 Oct '16, 00:51)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-56032" class="comment-tools"></div><div class="clear"></div><div id="comment-56032-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

