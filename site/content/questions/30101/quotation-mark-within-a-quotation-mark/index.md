+++
type = "question"
title = "quotation mark within a quotation mark"
description = '''OK, so I know if I want to search for specific word I type: something contains &quot;wanted word&quot;.  But now I would like to find a word which is within quotations in a packet, but if I put double quotations like this &quot;&quot;wanted word&quot;&quot; the filter turns red as it is invalid command.  So is there a way to fin...'''
date = "2014-02-22T13:35:00Z"
lastmod = "2014-02-22T14:00:00Z"
weight = 30101
keywords = [ "quotations", "mark", "word", "find", "quotation" ]
aliases = [ "/questions/30101" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [quotation mark within a quotation mark](/questions/30101/quotation-mark-within-a-quotation-mark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30101-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30101-score" class="post-score" title="current number of votes">0</div><span id="post-30101-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>OK, so I know if I want to search for specific word I type: something contains "wanted word".</p><p>But now I would like to find a word which is within quotations in a packet, but if I put double quotations like this ""wanted word"" the filter turns red as it is invalid command.</p><p>So is there a way to find word that is actually within quotation marks? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-quotations" rel="tag" title="see questions tagged &#39;quotations&#39;">quotations</span> <span class="post-tag tag-link-mark" rel="tag" title="see questions tagged &#39;mark&#39;">mark</span> <span class="post-tag tag-link-word" rel="tag" title="see questions tagged &#39;word&#39;">word</span> <span class="post-tag tag-link-find" rel="tag" title="see questions tagged &#39;find&#39;">find</span> <span class="post-tag tag-link-quotation" rel="tag" title="see questions tagged &#39;quotation&#39;">quotation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '14, 13:35</strong></p><img src="https://secure.gravatar.com/avatar/412b10652e55b9c4d3cc5243b7b58d0f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="myrddin&#39;s gravatar image" /><p><span>myrddin</span><br />
<span class="score" title="11 reputation points">11</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="myrddin has no accepted answers">0%</span></p></div></div><div id="comments-container-30101" class="comments-container"></div><div id="comment-tools-30101" class="comment-tools"></div><div class="clear"></div><div id="comment-30101-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30103"></span>

<div id="answer-container-30103" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30103-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30103-score" class="post-score" title="current number of votes">2</div><span id="post-30103-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="myrddin has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Use the "matches" operator and escape the quotation marks with a backslash.</p><p>Example: frame matches "\"wanted word\""</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Feb '14, 14:00</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-30103" class="comments-container"></div><div id="comment-tools-30103" class="comment-tools"></div><div class="clear"></div><div id="comment-30103-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

