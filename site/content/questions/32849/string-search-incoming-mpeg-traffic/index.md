+++
type = "question"
title = "string search incoming MPEG traffic"
description = '''I am trying to parse the incoming traffic ( mpeg ) and look for a string. i can do this packet analysis using the MPEG display filters where i look for the IDR frame and the adaptation field data.  I need to look at this adaptation field and find a particular string in Real-time. Any help will be gr...'''
date = "2014-05-16T11:59:00Z"
lastmod = "2014-05-16T11:59:00Z"
weight = 32849
keywords = [ "udp", "packet-capture", "rtp", "mpegts", "mpeg_dump" ]
aliases = [ "/questions/32849" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [string search incoming MPEG traffic](/questions/32849/string-search-incoming-mpeg-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32849-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32849-score" class="post-score" title="current number of votes">0</div><span id="post-32849-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to parse the incoming traffic ( mpeg ) and look for a string. i can do this packet analysis using the MPEG display filters where i look for the IDR frame and the adaptation field data.</p><p>I need to look at this adaptation field and find a particular string in Real-time.</p><p>Any help will be greatly appreciated.</p><p>Thanks nr</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-packet-capture" rel="tag" title="see questions tagged &#39;packet-capture&#39;">packet-capture</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-mpegts" rel="tag" title="see questions tagged &#39;mpegts&#39;">mpegts</span> <span class="post-tag tag-link-mpeg_dump" rel="tag" title="see questions tagged &#39;mpeg_dump&#39;">mpeg_dump</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 May '14, 11:59</strong></p><img src="https://secure.gravatar.com/avatar/77b560ffab418806c2ede8a745ceb8e3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nehaR&#39;s gravatar image" /><p><span>nehaR</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nehaR has no accepted answers">0%</span></p></div></div><div id="comments-container-32849" class="comments-container"></div><div id="comment-tools-32849" class="comment-tools"></div><div class="clear"></div><div id="comment-32849-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

