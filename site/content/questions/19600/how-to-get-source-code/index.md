+++
type = "question"
title = "how to get source code"
description = '''I work in a classified computer area and we cannot install software that is not on the list of authorized software. If I can get source code then I can build and run the code. I tried this page: http://www.wireshark.org/download.html# but nothing there will download the source code. How do I get a c...'''
date = "2013-03-17T19:23:00Z"
lastmod = "2013-03-18T03:42:00Z"
weight = 19600
keywords = [ "download", "source", "code" ]
aliases = [ "/questions/19600" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [how to get source code](/questions/19600/how-to-get-source-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19600-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19600-score" class="post-score" title="current number of votes">0</div><span id="post-19600-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I work in a classified computer area and we cannot install software that is not on the list of authorized software. If I can get source code then I can build and run the code. I tried this page: <a href="http://www.wireshark.org/download.html#">http://www.wireshark.org/download.html#</a> but nothing there will download the source code. How do I get a copy of the source code. Windows 7. Visual Studio if possible. Thank you, Bryan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-source" rel="tag" title="see questions tagged &#39;source&#39;">source</span> <span class="post-tag tag-link-code" rel="tag" title="see questions tagged &#39;code&#39;">code</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Mar '13, 19:23</strong></p><img src="https://secure.gravatar.com/avatar/8d134b48e679cc0bc391b56ebcbfa1f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bkelly13&#39;s gravatar image" /><p><span>bkelly13</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bkelly13 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Mar '13, 11:19</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-19600" class="comments-container"><span id="19614"></span><div id="comment-19614" class="comment"><div id="post-19614-score" class="comment-score"></div><div class="comment-text"><p>Please note that to build Wireshark on Windows you need to download and install lots of other bits of software, e.g. Cygwin, Python and download all the 3rd party libraries used by Wireshark.</p></div><div id="comment-19614-info" class="comment-info"><span class="comment-age">(18 Mar '13, 03:42)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-19600" class="comment-tools"></div><div class="clear"></div><div id="comment-19600-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="19601"></span>

<div id="answer-container-19601" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19601-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19601-score" class="post-score" title="current number of votes">0</div><span id="post-19601-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is a link on that page, leading to <a href="http://www.wireshark.org/develop.html">http://www.wireshark.org/develop.html</a>. Did you try that one?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Mar '13, 20:33</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-19601" class="comments-container"></div><div id="comment-tools-19601" class="comment-tools"></div><div class="clear"></div><div id="comment-19601-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="19604"></span>

<div id="answer-container-19604" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19604-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19604-score" class="post-score" title="current number of votes">0</div><span id="post-19604-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hmm, the page you refer to has a <a href="http://wiresharkdownloads.riverbed.com/wireshark/src/wireshark-1.8.6.tar.bz2">download link</a> on it.</p><p>Or you can get it from <a href="http://sourceforge.net/projects/wireshark/files/src/">SourceForge</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Mar '13, 23:24</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-19604" class="comments-container"></div><div id="comment-tools-19604" class="comment-tools"></div><div class="clear"></div><div id="comment-19604-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

