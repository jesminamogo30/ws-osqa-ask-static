+++
type = "question"
title = "Once captured ble data ,can I automate wireshark tool ?"
description = '''My question is related to wireshark tool , I knew once captured data I can analyzed data &#x27;manually&#x27; using wireshark tool . But my requirement is that after capturing data I can&#x27;t use wireshark GUI tool , can I do automate wireshark tool to run and get the specific data ? '''
date = "2014-12-29T12:26:00Z"
lastmod = "2014-12-30T04:01:00Z"
weight = 38771
keywords = [ "automate", "wireshark" ]
aliases = [ "/questions/38771" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Once captured ble data ,can I automate wireshark tool ?](/questions/38771/once-captured-ble-data-can-i-automate-wireshark-tool)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38771-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38771-score" class="post-score" title="current number of votes">0</div><span id="post-38771-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My question is related to wireshark tool , I knew once captured data I can analyzed data 'manually' using wireshark tool . But my requirement is that after capturing data I can't use wireshark GUI tool , can I do automate wireshark tool to run and get the specific data ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-automate" rel="tag" title="see questions tagged &#39;automate&#39;">automate</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Dec '14, 12:26</strong></p><img src="https://secure.gravatar.com/avatar/fac02024ccd7c3f5077fd66d73d7d498?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Naren&#39;s gravatar image" /><p><span>Naren</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Naren has no accepted answers">0%</span></p></div></div><div id="comments-container-38771" class="comments-container"><span id="38785"></span><div id="comment-38785" class="comment"><div id="post-38785-score" class="comment-score"></div><div class="comment-text"><p>it's probably possible, but you need to tell us <strong>which data</strong> you are interested in. Please add as much information as possible, maybe the manual steps you would perform to get the same result in Wireshark.</p></div><div id="comment-38785-info" class="comment-info"><span class="comment-age">(30 Dec '14, 04:01)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-38771" class="comment-tools"></div><div class="clear"></div><div id="comment-38771-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38782"></span>

<div id="answer-container-38782" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38782-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38782-score" class="post-score" title="current number of votes">0</div><span id="post-38782-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look at <a href="https://www.wireshark.org/docs/man-pages/">the command line tools</a> related to wireshark. These are very useful for task automation.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Dec '14, 03:55</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-38782" class="comments-container"></div><div id="comment-tools-38782" class="comment-tools"></div><div class="clear"></div><div id="comment-38782-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

