+++
type = "question"
title = "How do you capture RTP packets?"
description = '''how to see the higher-level protocol carried by a packet'''
date = "2013-12-14T04:24:00Z"
lastmod = "2013-12-15T20:57:00Z"
weight = 28104
keywords = [ "livecapturetcp" ]
aliases = [ "/questions/28104" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do you capture RTP packets?](/questions/28104/how-do-you-capture-rtp-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28104-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28104-score" class="post-score" title="current number of votes">0</div><span id="post-28104-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how to see the higher-level protocol carried by a packet</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-livecapturetcp" rel="tag" title="see questions tagged &#39;livecapturetcp&#39;">livecapturetcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Dec '13, 04:24</strong></p><img src="https://secure.gravatar.com/avatar/836c1e7747be8c7f309597f67e72bc30?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mauricio&#39;s gravatar image" /><p><span>Mauricio</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mauricio has no accepted answers">0%</span></p></div></div><div id="comments-container-28104" class="comments-container"><span id="28144"></span><div id="comment-28144" class="comment"><div id="post-28144-score" class="comment-score"></div><div class="comment-text"><p>Could you elaborate on exactly what you're trying to see or do? Capturing RTP packets is the same as capturing any other. When you ask how to see higher level protocols, in general wireshark will dissect/display every layer it knows how to. What specifically are you trying to accomplish?</p></div><div id="comment-28144-info" class="comment-info"><span class="comment-age">(15 Dec '13, 20:57)</span> <span class="comment-user userinfo">Quadratic</span></div></div></div><div id="comment-tools-28104" class="comment-tools"></div><div class="clear"></div><div id="comment-28104-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

