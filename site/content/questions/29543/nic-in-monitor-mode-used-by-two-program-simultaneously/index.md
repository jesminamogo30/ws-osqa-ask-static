+++
type = "question"
title = "NIC in Monitor mode used by two program simultaneously"
description = '''Presently I am designing sniifer that is uses NIC in monitor mode but side by side I am also using wireshark ( for which also NIC in monitor mode ). So both this program using monitor mode simultaneously. SO I want to know  Does it possible that two program using NIC in monitor mode simultaneosly ? ...'''
date = "2014-02-07T19:39:00Z"
lastmod = "2014-02-08T03:39:00Z"
weight = 29543
keywords = [ "sniffing", "intrusion" ]
aliases = [ "/questions/29543" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [NIC in Monitor mode used by two program simultaneously](/questions/29543/nic-in-monitor-mode-used-by-two-program-simultaneously)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29543-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29543-score" class="post-score" title="current number of votes">0</div><span id="post-29543-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Presently I am designing sniifer that is uses NIC in monitor mode but side by side I am also using wireshark ( for which also NIC in monitor mode ). So both this program using monitor mode simultaneously. SO I want to know Does it possible that two program using NIC in monitor mode simultaneosly ? Does it affect on packet capturing?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-intrusion" rel="tag" title="see questions tagged &#39;intrusion&#39;">intrusion</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Feb '14, 19:39</strong></p><img src="https://secure.gravatar.com/avatar/4f2e12b298828a7bdd200478480606da?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="WIDS&#39;s gravatar image" /><p><span>WIDS</span><br />
<span class="score" title="25 reputation points">25</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="WIDS has no accepted answers">0%</span></p></div></div><div id="comments-container-29543" class="comments-container"></div><div id="comment-tools-29543" class="comment-tools"></div><div class="clear"></div><div id="comment-29543-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29549"></span>

<div id="answer-container-29549" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29549-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29549-score" class="post-score" title="current number of votes">0</div><span id="post-29549-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="WIDS has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It should be possible. I know for sure that it is possible to run multiple dumpcap processes on a single network card, e.g. for capturing data from the same link but with different capture filters.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Feb '14, 03:39</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-29549" class="comments-container"></div><div id="comment-tools-29549" class="comment-tools"></div><div class="clear"></div><div id="comment-29549-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

