+++
type = "question"
title = "Adding the Http Referer to the packet summary line"
description = '''Hey folks,  for a project I want to add the Http Referer to the packet summary line and export as plaine text. Is there any possibility to do that? I can only find the &quot;Info Column&quot; in the column prefences and that&#x27;s tottaly messing up my table. thanks a lot Max'''
date = "2014-02-23T10:43:00Z"
lastmod = "2014-02-23T10:55:00Z"
weight = 30108
keywords = [ "referer", "http", "columns", "plain-text" ]
aliases = [ "/questions/30108" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Adding the Http Referer to the packet summary line](/questions/30108/adding-the-http-referer-to-the-packet-summary-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30108-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30108-score" class="post-score" title="current number of votes">0</div><span id="post-30108-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey folks, for a project I want to add the Http Referer to the packet summary line and export as plaine text. Is there any possibility to do that? I can only find the "Info Column" in the column prefences and that's tottaly messing up my table. thanks a lot Max</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-referer" rel="tag" title="see questions tagged &#39;referer&#39;">referer</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span> <span class="post-tag tag-link-plain-text" rel="tag" title="see questions tagged &#39;plain-text&#39;">plain-text</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '14, 10:43</strong></p><img src="https://secure.gravatar.com/avatar/1c6ad8a0200bb9f7acef74d7aacc7691?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Maxi%20Popaxi&#39;s gravatar image" /><p><span>Maxi Popaxi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Maxi Popaxi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Feb '14, 11:34</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-30108" class="comments-container"></div><div id="comment-tools-30108" class="comment-tools"></div><div class="clear"></div><div id="comment-30108-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30109"></span>

<div id="answer-container-30109" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30109-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30109-score" class="post-score" title="current number of votes">0</div><span id="post-30109-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>already found it myself just had to use the custom field type...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Feb '14, 10:55</strong></p><img src="https://secure.gravatar.com/avatar/1c6ad8a0200bb9f7acef74d7aacc7691?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Maxi%20Popaxi&#39;s gravatar image" /><p><span>Maxi Popaxi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Maxi Popaxi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Feb '14, 10:55</strong> </span></p></div></div><div id="comments-container-30109" class="comments-container"></div><div id="comment-tools-30109" class="comment-tools"></div><div class="clear"></div><div id="comment-30109-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

