+++
type = "question"
title = "Handshake ... TCP-Options ... connecting to printer"
description = '''Like I have been the TCP options need to be given and replied during the handshake. What I see is that the Print-Server is requesting this features, see screendump:  While the printer is replying with a limited answer, see screendump:  What I would like to know, if printers can support SACK and Wind...'''
date = "2017-10-17T10:05:00Z"
lastmod = "2017-10-20T08:36:00Z"
weight = 63974
keywords = [ "handshake", "printer", "tcp" ]
aliases = [ "/questions/63974" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Handshake ... TCP-Options ... connecting to printer](/questions/63974/handshake-tcp-options-connecting-to-printer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63974-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63974-score" class="post-score" title="current number of votes">0</div><span id="post-63974-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Like I have been the TCP options need to be given and replied during the handshake. What I see is that the Print-Server is requesting this features, see screendump: <img src="https://osqa-ask.wireshark.org/upfiles/Selection_029.png" alt="alt Print-Server" /></p><p>While the printer is replying with a limited answer, see screendump: <img src="https://osqa-ask.wireshark.org/upfiles/Selection_028_ZFAVtOG.png" alt="alt Printer" /></p><p>What I would like to know, if printers can support SACK and Window-Scale. Have anyone seen in a reply from a printer, that the printer will do also SACK and Window-Scale, TCP-Options. In the tech-documentation of the printer, is nothing about TCP feature's. So unknown if the printer will do it. But will good to know if someone has seen printers support it.</p><p>Printer: Zebra ZXP Series 7</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-handshake" rel="tag" title="see questions tagged &#39;handshake&#39;">handshake</span> <span class="post-tag tag-link-printer" rel="tag" title="see questions tagged &#39;printer&#39;">printer</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '17, 10:05</strong></p><img src="https://secure.gravatar.com/avatar/b91a47080428356f552ac248d01b1bc1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bennekom&#39;s gravatar image" /><p><span>Bennekom</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bennekom has no accepted answers">0%</span></p></img></div></div><div id="comments-container-63974" class="comments-container"></div><div id="comment-tools-63974" class="comment-tools"></div><div class="clear"></div><div id="comment-63974-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="63980"></span>

<div id="answer-container-63980" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63980-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63980-score" class="post-score" title="current number of votes">0</div><span id="post-63980-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/p3005_7A984NZ.JPG" alt="alt text" /></p><p>Hewlett Packard LaserJet P3005.</p><p>WS is supported but equals 1. SACK isn't supported.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Oct '17, 12:32</strong></p><img src="https://secure.gravatar.com/avatar/1e22670f8d643ca08d658b80a6782932?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Packet_vlad&#39;s gravatar image" /><p><span>Packet_vlad</span><br />
<span class="score" title="436 reputation points">436</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Packet_vlad has 5 accepted answers">20%</span></p></img></div></div><div id="comments-container-63980" class="comments-container"></div><div id="comment-tools-63980" class="comment-tools"></div><div class="clear"></div><div id="comment-63980-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="64000"></span>

<div id="answer-container-64000" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64000-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64000-score" class="post-score" title="current number of votes">0</div><span id="post-64000-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In my experience, most Enterprise/Business class printers have these capabilities (Xerox, HP, etc). For consumer grade, I have seen HP printers support SACK as well as Window Scaling. I have found other cheaper consumer grade printers to have fewer features and no interface to configure TCP. For your particular printer, you will need to spend the time working with your vendor to determine if it is possible to get a firmware upgrade to support this.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Oct '17, 07:34</strong></p><img src="https://secure.gravatar.com/avatar/3f2f87a6a68e4c51c3851c20b6c56a1a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="CMH_Tim&#39;s gravatar image" /><p><span>CMH_Tim</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="CMH_Tim has no accepted answers">0%</span></p></img></div></div><div id="comments-container-64000" class="comments-container"><span id="64053"></span><div id="comment-64053" class="comment"><div id="post-64053-score" class="comment-score"></div><div class="comment-text"><p>Thanks</p><p>In the mean time have I found some printer that do support:</p><p>SACK Window Scale Timestamps</p><p>That is a recent model Enterprise/Business class network printer.</p></div><div id="comment-64053-info" class="comment-info"><span class="comment-age">(20 Oct '17, 08:36)</span> <span class="comment-user userinfo">Bennekom</span></div></div></div><div id="comment-tools-64000" class="comment-tools"></div><div class="clear"></div><div id="comment-64000-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

