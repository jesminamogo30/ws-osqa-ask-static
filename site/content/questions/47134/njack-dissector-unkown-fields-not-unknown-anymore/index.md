+++
type = "question"
title = "NJACK dissector unkown fields not unknown anymore"
description = '''What would be the best way to submit an enhancement request for the NJAC dissector ?  I got my hands on some 3com NJ200 which the NJACK dissector is the dissector for and I can execute some of the commands from the remote configuration manager to determine what would be many of the unknown fields. W...'''
date = "2015-11-01T16:41:00Z"
lastmod = "2015-11-02T14:40:00Z"
weight = 47134
keywords = [ "njack" ]
aliases = [ "/questions/47134" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [NJACK dissector unkown fields not unknown anymore](/questions/47134/njack-dissector-unkown-fields-not-unknown-anymore)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47134-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47134-score" class="post-score" title="current number of votes">0</div><span id="post-47134-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What would be the best way to submit an enhancement request for the NJAC dissector ?</p><p>I got my hands on some 3com NJ200 which the NJACK dissector is the dissector for and I can execute some of the commands from the remote configuration manager to determine what would be many of the unknown fields.</p><p>Who should I contact for that ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-njack" rel="tag" title="see questions tagged &#39;njack&#39;">njack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Nov '15, 16:41</strong></p><img src="https://secure.gravatar.com/avatar/2eeeee003feb05238840aaa2bbccd657?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Richard%20Lavoie&#39;s gravatar image" /><p><span>Richard Lavoie</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Richard Lavoie has no accepted answers">0%</span></p></div></div><div id="comments-container-47134" class="comments-container"></div><div id="comment-tools-47134" class="comment-tools"></div><div class="clear"></div><div id="comment-47134-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47172"></span>

<div id="answer-container-47172" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47172-score" class="post-score" title="current number of votes">0</div><span id="post-47172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you want to submit an enhancement request to add the new fields for somebody else to implement, submit it on <a href="http://bugs.wireshark.org">the Wireshark Bugzilla</a>. Please attach a capture with NJACK messages containing the unknown fields.</p><p>If you want to implement it yourself, contributions to Wireshark should be submitted as patches using Gerrit, as described on <a href="https://wiki.wireshark.org/Development/SubmittingPatches">the SubmittingPatches page on the Wireshark Wiki</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Nov '15, 14:40</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Nov '15, 15:59</strong> </span></p></div></div><div id="comments-container-47172" class="comments-container"></div><div id="comment-tools-47172" class="comment-tools"></div><div class="clear"></div><div id="comment-47172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

