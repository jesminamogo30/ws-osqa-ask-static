+++
type = "question"
title = "How to Analyse the Wireshark report"
description = '''Hi I have wireshark logs captured during the application inaccesabilty i found that problem in tier 3 i.e is network layer , but i don know how to analyse the same even i have opnet also to analyze the same but am not able to analyze report can anybody help me on this.'''
date = "2014-03-11T23:06:00Z"
lastmod = "2014-03-11T23:06:00Z"
weight = 30708
keywords = [ "wisk" ]
aliases = [ "/questions/30708" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to Analyse the Wireshark report](/questions/30708/how-to-analyse-the-wireshark-report)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30708-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30708-score" class="post-score" title="current number of votes">0</div><span id="post-30708-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I have wireshark logs captured during the application inaccesabilty i found that problem in tier 3 i.e is network layer , but i don know how to analyse the same even i have opnet also to analyze the same but am not able to analyze report can anybody help me on this.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wisk" rel="tag" title="see questions tagged &#39;wisk&#39;">wisk</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Mar '14, 23:06</strong></p><img src="https://secure.gravatar.com/avatar/e61336bfc5ef4fe8651c0a251d622a51?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vasanth481254&#39;s gravatar image" /><p><span>vasanth481254</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vasanth481254 has no accepted answers">0%</span></p></div></div><div id="comments-container-30708" class="comments-container"></div><div id="comment-tools-30708" class="comment-tools"></div><div class="clear"></div><div id="comment-30708-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

