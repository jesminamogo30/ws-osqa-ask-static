+++
type = "question"
title = "How  does one  enable Decrypt 802.11?"
description = '''Monitor mode (type 802.11 plus radiotap header) Wireshark edit&amp;gt;preferences no option to Decrypt802.11  Object: attempting to use monitor mode to decode my own LAN traffic as promiscuous mode does not show all my wifi router LAN traffic, that I am interested in (as pointed out in the Wireshark web...'''
date = "2013-05-26T06:49:00Z"
lastmod = "2013-05-26T06:49:00Z"
weight = 21478
keywords = [ "802.11", "decrypt" ]
aliases = [ "/questions/21478" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How does one enable Decrypt 802.11?](/questions/21478/how-does-one-enable-decrypt-80211)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21478-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21478-score" class="post-score" title="current number of votes">0</div><span id="post-21478-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Monitor mode (type 802.11 plus radiotap header) Wireshark edit&gt;preferences no option to Decrypt802.11</p><p>Object: attempting to use monitor mode to decode my own LAN traffic as promiscuous mode does not show all my wifi router LAN traffic, that I am interested in (as pointed out in the Wireshark web site).</p><p>Running wireshark version 1.6.7 (very recently downloaded from Ubuntu software centre) on Ubuntu 12.04 (Dell Inspiron 510m notebook). I am using ‘sudo airmon-ng start wlan0’ (or wlan1 for external USB AWUS036NEH) to enable monitor mode on the internal generic Broadcom b43 wifi board. In wireshark on the Capture page,’ link layer header type 802.11 plus radiotap header’ is greyed out. However it shows all the ‘Broadcast’ data streaming from all the wifi stations in the area. However under Edit&gt;Preferences only shows ‘Display hidden protocols items’ with a box to enable. If enabled nothing obvious happens.</p><p>I had expected to see the choices displayed as per Preferences on <a href="http://wiki.wireshark.org/Wi-Fi">http://wiki.wireshark.org/Wi-Fi</a> And the decrypt option as per <a href="http://wiki.wireshark.org/HowToDecrypt802.11">http://wiki.wireshark.org/HowToDecrypt802.11</a> in order to decrypt my own data.</p><p>Note on my Windows 7 System PC, which has no monitor mode enabled, Wireshark version 1.6.6 , wrt Edit&gt; Preferences behaves as above i.e. ‘Display hidden protocols items’. Thus it looks as if I am only partially in monitor mode on the Ubuntu (Note same with sudo airmon-ng start mon0) Any suggestions please?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 May '13, 06:49</strong></p><img src="https://secure.gravatar.com/avatar/3465960e35877a57d79a82784bea702d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Perplexed&#39;s gravatar image" /><p><span>Perplexed</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Perplexed has no accepted answers">0%</span></p></div></div><div id="comments-container-21478" class="comments-container"></div><div id="comment-tools-21478" class="comment-tools"></div><div class="clear"></div><div id="comment-21478-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

