+++
type = "question"
title = "Who can help me analyze?"
description = '''Hello, members of my family have tried to kill me! Some of them are in a criminal organization! I have recorded many activities with Wireshark! I imagine when evaluated, stupid! Who can help, it is very much? Lots of chat and broadcast was recorded! Please reply via e-mail, Besides e-mail I find no ...'''
date = "2014-09-03T02:37:00Z"
lastmod = "2014-09-03T05:03:00Z"
weight = 35950
keywords = [ "decode", "traffic", "analysis", "wireshark" ]
aliases = [ "/questions/35950" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Who can help me analyze?](/questions/35950/who-can-help-me-analyze)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35950-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35950-score" class="post-score" title="current number of votes">0</div><span id="post-35950-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, members of my family have tried to kill me! Some of them are in a criminal organization! I have recorded many activities with Wireshark! I imagine when evaluated, stupid! Who can help, it is very much? Lots of chat and broadcast was recorded! Please reply via e-mail, Besides e-mail I find no other way to send the data to a helper! Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '14, 02:37</strong></p><img src="https://secure.gravatar.com/avatar/8cf4bce05e209332421498cf5adf0026?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pvandermeu&#39;s gravatar image" /><p><span>pvandermeu</span><br />
<span class="score" title="8 reputation points">8</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pvandermeu has no accepted answers">0%</span></p></div></div><div id="comments-container-35950" class="comments-container"></div><div id="comment-tools-35950" class="comment-tools"></div><div class="clear"></div><div id="comment-35950-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35951"></span>

<div id="answer-container-35951" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35951-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35951-score" class="post-score" title="current number of votes">1</div><span id="post-35951-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Simple answer: contact the local police authorities which may or may not have access to a cyber investigation unit, but they can most certainly get access to someone who can help (if this is not just some joke).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Sep '14, 02:41</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-35951" class="comments-container"><span id="35957"></span><div id="comment-35957" class="comment"><div id="post-35957-score" class="comment-score"></div><div class="comment-text"><p>Thank you! But you must know that I live in Egypt. The police here do not even know What is Wireshark!But i see, you are a expert. I hope it's not a problem if I send my e-mail address here! I ask the admins for forgiveness! <span class="__cf_email__" data-cfemail="87d7f1e8e9e3e2f5eae2f2c7fee6efe8e8a9e4e8ea">[email protected]</span></p></div><div id="comment-35957-info" class="comment-info"><span class="comment-age">(03 Sep '14, 05:03)</span> <span class="comment-user userinfo">pvandermeu</span></div></div></div><div id="comment-tools-35951" class="comment-tools"></div><div class="clear"></div><div id="comment-35951-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

