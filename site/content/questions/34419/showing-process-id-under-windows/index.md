+++
type = "question"
title = "Showing Process ID Under Windows"
description = '''Wireshark browser would be three times more useful if I had the option to record the process ID associated with each specific network request. Isolating requests to specific system services or drivers or user applications has extremely high value. Is there any way to get this information? Maybe some...'''
date = "2014-07-04T12:17:00Z"
lastmod = "2014-07-04T12:41:00Z"
weight = 34419
keywords = [ "windows", "process" ]
aliases = [ "/questions/34419" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Showing Process ID Under Windows](/questions/34419/showing-process-id-under-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34419-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34419-score" class="post-score" title="current number of votes">0</div><span id="post-34419-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark browser would be three times more useful if I had the option to record the process ID associated with each specific network request. Isolating requests to specific system services or drivers or user applications has extremely high value.</p><p>Is there any way to get this information? Maybe someone has a commercial add-in to Wireshark? Is there a competitive sniffer that offers this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-process" rel="tag" title="see questions tagged &#39;process&#39;">process</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jul '14, 12:17</strong></p><img src="https://secure.gravatar.com/avatar/b4b0545e23e75c3b64ea1c8f50c6a16b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="westes&#39;s gravatar image" /><p><span>westes</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="westes has no accepted answers">0%</span></p></div></div><div id="comments-container-34419" class="comments-container"></div><div id="comment-tools-34419" class="comment-tools"></div><div class="clear"></div><div id="comment-34419-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34422"></span>

<div id="answer-container-34422" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34422-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34422-score" class="post-score" title="current number of votes">1</div><span id="post-34422-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is there a competitive sniffer that offers this?</p></blockquote><p><a href="http://www.microsoft.com/en-us/download/details.aspx?id=4865">Microsoft Network Monitor</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jul '14, 12:41</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-34422" class="comments-container"></div><div id="comment-tools-34422" class="comment-tools"></div><div class="clear"></div><div id="comment-34422-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

