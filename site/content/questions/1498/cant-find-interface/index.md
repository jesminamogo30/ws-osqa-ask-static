+++
type = "question"
title = "can&#x27;t find interface"
description = '''window version : Windows 7 Professional K wireshark version : 1.4.2 mobile : samsung android phone when i use mobie dun, i want check my packet. but i can&#x27;t find interface about dun. that time, mobile has ip and dun connect success. how can i find interface in wireshark. thank you.'''
date = "2010-12-28T05:10:00Z"
lastmod = "2010-12-28T05:10:00Z"
weight = 1498
keywords = [ "interface" ]
aliases = [ "/questions/1498" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [can't find interface](/questions/1498/cant-find-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1498-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1498-score" class="post-score" title="current number of votes">0</div><span id="post-1498-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>window version : Windows 7 Professional K wireshark version : 1.4.2 mobile : samsung android phone</p><p>when i use mobie dun, i want check my packet. but i can't find interface about dun. that time, mobile has ip and dun connect success. how can i find interface in wireshark.</p><p>thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Dec '10, 05:10</strong></p><img src="https://secure.gravatar.com/avatar/d7eac629e36e4b13fbdf0d135ff84edb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nikol482&#39;s gravatar image" /><p><span>nikol482</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nikol482 has no accepted answers">0%</span></p></div></div><div id="comments-container-1498" class="comments-container"></div><div id="comment-tools-1498" class="comment-tools"></div><div class="clear"></div><div id="comment-1498-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

