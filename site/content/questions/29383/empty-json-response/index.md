+++
type = "question"
title = "empty json response?"
description = '''I am learning wireshark while trying to learn how ajax works... using 500px as example, and picking a random portfolio, http://500px.com/Pop315, I see this traffic...  browser requests page from 209.15.228.200, gets the page, which essentially contains just javascripts and div place holders. browser...'''
date = "2014-02-02T19:17:00Z"
lastmod = "2014-02-02T19:17:00Z"
weight = 29383
keywords = [ "500px", "json" ]
aliases = [ "/questions/29383" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [empty json response?](/questions/29383/empty-json-response)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29383-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29383-score" class="post-score" title="current number of votes">0</div><span id="post-29383-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am learning wireshark while trying to learn how ajax works... using 500px as example, and picking a random portfolio, <a href="http://500px.com/Pop315,">http://500px.com/Pop315,</a> I see this traffic...</p><ol><li>browser requests page from 209.15.228.200, gets the page, which essentially contains just javascripts and div place holders.</li><li>browser requests external javascript and an image from 72.21.91.19, gets both</li><li>browser sends a "GET /event?a=..." to 50.17.242.30</li><li>50.17.242.30 returns a json object consists of just "{}"???</li><li>browser talks other websites like goggle analytics...</li><li>now somehow, the browser knows all the thumbnails to get and asks 72.21.91.19 for all the tumbnails to display on the page.</li></ol><p>Looks to me, the browser javascript engine figured out what request to send to construct the page in step 3, then got that reply in step. However, the reply was just empty "{}", how can that be? If someone can run a quick test, I would really appreciate it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-500px" rel="tag" title="see questions tagged &#39;500px&#39;">500px</span> <span class="post-tag tag-link-json" rel="tag" title="see questions tagged &#39;json&#39;">json</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Feb '14, 19:17</strong></p><img src="https://secure.gravatar.com/avatar/874210770437fe869c01c135678178e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ctny&#39;s gravatar image" /><p><span>ctny</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ctny has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Feb '14, 19:19</strong> </span></p></div></div><div id="comments-container-29383" class="comments-container"></div><div id="comment-tools-29383" class="comment-tools"></div><div class="clear"></div><div id="comment-29383-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

