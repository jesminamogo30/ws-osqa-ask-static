+++
type = "question"
title = "Why no contextual menus when &quot;right-clicking&quot;?"
description = '''Problem: I do not get any contextual menus when I ctrl-click (the normal equivalent of &quot;right-click&quot;) on a field in the Wireshark GUI. Nothing happens: the behavior is the same whether I am ctrl-clicking in the Packet List pane or the Packet Details pane. Question: Is there a preference that I need ...'''
date = "2013-08-12T08:45:00Z"
lastmod = "2015-09-06T09:21:00Z"
weight = 23712
keywords = [ "xquartz", "macosx", "contextual-menu", "right-click" ]
aliases = [ "/questions/23712" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why no contextual menus when "right-clicking"?](/questions/23712/why-no-contextual-menus-when-right-clicking)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23712-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23712-score" class="post-score" title="current number of votes">1</div><span id="post-23712-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Problem: I do not get any contextual menus when I ctrl-click (the normal equivalent of "right-click") on a field in the Wireshark GUI. Nothing happens: the behavior is the same whether I am ctrl-clicking in the Packet List pane or the Packet Details pane. Question: Is there a preference that I need to set to get the correct behavior?</p><p>Wireshark runs under XQuartz 2.7.4 on my system.</p><p>This is the info from "About Wireshark" Version 1.10.1 (SVN Rev 50926 from /trunk-1.10)</p><p>Copyright 1998-2013 Gerald Combs <span><span class="__cf_email__" data-cfemail="f4939186959890b4839d8691879c95869fda9b8693">[email protected]</span></span> and contributors. This is free software; see the source for copying conditions. There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</p><p>Compiled (64-bit) with GTK+ 2.24.17, with Cairo 1.10.2, with Pango 1.30.1, with GLib 2.36.0, with libpcap, with libz 1.2.5, without POSIX capabilities, without libnl, with SMI 0.4.8, without c-ares, without ADNS, with Lua 5.1, without Python, with GnuTLS 2.12.19, with Gcrypt 1.5.0, with MIT Kerberos, with GeoIP, with PortAudio V19-devel (built Jul 16 2013 19:05:52), with AirPcap.</p><p>Running on Mac OS X 10.7.5, build 11G63 (Darwin 11.4.2), with locale .UTF-8, with libpcap version 1.1.1, with libz 1.2.5, GnuTLS 2.12.19, Gcrypt 1.5.0, without AirPcap. Intel(R) Core(TM)2 CPU T7400 @ 2.16GHz</p><p>Built using llvm-gcc 4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2336.9.00).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xquartz" rel="tag" title="see questions tagged &#39;xquartz&#39;">xquartz</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-contextual-menu" rel="tag" title="see questions tagged &#39;contextual-menu&#39;">contextual-menu</span> <span class="post-tag tag-link-right-click" rel="tag" title="see questions tagged &#39;right-click&#39;">right-click</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Aug '13, 08:45</strong></p><img src="https://secure.gravatar.com/avatar/6ad809c1810c299c075eb33342e62db3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mmd&#39;s gravatar image" /><p><span>mmd</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mmd has no accepted answers">0%</span></p></div></div><div id="comments-container-23712" class="comments-container"><span id="45645"></span><div id="comment-45645" class="comment"><div id="post-45645-score" class="comment-score"></div><div class="comment-text"><p>Running <code>Version 1.12.7 (v1.12.7-0-g7fc8978 from master-1.12)</code> on a MacBook with Yosemite, two finger clicking shows the the context menu.</p></div><div id="comment-45645-info" class="comment-info"><span class="comment-age">(06 Sep '15, 09:21)</span> <span class="comment-user userinfo">snowch</span></div></div></div><div id="comment-tools-23712" class="comment-tools"></div><div class="clear"></div><div id="comment-23712-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25089"></span>

<div id="answer-container-25089" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25089-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25089-score" class="post-score" title="current number of votes">3</div><span id="post-25089-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I do not get any contextual menus when I ctrl-click (the normal equivalent of "right-click") on a field in the Wireshark GUI.</p></blockquote><p>Wireshark is currently an X11 app; the equivalent of right-click in X11 is command-click, not ctrl-click. (And that's not the only thing different between X11 apps and native apps; the biggest difference is arguably the menu bar.)</p><p>We'd like to get X11 out of the way for a lot of reasons, but it's a work in progress (currently being done using the Qt toolkit).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Sep '13, 19:16</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-25089" class="comments-container"><span id="27980"></span><div id="comment-27980" class="comment"><div id="post-27980-score" class="comment-score"></div><div class="comment-text"><p>Guy, how confident are you that Command-Click produces contextual menus in Wireshark under OS X?</p><p>I agree that Right-Click does this under Windows &amp; Linux ... but on my newly-acquired OS X box (I'm new to OS X), I don't see anything when I Command-Click:</p><p>OS X 10.8.5, Xquartz 2.7.5, Wireshark 1.10.2, QtKit 7.7.1</p><p>?</p><p>--sk</p></div><div id="comment-27980-info" class="comment-info"><span class="comment-age">(10 Dec '13, 13:19)</span> <span class="comment-user userinfo">skendric</span></div></div><span id="27985"></span><div id="comment-27985" class="comment"><div id="post-27985-score" class="comment-score"></div><div class="comment-text"><p>Having just tried it, I'm <em>extremely</em> confident that, <em>with the GTK+ version of Wireshark</em> (built with an X11 version of GTK+, which is the version we ship), Command+Click produces contextual menus, at least with OS X 10.8.5 and XQuartz 2.7.4 and the top-of-trunk version of Wireshark. Perhaps XQuartz 2.7.5 broke something.</p><p>The Qt (that's capital Q, lower-case t) version is not X11-based, and will behave differently; it should use Control+Click, as with native OS X applications.</p><p>(The version of QTKit (that's capital Q, capital T, standing for "QuickTime") is irrelevant, as Wireshark doesn't use it. Yes, it's confusing. Feel free to let Apple and Digia have a "two men enter, one man leaves" cage match to decide who gets to use the letters "q" and "t" in sequence. :-))</p></div><div id="comment-27985-info" class="comment-info"><span class="comment-age">(10 Dec '13, 19:19)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-25089" class="comment-tools"></div><div class="clear"></div><div id="comment-25089-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

