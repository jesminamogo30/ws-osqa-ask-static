+++
type = "question"
title = "Can&#x27;t find Socket ID."
description = '''Can&#x27;t find Socket ID.  How can I know that all packets are in the same socket.'''
date = "2016-04-19T00:49:00Z"
lastmod = "2016-04-19T04:37:00Z"
weight = 51773
keywords = [ "sockets" ]
aliases = [ "/questions/51773" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can't find Socket ID.](/questions/51773/cant-find-socket-id)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51773-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51773-score" class="post-score" title="current number of votes">0</div><span id="post-51773-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can't find Socket ID. How can I know that all packets are in the same socket.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sockets" rel="tag" title="see questions tagged &#39;sockets&#39;">sockets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Apr '16, 00:49</strong></p><img src="https://secure.gravatar.com/avatar/6692d195da01f0362d9ea84958afefec?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AMitiev&#39;s gravatar image" /><p><span>AMitiev</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AMitiev has no accepted answers">0%</span></p></div></div><div id="comments-container-51773" class="comments-container"></div><div id="comment-tools-51773" class="comment-tools"></div><div class="clear"></div><div id="comment-51773-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51775"></span>

<div id="answer-container-51775" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51775-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51775-score" class="post-score" title="current number of votes">0</div><span id="post-51775-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>A socket is the combination of the IP address and the port. So if you're looking for sockets, combine source IP and source port, as well as destination IP and destination port.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '16, 03:49</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-51775" class="comments-container"><span id="51776"></span><div id="comment-51776" class="comment"><div id="post-51776-score" class="comment-score"></div><div class="comment-text"><p>I wondered if the OP was asking about an actual socket instance for the client\server application.</p><p>If so, for TCP, each new socket will have to undertake the 3-way handshake, but in UDP for example, there's no clue in a capture file.</p></div><div id="comment-51776-info" class="comment-info"><span class="comment-age">(19 Apr '16, 04:37)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-51775" class="comment-tools"></div><div class="clear"></div><div id="comment-51775-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

