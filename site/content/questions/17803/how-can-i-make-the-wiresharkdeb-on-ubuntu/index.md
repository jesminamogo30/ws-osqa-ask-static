+++
type = "question"
title = "How can i make the wireshark.deb on Ubuntu?"
description = '''How can i make the wireshark.deb on Ubuntu? in my step: 1,down the wireshark-1.8.3 code and decompress it. 2,./configure. 3,make. 4,?? how to make the .deb? Thanks!'''
date = "2013-01-20T23:10:00Z"
lastmod = "2013-01-21T19:25:00Z"
weight = 17803
keywords = [ "deb", "ubuntu" ]
aliases = [ "/questions/17803" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can i make the wireshark.deb on Ubuntu?](/questions/17803/how-can-i-make-the-wiresharkdeb-on-ubuntu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17803-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17803-score" class="post-score" title="current number of votes">0</div><span id="post-17803-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can i make the wireshark.deb on Ubuntu? in my step: 1,down the wireshark-1.8.3 code and decompress it. 2,./configure. 3,make. 4,?? how to make the .deb?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-deb" rel="tag" title="see questions tagged &#39;deb&#39;">deb</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jan '13, 23:10</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-17803" class="comments-container"></div><div id="comment-tools-17803" class="comment-tools"></div><div class="clear"></div><div id="comment-17803-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17804"></span>

<div id="answer-container-17804" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17804-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17804-score" class="post-score" title="current number of votes">1</div><span id="post-17804-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://www.wireshark.org/docs/wsdg_html_chunked/ChSrcBinary.html#ChSrcDeb">make debian-package</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jan '13, 05:45</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-17804" class="comments-container"><span id="17847"></span><div id="comment-17847" class="comment"><div id="post-17847-score" class="comment-score"></div><div class="comment-text"><p>when i run "make debian-pakage". a error in the end of procession: dh_movefiles: debian/tmp//usr/lib/wireshark/libwiretap.so.2.0.3 not found (supposed to put it in wireshark-common)</p><p>how can fix it?</p><p>thanks!</p></div><div id="comment-17847-info" class="comment-info"><span class="comment-age">(21 Jan '13, 19:25)</span> <span class="comment-user userinfo">smilezuzu</span></div></div></div><div id="comment-tools-17804" class="comment-tools"></div><div class="clear"></div><div id="comment-17804-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

