+++
type = "question"
title = "Source and Destination IP Address All 00:00:00:00:00:00"
description = '''I&#x27;ve had this problem for a while now. When I do a capture I get a lot of packets that they Source, Destination, MAC address and Data all come back with only zeros. The only thing on the Network that seems to have a problem with these packets is the boot process on some of our Mitel VoIP phone&#x27;s. An...'''
date = "2013-04-18T07:27:00Z"
lastmod = "2013-04-18T07:48:00Z"
weight = 20572
keywords = [ "badpackets" ]
aliases = [ "/questions/20572" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Source and Destination IP Address All 00:00:00:00:00:00](/questions/20572/source-and-destination-ip-address-all-000000000000)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20572-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20572-score" class="post-score" title="current number of votes">0</div><span id="post-20572-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've had this problem for a while now. When I do a capture I get a lot of packets that they Source, Destination, MAC address and Data all come back with only zeros. The only thing on the Network that seems to have a problem with these packets is the boot process on some of our Mitel VoIP phone's. Any ideas where to go with this? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-badpackets" rel="tag" title="see questions tagged &#39;badpackets&#39;">badpackets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '13, 07:27</strong></p><img src="https://secure.gravatar.com/avatar/7464c843519a97aa093ecf32f1bb820c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Caldwell_IT&#39;s gravatar image" /><p><span>Caldwell_IT</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Caldwell_IT has no accepted answers">0%</span></p></div></div><div id="comments-container-20572" class="comments-container"></div><div id="comment-tools-20572" class="comment-tools"></div><div class="clear"></div><div id="comment-20572-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20574"></span>

<div id="answer-container-20574" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20574-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20574-score" class="post-score" title="current number of votes">0</div><span id="post-20574-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>please see my answer to a similar question</p><blockquote><p><code>http://ask.wireshark.org/questions/20481/malformed-packets</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Apr '13, 07:29</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-20574" class="comments-container"><span id="20575"></span><div id="comment-20575" class="comment"><div id="post-20575-score" class="comment-score"></div><div class="comment-text"><p>Hi,</p><p>Disconnecting ports when you have over 3000 ports in 8 different buildings is not going to work to try and find this problem.</p><p>Thanks. Linda</p></div><div id="comment-20575-info" class="comment-info"><span class="comment-age">(18 Apr '13, 07:45)</span> <span class="comment-user userinfo">Caldwell_IT</span></div></div><span id="20576"></span><div id="comment-20576" class="comment"><div id="post-20576-score" class="comment-score"></div><div class="comment-text"><p>well, you are right, but that was only the last option, if my first suggestion (CAM table of switches) does not work!</p><p>Did you try that?</p></div><div id="comment-20576-info" class="comment-info"><span class="comment-age">(18 Apr '13, 07:48)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-20574" class="comment-tools"></div><div class="clear"></div><div id="comment-20574-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

