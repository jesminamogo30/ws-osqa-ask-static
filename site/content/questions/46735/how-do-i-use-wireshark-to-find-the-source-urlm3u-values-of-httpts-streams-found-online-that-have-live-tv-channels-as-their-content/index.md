+++
type = "question"
title = "How do I use wireshark to find the Source URL/m3u values of &#x27;http.ts&#x27;&#x27;- streams, found online that have live TV Channels as their content?"
description = '''Hi everyone, I am a fresh noob to wireshark though competent to follow direction and take guidance to tackle this task if anyone would be so very kind to help a wireshark-noob?? I have come across a list of streams in &#x27;http -/- .ts&#x27; format and urgently need your expert-assistance on how to use wires...'''
date = "2015-10-19T17:13:00Z"
lastmod = "2015-10-19T23:46:00Z"
weight = 46735
keywords = [ "url", "capture-filter", "http", "snooping", "snoop" ]
aliases = [ "/questions/46735" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I use wireshark to find the Source URL/m3u values of 'http.ts''- streams, found online that have live TV Channels as their content?](/questions/46735/how-do-i-use-wireshark-to-find-the-source-urlm3u-values-of-httpts-streams-found-online-that-have-live-tv-channels-as-their-content)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46735-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46735-score" class="post-score" title="current number of votes">0</div><span id="post-46735-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone, I am a fresh noob to wireshark though competent to follow direction and take guidance to tackle this task if anyone would be so very kind to help a wireshark-noob?? I have come across a list of streams in 'http -/- .ts' format and urgently need your expert-assistance on how to use wireshark to find the source link, token and whatever else is needed to play and sustain these streams to be used in an m3u/8 list either in VLC or kodi.</p><p>If a list guide for assistance could be given i would very much appreciate i and if some tips on filtering the information for this file type to find the source and teach how to create the file in the first, i would be eternally grateful in-deed.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-url" rel="tag" title="see questions tagged &#39;url&#39;">url</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-snooping" rel="tag" title="see questions tagged &#39;snooping&#39;">snooping</span> <span class="post-tag tag-link-snoop" rel="tag" title="see questions tagged &#39;snoop&#39;">snoop</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Oct '15, 17:13</strong></p><img src="https://secure.gravatar.com/avatar/248ec41397f09e94911be740b9c92bee?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Phoenix101&#39;s gravatar image" /><p><span>Phoenix101</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Phoenix101 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Oct '15, 03:55</strong> </span></p></div></div><div id="comments-container-46735" class="comments-container"><span id="46744"></span><div id="comment-46744" class="comment"><div id="post-46744-score" class="comment-score"></div><div class="comment-text"><p>Each post should have a clear, specific question in the title field. Please rephrase the title as a proper question.</p></div><div id="comment-46744-info" class="comment-info"><span class="comment-age">(19 Oct '15, 23:46)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-46735" class="comment-tools"></div><div class="clear"></div><div id="comment-46735-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

