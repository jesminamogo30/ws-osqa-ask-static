+++
type = "question"
title = "analyzing acl"
description = '''Hi Guys, which protocol can i analyze on Wireshark the Acls being processed up to a destination address ? best regards,'''
date = "2016-10-20T07:14:00Z"
lastmod = "2016-10-20T08:32:00Z"
weight = 56538
keywords = [ "acl" ]
aliases = [ "/questions/56538" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [analyzing acl](/questions/56538/analyzing-acl)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56538-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56538-score" class="post-score" title="current number of votes">0</div><span id="post-56538-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Guys,</p><p>which protocol can i analyze on Wireshark the Acls being processed up to a destination address ?</p><p>best regards,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-acl" rel="tag" title="see questions tagged &#39;acl&#39;">acl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Oct '16, 07:14</strong></p><img src="https://secure.gravatar.com/avatar/712e9fe6833029010906fabad50e7cb9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bruno%20trombim&#39;s gravatar image" /><p><span>bruno trombim</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bruno trombim has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Oct '16, 08:49</strong> </span></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span></p></div></div><div id="comments-container-56538" class="comments-container"><span id="56539"></span><div id="comment-56539" class="comment"><div id="post-56539-score" class="comment-score"></div><div class="comment-text"><p>Asking almost the same question once again does not cause an answer to come faster.</p></div><div id="comment-56539-info" class="comment-info"><span class="comment-age">(20 Oct '16, 08:32)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-56538" class="comment-tools"></div><div class="clear"></div><div id="comment-56538-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

