+++
type = "question"
title = "HL7 Msgs Capture"
description = '''I&#x27;m trying to capture HL7 msgs on the ip and port but can&#x27;t see them in the capture. Any direction on this. thanks tony'''
date = "2014-02-19T21:54:00Z"
lastmod = "2014-02-20T17:11:00Z"
weight = 30037
keywords = [ "hl7", "caputure" ]
aliases = [ "/questions/30037" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [HL7 Msgs Capture](/questions/30037/hl7-msgs-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30037-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30037-score" class="post-score" title="current number of votes">0</div><span id="post-30037-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to capture HL7 msgs on the ip and port but can't see them in the capture. Any direction on this. thanks tony</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hl7" rel="tag" title="see questions tagged &#39;hl7&#39;">hl7</span> <span class="post-tag tag-link-caputure" rel="tag" title="see questions tagged &#39;caputure&#39;">caputure</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '14, 21:54</strong></p><img src="https://secure.gravatar.com/avatar/917a228c47a2728376c6e4699592e573?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TVolpe&#39;s gravatar image" /><p><span>TVolpe</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TVolpe has no accepted answers">0%</span></p></div></div><div id="comments-container-30037" class="comments-container"><span id="30039"></span><div id="comment-30039" class="comment"><div id="post-30039-score" class="comment-score"></div><div class="comment-text"><p>can you please add some details <strong>how you tried to capture</strong>, meaning your capture setup</p><ul><li>OS and Wireshark version</li><li>interfaces LAN, WAN, etc.</li><li>where did you capture: client, server, switch</li><li>did you use any capture filters</li></ul></div><div id="comment-30039-info" class="comment-info"><span class="comment-age">(20 Feb '14, 00:49)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="30063"></span><div id="comment-30063" class="comment"><div id="post-30063-score" class="comment-score"></div><div class="comment-text"><p>Is that "HL7" as in <a href="https://www.hl7.org">Health Level Seven International</a>?</p></div><div id="comment-30063-info" class="comment-info"><span class="comment-age">(20 Feb '14, 17:11)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-30037" class="comment-tools"></div><div class="clear"></div><div id="comment-30037-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

