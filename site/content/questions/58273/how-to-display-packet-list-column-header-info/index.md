+++
type = "question"
title = "How to display Packet List column header &#x27;info&#x27;"
description = '''hi  How to display or enable Packet List column header &#x27;info&#x27; now im using Version 2.2.3'''
date = "2016-12-21T10:00:00Z"
lastmod = "2016-12-22T01:06:00Z"
weight = 58273
keywords = [ "column" ]
aliases = [ "/questions/58273" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to display Packet List column header 'info'](/questions/58273/how-to-display-packet-list-column-header-info)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58273-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58273-score" class="post-score" title="current number of votes">0</div><span id="post-58273-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>hi</p><p>How to display or enable Packet List column header 'info' now im using Version 2.2.3</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-column" rel="tag" title="see questions tagged &#39;column&#39;">column</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Dec '16, 10:00</strong></p><img src="https://secure.gravatar.com/avatar/2386b41a536a66e92a4d52b2b5c17a33?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="badr313&#39;s gravatar image" /><p><span>badr313</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="badr313 has no accepted answers">0%</span></p></div></div><div id="comments-container-58273" class="comments-container"></div><div id="comment-tools-58273" class="comment-tools"></div><div class="clear"></div><div id="comment-58273-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58276"></span>

<div id="answer-container-58276" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58276-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58276-score" class="post-score" title="current number of votes">0</div><span id="post-58276-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You have to open the Preferences dialog and choose Appearance -&gt; Columns. There you can enable Info again or you can add it as new one.</p><p>For adding a new one please do the following things:</p><ul><li>Press the + sign</li><li>As type you should choose "Information"</li><li>Then you should enable the the checkbox in the "Displayed" column</li><li>Optional you can akter the name of the column a t the title column</li></ul><p><img src="https://osqa-ask.wireshark.org/upfiles/2016-12-21_20-44-15_7MWOvVI.png" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Dec '16, 11:54</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></img></div></div><div id="comments-container-58276" class="comments-container"><span id="58290"></span><div id="comment-58290" class="comment"><div id="post-58290-score" class="comment-score"></div><div class="comment-text"><p>yeah got it! thanks....</p></div><div id="comment-58290-info" class="comment-info"><span class="comment-age">(22 Dec '16, 00:46)</span> <span class="comment-user userinfo">badr313</span></div></div><span id="58294"></span><div id="comment-58294" class="comment"><div id="post-58294-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-58294-info" class="comment-info"><span class="comment-age">(22 Dec '16, 01:06)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-58276" class="comment-tools"></div><div class="clear"></div><div id="comment-58276-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

