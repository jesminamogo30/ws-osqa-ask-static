+++
type = "question"
title = "Bad links on support site"
description = '''When accessing http://www.wireshark.org/security/wnpa-sec-2011-09.html the two links -- http://www.wireshark.org/security/wnpa-sec-2011-10.html and http://www.wireshark.org/security/wnpa-sec-2011-11.html -- both appear to be dead. They take you to the kitten on the server screen.'''
date = "2011-07-11T10:40:00Z"
lastmod = "2011-07-11T14:05:00Z"
weight = 4984
keywords = [ "wpna-sec", "links" ]
aliases = [ "/questions/4984" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Bad links on support site](/questions/4984/bad-links-on-support-site)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4984-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4984-score" class="post-score" title="current number of votes">0</div><span id="post-4984-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When accessing <a href="http://www.wireshark.org/security/wnpa-sec-2011-09.html">http://www.wireshark.org/security/wnpa-sec-2011-09.html</a> the two links -- <a href="http://www.wireshark.org/security/wnpa-sec-2011-10.html">http://www.wireshark.org/security/wnpa-sec-2011-10.html</a> and <a href="http://www.wireshark.org/security/wnpa-sec-2011-11.html">http://www.wireshark.org/security/wnpa-sec-2011-11.html</a> -- both appear to be dead. They take you to the kitten on the server screen.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wpna-sec" rel="tag" title="see questions tagged &#39;wpna-sec&#39;">wpna-sec</span> <span class="post-tag tag-link-links" rel="tag" title="see questions tagged &#39;links&#39;">links</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jul '11, 10:40</strong></p><img src="https://secure.gravatar.com/avatar/082c26dd61d8c949c8b7bcd21ee93891?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchladek&#39;s gravatar image" /><p><span>lchladek</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchladek has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Jul '11, 12:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-4984" class="comments-container"><span id="4985"></span><div id="comment-4985" class="comment"><div id="post-4985-score" class="comment-score"></div><div class="comment-text"><p>The best place to report Wireshark bugs, and problems with the Wireshark Web sites, is <a href="http://bugs.wireshark.org/">the Wireshark Bugzilla</a>.</p></div><div id="comment-4985-info" class="comment-info"><span class="comment-age">(11 Jul '11, 12:33)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-4984" class="comment-tools"></div><div class="clear"></div><div id="comment-4984-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4986"></span>

<div id="answer-container-4986" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4986-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4986-score" class="post-score" title="current number of votes">0</div><span id="post-4986-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Those two links are for advisories for Wireshark 1.4.8 and Wireshark 1.6.1, respectively. Neither have been released yet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jul '11, 14:05</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-4986" class="comments-container"></div><div id="comment-tools-4986" class="comment-tools"></div><div class="clear"></div><div id="comment-4986-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

