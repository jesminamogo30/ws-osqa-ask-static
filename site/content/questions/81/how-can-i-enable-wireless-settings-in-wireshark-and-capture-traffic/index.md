+++
type = "question"
title = "How Can I enable Wireless settings in Wireshark  and capture traffic ?"
description = '''My laptop doesn&#x27;t have wireless card (specificly it has been disabled ) and I want to capture all traffic from these three access points (my laptop ,two pc and access point is the same lan)'''
date = "2010-09-15T04:22:00Z"
lastmod = "2010-10-02T20:46:00Z"
weight = 81
keywords = [ "wlan" ]
aliases = [ "/questions/81" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How Can I enable Wireless settings in Wireshark and capture traffic ?](/questions/81/how-can-i-enable-wireless-settings-in-wireshark-and-capture-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-81-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-81-score" class="post-score" title="current number of votes">0</div><span id="post-81-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My laptop doesn't have wireless card (specificly it has been disabled ) and I want to capture all traffic from these three access points (my laptop ,two pc and access point is the same lan)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Sep '10, 04:22</strong></p><img src="https://secure.gravatar.com/avatar/b7fe847006c09499531beedbbf118b1f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wapi&#39;s gravatar image" /><p><span>wapi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wapi has no accepted answers">0%</span></p></div></div><div id="comments-container-81" class="comments-container"><span id="86"></span><div id="comment-86" class="comment"><div id="post-86-score" class="comment-score"></div><div class="comment-text"><p>What operating system are you using?</p></div><div id="comment-86-info" class="comment-info"><span class="comment-age">(15 Sep '10, 10:29)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-81" class="comment-tools"></div><div class="clear"></div><div id="comment-81-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="407"></span>

<div id="answer-container-407" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-407-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-407-score" class="post-score" title="current number of votes">1</div><span id="post-407-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well... you'll need a wireless card of some sort - whether you enable your native card and see what you can capture or perhaps, if it is a Windows system you get an AirPcap adapter. There is a video about testing adapters over at www.wiresharkbook.com/coffee.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Oct '10, 20:46</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-407" class="comments-container"></div><div id="comment-tools-407" class="comment-tools"></div><div class="clear"></div><div id="comment-407-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

