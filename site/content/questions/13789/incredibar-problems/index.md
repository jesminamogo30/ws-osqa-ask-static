+++
type = "question"
title = "Incredibar problems"
description = '''I&#x27;ve been having a strange malware problem with something called the Incredibar. I&#x27;ve been working with someone from Malwarebytes. We are still working on it but we haven&#x27;t been able to solve it yet. Meanwhile, I heard about Wire Shark through a friend and wanted to see if it could be applied to thi...'''
date = "2012-08-20T22:06:00Z"
lastmod = "2012-08-21T22:42:00Z"
weight = 13789
keywords = [ "ie8", "malware", "incredibar" ]
aliases = [ "/questions/13789" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Incredibar problems](/questions/13789/incredibar-problems)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13789-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13789-score" class="post-score" title="current number of votes">0</div><span id="post-13789-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've been having a strange malware problem with something called the Incredibar. I've been working with someone from Malwarebytes. We are still working on it but we haven't been able to solve it yet. Meanwhile, I heard about Wire Shark through a friend and wanted to see if it could be applied to this situation. I am running XP Home Edition on my computer for my OS. I've been using IE8, Chrome and Mozilla Firefox browsers. Initially all the browsers on my machine were infected. I was able to uninstall the Incredibar from add/remove programs then remove it from the browsers manually. We have run Malwarebytes, Spybot, Adware and even Combofix and scanned the registry for related files. After all this - whenever we open Youtube, Facebook or Yahoo, a small bar called the "Incredibar" just below the address bar. This only happens in IE8, not Chrome or Firefox. I wondered if WireShark can help me step through the process of opening one of the pages in IE8 and determine what process is executing to open the Incredibar when I go to one of the pages mentioned. Any info would be appreciated. Thanks, Matt</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ie8" rel="tag" title="see questions tagged &#39;ie8&#39;">ie8</span> <span class="post-tag tag-link-malware" rel="tag" title="see questions tagged &#39;malware&#39;">malware</span> <span class="post-tag tag-link-incredibar" rel="tag" title="see questions tagged &#39;incredibar&#39;">incredibar</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Aug '12, 22:06</strong></p><img src="https://secure.gravatar.com/avatar/1a01a50dbf0e61ead2dcbbee9446647a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="presto327&#39;s gravatar image" /><p><span>presto327</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="presto327 has no accepted answers">0%</span></p></div></div><div id="comments-container-13789" class="comments-container"></div><div id="comment-tools-13789" class="comment-tools"></div><div class="clear"></div><div id="comment-13789-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="13790"></span>

<div id="answer-container-13790" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13790-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13790-score" class="post-score" title="current number of votes">1</div><span id="post-13790-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I wondered if WireShark can help me step through the process of opening one of the pages in IE8 and determine what process is executing to open the Incredibar</p></blockquote><p>Wireshark cannot help in this case, as it's a network sniffer. It cannot show who created a packet.</p><p>You need something like Sysinternals <a href="http://technet.microsoft.com/de-de/sysinternals/bb896645.aspx">Process Monitor</a> or <a href="http://technet.microsoft.com/de-de/sysinternals/bb896653.aspx">Process Explorer</a>. Please ask the Sysinternals community how to use those tools to track down the malware.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Aug '12, 00:30</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-13790" class="comments-container"><span id="13806"></span><div id="comment-13806" class="comment"><div id="post-13806-score" class="comment-score"></div><div class="comment-text"><p>Thanks Kurt - I'll inquire. Appreciate the information! Matt</p></div><div id="comment-13806-info" class="comment-info"><span class="comment-age">(21 Aug '12, 22:36)</span> <span class="comment-user userinfo">presto327</span></div></div></div><div id="comment-tools-13790" class="comment-tools"></div><div class="clear"></div><div id="comment-13790-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="13798"></span>

<div id="answer-container-13798" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13798-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13798-score" class="post-score" title="current number of votes">1</div><span id="post-13798-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Googling for "incredibar" finds, in addition to what appears to be the Web site for the Incredibar itself (I didn't go to that site, as I have no idea whether it'd inject the Incredibar into a non-Windows OS or a non-IE browser, and won't give the domain name, as I suspect nobody else should go there either), a bunch of pages that purport to say how to remove the Incredibar. That might be easier than trying to figure out with Wireshark what process is responsible for the Incredibar and removing the program it's running - especially if, for example, the program is your Web browser and the Incredibar code is a DLL loaded by the browser as a plugin.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Aug '12, 12:31</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-13798" class="comments-container"><span id="13807"></span><div id="comment-13807" class="comment"><div id="post-13807-score" class="comment-score"></div><div class="comment-text"><p>Guy, Thanks for the comment. I wondered if it might be something along those lines - Your suggestion sounds good. Thanks again, Matt</p></div><div id="comment-13807-info" class="comment-info"><span class="comment-age">(21 Aug '12, 22:42)</span> <span class="comment-user userinfo">presto327</span></div></div></div><div id="comment-tools-13798" class="comment-tools"></div><div class="clear"></div><div id="comment-13798-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

