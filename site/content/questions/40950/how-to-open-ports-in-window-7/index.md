+++
type = "question"
title = "how to open ports in window 7"
description = '''i am tryingto add remote interface in wireshark remote interface menu, for that as per prerequisite need open port no 2002, i have created inbound firewall rule on target pc to open port 2002, and also tried to disable the firewall, though can not add the remote interface, so kindly guide to open po...'''
date = "2015-03-27T22:47:00Z"
lastmod = "2015-03-28T09:31:00Z"
weight = 40950
keywords = [ "ports" ]
aliases = [ "/questions/40950" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to open ports in window 7](/questions/40950/how-to-open-ports-in-window-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40950-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40950-score" class="post-score" title="current number of votes">0</div><span id="post-40950-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i am tryingto add remote interface in wireshark remote interface menu, for that as per prerequisite need open port no 2002, i have created inbound firewall rule on target pc to open port 2002, and also tried to disable the firewall, though can not add the remote interface, so kindly guide to open port 2002.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ports" rel="tag" title="see questions tagged &#39;ports&#39;">ports</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Mar '15, 22:47</strong></p><img src="https://secure.gravatar.com/avatar/5c0438a9e0a8ace741c1879a0f703f11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="drp19872&#39;s gravatar image" /><p><span>drp19872</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="drp19872 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-40950" class="comments-container"></div><div id="comment-tools-40950" class="comment-tools"></div><div class="clear"></div><div id="comment-40950-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40957"></span>

<div id="answer-container-40957" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40957-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40957-score" class="post-score" title="current number of votes">0</div><span id="post-40957-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have you started an <strong>rpcapd</strong> process on the target PC? That's the process that is connected to from Wireshark. If you haven't started it, you can't connect. rpcapd is an executable that is installed together with WinPCAP, so you need to install this first.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Mar '15, 09:31</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-40957" class="comments-container"></div><div id="comment-tools-40957" class="comment-tools"></div><div class="clear"></div><div id="comment-40957-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

