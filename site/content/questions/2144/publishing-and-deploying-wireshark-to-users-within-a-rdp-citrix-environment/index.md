+++
type = "question"
title = "Publishing and Deploying Wireshark to users Within a RDP / Citrix Environment"
description = '''Hi all  I have a customer who wants to deploy Wireshark as a published app to his user base i.e the app is being shared. They are technology firm who do use it for testing of there equipment that they produce etc. Just to make clear this isn&#x27;t for testing the Xenapp (RDO) server but for use as a bus...'''
date = "2011-02-04T02:37:00Z"
lastmod = "2014-02-06T04:23:00Z"
weight = 2144
keywords = [ "wireshark", "rdp", "citrix" ]
aliases = [ "/questions/2144" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Publishing and Deploying Wireshark to users Within a RDP / Citrix Environment](/questions/2144/publishing-and-deploying-wireshark-to-users-within-a-rdp-citrix-environment)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2144-score" class="post-score" title="current number of votes">0</div><span id="post-2144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all</p><p>I have a customer who wants to deploy Wireshark as a published app to his user base i.e the app is being shared. They are technology firm who do use it for testing of there equipment that they produce etc. Just to make clear this isn't for testing the Xenapp (RDO) server but for use as a business tool.</p><p>Does anyone know if this is supported? or if there are any pit falls gotchas people have had</p><p>From my experience just running wireshark on a PC does eat alot of Memory and leaks but running approx 10 -15 instances on 1 box.....................</p><p>Currently there are 4 Xenapp 6 Servers (RDP) which are provisioned and there are about 100 users logging on to therm</p><p>Any help would really be appreciated</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-rdp" rel="tag" title="see questions tagged &#39;rdp&#39;">rdp</span> <span class="post-tag tag-link-citrix" rel="tag" title="see questions tagged &#39;citrix&#39;">citrix</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Feb '11, 02:37</strong></p><img src="https://secure.gravatar.com/avatar/e8b0a2b2ff2dcf2e13f86d25a5771551?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dave%20Fraser&#39;s gravatar image" /><p><span>Dave Fraser</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dave Fraser has no accepted answers">0%</span></p></div></div><div id="comments-container-2144" class="comments-container"><span id="29488"></span><div id="comment-29488" class="comment"><div id="post-29488-score" class="comment-score"></div><div class="comment-text"><p>Did you customer deploy, did he have any problems&gt;</p></div><div id="comment-29488-info" class="comment-info"><span class="comment-age">(06 Feb '14, 04:23)</span> <span class="comment-user userinfo">Sad Developer</span></div></div></div><div id="comment-tools-2144" class="comment-tools"></div><div class="clear"></div><div id="comment-2144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

