+++
type = "question"
title = "Decrypt a TLSv1 stream"
description = '''I did the following steps:  Menu: Preferences / Protocols / SSL / RAS keys list Added IP / 443 / http / + key PEM file with RSA PRIVATE KEY Added the same with spdy as protocol Started the capturing including the TLSv1 &quot;Client Hello&quot; sequence &quot;Follow TCP Stream&quot; shows the encrypted stream &quot;Follow SS...'''
date = "2014-08-22T07:32:00Z"
lastmod = "2014-08-25T15:28:00Z"
weight = 35673
keywords = [ "tlsv1" ]
aliases = [ "/questions/35673" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt a TLSv1 stream](/questions/35673/decrypt-a-tlsv1-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35673-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35673-score" class="post-score" title="current number of votes">0</div><span id="post-35673-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I did the following steps:</p><ul><li>Menu: Preferences / Protocols / SSL / RAS keys list</li><li>Added IP / 443 / http / + key PEM file with RSA PRIVATE KEY</li><li>Added the same with spdy as protocol</li><li>Started the capturing including the TLSv1 "Client Hello" sequence</li><li>"Follow TCP Stream" shows the encrypted stream</li><li>"Follow SSL Stream" shows 0 bytes as conversation</li></ul><p>Have no idea, what's the problem.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tlsv1" rel="tag" title="see questions tagged &#39;tlsv1&#39;">tlsv1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '14, 07:32</strong></p><img src="https://secure.gravatar.com/avatar/b37632ba375938b2bca86668069cf068?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sven_schaefer&#39;s gravatar image" /><p><span>sven_schaefer</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sven_schaefer has no accepted answers">0%</span></p></div></div><div id="comments-container-35673" class="comments-container"><span id="35730"></span><div id="comment-35730" class="comment"><div id="post-35730-score" class="comment-score"></div><div class="comment-text"><p>please create a SSL debug file (see help) and post the first 100-200 lines here.</p></div><div id="comment-35730-info" class="comment-info"><span class="comment-age">(25 Aug '14, 15:28)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-35673" class="comment-tools"></div><div class="clear"></div><div id="comment-35673-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

