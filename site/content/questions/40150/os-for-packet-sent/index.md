+++
type = "question"
title = "OS for packet sent"
description = '''How do you determine the operating system used to send a packet?'''
date = "2015-02-28T10:02:00Z"
lastmod = "2015-02-28T14:07:00Z"
weight = 40150
keywords = [ "osused" ]
aliases = [ "/questions/40150" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [OS for packet sent](/questions/40150/os-for-packet-sent)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40150-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40150-score" class="post-score" title="current number of votes">0</div><span id="post-40150-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do you determine the operating system used to send a packet?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osused" rel="tag" title="see questions tagged &#39;osused&#39;">osused</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Feb '15, 10:02</strong></p><img src="https://secure.gravatar.com/avatar/5312128f6e96fb5e5f6d71b9bb485cd5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Margaret&#39;s gravatar image" /><p><span>Margaret</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Margaret has no accepted answers">0%</span></p></div></div><div id="comments-container-40150" class="comments-container"></div><div id="comment-tools-40150" class="comment-tools"></div><div class="clear"></div><div id="comment-40150-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40152"></span>

<div id="answer-container-40152" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40152-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40152-score" class="post-score" title="current number of votes">2</div><span id="post-40152-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>A single packet probably won't be good enough. The technique used to try to <em>guess</em> the OS used on a machine, based on network traffic from that machine, is called "OS fingerprinting", and <a href="https://www.google.com/search?q=%22os+fingerprinting%22&amp;gws_rd=ssl">a Google search for it</a> reveals a number of links; check the pages it finds for more information.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Feb '15, 14:07</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-40152" class="comments-container"></div><div id="comment-tools-40152" class="comment-tools"></div><div class="clear"></div><div id="comment-40152-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

