+++
type = "question"
title = "Speed Control"
description = '''Can i control others wifi speed through my wireshark? I am a newbie to this..'''
date = "2011-11-25T22:26:00Z"
lastmod = "2011-11-26T06:32:00Z"
weight = 7644
keywords = [ "control", "seed" ]
aliases = [ "/questions/7644" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Speed Control](/questions/7644/speed-control)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7644-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7644-score" class="post-score" title="current number of votes">0</div><span id="post-7644-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can i control others wifi speed through my wireshark? I am a newbie to this..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-control" rel="tag" title="see questions tagged &#39;control&#39;">control</span> <span class="post-tag tag-link-seed" rel="tag" title="see questions tagged &#39;seed&#39;">seed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Nov '11, 22:26</strong></p><img src="https://secure.gravatar.com/avatar/bc78c927dc9edffbfa1942f94d18d0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="akbarsha&#39;s gravatar image" /><p><span>akbarsha</span><br />
<span class="score" title="5 reputation points">5</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="akbarsha has no accepted answers">0%</span></p></div></div><div id="comments-container-7644" class="comments-container"></div><div id="comment-tools-7644" class="comment-tools"></div><div class="clear"></div><div id="comment-7644-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7645"></span>

<div id="answer-container-7645" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7645-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7645-score" class="post-score" title="current number of votes">1</div><span id="post-7645-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="akbarsha has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No you can't. Wireshark is a monitoring tool, which means it reads network frames and interprets them. It does not take an active role, neither injecting frames nor doing any kind of rate limiting.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Nov '11, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-7645" class="comments-container"><span id="7650"></span><div id="comment-7650" class="comment"><div id="post-7650-score" class="comment-score"></div><div class="comment-text"><p>Thanks so much..</p></div><div id="comment-7650-info" class="comment-info"><span class="comment-age">(26 Nov '11, 06:32)</span> <span class="comment-user userinfo">akbarsha</span></div></div></div><div id="comment-tools-7645" class="comment-tools"></div><div class="clear"></div><div id="comment-7645-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

