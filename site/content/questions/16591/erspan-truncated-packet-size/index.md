+++
type = "question"
title = "erspan - truncated packet - size"
description = '''I setup a erspan session with mtu 256 (truncates packets). Am i correct that wireshark will report the actual received data when giving throughput and packet size statistics ? (As opposed to look at info in ip/tcp header.( Does this mean i have to enable jumbo frames, and tunnel full packets using e...'''
date = "2012-12-05T05:40:00Z"
lastmod = "2012-12-05T05:40:00Z"
weight = 16591
keywords = [ "jumbo", "erspan" ]
aliases = [ "/questions/16591" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [erspan - truncated packet - size](/questions/16591/erspan-truncated-packet-size)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16591-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16591-score" class="post-score" title="current number of votes">0</div><span id="post-16591-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I setup a erspan session with mtu 256 (truncates packets).</p><p>Am i correct that wireshark will report the actual received data when giving throughput and packet size statistics ? (As opposed to look at info in ip/tcp header.(</p><p>Does this mean i have to enable jumbo frames, and tunnel full packets using erspan to get a accurate picture ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-jumbo" rel="tag" title="see questions tagged &#39;jumbo&#39;">jumbo</span> <span class="post-tag tag-link-erspan" rel="tag" title="see questions tagged &#39;erspan&#39;">erspan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Dec '12, 05:40</strong></p><img src="https://secure.gravatar.com/avatar/fed4e10954aba1076d14434f6216fb3c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="diepes&#39;s gravatar image" /><p><span>diepes</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="diepes has no accepted answers">0%</span></p></div></div><div id="comments-container-16591" class="comments-container"></div><div id="comment-tools-16591" class="comment-tools"></div><div class="clear"></div><div id="comment-16591-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

