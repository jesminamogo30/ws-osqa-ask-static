+++
type = "question"
title = "windows 8 problem"
description = '''hey. i have win 8 developer ver on my computer. i tried to install wireshrk but the winpcap driver isnt supported by win 8... dose anyone running win 8 ver of wireshark or atleaset some fitting drivers?? thanks.'''
date = "2012-03-16T18:13:00Z"
lastmod = "2012-03-17T06:27:00Z"
weight = 9589
keywords = [ "windows8" ]
aliases = [ "/questions/9589" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [windows 8 problem](/questions/9589/windows-8-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9589-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9589-score" class="post-score" title="current number of votes">0</div><span id="post-9589-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hey.</p><p>i have win 8 developer ver on my computer.</p><p>i tried to install wireshrk but the winpcap driver isnt supported by win 8...</p><p>dose anyone running win 8 ver of wireshark or atleaset some fitting drivers??</p><p>thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows8" rel="tag" title="see questions tagged &#39;windows8&#39;">windows8</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '12, 18:13</strong></p><img src="https://secure.gravatar.com/avatar/71b4c1506dc73e41eacd63d70f00ff7a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Helldad&#39;s gravatar image" /><p><span>Helldad</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Helldad has no accepted answers">0%</span></p></div></div><div id="comments-container-9589" class="comments-container"></div><div id="comment-tools-9589" class="comment-tools"></div><div class="clear"></div><div id="comment-9589-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9604"></span>

<div id="answer-container-9604" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9604-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9604-score" class="post-score" title="current number of votes">1</div><span id="post-9604-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This has been asked before. Please take a look at <a href="http://ask.wireshark.org/questions/7425/windows-8-support">this thread</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Mar '12, 06:27</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-9604" class="comments-container"></div><div id="comment-tools-9604" class="comment-tools"></div><div class="clear"></div><div id="comment-9604-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

