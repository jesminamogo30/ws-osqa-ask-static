+++
type = "question"
title = "how to decode TCAP messages using Wireshark"
description = '''Hi, I am trying to decode TCAP messages(for CAP protocol) using WIreshark(v1.0.9),and I find that the data contents are not decoded at all while trying to open the SS7 IP traces recived. Does it required to have special Dictionary files for TCAP while decoding such messages.'''
date = "2011-10-11T01:55:00Z"
lastmod = "2011-10-11T07:05:00Z"
weight = 6842
keywords = [ "tcap" ]
aliases = [ "/questions/6842" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to decode TCAP messages using Wireshark](/questions/6842/how-to-decode-tcap-messages-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6842-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6842-score" class="post-score" title="current number of votes">0</div><span id="post-6842-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to decode TCAP messages(for CAP protocol) using WIreshark(v1.0.9),and I find that the data contents are not decoded at all while trying to open the SS7 IP traces recived.</p><p>Does it required to have special Dictionary files for TCAP while decoding such messages.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcap" rel="tag" title="see questions tagged &#39;tcap&#39;">tcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Oct '11, 01:55</strong></p><img src="https://secure.gravatar.com/avatar/5378f88f4cd501f4cdf3709d79aa0d4f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rouseparty&#39;s gravatar image" /><p><span>rouseparty</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rouseparty has no accepted answers">0%</span></p></div></div><div id="comments-container-6842" class="comments-container"></div><div id="comment-tools-6842" class="comment-tools"></div><div class="clear"></div><div id="comment-6842-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6846"></span>

<div id="answer-container-6846" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6846-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6846-score" class="post-score" title="current number of votes">1</div><span id="post-6846-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi, The version you are using is very old, 1.6.2 is out. You may have to set the preferences to get Camel(CAP) deissected check the SSN used for your packets and ensure that the same SSN(s) are set in the Camel preferences.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Oct '11, 07:05</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-6846" class="comments-container"></div><div id="comment-tools-6846" class="comment-tools"></div><div class="clear"></div><div id="comment-6846-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

