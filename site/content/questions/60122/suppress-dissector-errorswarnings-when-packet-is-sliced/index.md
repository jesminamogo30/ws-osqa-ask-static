+++
type = "question"
title = "suppress dissector errors/warnings when packet is sliced"
description = '''Hi, When Wireshark tries to dissect sliced packets it displays errors/warnings for various protocol types for example SSL, SSH, OTV etc. Is there a way to suppress dissector errors/warning when packets is sliced/trimmed? If not can this be added as option? Thank you'''
date = "2017-03-16T11:55:00Z"
lastmod = "2017-03-16T12:07:00Z"
weight = 60122
keywords = [ "sliced", "trimmed", "dissector" ]
aliases = [ "/questions/60122" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [suppress dissector errors/warnings when packet is sliced](/questions/60122/suppress-dissector-errorswarnings-when-packet-is-sliced)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60122-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60122-score" class="post-score" title="current number of votes">0</div><span id="post-60122-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, When Wireshark tries to dissect sliced packets it displays errors/warnings for various protocol types for example SSL, SSH, OTV etc. Is there a way to suppress dissector errors/warning when packets is sliced/trimmed? If not can this be added as option? Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sliced" rel="tag" title="see questions tagged &#39;sliced&#39;">sliced</span> <span class="post-tag tag-link-trimmed" rel="tag" title="see questions tagged &#39;trimmed&#39;">trimmed</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Mar '17, 11:55</strong></p><img src="https://secure.gravatar.com/avatar/f0d049fff33eee7fbeff10d3c08275d7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yakovd&#39;s gravatar image" /><p><span>yakovd</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yakovd has no accepted answers">0%</span></p></div></div><div id="comments-container-60122" class="comments-container"></div><div id="comment-tools-60122" class="comment-tools"></div><div class="clear"></div><div id="comment-60122-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60124"></span>

<div id="answer-container-60124" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60124-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60124-score" class="post-score" title="current number of votes">0</div><span id="post-60124-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Codewise it's not easy to suppress the warnings, a dissector <em>shouldn't</em> try to check the packet length, just try to dissect it and get the malformed exception if it's too short.</p><p>What you can do though, is create a profile and disable all dissectors except the one up to where you sliced, i.e. Ethernet, ip, tcp so that other dissectors aren't called.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Mar '17, 12:07</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-60124" class="comments-container"></div><div id="comment-tools-60124" class="comment-tools"></div><div class="clear"></div><div id="comment-60124-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

