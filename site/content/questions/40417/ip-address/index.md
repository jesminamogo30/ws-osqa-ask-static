+++
type = "question"
title = "ip address"
description = '''is there any way to capture the Ip address of a friend whenever we are chatting through viber? I&#x27;ve browse web but it&#x27;s quiet complex !!! please help me'''
date = "2015-03-10T03:18:00Z"
lastmod = "2015-03-10T09:13:00Z"
weight = 40417
keywords = [ "capture" ]
aliases = [ "/questions/40417" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ip address](/questions/40417/ip-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40417-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40417-score" class="post-score" title="current number of votes">0</div><span id="post-40417-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is there any way to capture the Ip address of a friend whenever we are chatting through viber? I've browse web but it's quiet complex !!! please help me</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Mar '15, 03:18</strong></p><img src="https://secure.gravatar.com/avatar/56af1139d9b437ab6f9d41ed85681712?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gm%20Lecter&#39;s gravatar image" /><p><span>Gm Lecter</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gm Lecter has no accepted answers">0%</span></p></div></div><div id="comments-container-40417" class="comments-container"></div><div id="comment-tools-40417" class="comment-tools"></div><div class="clear"></div><div id="comment-40417-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40419"></span>

<div id="answer-container-40419" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40419-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40419-score" class="post-score" title="current number of votes">0</div><span id="post-40419-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's a question not really related to Wireshark, more to the way how Viber works and if they disclose the partner IP address within their app and/or protocol or by using p2p communication.</p><p>You'll (probably) find the answer here:</p><blockquote><p><a href="https://www.google.com/?q=viber+find+ip+address">https://www.google.com/?q=viber+find+ip+address</a><br />
<a href="https://www.google.com/?q=viber+track+ip+address">https://www.google.com/?q=viber+track+ip+address</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Mar '15, 04:29</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-40419" class="comments-container"><span id="40421"></span><div id="comment-40421" class="comment"><div id="post-40421-score" class="comment-score"></div><div class="comment-text"><p>I thought I can capture the IP address using wireshark !!<br />
</p></div><div id="comment-40421-info" class="comment-info"><span class="comment-age">(10 Mar '15, 05:41)</span> <span class="comment-user userinfo">Gm Lecter</span></div></div><span id="40431"></span><div id="comment-40431" class="comment"><div id="post-40431-score" class="comment-score"></div><div class="comment-text"><p>You can capture packets with Wireshark, which may contain IP addresses, which may be related to nodes involved in the network connection between you and a third party. However the question if you can see the actual IP address to the third party is dependent on the architecture of the service.</p></div><div id="comment-40431-info" class="comment-info"><span class="comment-age">(10 Mar '15, 09:13)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-40419" class="comment-tools"></div><div class="clear"></div><div id="comment-40419-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

