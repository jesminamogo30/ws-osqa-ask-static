+++
type = "question"
title = "Not save pipe settings"
description = '''I want to at a pipe to the interface list. Only when I add it (at Manage Interfaces/pipes), it isn&#x27;t stored. A remark at the page &quot;This version of Wireshark does not save pipe settings&quot; Why isn&#x27;t it stored in this version? (Version 2.4.1 (v2.4.1-0-gf42a0d2b6c)) Which version do I have to use, to be ...'''
date = "2017-10-09T04:13:00Z"
lastmod = "2017-10-09T04:13:00Z"
weight = 63756
keywords = [ "pipe", "settings" ]
aliases = [ "/questions/63756" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Not save pipe settings](/questions/63756/not-save-pipe-settings)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63756-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63756-score" class="post-score" title="current number of votes">0</div><span id="post-63756-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to at a pipe to the interface list. Only when I add it (at Manage Interfaces/pipes), it isn't stored. A remark at the page "This version of Wireshark does not save pipe settings"</p><p>Why isn't it stored in this version? (Version 2.4.1 (v2.4.1-0-gf42a0d2b6c)) Which version do I have to use, to be able to store those settings?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pipe" rel="tag" title="see questions tagged &#39;pipe&#39;">pipe</span> <span class="post-tag tag-link-settings" rel="tag" title="see questions tagged &#39;settings&#39;">settings</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '17, 04:13</strong></p><img src="https://secure.gravatar.com/avatar/3f6281d186fc6394b82137b017deceff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Arjan&#39;s gravatar image" /><p><span>Arjan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Arjan has no accepted answers">0%</span></p></div></div><div id="comments-container-63756" class="comments-container"></div><div id="comment-tools-63756" class="comment-tools"></div><div class="clear"></div><div id="comment-63756-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

