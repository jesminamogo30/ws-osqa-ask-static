+++
type = "question"
title = "Wireshark doesn&#x27;t show any network devices on my Windows 10 machine"
description = '''Hello all, I am having problem in establishing the connection between my laptop and external device.can you please suggest me to do the necessary steps exactly to establish the connection..?? also to decode the hex format data.. thanks in advance '''
date = "2015-08-19T04:32:00Z"
lastmod = "2015-08-20T22:17:00Z"
weight = 45236
keywords = [ "ethernet", "network", "windows10" ]
aliases = [ "/questions/45236" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark doesn't show any network devices on my Windows 10 machine](/questions/45236/wireshark-doesnt-show-any-network-devices-on-my-windows-10-machine)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45236-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45236-score" class="post-score" title="current number of votes">0</div><span id="post-45236-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all, I am having problem in establishing the connection between my laptop and external device.can you please suggest me to do the necessary steps exactly to establish the connection..?? also to decode the hex format data..</p><p>thanks in advance<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-windows10" rel="tag" title="see questions tagged &#39;windows10&#39;">windows10</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Aug '15, 04:32</strong></p><img src="https://secure.gravatar.com/avatar/e6e23e8aee92ade408c126f75d209d3f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nandish%20Rao&#39;s gravatar image" /><p><span>Nandish Rao</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nandish Rao has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Aug '15, 00:50</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-45236" class="comments-container"><span id="45251"></span><div id="comment-45251" class="comment"><div id="post-45251-score" class="comment-score"></div><div class="comment-text"><blockquote><p>suggest me to do the necessary steps exactly to establish the connection..??</p></blockquote><p>No, I can't, because you gave no information at all about the nature of that "connection" neither what the error message was, etc. Please add <strong>MUCH</strong> more details!</p><p>BTW: Is this a question related to Wireshark?</p><p>Regards<br />
Kurt</p></div><div id="comment-45251-info" class="comment-info"><span class="comment-age">(19 Aug '15, 13:55)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="45256"></span><div id="comment-45256" class="comment"><div id="post-45256-score" class="comment-score"></div><div class="comment-text"><p><span>@Kurt Knochner</span> * I have used a LAN cable to connect between my laptop and external device(here it is fetal monitor which is being used) * As far as I have seen in the wireshark document it says that an interface will be listed in the interfaces but Iam unable to see there as well * I have changed the necessary settings for LAN * I have got few data samples(mainly hex values) which is from the device and unable to decode those hex values how to decode that data sample??(it was not from my laptop though)</p></div><div id="comment-45256-info" class="comment-info"><span class="comment-age">(19 Aug '15, 21:14)</span> <span class="comment-user userinfo">Nandish Rao</span></div></div><span id="45257"></span><div id="comment-45257" class="comment"><div id="post-45257-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I have used a LAN cable to connect between my laptop and external device(here it is fetal monitor which is being used) * As far as I have seen in the wireshark document it says that an interface will be listed in the interfaces but Iam unable to see there as well</p></blockquote><p>I assume by "LAN" you mean "Ethernet LAN".</p><p>The "interface" that will be listed is the Ethernet interface on your laptop. What interfaces does Wireshark list?</p></div><div id="comment-45257-info" class="comment-info"><span class="comment-age">(19 Aug '15, 21:50)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="45258"></span><div id="comment-45258" class="comment"><div id="post-45258-score" class="comment-score"></div><div class="comment-text"><p><span>@Guy Harris</span> yeah its Ethernet LAN..and coming to the interface listing,when we open wireshark in the home page we can see the interfaces that are connected to the system.But I am unable to see that</p></div><div id="comment-45258-info" class="comment-info"><span class="comment-age">(19 Aug '15, 22:02)</span> <span class="comment-user userinfo">Nandish Rao</span></div></div><span id="45275"></span><div id="comment-45275" class="comment"><div id="post-45275-score" class="comment-score"></div><div class="comment-text"><blockquote><p>when we open wireshark in the home page we can see the interfaces that are connected to the system.But I am unable to see that</p></blockquote><p>So what interfaces, if any, <em>are</em> you seeing? And what version of what operating system is running on your laptop?</p></div><div id="comment-45275-info" class="comment-info"><span class="comment-age">(20 Aug '15, 10:34)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-45236" class="comment-tools"></div><div class="clear"></div><div id="comment-45236-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45285"></span>

<div id="answer-container-45285" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45285-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45285-score" class="post-score" title="current number of votes">0</div><span id="post-45285-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><span><span>@Guy Harris</span></span> I have resolved the issue before getting into what I did, regards from my side for your precious time.I am very much thankful for your concern. So here is the thing what I did;</p><ul><li><p>First of all we need to install win10pcap for receiving the data packets from the external device.</p></li><li><p>The reason behind this is that normal winpcap has NDIS 5.X so by using this one we cannot capture the data packets from the external device in windows 10 OS.</p></li><li><p>Win10pcap comes with NDIS 6.x which is supported by windows 10 OS</p></li><li><p>Once we are done with this installation process then we need to make the necessary changes for Ethernet LAN connection i.e. changing the proxy and all those stuff.</p></li><li><p>Coming to the interface lists that will appear,now its working perfectly alright now. I can able to see the Ethernet connection and this is important because whenever we use wireshark we have this constraint to select a device from which we need to capture the data packets without which wireshark will give a pop up telling that we need to select a device(external device)</p></li></ul><p>Thanks and Regards Nandish Rao</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Aug '15, 22:17</strong></p><img src="https://secure.gravatar.com/avatar/e6e23e8aee92ade408c126f75d209d3f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nandish%20Rao&#39;s gravatar image" /><p><span>Nandish Rao</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nandish Rao has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Aug '15, 22:20</strong> </span></p></div></div><div id="comments-container-45285" class="comments-container"></div><div id="comment-tools-45285" class="comment-tools"></div><div class="clear"></div><div id="comment-45285-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

