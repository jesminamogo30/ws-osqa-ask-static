+++
type = "question"
title = "How to decode a packet received through wireshark &amp; resolving some errors"
description = '''We are capturing traffic using JN5148EK010 nodes via Wireshark. The packets received are shown in the screenshot provided.   I want to know how to decode the data? An error occurs after capturing a few packets, whose screenshot is also provided. How to resolve this error? Another error (refer to thi...'''
date = "2013-02-15T00:52:00Z"
lastmod = "2013-02-15T02:37:00Z"
weight = 18649
keywords = [ "802.15.4", "packets", "wireshark" ]
aliases = [ "/questions/18649" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to decode a packet received through wireshark & resolving some errors](/questions/18649/how-to-decode-a-packet-received-through-wireshark-resolving-some-errors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18649-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18649-score" class="post-score" title="current number of votes">0</div><span id="post-18649-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>We are capturing traffic using <a href="http://www.jennic.com/support/user_guides/jn-ug-3062_jn5148-ek010_user_guide">JN5148EK010</a> nodes via Wireshark. The packets received are shown in the screenshot provided.</p><ol><li>I want to know how to decode the data?</li><li>An error occurs after capturing a few packets, whose screenshot is also provided. How to resolve this error?</li><li>Another error (refer to third screenshot) occurred. How to resolve that?</li></ol><p>Kindly help.</p><p>Please check the screenshots on the following link: <a href="http://stackoverflow.com/questions/14890872/how-to-decode-a-packet-received-through-wireshark-resolving-some-errors">http://stackoverflow.com/questions/14890872/how-to-decode-a-packet-received-through-wireshark-resolving-some-errors</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.15.4" rel="tag" title="see questions tagged &#39;802.15.4&#39;">802.15.4</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '13, 00:52</strong></p><img src="https://secure.gravatar.com/avatar/a626dd40716dc7336f3934d5ba42eafb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hamra%20Rehan&#39;s gravatar image" /><p><span>Hamra Rehan</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hamra Rehan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Feb '13, 05:42</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-18649" class="comments-container"><span id="18650"></span><div id="comment-18650" class="comment"><div id="post-18650-score" class="comment-score"></div><div class="comment-text"><p>Assisting analysis via screenshots is very frustrating. Please save a capture, containing just the 802.15.4 packets, using some form of file sharing service. <a href="http://cloudshark.org">Cloudshark</a> is a good place. Make sure the data is OK to share first though.</p></div><div id="comment-18650-info" class="comment-info"><span class="comment-age">(15 Feb '13, 02:37)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-18649" class="comment-tools"></div><div class="clear"></div><div id="comment-18649-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

