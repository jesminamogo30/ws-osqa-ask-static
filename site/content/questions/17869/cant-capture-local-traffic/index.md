+++
type = "question"
title = "Can&#x27;t capture local traffic"
description = '''I&#x27;ve been using Wireshark for a long time with no problem, but can&#x27;t get it to work right with this new server. I used to be able to see all my internal traffic. If I pinged eth0 or a TAP, I could see the ping. I can&#x27;t anymore. The ping works, but Wireshark isn&#x27;t capturing it. This is making trouble...'''
date = "2013-01-22T13:52:00Z"
lastmod = "2013-01-22T14:50:00Z"
weight = 17869
keywords = [ "internal", "local" ]
aliases = [ "/questions/17869" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't capture local traffic](/questions/17869/cant-capture-local-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17869-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17869-score" class="post-score" title="current number of votes">0</div><span id="post-17869-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've been using Wireshark for a long time with no problem, but can't get it to work right with this new server. I used to be able to see all my internal traffic. If I pinged eth0 or a TAP, I could see the ping. I can't anymore. The ping works, but Wireshark isn't capturing it. This is making troubleshooting other problems difficult. What am I doing wrong?</p><p>I'm using CentOS 5.8</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-internal" rel="tag" title="see questions tagged &#39;internal&#39;">internal</span> <span class="post-tag tag-link-local" rel="tag" title="see questions tagged &#39;local&#39;">local</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jan '13, 13:52</strong></p><img src="https://secure.gravatar.com/avatar/d23ce672e40ff2e033a8c7a42f223792?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mbenson&#39;s gravatar image" /><p><span>mbenson</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mbenson has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Jan '13, 13:59</strong> </span></p></div></div><div id="comments-container-17869" class="comments-container"><span id="17871"></span><div id="comment-17871" class="comment"><div id="post-17871-score" class="comment-score"></div><div class="comment-text"><p>By "internal traffic" do you mean that the machine has multiple virtual machines on it, and the "internal traffic" is traffic between virtual machines on the same host, or do you mean traffic from a given host or guest to the <em>same</em> host or guest?</p></div><div id="comment-17871-info" class="comment-info"><span class="comment-age">(22 Jan '13, 14:50)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-17869" class="comment-tools"></div><div class="clear"></div><div id="comment-17869-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

