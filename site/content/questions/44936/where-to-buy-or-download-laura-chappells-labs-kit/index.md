+++
type = "question"
title = "Where to buy or download laura Chappell&#x27;s   labs kit?"
description = '''I have searched all over the net torrents, filesharing sites nearly 99% of links are dead. I&#x27;m not interested in few MB worth of DVD , infact I want master collection worth GBs There must be someway I can get this stuff.Please help.'''
date = "2015-08-09T01:08:00Z"
lastmod = "2015-08-11T11:02:00Z"
weight = 44936
keywords = [ "download" ]
aliases = [ "/questions/44936" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Where to buy or download laura Chappell's labs kit?](/questions/44936/where-to-buy-or-download-laura-chappells-labs-kit)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44936-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44936-score" class="post-score" title="current number of votes">0</div><span id="post-44936-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have searched all over the net torrents, filesharing sites nearly 99% of links are dead.</p><p>I'm not interested in few MB worth of DVD , infact I want master collection worth GBs</p><p>There must be someway I can get this stuff.Please help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Aug '15, 01:08</strong></p><img src="https://secure.gravatar.com/avatar/5c252f43b9f910c1176157be9168cba2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="asad&#39;s gravatar image" /><p><span>asad</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="asad has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Aug '15, 07:14</strong> </span></p></div></div><div id="comments-container-44936" class="comments-container"></div><div id="comment-tools-44936" class="comment-tools"></div><div class="clear"></div><div id="comment-44936-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="44937"></span>

<div id="answer-container-44937" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44937-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44937-score" class="post-score" title="current number of votes">2</div><span id="post-44937-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Presuming you actually meant Laura Chappell's courses, You can purchase from the <a href="http://wiresharktraining.com/index.html">Wireshark University</a> site.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Aug '15, 05:15</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-44937" class="comments-container"></div><div id="comment-tools-44937" class="comment-tools"></div><div class="clear"></div><div id="comment-44937-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="44965"></span>

<div id="answer-container-44965" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44965-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44965-score" class="post-score" title="current number of votes">0</div><span id="post-44965-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Laura's Lab Kit (version 11) is available from <a href="http://www.riverbed.com/products/performance-management-control/network-performance-management/wireshark-world-tour.html#Download_Lab_Kit">this</a> link, hosted by Riverbed.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Aug '15, 11:02</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-44965" class="comments-container"></div><div id="comment-tools-44965" class="comment-tools"></div><div class="clear"></div><div id="comment-44965-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

