+++
type = "question"
title = "Decoder for BT IUP"
description = '''I am currently adding the legacy BT IUP protocol (ND1006:2007/05 and ND1104:2004/11) to our switch. I realize that this is an older protocol but it is still prevalent in the U.K. I use Wireshark for other protocol developments and wondered if you have a plug-in for this protocol.'''
date = "2013-10-10T06:49:00Z"
lastmod = "2013-10-10T11:23:00Z"
weight = 25877
keywords = [ "bt", "iup" ]
aliases = [ "/questions/25877" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Decoder for BT IUP](/questions/25877/decoder-for-bt-iup)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25877-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25877-score" class="post-score" title="current number of votes">0</div><span id="post-25877-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am currently adding the legacy BT IUP protocol (ND1006:2007/05 and ND1104:2004/11) to our switch. I realize that this is an older protocol but it is still prevalent in the U.K. I use Wireshark for other protocol developments and wondered if you have a plug-in for this protocol.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bt" rel="tag" title="see questions tagged &#39;bt&#39;">bt</span> <span class="post-tag tag-link-iup" rel="tag" title="see questions tagged &#39;iup&#39;">iup</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Oct '13, 06:49</strong></p><img src="https://secure.gravatar.com/avatar/fe95833133c89ad4320497019f3c1f5d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="britdave&#39;s gravatar image" /><p><span>britdave</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="britdave has no accepted answers">0%</span></p></div></div><div id="comments-container-25877" class="comments-container"></div><div id="comment-tools-25877" class="comment-tools"></div><div class="clear"></div><div id="comment-25877-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25890"></span>

<div id="answer-container-25890" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25890-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25890-score" class="post-score" title="current number of votes">0</div><span id="post-25890-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sorry, no, no one has ever written a dissector for IUP for Wireshark (or if they did they have not submitted it for inclusion in Wireshark).</p><p>If you're a programmer type you could always take on the project yourself--writing a basic dissector isn't that hard.</p><p>If you're not you could always <a href="https://bugs.wireshark.org">open an enhancement</a> request with some sample captures and links to the relevant specifications in case someone wants to take the project on. (Note that both a sample capture and the specs would be needed.) Though there are a lot of similar enhancement requests sitting out there (having not moved in years) so I'm not sure how much action yours would see...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Oct '13, 11:23</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-25890" class="comments-container"></div><div id="comment-tools-25890" class="comment-tools"></div><div class="clear"></div><div id="comment-25890-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

