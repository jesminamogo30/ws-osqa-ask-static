+++
type = "question"
title = "Airplay why is it so flakey?  anyone have a packet capture?"
description = '''Before I bust out my switch capable of doing spans, does anyone have packet captures of IPAD/IPHONE/etc. connecting to receivers for airplay? I&#x27;m thinking it uses some type of multicast to find each other, but I wasn&#x27;t sure. And not being tied to the Apple ecosystem, too lazy to go ready docs. If an...'''
date = "2013-10-12T07:14:00Z"
lastmod = "2013-10-12T21:40:00Z"
weight = 25934
keywords = [ "airplay" ]
aliases = [ "/questions/25934" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Airplay why is it so flakey? anyone have a packet capture?](/questions/25934/airplay-why-is-it-so-flakey-anyone-have-a-packet-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25934-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25934-score" class="post-score" title="current number of votes">0</div><span id="post-25934-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Before I bust out my switch capable of doing spans, does anyone have packet captures of IPAD/IPHONE/etc. connecting to receivers for airplay? I'm thinking it uses some type of multicast to find each other, but I wasn't sure. And not being tied to the Apple ecosystem, too lazy to go ready docs.</p><p>If anyone has such a trace, I'd love to take a look. If not, I'll drag up my switch and plug it in on my Denon receiver.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-airplay" rel="tag" title="see questions tagged &#39;airplay&#39;">airplay</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Oct '13, 07:14</strong></p><img src="https://secure.gravatar.com/avatar/63805f079ac429902641cad9d7cd69e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hansangb&#39;s gravatar image" /><p><span>hansangb</span><br />
<span class="score" title="791 reputation points">791</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hansangb has 7 accepted answers">12%</span></p></div></div><div id="comments-container-25934" class="comments-container"><span id="25944"></span><div id="comment-25944" class="comment"><div id="post-25944-score" class="comment-score"></div><div class="comment-text"><p>I believe to have read that AirPlay uses Bonjour and thus mDNS for service discovery. Unfortunately I cannot find that article right now and I don't have a pcap at hand.</p><p>Regards<br />
Kurt</p></div><div id="comment-25944-info" class="comment-info"><span class="comment-age">(12 Oct '13, 16:26)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="25945"></span><div id="comment-25945" class="comment"><div id="post-25945-score" class="comment-score"></div><div class="comment-text"><p>No Problemo. I'll get my switch out tomorrow and see why the IPads are not connecting to the Denon receivers (but sometimes, it works)</p></div><div id="comment-25945-info" class="comment-info"><span class="comment-age">(12 Oct '13, 21:40)</span> <span class="comment-user userinfo">hansangb</span></div></div></div><div id="comment-tools-25934" class="comment-tools"></div><div class="clear"></div><div id="comment-25934-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

