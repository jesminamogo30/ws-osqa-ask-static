+++
type = "question"
title = "Filter box not working on Qt Wireshark on OS X"
description = '''on osx using qt installed from homebrew the filter box doesn&#x27;t work, and also the menu items to bring up capture and display filters don&#x27;t open new windows... so it is pretty much not workable.'''
date = "2014-06-05T14:37:00Z"
lastmod = "2014-06-05T15:40:00Z"
weight = 33478
keywords = [ "osx", "qtshark" ]
aliases = [ "/questions/33478" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Filter box not working on Qt Wireshark on OS X](/questions/33478/filter-box-not-working-on-qt-wireshark-on-os-x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33478-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33478-score" class="post-score" title="current number of votes">0</div><span id="post-33478-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>on osx using qt installed from homebrew the filter box doesn't work, and also the menu items to bring up capture and display filters don't open new windows... so it is pretty much not workable.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-qtshark" rel="tag" title="see questions tagged &#39;qtshark&#39;">qtshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jun '14, 14:37</strong></p><img src="https://secure.gravatar.com/avatar/f7326fd554e09edf21ca727d6854e22e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gradyplayer&#39;s gravatar image" /><p><span>gradyplayer</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gradyplayer has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted <strong>05 Jun '14, 14:47</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-33478" class="comments-container"></div><div id="comment-tools-33478" class="comment-tools"></div><div class="clear"></div><div id="comment-33478-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="33479"></span>

<div id="answer-container-33479" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33479-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33479-score" class="post-score" title="current number of votes">0</div><span id="post-33479-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The Qt version of Wireshark is a work in progress, and is not guaranteed to have full functionality. Please file bugs on <a href="http://bugs.wireshark.org/">the Wireshark bugzilla</a> for any issues you encounter with it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jun '14, 14:50</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-33479" class="comments-container"><span id="33484"></span><div id="comment-33484" class="comment"><div id="post-33484-score" class="comment-score"></div><div class="comment-text"><p>excellent... would they prefer one bug with all the ui elements that don't work, or individual tickets?</p></div><div id="comment-33484-info" class="comment-info"><span class="comment-age">(05 Jun '14, 15:38)</span> <span class="comment-user userinfo">gradyplayer</span></div></div><span id="33485"></span><div id="comment-33485" class="comment"><div id="post-33485-score" class="comment-score"></div><div class="comment-text"><p>I'd go for individual tickets, as there are usually different bits of code that would need to be written for different issues. If two bugs end up having the same root cause, one can be dup'ed to the other.</p></div><div id="comment-33485-info" class="comment-info"><span class="comment-age">(05 Jun '14, 15:40)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-33479" class="comment-tools"></div><div class="clear"></div><div id="comment-33479-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

