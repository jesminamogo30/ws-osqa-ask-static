+++
type = "question"
title = "Wireshark  support"
description = '''we are migrating following applications to server 2008r2 and server 2012r2 and would like to know their compatibility over aforementioned platforms.  Wireshark 1.8.6 (32-bit) Wireshark 1.8.3 (32-bit) Wireshark 1.6.7 (32-bit)  response from developers would be very much appreciated'''
date = "2016-01-18T23:44:00Z"
lastmod = "2016-01-19T04:37:00Z"
weight = 49356
keywords = [ "support", "wireshark" ]
aliases = [ "/questions/49356" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark support](/questions/49356/wireshark-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49356-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49356-score" class="post-score" title="current number of votes">0</div><span id="post-49356-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>we are migrating following applications to server 2008r2 and server 2012r2 and would like to know their compatibility over aforementioned platforms.</p><ul><li>Wireshark 1.8.6 (32-bit)</li><li>Wireshark 1.8.3 (32-bit)</li><li>Wireshark 1.6.7 (32-bit)</li></ul><p>response from developers would be very much appreciated</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-support" rel="tag" title="see questions tagged &#39;support&#39;">support</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '16, 23:44</strong></p><img src="https://secure.gravatar.com/avatar/db10a9feeb8ccf574d0082a637de67fe?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prem_ashutosh&#39;s gravatar image" /><p><span>prem_ashutosh</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prem_ashutosh has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jan '16, 01:01</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-49356" class="comments-container"></div><div id="comment-tools-49356" class="comment-tools"></div><div class="clear"></div><div id="comment-49356-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49360"></span>

<div id="answer-container-49360" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49360-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49360-score" class="post-score" title="current number of votes">0</div><span id="post-49360-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would recommend looking at the <a href="https://wiki.wireshark.org/Development/LifeCycle">lifecycle page</a>. This states that support for these versions has ceased some time ago. It also indicates some platform support information. From the looks of it you should be oke.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jan '16, 01:04</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-49360" class="comments-container"><span id="49361"></span><div id="comment-49361" class="comment"><div id="post-49361-score" class="comment-score"></div><div class="comment-text"><p>Ok, so just to be on the safer side, will ethereal (now called wireshark) versioned 0.99 and lower be supported on Server 2008R2 and Server 2012R2</p></div><div id="comment-49361-info" class="comment-info"><span class="comment-age">(19 Jan '16, 01:11)</span> <span class="comment-user userinfo">prem_ashutosh</span></div></div><span id="49363"></span><div id="comment-49363" class="comment"><div id="post-49363-score" class="comment-score"></div><div class="comment-text"><p>If it works, then great, if it doesn't, I'm afraid that's it unless you wish to work on the issues yourselves (or pay someone to do so) as that version is obsolete.</p><p>Why do you want to run such old versions? If it's because you have plugins compiled for these versions you'll be much better served by updating the plugins to run in supported versions.</p></div><div id="comment-49363-info" class="comment-info"><span class="comment-age">(19 Jan '16, 02:27)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="49366"></span><div id="comment-49366" class="comment"><div id="post-49366-score" class="comment-score"></div><div class="comment-text"><p><span>@prem_ashutosh</span>: on the contrary. Since there is no mentioning of ceased support for these Windows versions even newer versions would work. And probably work better, with more problems fixed and features added.</p></div><div id="comment-49366-info" class="comment-info"><span class="comment-age">(19 Jan '16, 04:28)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="49367"></span><div id="comment-49367" class="comment"><div id="post-49367-score" class="comment-score"></div><div class="comment-text"><p><span>@grahamb</span> <span>@Jaap</span> : we are migrating some of our apps from server 2003 to server 2008 R2 and Server 2012 R2 platforms, hence the need to check the compatibility of such old apps</p></div><div id="comment-49367-info" class="comment-info"><span class="comment-age">(19 Jan '16, 04:37)</span> <span class="comment-user userinfo">prem_ashutosh</span></div></div></div><div id="comment-tools-49360" class="comment-tools"></div><div class="clear"></div><div id="comment-49360-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

