+++
type = "question"
title = "Couldn&#x27;t see sflow traffic sent from windows server 8(beta) box."
description = '''Hi, I was using wireshark 1.6.7&amp;amp;previous stable release trying to catch sflow data sent from my windows server 8(beta) to my windows 2008 R2 box. Using other 2 tools(netmon and sflowTrend) I could see sflow data sent to my monitoring box, but I couldn&#x27;t see it in wireshark. Actually I could see ...'''
date = "2012-04-25T11:11:00Z"
lastmod = "2012-04-25T17:17:00Z"
weight = 10443
keywords = [ "sflow" ]
aliases = [ "/questions/10443" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Couldn't see sflow traffic sent from windows server 8(beta) box.](/questions/10443/couldnt-see-sflow-traffic-sent-from-windows-server-8beta-box)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10443-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10443-score" class="post-score" title="current number of votes">0</div><span id="post-10443-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I was using wireshark 1.6.7&amp;previous stable release trying to catch sflow data sent from my windows server 8(beta) to my windows 2008 R2 box. Using other 2 tools(netmon and sflowTrend) I could see sflow data sent to my monitoring box, but I couldn't see it in wireshark. Actually I could see it once but couldn't no longer and I didn't seem to make any change to the tool. I spent hours trying to change setting of the tool but still couldn't see any sflow traffic. Could anyone help me?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sflow" rel="tag" title="see questions tagged &#39;sflow&#39;">sflow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Apr '12, 11:11</strong></p><img src="https://secure.gravatar.com/avatar/0ebf99a13c8651978ac1ee93639ea77b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="happydogcat&#39;s gravatar image" /><p><span>happydogcat</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="happydogcat has no accepted answers">0%</span></p></div></div><div id="comments-container-10443" class="comments-container"></div><div id="comment-tools-10443" class="comment-tools"></div><div class="clear"></div><div id="comment-10443-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10452"></span>

<div id="answer-container-10452" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10452-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10452-score" class="post-score" title="current number of votes">0</div><span id="post-10452-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi,</p><p>would you mind to post (or upload) some sample packets?</p><p>Regards Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Apr '12, 17:17</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span></p></div></div><div id="comments-container-10452" class="comments-container"></div><div id="comment-tools-10452" class="comment-tools"></div><div class="clear"></div><div id="comment-10452-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

