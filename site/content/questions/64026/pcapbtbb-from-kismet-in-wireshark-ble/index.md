+++
type = "question"
title = "pcapbtbb from kismet in wireshark, BLE"
description = '''Hello, I am generating a pcapbtbb file to read in wireshark. I keep reading that newer versions of wireshark do not need a plugin however when I open the file it still reads it as Ethernet as if there is no plugin. When I go to Help-&amp;gt;About Wireshark-&amp;gt;plugins , I don&#x27;t see a btbb plugin listed,...'''
date = "2017-10-19T07:00:00Z"
lastmod = "2017-10-19T07:00:00Z"
weight = 64026
keywords = [ "ble", "sniffing", "pcapbtbb", "bluetooth" ]
aliases = [ "/questions/64026" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [pcapbtbb from kismet in wireshark, BLE](/questions/64026/pcapbtbb-from-kismet-in-wireshark-ble)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64026-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64026-score" class="post-score" title="current number of votes">0</div><span id="post-64026-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I am generating a pcapbtbb file to read in wireshark. I keep reading that newer versions of wireshark do not need a plugin however when I open the file it still reads it as Ethernet as if there is no plugin. When I go to Help-&gt;About Wireshark-&gt;plugins , I don't see a btbb plugin listed, should there be one? My wireshark version is 2.4.2. Any thoughts or ideas are greatly appreciated. I'm looking at ble devices.</p><p>Thanks</p><p>*I tried Edit-&gt;preferences-&gt;protocols-&gt;DLT User...</p><p><a href="https://wiki.wireshark.org/CaptureSetup/Bluetooth">https://wiki.wireshark.org/CaptureSetup/Bluetooth</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ble" rel="tag" title="see questions tagged &#39;ble&#39;">ble</span> <span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-pcapbtbb" rel="tag" title="see questions tagged &#39;pcapbtbb&#39;">pcapbtbb</span> <span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Oct '17, 07:00</strong></p><img src="https://secure.gravatar.com/avatar/f40e6cc685f5a048cc2dbfd244bb9807?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wahba1995&#39;s gravatar image" /><p><span>wahba1995</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wahba1995 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Oct '17, 07:23</strong> </span></p></div></div><div id="comments-container-64026" class="comments-container"></div><div id="comment-tools-64026" class="comment-tools"></div><div class="clear"></div><div id="comment-64026-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

