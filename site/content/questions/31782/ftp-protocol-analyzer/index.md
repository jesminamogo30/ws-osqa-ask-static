+++
type = "question"
title = "FTP Protocol Analyzer"
description = '''Dear Sir, I&#x27;m a new to Wire-shark and i need to analyzer the FTP packet only, I used your Wire-shark program and i chose a TCP only in the filtering but when i enabled the TCP only i received all the TCP, So i need to filter the FTP data only Could you help me. Best regards Mohamed Hamed'''
date = "2014-04-14T02:49:00Z"
lastmod = "2014-04-15T06:51:00Z"
weight = 31782
keywords = [ "ftp" ]
aliases = [ "/questions/31782" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [FTP Protocol Analyzer](/questions/31782/ftp-protocol-analyzer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31782-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31782-score" class="post-score" title="current number of votes">0</div><span id="post-31782-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Sir, I'm a new to Wire-shark and i need to analyzer the FTP packet only, I used your Wire-shark program and i chose a TCP only in the filtering but when i enabled the TCP only i received all the TCP, So i need to filter the FTP data only</p><p>Could you help me.</p><p>Best regards Mohamed Hamed</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Apr '14, 02:49</strong></p><img src="https://secure.gravatar.com/avatar/43713de63584bb4ae789bd3abd841342?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ambitious&#39;s gravatar image" /><p><span>ambitious</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ambitious has no accepted answers">0%</span></p></div></div><div id="comments-container-31782" class="comments-container"></div><div id="comment-tools-31782" class="comment-tools"></div><div class="clear"></div><div id="comment-31782-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31783"></span>

<div id="answer-container-31783" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31783-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31783-score" class="post-score" title="current number of votes">0</div><span id="post-31783-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try filtering on "ftp or ftp-data".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Apr '14, 02:57</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-31783" class="comments-container"><span id="31785"></span><div id="comment-31785" class="comment"><div id="post-31785-score" class="comment-score"></div><div class="comment-text"><p>where can i find this filter?</p></div><div id="comment-31785-info" class="comment-info"><span class="comment-age">(14 Apr '14, 04:26)</span> <span class="comment-user userinfo">ambitious</span></div></div><span id="31786"></span><div id="comment-31786" class="comment"><div id="post-31786-score" class="comment-score"></div><div class="comment-text"><p>I guess you mean where to enter it? Try the filter input field below the toolbar buttons.</p><p>If you want a full list of filters try the expression builder.</p></div><div id="comment-31786-info" class="comment-info"><span class="comment-age">(14 Apr '14, 04:37)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="31823"></span><div id="comment-31823" class="comment"><div id="post-31823-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jasper for your great support I found it and i choose this filter, but thier is no data appeared when i chose either FTP data nor FTP</p><p>Note: I make a local FTP server and client @ address 127.0.0.1 using fileZilla server and client @ the same PC</p></div><div id="comment-31823-info" class="comment-info"><span class="comment-age">(15 Apr '14, 00:52)</span> <span class="comment-user userinfo">ambitious</span></div></div><span id="31824"></span><div id="comment-31824" class="comment"><div id="post-31824-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I make a local FTP server and client @ address 127.0.0.1</p></blockquote><p><span>@ambitious</span>: that does not work, as Wireshark on Windows (actually the WinPcap library) cannot capture localhost traffic.</p><p>Please use <a href="http://www.netresec.com/?page=RawCap">RawCap</a> instead.</p><p>BTW: For further questions: Please add as much detail as possible. We could have told you the problem with 127.0.0.1 in the first place, if you had mentioned it.</p></div><div id="comment-31824-info" class="comment-info"><span class="comment-age">(15 Apr '14, 01:18)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="31840"></span><div id="comment-31840" class="comment"><div id="post-31840-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jasper</p></div><div id="comment-31840-info" class="comment-info"><span class="comment-age">(15 Apr '14, 06:51)</span> <span class="comment-user userinfo">ambitious</span></div></div></div><div id="comment-tools-31783" class="comment-tools"></div><div class="clear"></div><div id="comment-31783-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

