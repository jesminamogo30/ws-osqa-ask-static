+++
type = "question"
title = "Where can I get the filter expressions, like ip.addr, in Developer mode?"
description = '''Where can I get the filter expressions, like ip.addr==x.x.x.x, in Developer mode? The stable mode of WS has got an &quot;Expression&quot; tab, and also Display Filter. Display filter is grayed out in dev mode. Any other options for this expression? '''
date = "2015-02-18T02:40:00Z"
lastmod = "2015-02-18T06:14:00Z"
weight = 39925
keywords = [ "expression" ]
aliases = [ "/questions/39925" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Where can I get the filter expressions, like ip.addr, in Developer mode?](/questions/39925/where-can-i-get-the-filter-expressions-like-ipaddr-in-developer-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39925-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39925-score" class="post-score" title="current number of votes">0</div><span id="post-39925-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Where can I get the filter expressions, like ip.addr==x.x.x.x, in Developer mode? The stable mode of WS has got an "Expression" tab, and also Display Filter. Display filter is grayed out in dev mode. Any other options for this expression?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-expression" rel="tag" title="see questions tagged &#39;expression&#39;">expression</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '15, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/dcc9c865113e1204e6aef63668fbc376?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="arch&#39;s gravatar image" /><p><span>arch</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="arch has no accepted answers">0%</span></p></div></div><div id="comments-container-39925" class="comments-container"><span id="39929"></span><div id="comment-39929" class="comment"><div id="post-39929-score" class="comment-score"></div><div class="comment-text"><p>Are you referring to the difference between GTK3 and Qt based GUI's?</p></div><div id="comment-39929-info" class="comment-info"><span class="comment-age">(18 Feb '15, 06:14)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-39925" class="comment-tools"></div><div class="clear"></div><div id="comment-39925-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

