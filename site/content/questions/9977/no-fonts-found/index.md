+++
type = "question"
title = "No fonts found"
description = '''Hi, I&#x27;m getting the following error..when i try to run wireshark-2.4.2 on solaris-10. No fonts found; this probably means that the fontconfig library is not correctly configured. You may need to edit the fonts.conf configuration file. More information about fontconfig can be found in the fontconfig(...'''
date = "2012-04-05T23:38:00Z"
lastmod = "2012-04-05T23:38:00Z"
weight = 9977
keywords = [ "runtime", "solaris", "build", "install" ]
aliases = [ "/questions/9977" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No fonts found](/questions/9977/no-fonts-found)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9977-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9977-score" class="post-score" title="current number of votes">0</div><span id="post-9977-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I'm getting the following error..when i try to run wireshark-2.4.2 on solaris-10.</p><p>No fonts found; this probably means that the fontconfig library is not correctly configured. You may need to edit the fonts.conf configuration file. More information about fontconfig can be found in the fontconfig(3) manual page and on <a href="http://fontconfig.org">http://fontconfig.org</a> <a href="http://fontconfig.org/">http://fontconfig.org/</a></p><p>I have installed fontconfig-2.4.2.</p><p>please help...i'm stuck with this issue.</p><p>Thank's in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-runtime" rel="tag" title="see questions tagged &#39;runtime&#39;">runtime</span> <span class="post-tag tag-link-solaris" rel="tag" title="see questions tagged &#39;solaris&#39;">solaris</span> <span class="post-tag tag-link-build" rel="tag" title="see questions tagged &#39;build&#39;">build</span> <span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Apr '12, 23:38</strong></p><img src="https://secure.gravatar.com/avatar/e04d89abdb2799efb4cb4f9d1d6cf495?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ichigo&#39;s gravatar image" /><p><span>Ichigo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ichigo has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Apr '12, 23:47</strong> </span></p></div></div><div id="comments-container-9977" class="comments-container"></div><div id="comment-tools-9977" class="comment-tools"></div><div class="clear"></div><div id="comment-9977-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

