+++
type = "question"
title = "wcnaportal compromised ?"
description = '''Hello, Since a few days now, the website for WCNA certification &quot;https://wcnaportal.com/&quot; is out of service (appears to have been compromised), Does anyone have any update (just want to pass the WCNA certification... :)'''
date = "2016-03-01T04:13:00Z"
lastmod = "2016-03-01T04:57:00Z"
weight = 50602
keywords = [ "wcna", "wcnaportal", "compromised" ]
aliases = [ "/questions/50602" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wcnaportal compromised ?](/questions/50602/wcnaportal-compromised)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50602-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50602-score" class="post-score" title="current number of votes">0</div><span id="post-50602-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Since a few days now, the website for WCNA certification "https://wcnaportal.com/" is out of service (appears to have been compromised), Does anyone have any update (just want to pass the WCNA certification... :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wcna" rel="tag" title="see questions tagged &#39;wcna&#39;">wcna</span> <span class="post-tag tag-link-wcnaportal" rel="tag" title="see questions tagged &#39;wcnaportal&#39;">wcnaportal</span> <span class="post-tag tag-link-compromised" rel="tag" title="see questions tagged &#39;compromised&#39;">compromised</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Mar '16, 04:13</strong></p><img src="https://secure.gravatar.com/avatar/ef7d55f4c2dfc094733932e57ee96e24?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SayaOtanashi&#39;s gravatar image" /><p><span>SayaOtanashi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SayaOtanashi has no accepted answers">0%</span></p></div></div><div id="comments-container-50602" class="comments-container"></div><div id="comment-tools-50602" class="comment-tools"></div><div class="clear"></div><div id="comment-50602-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50603"></span>

<div id="answer-container-50603" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50603-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50603-score" class="post-score" title="current number of votes">0</div><span id="post-50603-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, looks like it is broken/hacked. I notified Laura. Thanks!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Mar '16, 04:57</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-50603" class="comments-container"></div><div id="comment-tools-50603" class="comment-tools"></div><div class="clear"></div><div id="comment-50603-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

