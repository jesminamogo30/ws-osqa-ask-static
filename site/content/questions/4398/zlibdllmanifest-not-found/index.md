+++
type = "question"
title = "zlib.dll.manifest not found."
description = '''hi, i am getting the fallowing error during compilation. &quot;zlib1.dll.manifest : general error c1010070: Failed to load and parse the manife st. The system cannot find the file specified.&quot; does anybody give any idea to solve this. i tried to find the manifest file but its not present it seems.'''
date = "2011-06-06T05:56:00Z"
lastmod = "2011-06-06T07:30:00Z"
weight = 4398
keywords = [ "manifest" ]
aliases = [ "/questions/4398" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [zlib.dll.manifest not found.](/questions/4398/zlibdllmanifest-not-found)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4398-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4398-score" class="post-score" title="current number of votes">0</div><span id="post-4398-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, i am getting the fallowing error during compilation.</p><p>"zlib1.dll.manifest : general error c1010070: Failed to load and parse the manife st. The system cannot find the file specified."</p><p>does anybody give any idea to solve this. i tried to find the manifest file but its not present it seems.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-manifest" rel="tag" title="see questions tagged &#39;manifest&#39;">manifest</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jun '11, 05:56</strong></p><img src="https://secure.gravatar.com/avatar/257c9f9e498193d7ddde57090efe094a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sagu072&#39;s gravatar image" /><p><span>sagu072</span><br />
<span class="score" title="35 reputation points">35</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sagu072 has no accepted answers">0%</span></p></div></div><div id="comments-container-4398" class="comments-container"><span id="4402"></span><div id="comment-4402" class="comment"><div id="post-4402-score" class="comment-score"></div><div class="comment-text"><p>What version of Wireshark are you trying to compile? Which compiler are you using?</p></div><div id="comment-4402-info" class="comment-info"><span class="comment-age">(06 Jun '11, 07:30)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-4398" class="comment-tools"></div><div class="clear"></div><div id="comment-4398-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

