+++
type = "question"
title = "Validate Wireshark, as per US Food and Drug Administration requirements, for small tcp/ip system"
description = '''I am using Wireshark to validate the tcp/ip protocols of a small system. However, I must first validate Wireshark itself for tcp/ip protocols before I can use it to validate another system. Anyone have any simple solutions?'''
date = "2013-12-18T07:59:00Z"
lastmod = "2013-12-18T12:53:00Z"
weight = 28256
keywords = [ "validate", "tcp_ip" ]
aliases = [ "/questions/28256" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Validate Wireshark, as per US Food and Drug Administration requirements, for small tcp/ip system](/questions/28256/validate-wireshark-as-per-us-food-and-drug-administration-requirements-for-small-tcpip-system)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28256-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28256-score" class="post-score" title="current number of votes">0</div><span id="post-28256-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using Wireshark to validate the tcp/ip protocols of a small system. However, I must first validate Wireshark itself for tcp/ip protocols before I can use it to validate another system. Anyone have any simple solutions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-validate" rel="tag" title="see questions tagged &#39;validate&#39;">validate</span> <span class="post-tag tag-link-tcp_ip" rel="tag" title="see questions tagged &#39;tcp_ip&#39;">tcp_ip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Dec '13, 07:59</strong></p><img src="https://secure.gravatar.com/avatar/79b1d33c4c1504f45dc4eee6c9bf02b6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cgoogins&#39;s gravatar image" /><p><span>cgoogins</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cgoogins has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Dec '13, 12:52</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-28256" class="comments-container"><span id="28257"></span><div id="comment-28257" class="comment"><div id="post-28257-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by "validate Wireshark itself"? Do you want to feed it some packets and see if the decode is what it is supposed to be?</p></div><div id="comment-28257-info" class="comment-info"><span class="comment-age">(18 Dec '13, 08:04)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="28258"></span><div id="comment-28258" class="comment"><div id="post-28258-score" class="comment-score"></div><div class="comment-text"><p>Yes. I suppose. (BTW - I have limited experience in this area.)</p></div><div id="comment-28258-info" class="comment-info"><span class="comment-age">(18 Dec '13, 08:08)</span> <span class="comment-user userinfo">cgoogins</span></div></div><span id="28272"></span><div id="comment-28272" class="comment"><div id="post-28272-score" class="comment-score"></div><div class="comment-text"><p>Unfortunately, as Jasper indicated, without knowing what the FDA's definition of what "validation" means, there's not much we can do to help you.</p></div><div id="comment-28272-info" class="comment-info"><span class="comment-age">(18 Dec '13, 12:53)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-28256" class="comment-tools"></div><div class="clear"></div><div id="comment-28256-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28259"></span>

<div id="answer-container-28259" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28259-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28259-score" class="post-score" title="current number of votes">1</div><span id="post-28259-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The only reason to do this I can think of is that you need to have some sort of "chain of custody" in case you find something wrong with the "small system" and then someone challenges your findings by saying "you analyzed it wrong."</p><p>So if you have to you can still double check your findings with other analyzers (most of them commercial), but I have never seen anyone dare to argue against Wireshark as an analyzer when it comes to decoding common stuff like the IP or TCP layer :-)</p><p>So my advice would be to go ahead and do the check using Wireshark and see what happens. Don't waste time on validating Wireshark. Double check findings instead, including the hex dump if necessary.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Dec '13, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-28259" class="comments-container"><span id="28260"></span><div id="comment-28260" class="comment"><div id="post-28260-score" class="comment-score"></div><div class="comment-text"><p>Thank you very much for the advice. But, this is for a medical device, and the FDA requires proof that validation tools also be validated themselves.</p></div><div id="comment-28260-info" class="comment-info"><span class="comment-age">(18 Dec '13, 08:29)</span> <span class="comment-user userinfo">cgoogins</span></div></div><span id="28261"></span><div id="comment-28261" class="comment"><div id="post-28261-score" class="comment-score"></div><div class="comment-text"><p>I see. Can you ask someone at the FDA what the usual testing procedures are? Maybe they already have a list which includes Wireshark as a validated tool - it is the most common tool to be used for such examinations. And I'm pretty sure your device isn't the first medical device that has its TCP/IP stack checked.</p></div><div id="comment-28261-info" class="comment-info"><span class="comment-age">(18 Dec '13, 08:42)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="28270"></span><div id="comment-28270" class="comment"><div id="post-28270-score" class="comment-score"></div><div class="comment-text"><p>Thanks for the suggestions. I wish it could work that way, but unfortunately it doesn't. It is up to me to validate it.</p></div><div id="comment-28270-info" class="comment-info"><span class="comment-age">(18 Dec '13, 12:26)</span> <span class="comment-user userinfo">cgoogins</span></div></div></div><div id="comment-tools-28259" class="comment-tools"></div><div class="clear"></div><div id="comment-28259-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

