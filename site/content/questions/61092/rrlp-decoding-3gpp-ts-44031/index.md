+++
type = "question"
title = "RRLP decoding (3GPP TS 44.031)"
description = '''Hi guys, I am currently using wireshark-2.2.6. When trying to decode RRLP PDUs with wireshark the result differs with my decoder. Can you please tell me where I can find the exact version of 3GPP TS 44.031 wireshark for RRLP. Thanks a lot Omar AIT AMRANE'''
date = "2017-04-28T02:18:00Z"
lastmod = "2017-04-28T07:45:00Z"
weight = 61092
keywords = [ "44.031", "3gpp", "ts" ]
aliases = [ "/questions/61092" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RRLP decoding (3GPP TS 44.031)](/questions/61092/rrlp-decoding-3gpp-ts-44031)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61092-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61092-score" class="post-score" title="current number of votes">0</div><span id="post-61092-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys,</p><p>I am currently using wireshark-2.2.6. When trying to decode RRLP PDUs with wireshark the result differs with my decoder. Can you please tell me where I can find the exact version of 3GPP TS 44.031 wireshark for RRLP.</p><p>Thanks a lot Omar AIT AMRANE</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-44.031" rel="tag" title="see questions tagged &#39;44.031&#39;">44.031</span> <span class="post-tag tag-link-3gpp" rel="tag" title="see questions tagged &#39;3gpp&#39;">3gpp</span> <span class="post-tag tag-link-ts" rel="tag" title="see questions tagged &#39;ts&#39;">ts</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Apr '17, 02:18</strong></p><img src="https://secure.gravatar.com/avatar/98c95d4003dec700239fba3670a70612?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="oaa&#39;s gravatar image" /><p><span>oaa</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="oaa has no accepted answers">0%</span></p></div></div><div id="comments-container-61092" class="comments-container"></div><div id="comment-tools-61092" class="comment-tools"></div><div class="clear"></div><div id="comment-61092-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61103"></span>

<div id="answer-container-61103" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61103-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61103-score" class="post-score" title="current number of votes">0</div><span id="post-61103-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark 2.2.6 is using 3GPP TS 44.031 V11.0.0 as seen <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob;f=epan/dissectors/asn1/rrlp/RRLP-Messages.asn;h=8c4bdc8445b733d28ef2888de0cb350673a96a25;hb=32dac6a1ea518cb0062c139e2b3df298afc93c30">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '17, 06:23</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-61103" class="comments-container"><span id="61104"></span><div id="comment-61104" class="comment"><div id="post-61104-score" class="comment-score"></div><div class="comment-text"><p>Thank you very much. More generally where can I find the spec version for a specific protocol (LPP, MAP, RANAP, RRC, ...)</p></div><div id="comment-61104-info" class="comment-info"><span class="comment-age">(28 Apr '17, 06:37)</span> <span class="comment-user userinfo">oaa</span></div></div><span id="61105"></span><div id="comment-61105" class="comment"><div id="post-61105-score" class="comment-score"></div><div class="comment-text"><p>By browsing the source code and see the referenced version in the comments.</p></div><div id="comment-61105-info" class="comment-info"><span class="comment-age">(28 Apr '17, 07:45)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-61103" class="comment-tools"></div><div class="clear"></div><div id="comment-61103-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

