+++
type = "question"
title = "Wireshark 1.12.4 on Yosemite, Multiple Monitors, Window will not go to top of screen?"
description = '''I am running Wireshark 1.12.4 on the current version of OS X Yosemite (10.10.2) with the latest version of XQuartz (XQuartz 2.7.7 (xorg-server 1.15.2)). I have a MacBook Pro and am using a monitor as a 2nd display via a Thunderbolt port. When I run Wireshark, its Window will not go to the top of the...'''
date = "2015-04-02T10:24:00Z"
lastmod = "2015-04-02T13:56:00Z"
weight = 41156
keywords = [ "window", "yosemite", "display", "dual-monitor", "wireshark" ]
aliases = [ "/questions/41156" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 1.12.4 on Yosemite, Multiple Monitors, Window will not go to top of screen?](/questions/41156/wireshark-1124-on-yosemite-multiple-monitors-window-will-not-go-to-top-of-screen)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41156-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41156-score" class="post-score" title="current number of votes">0</div><span id="post-41156-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am running Wireshark 1.12.4 on the current version of OS X Yosemite (10.10.2) with the latest version of XQuartz (XQuartz 2.7.7 (xorg-server 1.15.2)). I have a MacBook Pro and am using a monitor as a 2nd display via a Thunderbolt port. When I run Wireshark, its Window will not go to the top of the display, it appears to be blocked 3 inches below my 2nd display. Even when I hit the full screen green button, the window will not occupy the full display, on either the MacBook display or the 2nd display. This problem has existed with all releases of Wireshark on Yosemite. Is this a known bug or should I submit a bug for it?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-window" rel="tag" title="see questions tagged &#39;window&#39;">window</span> <span class="post-tag tag-link-yosemite" rel="tag" title="see questions tagged &#39;yosemite&#39;">yosemite</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span> <span class="post-tag tag-link-dual-monitor" rel="tag" title="see questions tagged &#39;dual-monitor&#39;">dual-monitor</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '15, 10:24</strong></p><img src="https://secure.gravatar.com/avatar/895e402004f576b9eee8ab696e00688e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="crizzola&#39;s gravatar image" /><p><span>crizzola</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="crizzola has no accepted answers">0%</span></p></div></div><div id="comments-container-41156" class="comments-container"></div><div id="comment-tools-41156" class="comment-tools"></div><div class="clear"></div><div id="comment-41156-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41158"></span>

<div id="answer-container-41158" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41158-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41158-score" class="post-score" title="current number of votes">1</div><span id="post-41158-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can submit a bug but as the problem is likely to be in the gtk toolkit which is being dropped for Wireshark 2.0 there might not be a lot of interest in fixing it.</p><p>You can try the development releases of 1.99.x (currently 1.99.5) from the <a href="https://www.wireshark.org/download.html">download page</a>. These use the QT toolkit which apparently is much nicer on OSX. Note that the 1.99.x releases aren't yet fully feature complete.</p><p>If you get the same issue with 1.99.x then definitely raise a bug at the <a href="https://bugs.wireshark.org/bugzilla/">bug tracker</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '15, 10:31</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-41158" class="comments-container"><span id="41164"></span><div id="comment-41164" class="comment"><div id="post-41164-score" class="comment-score"></div><div class="comment-text"><p>Thanks Grahamb, will give 1.99.x a try.</p></div><div id="comment-41164-info" class="comment-info"><span class="comment-age">(02 Apr '15, 13:56)</span> <span class="comment-user userinfo">crizzola</span></div></div></div><div id="comment-tools-41158" class="comment-tools"></div><div class="clear"></div><div id="comment-41158-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

