+++
type = "question"
title = "API&#x27;s for wireshark to automate in TCL"
description = '''Planning to automate wireshark verification for captured output. Please let me know how can we do this. '''
date = "2011-01-24T20:53:00Z"
lastmod = "2011-01-24T22:28:00Z"
weight = 1911
keywords = [ "query" ]
aliases = [ "/questions/1911" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [API's for wireshark to automate in TCL](/questions/1911/apis-for-wireshark-to-automate-in-tcl)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1911-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1911-score" class="post-score" title="current number of votes">0</div><span id="post-1911-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Planning to automate wireshark verification for captured output. Please let me know how can we do this.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-query" rel="tag" title="see questions tagged &#39;query&#39;">query</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '11, 20:53</strong></p><img src="https://secure.gravatar.com/avatar/0feb1b6cb78dc2ab1fe13bbdc2684211?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rajassha&#39;s gravatar image" /><p><span>rajassha</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rajassha has no accepted answers">0%</span></p></div></div><div id="comments-container-1911" class="comments-container"></div><div id="comment-tools-1911" class="comment-tools"></div><div class="clear"></div><div id="comment-1911-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1912"></span>

<div id="answer-container-1912" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1912-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1912-score" class="post-score" title="current number of votes">0</div><span id="post-1912-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Look into using <a href="http://www.wireshark.org/docs/man-pages/tshark.html">tshark</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jan '11, 22:28</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1912" class="comments-container"></div><div id="comment-tools-1912" class="comment-tools"></div><div class="clear"></div><div id="comment-1912-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

