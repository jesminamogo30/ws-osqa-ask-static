+++
type = "question"
title = "[closed] path to dumpcap wrong, where to configure?"
description = '''When running wireshark as myself I see no interfaces and the following message Couldn&#x27;t run /usr/lib/x86_64-linux-gnu/dumpcap in child process: No such file or directory my dumpcap is not in that location but normally to /usr/bin when I sudo to wireshark, it works fine. How do I configure it to use ...'''
date = "2016-11-06T17:25:00Z"
lastmod = "2016-11-06T18:39:00Z"
weight = 57030
keywords = [ "configuration", "dumpcap" ]
aliases = [ "/questions/57030" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] path to dumpcap wrong, where to configure?](/questions/57030/path-to-dumpcap-wrong-where-to-configure)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57030-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57030-score" class="post-score" title="current number of votes">0</div><span id="post-57030-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When running wireshark as myself I see no interfaces and the following message</p><p>Couldn't run /usr/lib/x86_64-linux-gnu/dumpcap in child process: No such file or directory</p><p>my dumpcap is not in that location but normally to /usr/bin</p><p>when I sudo to wireshark, it works fine. How do I configure it to use the correct folder?</p><p>version 1.10.6 (v1.10.6 from master-1.10)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span> <span class="post-tag tag-link-dumpcap" rel="tag" title="see questions tagged &#39;dumpcap&#39;">dumpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '16, 17:25</strong></p><img src="https://secure.gravatar.com/avatar/467fd6c25699d1535ffcbbfd9e3e8e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pantel&#39;s gravatar image" /><p><span>Pantel</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pantel has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>10 Jan '17, 06:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-57030" class="comments-container"><span id="57033"></span><div id="comment-57033" class="comment"><div id="post-57033-score" class="comment-score"></div><div class="comment-text"><p>Did you build Wireshark from source yourself, or are you running a version from a package provided for your Linux distribution?</p></div><div id="comment-57033-info" class="comment-info"><span class="comment-age">(06 Nov '16, 18:39)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-57030" class="comment-tools"></div><div class="clear"></div><div id="comment-57030-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Not enough information provided (and no response to a request for that information)." by JeffMorriss 10 Jan '17, 06:16

</div>

</div>

</div>

