+++
type = "question"
title = "Where can I download Wireshark 1.12.13?"
description = '''I cannot find the download of Wireshark 1.12.13. Can please guide me? Thank you!'''
date = "2016-09-22T06:09:00Z"
lastmod = "2016-09-22T06:13:00Z"
weight = 55749
keywords = [ "1.12.13" ]
aliases = [ "/questions/55749" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Where can I download Wireshark 1.12.13?](/questions/55749/where-can-i-download-wireshark-11213)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55749-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55749-score" class="post-score" title="current number of votes">0</div><span id="post-55749-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I cannot find the download of Wireshark 1.12.13. Can please guide me? Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-1.12.13" rel="tag" title="see questions tagged &#39;1.12.13&#39;">1.12.13</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '16, 06:09</strong></p><img src="https://secure.gravatar.com/avatar/b4340a5aaf9627dca7a84262ffc0b3e6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mpfischer&#39;s gravatar image" /><p><span>mpfischer</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mpfischer has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Sep '16, 12:29</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-55749" class="comments-container"></div><div id="comment-tools-55749" class="comment-tools"></div><div class="clear"></div><div id="comment-55749-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55750"></span>

<div id="answer-container-55750" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55750-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55750-score" class="post-score" title="current number of votes">0</div><span id="post-55750-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://www.wireshark.org/download/win32/all-versions/">https://www.wireshark.org/download/win32/all-versions/</a></p><p><a href="https://www.wireshark.org/download/win64/all-versions/">https://www.wireshark.org/download/win64/all-versions/</a></p><p>for other OS, you'll manage to navigate I guess :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Sep '16, 06:13</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Sep '16, 06:14</strong> </span></p></div></div><div id="comments-container-55750" class="comments-container"></div><div id="comment-tools-55750" class="comment-tools"></div><div class="clear"></div><div id="comment-55750-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

