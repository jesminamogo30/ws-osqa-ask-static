+++
type = "question"
title = "WLAN card details to sniff pkts"
description = '''Hi I have a linux desktop (Ubuntu12.04 LTS, 64-bit, i7)with me. Can someone suggest me a WLAN card which I can plug it in for motherboard and start using it for WLAN Sniffing ? Objective :: I have a device connected to Access Point. I want to sniff/capture the handshaking pkts b/w both at MAC level....'''
date = "2014-03-26T02:18:00Z"
lastmod = "2014-03-26T02:18:00Z"
weight = 31164
keywords = [ "802.11", "sniffer", "ieee", "packet", "capture" ]
aliases = [ "/questions/31164" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WLAN card details to sniff pkts](/questions/31164/wlan-card-details-to-sniff-pkts)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31164-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31164-score" class="post-score" title="current number of votes">0</div><span id="post-31164-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I have a linux desktop (Ubuntu12.04 LTS, 64-bit, i7)with me. Can someone suggest me a WLAN card which I can plug it in for motherboard and start using it for WLAN Sniffing ?</p><p>Objective :: I have a device connected to Access Point. I want to sniff/capture the handshaking pkts b/w both at MAC level. I need a WLAN interface which can do this job for me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-sniffer" rel="tag" title="see questions tagged &#39;sniffer&#39;">sniffer</span> <span class="post-tag tag-link-ieee" rel="tag" title="see questions tagged &#39;ieee&#39;">ieee</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '14, 02:18</strong></p><img src="https://secure.gravatar.com/avatar/2023742ab6587c90ae933726d0edda56?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vamsi&#39;s gravatar image" /><p><span>vamsi</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vamsi has no accepted answers">0%</span></p></div></div><div id="comments-container-31164" class="comments-container"></div><div id="comment-tools-31164" class="comment-tools"></div><div class="clear"></div><div id="comment-31164-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

