+++
type = "question"
title = "Local Copy of Users Guide"
description = '''I just installed v1.4.0 and when I click on the Users Guide Icon (locally Installed) I still get the old 1.2.X users guide. How do I fix that? '''
date = "2010-09-22T12:45:00Z"
lastmod = "2010-10-03T02:49:00Z"
weight = 269
keywords = [ "users", "guide" ]
aliases = [ "/questions/269" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [Local Copy of Users Guide](/questions/269/local-copy-of-users-guide)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-269-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-269-score" class="post-score" title="current number of votes">0</div><span id="post-269-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed v1.4.0 and when I click on the Users Guide Icon (locally Installed) I still get the old 1.2.X users guide. How do I fix that?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-users" rel="tag" title="see questions tagged &#39;users&#39;">users</span> <span class="post-tag tag-link-guide" rel="tag" title="see questions tagged &#39;guide&#39;">guide</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '10, 12:45</strong></p><img src="https://secure.gravatar.com/avatar/749774c4ed70f7a8b22a292f023f73dd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="spoon47&#39;s gravatar image" /><p><span>spoon47</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="spoon47 has no accepted answers">0%</span></p></div></div><div id="comments-container-269" class="comments-container"></div><div id="comment-tools-269" class="comment-tools"></div><div class="clear"></div><div id="comment-269-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="289"></span>

<div id="answer-container-289" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-289-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-289-score" class="post-score" title="current number of votes">1</div><span id="post-289-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can download a copy here:</p><pre><code> http://www.wireshark.org/download/docs/user-guide-a4.pdf</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Sep '10, 03:33</strong></p><img src="https://secure.gravatar.com/avatar/a76e061e20ea73ec82b54bd9eaa9abe9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NetTech24&#39;s gravatar image" /><p><span>NetTech24</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NetTech24 has no accepted answers">0%</span></p></div></div><div id="comments-container-289" class="comments-container"><span id="320"></span><div id="comment-320" class="comment"><div id="post-320-score" class="comment-score"></div><div class="comment-text"><p>Other formats are available at http://www.wireshark.org/download/docs/</p></div><div id="comment-320-info" class="comment-info"><span class="comment-age">(24 Sep '10, 16:13)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-289" class="comment-tools"></div><div class="clear"></div><div id="comment-289-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="291"></span>

<div id="answer-container-291" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-291-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-291-score" class="post-score" title="current number of votes">1</div><span id="post-291-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's already on the <a href="http://wiki.wireshark.org/Development/Roadmap">Roadmap</a> for Wireshark 1.4.1, but you can always nag Gerald on it ;)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Sep '10, 03:47</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-291" class="comments-container"></div><div id="comment-tools-291" class="comment-tools"></div><div class="clear"></div><div id="comment-291-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="408"></span>

<div id="answer-container-408" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-408-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-408-score" class="post-score" title="current number of votes">0</div><span id="post-408-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Get the .chm file from the website and replace the existing file in your wireshark directory.</p><p>http://www.wireshark.org/download/docs/user-guide.chm</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Oct '10, 02:49</strong></p><img src="https://secure.gravatar.com/avatar/e5f17e21feaa02bf10f18d203d4b0369?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wmann&#39;s gravatar image" /><p><span>wmann</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wmann has no accepted answers">0%</span></p></div></div><div id="comments-container-408" class="comments-container"></div><div id="comment-tools-408" class="comment-tools"></div><div class="clear"></div><div id="comment-408-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

