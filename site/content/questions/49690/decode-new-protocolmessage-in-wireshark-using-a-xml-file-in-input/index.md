+++
type = "question"
title = "decode new protocol/message in wireshark using a xml file in input"
description = '''Hi, we are looking for decode new protocol/message in wireshark using a xml file in input. Have someone tried this way? Could you suggest any solution? Best regards.'''
date = "2016-02-01T07:19:00Z"
lastmod = "2016-02-02T00:13:00Z"
weight = 49690
keywords = [ "xml", "decode", "dissector", "plugin", "wireshark" ]
aliases = [ "/questions/49690" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [decode new protocol/message in wireshark using a xml file in input](/questions/49690/decode-new-protocolmessage-in-wireshark-using-a-xml-file-in-input)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49690-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49690-score" class="post-score" title="current number of votes">0</div><span id="post-49690-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, we are looking for decode new protocol/message in wireshark using a xml file in input. Have someone tried this way? Could you suggest any solution? Best regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xml" rel="tag" title="see questions tagged &#39;xml&#39;">xml</span> <span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-plugin" rel="tag" title="see questions tagged &#39;plugin&#39;">plugin</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '16, 07:19</strong></p><img src="https://secure.gravatar.com/avatar/79e0a73b733617b1903b10bdd0fc724a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mimmo%20Pignatelli&#39;s gravatar image" /><p><span>Mimmo Pignat...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mimmo Pignatelli has no accepted answers">0%</span></p></div></div><div id="comments-container-49690" class="comments-container"><span id="49704"></span><div id="comment-49704" class="comment"><div id="post-49704-score" class="comment-score"></div><div class="comment-text"><p>Are you trying to decode a protocol that involves sending XML over the wire - i.e., the protocol data is <em>itself</em> XML - or are you trying to decode a protocol where the syntax of the packets is specified by an XML-based language?</p></div><div id="comment-49704-info" class="comment-info"><span class="comment-age">(01 Feb '16, 18:32)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="49708"></span><div id="comment-49708" class="comment"><div id="post-49708-score" class="comment-score"></div><div class="comment-text"><p>Hi Guy, thanks for the answer. We're trying to decode a protocol where the syntax of the packets is specified by an XML-based language.<br />
Best regards, Mimmo</p></div><div id="comment-49708-info" class="comment-info"><span class="comment-age">(02 Feb '16, 00:13)</span> <span class="comment-user userinfo">Mimmo Pignat...</span></div></div></div><div id="comment-tools-49690" class="comment-tools"></div><div class="clear"></div><div id="comment-49690-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

