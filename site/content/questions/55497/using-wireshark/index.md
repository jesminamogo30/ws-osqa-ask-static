+++
type = "question"
title = "Using Wireshark"
description = '''I am very new to WireShark. Here is my problem. I have a user that RDP&#x27;s into a local machine, during the day it will start painting the screen. Will WireShark be able to help me find the problem? '''
date = "2016-09-12T15:29:00Z"
lastmod = "2016-09-12T15:50:00Z"
weight = 55497
keywords = [ "rdpprob" ]
aliases = [ "/questions/55497" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Using Wireshark](/questions/55497/using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55497-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55497-score" class="post-score" title="current number of votes">0</div><span id="post-55497-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am very new to WireShark. Here is my problem. I have a user that RDP's into a local machine, during the day it will start painting the screen. Will WireShark be able to help me find the problem?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rdpprob" rel="tag" title="see questions tagged &#39;rdpprob&#39;">rdpprob</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Sep '16, 15:29</strong></p><img src="https://secure.gravatar.com/avatar/077f547a052eb4933f849301453d60ec?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rwm2&#39;s gravatar image" /><p><span>rwm2</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rwm2 has no accepted answers">0%</span></p></div></div><div id="comments-container-55497" class="comments-container"><span id="55499"></span><div id="comment-55499" class="comment"><div id="post-55499-score" class="comment-score"></div><div class="comment-text"><p>Not really. What does 'starts painting the screen' mean?</p></div><div id="comment-55499-info" class="comment-info"><span class="comment-age">(12 Sep '16, 15:47)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="55500"></span><div id="comment-55500" class="comment"><div id="post-55500-score" class="comment-score"></div><div class="comment-text"><p>Paint the screen means that the running the rdp from her computer slows down to where the moves very slowly as if you were painting the screen from top to bottom</p></div><div id="comment-55500-info" class="comment-info"><span class="comment-age">(12 Sep '16, 15:50)</span> <span class="comment-user userinfo">rwm2</span></div></div></div><div id="comment-tools-55497" class="comment-tools"></div><div class="clear"></div><div id="comment-55497-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

