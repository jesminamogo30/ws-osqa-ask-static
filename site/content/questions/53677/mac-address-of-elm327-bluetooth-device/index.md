+++
type = "question"
title = "Mac address of elm327 bluetooth device"
description = '''I bought a elm327 bluetooth device, basically a OBD II scanner. When I first plugged it in to the OBD II port in my car, and paired it with my android phone, it showed the mac address. But the mac address almost immediately changed to just &quot;OBD II&quot;. How do I find out what the mac address was? I didn...'''
date = "2016-06-27T08:43:00Z"
lastmod = "2016-06-28T02:30:00Z"
weight = 53677
keywords = [ "obdii", "elm327", "mac", "address" ]
aliases = [ "/questions/53677" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Mac address of elm327 bluetooth device](/questions/53677/mac-address-of-elm327-bluetooth-device)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53677-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53677-score" class="post-score" title="current number of votes">0</div><span id="post-53677-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I bought a elm327 bluetooth device, basically a OBD II scanner. When I first plugged it in to the OBD II port in my car, and paired it with my android phone, it showed the mac address. But the mac address almost immediately changed to just "OBD II". How do I find out what the mac address was? I didnt have the chance to jot it down.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-obdii" rel="tag" title="see questions tagged &#39;obdii&#39;">obdii</span> <span class="post-tag tag-link-elm327" rel="tag" title="see questions tagged &#39;elm327&#39;">elm327</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jun '16, 08:43</strong></p><img src="https://secure.gravatar.com/avatar/f6ccc7158052d17279a6da4cec40e740?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kodai&#39;s gravatar image" /><p><span>Kodai</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kodai has no accepted answers">0%</span></p></div></div><div id="comments-container-53677" class="comments-container"></div><div id="comment-tools-53677" class="comment-tools"></div><div class="clear"></div><div id="comment-53677-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="53692"></span>

<div id="answer-container-53692" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53692-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53692-score" class="post-score" title="current number of votes">0</div><span id="post-53692-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unless you have a PC to which you could pair the OBDII interface box too, you'd need some Android forum to tell you whether the MAC is stored in some file. Wireshark can be used to capture Bluetooth traffic on air but so far only using a dedicated hardware which I think might be too expensive compared to the value of the information.</p><p>On the other hand, what is the actual value of the information? Do you need to be sure you have paired your phone with your own elm327 and not with your neighbour's?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jun '16, 01:08</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-53692" class="comments-container"></div><div id="comment-tools-53692" class="comment-tools"></div><div class="clear"></div><div id="comment-53692-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="53695"></span>

<div id="answer-container-53695" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53695-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53695-score" class="post-score" title="current number of votes">0</div><span id="post-53695-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unless you can make a capture, Wireshark isn't going to help here. Maybe you could unpair or "forget" the ELM327 device from your phone, and then search for it again to see the MAC address.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jun '16, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-53695" class="comments-container"></div><div id="comment-tools-53695" class="comment-tools"></div><div class="clear"></div><div id="comment-53695-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

