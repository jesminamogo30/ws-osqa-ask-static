+++
type = "question"
title = "No Packets - Windows XP SP3 - Wireless USB Adapter"
description = '''Hi, everyone. When I go to Capture &amp;gt; Interfaces..., there are interfaces, one of which is related to my wireless adapter. It shows an IP and the number of packets increasing. However, when I choose Start, nothing it shown on the screen, and, on the bottom, it is written &quot;No Packets&quot;. Why is this ...'''
date = "2013-06-27T08:19:00Z"
lastmod = "2013-06-27T13:26:00Z"
weight = 22412
keywords = [ "help" ]
aliases = [ "/questions/22412" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No Packets - Windows XP SP3 - Wireless USB Adapter](/questions/22412/no-packets-windows-xp-sp3-wireless-usb-adapter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22412-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22412-score" class="post-score" title="current number of votes">0</div><span id="post-22412-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, everyone. When I go to Capture &gt; Interfaces..., there are interfaces, one of which is related to my wireless adapter. It shows an IP and the number of packets increasing. However, when I choose Start, nothing it shown on the screen, and, on the bottom, it is written "No Packets". Why is this happening? Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jun '13, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/3224f6ba10ca11467f78bfaa17c3219b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stdq&#39;s gravatar image" /><p><span>stdq</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stdq has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Jun '13, 14:13</strong> </span></p></div></div><div id="comments-container-22412" class="comments-container"></div><div id="comment-tools-22412" class="comment-tools"></div><div class="clear"></div><div id="comment-22412-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="22413"></span>

<div id="answer-container-22413" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22413-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22413-score" class="post-score" title="current number of votes">0</div><span id="post-22413-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Just solved by disabling promiscuous mode, thanks to all again!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '13, 08:21</strong></p><img src="https://secure.gravatar.com/avatar/3224f6ba10ca11467f78bfaa17c3219b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stdq&#39;s gravatar image" /><p><span>stdq</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stdq has no accepted answers">0%</span></p></div></div><div id="comments-container-22413" class="comments-container"><span id="22420"></span><div id="comment-22420" class="comment"><div id="post-22420-score" class="comment-score"></div><div class="comment-text"><p>This is on WIndows, right? If so, please edit the question to note that in the title and the question (and to note that this is a Wi-Fi adapter).</p></div><div id="comment-22420-info" class="comment-info"><span class="comment-age">(27 Jun '13, 13:26)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-22413" class="comment-tools"></div><div class="clear"></div><div id="comment-22413-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

