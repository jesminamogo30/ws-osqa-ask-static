+++
type = "question"
title = "wireshark tracking school project for filesharing clients"
description = '''Hello together, i have a questin about filesharing. in our school we have ten pcs with bittorent...we have created a test torrent with utorrent...we upload it to an opentracker...and now we can download it.. but now the problem... we use wireshark to protocol the filetransfer...we use the torrent fi...'''
date = "2011-09-26T01:54:00Z"
lastmod = "2011-09-26T03:12:00Z"
weight = 6552
keywords = [ "project", "filter", "torrent", "filesharing", "wireshark" ]
aliases = [ "/questions/6552" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark tracking school project for filesharing clients](/questions/6552/wireshark-tracking-school-project-for-filesharing-clients)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6552-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6552-score" class="post-score" title="current number of votes">1</div><span id="post-6552-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello together,</p><p>i have a questin about filesharing. in our school we have ten pcs with bittorent...we have created a test torrent with utorrent...we upload it to an opentracker...and now we can download it..</p><p>but now the problem...</p><p>we use wireshark to protocol the filetransfer...we use the torrent filter....now we see some ip's ...with handshake...</p><p>is it possible to the the internet provider like...10.44.22.1.dialup-t-online,.de ???</p><p>Is it right that all clients that download this torrent will be shown in wireshark?</p><p>Or how does it work? In the bittorent client i see the peers...are this all clients that download the torrent?</p><p>i hope somebody can help us by our scool project.</p><p>have a nice day many thanks tim</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-project" rel="tag" title="see questions tagged &#39;project&#39;">project</span> <span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-torrent" rel="tag" title="see questions tagged &#39;torrent&#39;">torrent</span> <span class="post-tag tag-link-filesharing" rel="tag" title="see questions tagged &#39;filesharing&#39;">filesharing</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '11, 01:54</strong></p><img src="https://secure.gravatar.com/avatar/f624408a7d0b72c6d3cb2f752341d7e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="skullflower&#39;s gravatar image" /><p><span>skullflower</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="skullflower has no accepted answers">0%</span></p></div></div><div id="comments-container-6552" class="comments-container"></div><div id="comment-tools-6552" class="comment-tools"></div><div class="clear"></div><div id="comment-6552-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6555"></span>

<div id="answer-container-6555" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6555-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6555-score" class="post-score" title="current number of votes">0</div><span id="post-6555-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is more a bittorrent question than a Wireshark question. <a href="http://computer.howstuffworks.com/bittorrent.htm">Read here for more details on bittorrent</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Sep '11, 02:50</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-6555" class="comments-container"><span id="6556"></span><div id="comment-6556" class="comment"><div id="post-6556-score" class="comment-score"></div><div class="comment-text"><p>yes but we want to protocol/filter it by wireshark...</p></div><div id="comment-6556-info" class="comment-info"><span class="comment-age">(26 Sep '11, 03:12)</span> <span class="comment-user userinfo">skullflower</span></div></div></div><div id="comment-tools-6555" class="comment-tools"></div><div class="clear"></div><div id="comment-6555-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

