+++
type = "question"
title = "VirtuaBox Issue"
description = '''How do I only view packets sent on my Virtualbox VM?'''
date = "2016-02-01T16:31:00Z"
lastmod = "2016-02-01T16:31:00Z"
weight = 49702
keywords = [ "vm", "virtualbox" ]
aliases = [ "/questions/49702" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [VirtuaBox Issue](/questions/49702/virtuabox-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49702-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49702-score" class="post-score" title="current number of votes">0</div><span id="post-49702-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I only view packets sent on my Virtualbox VM?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vm" rel="tag" title="see questions tagged &#39;vm&#39;">vm</span> <span class="post-tag tag-link-virtualbox" rel="tag" title="see questions tagged &#39;virtualbox&#39;">virtualbox</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '16, 16:31</strong></p><img src="https://secure.gravatar.com/avatar/c71895fdabd96520432617eb8263ced5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Harpo&#39;s gravatar image" /><p><span>Harpo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Harpo has no accepted answers">0%</span></p></div></div><div id="comments-container-49702" class="comments-container"></div><div id="comment-tools-49702" class="comment-tools"></div><div class="clear"></div><div id="comment-49702-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

