+++
type = "question"
title = "PLC PRIME protocol dissector"
description = '''Hello, I have worked with some teammates on a dissector for PLC PRIME protocol. Just to check before going further, is there anyone else working on a dissector for PLC PRIME and has the intention to add it to wireshark? Thanks'''
date = "2013-03-12T07:31:00Z"
lastmod = "2013-03-12T07:53:00Z"
weight = 19386
keywords = [ "prime", "plc" ]
aliases = [ "/questions/19386" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [PLC PRIME protocol dissector](/questions/19386/plc-prime-protocol-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19386-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19386-score" class="post-score" title="current number of votes">0</div><span id="post-19386-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have worked with some teammates on a dissector for PLC PRIME protocol. Just to check before going further, is there anyone else working on a dissector for PLC PRIME and has the intention to add it to wireshark?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-prime" rel="tag" title="see questions tagged &#39;prime&#39;">prime</span> <span class="post-tag tag-link-plc" rel="tag" title="see questions tagged &#39;plc&#39;">plc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '13, 07:31</strong></p><img src="https://secure.gravatar.com/avatar/5d11f2a19f763904f5b7102e82e613e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mihaIzKamnea&#39;s gravatar image" /><p><span>mihaIzKamnea</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mihaIzKamnea has no accepted answers">0%</span></p></div></div><div id="comments-container-19386" class="comments-container"></div><div id="comment-tools-19386" class="comment-tools"></div><div class="clear"></div><div id="comment-19386-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19387"></span>

<div id="answer-container-19387" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19387-score" class="post-score" title="current number of votes">0</div><span id="post-19387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You'll probably get a better response on the <a href="https://www.wireshark.org/mailman/listinfo/wireshark-dev">dev</a> mailing list. Also have you searched bugzilla for any enhancement requests?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Mar '13, 07:53</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-19387" class="comments-container"></div><div id="comment-tools-19387" class="comment-tools"></div><div class="clear"></div><div id="comment-19387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

