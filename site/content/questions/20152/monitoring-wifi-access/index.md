+++
type = "question"
title = "Monitoring wifi access."
description = '''My son is a normal teenager and i have a feeling he is accessing inappropriate sites. Is there a way i can see the sites he is accessing on his ipad by looking at my wifi?'''
date = "2013-04-07T10:35:00Z"
lastmod = "2013-04-07T11:27:00Z"
weight = 20152
keywords = [ "access", "wifi" ]
aliases = [ "/questions/20152" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Monitoring wifi access.](/questions/20152/monitoring-wifi-access)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20152-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20152-score" class="post-score" title="current number of votes">0</div><span id="post-20152-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My son is a normal teenager and i have a feeling he is accessing inappropriate sites. Is there a way i can see the sites he is accessing on his ipad by looking at my wifi?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-access" rel="tag" title="see questions tagged &#39;access&#39;">access</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '13, 10:35</strong></p><img src="https://secure.gravatar.com/avatar/70d076fa757a43645f2046496b5e8976?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BrooklynBoy77&#39;s gravatar image" /><p><span>BrooklynBoy77</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BrooklynBoy77 has no accepted answers">0%</span></p></div></div><div id="comments-container-20152" class="comments-container"></div><div id="comment-tools-20152" class="comment-tools"></div><div class="clear"></div><div id="comment-20152-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20154"></span>

<div id="answer-container-20154" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20154-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20154-score" class="post-score" title="current number of votes">1</div><span id="post-20154-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, if you</p><ol><li>have a wifi card and operating system that allows Wireshark to capture the WiFi radio layer. On Windows you need AirPCAP, on Linux/Mac OS you may be able to do without</li><li>your WiFi network is unencrypted or you have the decryption key and the skills to use it to have Wireshark decode the packets</li><li>you know the ethernet/IP address of the iPad</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Apr '13, 11:27</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-20154" class="comments-container"></div><div id="comment-tools-20154" class="comment-tools"></div><div class="clear"></div><div id="comment-20154-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

