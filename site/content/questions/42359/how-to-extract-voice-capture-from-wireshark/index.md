+++
type = "question"
title = "How to extract voice capture from Wireshark"
description = '''I have captured an RTP session that uses G.711 codec. I am able to playback the voice portion of the capture using Wireshark (Telephony / VoIP Calls). I am interested in just saving the voice portion as a separate file. Any suggestions?'''
date = "2015-05-13T06:51:00Z"
lastmod = "2015-05-13T07:22:00Z"
weight = 42359
keywords = [ "voice" ]
aliases = [ "/questions/42359" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to extract voice capture from Wireshark](/questions/42359/how-to-extract-voice-capture-from-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42359-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42359-score" class="post-score" title="current number of votes">0</div><span id="post-42359-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured an RTP session that uses G.711 codec. I am able to playback the voice portion of the capture using Wireshark (Telephony / VoIP Calls). I am interested in just saving the voice portion as a separate file. Any suggestions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voice" rel="tag" title="see questions tagged &#39;voice&#39;">voice</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '15, 06:51</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-42359" class="comments-container"></div><div id="comment-tools-42359" class="comment-tools"></div><div class="clear"></div><div id="comment-42359-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42362"></span>

<div id="answer-container-42362" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42362-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42362-score" class="post-score" title="current number of votes">1</div><span id="post-42362-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Amato_C has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Check this page of the <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChTelRTPAnalysis.html">Users Guide</a>, especially the last line.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 May '15, 07:13</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-42362" class="comments-container"><span id="42363"></span><div id="comment-42363" class="comment"><div id="post-42363-score" class="comment-score"></div><div class="comment-text"><p><span>@Jaap</span> = That worked. Thanks!</p></div><div id="comment-42363-info" class="comment-info"><span class="comment-age">(13 May '15, 07:22)</span> <span class="comment-user userinfo">Amato_C</span></div></div></div><div id="comment-tools-42362" class="comment-tools"></div><div class="clear"></div><div id="comment-42362-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

