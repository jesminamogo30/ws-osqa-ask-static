+++
type = "question"
title = "Faxing over IP (FoIP)"
description = '''Hello Wireshark Fans! I&#x27;m going to be on site tomorrow to troubleshoot a Ricoh copier that will be setup to Fax over IP (FoIP). I am responsible for using Wireshark to capture the packets between the sending Ricoh and the destination Ricoh.  I know how to find SIP and Skinny Client Control Protocol ...'''
date = "2017-06-12T11:36:00Z"
lastmod = "2017-06-13T02:45:00Z"
weight = 61955
keywords = [ "pbx", "foip", "wireshark" ]
aliases = [ "/questions/61955" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Faxing over IP (FoIP)](/questions/61955/faxing-over-ip-foip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61955-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61955-score" class="post-score" title="current number of votes">0</div><span id="post-61955-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Wireshark Fans!</p><p>I'm going to be on site tomorrow to troubleshoot a Ricoh copier that will be setup to Fax over IP (FoIP). I am responsible for using Wireshark to capture the packets between the sending Ricoh and the destination Ricoh.</p><p>I know how to find SIP and Skinny Client Control Protocol (SCCP), but I am not familiar with what protocol(s) are using for faxing over IP.</p><p>Does anyone know what to look for in this situation? The customer will be using the Cisco Unified Communications Manager VoIP PBX for the phone system.</p><p>Any assistance would be greatly appreciated!</p><p>Thanks,</p><p>Getnoldfast</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pbx" rel="tag" title="see questions tagged &#39;pbx&#39;">pbx</span> <span class="post-tag tag-link-foip" rel="tag" title="see questions tagged &#39;foip&#39;">foip</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jun '17, 11:36</strong></p><img src="https://secure.gravatar.com/avatar/a44f34bb3fbe05d422ad198e7ecec432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Getnoldfast&#39;s gravatar image" /><p><span>Getnoldfast</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Getnoldfast has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Jun '17, 11:38</strong> </span></p></div></div><div id="comments-container-61955" class="comments-container"></div><div id="comment-tools-61955" class="comment-tools"></div><div class="clear"></div><div id="comment-61955-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61980"></span>

<div id="answer-container-61980" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61980-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61980-score" class="post-score" title="current number of votes">0</div><span id="post-61980-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="https://wiki.wireshark.org/T38">T.38</a>, although I'm not sure how current this description is.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Jun '17, 02:45</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-61980" class="comments-container"></div><div id="comment-tools-61980" class="comment-tools"></div><div class="clear"></div><div id="comment-61980-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

