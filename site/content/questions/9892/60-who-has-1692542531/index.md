+++
type = "question"
title = "60 who has 169.254.25.31"
description = '''I am seeing many of this type of listing &quot;60 who has 169.254.25.31? tell 10.248.252.50&quot; Can you please tell me what it means? Thanks'''
date = "2012-04-01T21:24:00Z"
lastmod = "2012-09-21T09:27:00Z"
weight = 9892
keywords = [ "arp" ]
aliases = [ "/questions/9892" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [60 who has 169.254.25.31](/questions/9892/60-who-has-1692542531)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9892-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9892-score" class="post-score" title="current number of votes">0</div><span id="post-9892-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am seeing many of this type of listing "60 who has 169.254.25.31? tell 10.248.252.50" Can you please tell me what it means?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-arp" rel="tag" title="see questions tagged &#39;arp&#39;">arp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '12, 21:24</strong></p><img src="https://secure.gravatar.com/avatar/aef358145abba14a2488d04bae8d27b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dbruce&#39;s gravatar image" /><p><span>dbruce</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dbruce has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Sep '12, 08:34</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-9892" class="comments-container"></div><div id="comment-tools-9892" class="comment-tools"></div><div class="clear"></div><div id="comment-9892-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9894"></span>

<div id="answer-container-9894" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9894-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9894-score" class="post-score" title="current number of votes">2</div><span id="post-9894-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's an <a href="http://en.wikipedia.org/wiki/Address_Resolution_Protocol">Address Resolution Protocol</a> (ARP) packet. The machine with an IP address of 10.248.252.50 wants to send a packet to the machine with an IP address of 169.254.25.31, and believes that machine is on the same network as it, so it's sending a broadcast packet on the network in the hopes of getting back a packet giving the MAC address (such as an Ethernet address) for that machine.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '12, 00:48</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-9894" class="comments-container"><span id="14435"></span><div id="comment-14435" class="comment"><div id="post-14435-score" class="comment-score">1</div><div class="comment-text"><p>169.254.0.0/16 is the <a href="http://support.microsoft.com/kb/220874">APIPA</a> range. How can a system with 10.248.252.50 believe it is in the same subnet? That would be only possible with a netmask of /0 (which does not make any sense)!?</p><p><span></span><span>@dbruce</span>: Please check the configuration of the system 10.248.252.50.</p></div><div id="comment-14435-info" class="comment-info"><span class="comment-age">(21 Sep '12, 09:27)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-9894" class="comment-tools"></div><div class="clear"></div><div id="comment-9894-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

