+++
type = "question"
title = "Testing Pcaps w/ tshark from the command line."
description = '''Im looking for a way to test call quality from the command line. I am working on writing a script, and I am limited into what I can install remotely.  Im guessing to test the jitters, but could really use your input. '''
date = "2015-04-24T06:52:00Z"
lastmod = "2015-04-24T06:52:00Z"
weight = 41778
keywords = [ "jitters", "tshark", "pcaps" ]
aliases = [ "/questions/41778" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Testing Pcaps w/ tshark from the command line.](/questions/41778/testing-pcaps-w-tshark-from-the-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41778-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41778-score" class="post-score" title="current number of votes">0</div><span id="post-41778-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Im looking for a way to test call quality from the command line. I am working on writing a script, and I am limited into what I can install remotely.</p><p>Im guessing to test the jitters, but could really use your input.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-jitters" rel="tag" title="see questions tagged &#39;jitters&#39;">jitters</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-pcaps" rel="tag" title="see questions tagged &#39;pcaps&#39;">pcaps</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '15, 06:52</strong></p><img src="https://secure.gravatar.com/avatar/48d7568905601f75c0519682ce1f3124?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RobRob&#39;s gravatar image" /><p><span>RobRob</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RobRob has no accepted answers">0%</span></p></div></div><div id="comments-container-41778" class="comments-container"></div><div id="comment-tools-41778" class="comment-tools"></div><div class="clear"></div><div id="comment-41778-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

