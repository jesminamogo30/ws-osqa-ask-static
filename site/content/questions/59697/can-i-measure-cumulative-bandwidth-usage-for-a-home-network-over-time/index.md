+++
type = "question"
title = "Can I measure cumulative bandwidth usage for a home network over time?"
description = '''Quick question, with Wireshark, could I measure the total # of bytes used by my home network over a period of about a month? I am trying to monitor how much bandwidth I use in a billing cycle and compare the number to what my ISP claims I used. I have multiple wireless clients on the network that wo...'''
date = "2017-02-26T14:10:00Z"
lastmod = "2017-02-27T01:58:00Z"
weight = 59697
keywords = [ "usage", "bandwidth", "cumulative", "data" ]
aliases = [ "/questions/59697" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can I measure cumulative bandwidth usage for a home network over time?](/questions/59697/can-i-measure-cumulative-bandwidth-usage-for-a-home-network-over-time)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59697-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59697-score" class="post-score" title="current number of votes">0</div><span id="post-59697-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Quick question, with Wireshark, could I measure the total # of bytes used by my home network over a period of about a month? I am trying to monitor how much bandwidth I use in a billing cycle and compare the number to what my ISP claims I used.</p><p>I have multiple wireless clients on the network that would need to be monitored as well. Does that mean I would need a machine running Wireshark between my access point and modem? Or is there another way to capture the cumulative amount of data on my network? Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-usage" rel="tag" title="see questions tagged &#39;usage&#39;">usage</span> <span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-cumulative" rel="tag" title="see questions tagged &#39;cumulative&#39;">cumulative</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Feb '17, 14:10</strong></p><img src="https://secure.gravatar.com/avatar/8cf8f3f55f9e83910c529408f643a53d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tasmainia300&#39;s gravatar image" /><p><span>Tasmainia300</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tasmainia300 has no accepted answers">0%</span></p></div></div><div id="comments-container-59697" class="comments-container"></div><div id="comment-tools-59697" class="comment-tools"></div><div class="clear"></div><div id="comment-59697-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59702"></span>

<div id="answer-container-59702" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59702-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59702-score" class="post-score" title="current number of votes">0</div><span id="post-59702-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark isn't really the tool for this, it's a packet analyser that also presents some statistics about the capture file.</p><p>Probably the best approach would be to see what info your access point provides (if all clients connect to the internet via that). For many AP's there are alternative firmware installations that provide much more info and facilities than the stock manufacturer firmware, e.g <a href="https://www.google.co.uk/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=0ahUKEwiy9_vKgbDSAhWEBsAKHZn5A6kQFggaMAA&amp;url=https%3A%2F%2Fopenwrt.org%2F&amp;usg=AFQjCNGywpC0moi5mmj3pNPFSKknCXhrKg&amp;bvm=bv.148073327,d.ZGg">OpenWRT</a> etc. There's a list <a href="https://en.wikipedia.org/wiki/List_of_router_firmware_projects">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Feb '17, 01:58</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-59702" class="comments-container"></div><div id="comment-tools-59702" class="comment-tools"></div><div class="clear"></div><div id="comment-59702-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

