+++
type = "question"
title = "What version of the Wireshark support Docsis 3.1 frames?"
description = '''Will like to know which version will support Docsis 3.1 frames. thanks in advance,'''
date = "2014-09-30T09:24:00Z"
lastmod = "2014-09-30T09:24:00Z"
weight = 36728
keywords = [ "version", "wireshark" ]
aliases = [ "/questions/36728" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What version of the Wireshark support Docsis 3.1 frames?](/questions/36728/what-version-of-the-wireshark-support-docsis-31-frames)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36728-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36728-score" class="post-score" title="current number of votes">0</div><span id="post-36728-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Will like to know which version will support Docsis 3.1 frames. thanks in advance,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-version" rel="tag" title="see questions tagged &#39;version&#39;">version</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '14, 09:24</strong></p><img src="https://secure.gravatar.com/avatar/fe7b8b8f82626427d3ae7d5428f2102d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="christenmu&#39;s gravatar image" /><p><span>christenmu</span><br />
<span class="score" title="36 reputation points">36</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="christenmu has one accepted answer">50%</span></p></div></div><div id="comments-container-36728" class="comments-container"></div><div id="comment-tools-36728" class="comment-tools"></div><div class="clear"></div><div id="comment-36728-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

