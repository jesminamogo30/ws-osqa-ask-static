+++
type = "question"
title = "[closed] How to disable Insert Online Pictures/Videos in Office 2016?"
description = '''I&#x27;m trying to determine the correct registry edit to make to disable the feature of Microsoft Office 2016 that allows internal internet access via the Insert Picture/Insert Video feature. I&#x27;ve navigated to: HKEY_Current_User/SOFTWARE/Microsoft/Office/16.0 and I believe it is likely something in eith...'''
date = "2017-06-24T07:58:00Z"
lastmod = "2017-06-24T07:58:00Z"
weight = 62280
keywords = [ "registry", "office" ]
aliases = [ "/questions/62280" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to disable Insert Online Pictures/Videos in Office 2016?](/questions/62280/how-to-disable-insert-online-picturesvideos-in-office-2016)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62280-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62280-score" class="post-score" title="current number of votes">-1</div><span id="post-62280-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to determine the correct registry edit to make to disable the feature of Microsoft Office 2016 that allows internal internet access via the Insert Picture/Insert Video feature. I've navigated to: HKEY_Current_User/SOFTWARE/Microsoft/Office/16.0 and I believe it is likely something in either General or Insert Media/Online Media, but I'm not sure where to go from here. Does anyone have suggestions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-registry" rel="tag" title="see questions tagged &#39;registry&#39;">registry</span> <span class="post-tag tag-link-office" rel="tag" title="see questions tagged &#39;office&#39;">office</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jun '17, 07:58</strong></p><img src="https://secure.gravatar.com/avatar/985a9e3f6bd47e8b15fe0ec9ed4faea3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ARChurch&#39;s gravatar image" /><p><span>ARChurch</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ARChurch has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>24 Jun '17, 08:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-62280" class="comments-container"></div><div id="comment-tools-62280" class="comment-tools"></div><div class="clear"></div><div id="comment-62280-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by cmaynard 24 Jun '17, 08:35

</div>

</div>

</div>

