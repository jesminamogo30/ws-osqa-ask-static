+++
type = "question"
title = "cookies not filtering"
description = '''I am trying to filter the cookies by using http.cookie and nothing is showing. does this mean that wireshark is not capturing the cookies? i am specifically trying to obtain facebook cookies im on a wifi network. pleasee help!!'''
date = "2015-05-01T14:37:00Z"
lastmod = "2015-05-01T17:07:00Z"
weight = 42008
keywords = [ "cookie" ]
aliases = [ "/questions/42008" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [cookies not filtering](/questions/42008/cookies-not-filtering)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42008-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42008-score" class="post-score" title="current number of votes">0</div><span id="post-42008-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to filter the cookies by using http.cookie and nothing is showing. does this mean that wireshark is not capturing the cookies? i am specifically trying to obtain facebook cookies im on a wifi network. pleasee help!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cookie" rel="tag" title="see questions tagged &#39;cookie&#39;">cookie</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 May '15, 14:37</strong></p><img src="https://secure.gravatar.com/avatar/1825d2a0c221c201998bed4163428058?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sora7ya&#39;s gravatar image" /><p><span>sora7ya</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sora7ya has no accepted answers">0%</span></p></div></div><div id="comments-container-42008" class="comments-container"></div><div id="comment-tools-42008" class="comment-tools"></div><div class="clear"></div><div id="comment-42008-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42012"></span>

<div id="answer-container-42012" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42012-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42012-score" class="post-score" title="current number of votes">0</div><span id="post-42012-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It means Facebook is using SSL so you can't filter for http.cookie. You can view the cookies in the browser or you can use a software like Fiddler and turn on SSL interception.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 May '15, 15:38</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 May '15, 03:27</strong> </span></p></div></div><div id="comments-container-42012" class="comments-container"><span id="42015"></span><div id="comment-42015" class="comment"><div id="post-42015-score" class="comment-score"></div><div class="comment-text"><p>ok so how do i do it then? I also noticed that there were no cookies that were captured the search came back blank.</p></div><div id="comment-42015-info" class="comment-info"><span class="comment-age">(01 May '15, 17:07)</span> <span class="comment-user userinfo">sora7ya</span></div></div></div><div id="comment-tools-42012" class="comment-tools"></div><div class="clear"></div><div id="comment-42012-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

