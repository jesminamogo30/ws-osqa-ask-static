+++
type = "question"
title = "does wireshark help is faster and better calls over voip ?"
description = '''Hi I use free voip account for incoming calls ... the only problem is that it takes a while to connect the call between me and someone who is calling... it takes more than 10 seconds to connect the call after someone calls.... will your software help me in this also... I use voip call on windows 7.....'''
date = "2016-01-28T04:57:00Z"
lastmod = "2016-01-28T05:09:00Z"
weight = 49592
keywords = [ "free", "voip" ]
aliases = [ "/questions/49592" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [does wireshark help is faster and better calls over voip ?](/questions/49592/does-wireshark-help-is-faster-and-better-calls-over-voip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49592-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49592-score" class="post-score" title="current number of votes">0</div><span id="post-49592-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I use free voip account for incoming calls ... the only problem is that it takes a while to connect the call between me and someone who is calling... it takes more than 10 seconds to connect the call after someone calls.... will your software help me in this also... I use voip call on windows 7...</p><p>is your software free for private use ??</p><p>pls reply at <span class="__cf_email__" data-cfemail="bbdfdacfded6de888c8a82fbd3d4cfd6dad2d795d8d4d6">[email protected]</span></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-free" rel="tag" title="see questions tagged &#39;free&#39;">free</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jan '16, 04:57</strong></p><img src="https://secure.gravatar.com/avatar/1f46b6ec4c8a7bf495ed0b9aa3db5a0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tushar%20Dutta&#39;s gravatar image" /><p><span>Tushar Dutta</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tushar Dutta has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jul '16, 15:54</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-49592" class="comments-container"></div><div id="comment-tools-49592" class="comment-tools"></div><div class="clear"></div><div id="comment-49592-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49594"></span>

<div id="answer-container-49594" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49594-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49594-score" class="post-score" title="current number of votes">1</div><span id="post-49594-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Nope, Wireshark (which is free) by itself, won't make VOIP calls faster or better as it's a packet analyzer, not any sort of VOIP software.</p><p>Using the packet analysis functions (warning detailed network knowledge required), someone practiced in the arts of VOIP could possibly analyse the issue and suggest where improvements could be made.</p><p>If you would like assistance with analysis of a capture of your VOIP traffic, then you'll have to make such a capture, and then post in in a publicly accessible place, e.g. Google Driver, Dropbox etc. See the Wiki page on <a href="https://wiki.wireshark.org/CaptureSetup">Capture Setup</a> for a likely place to learn how to start making such a capture.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jan '16, 05:09</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-49594" class="comments-container"></div><div id="comment-tools-49594" class="comment-tools"></div><div class="clear"></div><div id="comment-49594-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

