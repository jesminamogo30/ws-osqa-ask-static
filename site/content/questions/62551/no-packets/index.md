+++
type = "question"
title = "No Packets"
description = '''I started capturing wifi but it writes &quot;no packets&quot; under the middle place of wireshark legacy. It says &quot;live capturing in progress&quot; but nothing changed on the screen. waited for ten minutes. What may be the problem?'''
date = "2017-07-05T22:16:00Z"
lastmod = "2017-07-21T00:08:00Z"
weight = 62551
keywords = [ "no", "packets", "wireshark" ]
aliases = [ "/questions/62551" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No Packets](/questions/62551/no-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62551-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62551-score" class="post-score" title="current number of votes">0</div><span id="post-62551-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I started capturing wifi but it writes "no packets" under the middle place of wireshark legacy. It says "live capturing in progress" but nothing changed on the screen. waited for ten minutes. What may be the problem?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-no" rel="tag" title="see questions tagged &#39;no&#39;">no</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jul '17, 22:16</strong></p><img src="https://secure.gravatar.com/avatar/68ba7039979a2b654686056117957167?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kiv&#39;s gravatar image" /><p><span>kiv</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kiv has no accepted answers">0%</span></p></div></div><div id="comments-container-62551" class="comments-container"><span id="62948"></span><div id="comment-62948" class="comment"><div id="post-62948-score" class="comment-score">1</div><div class="comment-text"><p>Which operating system are you using?</p><p>The first thing to check is that you have selected the correct network interface. If you click on the interface button, do you see Wi-Fi adapters listed?</p></div><div id="comment-62948-info" class="comment-info"><span class="comment-age">(20 Jul '17, 23:31)</span> <span class="comment-user userinfo">PaulOfford</span></div></div><span id="62949"></span><div id="comment-62949" class="comment"><div id="post-62949-score" class="comment-score"></div><div class="comment-text"><p>İ use win. 10. İ see "wi-fi" on network list. Do i have to choose it first? Then what iş next?</p></div><div id="comment-62949-info" class="comment-info"><span class="comment-age">(21 Jul '17, 00:08)</span> <span class="comment-user userinfo">kiv</span></div></div></div><div id="comment-tools-62551" class="comment-tools"></div><div class="clear"></div><div id="comment-62551-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

