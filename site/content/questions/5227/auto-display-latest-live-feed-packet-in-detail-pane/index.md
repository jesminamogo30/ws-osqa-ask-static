+++
type = "question"
title = "Auto-display latest live-feed packet in detail pane?"
description = '''How to auto-display latest live-feed packet in detail pane?'''
date = "2011-07-25T12:07:00Z"
lastmod = "2011-07-26T19:24:00Z"
weight = 5227
keywords = [ "display", "pane" ]
aliases = [ "/questions/5227" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Auto-display latest live-feed packet in detail pane?](/questions/5227/auto-display-latest-live-feed-packet-in-detail-pane)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5227-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5227-score" class="post-score" title="current number of votes">0</div><span id="post-5227-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to auto-display latest live-feed packet in detail pane?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span> <span class="post-tag tag-link-pane" rel="tag" title="see questions tagged &#39;pane&#39;">pane</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jul '11, 12:07</strong></p><img src="https://secure.gravatar.com/avatar/d0d2f9204b0743c80deacebd93b409de?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="crypto9999&#39;s gravatar image" /><p><span>crypto9999</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="crypto9999 has no accepted answers">0%</span></p></div></div><div id="comments-container-5227" class="comments-container"></div><div id="comment-tools-5227" class="comment-tools"></div><div class="clear"></div><div id="comment-5227-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5290"></span>

<div id="answer-container-5290" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5290-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5290-score" class="post-score" title="current number of votes">0</div><span id="post-5290-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Unfortunately, while Wireshark has an option to always show, in the packet list pane, the most recently captured packets, it does not have an option to always select the most recently captured packet. That would be a problem if it were the <em>only</em> behavior available - if you wanted to look at a particular packet while a capture is in progress, it would keep un-selecting that packet and selecting the most recently captured packet - but it could be a useful <em>option</em>. You should file an enhancement request on <a href="http://bugs.wireshark.org/">the Wireshark bugzilla</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jul '11, 19:24</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-5290" class="comments-container"></div><div id="comment-tools-5290" class="comment-tools"></div><div class="clear"></div><div id="comment-5290-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

