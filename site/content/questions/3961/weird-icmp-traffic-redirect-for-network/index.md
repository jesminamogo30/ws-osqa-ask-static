+++
type = "question"
title = "Weird icmp traffic - redirect for network"
description = '''I just started seeing this strange icmp traffic generated from my pc (192.168.0.229). Can anyone explain why is this happening? In addition to my anti virus protection (Symantec) I&#x27;ve tried several other scanners and haven&#x27;t detected anything. The icmp payload seems strange as well (192.168.0.209 is...'''
date = "2011-05-05T12:39:00Z"
lastmod = "2011-05-06T10:22:00Z"
weight = 3961
keywords = [ "redirect", "icmp", "network" ]
aliases = [ "/questions/3961" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Weird icmp traffic - redirect for network](/questions/3961/weird-icmp-traffic-redirect-for-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3961-score" class="post-score" title="current number of votes">0</div><span id="post-3961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just started seeing this strange icmp traffic generated from my pc (192.168.0.229). Can anyone explain why is this happening? In addition to my anti virus protection (Symantec) I've tried several other scanners and haven't detected anything. The icmp payload seems strange as well (192.168.0.209 is not a router, 192.168.0.244 is a domain controller).</p><p>Thank you. <a href="http://img707.imageshack.us/img707/7526/icmp.png">link text</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-redirect" rel="tag" title="see questions tagged &#39;redirect&#39;">redirect</span> <span class="post-tag tag-link-icmp" rel="tag" title="see questions tagged &#39;icmp&#39;">icmp</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '11, 12:39</strong></p><img src="https://secure.gravatar.com/avatar/e7d1d3994349a9ea0554a6430dbe2ec8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="naskop&#39;s gravatar image" /><p><span>naskop</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="naskop has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 May '11, 12:43</strong> </span></p></div></div><div id="comments-container-3961" class="comments-container"><span id="3962"></span><div id="comment-3962" class="comment"><div id="post-3962-score" class="comment-score"></div><div class="comment-text"><p>http://img707.imageshack.us/img707/7526/icmp.png</p></div><div id="comment-3962-info" class="comment-info"><span class="comment-age">(05 May '11, 12:40)</span> <span class="comment-user userinfo">naskop</span></div></div><span id="3963"></span><div id="comment-3963" class="comment"><div id="post-3963-score" class="comment-score"></div><div class="comment-text"><p>And Laura, my name's not Fred :)</p></div><div id="comment-3963-info" class="comment-info"><span class="comment-age">(05 May '11, 13:59)</span> <span class="comment-user userinfo">naskop</span></div></div><span id="3964"></span><div id="comment-3964" class="comment"><div id="post-3964-score" class="comment-score"></div><div class="comment-text"><p>Can you make the actual capture file available, instead of a picture? What I'm seeing doesn't make sense to me, and I'd like to be able to poke around a bit.</p></div><div id="comment-3964-info" class="comment-info"><span class="comment-age">(05 May '11, 15:06)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div><span id="3981"></span><div id="comment-3981" class="comment"><div id="post-3981-score" class="comment-score"></div><div class="comment-text"><p>It seems to be malware related. I've reinstalled my computer since and I don't see such traffic anymore. I will try to clean up the tracefile and post it for reference.</p></div><div id="comment-3981-info" class="comment-info"><span class="comment-age">(06 May '11, 10:22)</span> <span class="comment-user userinfo">naskop</span></div></div></div><div id="comment-tools-3961" class="comment-tools"></div><div class="clear"></div><div id="comment-3961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="3972"></span>

<div id="answer-container-3972" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3972-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3972-score" class="post-score" title="current number of votes">2</div><span id="post-3972-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What's most likely going on is a typo, in the IP configuration in the DHCP server in this network.</p><p>From your picture it occurs that your PC (192.168.0.229) is being hammered with traffic intended for the gateway (192.168.0.209). Since your PC gets these packets, and does seem to know what the gateway address is (fixed config maybe?) it sends out these redirects where to actually find the gateway.</p><p>If even 192.168.0.209 isn't your gateway, then you really need to do some auditing of your network configuration.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 May '11, 23:40</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-3972" class="comments-container"></div><div id="comment-tools-3972" class="comment-tools"></div><div class="clear"></div><div id="comment-3972-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="3979"></span>

<div id="answer-container-3979" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3979-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3979-score" class="post-score" title="current number of votes">1</div><span id="post-3979-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I agree with @Jaap that the DHCP server is probably handing out a wrong gateway address, but it looks like there's an additional misconfiguration. The ICMP redirect shown on the graphic is for a DNS response from 192.168.0.244 to 192.168.0.209. If a standard /24 subnet mask is in use, then the PC, the DC, and the router would all be on the same subnet. In that case, the packet should have gone directly from .244 to .209 in a Layer 2 Ethernet frame. The DC (.244) thinks it's on the same subnet as the PC (.229) but but a different subnet from the router (.209), because it tried to communicate with the router indirectly using the PC as a gateway. And the PC thinks it's on the same subnet as the router, because it issued a redirect for the router's address. It looks like these three systems don't agree on the subnet mask. FYI, a mask of /27 or higher would cause the DC to believe that it's on the same subnet as the PC but a different subnet from the router.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 May '11, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-3979" class="comments-container"></div><div id="comment-tools-3979" class="comment-tools"></div><div class="clear"></div><div id="comment-3979-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

