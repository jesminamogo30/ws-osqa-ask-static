+++
type = "question"
title = "Vmware pcoip"
description = '''Hi I am trying to access vmware pcoip through asa ipsec tunnel but unable to go through , i am unable to figure this out what is happening, here is the capture on ASA internal interface http://cloudshark.org/captures/dcf23d2c73bd thnks'''
date = "2013-10-07T08:27:00Z"
lastmod = "2013-10-07T08:27:00Z"
weight = 25717
keywords = [ "pcoip", "vmware" ]
aliases = [ "/questions/25717" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Vmware pcoip](/questions/25717/vmware-pcoip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25717-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25717-score" class="post-score" title="current number of votes">0</div><span id="post-25717-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am trying to access vmware pcoip through asa ipsec tunnel but unable to go through , i am unable to figure this out what is happening, here is the capture on ASA internal interface <a href="http://cloudshark.org/captures/dcf23d2c73bd">http://cloudshark.org/captures/dcf23d2c73bd</a></p><p>thnks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcoip" rel="tag" title="see questions tagged &#39;pcoip&#39;">pcoip</span> <span class="post-tag tag-link-vmware" rel="tag" title="see questions tagged &#39;vmware&#39;">vmware</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '13, 08:27</strong></p><img src="https://secure.gravatar.com/avatar/0822b856a5ac48f5b03ded3357b99af2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="imrans&#39;s gravatar image" /><p><span>imrans</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="imrans has no accepted answers">0%</span></p></div></div><div id="comments-container-25717" class="comments-container"></div><div id="comment-tools-25717" class="comment-tools"></div><div class="clear"></div><div id="comment-25717-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

