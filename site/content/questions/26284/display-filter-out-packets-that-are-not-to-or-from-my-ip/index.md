+++
type = "question"
title = "display-filter out packets that are not to or from my IP"
description = '''I should also mention that I am connected to a wifi router and so maybe I won&#x27;t be able to see traffic that is not intended for my machine ?'''
date = "2013-10-22T02:40:00Z"
lastmod = "2013-10-23T15:49:00Z"
weight = 26284
keywords = [ "to", "router", "from", "display-filter" ]
aliases = [ "/questions/26284" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [display-filter out packets that are not to or from my IP](/questions/26284/display-filter-out-packets-that-are-not-to-or-from-my-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26284-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26284-score" class="post-score" title="current number of votes">0</div><span id="post-26284-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I should also mention that I am connected to a wifi router and so maybe I won't be able to see traffic that is not intended for my machine ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-to" rel="tag" title="see questions tagged &#39;to&#39;">to</span> <span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-from" rel="tag" title="see questions tagged &#39;from&#39;">from</span> <span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Oct '13, 02:40</strong></p><img src="https://secure.gravatar.com/avatar/94eb051be96f49a1665b097330fd97bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ychaouche&#39;s gravatar image" /><p><span>ychaouche</span><br />
<span class="score" title="31 reputation points">31</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ychaouche has one accepted answer">100%</span></p></div></div><div id="comments-container-26284" class="comments-container"></div><div id="comment-tools-26284" class="comment-tools"></div><div class="clear"></div><div id="comment-26284-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26337"></span>

<div id="answer-container-26337" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26337-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26337-score" class="post-score" title="current number of votes">0</div><span id="post-26337-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This works for me:</p><p>!ip.addr==192.168.1.100</p><p>Replace IP addr with your current IP ;)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Oct '13, 15:49</strong></p><img src="https://secure.gravatar.com/avatar/3d1f94fd059d26805abac57ed6299bc9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="randyp&#39;s gravatar image" /><p><span>randyp</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="randyp has no accepted answers">0%</span></p></div></div><div id="comments-container-26337" class="comments-container"></div><div id="comment-tools-26337" class="comment-tools"></div><div class="clear"></div><div id="comment-26337-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

