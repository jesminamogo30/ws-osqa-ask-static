+++
type = "question"
title = "Apple Airport Extreme / Time Capsule &#x27;Enable Default Host&#x27; option"
description = '''Hey, So this is the setup... I&#x27;ve got a Time Capsule and I&#x27;ve set the &#x27;Enable default host&#x27; option which, from what I understand, redirects ALL traffic unaltered to a given IP address. All works as expected i.e. not a lot of traffic before to loads of traffic after activating. However, all the traff...'''
date = "2013-05-04T12:47:00Z"
lastmod = "2013-05-04T12:47:00Z"
weight = 20952
keywords = [ "default", "capsule", "host", "apple", "time" ]
aliases = [ "/questions/20952" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Apple Airport Extreme / Time Capsule 'Enable Default Host' option](/questions/20952/apple-airport-extreme-time-capsule-enable-default-host-option)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20952-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20952-score" class="post-score" title="current number of votes">0</div><span id="post-20952-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey,</p><p>So this is the setup... I've got a Time Capsule and I've set the 'Enable default host' option which, from what I understand, redirects ALL traffic unaltered to a given IP address.</p><p>All works as expected i.e. not a lot of traffic before to loads of traffic after activating. However, all the traffic now has the default host IP address removing the intended machines IP address.</p><p>I would like to know if the original intended machines IP address is preserved in the data and if so the name of the column I can apply. Sounds like a bit of a long shot but it would help identify the machine receiving the packet.</p><p>Thanks for help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-default" rel="tag" title="see questions tagged &#39;default&#39;">default</span> <span class="post-tag tag-link-capsule" rel="tag" title="see questions tagged &#39;capsule&#39;">capsule</span> <span class="post-tag tag-link-host" rel="tag" title="see questions tagged &#39;host&#39;">host</span> <span class="post-tag tag-link-apple" rel="tag" title="see questions tagged &#39;apple&#39;">apple</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 May '13, 12:47</strong></p><img src="https://secure.gravatar.com/avatar/bb0dd8c140ac683baf24b6438c825c87?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ASGR&#39;s gravatar image" /><p><span>ASGR</span><br />
<span class="score" title="20 reputation points">20</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ASGR has no accepted answers">0%</span></p></div></div><div id="comments-container-20952" class="comments-container"></div><div id="comment-tools-20952" class="comment-tools"></div><div class="clear"></div><div id="comment-20952-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

