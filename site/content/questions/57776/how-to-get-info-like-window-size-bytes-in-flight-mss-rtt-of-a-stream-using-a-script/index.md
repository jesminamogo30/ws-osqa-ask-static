+++
type = "question"
title = "How to get info like window size, bytes in flight, MSS, RTT of a stream using a script."
description = '''My use case is : I will have two conversations(streams) separated out as pcap in a folder. I need to compare bytes in flight, window size, MSS value (both server advertised and client), avg RTT, number of retransmissions etc for these two flows. Is there any tool or script using tshark or other tool...'''
date = "2016-12-01T22:23:00Z"
lastmod = "2016-12-01T22:23:00Z"
weight = 57776
keywords = [ "rtt", "retransmssion", "tshark", "scripting", "tcp-bytes-in-flight" ]
aliases = [ "/questions/57776" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to get info like window size, bytes in flight, MSS, RTT of a stream using a script.](/questions/57776/how-to-get-info-like-window-size-bytes-in-flight-mss-rtt-of-a-stream-using-a-script)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57776-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57776-score" class="post-score" title="current number of votes">0</div><span id="post-57776-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My use case is : I will have two conversations(streams) separated out as pcap in a folder. I need to compare bytes in flight, window size, MSS value (both server advertised and client), avg RTT, number of retransmissions etc for these two flows. Is there any tool or script using tshark or other tools to get these info and populate to excel sheet. Basic is to parse the file and get these details, later i can manipulate how to present them. Looked on web, there are so many tools like pcaplusplus splitter etc, but i need cmd line options so that I can prepare a perl/python/bash script to automate this. Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtt" rel="tag" title="see questions tagged &#39;rtt&#39;">rtt</span> <span class="post-tag tag-link-retransmssion" rel="tag" title="see questions tagged &#39;retransmssion&#39;">retransmssion</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-scripting" rel="tag" title="see questions tagged &#39;scripting&#39;">scripting</span> <span class="post-tag tag-link-tcp-bytes-in-flight" rel="tag" title="see questions tagged &#39;tcp-bytes-in-flight&#39;">tcp-bytes-in-flight</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Dec '16, 22:23</strong></p><img src="https://secure.gravatar.com/avatar/7c1c3e29d91ab5219747ce86c8c50dc2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="avendra1989&#39;s gravatar image" /><p><span>avendra1989</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="avendra1989 has no accepted answers">0%</span></p></div></div><div id="comments-container-57776" class="comments-container"></div><div id="comment-tools-57776" class="comment-tools"></div><div class="clear"></div><div id="comment-57776-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

