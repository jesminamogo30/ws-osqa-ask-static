+++
type = "question"
title = "Can&#x27;t see BSSAP messages at wireshark"
description = '''Hi, I am trying to see the BSSAP (VLR) messages at a wireshark trace but when I put &quot;bssap&quot; in the wireshark filter it doesn&#x27;t show anything. The SSN for BSSAP in my wireshark preferences is 98. Should I change anything?'''
date = "2015-02-25T06:35:00Z"
lastmod = "2015-02-25T06:35:00Z"
weight = 40069
keywords = [ "bssap", "vlr" ]
aliases = [ "/questions/40069" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't see BSSAP messages at wireshark](/questions/40069/cant-see-bssap-messages-at-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40069-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40069-score" class="post-score" title="current number of votes">0</div><span id="post-40069-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to see the BSSAP (VLR) messages at a wireshark trace but when I put "bssap" in the wireshark filter it doesn't show anything.</p><p>The SSN for BSSAP in my wireshark preferences is 98.</p><p>Should I change anything?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bssap" rel="tag" title="see questions tagged &#39;bssap&#39;">bssap</span> <span class="post-tag tag-link-vlr" rel="tag" title="see questions tagged &#39;vlr&#39;">vlr</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Feb '15, 06:35</strong></p><img src="https://secure.gravatar.com/avatar/d8a12d1a9f0522530f85690edda833ff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Thodoris&#39;s gravatar image" /><p><span>Thodoris</span><br />
<span class="score" title="10 reputation points">10</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Thodoris has no accepted answers">0%</span></p></div></div><div id="comments-container-40069" class="comments-container"></div><div id="comment-tools-40069" class="comment-tools"></div><div class="clear"></div><div id="comment-40069-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

