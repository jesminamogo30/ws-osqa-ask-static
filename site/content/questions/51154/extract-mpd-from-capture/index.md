+++
type = "question"
title = "Extract MPD from capture"
description = '''How obtain MPD file with Wireshark or other tools?'''
date = "2016-03-24T07:25:00Z"
lastmod = "2016-03-24T07:32:00Z"
weight = 51154
keywords = [ "extract" ]
aliases = [ "/questions/51154" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Extract MPD from capture](/questions/51154/extract-mpd-from-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51154-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51154-score" class="post-score" title="current number of votes">0</div><span id="post-51154-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How obtain MPD file with Wireshark or other tools?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-extract" rel="tag" title="see questions tagged &#39;extract&#39;">extract</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Mar '16, 07:25</strong></p><img src="https://secure.gravatar.com/avatar/e39189b0c109c57cc6a955af9bc88429?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="adrian%20haro&#39;s gravatar image" /><p><span>adrian haro</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="adrian haro has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>24 Mar '16, 07:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-51154" class="comments-container"><span id="51155"></span><div id="comment-51155" class="comment"><div id="post-51155-score" class="comment-score"></div><div class="comment-text"><p>What do you mean by "MPD file"?</p></div><div id="comment-51155-info" class="comment-info"><span class="comment-age">(24 Mar '16, 07:32)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-51154" class="comment-tools"></div><div class="clear"></div><div id="comment-51154-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

