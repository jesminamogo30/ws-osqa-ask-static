+++
type = "question"
title = "Wireshark not capture HTTP, TCP"
description = '''Hello! I am new to using Wireshark and I can not capture packets from other protocols PC&#x27;sa not (NBNS, ARP, LLMNR, BROWSER) The idea is to see HTTP, TCP. I tell them I have a wired network, I hope it&#x27;s the right place to ask the question because nowhere in this forum I found the answer. I have a wir...'''
date = "2015-08-24T08:35:00Z"
lastmod = "2015-08-24T18:19:00Z"
weight = 45319
keywords = [ "protocol" ]
aliases = [ "/questions/45319" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not capture HTTP, TCP](/questions/45319/wireshark-not-capture-http-tcp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45319-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45319-score" class="post-score" title="current number of votes">0</div><span id="post-45319-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello! I am new to using Wireshark and I can not capture packets from other protocols PC'sa not (NBNS, ARP, LLMNR, BROWSER) The idea is to see HTTP, TCP.</p><p>I tell them I have a wired network, I hope it's the right place to ask the question because nowhere in this forum I found the answer.</p><p>I have a wired network as I said before with a PC that makes firewall, all Windows PC and from a wired notebook "" no wifi "" I'm running the Wireshark in promiscuous mode configured from the application, but I can not see above protocols.</p><p>clean the ARP table. What if my plate is not ethernet "I have a wired LAN" the need to force from my windows to work in promiscuous mode or monitor.</p><p>Am i doing something wrong? Only it works on WIFI? My plate will not be supported in monitor mode?</p><p>I hope you can help me and thank you for reading my post !!!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Aug '15, 08:35</strong></p><img src="https://secure.gravatar.com/avatar/c57cb9db1b3d62e978383a9967070a77?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lsaida&#39;s gravatar image" /><p><span>lsaida</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lsaida has no accepted answers">0%</span></p></div></div><div id="comments-container-45319" class="comments-container"></div><div id="comment-tools-45319" class="comment-tools"></div><div class="clear"></div><div id="comment-45319-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="45320"></span>

<div id="answer-container-45320" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45320-score" class="post-score" title="current number of votes">1</div><span id="post-45320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you read and follow the recommendations on the following Wiki page regarding capturing traffic on a wired network?</p><p><a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">https://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Aug '15, 10:47</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-45320" class="comments-container"></div><div id="comment-tools-45320" class="comment-tools"></div><div class="clear"></div><div id="comment-45320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="45322"></span>

<div id="answer-container-45322" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45322-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45322-score" class="post-score" title="current number of votes">0</div><span id="post-45322-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I would confirm that I'm taking a trace on the right interface. In Wireshark, click Capture &gt; Interfaces and confirm you have enabled the trace on the interface that is getting the packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Aug '15, 11:45</strong></p><img src="https://secure.gravatar.com/avatar/32272e9efae0156b7a71e9b634428d14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smp&#39;s gravatar image" /><p><span>smp</span><br />
<span class="score" title="39 reputation points">39</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smp has no accepted answers">0%</span></p></div></div><div id="comments-container-45322" class="comments-container"><span id="45330"></span><div id="comment-45330" class="comment"><div id="post-45330-score" class="comment-score"></div><div class="comment-text"><p>Sorry for the additional questions, but I am trying to understand your configuration.</p><ol><li><p>Are you trying to capture traffic on the same computer that is generating traffic? Or are you trying to capture traffic from another computer on the network?</p></li><li><p>When you begin a capture in Wireshark, what interfaces do you see in the Capture -&gt; Interfaces dialog box?</p></li><li><p>Have you tried to start Wireshark, begin the capture on the interface that connects to your Ethernet LAN, open a web browser and go to a web page (for example www.google.com)?</p></li><li><p>After stopping the capture do you see any traffic such as ARP, DNS, ect.?</p></li></ol></div><div id="comment-45330-info" class="comment-info"><span class="comment-age">(24 Aug '15, 18:19)</span> <span class="comment-user userinfo">Amato_C</span></div></div></div><div id="comment-tools-45322" class="comment-tools"></div><div class="clear"></div><div id="comment-45322-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

