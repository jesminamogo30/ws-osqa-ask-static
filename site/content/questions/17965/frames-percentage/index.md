+++
type = "question"
title = "FRAMES percentage"
description = '''How do I ascertain the percentage of management frames vs data frames? Edit: changed all upper case characters to resonable cases. Hint : you should check if your CAPS lock key is stuck, or if someone stole all your lower case letters ;-)'''
date = "2013-01-26T08:26:00Z"
lastmod = "2013-01-26T11:43:00Z"
weight = 17965
keywords = [ "percent" ]
aliases = [ "/questions/17965" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [FRAMES percentage](/questions/17965/frames-percentage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17965-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17965-score" class="post-score" title="current number of votes">0</div><span id="post-17965-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I ascertain the percentage of management frames vs data frames?</p><p>Edit: changed all upper case characters to resonable cases. Hint : you should check if your CAPS lock key is stuck, or if someone stole all your lower case letters ;-)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-percent" rel="tag" title="see questions tagged &#39;percent&#39;">percent</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jan '13, 08:26</strong></p><img src="https://secure.gravatar.com/avatar/0fca65dce0e1e1020b282d219a0fc87c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SEGACIOUSBLONDE&#39;s gravatar image" /><p><span>SEGACIOUSBLONDE</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SEGACIOUSBLONDE has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Jan '13, 08:36</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-17965" class="comments-container"></div><div id="comment-tools-17965" class="comment-tools"></div><div class="clear"></div><div id="comment-17965-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17966"></span>

<div id="answer-container-17966" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17966-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17966-score" class="post-score" title="current number of votes">0</div><span id="post-17966-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You could try to find a filter that will only show management frames, or will only show data frames. This usually depends on your protocols, so there is no generic answer for this, except maybe filtering on TCP packets that have a payload length of 0.</p><p>If you use the latest development builds of Wireshark (1.9.x) you'll notice that the status bar will tell you how many packets are still displayed by the filter, and what that percentage is compared to all frames in the file. Older versions will not yet show the percentage.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jan '13, 08:39</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-17966" class="comments-container"><span id="17968"></span><div id="comment-17968" class="comment"><div id="post-17968-score" class="comment-score"></div><div class="comment-text"><p>TY FOR YOUR HELP!! SORRY IF UPPERCASE LETTERS BOTHER YOU. I HAVE A VISION PROBLEM DUE TO AN ACCIDENT. I DON'T SEE WHY PEOPLE USE ALL LOWER CASE LETTERS (NO PUN INTENDED.)</p></div><div id="comment-17968-info" class="comment-info"><span class="comment-age">(26 Jan '13, 10:04)</span> <span class="comment-user userinfo">SEGACIOUSBLONDE</span></div></div><span id="17971"></span><div id="comment-17971" class="comment"><div id="post-17971-score" class="comment-score"></div><div class="comment-text"><p>SORRY TO HEAR THAT. UPPERCASE IS USUALLY CONSIDERED "SCREAMING" AT SOMEONE, SO THAT IS WHY.</p></div><div id="comment-17971-info" class="comment-info"><span class="comment-age">(26 Jan '13, 11:43)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-17966" class="comment-tools"></div><div class="clear"></div><div id="comment-17966-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

