+++
type = "question"
title = "non-shared tshark preferences"
description = '''Hi, Tshark uses the same search path as Wireshark to look for preferences. But sometimes I&#x27;d like to let Tshark use a completely different setting, typically prepared in a temporary file, without interfering with the system- or user-wide Wireshark setting. Is that doable ? (one would expect an envir...'''
date = "2013-11-07T10:05:00Z"
lastmod = "2013-11-07T12:49:00Z"
weight = 26726
keywords = [ "preferences" ]
aliases = [ "/questions/26726" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [non-shared tshark preferences](/questions/26726/non-shared-tshark-preferences)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26726-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26726-score" class="post-score" title="current number of votes">0</div><span id="post-26726-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Tshark uses the same search path as Wireshark to look for preferences. But sometimes I'd like to let Tshark use a completely different setting, typically prepared in a temporary file, without interfering with the system- or user-wide Wireshark setting. Is that doable ? (one would expect an environment var or an option for this; none seem to be documented)</p><p>Thanks in advance,</p><p>-Alex</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-preferences" rel="tag" title="see questions tagged &#39;preferences&#39;">preferences</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Nov '13, 10:05</strong></p><img src="https://secure.gravatar.com/avatar/07fc262875e59f31b8ef37b3c93fee6b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ferrieux&#39;s gravatar image" /><p><span>ferrieux</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ferrieux has no accepted answers">0%</span></p></div></div><div id="comments-container-26726" class="comments-container"></div><div id="comment-tools-26726" class="comment-tools"></div><div class="clear"></div><div id="comment-26726-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26731"></span>

<div id="answer-container-26731" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26731-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26731-score" class="post-score" title="current number of votes">0</div><span id="post-26731-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>For protocol preferences, at least, the <code>-o</code> flag can be used in TShark to set a preference from the command line for that particular instance of TShark.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Nov '13, 12:49</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-26731" class="comments-container"></div><div id="comment-tools-26731" class="comment-tools"></div><div class="clear"></div><div id="comment-26731-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

