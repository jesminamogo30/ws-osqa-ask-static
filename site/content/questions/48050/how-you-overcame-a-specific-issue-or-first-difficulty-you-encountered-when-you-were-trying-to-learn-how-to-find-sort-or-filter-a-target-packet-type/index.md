+++
type = "question"
title = "[closed] How you overcame a specific issue or first difficulty you encountered when you were trying to learn how to find, sort or filter a target packet type"
description = '''How you overcame a specific issue or first difficulty you encountered when you were trying to learn how to find, sort or filter a target packet type'''
date = "2015-11-28T22:57:00Z"
lastmod = "2015-11-29T14:53:00Z"
weight = 48050
keywords = [ "wireshark" ]
aliases = [ "/questions/48050" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How you overcame a specific issue or first difficulty you encountered when you were trying to learn how to find, sort or filter a target packet type](/questions/48050/how-you-overcame-a-specific-issue-or-first-difficulty-you-encountered-when-you-were-trying-to-learn-how-to-find-sort-or-filter-a-target-packet-type)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48050-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48050-score" class="post-score" title="current number of votes">0</div><span id="post-48050-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How you overcame a specific issue or first difficulty you encountered when you were trying to learn how to find, sort or filter a target packet type</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Nov '15, 22:57</strong></p><img src="https://secure.gravatar.com/avatar/872f48404e24eeb7dc670848d6faf7dd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vamsi%20Krishna&#39;s gravatar image" /><p><span>Vamsi Krishna</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vamsi Krishna has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>29 Nov '15, 14:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-48050" class="comments-container"><span id="48055"></span><div id="comment-48055" class="comment"><div id="post-48055-score" class="comment-score"></div><div class="comment-text"><p>The question is unclear.</p><p>Can you provide more information: e.g., a detailed description of a specific issue ?</p></div><div id="comment-48055-info" class="comment-info"><span class="comment-age">(29 Nov '15, 06:21)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div><span id="48063"></span><div id="comment-48063" class="comment"><div id="post-48063-score" class="comment-score"></div><div class="comment-text"><p>Unfortunately, this isn't really a good question for a Q&amp;A site such as Ask Wireshark. We need a specific question to which specific answers can be provided.</p></div><div id="comment-48063-info" class="comment-info"><span class="comment-age">(29 Nov '15, 14:53)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-48050" class="comment-tools"></div><div class="clear"></div><div id="comment-48050-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 29 Nov '15, 14:53

</div>

</div>

</div>

