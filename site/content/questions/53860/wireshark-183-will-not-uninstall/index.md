+++
type = "question"
title = "Wireshark 1.8.3 will not uninstall"
description = '''I am trying to remove Wireshark from my Windows 2008r2 server. I go to the wireshark directory and right click uninstall.exe and run as administrator. It starts the uninstall process but them fails and says rawshark.exe is the problem. I check task manager &amp;gt;&amp;gt;Show all processes with no rawshark...'''
date = "2016-07-06T12:08:00Z"
lastmod = "2016-07-08T07:25:00Z"
weight = 53860
keywords = [ "win2008r2", "rawshark", "wireshark", "uninstall" ]
aliases = [ "/questions/53860" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 1.8.3 will not uninstall](/questions/53860/wireshark-183-will-not-uninstall)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53860-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53860-score" class="post-score" title="current number of votes">0</div><span id="post-53860-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to remove Wireshark from my Windows 2008r2 server. I go to the wireshark directory and right click uninstall.exe and run as administrator. It starts the uninstall process but them fails and says rawshark.exe is the problem. I check task manager &gt;&gt;Show all processes with no rawshark.exe running. Another post says delete Rawshark. I searched for rawshark.exe and no results found. Can anyone help with this problem?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-win2008r2" rel="tag" title="see questions tagged &#39;win2008r2&#39;">win2008r2</span> <span class="post-tag tag-link-rawshark" rel="tag" title="see questions tagged &#39;rawshark&#39;">rawshark</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-uninstall" rel="tag" title="see questions tagged &#39;uninstall&#39;">uninstall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jul '16, 12:08</strong></p><img src="https://secure.gravatar.com/avatar/54cd46f3ba26905f2a674c59f98aac43?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JohnO&#39;s gravatar image" /><p><span>JohnO</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JohnO has no accepted answers">0%</span></p></div></div><div id="comments-container-53860" class="comments-container"><span id="53892"></span><div id="comment-53892" class="comment"><div id="post-53892-score" class="comment-score"></div><div class="comment-text"><p>Have you tried uninstalling from the "Programs and Features" dialog, not that I think that will make much difference?</p><p>Can you rename rawshark.exe in the Wireshark installation directory?</p></div><div id="comment-53892-info" class="comment-info"><span class="comment-age">(07 Jul '16, 03:26)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="53903"></span><div id="comment-53903" class="comment"><div id="post-53903-score" class="comment-score"></div><div class="comment-text"><p>Grahamb, Thanks for the quick reply. I have tried to use programs and features (1st attempt to remove) Which then led me to a google search. I then tried to use the uninstall.exe in the wireshark directory with run as Administrator. Same error (rawshark.exe) I then tried the suggestion to manually delete rawshark.exe. That file was not found anywhere. But yet running the uninstall still fails with the error stating rawshark can not be removed it is in use etc. I have rebooted and still the same results. Any other ideas are greatly appreciated.</p></div><div id="comment-53903-info" class="comment-info"><span class="comment-age">(07 Jul '16, 07:21)</span> <span class="comment-user userinfo">JohnO</span></div></div><span id="53904"></span><div id="comment-53904" class="comment"><div id="post-53904-score" class="comment-score"></div><div class="comment-text"><p>Can you show the exact error message?</p></div><div id="comment-53904-info" class="comment-info"><span class="comment-age">(07 Jul '16, 07:30)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="53937"></span><div id="comment-53937" class="comment"><div id="post-53937-score" class="comment-score"></div><div class="comment-text"><p>Do you have <a href="https://ask.wireshark.org/questions/27855/cant-uninstall-wireshark-on-win8-64bit">zonealarm</a> installed?</p></div><div id="comment-53937-info" class="comment-info"><span class="comment-age">(08 Jul '16, 07:25)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-53860" class="comment-tools"></div><div class="clear"></div><div id="comment-53860-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

