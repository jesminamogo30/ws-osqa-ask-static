+++
type = "question"
title = "GeoIP setting Source address"
description = '''Is there any way to set the local address while using GeoIP? I&#x27;m doing a capture from a 192.168 address and it doesn&#x27;t know where it&#x27;s located. I know it&#x27;s a private address and the DB has no way of knowing where it&#x27;s located.'''
date = "2017-01-21T11:17:00Z"
lastmod = "2017-01-24T02:20:00Z"
weight = 58933
keywords = [ "edit", "geoip" ]
aliases = [ "/questions/58933" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [GeoIP setting Source address](/questions/58933/geoip-setting-source-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58933-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58933-score" class="post-score" title="current number of votes">0</div><span id="post-58933-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any way to set the local address while using GeoIP? I'm doing a capture from a 192.168 address and it doesn't know where it's located. I know it's a private address and the DB has no way of knowing where it's located.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-edit" rel="tag" title="see questions tagged &#39;edit&#39;">edit</span> <span class="post-tag tag-link-geoip" rel="tag" title="see questions tagged &#39;geoip&#39;">geoip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '17, 11:17</strong></p><img src="https://secure.gravatar.com/avatar/5e8eb5965aac554dd94d2d3fe9aa8dad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jeff%20in%20RTP&#39;s gravatar image" /><p><span>jeff in RTP</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jeff in RTP has no accepted answers">0%</span></p></div></div><div id="comments-container-58933" class="comments-container"></div><div id="comment-tools-58933" class="comment-tools"></div><div class="clear"></div><div id="comment-58933-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59005"></span>

<div id="answer-container-59005" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59005-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59005-score" class="post-score" title="current number of votes">0</div><span id="post-59005-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Currently only GeoIP databases are supported (files beginning with "Geo" and ending with ".dat").</p><p>To add your local RFC1918 addresses it would be necessary to build your own database. Maybe <a href="https://github.com/threatstream/mhn/wiki/Customizing-Maxmind-IP-Geo-DB-for-Internal-Networks">this github repo</a> is a start.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jan '17, 02:20</strong></p><img src="https://secure.gravatar.com/avatar/11cda2a4be5391632a5b28af1927307b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Uli&#39;s gravatar image" /><p><span>Uli</span><br />
<span class="score" title="903 reputation points">903</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Uli has 16 accepted answers">29%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Jan '17, 02:22</strong> </span></p></div></div><div id="comments-container-59005" class="comments-container"></div><div id="comment-tools-59005" class="comment-tools"></div><div class="clear"></div><div id="comment-59005-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

