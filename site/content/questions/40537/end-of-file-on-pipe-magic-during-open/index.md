+++
type = "question"
title = "End of file on pipe magic during open"
description = '''Hi, I am currently doing some work on gns3 using virtual routers loaded on virtualbox on linux. I get an error message that says &quot;end of file on pipe magic during open&quot; whenever i try to start wireshark. Any help please? Thanks in advance.  Best Regards.'''
date = "2015-03-13T05:26:00Z"
lastmod = "2015-03-16T02:47:00Z"
weight = 40537
keywords = [ "gns3", "wireshark", "error" ]
aliases = [ "/questions/40537" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [End of file on pipe magic during open](/questions/40537/end-of-file-on-pipe-magic-during-open)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40537-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40537-score" class="post-score" title="current number of votes">0</div><span id="post-40537-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am currently doing some work on gns3 using virtual routers loaded on virtualbox on linux. I get an error message that says "end of file on pipe magic during open" whenever i try to start wireshark. Any help please? Thanks in advance.</p><p>Best Regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gns3" rel="tag" title="see questions tagged &#39;gns3&#39;">gns3</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Mar '15, 05:26</strong></p><img src="https://secure.gravatar.com/avatar/bc7fdd938bbdd21c3b27f7663eddd255?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="arbeorlar&#39;s gravatar image" /><p><span>arbeorlar</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="arbeorlar has no accepted answers">0%</span></p></div></div><div id="comments-container-40537" class="comments-container"><span id="40606"></span><div id="comment-40606" class="comment"><div id="post-40606-score" class="comment-score"></div><div class="comment-text"><p>Did you do a search on "End of file on pipe magic during open"? Lots of interesting hits.</p><p>You say you start Wireshark, but don't say how. So, how do you start Wireshark?</p></div><div id="comment-40606-info" class="comment-info"><span class="comment-age">(16 Mar '15, 02:47)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-40537" class="comment-tools"></div><div class="clear"></div><div id="comment-40537-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

