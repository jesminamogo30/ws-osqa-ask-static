+++
type = "question"
title = "Decrypt ESP packets with out preshared key Using certificates."
description = '''Hello  I would like to decrypt ESP packets. I have reading the forum where most of the people are doing the same with preshared key. But in my case I am using certificates (x509) for authentication.  Can you please let me know is there a way to decrypt ESP packets with certificates. Thanks Rao'''
date = "2014-02-26T13:57:00Z"
lastmod = "2014-02-26T13:57:00Z"
weight = 30215
keywords = [ "esp", "authrority", "certificate", "ipsec" ]
aliases = [ "/questions/30215" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt ESP packets with out preshared key Using certificates.](/questions/30215/decrypt-esp-packets-with-out-preshared-key-using-certificates)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30215-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30215-score" class="post-score" title="current number of votes">0</div><span id="post-30215-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I would like to decrypt ESP packets. I have reading the forum where most of the people are doing the same with preshared key. But in my case I am using certificates (x509) for authentication. Can you please let me know is there a way to decrypt ESP packets with certificates.</p><p>Thanks Rao</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-esp" rel="tag" title="see questions tagged &#39;esp&#39;">esp</span> <span class="post-tag tag-link-authrority" rel="tag" title="see questions tagged &#39;authrority&#39;">authrority</span> <span class="post-tag tag-link-certificate" rel="tag" title="see questions tagged &#39;certificate&#39;">certificate</span> <span class="post-tag tag-link-ipsec" rel="tag" title="see questions tagged &#39;ipsec&#39;">ipsec</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Feb '14, 13:57</strong></p><img src="https://secure.gravatar.com/avatar/b89095289ce3864f3dc6ec668258b488?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rao&#39;s gravatar image" /><p><span>rao</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rao has no accepted answers">0%</span></p></div></div><div id="comments-container-30215" class="comments-container"></div><div id="comment-tools-30215" class="comment-tools"></div><div class="clear"></div><div id="comment-30215-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

