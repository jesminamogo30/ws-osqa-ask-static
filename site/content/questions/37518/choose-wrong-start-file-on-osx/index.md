+++
type = "question"
title = "Choose wrong start file on OSX"
description = '''Hi I need help. i use Mac os X mavericks and when i installed wireshark i had to tell the program were X11 was i chose the wrong program the mac thinks Apple script editor is X11 to SystemInformation. so I can&#x27;t run wireshark. I have tried to reinstall but that doesn&#x27;t work. '''
date = "2014-11-01T01:39:00Z"
lastmod = "2014-11-02T06:45:00Z"
weight = 37518
keywords = [ "start", "choose", "file" ]
aliases = [ "/questions/37518" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Choose wrong start file on OSX](/questions/37518/choose-wrong-start-file-on-osx)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37518-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37518-score" class="post-score" title="current number of votes">0</div><span id="post-37518-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I need help. i use Mac os X mavericks and when i installed wireshark i had to tell the program were X11 was i chose the wrong program the mac thinks Apple script editor is X11 to SystemInformation. so I can't run wireshark. I have tried to reinstall but that doesn't work.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-start" rel="tag" title="see questions tagged &#39;start&#39;">start</span> <span class="post-tag tag-link-choose" rel="tag" title="see questions tagged &#39;choose&#39;">choose</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Nov '14, 01:39</strong></p><img src="https://secure.gravatar.com/avatar/84162459cb46654dd3767c6ca2a4da9f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deknoy&#39;s gravatar image" /><p><span>deknoy</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deknoy has no accepted answers">0%</span></p></div></div><div id="comments-container-37518" class="comments-container"></div><div id="comment-tools-37518" class="comment-tools"></div><div class="clear"></div><div id="comment-37518-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37540"></span>

<div id="answer-container-37540" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37540-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37540-score" class="post-score" title="current number of votes">0</div><span id="post-37540-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the answers to a similar questions for possible solutions:</p><blockquote><p><a href="https://ask.wireshark.org/questions/29345/wrong-start-file-chosen-for-x11-on-osx">https://ask.wireshark.org/questions/29345/wrong-start-file-chosen-for-x11-on-osx</a><br />
<a href="https://ask.wireshark.org/questions/12140/cant-run-wireshark-in-mac-os-x-mountain-lion">https://ask.wireshark.org/questions/12140/cant-run-wireshark-in-mac-os-x-mountain-lion</a><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Nov '14, 06:45</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Nov '14, 06:47</strong> </span></p></div></div><div id="comments-container-37540" class="comments-container"></div><div id="comment-tools-37540" class="comment-tools"></div><div class="clear"></div><div id="comment-37540-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

