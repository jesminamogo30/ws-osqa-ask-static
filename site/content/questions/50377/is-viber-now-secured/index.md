+++
type = "question"
title = "[closed] Is Viber now secured"
description = '''Does viber have the key to decrypt our messages and photos sent through viber? They claim that they use The state of art encryption and messages are secured. Also they says that when ever we send photos through the app, once they are downloaded to the destination phone the photos are removed automat...'''
date = "2016-02-20T21:50:00Z"
lastmod = "2016-02-21T05:41:00Z"
weight = 50377
keywords = [ "encryption", "viber" ]
aliases = [ "/questions/50377" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Is Viber now secured](/questions/50377/is-viber-now-secured)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50377-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50377-score" class="post-score" title="current number of votes">0</div><span id="post-50377-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does viber have the key to decrypt our messages and photos sent through viber? They claim that they use The state of art encryption and messages are secured. Also they says that when ever we send photos through the app, once they are downloaded to the destination phone the photos are removed automatically from their viber servers.. If they delete such messages what they might share with their third parties to comply with such laws mentioned in there privacy policy? is it just the personal info?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-encryption" rel="tag" title="see questions tagged &#39;encryption&#39;">encryption</span> <span class="post-tag tag-link-viber" rel="tag" title="see questions tagged &#39;viber&#39;">viber</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Feb '16, 21:50</strong></p><img src="https://secure.gravatar.com/avatar/997f29cd16dbb3f20fbc661a97ab1551?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jason%20Hal&#39;s gravatar image" /><p><span>Jason Hal</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jason Hal has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>21 Feb '16, 01:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-50377" class="comments-container"><span id="50379"></span><div id="comment-50379" class="comment"><div id="post-50379-score" class="comment-score"></div><div class="comment-text"><p>Why did Jasper closed the thread?</p></div><div id="comment-50379-info" class="comment-info"><span class="comment-age">(21 Feb '16, 01:39)</span> <span class="comment-user userinfo">Jason Hal</span></div></div><span id="50384"></span><div id="comment-50384" class="comment"><div id="post-50384-score" class="comment-score"></div><div class="comment-text"><p>It's not a Wireshark question, hence not suitable for a Wireshark Q&amp;A site.</p></div><div id="comment-50384-info" class="comment-info"><span class="comment-age">(21 Feb '16, 05:41)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-50377" class="comment-tools"></div><div class="clear"></div><div id="comment-50377-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jasper 21 Feb '16, 01:22

</div>

</div>

</div>

