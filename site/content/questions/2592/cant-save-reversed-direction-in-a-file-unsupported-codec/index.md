+++
type = "question"
title = "Can&#x27;t save reversed direction in a file: Unsupported Codec!"
description = '''Hey guys, I am new to Wireshark I am trying to save a g711A payload. When i try to save in both channels i get the folowwing error: Can&#x27;t save reversed direction in a file: Unsupported Codec! What is the problem: I am running wireshark on a server, connected to it trough RDP. Thanks in advance. Gree...'''
date = "2011-03-01T00:47:00Z"
lastmod = "2011-03-01T00:47:00Z"
weight = 2592
keywords = [ "save", "payload", "stream", "rtp" ]
aliases = [ "/questions/2592" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't save reversed direction in a file: Unsupported Codec!](/questions/2592/cant-save-reversed-direction-in-a-file-unsupported-codec)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2592-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2592-score" class="post-score" title="current number of votes">1</div><span id="post-2592-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey guys, I am new to Wireshark I am trying to save a g711A payload. When i try to save in both channels i get the folowwing error:</p><p>Can't save reversed direction in a file: Unsupported Codec!</p><p>What is the problem: I am running wireshark on a server, connected to it trough RDP. Thanks in advance.</p><p>Greets, Cem</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span> <span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Mar '11, 00:47</strong></p><img src="https://secure.gravatar.com/avatar/13c0548e2f29f1cc5c2b147334637dbb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cemmetje&#39;s gravatar image" /><p><span>cemmetje</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cemmetje has no accepted answers">0%</span></p></div></div><div id="comments-container-2592" class="comments-container"></div><div id="comment-tools-2592" class="comment-tools"></div><div class="clear"></div><div id="comment-2592-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

