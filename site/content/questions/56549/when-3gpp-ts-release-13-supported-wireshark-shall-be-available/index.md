+++
type = "question"
title = "When 3gpp TS Release 13 supported wireshark shall be available ?"
description = '''When 3gpp TS Release 13 supported wireshark shall be available ?'''
date = "2016-10-20T22:39:00Z"
lastmod = "2016-10-21T08:18:00Z"
weight = 56549
keywords = [ "release", "3gpp", "supported", "13", "wireshark" ]
aliases = [ "/questions/56549" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [When 3gpp TS Release 13 supported wireshark shall be available ?](/questions/56549/when-3gpp-ts-release-13-supported-wireshark-shall-be-available)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56549-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56549-score" class="post-score" title="current number of votes">0</div><span id="post-56549-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When 3gpp TS Release 13 supported wireshark shall be available ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-release" rel="tag" title="see questions tagged &#39;release&#39;">release</span> <span class="post-tag tag-link-3gpp" rel="tag" title="see questions tagged &#39;3gpp&#39;">3gpp</span> <span class="post-tag tag-link-supported" rel="tag" title="see questions tagged &#39;supported&#39;">supported</span> <span class="post-tag tag-link-13" rel="tag" title="see questions tagged &#39;13&#39;">13</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Oct '16, 22:39</strong></p><img src="https://secure.gravatar.com/avatar/7c6366899ae981fd30c9174102b9b12b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="AUTORISE&#39;s gravatar image" /><p><span>AUTORISE</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="AUTORISE has no accepted answers">0%</span></p></div></div><div id="comments-container-56549" class="comments-container"></div><div id="comment-tools-56549" class="comment-tools"></div><div class="clear"></div><div id="comment-56549-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="56551"></span>

<div id="answer-container-56551" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56551-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56551-score" class="post-score" title="current number of votes">1</div><span id="post-56551-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In the current master source branch (the one used to generate the Wireshark 2.3.0 development releases), several 3GPP protocols were already upgraded to R13. Could you clarify which protocols you are looking for?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Oct '16, 02:23</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-56551" class="comments-container"><span id="56565"></span><div id="comment-56565" class="comment"><div id="post-56565-score" class="comment-score"></div><div class="comment-text"><p>Thanks for you reply Pascal. I wanted it for Diameter and GTP protocols.</p></div><div id="comment-56565-info" class="comment-info"><span class="comment-age">(21 Oct '16, 06:57)</span> <span class="comment-user userinfo">AUTORISE</span></div></div><span id="56570"></span><div id="comment-56570" class="comment"><div id="post-56570-score" class="comment-score"></div><div class="comment-text"><p>For those it's not so easy as people add what they need and not necessarily everything new in a specific release. But the master should be fairly up to date. The only advice I can give is to try it and bugreport anything missing.</p></div><div id="comment-56570-info" class="comment-info"><span class="comment-age">(21 Oct '16, 08:18)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-56551" class="comment-tools"></div><div class="clear"></div><div id="comment-56551-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="56550"></span>

<div id="answer-container-56550" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56550-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56550-score" class="post-score" title="current number of votes">0</div><span id="post-56550-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It will be available when some party contributes the required changes to the project and these are subsequently merged and released. There is no set release content schedule, due to the fact that this is (mainly) a volunteer project.</p><p>If it's not already in the development builds, you may always create an enhancement request in <a href="https://bugs.wireshark.org/bugzilla/">bugzilla</a> and document your request as well as possible, with references to the applicable standards, sample capture files and whatever is relevant. This can then be picked up by someone willing to implement this.</p><p>And of course if you are willing and able to contribute the changes yourself, you are welcome to push them to <a href="https://wiki.wireshark.org/Development/SubmittingPatches">Gerrit</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Oct '16, 02:05</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-56550" class="comments-container"></div><div id="comment-tools-56550" class="comment-tools"></div><div class="clear"></div><div id="comment-56550-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

