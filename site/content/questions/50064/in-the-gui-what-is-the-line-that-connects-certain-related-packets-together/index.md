+++
type = "question"
title = "In the GUI, what is the line that connects certain related packets together?"
description = '''Don&#x27;t know what to call it to search for it and don&#x27;t see it mentioned anywhere. In the GUI, what is the line that connects certain related packets together? Maybe I don&#x27;t understand how to use this new function but I don&#x27;t like that it seems to make the packet window jump up and down and not stay o...'''
date = "2016-02-10T10:26:00Z"
lastmod = "2016-02-17T07:22:00Z"
weight = 50064
keywords = [ "connector", "gui", "line" ]
aliases = [ "/questions/50064" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [In the GUI, what is the line that connects certain related packets together?](/questions/50064/in-the-gui-what-is-the-line-that-connects-certain-related-packets-together)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50064-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50064-score" class="post-score" title="current number of votes">0</div><span id="post-50064-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Don't know what to call it to search for it and don't see it mentioned anywhere. In the GUI, what is the line that connects certain related packets together? Maybe I don't understand how to use this new function but I don't like that it seems to make the packet window jump up and down and not stay on the last packet like I have selected. I can eventually select a packet that I assume doesn't connect to any others and get it to stop. Is there a way to disable this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connector" rel="tag" title="see questions tagged &#39;connector&#39;">connector</span> <span class="post-tag tag-link-gui" rel="tag" title="see questions tagged &#39;gui&#39;">gui</span> <span class="post-tag tag-link-line" rel="tag" title="see questions tagged &#39;line&#39;">line</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '16, 10:26</strong></p><img src="https://secure.gravatar.com/avatar/0833f7ef8618ac6b7842265fbaa39861?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="itsme0k&#39;s gravatar image" /><p><span>itsme0k</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="itsme0k has no accepted answers">0%</span></p></div></div><div id="comments-container-50064" class="comments-container"></div><div id="comment-tools-50064" class="comment-tools"></div><div class="clear"></div><div id="comment-50064-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="50074"></span>

<div id="answer-container-50074" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50074-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50074-score" class="post-score" title="current number of votes">0</div><span id="post-50074-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>So I found in Preferences, Advanced, (very last item) gui.packet_list_show_related and made that False. That turns off the visual line in the first column but the list still jumps back and forth.</p><p>Just in case you aren't following my description, wtih auto scroll to last packet in live capture = on</p><p>Example</p><p>screen top -----</p><p>No.</p><p>9001</p><p>9002</p><p>9003</p><p>screen bottom ----</p><p>then it all of a sudden jumps to</p><p>screen top -----</p><p>No.</p><p>1001</p><p>1002</p><p>1003</p><p>screen bottom ----</p><p>then jumps back to bottom</p><p>screen top -----</p><p>No.</p><p>9100</p><p>9101</p><p>9102</p><p>screen bottom ----</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Feb '16, 12:26</strong></p><img src="https://secure.gravatar.com/avatar/0833f7ef8618ac6b7842265fbaa39861?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="itsme0k&#39;s gravatar image" /><p><span>itsme0k</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="itsme0k has no accepted answers">0%</span></p></div></div><div id="comments-container-50074" class="comments-container"></div><div id="comment-tools-50074" class="comment-tools"></div><div class="clear"></div><div id="comment-50074-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="50076"></span>

<div id="answer-container-50076" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50076-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50076-score" class="post-score" title="current number of votes">0</div><span id="post-50076-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Maybe I don't understand how to use this new function but I don't like that it seems to make the packet window jump up and down and not stay on the last packet like I have selected.</p></blockquote><p>It's supposed to decorate the display, to indicate related packets, but not prevent it from being stable. Please file a bug on this behavior on <a href="http://bugs.wireshark.org/">the Wireshark Bugzilla</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Feb '16, 14:16</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-50076" class="comments-container"><span id="50269"></span><div id="comment-50269" class="comment"><div id="post-50269-score" class="comment-score"></div><div class="comment-text"><p>Just in case someone comes along later here it is <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12130">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12130</a></p></div><div id="comment-50269-info" class="comment-info"><span class="comment-age">(17 Feb '16, 07:22)</span> <span class="comment-user userinfo">itsme0k</span></div></div></div><div id="comment-tools-50076" class="comment-tools"></div><div class="clear"></div><div id="comment-50076-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

