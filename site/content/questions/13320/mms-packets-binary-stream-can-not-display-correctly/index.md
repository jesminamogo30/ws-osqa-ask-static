+++
type = "question"
title = "MMS packet&#x27;s binary stream can not display correctly"
description = '''On my PC，I found the reassembled MMS packet can not display in 1.8.0 and 1.8.1,please pay attention to this promble,thanks wish Wireshark become more and more powerful and useful if any question,please emailto:zxr1001@126.com'''
date = "2012-08-02T08:41:00Z"
lastmod = "2012-08-06T07:32:00Z"
weight = 13320
keywords = [ "wrong", "1.8.0" ]
aliases = [ "/questions/13320" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [MMS packet's binary stream can not display correctly](/questions/13320/mms-packets-binary-stream-can-not-display-correctly)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13320-score" class="post-score" title="current number of votes">0</div><span id="post-13320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>On my PC，I found the reassembled MMS packet can not display in 1.8.0 and 1.8.1,please pay attention to this promble,thanks wish Wireshark become more and more powerful and useful</p><p>if any question,please emailto:<span class="__cf_email__" data-cfemail="5923212b6869696819">[email protected]</span><a href="http://126.com">126.com</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wrong" rel="tag" title="see questions tagged &#39;wrong&#39;">wrong</span> <span class="post-tag tag-link-1.8.0" rel="tag" title="see questions tagged &#39;1.8.0&#39;">1.8.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Aug '12, 08:41</strong></p><img src="https://secure.gravatar.com/avatar/5cc486c6800c70cc5ef12f4a8bf06709?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zxr&#39;s gravatar image" /><p><span>zxr</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zxr has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Aug '12, 16:43</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-13320" class="comments-container"></div><div id="comment-tools-13320" class="comment-tools"></div><div class="clear"></div><div id="comment-13320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13323"></span>

<div id="answer-container-13323" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13323-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13323-score" class="post-score" title="current number of votes">0</div><span id="post-13323-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Does it work (with the same capture file) in earlier versions?</p><p>Anyway, we'd need to see a sample capture to know what's going on; you could put a sample capture on <a href="http://cloudshark.org">cloudshark.org</a> or open a <a href="https://bugs.wireshark.org">bug report</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Aug '12, 11:42</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-13323" class="comments-container"><span id="13397"></span><div id="comment-13397" class="comment"><div id="post-13397-score" class="comment-score"></div><div class="comment-text"><p>I Capture it with a Android software named Shark on my Phone.The file can display normally in v1.7.1 and before, but from 1.8.0,the reassembled MMS packet don't display . I can't put my file <a href="http://cloudshark.org">cloudshark.org</a>, can you give me a Email address? so i can send the file to you,thanks. you can mali me <span class="__cf_email__" data-cfemail="611b19135051515021">[email protected]</span><a href="http://126.com">126.com</a></p></div><div id="comment-13397-info" class="comment-info"><span class="comment-age">(06 Aug '12, 07:32)</span> <span class="comment-user userinfo">zxr</span></div></div></div><div id="comment-tools-13323" class="comment-tools"></div><div class="clear"></div><div id="comment-13323-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

