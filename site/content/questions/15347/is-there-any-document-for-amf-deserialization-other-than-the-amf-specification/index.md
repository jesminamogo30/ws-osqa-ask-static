+++
type = "question"
title = "Is there any document for amf deserialization other than the amf specification????"
description = '''I am writing a plugin for AMF. I have gone through AMF specification but its not at all helpful to write deserialization code.  Is there any other document which contains detailed explanation for deserialization?????? '''
date = "2012-10-29T20:52:00Z"
lastmod = "2012-10-30T10:10:00Z"
weight = 15347
keywords = [ "amf" ]
aliases = [ "/questions/15347" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Is there any document for amf deserialization other than the amf specification????](/questions/15347/is-there-any-document-for-amf-deserialization-other-than-the-amf-specification)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15347-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15347-score" class="post-score" title="current number of votes">0</div><span id="post-15347-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am writing a plugin for AMF. I have gone through AMF specification but its not at all helpful to write deserialization code. Is there any other document which contains detailed explanation for deserialization??????<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-amf" rel="tag" title="see questions tagged &#39;amf&#39;">amf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '12, 20:52</strong></p><img src="https://secure.gravatar.com/avatar/b0ed262c234b0aa9fae2e5b2d51b14c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Akhil&#39;s gravatar image" /><p><span>Akhil</span><br />
<span class="score" title="53 reputation points">53</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="28 badges"><span class="silver">●</span><span class="badgecount">28</span></span><span title="31 badges"><span class="bronze">●</span><span class="badgecount">31</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Akhil has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-15347" class="comments-container"></div><div id="comment-tools-15347" class="comment-tools"></div><div class="clear"></div><div id="comment-15347-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15383"></span>

<div id="answer-container-15383" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15383-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15383-score" class="post-score" title="current number of votes">0</div><span id="post-15383-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Akhil has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The best place to learn about that is Adobes open source implementation: <a href="http://sourceforge.net/adobe/blazeds/wiki/Overview/">BlazeDS</a></p><p>You can also check any other <a href="http://en.wikipedia.org/wiki/Action_Message_Format">AMF implementation</a>. They all contain serializer/de-serializer code.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Oct '12, 10:10</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-15383" class="comments-container"></div><div id="comment-tools-15383" class="comment-tools"></div><div class="clear"></div><div id="comment-15383-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

