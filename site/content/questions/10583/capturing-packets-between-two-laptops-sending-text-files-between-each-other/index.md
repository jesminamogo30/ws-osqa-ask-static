+++
type = "question"
title = "Capturing packets between two laptops sending text files between each other"
description = '''Hi there, I am currently doing a university project. I am trying to send a text file from one laptop to another over wi-fi, with both laptops connected to the same network. I am then using a third laptop to try and capture the packets of this file transfer but I cannot seem to capture anything comin...'''
date = "2012-05-02T03:59:00Z"
lastmod = "2012-05-02T06:54:00Z"
weight = 10583
keywords = [ "transfer", "wifi", "file", "packet" ]
aliases = [ "/questions/10583" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing packets between two laptops sending text files between each other](/questions/10583/capturing-packets-between-two-laptops-sending-text-files-between-each-other)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10583-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10583-score" class="post-score" title="current number of votes">0</div><span id="post-10583-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there,</p><p>I am currently doing a university project. I am trying to send a text file from one laptop to another over wi-fi, with both laptops connected to the same network. I am then using a third laptop to try and capture the packets of this file transfer but I cannot seem to capture anything coming or going from these two laptops.</p><p>Can anyone please help me?</p><p>Thanks,</p><p>Anish</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-transfer" rel="tag" title="see questions tagged &#39;transfer&#39;">transfer</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '12, 03:59</strong></p><img src="https://secure.gravatar.com/avatar/ca147e9a0a32a00a2125a6571ff45b93?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="4ni5h&#39;s gravatar image" /><p><span>4ni5h</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="4ni5h has no accepted answers">0%</span></p></div></div><div id="comments-container-10583" class="comments-container"></div><div id="comment-tools-10583" class="comment-tools"></div><div class="clear"></div><div id="comment-10583-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="10587"></span>

<div id="answer-container-10587" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10587-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10587-score" class="post-score" title="current number of votes">0</div><span id="post-10587-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>did you check, and follow, the instructions in the WLAN sniffing wiki?</p><p><a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a></p><p>1.) What's the OS on your sniffer machine (Windows, Linux, BSD)?</p><p>2.) Is the Wifi connection encrypted? If so: How (WEP, WPA, etc.)?</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 May '12, 04:45</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-10587" class="comments-container"><span id="10588"></span><div id="comment-10588" class="comment"><div id="post-10588-score" class="comment-score"></div><div class="comment-text"><p>Hi Kurt,</p><p>Thanks for your reply, yes i had a brief look at that a while back but I will look into that in more detail now.</p><p>The sniffer machine is windows 7.</p><p>With regards to the encryption, the router is encrypted with WPA. However I want to carry out the experiment with encryption on and off. When I tried both these tests yesterday I still had trouble finding anything even when encryption was disabled.</p><p>Anish</p></div><div id="comment-10588-info" class="comment-info"><span class="comment-age">(02 May '12, 04:52)</span> <span class="comment-user userinfo">4ni5h</span></div></div><span id="10589"></span><div id="comment-10589" class="comment"><div id="post-10589-score" class="comment-score"></div><div class="comment-text"><p>did you connect the windows 7 sniffer machine to the SSID of the test clients?</p></div><div id="comment-10589-info" class="comment-info"><span class="comment-age">(02 May '12, 04:54)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="10591"></span><div id="comment-10591" class="comment"><div id="post-10591-score" class="comment-score"></div><div class="comment-text"><p>please consider the limitations of Wifi sniffing on the windows platform. Search for "Capturing WLAN traffic on Windows" in the wiki link mentioned above.</p></div><div id="comment-10591-info" class="comment-info"><span class="comment-age">(02 May '12, 04:57)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="10598"></span><div id="comment-10598" class="comment"><div id="post-10598-score" class="comment-score"></div><div class="comment-text"><p>All three laptops were connected wirelessly to the same network.</p></div><div id="comment-10598-info" class="comment-info"><span class="comment-age">(02 May '12, 06:43)</span> <span class="comment-user userinfo">4ni5h</span></div></div><span id="10601"></span><div id="comment-10601" class="comment"><div id="post-10601-score" class="comment-score"></div><div class="comment-text"><p>as menitoned above, please check the wiki and the limitations on windows.</p></div><div id="comment-10601-info" class="comment-info"><span class="comment-age">(02 May '12, 06:54)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-10587" class="comment-tools"></div><div class="clear"></div><div id="comment-10587-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="10592"></span>

<div id="answer-container-10592" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10592-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10592-score" class="post-score" title="current number of votes">0</div><span id="post-10592-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the Wiki page on Wireless LAN capturing <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">HERE</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 May '12, 05:07</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-10592" class="comments-container"></div><div id="comment-tools-10592" class="comment-tools"></div><div class="clear"></div><div id="comment-10592-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

