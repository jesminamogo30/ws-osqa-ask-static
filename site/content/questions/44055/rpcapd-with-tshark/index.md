+++
type = "question"
title = "rpcapd with tshark"
description = '''Hello all, I have tshark running on my local linux pc and i need to trace the traffic from multiple remote linux pc&#x27;s, this should be possible by deploying rpcapd on my remote pc&#x27;s, but how can i add the remote network interfaces for tshark from command line without GUI support? any suggestions woul...'''
date = "2015-07-10T08:28:00Z"
lastmod = "2015-07-10T08:28:00Z"
weight = 44055
keywords = [ "remote-monitoring", "rpcapd" ]
aliases = [ "/questions/44055" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [rpcapd with tshark](/questions/44055/rpcapd-with-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44055-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44055-score" class="post-score" title="current number of votes">0</div><span id="post-44055-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all, I have tshark running on my local linux pc and i need to trace the traffic from multiple remote linux pc's, this should be possible by deploying rpcapd on my remote pc's, but how can i add the remote network interfaces for tshark from command line without GUI support? any suggestions would be helpful....<br />
</p><p>Thanks, Vys</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote-monitoring" rel="tag" title="see questions tagged &#39;remote-monitoring&#39;">remote-monitoring</span> <span class="post-tag tag-link-rpcapd" rel="tag" title="see questions tagged &#39;rpcapd&#39;">rpcapd</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jul '15, 08:28</strong></p><img src="https://secure.gravatar.com/avatar/bd239e39087a9649712df7822ff4e977?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vysr2939&#39;s gravatar image" /><p><span>vysr2939</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vysr2939 has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-44055" class="comments-container"></div><div id="comment-tools-44055" class="comment-tools"></div><div class="clear"></div><div id="comment-44055-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

