+++
type = "question"
title = "Can&#x27;t open Wireshark on OS X"
description = '''for some reason I still can&#x27;t open Wireshark. I have checked that I have /usr/X11 --&amp;gt; /opt/X11  and /opt/X11/lib/libcairo.2.dylib exists as well.'''
date = "2015-04-13T13:06:00Z"
lastmod = "2015-04-13T13:23:00Z"
weight = 41416
keywords = [ "osx" ]
aliases = [ "/questions/41416" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can't open Wireshark on OS X](/questions/41416/cant-open-wireshark-on-os-x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41416-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41416-score" class="post-score" title="current number of votes">0</div><span id="post-41416-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>for some reason I still can't open Wireshark. I have checked that I have /usr/X11 --&gt; /opt/X11 and /opt/X11/lib/libcairo.2.dylib exists as well.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Apr '15, 13:06</strong></p><img src="https://secure.gravatar.com/avatar/faf670c5a587b3fb4ace00ff3e10fded?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Fred%20Xu&#39;s gravatar image" /><p><span>Fred Xu</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Fred Xu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>13 Apr '15, 13:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-41416" class="comments-container"><span id="41419"></span><div id="comment-41419" class="comment"><div id="post-41419-score" class="comment-score"></div><div class="comment-text"><p>What versions of OS X and Wireshark are you using?</p><p>What happens when you double-click Wireshark?</p><p>Does a crash report show up for Wireshark?</p></div><div id="comment-41419-info" class="comment-info"><span class="comment-age">(13 Apr '15, 13:23)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-41416" class="comment-tools"></div><div class="clear"></div><div id="comment-41416-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

