+++
type = "question"
title = "Follow stream and realtime display only after entering preferences"
description = '''Hi, I am using WS 1.11.3-1673 etc. When starting capture my details pane doesn&#x27;t show realtime packetbytes.  Only when I enter &quot;Preferences&quot; and click &quot;Ok&quot; the latest frame and corresponding details and bytes are shown. It makes no difference if my view is &quot;Expanded&quot;. Kind regards, Loe'''
date = "2014-02-19T00:47:00Z"
lastmod = "2014-02-19T00:47:00Z"
weight = 29994
keywords = [ "real-time" ]
aliases = [ "/questions/29994" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Follow stream and realtime display only after entering preferences](/questions/29994/follow-stream-and-realtime-display-only-after-entering-preferences)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29994-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29994-score" class="post-score" title="current number of votes">0</div><span id="post-29994-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am using WS 1.11.3-1673 etc. When starting capture my details pane doesn't show realtime packetbytes. Only when I enter "Preferences" and click "Ok" the latest frame and corresponding details and bytes are shown. It makes no difference if my view is "Expanded".</p><p>Kind regards,</p><p>Loe</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-real-time" rel="tag" title="see questions tagged &#39;real-time&#39;">real-time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '14, 00:47</strong></p><img src="https://secure.gravatar.com/avatar/bbba1b5736eafd024fe8d8a4325807db?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Loe%20Walter&#39;s gravatar image" /><p><span>Loe Walter</span><br />
<span class="score" title="41 reputation points">41</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Loe Walter has no accepted answers">0%</span></p></div></div><div id="comments-container-29994" class="comments-container"></div><div id="comment-tools-29994" class="comment-tools"></div><div class="clear"></div><div id="comment-29994-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

