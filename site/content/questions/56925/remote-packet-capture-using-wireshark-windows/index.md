+++
type = "question"
title = "Remote Packet Capture using Wireshark (Windows)"
description = '''I am trying to set up remote packet capture amongst 2 machines within my network. I have installed Wireshark and WinPcap on the server and I have installed WinPcap on the target. I have both turned on Remote Packet Capture Protocol v.0 (experimental) and ensured rpcapd is running in services. I have...'''
date = "2016-11-02T03:45:00Z"
lastmod = "2016-11-02T03:45:00Z"
weight = 56925
keywords = [ "remote-capture", "network", "beginner", "wireshark" ]
aliases = [ "/questions/56925" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Remote Packet Capture using Wireshark (Windows)](/questions/56925/remote-packet-capture-using-wireshark-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56925-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56925-score" class="post-score" title="current number of votes">0</div><span id="post-56925-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to set up remote packet capture amongst 2 machines within my network. I have installed Wireshark and WinPcap on the server and I have installed WinPcap on the target. I have both turned on Remote Packet Capture Protocol v.0 (experimental) and ensured rpcapd is running in services. I have turned off Windows firewall on both machines(The rules have been explicitly applied to both inbound and outbound on both of the PCs firewalls that port 2002 is allowed). Now when I go to wireshark and look up remote interfaces, I type in the IP address of the target and the port (2002 by default) but it says it cannot find any interfaces. Am I missing something here? Any advice would be much appreciated.</p><p>The PC's are both domain aware and on the same domain. The PC's are also on the same IP subnet but on different physical switches - if that makes a difference.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote-capture" rel="tag" title="see questions tagged &#39;remote-capture&#39;">remote-capture</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-beginner" rel="tag" title="see questions tagged &#39;beginner&#39;">beginner</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '16, 03:45</strong></p><img src="https://secure.gravatar.com/avatar/c270f21add484b76a89e78e9345226e9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="CrazyHorse019&#39;s gravatar image" /><p><span>CrazyHorse019</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="CrazyHorse019 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Nov '16, 03:47</strong> </span></p></div></div><div id="comments-container-56925" class="comments-container"></div><div id="comment-tools-56925" class="comment-tools"></div><div class="clear"></div><div id="comment-56925-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

