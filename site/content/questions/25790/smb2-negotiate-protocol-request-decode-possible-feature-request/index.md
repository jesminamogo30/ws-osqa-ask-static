+++
type = "question"
title = "SMB2 Negotiate Protocol Request Decode / Possible feature request"
description = '''I notice that the decode for an SMB2 Negotiate Protocol Request is fairly sparse compared to the decode used for the equivalent SMB1 Request SMB1 Dialect: PC Network Program 1.0 Dialect: LANMAN1.0 Dialect: Windows for Workgroups 3.1a Dialect: LM1.2x002 Dialect: LANMAN2.1 Dialect: NT LM 0.12 Dialect:...'''
date = "2013-10-09T02:10:00Z"
lastmod = "2013-10-09T02:10:00Z"
weight = 25790
keywords = [ "smb2", "smb1" ]
aliases = [ "/questions/25790" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SMB2 Negotiate Protocol Request Decode / Possible feature request](/questions/25790/smb2-negotiate-protocol-request-decode-possible-feature-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25790-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25790-score" class="post-score" title="current number of votes">0</div><span id="post-25790-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I notice that the decode for an SMB2 Negotiate Protocol Request is fairly sparse compared to the decode used for the equivalent SMB1 Request</p><p>SMB1 Dialect: PC Network Program 1.0 Dialect: LANMAN1.0 Dialect: Windows for Workgroups 3.1a Dialect: LM1.2x002 Dialect: LANMAN2.1 Dialect: NT LM 0.12 Dialect: SMB 2.002 Dialect: SMB 2.???</p><p>SMB2 Dialect: 0x0202 Dialect: 0x0210</p><p>In the SMB2 decode, I would prefer to see something like: Dialect: SMB 2.02 (Vista/Win2003) Dialect: SMB 2.10 (Win7/Win2008)</p><p>I would be happy to submit a feature request ... but before I do that, would anyone like to push back and either defend the current approach or offer a third path?</p><p>--sk</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-smb2" rel="tag" title="see questions tagged &#39;smb2&#39;">smb2</span> <span class="post-tag tag-link-smb1" rel="tag" title="see questions tagged &#39;smb1&#39;">smb1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Oct '13, 02:10</strong></p><img src="https://secure.gravatar.com/avatar/18ae5b8bfddad49931ec557b9342075a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="skendric&#39;s gravatar image" /><p><span>skendric</span><br />
<span class="score" title="11 reputation points">11</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="skendric has no accepted answers">0%</span></p></div></div><div id="comments-container-25790" class="comments-container"></div><div id="comment-tools-25790" class="comment-tools"></div><div class="clear"></div><div id="comment-25790-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

