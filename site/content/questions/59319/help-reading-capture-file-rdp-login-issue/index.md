+++
type = "question"
title = "Help reading capture file (RDP Login issue)"
description = '''Hi.  I&#x27;m excerpering a problem with long response time to my RDP setup.  When connection with port 443 the first time, connection time is up to about 15-20 seconds. After the connection is etablished and i do a logout and try another login, the connection time is only 1-3 seconds (If the new login i...'''
date = "2017-02-10T00:37:00Z"
lastmod = "2017-02-10T02:24:00Z"
weight = 59319
keywords = [ "read", "capture", "login", "rdp", "help" ]
aliases = [ "/questions/59319" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help reading capture file (RDP Login issue)](/questions/59319/help-reading-capture-file-rdp-login-issue)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59319-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59319-score" class="post-score" title="current number of votes">0</div><span id="post-59319-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi.</p><p>I'm excerpering a problem with long response time to my RDP setup. When connection with port 443 the first time, connection time is up to about 15-20 seconds. After the connection is etablished and i do a logout and try another login, the connection time is only 1-3 seconds (If the new login is made within a few minutes).</p><p>The setup is first through a ZyXel USG 310 FireWall (Port 433 is open), and from here there are 2 cases.<br />
<br />
-First case: Firewall --&gt; RD Gateway that is connected to AD, RD Connection Broker, and the remote host<br />
<br />
-Second case: Firewall --&gt; directly into remote host</p><p>In both cases the same issue is present.</p><p>Do you know a place where it's possible to get help reading a network capture file, from the firewall (readable in Wireshark)</p><p>Kind regards<br />
<br />
Svein Svendsen<br />
<br />
Mitcom</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-read" rel="tag" title="see questions tagged &#39;read&#39;">read</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-login" rel="tag" title="see questions tagged &#39;login&#39;">login</span> <span class="post-tag tag-link-rdp" rel="tag" title="see questions tagged &#39;rdp&#39;">rdp</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '17, 00:37</strong></p><img src="https://secure.gravatar.com/avatar/2520f9c13575b1d41076f5ad2cc9697f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mitcom&#39;s gravatar image" /><p><span>Mitcom</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mitcom has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-59319" class="comments-container"><span id="59322"></span><div id="comment-59322" class="comment"><div id="post-59322-score" class="comment-score"></div><div class="comment-text"><p>Or is there a company that looks at these network captures?</p></div><div id="comment-59322-info" class="comment-info"><span class="comment-age">(10 Feb '17, 02:24)</span> <span class="comment-user userinfo">Mitcom</span></div></div></div><div id="comment-tools-59319" class="comment-tools"></div><div class="clear"></div><div id="comment-59319-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

