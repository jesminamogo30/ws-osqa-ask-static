+++
type = "question"
title = "IEC 60870-5-104-Asdu IO reading"
description = '''A month ago, I could not determine how close I was to stablish communication in a IEC 60870-5-104 link, can someone help?, in my wireshark file I can see: 104apci&amp;lt;-U (STARTDT act) (Master) 104apci-&amp;gt;U STARTDT con (Slave) 104apcu 1,0&amp;lt;-0 C_IC_NA_1 Act 104apcu 1,0-&amp;gt;0 C_IC_NA_1 UkComAdrASDU_N...'''
date = "2014-01-04T10:17:00Z"
lastmod = "2014-01-04T10:17:00Z"
weight = 28571
keywords = [ "60870-5-104", "iec" ]
aliases = [ "/questions/28571" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [IEC 60870-5-104-Asdu IO reading](/questions/28571/iec-60870-5-104-asdu-io-reading)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28571-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28571-score" class="post-score" title="current number of votes">0</div><span id="post-28571-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>A month ago, I could not determine how close I was to stablish communication in a IEC 60870-5-104 link, can someone help?, in my wireshark file I can see:</p><pre><code>104apci&lt;-U (STARTDT act) (Master)
104apci-&gt;U STARTDT con (Slave)
104apcu 1,0&lt;-0 C_IC_NA_1 Act
104apcu 1,0-&gt;0 C_IC_NA_1 UkComAdrASDU_NEGA IOA=0
.
.
104apcu 2,0-&gt;0 M_SP_NA_1(1) Spont IOA=32 &#39;Single point information&#39;
--
IEC 60870-5-104-Asdu: 2,0-&gt;0 M_SP_NA_1 Spont IOA=32 &#39;single-point information&#39;
    TypeId: M_SP_NA_1 (1)
    .000 0001 = NumIx: 1
    ..00 0011 = CauseTx: Spont (3)
    .0.. .... = Negative: False
    0... .... = Test: False
    OA: 0
    Addr: 2
    IOA: 32
    IEC 60870-5-104-Asdu: Value
        IOA: 32
        Value: ON - Status: Not blocked, Not Substituted, Topical, Valid
</code></pre><p>At this point I am not sure if I was reading a single point of information, index 1?, can you help?!. file available on request.</p><p>Tajin23</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-60870-5-104" rel="tag" title="see questions tagged &#39;60870-5-104&#39;">60870-5-104</span> <span class="post-tag tag-link-iec" rel="tag" title="see questions tagged &#39;iec&#39;">iec</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '14, 10:17</strong></p><img src="https://secure.gravatar.com/avatar/4c437adbe67fae53d6e66f0181dde7d8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tajin23&#39;s gravatar image" /><p><span>Tajin23</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tajin23 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Jan '14, 15:05</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-28571" class="comments-container"></div><div id="comment-tools-28571" class="comment-tools"></div><div class="clear"></div><div id="comment-28571-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

