+++
type = "question"
title = "Debug console: no printout at all"
description = '''Hi All, I&#x27;m trying to debug my dissector plugin using debug console http://wiki.wireshark.org/Development/Tips No g_print nor just printf nor ostream std::cout doesn&#x27;t work.  Just writing to the file - it works but it&#x27;s not so useful like console. What could be the reason ? Which secret flag should ...'''
date = "2013-03-12T03:30:00Z"
lastmod = "2013-03-12T03:30:00Z"
weight = 19380
keywords = [ "printout", "g_print", "cout", "debug_console", "printf" ]
aliases = [ "/questions/19380" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Debug console: no printout at all](/questions/19380/debug-console-no-printout-at-all)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19380-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19380-score" class="post-score" title="current number of votes">0</div><span id="post-19380-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,<br />
I'm trying to debug my dissector plugin using debug console <a href="http://wiki.wireshark.org/Development/Tips">http://wiki.wireshark.org/Development/Tips</a><br />
</p><p>No <strong><em>g_print</em></strong> nor just <strong><em>printf</em></strong> nor ostream <strong><em>std::cout</em></strong> doesn't work.<br />
Just writing to the file - it works but it's not so useful like console.<br />
<strong>What could be the reason ?</strong><br />
Which secret flag should I check ?<br />
<br />
I compiled <strong><em>Wireshark 1.9.0 ( svn rev 48261)</em></strong> for Windows,<br />
activated debug console as "Always (debugging)" but I see nothing there !</p><p>See the same issue with no answer: <a href="http://ask.wireshark.org/questions/11874/debug-console-nothing-displayed">http://ask.wireshark.org/questions/11874/debug-console-nothing-displayed</a><br />
<br />
Thanks,<br />
Igor</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-printout" rel="tag" title="see questions tagged &#39;printout&#39;">printout</span> <span class="post-tag tag-link-g_print" rel="tag" title="see questions tagged &#39;g_print&#39;">g_print</span> <span class="post-tag tag-link-cout" rel="tag" title="see questions tagged &#39;cout&#39;">cout</span> <span class="post-tag tag-link-debug_console" rel="tag" title="see questions tagged &#39;debug_console&#39;">debug_console</span> <span class="post-tag tag-link-printf" rel="tag" title="see questions tagged &#39;printf&#39;">printf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '13, 03:30</strong></p><img src="https://secure.gravatar.com/avatar/0844d0ce30390c9b9e41aa808a9dffab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Scorpion&#39;s gravatar image" /><p><span>Scorpion</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Scorpion has no accepted answers">0%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>12 Mar '13, 03:48</strong> </span></p></div></div><div id="comments-container-19380" class="comments-container"></div><div id="comment-tools-19380" class="comment-tools"></div><div class="clear"></div><div id="comment-19380-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

