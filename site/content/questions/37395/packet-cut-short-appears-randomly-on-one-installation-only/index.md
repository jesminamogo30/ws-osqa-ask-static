+++
type = "question"
title = "&quot;Packet Cut Short&quot; appears randomly on one installation only"
description = '''I have two machines set up. Reading an identical saved .pcap file, one machine&#x27;s wireshark installation comes up with an error: &quot;The capture file appears to have been cut short in the middle of a packet.&quot; The other machine displays the results precisely as expected. What could cause this difference?...'''
date = "2014-10-28T07:46:00Z"
lastmod = "2014-10-28T08:52:00Z"
weight = 37395
keywords = [ "installation" ]
aliases = [ "/questions/37395" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ["Packet Cut Short" appears randomly on one installation only](/questions/37395/packet-cut-short-appears-randomly-on-one-installation-only)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37395-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37395-score" class="post-score" title="current number of votes">0</div><span id="post-37395-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have two machines set up. Reading an identical saved .pcap file, one machine's wireshark installation comes up with an error: "The capture file appears to have been cut short in the middle of a packet." The other machine displays the results precisely as expected. What could cause this difference? They are both running windows 7 64-bit with wireshark 1.12.1 installed.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Oct '14, 07:46</strong></p><img src="https://secure.gravatar.com/avatar/2f8c41cefb77dc9ab12aa3784bf94c70?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="11bkrain&#39;s gravatar image" /><p><span>11bkrain</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="11bkrain has no accepted answers">0%</span></p></div></div><div id="comments-container-37395" class="comments-container"><span id="37396"></span><div id="comment-37396" class="comment"><div id="post-37396-score" class="comment-score"></div><div class="comment-text"><p>How did you transfer the PCAP file from one to the other? FTP maybe?</p></div><div id="comment-37396-info" class="comment-info"><span class="comment-age">(28 Oct '14, 07:59)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="37399"></span><div id="comment-37399" class="comment"><div id="post-37399-score" class="comment-score"></div><div class="comment-text"><p>Yeah, I'm using WinSCP to FTP the .pcap file</p></div><div id="comment-37399-info" class="comment-info"><span class="comment-age">(28 Oct '14, 08:09)</span> <span class="comment-user userinfo">11bkrain</span></div></div><span id="37400"></span><div id="comment-37400" class="comment"><div id="post-37400-score" class="comment-score"></div><div class="comment-text"><p>from a third party server</p></div><div id="comment-37400-info" class="comment-info"><span class="comment-age">(28 Oct '14, 08:15)</span> <span class="comment-user userinfo">11bkrain</span></div></div></div><div id="comment-tools-37395" class="comment-tools"></div><div class="clear"></div><div id="comment-37395-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37401"></span>

<div id="answer-container-37401" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37401-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37401-score" class="post-score" title="current number of votes">1</div><span id="post-37401-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Make sure you transfer it as a binary file.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Oct '14, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-37401" class="comments-container"><span id="37403"></span><div id="comment-37403" class="comment"><div id="post-37403-score" class="comment-score"></div><div class="comment-text"><p>that worked, awesome! Thanks</p></div><div id="comment-37403-info" class="comment-info"><span class="comment-age">(28 Oct '14, 08:45)</span> <span class="comment-user userinfo">11bkrain</span></div></div><span id="37406"></span><div id="comment-37406" class="comment"><div id="post-37406-score" class="comment-score"></div><div class="comment-text"><p><span>@11bkrain</span>: If a supplied answer resolves your question can you please "accept" it by clicking the checkmark icon next to it. This highlights good answers for the benefit of subsequent users with the same or similar questions. For extra points you can up-vote the answer (thumb up).</p></div><div id="comment-37406-info" class="comment-info"><span class="comment-age">(28 Oct '14, 08:52)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-37401" class="comment-tools"></div><div class="clear"></div><div id="comment-37401-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

