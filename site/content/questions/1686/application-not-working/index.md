+++
type = "question"
title = "application not working"
description = '''I&#x27;ve downloaded Wireshark 1.4.2 PPC 32.dmg today and after installing it, the application does not open. it does not bring an application window up to the desktop; it is as if it was not there. Thank you'''
date = "2011-01-08T18:42:00Z"
lastmod = "2011-03-22T08:15:00Z"
weight = 1686
keywords = [ "work", "does", "it" ]
aliases = [ "/questions/1686" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [application not working](/questions/1686/application-not-working)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1686-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1686-score" class="post-score" title="current number of votes">0</div><span id="post-1686-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've downloaded Wireshark 1.4.2 PPC 32.dmg today and after installing it, the application does not open. it does not bring an application window up to the desktop; it is as if it was not there. Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-work" rel="tag" title="see questions tagged &#39;work&#39;">work</span> <span class="post-tag tag-link-does" rel="tag" title="see questions tagged &#39;does&#39;">does</span> <span class="post-tag tag-link-it" rel="tag" title="see questions tagged &#39;it&#39;">it</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jan '11, 18:42</strong></p><img src="https://secure.gravatar.com/avatar/e1446b02c33425f3b68339dfe53317de?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rubo&#39;s gravatar image" /><p><span>rubo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rubo has no accepted answers">0%</span></p></div></div><div id="comments-container-1686" class="comments-container"><span id="3027"></span><div id="comment-3027" class="comment"><div id="post-3027-score" class="comment-score"></div><div class="comment-text"><p>Try running it from the command-line to see what error messages, if any, are displayed.</p></div><div id="comment-3027-info" class="comment-info"><span class="comment-age">(22 Mar '11, 08:15)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-1686" class="comment-tools"></div><div class="clear"></div><div id="comment-1686-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

