+++
type = "question"
title = "[closed] Heuristic Dissector leaves &quot;data&quot; on protocol tree"
description = '''I have a dissector which seems to work perfectly. If I register to the TCP ports specifically, it is clean, but if I instead use it as heuristic TCP, it will run, but then below the dissector&#x27;s tree entry there is a &quot;data&quot; entry with the TCP payload, even though the dissector should have consumed th...'''
date = "2012-02-22T04:56:00Z"
lastmod = "2012-02-22T04:59:00Z"
weight = 9171
keywords = [ "heuristic", "dissector" ]
aliases = [ "/questions/9171" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Heuristic Dissector leaves "data" on protocol tree](/questions/9171/heuristic-dissector-leaves-data-on-protocol-tree)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9171-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9171-score" class="post-score" title="current number of votes">0</div><span id="post-9171-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a dissector which seems to work perfectly. If I register to the TCP ports specifically, it is clean, but if I instead use it as heuristic TCP, it will run, but then below the dissector's tree entry there is a "data" entry with the TCP payload, even though the dissector should have consumed the entire payload. Is there a way to block this behavior so that the dissector's tree data will not be followed by the extra entry? Thanks in advance!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-heuristic" rel="tag" title="see questions tagged &#39;heuristic&#39;">heuristic</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Feb '12, 04:56</strong></p><img src="https://secure.gravatar.com/avatar/492460a2d6b7a5cfbe814f16f86686e0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Clifford%20Haas&#39;s gravatar image" /><p><span>Clifford Haas</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Clifford Haas has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>23 Feb '12, 07:28</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-9171" class="comments-container"><span id="9172"></span><div id="comment-9172" class="comment"><div id="post-9172-score" class="comment-score"></div><div class="comment-text"><p>What was wrong with the answer Jaap gave to your near identical <a href="http://ask.wireshark.org/questions/9168/why-does-my-heuristic-dissector-leave-data-on-the-tree">question</a> from yesterday?</p></div><div id="comment-9172-info" class="comment-info"><span class="comment-age">(22 Feb '12, 04:59)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-9171" class="comment-tools"></div><div class="clear"></div><div id="comment-9171-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by multipleinterfaces 23 Feb '12, 07:28

</div>

</div>

</div>

