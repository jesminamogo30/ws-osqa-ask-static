+++
type = "question"
title = "How to baseline large network"
description = '''Baseline, baseline, baseline is what I keep reading but I&#x27;m not finding any good tutorials on how to do so. I take captures from our core network but I have to split them off into 100MB file sizes because there is so much traffic coming through them. I can then go into each file and view statistics ...'''
date = "2014-09-05T05:50:00Z"
lastmod = "2014-09-05T05:50:00Z"
weight = 36022
keywords = [ "baseline" ]
aliases = [ "/questions/36022" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to baseline large network](/questions/36022/how-to-baseline-large-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36022-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36022-score" class="post-score" title="current number of votes">0</div><span id="post-36022-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Baseline, baseline, baseline is what I keep reading but I'm not finding any good tutorials on how to do so. I take captures from our core network but I have to split them off into 100MB file sizes because there is so much traffic coming through them. I can then go into each file and view statistics but that is cumbersome and doesn't give me one report to view overall network traffic and port usage. You can't export the protocol hierarchy statistics, so how am I supposed to create a baseline?</p><p>Anyone have luck creating a baseline report on a large network? I'd like to take captures of the core network once a month and create reports/statistics on top talkers and protocols in use.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-baseline" rel="tag" title="see questions tagged &#39;baseline&#39;">baseline</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Sep '14, 05:50</strong></p><img src="https://secure.gravatar.com/avatar/8f27ed3d96846021d5c0498b6102bf64?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="judgejudy&#39;s gravatar image" /><p><span>judgejudy</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="judgejudy has no accepted answers">0%</span></p></div></div><div id="comments-container-36022" class="comments-container"></div><div id="comment-tools-36022" class="comment-tools"></div><div class="clear"></div><div id="comment-36022-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

