+++
type = "question"
title = "latest tshark is truncating the ESP packets coming from UE to P-CSCF"
description = '''Hi All, We are using the tshark.exe for converting the PCAP files to TEXT files for our tool. We are able to do so using the earlier Wireshark version 1.12.6, however, when we moved to Wireshark version 2.4.1 the tshark is truncating the ESP packets coming from UE to P-CSCF. We have tried several co...'''
date = "2017-09-21T03:56:00Z"
lastmod = "2017-09-21T06:42:00Z"
weight = 63619
keywords = [ "tshark" ]
aliases = [ "/questions/63619" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [latest tshark is truncating the ESP packets coming from UE to P-CSCF](/questions/63619/latest-tshark-is-truncating-the-esp-packets-coming-from-ue-to-p-cscf)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63619-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63619-score" class="post-score" title="current number of votes">0</div><span id="post-63619-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All, We are using the tshark.exe for converting the PCAP files to TEXT files for our tool. We are able to do so using the earlier Wireshark version 1.12.6, however, when we moved to Wireshark version 2.4.1 the tshark is truncating the ESP packets coming from UE to P-CSCF.</p><p>We have tried several config changes and settings but the ESP/SIP packets are still getting truncated and therefore the tool is unable to capture them.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Sep '17, 03:56</strong></p><img src="https://secure.gravatar.com/avatar/fec31169b8123cb3c141b393ddd34ec8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kamta&#39;s gravatar image" /><p><span>kamta</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kamta has no accepted answers">0%</span></p></div></div><div id="comments-container-63619" class="comments-container"><span id="63622"></span><div id="comment-63622" class="comment"><div id="post-63622-score" class="comment-score"></div><div class="comment-text"><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>?</p></div><div id="comment-63622-info" class="comment-info"><span class="comment-age">(21 Sep '17, 05:21)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="63624"></span><div id="comment-63624" class="comment"><div id="post-63624-score" class="comment-score"></div><div class="comment-text"><p>Maybe you could share the exact <code>tshark.exe</code> command-line you're using and include <code>tshark.exe -v</code> output too in case any of it matters.</p></div><div id="comment-63624-info" class="comment-info"><span class="comment-age">(21 Sep '17, 06:42)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-63619" class="comment-tools"></div><div class="clear"></div><div id="comment-63619-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

