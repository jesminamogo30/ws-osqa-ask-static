+++
type = "question"
title = "Cannot capture Android http traffic"
description = '''INTRODUCTION: Wireshark is set up and works completely fine on MacOSX Mavericks. There are several devices connected to the home router which can be successfully monitored. Http traffic from all devices such as Windows 7 and even iOS devices can be captured via WiFI.  PROBLEM: No http traffic can be...'''
date = "2015-01-14T08:00:00Z"
lastmod = "2015-01-15T00:55:00Z"
weight = 39127
keywords = [ "capture", "android", "http", "monitor" ]
aliases = [ "/questions/39127" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot capture Android http traffic](/questions/39127/cannot-capture-android-http-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39127-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39127-score" class="post-score" title="current number of votes">0</div><span id="post-39127-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><strong>INTRODUCTION:</strong> Wireshark is set up and works completely fine on MacOSX Mavericks. There are several devices connected to the home router which can be successfully monitored. Http traffic from all devices such as Windows 7 and even iOS devices can be captured via WiFI.</p><p><strong>PROBLEM:</strong> No http traffic can be captured from Android devices. Traffic from all devices such as Windows 7 and even iOS devices can be captured, EXCEPT TRAFFIC FROM ANDROID DEVICES. There is however a connection with Android devices because a TCP protocol has been 'intercepted', however no http traffic is shown. Problem is initially thought to be because of the several 802.11b/g/n settings, but the router, Macbook WLAN adapter as well as all devices support 802.11n.</p><p><strong>GOAL:</strong> Capture and monitor also, and especially, Android http traffic on Macbook MacOSX Mavericks.</p><p><strong>KEY INFORMATION:</strong> There is no physical access to other devices, meaning the option to set up certain ports or configure network settings on other devices, is not available.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '15, 08:00</strong></p><img src="https://secure.gravatar.com/avatar/b350fd07242a09d7cbf1697f71fa6ea2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sher-e-Arya&#39;s gravatar image" /><p><span>Sher-e-Arya</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sher-e-Arya has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Jan '15, 08:04</strong> </span></p></div></div><div id="comments-container-39127" class="comments-container"><span id="39146"></span><div id="comment-39146" class="comment"><div id="post-39146-score" class="comment-score"></div><div class="comment-text"><blockquote><p>No http traffic can be captured from Android devices.</p></blockquote><p>is it just the http traffic you are not seeing or no traffic (wifi) at all from the Android device? Would you see pings from/to the Android device?</p></div><div id="comment-39146-info" class="comment-info"><span class="comment-age">(15 Jan '15, 00:55)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-39127" class="comment-tools"></div><div class="clear"></div><div id="comment-39127-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

