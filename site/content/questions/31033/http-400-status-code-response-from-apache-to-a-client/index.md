+++
type = "question"
title = "HTTP 400 Status Code response from Apache to a client"
description = '''I want to understand the packet protocol for when packets are sent and then await for an ACK response to Apache and for the case where Packets are received at Apache but the original packet from the client is &#x27;split&#x27; into two packets for Apache.'''
date = "2014-03-20T19:29:00Z"
lastmod = "2014-03-20T23:13:00Z"
weight = 31033
keywords = [ "status", "http", "400" ]
aliases = [ "/questions/31033" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [HTTP 400 Status Code response from Apache to a client](/questions/31033/http-400-status-code-response-from-apache-to-a-client)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31033-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31033-score" class="post-score" title="current number of votes">0</div><span id="post-31033-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to understand the packet protocol for when packets are sent and then await for an ACK response to Apache and for the case where Packets are received at Apache but the original packet from the client is 'split' into two packets for Apache.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-status" rel="tag" title="see questions tagged &#39;status&#39;">status</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-400" rel="tag" title="see questions tagged &#39;400&#39;">400</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '14, 19:29</strong></p><img src="https://secure.gravatar.com/avatar/90c9a2a4b7db59e1026f39af5e1e9bbf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hflinn&#39;s gravatar image" /><p><span>hflinn</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hflinn has no accepted answers">0%</span></p></div></div><div id="comments-container-31033" class="comments-container"><span id="31039"></span><div id="comment-31039" class="comment"><div id="post-31039-score" class="comment-score"></div><div class="comment-text"><p>This is a Q&amp;A site (<strong>Questions</strong> &amp; Answers), so what is your <strong>question</strong>?</p></div><div id="comment-31039-info" class="comment-info"><span class="comment-age">(20 Mar '14, 23:13)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-31033" class="comment-tools"></div><div class="clear"></div><div id="comment-31033-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

