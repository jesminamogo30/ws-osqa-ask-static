+++
type = "question"
title = "Support on ALT Codes"
description = '''I would like to know if wireshark supports ALT codes or not ? There is case where we see that originator as XYZ_ABC in the incoming packet, but customer says it is this character ◄ [ ALT+17 ]'''
date = "2014-09-11T23:48:00Z"
lastmod = "2014-09-11T23:48:00Z"
weight = 36253
keywords = [ "alt", "codes" ]
aliases = [ "/questions/36253" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Support on ALT Codes](/questions/36253/support-on-alt-codes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36253-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36253-score" class="post-score" title="current number of votes">0</div><span id="post-36253-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know if wireshark supports ALT codes or not ? There is case where we see that originator as XYZ_ABC in the incoming packet, but customer says it is this character ◄ [ ALT+17 ]</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-alt" rel="tag" title="see questions tagged &#39;alt&#39;">alt</span> <span class="post-tag tag-link-codes" rel="tag" title="see questions tagged &#39;codes&#39;">codes</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Sep '14, 23:48</strong></p><img src="https://secure.gravatar.com/avatar/ea81afbd71dc63ea6a6506203bc83c3e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="creative&#39;s gravatar image" /><p><span>creative</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="creative has no accepted answers">0%</span></p></div></div><div id="comments-container-36253" class="comments-container"></div><div id="comment-tools-36253" class="comment-tools"></div><div class="clear"></div><div id="comment-36253-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

