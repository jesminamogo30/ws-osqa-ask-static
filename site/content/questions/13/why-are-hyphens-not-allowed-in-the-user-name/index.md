+++
type = "question"
title = "Why are hyphens not allowed in the user-name?"
description = '''Yep... very off-topic, I know... But still... Is there a technical reason? If not, I&#x27;d like to be &quot;SYN-bit&quot; :-)'''
date = "2010-09-09T15:21:00Z"
lastmod = "2010-09-15T12:59:00Z"
weight = 13
keywords = [ "login", "meta" ]
aliases = [ "/questions/13" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Why are hyphens not allowed in the user-name?](/questions/13/why-are-hyphens-not-allowed-in-the-user-name)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13-score" class="post-score" title="current number of votes">0</div><span id="post-13-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Yep... very off-topic, I know... But still... Is there a technical reason? If not, I'd like to be "SYN-bit" :-)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-login" rel="tag" title="see questions tagged &#39;login&#39;">login</span> <span class="post-tag tag-link-meta" rel="tag" title="see questions tagged &#39;meta&#39;">meta</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '10, 15:21</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Sep '10, 22:50</strong> </span></p></div></div><div id="comments-container-13" class="comments-container"></div><div id="comment-tools-13" class="comment-tools"></div><div class="clear"></div><div id="comment-13-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="14"></span>

<div id="answer-container-14" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14-score" class="post-score" title="current number of votes">2</div><span id="post-14-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SYN-bit has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is a <a href="http://jira.osqa.net/browse/OSQA-436">known bug in OSQA</a>. Unfortunately no one has come up with a fix.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Sep '10, 15:30</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-14" class="comments-container"><span id="15"></span><div id="comment-15" class="comment"><div id="post-15-score" class="comment-score"></div><div class="comment-text"><p>Hmmm... so much for my marketing plan ;-)</p></div><div id="comment-15-info" class="comment-info"><span class="comment-age">(09 Sep '10, 15:38)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-14" class="comment-tools"></div><div class="clear"></div><div id="comment-14-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="106"></span>

<div id="answer-container-106" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-106-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-106-score" class="post-score" title="current number of votes">0</div><span id="post-106-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>My generic answer is that most programming languages are friendlier toward an underscore than hyphen, it’s been that way since I can remember. That goes back to Pascal, FORTRAN, and various other old school languages I have studied. Therefore it could be a result of the language it was written in. I could be wrong, I did buy an eraser once and never got the chance to use it. ; ).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '10, 12:49</strong></p><img src="https://secure.gravatar.com/avatar/8532b1faab752153518d45ae486db3f2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrpatmorris&#39;s gravatar image" /><p><span>mrpatmorris</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrpatmorris has no accepted answers">0%</span></p></div></div><div id="comments-container-106" class="comments-container"><span id="107"></span><div id="comment-107" class="comment"><div id="post-107-score" class="comment-score"></div><div class="comment-text"><p>That's certainly true when it comes to variable names and the like. But if a data element can't contain certain characters because of the way things are programmed, then something is not programmed right and I would consider that a bug.</p><p>It could of course also be a choice what characters to accept or not :-)</p></div><div id="comment-107-info" class="comment-info"><span class="comment-age">(15 Sep '10, 12:59)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div></div><div id="comment-tools-106" class="comment-tools"></div><div class="clear"></div><div id="comment-106-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

