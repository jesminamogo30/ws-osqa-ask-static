+++
type = "question"
title = "svn checkout error"
description = '''hi i am getting the fallowing the error while checking out the wireshark source code. Error: OPTIONS of &#x27;http://anonsvn.wireshark.org/wireshark/trunk&#x27;: Could not resolve  Error: hostname `proxyname&#x27;: The requested name is valid and  Error: was found in the database, but it does not have the correct ...'''
date = "2011-06-05T22:51:00Z"
lastmod = "2011-06-06T07:36:00Z"
weight = 4392
keywords = [ "svn", "checkout" ]
aliases = [ "/questions/4392" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [svn checkout error](/questions/4392/svn-checkout-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4392-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4392-score" class="post-score" title="current number of votes">0</div><span id="post-4392-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi i am getting the fallowing the error while checking out the wireshark source code.</p><p>Error: OPTIONS of 'http://anonsvn.wireshark.org/wireshark/trunk': Could not resolve<br />
Error: hostname `proxyname': The requested name is valid and<br />
Error: was found in the database, but it does not have the correct associated data<br />
Error: being resolved for.<br />
Error: (http://anonsvn.wireshark.org)<br />
</p><p>what might be the problem, can anybody tel me .? thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-svn" rel="tag" title="see questions tagged &#39;svn&#39;">svn</span> <span class="post-tag tag-link-checkout" rel="tag" title="see questions tagged &#39;checkout&#39;">checkout</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jun '11, 22:51</strong></p><img src="https://secure.gravatar.com/avatar/257c9f9e498193d7ddde57090efe094a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sagu072&#39;s gravatar image" /><p><span>sagu072</span><br />
<span class="score" title="35 reputation points">35</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="24 badges"><span class="silver">●</span><span class="badgecount">24</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sagu072 has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-4392" class="comments-container"></div><div id="comment-tools-4392" class="comment-tools"></div><div class="clear"></div><div id="comment-4392-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4403"></span>

<div id="answer-container-4403" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4403-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4403-score" class="post-score" title="current number of votes">0</div><span id="post-4403-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Which Subversion client are you using? It sounds like you might have the word <code>proxyname</code> set as the proxy server in your configuration.</p><p>Also, Subversion over HTTP uses methods that are <a href="http://subversion.apache.org/faq.html#proxy">prohibited by some proxies</a>. You might have better luck using https://anonsvn.wireshark.org/... instead.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jun '11, 07:36</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span> </br></br></p></div></div><div id="comments-container-4403" class="comments-container"></div><div id="comment-tools-4403" class="comment-tools"></div><div class="clear"></div><div id="comment-4403-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

