+++
type = "question"
title = "Unable to view many variables in details section of Wireshark"
description = '''I am trying to do a homework lab, however when I got to the part where I had to describe information about the server I am getting info from a lot seemed to be missing. Here is a screen cap of what my Wireshark prorgam is showing me. Here is what another solution manual got on their Wireshark. I&#x27;m n...'''
date = "2015-09-14T14:45:00Z"
lastmod = "2015-09-14T18:52:00Z"
weight = 45834
keywords = [ "last-modified", "content-length", "details" ]
aliases = [ "/questions/45834" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to view many variables in details section of Wireshark](/questions/45834/unable-to-view-many-variables-in-details-section-of-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45834-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45834-score" class="post-score" title="current number of votes">0</div><span id="post-45834-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to do a <a href="http://docdro.id/wYANTqU">homework lab</a>, however when I got to the part where I had to describe information about the server I am getting info from a lot seemed to be missing. <a href="http://imgur.com/rTxfryX">Here is a screen cap</a> of what my Wireshark prorgam is showing me. <a href="https://inst.eecs.berkeley.edu/~ee122/sp06/Homeworks/Ethereal_HTTP_Solution.pdf">Here is what another solution manual got</a> on their Wireshark. I'm not exactly sure what I'm doing wrong that I dont have a lot of those things, like Content-Length or Last-Modified.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-last-modified" rel="tag" title="see questions tagged &#39;last-modified&#39;">last-modified</span> <span class="post-tag tag-link-content-length" rel="tag" title="see questions tagged &#39;content-length&#39;">content-length</span> <span class="post-tag tag-link-details" rel="tag" title="see questions tagged &#39;details&#39;">details</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '15, 14:45</strong></p><img src="https://secure.gravatar.com/avatar/c9b960ba93a9c715b6b4c99528bf36ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="draav&#39;s gravatar image" /><p><span>draav</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="draav has no accepted answers">0%</span></p></div></div><div id="comments-container-45834" class="comments-container"></div><div id="comment-tools-45834" class="comment-tools"></div><div class="clear"></div><div id="comment-45834-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45836"></span>

<div id="answer-container-45836" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45836-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45836-score" class="post-score" title="current number of votes">1</div><span id="post-45836-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You don't have it because the server didn't send it.</p><p>Did you wait more than one minute before starting the capture, as step 3 of "1. The Basic HTTP GET/response interaction" said to do? If not, do so.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Sep '15, 17:01</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-45836" class="comments-container"><span id="45837"></span><div id="comment-45837" class="comment"><div id="post-45837-score" class="comment-score"></div><div class="comment-text"><p>Yes I tried that, a few times. I'm gonna clear my cache and try again, then I'm gonna try with a different browser also.</p></div><div id="comment-45837-info" class="comment-info"><span class="comment-age">(14 Sep '15, 18:52)</span> <span class="comment-user userinfo">draav</span></div></div></div><div id="comment-tools-45836" class="comment-tools"></div><div class="clear"></div><div id="comment-45836-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

