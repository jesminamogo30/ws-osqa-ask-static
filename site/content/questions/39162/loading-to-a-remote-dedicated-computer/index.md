+++
type = "question"
title = "loading to a remote dedicated computer"
description = '''I need to load Wireshark to a computer that never gets hooked to the internet. This is for testing a remote closed system and it is forbidden to have internet connections. Is the a way to load this to a thumbdrive and transfer it to the remote computer? Thanks,'''
date = "2015-01-15T08:15:00Z"
lastmod = "2015-01-15T08:47:00Z"
weight = 39162
keywords = [ "remote-computer", "closed-system" ]
aliases = [ "/questions/39162" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [loading to a remote dedicated computer](/questions/39162/loading-to-a-remote-dedicated-computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39162-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39162-score" class="post-score" title="current number of votes">0</div><span id="post-39162-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to load Wireshark to a computer that never gets hooked to the internet. This is for testing a remote closed system and it is forbidden to have internet connections. Is the a way to load this to a thumbdrive and transfer it to the remote computer?</p><p>Thanks,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-remote-computer" rel="tag" title="see questions tagged &#39;remote-computer&#39;">remote-computer</span> <span class="post-tag tag-link-closed-system" rel="tag" title="see questions tagged &#39;closed-system&#39;">closed-system</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '15, 08:15</strong></p><img src="https://secure.gravatar.com/avatar/d5d2334b4550cca519350ed644b03c56?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hardwaredan&#39;s gravatar image" /><p><span>hardwaredan</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hardwaredan has no accepted answers">0%</span></p></div></div><div id="comments-container-39162" class="comments-container"></div><div id="comment-tools-39162" class="comment-tools"></div><div class="clear"></div><div id="comment-39162-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39164"></span>

<div id="answer-container-39164" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39164-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39164-score" class="post-score" title="current number of votes">2</div><span id="post-39164-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure, you can either put the installer on the thumbdrive and run it on the remote computer to install Wireshark, or use the portable version. Both can be downloaded at <a href="https://www.wireshark.org/download.html">www.wireshark.org</a></p><p>Keep in mind that the portable version also needs to install drivers to capture data, just like the install version does.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jan '15, 08:25</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-39164" class="comments-container"><span id="39165"></span><div id="comment-39165" class="comment"><div id="post-39165-score" class="comment-score"></div><div class="comment-text"><p>you can also boot the computer with a Linux Dsitribution that contains Wireshark, like <a href="https://www.kali.org/">Kali</a>.</p></div><div id="comment-39165-info" class="comment-info"><span class="comment-age">(15 Jan '15, 08:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="39167"></span><div id="comment-39167" class="comment"><div id="post-39167-score" class="comment-score">1</div><div class="comment-text"><p>Note that the development branch (1.99) conforms to the current PortableApps standards. I'm not sure about the 1.12 branch.</p></div><div id="comment-39167-info" class="comment-info"><span class="comment-age">(15 Jan '15, 08:47)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-39164" class="comment-tools"></div><div class="clear"></div><div id="comment-39164-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

