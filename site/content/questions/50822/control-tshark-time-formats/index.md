+++
type = "question"
title = "Control tshark time formats"
description = '''If we want to capture packets packets in seconds rather than in nano seconds what&#x27;s the command for tshark'''
date = "2016-03-11T06:22:00Z"
lastmod = "2016-03-11T06:27:00Z"
weight = 50822
keywords = [ "format", "tshark", "time" ]
aliases = [ "/questions/50822" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Control tshark time formats](/questions/50822/control-tshark-time-formats)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50822-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50822-score" class="post-score" title="current number of votes">0</div><span id="post-50822-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If we want to capture packets packets in seconds rather than in nano seconds what's the command for tshark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-format" rel="tag" title="see questions tagged &#39;format&#39;">format</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Mar '16, 06:22</strong></p><img src="https://secure.gravatar.com/avatar/3ff916d0f42cb423bb2a8599d347cb4c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amit%20Mahajan&#39;s gravatar image" /><p><span>Amit Mahajan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amit Mahajan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>11 Mar '16, 06:24</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-50822" class="comments-container"><span id="50823"></span><div id="comment-50823" class="comment"><div id="post-50823-score" class="comment-score"></div><div class="comment-text"><p>Please start your own question, not ask an unrelated query in an existing one as an "Answer". This is a Q&amp;A site, not a forum, read the <a href="https://ask.wireshark.org/faq/">FAQ</a> for more info.</p><p>The packets are captured using whatever format is provided by the capture mechanism. Do you want to control the display of packet timestamps in tshark output?</p></div><div id="comment-50823-info" class="comment-info"><span class="comment-age">(11 Mar '16, 06:27)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-50822" class="comment-tools"></div><div class="clear"></div><div id="comment-50822-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

