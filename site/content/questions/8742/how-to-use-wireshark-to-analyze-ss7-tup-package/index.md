+++
type = "question"
title = "How to use wireshark to analyze SS7 TUP package"
description = '''I have a PCAP file for ss7 TUP protocol, BUT i can not decode correctly, it show &quot;data&quot; only, but the signal type show TUP and wireshark can decode the DTP/OPC. anyone knows how to decode correct TUP protocol using wireshark? many thanks. attachment is a sample for TUP signal 0000 84 00 00 00 09 09 ...'''
date = "2012-02-01T01:00:00Z"
lastmod = "2012-02-01T05:40:00Z"
weight = 8742
keywords = [ "tup" ]
aliases = [ "/questions/8742" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to use wireshark to analyze SS7 TUP package](/questions/8742/how-to-use-wireshark-to-analyze-ss7-tup-package)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8742-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8742-score" class="post-score" title="current number of votes">0</div><span id="post-8742-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a PCAP file for ss7 TUP protocol, BUT i can not decode correctly, it show "data" only, but the signal type show TUP and wireshark can decode the DTP/OPC. anyone knows how to decode correct TUP protocol using wireshark? many thanks. attachment is a sample for TUP signal</p><p><code>0000  84 00 00 00 09 09 09 61  00 11 18 00 30 11 04      .......a ....0..</code></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tup" rel="tag" title="see questions tagged &#39;tup&#39;">tup</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '12, 01:00</strong></p><img src="https://secure.gravatar.com/avatar/d19a6c396739b25a540f99f89edb6729?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ivandu1013&#39;s gravatar image" /><p><span>ivandu1013</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ivandu1013 has no accepted answers">0%</span></p></div></div><div id="comments-container-8742" class="comments-container"></div><div id="comment-tools-8742" class="comment-tools"></div><div class="clear"></div><div id="comment-8742-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8744"></span>

<div id="answer-container-8744" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8744-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8744-score" class="post-score" title="current number of votes">0</div><span id="post-8744-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no TUP dissector in Wireshark and there is lots of national variants I beleve.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '12, 05:40</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-8744" class="comments-container"></div><div id="comment-tools-8744" class="comment-tools"></div><div class="clear"></div><div id="comment-8744-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

