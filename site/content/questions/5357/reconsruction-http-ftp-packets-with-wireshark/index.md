+++
type = "question"
title = "Reconsruction HTTP , FTP packets with wireshark"
description = '''As I know , I can reconstruct voip call. How about the https and ftp traffic ? If yes , Please let me know the way ? Thanks,'''
date = "2011-07-29T02:15:00Z"
lastmod = "2011-07-30T13:07:00Z"
weight = 5357
keywords = [ "playback" ]
aliases = [ "/questions/5357" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Reconsruction HTTP , FTP packets with wireshark](/questions/5357/reconsruction-http-ftp-packets-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5357-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5357-score" class="post-score" title="current number of votes">0</div><span id="post-5357-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>As I know , I can reconstruct voip call.</p><p>How about the https and ftp traffic ?</p><p>If yes , Please let me know the way ?</p><p>Thanks,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-playback" rel="tag" title="see questions tagged &#39;playback&#39;">playback</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jul '11, 02:15</strong></p><img src="https://secure.gravatar.com/avatar/3c5031d07907058abcb9647f1edb1198?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yves&#39;s gravatar image" /><p><span>yves</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yves has no accepted answers">0%</span></p></div></div><div id="comments-container-5357" class="comments-container"></div><div id="comment-tools-5357" class="comment-tools"></div><div class="clear"></div><div id="comment-5357-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5371"></span>

<div id="answer-container-5371" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5371-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5371-score" class="post-score" title="current number of votes">0</div><span id="post-5371-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>For HTTP you can use File -&gt; Export -&gt; Objects -&gt; HTTP.</p><p>For FTP locate the TCP-Session with FTP-Data, Right-click on a packet of the connection and choose Follow TCP-Stream. You get a new Window where you can save the transferred data.</p><p>Good hunting...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jul '11, 13:07</strong></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packethunter has 8 accepted answers">8%</span></p></div></div><div id="comments-container-5371" class="comments-container"></div><div id="comment-tools-5371" class="comment-tools"></div><div class="clear"></div><div id="comment-5371-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

