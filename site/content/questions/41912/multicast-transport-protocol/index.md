+++
type = "question"
title = "Multicast Transport Protocol"
description = '''Is this MTP (Multicast Transport Protocol)- MTP  supported in Wireshark? Thanks for reply.'''
date = "2015-04-28T02:27:00Z"
lastmod = "2015-04-28T07:18:00Z"
weight = 41912
keywords = [ "mtp3", "mtp-2", "wireshark" ]
aliases = [ "/questions/41912" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Multicast Transport Protocol](/questions/41912/multicast-transport-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41912-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41912-score" class="post-score" title="current number of votes">0</div><span id="post-41912-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is this MTP (Multicast Transport Protocol)- <a href="http://www.networksorcery.com/enp/protocol/mtp.htm">MTP</a> supported in Wireshark?</p><p>Thanks for reply.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mtp3" rel="tag" title="see questions tagged &#39;mtp3&#39;">mtp3</span> <span class="post-tag tag-link-mtp-2" rel="tag" title="see questions tagged &#39;mtp-2&#39;">mtp-2</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Apr '15, 02:27</strong></p><img src="https://secure.gravatar.com/avatar/547e317838801b489569fe5bf3ab9e14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Matej&#39;s gravatar image" /><p><span>Matej</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Matej has no accepted answers">0%</span></p></div></div><div id="comments-container-41912" class="comments-container"></div><div id="comment-tools-41912" class="comment-tools"></div><div class="clear"></div><div id="comment-41912-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41917"></span>

<div id="answer-container-41917" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41917-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41917-score" class="post-score" title="current number of votes">0</div><span id="post-41917-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Matej has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark does not currently have a dissector for Multicast Transport Protocol (IP protocol number 92).</p><p>I see that the RFC is fairly old. My question (knowing nothing about MTP): is this protocol currently in use ?</p><p>In any case, you can submit an enhancement request at bugs.wireshark.org: (with a capture file containing MTP packets).</p><p>Someone may then volunteer to create an MTP dissector.</p><p>Or: you can write and contribute a Wireshark dissector for MTP.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '15, 07:18</strong></p><img src="https://secure.gravatar.com/avatar/bfb20acfe44690473b10c7963b5d4a18?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bill%20Meier&#39;s gravatar image" /><p><span>Bill Meier ♦♦</span><br />
<span class="score" title="3180 reputation points"><span>3.2k</span></span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="50 badges"><span class="bronze">●</span><span class="badgecount">50</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bill Meier has 31 accepted answers">17%</span></p></div></div><div id="comments-container-41917" class="comments-container"></div><div id="comment-tools-41917" class="comment-tools"></div><div class="clear"></div><div id="comment-41917-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

