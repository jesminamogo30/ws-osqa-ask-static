+++
type = "question"
title = "Alfa AWUS036NH can&#x27;t set monitor mode in Mac OS"
description = '''Hello, I&#x27;m using Alfa AWUS036NH, Wireshark 1.8.4 and Mac OS 10.8.2. I can&#x27;t set monitor mode, that&#x27;s what I get:   a disabled checkbox :( Is there any way to turn on monitor mode with this card and mac os? best regards, Rorax'''
date = "2012-12-19T12:49:00Z"
lastmod = "2012-12-19T19:56:00Z"
weight = 17077
keywords = [ "os", "mac", "alfa", "monitor", "mode" ]
aliases = [ "/questions/17077" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Alfa AWUS036NH can't set monitor mode in Mac OS](/questions/17077/alfa-awus036nh-cant-set-monitor-mode-in-mac-os)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17077-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17077-score" class="post-score" title="current number of votes">0</div><span id="post-17077-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I'm using Alfa AWUS036NH, Wireshark 1.8.4 and Mac OS 10.8.2. I can't set monitor mode, that's what I get:</p><p><img src="http://i.imgur.com/bqEuv.png" alt="alt text" /></p><p>a disabled checkbox :(</p><p>Is there any way to turn on monitor mode with this card and mac os?</p><p>best regards, Rorax</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-os" rel="tag" title="see questions tagged &#39;os&#39;">os</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-alfa" rel="tag" title="see questions tagged &#39;alfa&#39;">alfa</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-mode" rel="tag" title="see questions tagged &#39;mode&#39;">mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Dec '12, 12:49</strong></p><img src="https://secure.gravatar.com/avatar/7562132a846a8d6752c068f37ec4e409?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rorax&#39;s gravatar image" /><p><span>rorax</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rorax has no accepted answers">0%</span></p></img></div></div><div id="comments-container-17077" class="comments-container"></div><div id="comment-tools-17077" class="comment-tools"></div><div class="clear"></div><div id="comment-17077-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17078"></span>

<div id="answer-container-17078" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17078-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17078-score" class="post-score" title="current number of votes">1</div><span id="post-17078-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As far as I know, this only works with AirPort Extreme adapters.</p><blockquote><p><code>http://wiki.wireshark.org/CaptureSetup/WLAN#Mac_OS_X</code><br />
</p></blockquote><p>You could run BackTrack Linux in a virtual machine and map the USB device to that virtual machine. A lot of people do exactly that.</p><blockquote><p><code>http://www.backtrack-linux.org/</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Dec '12, 13:04</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Dec '12, 13:35</strong> </span></p></div></div><div id="comments-container-17078" class="comments-container"><span id="17079"></span><div id="comment-17079" class="comment"><div id="post-17079-score" class="comment-score">1</div><div class="comment-text"><blockquote><p>As far as I know, this only works with AirPort Extreme adapters.</p></blockquote><p>Yes. Sadly, the IO80211* classes in IOKit are private, so third-party network drivers have to be Ethernet drivers; this means that third-party 802.11 adapter drivers can't expose any capabilities other than those offered by Ethernet adapters, including monitor mode support (which involves supplying 802.11 headers rather than Ethernet headers - from userland, requesting monitor mode is done by selecting one of the 802.11 DLT_ values with a BIOCSDLT ioctl on the BPF device).</p></div><div id="comment-17079-info" class="comment-info"><span class="comment-age">(19 Dec '12, 19:56)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-17078" class="comment-tools"></div><div class="clear"></div><div id="comment-17078-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

