+++
type = "question"
title = "show registration timer"
description = '''how do i check in wireshark what is my registration timer? eg my sip provider it asks for 180, how can i see from wireshark that i am sending it?'''
date = "2017-04-10T01:52:00Z"
lastmod = "2017-04-10T01:52:00Z"
weight = 60697
keywords = [ "filter" ]
aliases = [ "/questions/60697" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [show registration timer](/questions/60697/show-registration-timer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60697-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60697-score" class="post-score" title="current number of votes">0</div><span id="post-60697-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how do i check in wireshark what is my registration timer? eg my sip provider it asks for 180, how can i see from wireshark that i am sending it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Apr '17, 01:52</strong></p><img src="https://secure.gravatar.com/avatar/f49cd74f35d6d0358d467c0784dc56f2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="atux_null&#39;s gravatar image" /><p><span>atux_null</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="atux_null has no accepted answers">0%</span></p></div></div><div id="comments-container-60697" class="comments-container"></div><div id="comment-tools-60697" class="comment-tools"></div><div class="clear"></div><div id="comment-60697-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

