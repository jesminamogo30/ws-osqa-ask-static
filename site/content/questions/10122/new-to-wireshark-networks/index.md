+++
type = "question"
title = "New to WireShark &amp; Networks"
description = '''I run a small business and have network problems from time to time. I am very interested in broadening my knowledge and also get to grips with WireShark. As this is a new field and I have limited knowledge about Networks, please can someone direct me in where to start, maybe literature, videos and/o...'''
date = "2012-04-13T04:26:00Z"
lastmod = "2012-04-13T04:26:00Z"
weight = 10122
keywords = [ "newbie", "starter", "beginner" ]
aliases = [ "/questions/10122" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [New to WireShark & Networks](/questions/10122/new-to-wireshark-networks)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10122-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10122-score" class="post-score" title="current number of votes">0</div><span id="post-10122-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I run a small business and have network problems from time to time. I am very interested in broadening my knowledge and also get to grips with WireShark.</p><p>As this is a new field and I have limited knowledge about Networks, please can someone direct me in where to start, maybe literature, videos and/or websites that will help me. I've read through the WireShark Manual and looked at some of the linked videos, but I think I need to get a grip on Networks first.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-newbie" rel="tag" title="see questions tagged &#39;newbie&#39;">newbie</span> <span class="post-tag tag-link-starter" rel="tag" title="see questions tagged &#39;starter&#39;">starter</span> <span class="post-tag tag-link-beginner" rel="tag" title="see questions tagged &#39;beginner&#39;">beginner</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Apr '12, 04:26</strong></p><img src="https://secure.gravatar.com/avatar/4adad4f01355593b9f13b5c0d32812dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Antoine&#39;s gravatar image" /><p><span>Antoine</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Antoine has no accepted answers">0%</span></p></div></div><div id="comments-container-10122" class="comments-container"></div><div id="comment-tools-10122" class="comment-tools"></div><div class="clear"></div><div id="comment-10122-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

