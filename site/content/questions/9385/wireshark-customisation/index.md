+++
type = "question"
title = "wireshark customisation"
description = '''hello guys, I&#x27;m trying to customize wireshark for my team needs ,i have created a new menu-item in main menu-bar. the menu contains options to add pcap files and filter.then on click it will generate an output file for specific values in packets. so please kindly guide me how to proceed for filter i...'''
date = "2012-03-05T22:19:00Z"
lastmod = "2012-03-05T22:19:00Z"
weight = 9385
keywords = [ "wireshark", "display-filter" ]
aliases = [ "/questions/9385" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark customisation](/questions/9385/wireshark-customisation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9385-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9385-score" class="post-score" title="current number of votes">0</div><span id="post-9385-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello guys,</p><p>I'm trying to customize wireshark for my team needs ,i have created a new menu-item in main menu-bar. the menu contains options to add pcap files and filter.then on click it will generate an output file for specific values in packets. so please kindly guide me how to proceed for filter integration ,and what files are to be made changes to have filter connected to this menu item</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '12, 22:19</strong></p><img src="https://secure.gravatar.com/avatar/6cb6685f12bd537f0f2e1e86a591e940?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sangmeshp&#39;s gravatar image" /><p><span>sangmeshp</span><br />
<span class="score" title="36 reputation points">36</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sangmeshp has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Mar '12, 09:49</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-9385" class="comments-container"></div><div id="comment-tools-9385" class="comment-tools"></div><div class="clear"></div><div id="comment-9385-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

