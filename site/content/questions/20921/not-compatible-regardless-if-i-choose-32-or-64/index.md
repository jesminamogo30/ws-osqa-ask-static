+++
type = "question"
title = "&quot;Not compatible&quot; - regardless if I choose 32 or 64"
description = '''Hello, I am trying to install Wireshark on Win 7 64-bit but I am getting the following error message when trying to install. And I have tried both the 32-bit and 64-bit executables. Any ideas on what is going wrong here? &quot;The version of this file is not compatible with the version of Windows you&#x27;re ...'''
date = "2013-05-02T22:13:00Z"
lastmod = "2013-05-03T04:20:00Z"
weight = 20921
keywords = [ "compatible", "version", "64-bit" ]
aliases = [ "/questions/20921" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ["Not compatible" - regardless if I choose 32 or 64](/questions/20921/not-compatible-regardless-if-i-choose-32-or-64)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20921-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20921-score" class="post-score" title="current number of votes">0</div><span id="post-20921-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I am trying to install Wireshark on Win 7 64-bit but I am getting the following error message when trying to install. And I have tried both the 32-bit and 64-bit executables. Any ideas on what is going wrong here?</p><p>"The version of this file is not compatible with the version of Windows you're running. Check your computer's system information to see whether you need an x86 (32-bit) or x64 (64-bit) version of the program, and then contact the software publisher."</p><p>Thanks in advance</p><p>-C</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-compatible" rel="tag" title="see questions tagged &#39;compatible&#39;">compatible</span> <span class="post-tag tag-link-version" rel="tag" title="see questions tagged &#39;version&#39;">version</span> <span class="post-tag tag-link-64-bit" rel="tag" title="see questions tagged &#39;64-bit&#39;">64-bit</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 May '13, 22:13</strong></p><img src="https://secure.gravatar.com/avatar/9c94ef143d578144676ce3c3465c8ec4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chupalo&#39;s gravatar image" /><p><span>chupalo</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chupalo has no accepted answers">0%</span></p></div></div><div id="comments-container-20921" class="comments-container"><span id="20922"></span><div id="comment-20922" class="comment"><div id="post-20922-score" class="comment-score"></div><div class="comment-text"><p>You probably need to tell us exactly what version (the name of the installation file) you are trying to install. Also make sure you install as an Administrator (though I expect you would get a different message otherwise)</p></div><div id="comment-20922-info" class="comment-info"><span class="comment-age">(02 May '13, 23:07)</span> <span class="comment-user userinfo">martyvis</span></div></div></div><div id="comment-tools-20921" class="comment-tools"></div><div class="clear"></div><div id="comment-20921-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20932"></span>

<div id="answer-container-20932" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20932-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20932-score" class="post-score" title="current number of votes">3</div><span id="post-20932-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The Wireshark installer files are compressed. I guess there was either a problem during the download (corrupted file) or some security software on your system (AV, Endpoint Security, etc.) modified (cleaned) the installer as it (falsely) thought to have found malware.</p><p>Please download the installer from <a href="http://www.wireshark.org">http://www.wireshark.org</a> and <a href="http://support.microsoft.com/kb/841290">then check the signature</a> of the downloaded file (see <strong>Verify Downloads</strong> on the download page). If the signature is correct, run the installer. If the signature is not O.K., try to identify the source of the download problem.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 May '13, 04:20</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-20932" class="comments-container"></div><div id="comment-tools-20932" class="comment-tools"></div><div class="clear"></div><div id="comment-20932-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

