+++
type = "question"
title = "wireshark portable and winpcap"
description = '''I am using full installations of wireshark lately.  in earlier times, I used to use the portable version and it always installed temporarily winpcap on the machine I was starting portable wireshark on. I just noticed that this does not happen anymore. Can someone point me to a place where I can read...'''
date = "2017-10-29T12:19:00Z"
lastmod = "2017-10-29T12:19:00Z"
weight = 64328
keywords = [ "winpcap", "portable" ]
aliases = [ "/questions/64328" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark portable and winpcap](/questions/64328/wireshark-portable-and-winpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64328-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64328-score" class="post-score" title="current number of votes">0</div><span id="post-64328-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using full installations of wireshark lately. in earlier times, I used to use the portable version and it always installed temporarily winpcap on the machine I was starting portable wireshark on.</p><p>I just noticed that this does not happen anymore.</p><p>Can someone point me to a place where I can read about this change? or is there a way to make portable wireshark automatically start/install winpcap again?</p><p>thanks dan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-portable" rel="tag" title="see questions tagged &#39;portable&#39;">portable</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Oct '17, 12:19</strong></p><img src="https://secure.gravatar.com/avatar/c1c349f269892c3b94a854a18a4d6b07?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="piuslor&#39;s gravatar image" /><p><span>piuslor</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="piuslor has one accepted answer">100%</span></p></div></div><div id="comments-container-64328" class="comments-container"></div><div id="comment-tools-64328" class="comment-tools"></div><div class="clear"></div><div id="comment-64328-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

