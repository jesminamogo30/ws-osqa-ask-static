+++
type = "question"
title = "Decrypting  MS Kerberos blobs (Win10)"
description = '''Hey all, I&#x27;m trying to decrypt Kerberos blobs (like padata, etc..) appears in Microsoft Kerberos tickets iv&#x27;e captured with wireshark. I got keytab for the user account, and i tried to use it with various of cipher suites (rc4/aes256) with the correct client configuration as well. For some reason wi...'''
date = "2016-04-07T12:20:00Z"
lastmod = "2016-04-07T12:20:00Z"
weight = 51491
keywords = [ "kerberos", "decrypt", "dissectors" ]
aliases = [ "/questions/51491" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypting MS Kerberos blobs (Win10)](/questions/51491/decrypting-ms-kerberos-blobs-win10)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51491-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51491-score" class="post-score" title="current number of votes">0</div><span id="post-51491-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey all,</p><p>I'm trying to decrypt Kerberos blobs (like padata, etc..) appears in Microsoft Kerberos tickets iv'e captured with wireshark.</p><p>I got keytab for the user account, and i tried to use it with various of cipher suites (rc4/aes256) with the correct client configuration as well.</p><p>For some reason wireshark doesn't decrypt any of the blobs. My env is : AD Domain 2012 R2, Windows 10 Client. (x64 both)</p><p>Can anyone shed some light on this ?</p><p>Thx.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-kerberos" rel="tag" title="see questions tagged &#39;kerberos&#39;">kerberos</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span> <span class="post-tag tag-link-dissectors" rel="tag" title="see questions tagged &#39;dissectors&#39;">dissectors</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '16, 12:20</strong></p><img src="https://secure.gravatar.com/avatar/d2a300d58390c33dbc3c9d8c3cf97f04?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="speidy&#39;s gravatar image" /><p><span>speidy</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="speidy has no accepted answers">0%</span></p></div></div><div id="comments-container-51491" class="comments-container"></div><div id="comment-tools-51491" class="comment-tools"></div><div class="clear"></div><div id="comment-51491-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

