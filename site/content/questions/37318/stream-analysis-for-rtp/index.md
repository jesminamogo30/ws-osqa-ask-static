+++
type = "question"
title = "Stream Analysis for RTP"
description = '''Hi we have been trying to figure out what &quot;Lost RTP Packets = 14674 (48.94%)&quot; means in the image linked. Does it actually mean we lost 48.94% of the transmitted packets? http://screencast.com/t/FLI8duXucMgi thank you'''
date = "2014-10-23T12:28:00Z"
lastmod = "2014-10-27T07:37:00Z"
weight = 37318
keywords = [ "streams" ]
aliases = [ "/questions/37318" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Stream Analysis for RTP](/questions/37318/stream-analysis-for-rtp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37318-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37318-score" class="post-score" title="current number of votes">0</div><span id="post-37318-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi we have been trying to figure out what "Lost RTP Packets = 14674 (48.94%)" means in the image linked. Does it actually mean we lost 48.94% of the transmitted packets?</p><p><a href="http://screencast.com/t/FLI8duXucMgi">http://screencast.com/t/FLI8duXucMgi</a></p><p>thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-streams" rel="tag" title="see questions tagged &#39;streams&#39;">streams</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Oct '14, 12:28</strong></p><img src="https://secure.gravatar.com/avatar/0b2d65e22a05c0c9b4ef912e1e3a3b87?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lginet77&#39;s gravatar image" /><p><span>lginet77</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lginet77 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Oct '14, 12:29</strong> </span></p></div></div><div id="comments-container-37318" class="comments-container"></div><div id="comment-tools-37318" class="comment-tools"></div><div class="clear"></div><div id="comment-37318-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37373"></span>

<div id="answer-container-37373" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37373-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37373-score" class="post-score" title="current number of votes">0</div><span id="post-37373-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, as in, probably not. There may be some confusion about the packet streams going back and forth. Also due to an undetermined codec, hence RTP encapsulation profile, it's impossible to say without more info.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Oct '14, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-37373" class="comments-container"></div><div id="comment-tools-37373" class="comment-tools"></div><div class="clear"></div><div id="comment-37373-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

