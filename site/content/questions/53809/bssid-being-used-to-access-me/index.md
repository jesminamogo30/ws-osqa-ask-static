+++
type = "question"
title = "BSSID Being Used to access me"
description = '''Howdy All, It seems I have a secret ghost who likes to watch me. His/Her BSSID is 00:0d:67:6f:05:ac, he/she is using Wireshark some how and imitating xfinity, in the past my &quot;lover&quot; has used Franklin Gadget virtual adapters P2P , ya know. So much so that I can&#x27;t enjoy these products. Sometimes their...'''
date = "2016-07-04T08:56:00Z"
lastmod = "2016-07-04T08:56:00Z"
weight = 53809
keywords = [ "shell", "remote", "bssid", "accesspoint" ]
aliases = [ "/questions/53809" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [BSSID Being Used to access me](/questions/53809/bssid-being-used-to-access-me)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53809-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53809-score" class="post-score" title="current number of votes">-1</div><span id="post-53809-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Howdy All, It seems I have a secret ghost who likes to watch me. His/Her BSSID is 00:0d:67:6f:05:ac, he/she is using Wireshark some how and imitating xfinity, in the past my "lover" has used Franklin Gadget virtual adapters P2P , ya know. So much so that I can't enjoy these products. Sometimes their BSSID resolve into straight up IP addresses, for whatever the reason is and I no longer care, for the users here, how do I prevent them using the same tools heshe uses against me to stop them from gaining access. I have to give credit where credit is do, they are intensely skilled. Too bad we won't ever be friends, any suggestions.. they found a way to always connect me to them. You get tired over the years, so I'm reaching out to the community for your advice, the people who actually know what they are talking about. Thanks in advance. Jason</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-shell" rel="tag" title="see questions tagged &#39;shell&#39;">shell</span> <span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span> <span class="post-tag tag-link-bssid" rel="tag" title="see questions tagged &#39;bssid&#39;">bssid</span> <span class="post-tag tag-link-accesspoint" rel="tag" title="see questions tagged &#39;accesspoint&#39;">accesspoint</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jul '16, 08:56</strong></p><img src="https://secure.gravatar.com/avatar/db7e56eaadd5d89e19a60c29308d9f4e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SALTYWAT3R&#39;s gravatar image" /><p><span>SALTYWAT3R</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SALTYWAT3R has no accepted answers">0%</span></p></div></div><div id="comments-container-53809" class="comments-container"></div><div id="comment-tools-53809" class="comment-tools"></div><div class="clear"></div><div id="comment-53809-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

