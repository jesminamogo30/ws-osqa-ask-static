+++
type = "question"
title = "WNSRP support?"
description = '''Hello, I have captured a mobile link using wireshark application. The file is a Sip conversation for a video call using WNSRP instead of h245 for its control channel. When I see the capture file analysis and use the filter h245, four h245 messages have been found; in their Information field it says ...'''
date = "2012-06-11T01:03:00Z"
lastmod = "2012-06-11T01:03:00Z"
weight = 11805
keywords = [ "wnsrp" ]
aliases = [ "/questions/11805" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WNSRP support?](/questions/11805/wnsrp-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11805-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11805-score" class="post-score" title="current number of votes">0</div><span id="post-11805-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I have captured a mobile link using wireshark application. The file is a Sip conversation for a video call using WNSRP instead of h245 for its control channel. When I see the capture file analysis and use the filter h245, four h245 messages have been found; in their Information field it says terminalCapabilitySet, MasterSlaveDetermination, and vendorIdentification messages is detected. No more h245 messages are found, so openLogicalChannel and multiplexEntrySend messages is not available. Does it depend on WNSRP usage that the wireshark application cannot detect these messages or it means that the connection does not exchange this information at all? Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wnsrp" rel="tag" title="see questions tagged &#39;wnsrp&#39;">wnsrp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jun '12, 01:03</strong></p><img src="https://secure.gravatar.com/avatar/67a8268086d4a77f1095b1fb749bbfb0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pegah&#39;s gravatar image" /><p><span>pegah</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pegah has no accepted answers">0%</span></p></div></div><div id="comments-container-11805" class="comments-container"></div><div id="comment-tools-11805" class="comment-tools"></div><div class="clear"></div><div id="comment-11805-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

