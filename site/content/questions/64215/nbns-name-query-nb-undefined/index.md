+++
type = "question"
title = "NBNS Name query NB UNDEFINED"
description = '''Dear all, On our office VLAN we see numerous (about 10 percent of all traffic on all switchports) NBNS &quot;UNDEFINED&quot; queries: 1052 23.531946 10.1.1.28 10.1.1.255 NBNS 92 Name query NB UNDEFINED&amp;lt;00&amp;gt;  We also see normal NBNS queries for hostnames, wpad, etc. However we are unable to find any expla...'''
date = "2017-10-26T01:14:00Z"
lastmod = "2017-10-27T00:25:00Z"
weight = 64215
keywords = [ "query", "nbns", "undefined" ]
aliases = [ "/questions/64215" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [NBNS Name query NB UNDEFINED](/questions/64215/nbns-name-query-nb-undefined)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64215-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64215-score" class="post-score" title="current number of votes">0</div><span id="post-64215-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear all,</p><p>On our office VLAN we see numerous (about 10 percent of all traffic on all switchports) NBNS "UNDEFINED" queries:</p><pre><code>1052 23.531946 10.1.1.28 10.1.1.255 NBNS 92 Name query NB UNDEFINED&lt;00&gt;</code></pre><p>We also see normal NBNS queries for hostnames, wpad, etc. However we are unable to find any explanation for the "UNDEFINED" queries. Please can someone shed some light on this?</p><p>Many thanks :)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-query" rel="tag" title="see questions tagged &#39;query&#39;">query</span> <span class="post-tag tag-link-nbns" rel="tag" title="see questions tagged &#39;nbns&#39;">nbns</span> <span class="post-tag tag-link-undefined" rel="tag" title="see questions tagged &#39;undefined&#39;">undefined</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '17, 01:14</strong></p><img src="https://secure.gravatar.com/avatar/91e656c12e2013fc89be28b09d240fdb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="markvdb&#39;s gravatar image" /><p><span>markvdb</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="markvdb has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Oct '17, 02:52</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-64215" class="comments-container"><span id="64239"></span><div id="comment-64239" class="comment"><div id="post-64239-score" class="comment-score"></div><div class="comment-text"><p>Can you share a capture?</p></div><div id="comment-64239-info" class="comment-info"><span class="comment-age">(26 Oct '17, 08:08)</span> <span class="comment-user userinfo">Papa Packet</span></div></div><span id="64267"></span><div id="comment-64267" class="comment"><div id="post-64267-score" class="comment-score"></div><div class="comment-text"><p>As a trace contains customer data, I prefer not to, but if you tell me what you're looking for (in the capture) I'll extract it or make a short trace. Thx!</p></div><div id="comment-64267-info" class="comment-info"><span class="comment-age">(27 Oct '17, 00:25)</span> <span class="comment-user userinfo">markvdb</span></div></div></div><div id="comment-tools-64215" class="comment-tools"></div><div class="clear"></div><div id="comment-64215-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

