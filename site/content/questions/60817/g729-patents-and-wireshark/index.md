+++
type = "question"
title = "G.729 patents and Wireshark"
description = '''According to SiproLAB website (http://www.sipro.com/G729.html) it appears that the G.729 patents have expired. Are any development resources currently working on integrating G.729 decode capabilities into Wireshark?'''
date = "2017-04-13T12:59:00Z"
lastmod = "2017-07-30T03:12:00Z"
weight = 60817
keywords = [ "g729", "g.729" ]
aliases = [ "/questions/60817" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [G.729 patents and Wireshark](/questions/60817/g729-patents-and-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60817-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60817-score" class="post-score" title="current number of votes">0</div><span id="post-60817-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>According to SiproLAB website (<a href="http://www.sipro.com/G729.html)">http://www.sipro.com/G729.html)</a> it appears that the G.729 patents have expired. Are any development resources currently working on integrating G.729 decode capabilities into Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-g729" rel="tag" title="see questions tagged &#39;g729&#39;">g729</span> <span class="post-tag tag-link-g.729" rel="tag" title="see questions tagged &#39;g.729&#39;">g.729</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Apr '17, 12:59</strong></p><img src="https://secure.gravatar.com/avatar/403d49734ede4db31889667535e3cbdd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ChuckD&#39;s gravatar image" /><p><span>ChuckD</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ChuckD has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Apr '17, 07:48</strong> </span></p></div></div><div id="comments-container-60817" class="comments-container"></div><div id="comment-tools-60817" class="comment-tools"></div><div class="clear"></div><div id="comment-60817-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="63243"></span>

<div id="answer-container-63243" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63243-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63243-score" class="post-score" title="current number of votes">2</div><span id="post-63243-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>G.729 playback is now merged in Wireshark development builds for Linux and Windows (starting from v2.5.0rc0-558-g3e54cabf81). You can grab Windows pre compiled binaries <a href="https://www.wireshark.org/download/automated/">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jul '17, 03:12</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-63243" class="comments-container"></div><div id="comment-tools-63243" class="comment-tools"></div><div class="clear"></div><div id="comment-63243-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="60831"></span>

<div id="answer-container-60831" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60831-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60831-score" class="post-score" title="current number of votes">0</div><span id="post-60831-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's always a chance, and the odds have now improved considerably.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Apr '17, 04:35</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-60831" class="comments-container"></div><div id="comment-tools-60831" class="comment-tools"></div><div class="clear"></div><div id="comment-60831-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="60833"></span>

<div id="answer-container-60833" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60833-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60833-score" class="post-score" title="current number of votes">0</div><span id="post-60833-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No one is currently working on it as far as I know, but it's worth filling an enhancement request in our <a href="https://bugs.wireshark.org/bugzilla/">Bugzilla</a> with a pcap containing a call using this codec for testing purpose.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Apr '17, 10:42</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-60833" class="comments-container"><span id="60861"></span><div id="comment-60861" class="comment"><div id="post-60861-score" class="comment-score">2</div><div class="comment-text"><p>I planned to add this feature in February, but have not gotten to this yet. A sample capture for G.729A is available at <a href="https://wiki.wireshark.org/SampleCaptures#SIP_and_RTP,">https://wiki.wireshark.org/SampleCaptures#SIP_and_RTP,</a> while the <a href="http://www.linphone.org/technical-corner/bcg729/overview">bcg729 library</a> looks promising. (<a href="https://lwn.net/Articles/713292/">Apparently</a> ffmpeg has a decoder too, but I did not look into that.)</p></div><div id="comment-60861-info" class="comment-info"><span class="comment-age">(16 Apr '17, 16:39)</span> <span class="comment-user userinfo">Lekensteyn</span></div></div><span id="60939"></span><div id="comment-60939" class="comment"><div id="post-60939-score" class="comment-score">2</div><div class="comment-text"><p>For the sake of completeness: Enhancement request is documented with <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=13635">bug 13635</a></p></div><div id="comment-60939-info" class="comment-info"><span class="comment-age">(20 Apr '17, 23:30)</span> <span class="comment-user userinfo">Uli</span></div></div><span id="63128"></span><div id="comment-63128" class="comment"><div id="post-63128-score" class="comment-score"></div><div class="comment-text"><p>Guys, all voip engineers around the world will kiss your hands after you implement g729 decoder into wireshark.</p></div><div id="comment-63128-info" class="comment-info"><span class="comment-age">(26 Jul '17, 05:42)</span> <span class="comment-user userinfo">ky4k0b</span></div></div><span id="63137"></span><div id="comment-63137" class="comment"><div id="post-63137-score" class="comment-score">1</div><div class="comment-text"><p>Until that happens, there is always the possibility to extract the RTP flow alone and use pcapplay capabilities of SIPp, playing the captured stream to a hardware phone or a transcoding SBC.</p></div><div id="comment-63137-info" class="comment-info"><span class="comment-age">(26 Jul '17, 08:54)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="63162"></span><div id="comment-63162" class="comment"><div id="post-63162-score" class="comment-score"></div><div class="comment-text"><p>You can follow <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=13635">bug 13635</a> as I'm currently working on a patch adding this capability</p></div><div id="comment-63162-info" class="comment-info"><span class="comment-age">(27 Jul '17, 00:33)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div><span id="63164"></span><div id="comment-63164" class="comment not_top_scorer"><div id="post-63164-score" class="comment-score"></div><div class="comment-text"><p>I'll try and have a look.</p></div><div id="comment-63164-info" class="comment-info"><span class="comment-age">(27 Jul '17, 01:38)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-60833" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-60833-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

