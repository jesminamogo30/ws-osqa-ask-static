+++
type = "question"
title = "ble-sniffer-osx fails to find Wireshark"
description = '''The bel-sniffer-osx can sniff but &quot;Capture to Wireshark&quot; button is dim.'''
date = "2015-07-15T06:52:00Z"
lastmod = "2015-07-17T06:49:00Z"
weight = 44173
keywords = [ "ble-sniffer-osx", "nordic" ]
aliases = [ "/questions/44173" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [ble-sniffer-osx fails to find Wireshark](/questions/44173/ble-sniffer-osx-fails-to-find-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44173-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44173-score" class="post-score" title="current number of votes">0</div><span id="post-44173-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The bel-sniffer-osx can sniff but "Capture to Wireshark" button is dim.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ble-sniffer-osx" rel="tag" title="see questions tagged &#39;ble-sniffer-osx&#39;">ble-sniffer-osx</span> <span class="post-tag tag-link-nordic" rel="tag" title="see questions tagged &#39;nordic&#39;">nordic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jul '15, 06:52</strong></p><img src="https://secure.gravatar.com/avatar/a2005183e9bc8d1a3f335965972cbb2f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Max%20Lu&#39;s gravatar image" /><p><span>Max Lu</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Max Lu has no accepted answers">0%</span></p></div></div><div id="comments-container-44173" class="comments-container"></div><div id="comment-tools-44173" class="comment-tools"></div><div class="clear"></div><div id="comment-44173-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="44176"></span>

<div id="answer-container-44176" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44176-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44176-score" class="post-score" title="current number of votes">0</div><span id="post-44176-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please contact Roland King via <a href="http://sourceforge.net/projects/nrfblesnifferosx/">nrf-ble-sniffer-osx</a>, assuming that is what you mean. As this is not a Wireshark problem itself please contact the relevant forum.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jul '15, 07:40</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-44176" class="comments-container"><span id="44208"></span><div id="comment-44208" class="comment"><div id="post-44208-score" class="comment-score"></div><div class="comment-text"><p>That's probably because you're missing some relevant plugin I assume.</p></div><div id="comment-44208-info" class="comment-info"><span class="comment-age">(16 Jul '15, 05:42)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-44176" class="comment-tools"></div><div class="clear"></div><div id="comment-44176-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="44240"></span>

<div id="answer-container-44240" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44240-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44240-score" class="post-score" title="current number of votes">0</div><span id="post-44240-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I fix it. The ble=sniffer-osx can't find Wireshark because my access to /usr/local/bin is denied. I have to type the following command in terminal: sudo chown -R $(whoami) /usr/local/bin</p><p>After that, Wireshark works well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jul '15, 06:49</strong></p><img src="https://secure.gravatar.com/avatar/a2005183e9bc8d1a3f335965972cbb2f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Max%20Lu&#39;s gravatar image" /><p><span>Max Lu</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Max Lu has no accepted answers">0%</span></p></div></div><div id="comments-container-44240" class="comments-container"></div><div id="comment-tools-44240" class="comment-tools"></div><div class="clear"></div><div id="comment-44240-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

