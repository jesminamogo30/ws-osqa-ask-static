+++
type = "question"
title = "Why the Sequence field in quic Protocl changed suddenly and not continuous"
description = '''When I decode a Quic capture file, the Sequence field in QUIC header changed suddenly and not continuous, even the all packets in a same flow and NO. field in wireshake is continuous. Sequence changed like (10-》11-》1000-》1001=》1002 =》1 =》2) it seems '''
date = "2017-01-16T23:48:00Z"
lastmod = "2017-01-16T23:48:00Z"
weight = 58828
keywords = [ "quic" ]
aliases = [ "/questions/58828" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Why the Sequence field in quic Protocl changed suddenly and not continuous](/questions/58828/why-the-sequence-field-in-quic-protocl-changed-suddenly-and-not-continuous)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58828-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58828-score" class="post-score" title="current number of votes">0</div><span id="post-58828-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I decode a Quic capture file, the Sequence field in QUIC header changed suddenly and not continuous, even the all packets in a same flow and NO. field in wireshake is continuous. Sequence changed like (10-》11-》1000-》1001=》1002 =》1 =》2) it seems</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-quic" rel="tag" title="see questions tagged &#39;quic&#39;">quic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jan '17, 23:48</strong></p><img src="https://secure.gravatar.com/avatar/853d7093103a60a3b0083b42b705b99e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="neil_hao&#39;s gravatar image" /><p><span>neil_hao</span><br />
<span class="score" title="26 reputation points">26</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="neil_hao has no accepted answers">0%</span></p></div></div><div id="comments-container-58828" class="comments-container"></div><div id="comment-tools-58828" class="comment-tools"></div><div class="clear"></div><div id="comment-58828-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

