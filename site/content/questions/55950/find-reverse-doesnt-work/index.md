+++
type = "question"
title = "Find reverse doesn&#x27;t work"
description = '''After capturing traffic and selecting all RTP streams then selecting a packet. The find reverse button no longer finds the reverse part of the stream. Why is this? is there a new step? It worked on older versions of Wireshark. Thanks for any help!'''
date = "2016-09-28T08:25:00Z"
lastmod = "2016-09-28T08:45:00Z"
weight = 55950
keywords = [ "rtp" ]
aliases = [ "/questions/55950" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Find reverse doesn't work](/questions/55950/find-reverse-doesnt-work)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55950-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55950-score" class="post-score" title="current number of votes">0</div><span id="post-55950-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>After capturing traffic and selecting all RTP streams then selecting a packet. The find reverse button no longer finds the reverse part of the stream. Why is this? is there a new step? It worked on older versions of Wireshark. Thanks for any help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Sep '16, 08:25</strong></p><img src="https://secure.gravatar.com/avatar/4c483b36d182fca3ee007e2191c49b3c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nugget_59&#39;s gravatar image" /><p><span>Nugget_59</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nugget_59 has no accepted answers">0%</span></p></div></div><div id="comments-container-55950" class="comments-container"><span id="55951"></span><div id="comment-55951" class="comment"><div id="post-55951-score" class="comment-score"></div><div class="comment-text"><p>Does the same capture still work in an older version, i.e. is the issue with the capture rather than the newer version?</p></div><div id="comment-55951-info" class="comment-info"><span class="comment-age">(28 Sep '16, 08:27)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="55952"></span><div id="comment-55952" class="comment"><div id="post-55952-score" class="comment-score"></div><div class="comment-text"><p>Yes it works with the capture on older version. The older version of WS will allow you to click find reverse and it then highlights the reverse stream and then you can analyze both streams simultaneously. The newer version of WS. nothing happens when you click find reverse.</p></div><div id="comment-55952-info" class="comment-info"><span class="comment-age">(28 Sep '16, 08:32)</span> <span class="comment-user userinfo">Nugget_59</span></div></div><span id="55953"></span><div id="comment-55953" class="comment"><div id="post-55953-score" class="comment-score"></div><div class="comment-text"><p>Same case here, worth filing a bug.</p></div><div id="comment-55953-info" class="comment-info"><span class="comment-age">(28 Sep '16, 08:34)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="55954"></span><div id="comment-55954" class="comment"><div id="post-55954-score" class="comment-score"></div><div class="comment-text"><p>Just joined for this one issue. Where do I file bug report?</p></div><div id="comment-55954-info" class="comment-info"><span class="comment-age">(28 Sep '16, 08:35)</span> <span class="comment-user userinfo">Nugget_59</span></div></div><span id="55956"></span><div id="comment-55956" class="comment"><div id="post-55956-score" class="comment-score">1</div><div class="comment-text"><p>The Wireshark <a href="https://bugs.wireshark.org">Bugzilla</a>, noting the information on the wiki page about submitting <a href="https://wiki.wireshark.org/ReportingBugs">bug reports</a>.</p></div><div id="comment-55956-info" class="comment-info"><span class="comment-age">(28 Sep '16, 08:45)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-55950" class="comment-tools"></div><div class="clear"></div><div id="comment-55950-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

