+++
type = "question"
title = "Catch all packets going over Windows Hosted Virtual Adapter"
description = '''I created virtual hosted adapter on my notebook (Win 8.1) and conntected iphone and tv to this wi-fi (becouse i need to understand one protocol) than i tryed to catch packets going between them but without success (I used wireshark &amp;amp; RawCap) but i didnt catched any packets (but im sure that some...'''
date = "2014-02-27T01:11:00Z"
lastmod = "2014-02-27T01:11:00Z"
weight = 30227
keywords = [ "wlan", "wifi", "packets", "wireshark" ]
aliases = [ "/questions/30227" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Catch all packets going over Windows Hosted Virtual Adapter](/questions/30227/catch-all-packets-going-over-windows-hosted-virtual-adapter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30227-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30227-score" class="post-score" title="current number of votes">0</div><span id="post-30227-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I created virtual hosted adapter on my notebook (Win 8.1) and conntected iphone and tv to this wi-fi (becouse i need to understand one protocol) than i tryed to catch packets going between them but without success (I used wireshark &amp; RawCap) but i didnt catched any packets (but im sure that some packets are delivered becouse the purpouse of the protocol - app works)</p><p>When i tryed to communicate from my pc to tv(same protocol) i was able to catch that packets. Also when tv or iphone requested somesthing on internet ( I shared pc ethernet adapter with virtual adapter) I was able to catch all these packets incoming / outgoing.</p><p>I have enabled promiscuity mode.</p><p>How can i acces them ?</p><p>Do you have any other suggestion how to cath all packet of tv or iphone ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '14, 01:11</strong></p><img src="https://secure.gravatar.com/avatar/1d751c6a335be9fb0f1b9fab7f7ffbc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Osel%20Miko%20D%C5%99evorubec&#39;s gravatar image" /><p><span>Osel Miko Dř...</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Osel Miko Dřevorubec has one accepted answer">100%</span></p></div></div><div id="comments-container-30227" class="comments-container"></div><div id="comment-tools-30227" class="comment-tools"></div><div class="clear"></div><div id="comment-30227-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

