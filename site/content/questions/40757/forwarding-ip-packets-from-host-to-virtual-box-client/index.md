+++
type = "question"
title = "[closed] Forwarding IP packets from Host to Virtual box client"
description = '''I have wireshark installed on both Win 7 host and Ubuntu client. I am able to see my LAN traffic on the windows 7 host, I want to forward IP packets from a certain IP address to the Ubuntu client. Can some one help me with this? '''
date = "2015-03-21T06:34:00Z"
lastmod = "2015-03-21T06:34:00Z"
weight = 40757
keywords = [ "virtualbox" ]
aliases = [ "/questions/40757" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Forwarding IP packets from Host to Virtual box client](/questions/40757/forwarding-ip-packets-from-host-to-virtual-box-client)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40757-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40757-score" class="post-score" title="current number of votes">0</div><span id="post-40757-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have wireshark installed on both Win 7 host and Ubuntu client. I am able to see my LAN traffic on the windows 7 host, I want to forward IP packets from a certain IP address to the Ubuntu client. Can some one help me with this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-virtualbox" rel="tag" title="see questions tagged &#39;virtualbox&#39;">virtualbox</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Mar '15, 06:34</strong></p><img src="https://secure.gravatar.com/avatar/99d3921c59ae3b2e97b089f206cdd36c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nachi&#39;s gravatar image" /><p><span>Nachi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nachi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>21 Mar '15, 12:41</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-40757" class="comments-container"></div><div id="comment-tools-40757" class="comment-tools"></div><div class="clear"></div><div id="comment-40757-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 21 Mar '15, 12:41

</div>

</div>

</div>

