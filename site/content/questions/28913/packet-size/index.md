+++
type = "question"
title = "Packet size"
description = '''Can Wireshark provide you with network traffic packet size counts? How and where ? Are you able to distinguish how many of each packet size was transmitted on your LAN segment? '''
date = "2014-01-15T07:03:00Z"
lastmod = "2014-01-15T13:32:00Z"
weight = 28913
keywords = [ "homework", "packet", "size" ]
aliases = [ "/questions/28913" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packet size](/questions/28913/packet-size)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28913-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28913-score" class="post-score" title="current number of votes">0</div><span id="post-28913-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can Wireshark provide you with network traffic packet size counts? How and where ? Are you able to distinguish how many of each packet size was transmitted on your LAN segment?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-homework" rel="tag" title="see questions tagged &#39;homework&#39;">homework</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-size" rel="tag" title="see questions tagged &#39;size&#39;">size</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '14, 07:03</strong></p><img src="https://secure.gravatar.com/avatar/81dc7ef173dc45679c115b238b7eeadb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jw123&#39;s gravatar image" /><p><span>jw123</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jw123 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jan '14, 07:25</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-28913" class="comments-container"><span id="28916"></span><div id="comment-28916" class="comment"><div id="post-28916-score" class="comment-score">2</div><div class="comment-text"><p>The answer might be a bit longer. By when do you have to submit the homework?</p></div><div id="comment-28916-info" class="comment-info"><span class="comment-age">(15 Jan '14, 07:07)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="28920"></span><div id="comment-28920" class="comment"><div id="post-28920-score" class="comment-score"></div><div class="comment-text"><p>Sunday 1/19</p></div><div id="comment-28920-info" class="comment-info"><span class="comment-age">(15 Jan '14, 07:16)</span> <span class="comment-user userinfo">jw123</span></div></div></div><div id="comment-tools-28913" class="comment-tools"></div><div class="clear"></div><div id="comment-28913-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28935"></span>

<div id="answer-container-28935" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28935-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28935-score" class="post-score" title="current number of votes">0</div><span id="post-28935-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The answer is pretty short when just just hinting about the solution I think, so here we go: "Statistics menu".</p><p>Sometimes I wonder if students even bother to fire up Wireshark to take a look at where it could be before coming here and asking for answers... :-) [not part of the answer, so it's still short ;-)]</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jan '14, 13:32</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-28935" class="comments-container"></div><div id="comment-tools-28935" class="comment-tools"></div><div class="clear"></div><div id="comment-28935-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

