+++
type = "question"
title = "Supported Adapters for Wireless packet capturing"
description = '''Hello,  Please suggest us the Adapter to be used along with Wireshark for capturing Wireless packets.  If you have the list of supported adapters in your web page please direct us to the link.  Also we would like to know if the adapter model D-Link DWA-160 can be used with Wireshark for capturing Wi...'''
date = "2012-01-20T04:16:00Z"
lastmod = "2012-01-20T16:32:00Z"
weight = 8504
keywords = [ "wireless", "adapter" ]
aliases = [ "/questions/8504" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Supported Adapters for Wireless packet capturing](/questions/8504/supported-adapters-for-wireless-packet-capturing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8504-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8504-score" class="post-score" title="current number of votes">0</div><span id="post-8504-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Please suggest us the Adapter to be used along with Wireshark for capturing Wireless packets.</p><p>If you have the list of supported adapters in your web page please direct us to the link.</p><p>Also we would like to know if the adapter model D-Link DWA-160 can be used with Wireshark for capturing Wireless packets?</p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-adapter" rel="tag" title="see questions tagged &#39;adapter&#39;">adapter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jan '12, 04:16</strong></p><img src="https://secure.gravatar.com/avatar/edfb7bcc9e39f5df69d04184d8e8397f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="urmila&#39;s gravatar image" /><p><span>urmila</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="urmila has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Feb '12, 20:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-8504" class="comments-container"></div><div id="comment-tools-8504" class="comment-tools"></div><div class="clear"></div><div id="comment-8504-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="8515"></span>

<div id="answer-container-8515" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8515-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8515-score" class="post-score" title="current number of votes">0</div><span id="post-8515-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you're talking wireless captures on Windows you'll have to buy AirPCAP adapters, because any other adapter will not show you frames other than your own, and without the physical layer. If you're running Linux you might take a look at cards with Atheros chipsets, which have a good reputation.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jan '12, 12:33</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-8515" class="comments-container"><span id="8522"></span><div id="comment-8522" class="comment"><div id="post-8522-score" class="comment-score"></div><div class="comment-text"><p>...and if you're running Mac OS X (on a Macintosh, rather than a Hackintosh), the built-in wireless adapters on those Macs that have them should work in monitor mode).</p><p>For Linux, at least, there's an item in the aircrack-ng FAQ <a href="http://www.aircrack-ng.org/doku.php?id=faq#what_is_the_best_wireless_card_to_buy">"What is the best wireless card to buy?"</a>.</p></div><div id="comment-8522-info" class="comment-info"><span class="comment-age">(20 Jan '12, 15:34)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-8515" class="comment-tools"></div><div class="clear"></div><div id="comment-8515-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="8523"></span>

<div id="answer-container-8523" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8523-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8523-score" class="post-score" title="current number of votes">0</div><span id="post-8523-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark ultimately depends on the operating system on which it's running and on the drivers for the wireless adapter for monitor mode support, so we don't maintain our own list of supported adapters.</p><p>For Linux, we have, in the Linux subsection of <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">the CaptureSetup/WLAN page of the Wireshark Wiki</a> links to a number of pages that discuss adapter support on Linux.</p><p>For *BSD, I think most drivers should support monitor mode if the adapters are supported at all and support monitor mode; if any of the pages that discuss adapter support on Linux discuss the hardware merits or cost of particular adapters, those would apply to *BSD as well.</p><p>As I noted in my comment on Jasper's answer, if you're running Mac OS X on a Mac, if it has a wireless adapter, monitor mode should work.</p><p>For all of those OSes, see <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">the CaptureSetup/WLAN page of the Wireshark Wiki</a> for information on how to enable monitor mode.</p><p>For Windows, we don't support monitor mode on any adapters, and promiscuous mode generally doesn't work very well, so you can only capture in non-promiscuous mode, meaning you'll only see traffic to and from your machine. As Jasper noted, you'd need an <a href="http://www.riverbed.com/us/products/cascade/wireshark_enhancements/airpcap.php">AirPcap adapter</a> to capture traffic to and from other machines on the network.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jan '12, 16:32</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jan '12, 16:32</strong> </span></p></div></div><div id="comments-container-8523" class="comments-container"></div><div id="comment-tools-8523" class="comment-tools"></div><div class="clear"></div><div id="comment-8523-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

