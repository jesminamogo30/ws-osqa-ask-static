+++
type = "question"
title = "Mac users help"
description = '''I&#x27;ve downloaded wireshark and quartz from trusted sites, although when I try to run wireshark I get a notification &quot;unable to open programme&quot; which quickly turns to &quot;wireshark quit unexpectedly, reopen?&quot; Where have I gone wrong?'''
date = "2017-04-29T16:24:00Z"
lastmod = "2017-04-30T00:59:00Z"
weight = 61118
keywords = [ "mac" ]
aliases = [ "/questions/61118" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Mac users help](/questions/61118/mac-users-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61118-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61118-score" class="post-score" title="current number of votes">0</div><span id="post-61118-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've downloaded wireshark and quartz from trusted sites, although when I try to run wireshark I get a notification "unable to open programme" which quickly turns to "wireshark quit unexpectedly, reopen?" Where have I gone wrong?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Apr '17, 16:24</strong></p><img src="https://secure.gravatar.com/avatar/33ee67be53273e7cdbada9a11aec2cb1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dangodsall&#39;s gravatar image" /><p><span>dangodsall</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dangodsall has no accepted answers">0%</span></p></div></div><div id="comments-container-61118" class="comments-container"><span id="61119"></span><div id="comment-61119" class="comment"><div id="post-61119-score" class="comment-score"></div><div class="comment-text"><p>Versions used?</p></div><div id="comment-61119-info" class="comment-info"><span class="comment-age">(29 Apr '17, 16:33)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="61120"></span><div id="comment-61120" class="comment"><div id="post-61120-score" class="comment-score"></div><div class="comment-text"><p>xquartz - 2.7.11 Wireshark 2.2.6</p><p>Currently running macos sierra</p></div><div id="comment-61120-info" class="comment-info"><span class="comment-age">(29 Apr '17, 16:36)</span> <span class="comment-user userinfo">dangodsall</span></div></div><span id="61124"></span><div id="comment-61124" class="comment"><div id="post-61124-score" class="comment-score"></div><div class="comment-text"><p>Okay then, no use for xquartz here, since Wireshark 2.2 uses the Qt interface libraries to provide a macOS native interface. Have you installed from the Wireshark 2.2.6 intel 64.dmg ?</p></div><div id="comment-61124-info" class="comment-info"><span class="comment-age">(30 Apr '17, 00:59)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-61118" class="comment-tools"></div><div class="clear"></div><div id="comment-61118-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61121"></span>

<div id="answer-container-61121" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61121-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61121-score" class="post-score" title="current number of votes">0</div><span id="post-61121-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What were the "trusted sites"? If their hostnames didn't end in "wireshark.org" can you try downloading the official macOS installer from <a href="https://www.wireshark.org/?">https://www.wireshark.org/?</a> Note that we no longer require XQuartz.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Apr '17, 16:46</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-61121" class="comments-container"><span id="61122"></span><div id="comment-61122" class="comment"><div id="post-61122-score" class="comment-score"></div><div class="comment-text"><p>Sadly was from wireshark,hense the confusion</p></div><div id="comment-61122-info" class="comment-info"><span class="comment-age">(29 Apr '17, 16:49)</span> <span class="comment-user userinfo">dangodsall</span></div></div></div><div id="comment-tools-61121" class="comment-tools"></div><div class="clear"></div><div id="comment-61121-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

