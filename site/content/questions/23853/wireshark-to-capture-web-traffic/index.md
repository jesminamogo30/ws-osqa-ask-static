+++
type = "question"
title = "Wireshark to capture web traffic"
description = '''Hi all, I&#x27;m new to the forum.... I have a Motorola SBG 6580 modem/router/WiFi at home on cable broadband. I have 1 PC which is connected to the 1 of the 4 ports on the router. I have 1 laptop, which is connected wirelessly to the router. Is it possible to capture any network traffic; in particular w...'''
date = "2013-08-19T14:48:00Z"
lastmod = "2013-08-20T02:31:00Z"
weight = 23853
keywords = [ "web", "6580", "traffic", "sbg" ]
aliases = [ "/questions/23853" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark to capture web traffic](/questions/23853/wireshark-to-capture-web-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23853-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23853-score" class="post-score" title="current number of votes">0</div><span id="post-23853-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all, I'm new to the forum.... I have a Motorola SBG 6580 modem/router/WiFi at home on cable broadband. I have 1 PC which is connected to the 1 of the 4 ports on the router. I have 1 laptop, which is connected wirelessly to the router.</p><p>Is it possible to capture any network traffic; in particular web traffic coming out of the laptop?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-web" rel="tag" title="see questions tagged &#39;web&#39;">web</span> <span class="post-tag tag-link-6580" rel="tag" title="see questions tagged &#39;6580&#39;">6580</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-sbg" rel="tag" title="see questions tagged &#39;sbg&#39;">sbg</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Aug '13, 14:48</strong></p><img src="https://secure.gravatar.com/avatar/9937da77d5095867ba14a4ff1dd362f6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wiresharkuser&#39;s gravatar image" /><p><span>wiresharkuser</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wiresharkuser has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Aug '13, 02:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-23853" class="comments-container"><span id="23860"></span><div id="comment-23860" class="comment"><div id="post-23860-score" class="comment-score"></div><div class="comment-text"><p>have you tried to capture on related interfaces at each laptop? use <code>http</code> filter. and read <a href="http://ask.wireshark.org/questions/23847/how-can-i-capture-traffic-between-two-devices-on-wi-fi/23852">this</a></p></div><div id="comment-23860-info" class="comment-info"><span class="comment-age">(20 Aug '13, 01:48)</span> <span class="comment-user userinfo">HiB</span></div></div><span id="23867"></span><div id="comment-23867" class="comment"><div id="post-23867-score" class="comment-score"></div><div class="comment-text"><p>I believe the OP wants to capture the Laptop traffic on the PC, which would be impossible unless the SBG 6580 supports it with Port Mirroring. However, a Google search does not indicate such a feature on the SBG 6580.</p></div><div id="comment-23867-info" class="comment-info"><span class="comment-age">(20 Aug '13, 02:31)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-23853" class="comment-tools"></div><div class="clear"></div><div id="comment-23853-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

