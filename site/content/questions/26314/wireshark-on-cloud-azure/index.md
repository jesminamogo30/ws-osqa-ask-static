+++
type = "question"
title = "WireShark on cloud -Azure"
description = '''can i run it on microsoft cloud - azure?'''
date = "2013-10-23T01:53:00Z"
lastmod = "2013-10-23T04:20:00Z"
weight = 26314
keywords = [ "azure", "cloud" ]
aliases = [ "/questions/26314" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark on cloud -Azure](/questions/26314/wireshark-on-cloud-azure)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26314-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26314-score" class="post-score" title="current number of votes">0</div><span id="post-26314-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>can i run it on microsoft cloud - azure?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-azure" rel="tag" title="see questions tagged &#39;azure&#39;">azure</span> <span class="post-tag tag-link-cloud" rel="tag" title="see questions tagged &#39;cloud&#39;">cloud</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Oct '13, 01:53</strong></p><img src="https://secure.gravatar.com/avatar/7cc57fd6b7fa2cb83e1298a391b186c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shruthi&#39;s gravatar image" /><p><span>Shruthi</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shruthi has no accepted answers">0%</span></p></div></div><div id="comments-container-26314" class="comments-container"></div><div id="comment-tools-26314" class="comment-tools"></div><div class="clear"></div><div id="comment-26314-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26319"></span>

<div id="answer-container-26319" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26319-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26319-score" class="post-score" title="current number of votes">0</div><span id="post-26319-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Azure is just a 'framework' for virtual machines hosted by Microsoft. You can run Windows or Linux in those virtual machines and you can install whatever software you want (maybe with some exceptions - check the Azure docs).</p><p>So, I don't see any reason why Wireshark would not run in Azure.</p><p>But what do want to do? Analyzing capture files might work well. Capturing traffic is most certainly limited to the traffic of the virtual where Wireshark is running.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Oct '13, 04:20</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 Oct '13, 04:22</strong> </span></p></div></div><div id="comments-container-26319" class="comments-container"></div><div id="comment-tools-26319" class="comment-tools"></div><div class="clear"></div><div id="comment-26319-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

