+++
type = "question"
title = "How to use??"
description = '''I&#x27;m new and have zero experience. I have a VERY hacked network that no antivirus, computer professional, wipes, etc. can fix. I tried to set up and run wire shark, but it keeps saying it can&#x27;t capture due to capture setting being promiscuous. Even when I click off the promiscuous option, it still wo...'''
date = "2017-07-04T20:25:00Z"
lastmod = "2017-07-04T23:44:00Z"
weight = 62509
keywords = [ "urgent-help" ]
aliases = [ "/questions/62509" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to use??](/questions/62509/how-to-use)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62509-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62509-score" class="post-score" title="current number of votes">0</div><span id="post-62509-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm new and have zero experience. I have a VERY hacked network that no antivirus, computer professional, wipes, etc. can fix. I tried to set up and run wire shark, but it keeps saying it can't capture due to capture setting being promiscuous. Even when I click off the promiscuous option, it still won't run.</p><p>Is there an easy way I can gather info showing what and who is on my network and computers so I can understand how to get rid of unwanted people and processes and protect my network? HELP. SOS Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-urgent-help" rel="tag" title="see questions tagged &#39;urgent-help&#39;">urgent-help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jul '17, 20:25</strong></p><img src="https://secure.gravatar.com/avatar/44e35b5827c8b81fc507f12ea7954fd4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ljkara&#39;s gravatar image" /><p><span>ljkara</span><br />
<span class="score" title="9 reputation points">9</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ljkara has no accepted answers">0%</span></p></div></div><div id="comments-container-62509" class="comments-container"></div><div id="comment-tools-62509" class="comment-tools"></div><div class="clear"></div><div id="comment-62509-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62512"></span>

<div id="answer-container-62512" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62512-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62512-score" class="post-score" title="current number of votes">0</div><span id="post-62512-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It seems that you have other problems than running Wireshark. Have a professional come in and assist you with your situation, on site. The scope is just to large for a simps Q&amp;A site.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jul '17, 23:44</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-62512" class="comments-container"></div><div id="comment-tools-62512" class="comment-tools"></div><div class="clear"></div><div id="comment-62512-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

