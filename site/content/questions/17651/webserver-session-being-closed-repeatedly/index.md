+++
type = "question"
title = "Webserver session being closed repeatedly"
description = '''Hi all: I have a surveillance DVR that hosts a webserver through port 8880 and I&#x27;ve been having trouble accessing its webpage, its extremely slow to load components. I captured its traffic and I&#x27;m seeing a lot of TCP FIN packets being sent and the connection re-established, the server&#x27;s page eventua...'''
date = "2013-01-13T10:08:00Z"
lastmod = "2013-01-17T18:50:00Z"
weight = 17651
keywords = [ "fin", "connection", "slow", "http", "tcp" ]
aliases = [ "/questions/17651" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Webserver session being closed repeatedly](/questions/17651/webserver-session-being-closed-repeatedly)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17651-score" class="post-score" title="current number of votes">0</div><span id="post-17651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all:<br />
I have a surveillance DVR that hosts a webserver through port 8880 and I've been having trouble accessing its webpage, its extremely slow to load components. I captured its traffic and I'm seeing a lot of TCP FIN packets being sent and the connection re-established, the server's page eventually loads though, just want to get some insight as to a cause. Please let my know anything else you would need to know as I'm sure what I've provided is not enough.<br />
cddbp-alt = 8880<br />
<img src="http://rk.blahsoft.com/sw-17616_IE2.png" alt="alt text" /></p><p><img src="http://rk.blahsoft.com/sw-17616_IE.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fin" rel="tag" title="see questions tagged &#39;fin&#39;">fin</span> <span class="post-tag tag-link-connection" rel="tag" title="see questions tagged &#39;connection&#39;">connection</span> <span class="post-tag tag-link-slow" rel="tag" title="see questions tagged &#39;slow&#39;">slow</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jan '13, 10:08</strong></p><img src="https://secure.gravatar.com/avatar/f08740de6f428d94664d52640ad2f113?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="reikokuko&#39;s gravatar image" /><p><span>reikokuko</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="reikokuko has no accepted answers">0%</span> </br></br></p></img></div></div><div id="comments-container-17651" class="comments-container"><span id="17764"></span><div id="comment-17764" class="comment"><div id="post-17764-score" class="comment-score"></div><div class="comment-text"><p>who is sending the FIN packet first? the DVR or your pc? If you can add a screenshot of the packet capture that will help, or even better the upload the capture to cloudshark.org.</p></div><div id="comment-17764-info" class="comment-info"><span class="comment-age">(17 Jan '13, 18:50)</span> <span class="comment-user userinfo">jasther</span></div></div></div><div id="comment-tools-17651" class="comment-tools"></div><div class="clear"></div><div id="comment-17651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

