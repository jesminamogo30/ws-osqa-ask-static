+++
type = "question"
title = "Which Wireshark version should I download on windows 8?"
description = '''I think I&#x27;m running 8.1, encase that makes a difference. Thanks'''
date = "2016-09-09T22:30:00Z"
lastmod = "2016-09-12T14:34:00Z"
weight = 55454
keywords = [ "windows", "newbie", "wireshark" ]
aliases = [ "/questions/55454" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Which Wireshark version should I download on windows 8?](/questions/55454/which-wireshark-version-should-i-download-on-windows-8)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55454-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55454-score" class="post-score" title="current number of votes">0</div><span id="post-55454-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I think I'm running 8.1, encase that makes a difference. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-newbie" rel="tag" title="see questions tagged &#39;newbie&#39;">newbie</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '16, 22:30</strong></p><img src="https://secure.gravatar.com/avatar/031ce4da77db40d3677f346991f020df?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nicoletta&#39;s gravatar image" /><p><span>Nicoletta</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nicoletta has no accepted answers">0%</span></p></div></div><div id="comments-container-55454" class="comments-container"></div><div id="comment-tools-55454" class="comment-tools"></div><div class="clear"></div><div id="comment-55454-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55456"></span>

<div id="answer-container-55456" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55456-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55456-score" class="post-score" title="current number of votes">1</div><span id="post-55456-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The newest stable version (32 or 64 bit one depending on your system) is the best starting point for most uses, and it is what the download section of www.wireshark.org offers you first. However, even the latest stable version may sometimes introduce new bugs which prevent it from working properly in your particular environment, so you may need to use an older one. Or the latest stable version may not contain yet some features you need, so you may need to use a development one which, for most of the time, offers more features for the price of less stability.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Sep '16, 22:57</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Sep '16, 04:41</strong> </span></p></div></div><div id="comments-container-55456" class="comments-container"><span id="55460"></span><div id="comment-55460" class="comment"><div id="post-55460-score" class="comment-score"></div><div class="comment-text"><p>Wireshark 2.2.x supports Windows versions from Vista onwards, as stated in the <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChIntroPlatforms.html">User Guide</a> which is linked from the download page.</p><blockquote>The current version of Wireshark should support any version of Windows that is still within its extended support lifetime. At the time of writing this includes Windows 10, 8, 7, Vista, Server 2016, Server 2012 R2, Server 2012, Server 2008 R2, and Server 2008.</blockquote><p>The latest stable always has the least number of unknown bugs, but will likely have the same number of unknown bugs as previous versions.</p></div><div id="comment-55460-info" class="comment-info"><span class="comment-age">(10 Sep '16, 03:43)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="55462"></span><div id="comment-55462" class="comment"><div id="post-55462-score" class="comment-score"></div><div class="comment-text"><p>Thanks, for your help Sindy.</p></div><div id="comment-55462-info" class="comment-info"><span class="comment-age">(10 Sep '16, 04:33)</span> <span class="comment-user userinfo">Nicoletta</span></div></div><span id="55496"></span><div id="comment-55496" class="comment"><div id="post-55496-score" class="comment-score"></div><div class="comment-text"><p>To expand a bit on sindy's answer, the wireshark.org main page and download page should draw a little download indicator next to the link most appropriate for your platform. If it doesn't then that's a web site bug.</p></div><div id="comment-55496-info" class="comment-info"><span class="comment-age">(12 Sep '16, 14:34)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-55456" class="comment-tools"></div><div class="clear"></div><div id="comment-55456-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

