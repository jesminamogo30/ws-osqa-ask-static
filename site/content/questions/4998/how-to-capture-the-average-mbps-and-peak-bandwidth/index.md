+++
type = "question"
title = "How to capture the average mbps and peak bandwidth?"
description = '''How can you find the average mbps and peak bandwidth while capturing video?'''
date = "2011-07-12T13:36:00Z"
lastmod = "2011-07-13T02:25:00Z"
weight = 4998
keywords = [ "average", "bandwidth", "mbps" ]
aliases = [ "/questions/4998" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture the average mbps and peak bandwidth?](/questions/4998/how-to-capture-the-average-mbps-and-peak-bandwidth)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4998-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4998-score" class="post-score" title="current number of votes">0</div><span id="post-4998-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can you find the average mbps and peak bandwidth while capturing video?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-average" rel="tag" title="see questions tagged &#39;average&#39;">average</span> <span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-mbps" rel="tag" title="see questions tagged &#39;mbps&#39;">mbps</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jul '11, 13:36</strong></p><img src="https://secure.gravatar.com/avatar/cdac59ba08bf462169b37c45f1a52b4a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="geegee&#39;s gravatar image" /><p><span>geegee</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="geegee has no accepted answers">0%</span></p></div></div><div id="comments-container-4998" class="comments-container"></div><div id="comment-tools-4998" class="comment-tools"></div><div class="clear"></div><div id="comment-4998-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5001"></span>

<div id="answer-container-5001" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5001-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5001-score" class="post-score" title="current number of votes">0</div><span id="post-5001-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can use some of the statistics menu options for that, for example if you filter out your video stream you can then select the summary statistics to see the average mbps for that stream. Peak could be determined from the I/O graph (also in the statistics menu); just set the Y axis to bit/interval and set the interval to seconds. Then you can look at the graph, find the highest peak and that's your peak bandwidth.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Jul '11, 15:00</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-5001" class="comments-container"><span id="5011"></span><div id="comment-5011" class="comment"><div id="post-5011-score" class="comment-score"></div><div class="comment-text"><p>Thank you, very much Jasper</p><p>[converted to a comment]</p></div><div id="comment-5011-info" class="comment-info"><span class="comment-age">(13 Jul '11, 02:25)</span> <span class="comment-user userinfo">geegee</span></div></div></div><div id="comment-tools-5001" class="comment-tools"></div><div class="clear"></div><div id="comment-5001-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

