+++
type = "question"
title = "Can I trace traffic on a laptop using an integral 3G SIM"
description = '''We have a number of laptops out in the field using 3G sims for mobile broadband. We have seena massive increase in data useage and wanted to use wireshark to try and identify which applications might be responsible for the increase. However when I loaded it on 1 of the laptops the Mobile broadband c...'''
date = "2017-07-26T01:15:00Z"
lastmod = "2017-07-26T09:39:00Z"
weight = 63110
keywords = [ "mobile", "3g", "sim" ]
aliases = [ "/questions/63110" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can I trace traffic on a laptop using an integral 3G SIM](/questions/63110/can-i-trace-traffic-on-a-laptop-using-an-integral-3g-sim)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63110-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63110-score" class="post-score" title="current number of votes">0</div><span id="post-63110-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We have a number of laptops out in the field using 3G sims for mobile broadband. We have seena massive increase in data useage and wanted to use wireshark to try and identify which applications might be responsible for the increase. However when I loaded it on 1 of the laptops the Mobile broadband connection doesn't show up for tracing, only bluetooth, LAN and WAN. Is there a way for me to achive what I need?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span> <span class="post-tag tag-link-3g" rel="tag" title="see questions tagged &#39;3g&#39;">3g</span> <span class="post-tag tag-link-sim" rel="tag" title="see questions tagged &#39;sim&#39;">sim</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '17, 01:15</strong></p><img src="https://secure.gravatar.com/avatar/4729417e9a28d07d60edbd59611f6d03?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="brittas&#39;s gravatar image" /><p><span>brittas</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="brittas has no accepted answers">0%</span></p></div></div><div id="comments-container-63110" class="comments-container"><span id="63111"></span><div id="comment-63111" class="comment"><div id="post-63111-score" class="comment-score"></div><div class="comment-text"><p>What is your OS and which version and what is your Wireshark version?</p></div><div id="comment-63111-info" class="comment-info"><span class="comment-age">(26 Jul '17, 01:36)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="63114"></span><div id="comment-63114" class="comment"><div id="post-63114-score" class="comment-score"></div><div class="comment-text"><p>Also what capture library are you using, WinPcap or npcap (or even something else)?</p></div><div id="comment-63114-info" class="comment-info"><span class="comment-age">(26 Jul '17, 02:39)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="63139"></span><div id="comment-63139" class="comment"><div id="post-63139-score" class="comment-score"></div><div class="comment-text"><p>Windows 7 SP1 WinPcap I think. I just did the free download and hit all the defaults on the install. I'm not really technical and I think maybe Wireshark isn't the tool for me. I've just now looking at Glasswire which might be a bit better for my limited technical knowledge. Thanks for the replies though</p></div><div id="comment-63139-info" class="comment-info"><span class="comment-age">(26 Jul '17, 09:39)</span> <span class="comment-user userinfo">brittas</span></div></div></div><div id="comment-tools-63110" class="comment-tools"></div><div class="clear"></div><div id="comment-63110-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

