+++
type = "question"
title = "AMR not recognized"
description = '''When opening one and the same pcap file the RTP packets are shown as AMR with wireshark 1.6, whereas wireshark 1.10.6 shows only RTP. Settings for RTP and AMR are the same in both versions. How can I view AMR in version 1.10.6?'''
date = "2014-03-24T04:56:00Z"
lastmod = "2014-03-24T07:35:00Z"
weight = 31110
keywords = [ "sip", "amr", "rtp" ]
aliases = [ "/questions/31110" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [AMR not recognized](/questions/31110/amr-not-recognized)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31110-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31110-score" class="post-score" title="current number of votes">0</div><span id="post-31110-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When opening one and the same pcap file the RTP packets are shown as AMR with wireshark 1.6, whereas wireshark 1.10.6 shows only RTP. Settings for RTP and AMR are the same in both versions. How can I view AMR in version 1.10.6?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-amr" rel="tag" title="see questions tagged &#39;amr&#39;">amr</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Mar '14, 04:56</strong></p><img src="https://secure.gravatar.com/avatar/fa1c99a0a2ee5c9fe1afcc49d148b30b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="systest&#39;s gravatar image" /><p><span>systest</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="systest has no accepted answers">0%</span></p></div></div><div id="comments-container-31110" class="comments-container"><span id="31111"></span><div id="comment-31111" class="comment"><div id="post-31111-score" class="comment-score"></div><div class="comment-text"><p>Can you post the capture file somewhere we can see it - like cloudshark.org?</p></div><div id="comment-31111-info" class="comment-info"><span class="comment-age">(24 Mar '14, 07:35)</span> <span class="comment-user userinfo">Hadriel</span></div></div></div><div id="comment-tools-31110" class="comment-tools"></div><div class="clear"></div><div id="comment-31110-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

