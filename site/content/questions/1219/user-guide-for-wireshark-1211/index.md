+++
type = "question"
title = "User Guide for Wireshark 1.2.11"
description = '''I am looking for a copy of the user guide for 1.2 or better 1.2.11. I am a student and this is the copy we are using in class. Any help would be appreciated.'''
date = "2010-12-02T21:28:00Z"
lastmod = "2010-12-03T11:38:00Z"
weight = 1219
keywords = [ "guide" ]
aliases = [ "/questions/1219" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [User Guide for Wireshark 1.2.11](/questions/1219/user-guide-for-wireshark-1211)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1219-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1219-score" class="post-score" title="current number of votes">0</div><span id="post-1219-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking for a copy of the user guide for 1.2 or better 1.2.11. I am a student and this is the copy we are using in class. Any help would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-guide" rel="tag" title="see questions tagged &#39;guide&#39;">guide</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Dec '10, 21:28</strong></p><img src="https://secure.gravatar.com/avatar/aa68250230b84a40c3b96f75b945b274?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dr_duff&#39;s gravatar image" /><p><span>dr_duff</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dr_duff has no accepted answers">0%</span></p></div></div><div id="comments-container-1219" class="comments-container"></div><div id="comment-tools-1219" class="comment-tools"></div><div class="clear"></div><div id="comment-1219-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1232"></span>

<div id="answer-container-1232" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1232-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1232-score" class="post-score" title="current number of votes">0</div><span id="post-1232-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you are working on a Windows PC, then by default, the user guide is located in "C:Program FilesWiresharkuser-guide.chm". If it's not there, you can get it by following a few steps:</p><ol><li>Visit <a href="http://anonsvn.wireshark.org/viewvc/releases/">http://anonsvn.wireshark.org/viewvc/releases/</a> to find the subversion revision number and date associated with the release you are interested in. In your case, 1.2.11 corresponded to subversion revision 34021 and was released 3 months ago on August 30, 2010. (REF: <a href="http://www.wireshark.org/news/20100830.html">http://www.wireshark.org/news/20100830.html</a>)</li><li>Visit <a href="http://anonsvn.wireshark.org/wireshark-win32-libs/tags/">http://anonsvn.wireshark.org/wireshark-win32-libs/tags/</a> and look for the tag date that is closest to, but not after, the release date. In your case, this is tag 2010-08-27-1.2/</li><li>Navigate to the packages/ directory and download the user-guide-#####.zip file you see there. In your case, this is user-guide-31672.zip. Don't worry that the subversion revision number attached to the file doesn't match the subversion revision number associated with the file. That simply means that the user guide probably wasn't updated since that revision.</li><li>After downloading the file and extracting it, you will have the user-guide.chm file corresponding to your desired release.</li></ol><p>I don't know where or if the pdf renditions of the user guide are archived.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Dec '10, 11:38</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-1232" class="comments-container"></div><div id="comment-tools-1232" class="comment-tools"></div><div class="clear"></div><div id="comment-1232-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

