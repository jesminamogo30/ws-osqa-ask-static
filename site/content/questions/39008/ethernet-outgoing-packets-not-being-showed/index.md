+++
type = "question"
title = "ethernet Outgoing packets not being showed."
description = '''Please help. Wireshark do not show outgoing packets, only incoming. I have installed latest wireshark and winpcap. PC is Windows 8.0.  I already tried unistall winpcap, reinstall ethernet driver, install wireshark 32bits and 64bits. Still the same. Any suggestion will be welcome. Thanks. Horace.'''
date = "2015-01-09T11:50:00Z"
lastmod = "2015-01-10T04:27:00Z"
weight = 39008
keywords = [ "packets", "incoming", "winpcap" ]
aliases = [ "/questions/39008" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ethernet Outgoing packets not being showed.](/questions/39008/ethernet-outgoing-packets-not-being-showed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39008-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39008-score" class="post-score" title="current number of votes">0</div><span id="post-39008-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please help. Wireshark do not show outgoing packets, only incoming. I have installed latest wireshark and winpcap. PC is Windows 8.0.</p><p>I already tried unistall winpcap, reinstall ethernet driver, install wireshark 32bits and 64bits.</p><p>Still the same. Any suggestion will be welcome.</p><p>Thanks.</p><p>Horace.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-incoming" rel="tag" title="see questions tagged &#39;incoming&#39;">incoming</span> <span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jan '15, 11:50</strong></p><img src="https://secure.gravatar.com/avatar/41f8df26a3f1e9182f6155818781c5be?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="atmoss&#39;s gravatar image" /><p><span>atmoss</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="atmoss has no accepted answers">0%</span></p></div></div><div id="comments-container-39008" class="comments-container"></div><div id="comment-tools-39008" class="comment-tools"></div><div class="clear"></div><div id="comment-39008-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39016"></span>

<div id="answer-container-39016" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39016-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39016-score" class="post-score" title="current number of votes">0</div><span id="post-39016-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please read my answer to a similar question</p><blockquote><p><a href="https://ask.wireshark.org/questions/28909/no-outgoing-packets">https://ask.wireshark.org/questions/28909/no-outgoing-packets</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jan '15, 04:27</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-39016" class="comments-container"></div><div id="comment-tools-39016" class="comment-tools"></div><div class="clear"></div><div id="comment-39016-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

