+++
type = "question"
title = "What free 3rd party apps are you using to summarize and analyze the Wireshark captures?"
description = '''What free 3rd party apps are you using to summarize and analyze the Wireshark captures?  Something that can help extract and indentify the data. Something that can quickly pull out the java scripts, pictures, exe’s etc.  Thanks for the help in advance. '''
date = "2010-10-05T11:40:00Z"
lastmod = "2010-10-05T14:22:00Z"
weight = 418
keywords = [ "tools" ]
aliases = [ "/questions/418" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [What free 3rd party apps are you using to summarize and analyze the Wireshark captures?](/questions/418/what-free-3rd-party-apps-are-you-using-to-summarize-and-analyze-the-wireshark-captures)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-418-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-418-score" class="post-score" title="current number of votes">0</div><span id="post-418-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What free 3rd party apps are you using to summarize and analyze the Wireshark captures?</p><p>Something that can help extract and indentify the data. Something that can quickly pull out the java scripts, pictures, exe’s etc.</p><p>Thanks for the help in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tools" rel="tag" title="see questions tagged &#39;tools&#39;">tools</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Oct '10, 11:40</strong></p><img src="https://secure.gravatar.com/avatar/1307e9c0b8d6dde0fdadb1cc05eb75e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Onebusytech&#39;s gravatar image" /><p><span>Onebusytech</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Onebusytech has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Oct '10, 11:41</strong> </span></p></div></div><div id="comments-container-418" class="comments-container"><span id="424"></span><div id="comment-424" class="comment"><div id="post-424-score" class="comment-score"></div><div class="comment-text"><p>Thanks for the two sugestions.</p></div><div id="comment-424-info" class="comment-info"><span class="comment-age">(05 Oct '10, 14:15)</span> <span class="comment-user userinfo">Onebusytech</span></div></div></div><div id="comment-tools-418" class="comment-tools"></div><div class="clear"></div><div id="comment-418-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="419"></span>

<div id="answer-container-419" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-419-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-419-score" class="post-score" title="current number of votes">0</div><span id="post-419-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you are looking at pulling these out of an HTML stream, you can do that in Wireshark.</p><p>File &gt; Export &gt; Objects &gt; HTTP.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Oct '10, 11:57</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-419" class="comments-container"><span id="425"></span><div id="comment-425" class="comment"><div id="post-425-score" class="comment-score"></div><div class="comment-text"><p>Thanks, I tried that and it is a very nice summary. Had not seen that before. Thanks</p></div><div id="comment-425-info" class="comment-info"><span class="comment-age">(05 Oct '10, 14:22)</span> <span class="comment-user userinfo">Onebusytech</span></div></div></div><div id="comment-tools-419" class="comment-tools"></div><div class="clear"></div><div id="comment-419-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="420"></span>

<div id="answer-container-420" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-420-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-420-score" class="post-score" title="current number of votes">0</div><span id="post-420-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Chaosreader works well</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Oct '10, 12:28</strong></p><img src="https://secure.gravatar.com/avatar/5ae7383b08bf286bf440f86b6a5e5458?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KEM&#39;s gravatar image" /><p><span>KEM</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KEM has no accepted answers">0%</span></p></div></div><div id="comments-container-420" class="comments-container"></div><div id="comment-tools-420" class="comment-tools"></div><div class="clear"></div><div id="comment-420-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="421"></span>

<div id="answer-container-421" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-421-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-421-score" class="post-score" title="current number of votes">0</div><span id="post-421-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Network Miner. It has its quirks but it has some nice features as well. <a href="http://networkminer.sourceforge.net/">link text</a> <embed src="http://sourceforge.net/dbimage.php?id=218429" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Oct '10, 13:47</strong></p><img src="https://secure.gravatar.com/avatar/e7d1d3994349a9ea0554a6430dbe2ec8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="naskop&#39;s gravatar image" /><p><span>naskop</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="naskop has no accepted answers">0%</span></p></img></div></div><div id="comments-container-421" class="comments-container"></div><div id="comment-tools-421" class="comment-tools"></div><div class="clear"></div><div id="comment-421-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

