+++
type = "question"
title = "What blocks the addition of an interface?"
description = '''I can not add an interface by IP address. would a baraccuda web filter affect this?'''
date = "2012-12-07T12:15:00Z"
lastmod = "2012-12-07T13:33:00Z"
weight = 16703
keywords = [ "interface", "addition", "problem" ]
aliases = [ "/questions/16703" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What blocks the addition of an interface?](/questions/16703/what-blocks-the-addition-of-an-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16703-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16703-score" class="post-score" title="current number of votes">0</div><span id="post-16703-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can not add an interface by IP address. would a baraccuda web filter affect this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-addition" rel="tag" title="see questions tagged &#39;addition&#39;">addition</span> <span class="post-tag tag-link-problem" rel="tag" title="see questions tagged &#39;problem&#39;">problem</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Dec '12, 12:15</strong></p><img src="https://secure.gravatar.com/avatar/316d026ef774fd19e972ffd4cb8a337e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dbar&#39;s gravatar image" /><p><span>dbar</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dbar has no accepted answers">0%</span></p></div></div><div id="comments-container-16703" class="comments-container"><span id="16709"></span><div id="comment-16709" class="comment"><div id="post-16709-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I can not add an interface by IP address.</p></blockquote><ul><li>what does that mean?</li><li>what did you do to "add an interface"?</li><li>what is your OS version</li><li>what is your Wireshark version?</li></ul></div><div id="comment-16709-info" class="comment-info"><span class="comment-age">(07 Dec '12, 12:38)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="16710"></span><div id="comment-16710" class="comment"><div id="post-16710-score" class="comment-score"></div><div class="comment-text"><p>Can't get list of interfaces: Is the server properly installed on 192.168.100.224? connect() failed: No connection could be made because the target machine actively refused it. (code 10061)</p><p>This is what I get when I attempt to add a server or other device to capture packets.</p></div><div id="comment-16710-info" class="comment-info"><span class="comment-age">(07 Dec '12, 12:53)</span> <span class="comment-user userinfo">dbar</span></div></div></div><div id="comment-tools-16703" class="comment-tools"></div><div class="clear"></div><div id="comment-16703-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="16711"></span>

<div id="answer-container-16711" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16711-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16711-score" class="post-score" title="current number of votes">0</div><span id="post-16711-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Ah, you are talking about a remote capture setup. Did you start rpcapd on the remote machine?</p><blockquote><p><code>http://www.winpcap.org/docs/docs_40_2/html/group__remote.html</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Dec '12, 13:33</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-16711" class="comments-container"></div><div id="comment-tools-16711" class="comment-tools"></div><div class="clear"></div><div id="comment-16711-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

