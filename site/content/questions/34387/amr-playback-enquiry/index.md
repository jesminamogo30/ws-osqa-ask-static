+++
type = "question"
title = "AMR playback enquiry"
description = '''Hi, I am new to using wireshark. Now I have a pcap file containing SIP and RTP (AMR). I would like to playback the RTP to check whether the connecting tone is play as designed. But I found that it is fail to play the extracted .raw file obtained from wireshark. I already used the AVS Audio Converter...'''
date = "2014-07-03T08:08:00Z"
lastmod = "2014-07-08T08:46:00Z"
weight = 34387
keywords = [ "amr", "playback", "rtp" ]
aliases = [ "/questions/34387" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [AMR playback enquiry](/questions/34387/amr-playback-enquiry)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34387-score" class="post-score" title="current number of votes">0</div><span id="post-34387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am new to using wireshark. Now I have a pcap file containing SIP and RTP (AMR). I would like to playback the RTP to check whether the connecting tone is play as designed. But I found that it is fail to play the extracted .raw file obtained from wireshark. I already used the AVS Audio Converter to change the format of the file from .raw to .amr and use VLC player to play it. Unfortunately, it still fails. Is there anyone who can provide helping hands?? :(</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-amr" rel="tag" title="see questions tagged &#39;amr&#39;">amr</span> <span class="post-tag tag-link-playback" rel="tag" title="see questions tagged &#39;playback&#39;">playback</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '14, 08:08</strong></p><img src="https://secure.gravatar.com/avatar/ff5c714354f034556a5034fe39aba17f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="trillionsun&#39;s gravatar image" /><p><span>trillionsun</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="trillionsun has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Jul '14, 08:09</strong> </span></p></div></div><div id="comments-container-34387" class="comments-container"></div><div id="comment-tools-34387" class="comment-tools"></div><div class="clear"></div><div id="comment-34387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34467"></span>

<div id="answer-container-34467" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34467-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34467-score" class="post-score" title="current number of votes">0</div><span id="post-34467-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See my answer to a similar question:</p><blockquote><p><a href="http://ask.wireshark.org/questions/26222/how-to-play-rtp-payload-which-is-in-amr-codec">http://ask.wireshark.org/questions/26222/how-to-play-rtp-payload-which-is-in-amr-codec</a></p></blockquote><p>Maybe that helps.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jul '14, 08:46</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-34467" class="comments-container"></div><div id="comment-tools-34467" class="comment-tools"></div><div class="clear"></div><div id="comment-34467-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

