+++
type = "question"
title = "Couldn&#x27;t find debug console option in EDIT|Preference"
description = '''I want to debug my plugin but couldn&#x27;t find debug console option in EDIT|Preference'''
date = "2012-11-06T22:28:00Z"
lastmod = "2012-11-08T05:32:00Z"
weight = 15606
keywords = [ "debug_console" ]
aliases = [ "/questions/15606" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Couldn't find debug console option in EDIT|Preference](/questions/15606/couldnt-find-debug-console-option-in-editpreference)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15606-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15606-score" class="post-score" title="current number of votes">0</div><span id="post-15606-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to debug my plugin but couldn't find debug console option in EDIT|Preference</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-debug_console" rel="tag" title="see questions tagged &#39;debug_console&#39;">debug_console</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '12, 22:28</strong></p><img src="https://secure.gravatar.com/avatar/b0ed262c234b0aa9fae2e5b2d51b14c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Akhil&#39;s gravatar image" /><p><span>Akhil</span><br />
<span class="score" title="53 reputation points">53</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="28 badges"><span class="silver">●</span><span class="badgecount">28</span></span><span title="31 badges"><span class="bronze">●</span><span class="badgecount">31</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Akhil has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Nov '12, 00:30</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-15606" class="comments-container"></div><div id="comment-tools-15606" class="comment-tools"></div><div class="clear"></div><div id="comment-15606-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="15625"></span>

<div id="answer-container-15625" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15625-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15625-score" class="post-score" title="current number of votes">0</div><span id="post-15625-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have said the same in the other question of yours. Go to "Edit-&gt;Preference". In the option "Open a console window", change it from "Never" to "Always(debugging)" and click "Apply"</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Nov '12, 02:56</strong></p><img src="https://secure.gravatar.com/avatar/46196bc495ce51058590c4e4ae334d22?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SidR&#39;s gravatar image" /><p><span>SidR</span><br />
<span class="score" title="245 reputation points">245</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="22 badges"><span class="bronze">●</span><span class="badgecount">22</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SidR has 3 accepted answers">30%</span></p></div></div><div id="comments-container-15625" class="comments-container"><span id="15677"></span><div id="comment-15677" class="comment"><div id="post-15677-score" class="comment-score"></div><div class="comment-text"><p>In ubuntu option "Open a console window" is not present</p></div><div id="comment-15677-info" class="comment-info"><span class="comment-age">(07 Nov '12, 22:51)</span> <span class="comment-user userinfo">Akhil</span></div></div></div><div id="comment-tools-15625" class="comment-tools"></div><div class="clear"></div><div id="comment-15625-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="15631"></span>

<div id="answer-container-15631" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15631-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15631-score" class="post-score" title="current number of votes">0</div><span id="post-15631-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Linux doesn't need this option, just launch Wireshark from a terminal window.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Nov '12, 03:50</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-15631" class="comments-container"><span id="15688"></span><div id="comment-15688" class="comment"><div id="post-15688-score" class="comment-score"></div><div class="comment-text"><p>I am launching wireshark from terminal. what should i do after that to start debugging??????</p></div><div id="comment-15688-info" class="comment-info"><span class="comment-age">(08 Nov '12, 00:51)</span> <span class="comment-user userinfo">Akhil</span></div></div><span id="15692"></span><div id="comment-15692" class="comment"><div id="post-15692-score" class="comment-score"></div><div class="comment-text"><p>As already answered in the following question:</p><blockquote><p><code>http://ask.wireshark.org/questions/15602/how-to-debug-my-plugin</code></p></blockquote><p>You need to add debug statements to your code to see some output in the debug console.</p><blockquote><p><code>http://wiki.wireshark.org/Development/Tips</code><br />
</p></blockquote><p>Please read the Developer Guide thoroughly.</p><p>BTW: Is it really necessary to add 5-6 question marks (?????) to your questions? One should be sufficient to properly flag your sentence as a question ;-)</p></div><div id="comment-15692-info" class="comment-info"><span class="comment-age">(08 Nov '12, 01:15)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="15703"></span><div id="comment-15703" class="comment"><div id="post-15703-score" class="comment-score"></div><div class="comment-text"><p>I have added the "g_print " statemnet in my code. But the debug console is not opened.</p></div><div id="comment-15703-info" class="comment-info"><span class="comment-age">(08 Nov '12, 03:00)</span> <span class="comment-user userinfo">Akhil</span></div></div><span id="15704"></span><div id="comment-15704" class="comment"><div id="post-15704-score" class="comment-score"></div><div class="comment-text"><p>Did you start Wireshark from a console window? Are you sure the g_print statement is executed?</p></div><div id="comment-15704-info" class="comment-info"><span class="comment-age">(08 Nov '12, 04:10)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="15706"></span><div id="comment-15706" class="comment"><div id="post-15706-score" class="comment-score"></div><div class="comment-text"><p>Yes i started Wireshark from a console window. and the g_print statement is executed</p></div><div id="comment-15706-info" class="comment-info"><span class="comment-age">(08 Nov '12, 04:34)</span> <span class="comment-user userinfo">Akhil</span></div></div><span id="15707"></span><div id="comment-15707" class="comment not_top_scorer"><div id="post-15707-score" class="comment-score"></div><div class="comment-text"><p>well, I don't know what's wrong with your code or system, but if I add the following statement to the TCP dissector, I get a message on the console every time the dissector gets executed.</p><blockquote><p><code>g_print("[INFO]  Entering TCP dissector\n");</code></p></blockquote><p>Maybe you try it with a standard dissector first, to see if there is a general problem with your code and/or system. If that works, it should work within your dissector as well. If it does not work, your g_print code is not executed.</p><p>Regards<br />
Kurt</p></div><div id="comment-15707-info" class="comment-info"><span class="comment-age">(08 Nov '12, 05:32)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-15631" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-15631-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

