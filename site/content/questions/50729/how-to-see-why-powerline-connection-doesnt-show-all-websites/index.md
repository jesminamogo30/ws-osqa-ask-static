+++
type = "question"
title = "How to see why powerline connection doesn&#x27;t show all websites?"
description = '''Hi guys. I hope some of you can help me with my problem using a powerline connection. I have a LTE receiving unit (TurboBox) and a Vodafone EasyBox (router). When both are connected with a LAN cable, everything is fine. When I connect them with a powerline connection (one box usually stands in a dif...'''
date = "2016-03-05T07:33:00Z"
lastmod = "2016-03-05T09:07:00Z"
weight = 50729
keywords = [ "powerline", "lte" ]
aliases = [ "/questions/50729" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to see why powerline connection doesn't show all websites?](/questions/50729/how-to-see-why-powerline-connection-doesnt-show-all-websites)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50729-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50729-score" class="post-score" title="current number of votes">0</div><span id="post-50729-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys. I hope some of you can help me with my problem using a powerline connection.</p><p>I have a LTE receiving unit (TurboBox) and a Vodafone EasyBox (router). When both are connected with a LAN cable, everything is fine. When I connect them with a powerline connection (one box usually stands in a different room) only some websites work (google, facebook, wikipedia, wireshark homepage). I have already tried a lot of things, but I still dont't know what's the matter here. Vodafone support doesn't know a thing either (it's a very unusual problem). Funfact: Before a big network breakdown by Vodafone everything worked fine with(!) the powerline adapters.</p><p>So the questions is: How can I use wireshark to spot the error? Any ideas? First, it looked like a IPv6 issue, but the router doesn't even support that and I'm connected with IPv4. It's even possible to ping the malfunctioning websites etc. Plus I have a new router, so it's unlikely that it's a hardware issue.</p><p>It's really crazy because powerline adapters should only forward the signals ...</p><p>I hope someone has an idea.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-powerline" rel="tag" title="see questions tagged &#39;powerline&#39;">powerline</span> <span class="post-tag tag-link-lte" rel="tag" title="see questions tagged &#39;lte&#39;">lte</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '16, 07:33</strong></p><img src="https://secure.gravatar.com/avatar/a67a6476187bef19ac26ba564459d560?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pap&#39;s gravatar image" /><p><span>Pap</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pap has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Mar '16, 20:37</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-50729" class="comments-container"><span id="50730"></span><div id="comment-50730" class="comment"><div id="post-50730-score" class="comment-score"></div><div class="comment-text"><p>I would start from connecting the PC's wired Ethernet port instead of the EasyBox, with and without the powerline modems in the path, to find out whether the same sites are affected or not if the EasyBox is not part of the chain. Use of Wireshark would come next.</p></div><div id="comment-50730-info" class="comment-info"><span class="comment-age">(05 Mar '16, 09:07)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-50729" class="comment-tools"></div><div class="clear"></div><div id="comment-50729-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

