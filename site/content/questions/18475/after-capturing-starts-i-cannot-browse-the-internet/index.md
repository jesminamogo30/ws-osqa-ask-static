+++
type = "question"
title = "After capturing starts I cannot browse the internet"
description = '''Hello to everybody, I&#x27;m running wireshark on Ubuntu 12.04.2 and it looks really great. The only problem is that once I&#x27;ve started it and it&#x27;s capturing network traffic I&#x27;m no longer able to surf in the internet. The problem remains even after that I close the program. So I can close and start wiresh...'''
date = "2013-02-10T07:19:00Z"
lastmod = "2016-01-06T06:59:00Z"
weight = 18475
keywords = [ "internet", "stop", "network", "wireshark" ]
aliases = [ "/questions/18475" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [After capturing starts I cannot browse the internet](/questions/18475/after-capturing-starts-i-cannot-browse-the-internet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18475-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18475-score" class="post-score" title="current number of votes">0</div><span id="post-18475-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello to everybody, I'm running wireshark on Ubuntu 12.04.2 and it looks really great. The only problem is that once I've started it and it's capturing network traffic I'm no longer able to surf in the internet. The problem remains even after that I close the program. So I can close and start wireshark as many times as I want, it always works, but after the first time I start it I cannot surf the web anymore... I also tried to do some "ping google.com" or "ping &lt;ip address="" of="" google.com=""&gt;" and it also does not work. Do you know what could be the reason?</p><p>Thank you very very much, and I hope this is not a super stupid question that every noob asks (I looked for it in the database but I couldn't find it).</p><p>Best, Alessandro</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span> <span class="post-tag tag-link-stop" rel="tag" title="see questions tagged &#39;stop&#39;">stop</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '13, 07:19</strong></p><img src="https://secure.gravatar.com/avatar/201d2bf5085202a0f58cde423c4a743c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alessandro%20Mammana&#39;s gravatar image" /><p><span>Alessandro M...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alessandro Mammana has no accepted answers">0%</span></p></div></div><div id="comments-container-18475" class="comments-container"><span id="18477"></span><div id="comment-18477" class="comment"><div id="post-18477-score" class="comment-score"></div><div class="comment-text"><p>Interface type, brand and model?</p></div><div id="comment-18477-info" class="comment-info"><span class="comment-age">(10 Feb '13, 07:51)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="18479"></span><div id="comment-18479" class="comment"><div id="post-18479-score" class="comment-score"></div><div class="comment-text"><p>it's the wireless interface, the model should be Centrino Wireless-N 1000 and the brand Intel. Thank you so much, Alessandro</p></div><div id="comment-18479-info" class="comment-info"><span class="comment-age">(10 Feb '13, 13:36)</span> <span class="comment-user userinfo">Alessandro M...</span></div></div><span id="18510"></span><div id="comment-18510" class="comment"><div id="post-18510-score" class="comment-score"></div><div class="comment-text"><p>Does it have anything to do with the fact that I'm in promiscuous mode? Is it supposed to happen?</p></div><div id="comment-18510-info" class="comment-info"><span class="comment-age">(11 Feb '13, 13:05)</span> <span class="comment-user userinfo">Alessandro M...</span></div></div><span id="18513"></span><div id="comment-18513" class="comment"><div id="post-18513-score" class="comment-score"></div><div class="comment-text"><p>I don't know, monitor mode and promiscuous mode can do funny things with wireless.</p></div><div id="comment-18513-info" class="comment-info"><span class="comment-age">(11 Feb '13, 14:39)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="48909"></span><div id="comment-48909" class="comment"><div id="post-48909-score" class="comment-score"></div><div class="comment-text"><p>I know this question is 3 years old... But since it's never been answerred properly:</p><p>I have same problem, but I am using WIRED network. All internet is disabled until I reboot. SO Wifi doesn't seem to be the culprit. Adapter: Motherboard onboard (Asus P8Z77V-Lx)</p></div><div id="comment-48909-info" class="comment-info"><span class="comment-age">(06 Jan '16, 06:58)</span> <span class="comment-user userinfo">DonDino</span></div></div><span id="48910"></span><div id="comment-48910" class="comment not_top_scorer"><div id="post-48910-score" class="comment-score"></div><div class="comment-text"><p><span></span><span>@DonDino</span>, what OS and what Wireshark version? Copy and paste the contents of the Wireshark Help -&gt; About Wireshark box because there's other info there that is useful.</p></div><div id="comment-48910-info" class="comment-info"><span class="comment-age">(06 Jan '16, 06:59)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-18475" class="comment-tools"><span class="comments-showing"> showing 5 of 6 </span> <a href="#" class="show-all-comments-link">show 1 more comments</a></div><div class="clear"></div><div id="comment-18475-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18516"></span>

<div id="answer-container-18516" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18516-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18516-score" class="post-score" title="current number of votes">0</div><span id="post-18516-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's probably because wireless adapters can't send and receive at the same time, they're kinda "half duplex". So if your card is in monitor and/or promiscuous mode it is possible that it decides to only receive to avoid missing anything while sending on its own. That would imply that you can't send any requests to the internet, and thus not get anything back.</p><p>Usually, if you need to use a wireless adapter to communicate and you also need to capture wireless at the same time, you need a second adapter.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Feb '13, 16:06</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-18516" class="comments-container"><span id="18561"></span><div id="comment-18561" class="comment"><div id="post-18561-score" class="comment-score"></div><div class="comment-text"><p>Ok, but this does not explain why after I close wireshark the connection still does not work! I think that wireshark should restore the settings it found before starting capturing network traffic. As a temporary solution, do you know if there is a linux command that I could try to restore the settings of the wireless card? Thank you very very much.</p></div><div id="comment-18561-info" class="comment-info"><span class="comment-age">(12 Feb '13, 11:22)</span> <span class="comment-user userinfo">Alessandro M...</span></div></div><span id="18562"></span><div id="comment-18562" class="comment"><div id="post-18562-score" class="comment-score"></div><div class="comment-text"><p>Do you enable promiscuous or monitor mode?</p></div><div id="comment-18562-info" class="comment-info"><span class="comment-age">(12 Feb '13, 11:29)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-18516" class="comment-tools"></div><div class="clear"></div><div id="comment-18516-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

