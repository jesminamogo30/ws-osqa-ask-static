+++
type = "question"
title = "does tshark returns iops value"
description = '''I would like to know if tshark could return iops result as well. Just like avg latency.'''
date = "2011-09-22T13:58:00Z"
lastmod = "2011-09-22T13:58:00Z"
weight = 6494
keywords = [ "iops", "tshark", "option" ]
aliases = [ "/questions/6494" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [does tshark returns iops value](/questions/6494/does-tshark-returns-iops-value)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6494-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6494-score" class="post-score" title="current number of votes">0</div><span id="post-6494-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to know if tshark could return iops result as well. Just like avg latency.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iops" rel="tag" title="see questions tagged &#39;iops&#39;">iops</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-option" rel="tag" title="see questions tagged &#39;option&#39;">option</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '11, 13:58</strong></p><img src="https://secure.gravatar.com/avatar/009622f35eab24cfbde3547b04a5bbea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="asif&#39;s gravatar image" /><p><span>asif</span><br />
<span class="score" title="1 reputation points">1</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="asif has no accepted answers">0%</span></p></div></div><div id="comments-container-6494" class="comments-container"></div><div id="comment-tools-6494" class="comment-tools"></div><div class="clear"></div><div id="comment-6494-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

