+++
type = "question"
title = "&quot;tpacketcapture&quot; does not capture DNS"
description = '''I captured packets using &quot;tpacketcapture&quot; in my mobile, for both wifi and 3G network.Unlike wifi, for 3G traffic, dns packets are not capturing. Please do clarify the query?'''
date = "2015-11-03T23:47:00Z"
lastmod = "2015-11-04T01:35:00Z"
weight = 47211
keywords = [ "not", "traffic", "displaying", "dns" ]
aliases = [ "/questions/47211" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# ["tpacketcapture" does not capture DNS](/questions/47211/tpacketcapture-does-not-capture-dns)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47211-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47211-score" class="post-score" title="current number of votes">0</div><span id="post-47211-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I captured packets using "tpacketcapture" in my mobile, for both wifi and 3G network.Unlike wifi, for 3G traffic, dns packets are not capturing. Please do clarify the query?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-displaying" rel="tag" title="see questions tagged &#39;displaying&#39;">displaying</span> <span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Nov '15, 23:47</strong></p><img src="https://secure.gravatar.com/avatar/478696dd2ced1d5334d6610b83b1f6d6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shijina&#39;s gravatar image" /><p><span>shijina</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shijina has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Nov '15, 02:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span></p></div></div><div id="comments-container-47211" class="comments-container"><span id="47216"></span><div id="comment-47216" class="comment"><div id="post-47216-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I captured packets using "tpacketcapture" in my mobile, for both wifi and 3G network. Unlike wifi, for 3G traffic, dns packets are not capturing. Please do clarify the query?</p></blockquote><p>Sounds like a perfect question for the vendor support of <a href="https://play.google.com/store/apps/details?id=jp.co.taosoftware.android.packetcapture&amp;hl=de">tpacketcapture</a>!</p></div><div id="comment-47216-info" class="comment-info"><span class="comment-age">(04 Nov '15, 01:35)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-47211" class="comment-tools"></div><div class="clear"></div><div id="comment-47211-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

