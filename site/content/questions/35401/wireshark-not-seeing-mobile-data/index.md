+++
type = "question"
title = "Wireshark not seeing mobile data."
description = '''My iPhone is on my network. I can see the other pc traffic, but nothing from the iPhone. The iPhone is connected to the wireless network and working fine. What can I do? Thanks'''
date = "2014-08-11T04:59:00Z"
lastmod = "2014-08-12T13:01:00Z"
weight = 35401
keywords = [ "mobile", "traffic", "iphone" ]
aliases = [ "/questions/35401" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not seeing mobile data.](/questions/35401/wireshark-not-seeing-mobile-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35401-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35401-score" class="post-score" title="current number of votes">0</div><span id="post-35401-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My iPhone is on my network. I can see the other pc traffic, but nothing from the iPhone. The iPhone is connected to the wireless network and working fine. What can I do?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mobile" rel="tag" title="see questions tagged &#39;mobile&#39;">mobile</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Aug '14, 04:59</strong></p><img src="https://secure.gravatar.com/avatar/14e8ff606c6f737f476604034a184baa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="catcurio&#39;s gravatar image" /><p><span>catcurio</span><br />
<span class="score" title="1 reputation points">1</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="catcurio has no accepted answers">0%</span></p></div></div><div id="comments-container-35401" class="comments-container"><span id="35440"></span><div id="comment-35440" class="comment"><div id="post-35440-score" class="comment-score"></div><div class="comment-text"><p>if you could upload a packet capture with your iphone's IP, as well as where you are getting the capture from (pc, router, managed switch, etc) it would be easier to advise you. Also, what traffic are you trying to see? are you making HTTP requests or just connecting to the network? these details would help to direct you</p></div><div id="comment-35440-info" class="comment-info"><span class="comment-age">(12 Aug '14, 13:01)</span> <span class="comment-user userinfo">jmadsen</span></div></div></div><div id="comment-tools-35401" class="comment-tools"></div><div class="clear"></div><div id="comment-35401-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

