+++
type = "question"
title = "SAG (SBC)?"
description = '''What do the SAG letters at sag-lookup-on-redirect mean?'''
date = "2013-03-10T16:16:00Z"
lastmod = "2013-03-10T18:52:00Z"
weight = 19343
keywords = [ "sbc", "sip", "sag", "ip" ]
aliases = [ "/questions/19343" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [SAG (SBC)?](/questions/19343/sag-sbc)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19343-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19343-score" class="post-score" title="current number of votes">0</div><span id="post-19343-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What do the SAG letters at sag-lookup-on-redirect mean?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sbc" rel="tag" title="see questions tagged &#39;sbc&#39;">sbc</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-sag" rel="tag" title="see questions tagged &#39;sag&#39;">sag</span> <span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Mar '13, 16:16</strong></p><img src="https://secure.gravatar.com/avatar/f6f4f2924ab897a5f768a7f4b97dce0e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sharkie&#39;s gravatar image" /><p><span>sharkie</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sharkie has no accepted answers">0%</span></p></div></div><div id="comments-container-19343" class="comments-container"></div><div id="comment-tools-19343" class="comment-tools"></div><div class="clear"></div><div id="comment-19343-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="19344"></span>

<div id="answer-container-19344" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19344-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19344-score" class="post-score" title="current number of votes">0</div><span id="post-19344-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have the answer. It's Session Agent Group.</p><p><a href="http://www.markholloway.com/blog/?p=1684">http://www.markholloway.com/blog/?p=1684</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Mar '13, 16:25</strong></p><img src="https://secure.gravatar.com/avatar/f6f4f2924ab897a5f768a7f4b97dce0e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sharkie&#39;s gravatar image" /><p><span>sharkie</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sharkie has no accepted answers">0%</span></p></div></div><div id="comments-container-19344" class="comments-container"></div><div id="comment-tools-19344" class="comment-tools"></div><div class="clear"></div><div id="comment-19344-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="19345"></span>

<div id="answer-container-19345" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19345-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19345-score" class="post-score" title="current number of votes">0</div><span id="post-19345-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Umm... that's not really a Wireshark question. :) Probably more appropriate on <a href="http://community.acmepacket.com/">community.acmepacket.com</a>. Did you see this in wireshark from a capture file somehow? I don't think even Acme's log2cap tool will put it in a pcap file.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Mar '13, 18:52</strong></p><img src="https://secure.gravatar.com/avatar/d02f20c18a7742ec73a666f1974bf6dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hadriel&#39;s gravatar image" /><p><span>Hadriel</span><br />
<span class="score" title="2652 reputation points"><span>2.7k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="39 badges"><span class="bronze">●</span><span class="badgecount">39</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hadriel has 30 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Mar '13, 18:53</strong> </span></p></div></div><div id="comments-container-19345" class="comments-container"></div><div id="comment-tools-19345" class="comment-tools"></div><div class="clear"></div><div id="comment-19345-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

