+++
type = "question"
title = "[closed] How to analyze packet drops in wireshark ?"
description = '''I would like to analze the packet drops in wireshark which means that what packets are dropped and on what protocol it got dropped and what was the reason why it was dropped'''
date = "2016-11-09T17:53:00Z"
lastmod = "2016-11-10T06:17:00Z"
weight = 57234
keywords = [ "tag" ]
aliases = [ "/questions/57234" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to analyze packet drops in wireshark ?](/questions/57234/how-to-analyze-packet-drops-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57234-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57234-score" class="post-score" title="current number of votes">0</div><span id="post-57234-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to analze the packet drops in wireshark which means that what packets are dropped and on what protocol it got dropped and what was the reason why it was dropped</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tag" rel="tag" title="see questions tagged &#39;tag&#39;">tag</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Nov '16, 17:53</strong></p><img src="https://secure.gravatar.com/avatar/7ec71c5d609163ae8d5ba8028ff71b1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sameetha&#39;s gravatar image" /><p><span>sameetha</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sameetha has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>10 Nov '16, 10:46</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-57234" class="comments-container"><span id="57237"></span><div id="comment-57237" class="comment"><div id="post-57237-score" class="comment-score"></div><div class="comment-text"><p>You mean trying to analyse a firewall??</p></div><div id="comment-57237-info" class="comment-info"><span class="comment-age">(09 Nov '16, 22:17)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="57265"></span><div id="comment-57265" class="comment"><div id="post-57265-score" class="comment-score"></div><div class="comment-text"><p>I have a scenario where there is a slowness in accessing an applicationfrom user's system after 6pm. So I just wanted to know what is happening that making the access to the application slow at that time. Is there any way that we can do it through wireshark</p></div><div id="comment-57265-info" class="comment-info"><span class="comment-age">(10 Nov '16, 06:17)</span> <span class="comment-user userinfo">sameetha</span></div></div></div><div id="comment-tools-57234" class="comment-tools"></div><div class="clear"></div><div id="comment-57234-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question https://ask.wireshark.org/questions/57269" by Jaap 10 Nov '16, 10:46

</div>

</div>

</div>

