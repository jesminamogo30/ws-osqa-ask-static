+++
type = "question"
title = "Starting problem"
description = '''When I try to run the application a small window is shown on the screen for a very short time and then happens nothing. I can see only the icon on the bottom bar (see here http://aijaa.com/000509434345). So what went wrong?'''
date = "2012-01-25T23:48:00Z"
lastmod = "2012-01-26T01:02:00Z"
weight = 8616
keywords = [ "starting" ]
aliases = [ "/questions/8616" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Starting problem](/questions/8616/starting-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8616-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8616-score" class="post-score" title="current number of votes">0</div><span id="post-8616-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I try to run the application a small window is shown on the screen for a very short time and then happens nothing. I can see only the icon on the bottom bar (see here http://aijaa.com/000509434345).</p><p>So what went wrong?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-starting" rel="tag" title="see questions tagged &#39;starting&#39;">starting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jan '12, 23:48</strong></p><img src="https://secure.gravatar.com/avatar/eac36907bbdce1a89bb417d19c82606a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scalpguy&#39;s gravatar image" /><p><span>scalpguy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scalpguy has no accepted answers">0%</span></p></div></div><div id="comments-container-8616" class="comments-container"></div><div id="comment-tools-8616" class="comment-tools"></div><div class="clear"></div><div id="comment-8616-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8619"></span>

<div id="answer-container-8619" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8619-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8619-score" class="post-score" title="current number of votes">0</div><span id="post-8619-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>For some reason the app was minimized and there was no maximize menu item available. I am using a Dexpot desktop/windows manager where I found the way to maximize it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jan '12, 01:02</strong></p><img src="https://secure.gravatar.com/avatar/eac36907bbdce1a89bb417d19c82606a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scalpguy&#39;s gravatar image" /><p><span>scalpguy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scalpguy has no accepted answers">0%</span></p></div></div><div id="comments-container-8619" class="comments-container"></div><div id="comment-tools-8619" class="comment-tools"></div><div class="clear"></div><div id="comment-8619-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

