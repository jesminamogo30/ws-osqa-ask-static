+++
type = "question"
title = "how do you do GTP dynamic IP capture filtering?"
description = '''Hi,  I am trying to do GTP capture filtering because of the amount of traffic on the server, last time I was told to use the following filtering: (vlan and ip6[64:4]=0x2a008a00 and ip6[68:4]=0x20000035 and ip6[72:4]=0x00000000 and ip6[76:4]=0x00000011) the problem is that the SIP messages&#x27; header ar...'''
date = "2016-06-22T09:13:00Z"
lastmod = "2016-06-22T09:13:00Z"
weight = 53613
keywords = [ "gtpv2" ]
aliases = [ "/questions/53613" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how do you do GTP dynamic IP capture filtering?](/questions/53613/how-do-you-do-gtp-dynamic-ip-capture-filtering)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53613-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53613-score" class="post-score" title="current number of votes">0</div><span id="post-53613-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am trying to do GTP capture filtering because of the amount of traffic on the server, last time I was told to use the following filtering:</p><p>(vlan and ip6[64:4]=0x2a008a00 and ip6[68:4]=0x20000035 and ip6[72:4]=0x00000000 and ip6[76:4]=0x00000011)</p><p>the problem is that the SIP messages' header are not always at the same position due to different messages, is there another way to capture them?</p><p>Thanks! Joseph</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtpv2" rel="tag" title="see questions tagged &#39;gtpv2&#39;">gtpv2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jun '16, 09:13</strong></p><img src="https://secure.gravatar.com/avatar/7035162f27c4b75a86d214a1769ac443?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joseph75074&#39;s gravatar image" /><p><span>joseph75074</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joseph75074 has no accepted answers">0%</span></p></div></div><div id="comments-container-53613" class="comments-container"></div><div id="comment-tools-53613" class="comment-tools"></div><div class="clear"></div><div id="comment-53613-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

