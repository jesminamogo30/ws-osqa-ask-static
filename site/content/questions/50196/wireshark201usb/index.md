+++
type = "question"
title = "wireshark更新到2.0.1以后，导致USB无法使用"
description = '''我今天更新了wireshark，但是我无法控制我的手，选择了USB抓包 这个组件的安装，然后我的电脑上所有USB都瘫痪了，已经没法使用了。 我的硬件是DELL的。系统是win8专业版。求助，您可以发送解决方案到我的邮箱woailiyushen#gmail.com 向您真诚的感谢'''
date = "2016-02-15T00:18:00Z"
lastmod = "2016-02-15T22:18:00Z"
weight = 50196
keywords = [ "badusb" ]
aliases = [ "/questions/50196" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark更新到2.0.1以后，导致USB无法使用](/questions/50196/wireshark201usb)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50196-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50196-score" class="post-score" title="current number of votes">0</div><span id="post-50196-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>我今天更新了wireshark，但是我无法控制我的手，选择了USB抓包 这个组件的安装，然后我的电脑上所有USB都瘫痪了，已经没法使用了。 我的硬件是DELL的。系统是win8专业版。求助，您可以发送解决方案到我的邮箱woailiyushen#gmail.com 向您真诚的感谢</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-badusb" rel="tag" title="see questions tagged &#39;badusb&#39;">badusb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '16, 00:18</strong></p><img src="https://secure.gravatar.com/avatar/f08de33782d28e48e1e1717073f7a3d3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alalmaliya&#39;s gravatar image" /><p><span>alalmaliya</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alalmaliya has one accepted answer">100%</span></p></div></div><div id="comments-container-50196" class="comments-container"><span id="50206"></span><div id="comment-50206" class="comment"><div id="post-50206-score" class="comment-score"></div><div class="comment-text"><p>According to Google Translate this is:</p><blockquote>I updated today wireshark, but I can not control my hands , choose USB capture this component is installed , then on my computer all USB paralyzed , it has been unable to use. My hardware is a DELL . Win8 system Professional. Help, you can send to my mailbox solutions woailiyushen # gmail.com to your sincere thanks</blockquote></div><div id="comment-50206-info" class="comment-info"><span class="comment-age">(15 Feb '16, 02:26)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-50196" class="comment-tools"></div><div class="clear"></div><div id="comment-50196-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50198"></span>

<div id="answer-container-50198" class="answer accepted-answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50198-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50198-score" class="post-score" title="current number of votes">0</div><span id="post-50198-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="alalmaliya has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>已经解决这个问题了 卸载这个usb抓包的组件，然后重启电脑。</p></div><div class="answer-controls post-controls"><div class="community-wiki">This answer is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Feb '16, 00:38</strong></p><img src="https://secure.gravatar.com/avatar/f08de33782d28e48e1e1717073f7a3d3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="alalmaliya&#39;s gravatar image" /><p><span>alalmaliya</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="alalmaliya has one accepted answer">100%</span></p></div></div><div id="comments-container-50198" class="comments-container"><span id="50205"></span><div id="comment-50205" class="comment"><div id="post-50205-score" class="comment-score"></div><div class="comment-text"><p>According to Google Translate this is:</p><blockquote>We have solved this problem uninstall the usb capture component and then restart the computer .</blockquote></div><div id="comment-50205-info" class="comment-info"><span class="comment-age">(15 Feb '16, 02:26)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="50208"></span><div id="comment-50208" class="comment"><div id="post-50208-score" class="comment-score">1</div><div class="comment-text"><p>Yes, it's a problem with USPpcap; see <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11766">Wireshark bug 11766</a>. I'll let somebody else translate that into Chinese.</p></div><div id="comment-50208-info" class="comment-info"><span class="comment-age">(15 Feb '16, 03:36)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="50225"></span><div id="comment-50225" class="comment"><div id="post-50225-score" class="comment-score"></div><div class="comment-text"><p>感谢楼上给出的帮助。我英语学的不好。辛苦你用谷歌翻译了。</p></div><div id="comment-50225-info" class="comment-info"><span class="comment-age">(15 Feb '16, 22:18)</span> <span class="comment-user userinfo">alalmaliya</span></div></div></div><div id="comment-tools-50198" class="comment-tools"></div><div class="clear"></div><div id="comment-50198-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

