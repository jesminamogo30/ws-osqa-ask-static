+++
type = "question"
title = "[closed] How to block Call of Duty (COD) on the local network?"
description = '''I&#x27;m a teacher and my students are playing COD on our network on their laptops from a flash drive. Every time we block the file name they just change the file name, get someone to be the server, and the whole process starts up again.'''
date = "2014-07-24T02:14:00Z"
lastmod = "2014-07-24T14:58:00Z"
weight = 34868
keywords = [ "cod", "block", "localnetwork" ]
aliases = [ "/questions/34868" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to block Call of Duty (COD) on the local network?](/questions/34868/how-to-block-call-of-duty-cod-on-the-local-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34868-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34868-score" class="post-score" title="current number of votes">0</div><span id="post-34868-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm a teacher and my students are playing COD on our network on their laptops from a flash drive. Every time we block the file name they just change the file name, get someone to be the server, and the whole process starts up again.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cod" rel="tag" title="see questions tagged &#39;cod&#39;">cod</span> <span class="post-tag tag-link-block" rel="tag" title="see questions tagged &#39;block&#39;">block</span> <span class="post-tag tag-link-localnetwork" rel="tag" title="see questions tagged &#39;localnetwork&#39;">localnetwork</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jul '14, 02:14</strong></p><img src="https://secure.gravatar.com/avatar/7ea05361eb7d66f6b002057c228279dd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gotahope&#39;s gravatar image" /><p><span>gotahope</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gotahope has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>25 Jul '14, 01:21</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-34868" class="comments-container"></div><div id="comment-tools-34868" class="comment-tools"></div><div class="clear"></div><div id="comment-34868-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 25 Jul '14, 01:21

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="34869"></span>

<div id="answer-container-34869" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34869-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34869-score" class="post-score" title="current number of votes">0</div><span id="post-34869-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is not really related to Wireshark or network analysis...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jul '14, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-34869" class="comments-container"></div><div id="comment-tools-34869" class="comment-tools"></div><div class="clear"></div><div id="comment-34869-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="34899"></span>

<div id="answer-container-34899" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34899-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34899-score" class="post-score" title="current number of votes">0</div><span id="post-34899-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Someone asked a similar question over at <a href="http://social.technet.microsoft.com/Forums/windowsserver/en-US/97cbbe0c-3ba9-49ac-955f-6d4f9d6ec556/block-network-games-such-as-halo-call-of-duty">http://social.technet.microsoft.com/Forums/windowsserver/en-US/97cbbe0c-3ba9-49ac-955f-6d4f9d6ec556/block-network-games-such-as-halo-call-of-duty</a> where setting <a href="http://technet.microsoft.com/en-us/magazine/2008.06.srp.aspx">Software Restriction Policies</a> appears to be the best answer, but that assumes that <strong><em>you</em></strong> administer the laptops. If the laptops are theirs, then that probably won't help you. In that case, you may have to try to find the ports in use and block them on your network. The <a href="http://findports.com/category.php?tag=document-list-c">Findports</a> site may or may not be helpful in identifying those ports, but you might want to capture traffic from your network to see if those are actually the ports in use or not.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jul '14, 14:58</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-34899" class="comments-container"></div><div id="comment-tools-34899" class="comment-tools"></div><div class="clear"></div><div id="comment-34899-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

