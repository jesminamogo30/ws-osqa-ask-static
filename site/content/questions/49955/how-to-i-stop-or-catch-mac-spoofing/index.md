+++
type = "question"
title = "How to I stop or catch Mac Spoofing?"
description = '''Hi All, How can I stop or catch mac spoofing? Recently I sound out that someone unauthorized has been connecting to my personal network. Thank you!'''
date = "2016-02-07T18:02:00Z"
lastmod = "2016-02-07T22:33:00Z"
weight = 49955
keywords = [ "spoofing" ]
aliases = [ "/questions/49955" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to I stop or catch Mac Spoofing?](/questions/49955/how-to-i-stop-or-catch-mac-spoofing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49955-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49955-score" class="post-score" title="current number of votes">0</div><span id="post-49955-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All, How can I stop or catch mac spoofing? Recently I sound out that someone unauthorized has been connecting to my personal network. Thank you!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-spoofing" rel="tag" title="see questions tagged &#39;spoofing&#39;">spoofing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Feb '16, 18:02</strong></p><img src="https://secure.gravatar.com/avatar/4cec3c57336fdf66413366c8a0035969?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="charchar&#39;s gravatar image" /><p><span>charchar</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="charchar has no accepted answers">0%</span></p></div></div><div id="comments-container-49955" class="comments-container"><span id="49956"></span><div id="comment-49956" class="comment"><div id="post-49956-score" class="comment-score"></div><div class="comment-text"><p>How do you think he has done this, per WLAN, WAN or Ethernet?</p></div><div id="comment-49956-info" class="comment-info"><span class="comment-age">(07 Feb '16, 22:33)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-49955" class="comment-tools"></div><div class="clear"></div><div id="comment-49955-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

