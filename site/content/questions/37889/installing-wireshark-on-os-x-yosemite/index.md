+++
type = "question"
title = "Installing Wireshark on OS X Yosemite"
description = '''Hey everyone, Firstly, I want to mention that my main purpose for using this software is to monitor the activity of my teenage daughter. Would you say that Wireshark is too robust for something like this? I&#x27;m actually excited about learning how to use the program, but I need some guidance with the i...'''
date = "2014-11-16T22:11:00Z"
lastmod = "2014-11-16T22:25:00Z"
weight = 37889
keywords = [ "installation" ]
aliases = [ "/questions/37889" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Installing Wireshark on OS X Yosemite](/questions/37889/installing-wireshark-on-os-x-yosemite)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37889-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37889-score" class="post-score" title="current number of votes">0</div><span id="post-37889-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey everyone,</p><p>Firstly, I want to mention that my main purpose for using this software is to monitor the activity of my teenage daughter. Would you say that Wireshark is too robust for something like this? I'm actually excited about learning how to use the program, but I need some guidance with the installation. So far I've downloaded wireshark and the XQuartz app that is required. Where should I even begin with installing? I've read some of the instructions in the manual, but a little assistance would be cool.</p><p>thanking that awesome person, who responds to this question, in advance. : )</p><p>anon.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Nov '14, 22:11</strong></p><img src="https://secure.gravatar.com/avatar/d37456748a82ac254a118136397586dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rawlem&#39;s gravatar image" /><p><span>rawlem</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rawlem has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Nov '14, 22:15</strong> </span></p></div></div><div id="comments-container-37889" class="comments-container"><span id="37890"></span><div id="comment-37890" class="comment"><div id="post-37890-score" class="comment-score"></div><div class="comment-text"><p>I should have asked, where do I begin with setting up the program to work..?</p><p>Thanks!</p></div><div id="comment-37890-info" class="comment-info"><span class="comment-age">(16 Nov '14, 22:13)</span> <span class="comment-user userinfo">rawlem</span></div></div><span id="37891"></span><div id="comment-37891" class="comment"><div id="post-37891-score" class="comment-score"></div><div class="comment-text"><p>yep..me again. i figure this may help determine my next steps.</p><p>After glancing at the readme file, I had a mental block here:</p><p><strong>If this is your first time installing XQuartz, you may wish to logout and log back in. This will update your DISPLAY environment variable to point to XQuartz.app rather than X11.app. If you would prefer to keep using X11.app as your default server (you can still launch XQuartz.app manually), you'll want to disable /Library/LaunchAgents/org.macosforge.xquartz.startx.plist using launchctl(1).</strong></p><p>This would be my first time..does this mean I should log in and out of my user account and then try to install?</p></div><div id="comment-37891-info" class="comment-info"><span class="comment-age">(16 Nov '14, 22:25)</span> <span class="comment-user userinfo">rawlem</span></div></div></div><div id="comment-tools-37889" class="comment-tools"></div><div class="clear"></div><div id="comment-37889-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

