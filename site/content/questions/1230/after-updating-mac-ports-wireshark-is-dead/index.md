+++
type = "question"
title = "after updating mac ports wireshark is dead!"
description = '''Guys, I run Mac OSX 10.5.8 and after updating Mac ports wireshark is dead in the water, just wont load. Anybody know how I can fix this?'''
date = "2010-12-03T09:20:00Z"
lastmod = "2010-12-03T09:20:00Z"
weight = 1230
keywords = [ "wireshark" ]
aliases = [ "/questions/1230" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [after updating mac ports wireshark is dead!](/questions/1230/after-updating-mac-ports-wireshark-is-dead)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1230-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1230-score" class="post-score" title="current number of votes">0</div><span id="post-1230-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Guys, I run Mac OSX 10.5.8 and after updating Mac ports wireshark is dead in the water, just wont load. Anybody know how I can fix this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Dec '10, 09:20</strong></p><img src="https://secure.gravatar.com/avatar/139e5ad10db460172572347feb22a6c5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="faboge&#39;s gravatar image" /><p><span>faboge</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="faboge has no accepted answers">0%</span></p></div></div><div id="comments-container-1230" class="comments-container"></div><div id="comment-tools-1230" class="comment-tools"></div><div class="clear"></div><div id="comment-1230-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

