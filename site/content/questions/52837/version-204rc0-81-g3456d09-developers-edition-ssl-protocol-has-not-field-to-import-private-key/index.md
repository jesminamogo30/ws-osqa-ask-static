+++
type = "question"
title = "Version 2.0.4rc0-81-g3456d09 developers edition SSL protocol has not field to import private key"
description = '''IN preferences | Protocols | SSL, there are not fields to import keys are specify debug files. There are just three checkboxes: reassemble SSL records... reassemble ssl applicaitons.... and MAC Ignore....  How do I import private and session keys to decrypt a trace?'''
date = "2016-05-23T12:24:00Z"
lastmod = "2016-05-24T03:15:00Z"
weight = 52837
keywords = [ "ssl", "decryption" ]
aliases = [ "/questions/52837" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Version 2.0.4rc0-81-g3456d09 developers edition SSL protocol has not field to import private key](/questions/52837/version-204rc0-81-g3456d09-developers-edition-ssl-protocol-has-not-field-to-import-private-key)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52837-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52837-score" class="post-score" title="current number of votes">0</div><span id="post-52837-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>IN preferences | Protocols | SSL, there are not fields to import keys are specify debug files. There are just three checkboxes: reassemble SSL records... reassemble ssl applicaitons.... and MAC Ignore....</p><p>How do I import private and session keys to decrypt a trace?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '16, 12:24</strong></p><img src="https://secure.gravatar.com/avatar/746b5c3eccaa5587baf9fcc97adbb948?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hmeista&#39;s gravatar image" /><p><span>hmeista</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hmeista has no accepted answers">0%</span></p></div></div><div id="comments-container-52837" class="comments-container"></div><div id="comment-tools-52837" class="comment-tools"></div><div class="clear"></div><div id="comment-52837-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52859"></span>

<div id="answer-container-52859" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52859-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52859-score" class="post-score" title="current number of votes">0</div><span id="post-52859-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Could you post the content of Help -&gt; About Wireshark window?</p><p>Presumably you compiled Wireshark yourself, without libgcrypt and libgnutls support. This deactivates the options you are looking for.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 May '16, 03:15</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-52859" class="comments-container"></div><div id="comment-tools-52859" class="comment-tools"></div><div class="clear"></div><div id="comment-52859-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

