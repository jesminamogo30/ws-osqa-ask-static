+++
type = "question"
title = "RRC message not found in trace"
description = '''Not able to found RRC message packets in logs'''
date = "2011-01-21T23:28:00Z"
lastmod = "2012-07-16T00:33:00Z"
weight = 1869
keywords = [ "protocol" ]
aliases = [ "/questions/1869" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [RRC message not found in trace](/questions/1869/rrc-message-not-found-in-trace)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1869-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1869-score" class="post-score" title="current number of votes">0</div><span id="post-1869-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Not able to found RRC message packets in logs</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '11, 23:28</strong></p><img src="https://secure.gravatar.com/avatar/7b3ef1d81dd12fb0ea1f18060f5c60f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ravi&#39;s gravatar image" /><p><span>Ravi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ravi has no accepted answers">0%</span></p></div></div><div id="comments-container-1869" class="comments-container"><span id="1870"></span><div id="comment-1870" class="comment"><div id="post-1870-score" class="comment-score"></div><div class="comment-text"><p>I have installed</p><p>latest WinPCAP 4.1.2</p><p>WireShark based 3G Protocol Analyzer Version 4.0.0 Based on official release 1.0.4 of Wireshark</p><p>Also, checked Tab Analyze ....&gt; Enabaled all Protocol</p><p>Pls help us if anything is required to view RRC &amp; FP protocol messages which is captured in log file (RANAP &amp; NBAP messages are visible)</p></div><div id="comment-1870-info" class="comment-info"><span class="comment-age">(22 Jan '11, 00:32)</span> <span class="comment-user userinfo">Ravi</span></div></div><span id="1894"></span><div id="comment-1894" class="comment"><div id="post-1894-score" class="comment-score"></div><div class="comment-text"><p>Hi, You should probably ask the developers of the "WireShark based 3G Protocol Analyzer Version 4.0.0" about that. Is the RRC carried inside FP? If so I think a special header is needed to carry the FP configuration. See http://wiki.wireshark.org/FP%20Hint http://wiki.wireshark.org/META</p></div><div id="comment-1894-info" class="comment-info"><span class="comment-age">(23 Jan '11, 09:35)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="10629"></span><div id="comment-10629" class="comment"><div id="post-10629-score" class="comment-score"></div><div class="comment-text"><p>can any body please help me Where i can get wireshak 3G protocol analyzer software...</p><p>Thanks in advance.</p></div><div id="comment-10629-info" class="comment-info"><span class="comment-age">(03 May '12, 04:11)</span> <span class="comment-user userinfo">Satish_Kumar</span></div></div><span id="12736"></span><div id="comment-12736" class="comment"><div id="post-12736-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I have installed<br />
WireShark based 3G Protocol Analyzer Version 4.0.0 Based on official release 1.0.4 of Wireshark</p></blockquote><p><span></span><span>@Ravi</span>: Can you shed some light on the download link of "WireShark based 3G Protocol Analyzer", see also this question: <a href="http://ask.wireshark.org/questions/12727/wireshark-based-3g-protocol-analyser">http://ask.wireshark.org/questions/12727/wireshark-based-3g-protocol-analyser</a></p><p>Thank you!</p></div><div id="comment-12736-info" class="comment-info"><span class="comment-age">(16 Jul '12, 00:33)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-1869" class="comment-tools"></div><div class="clear"></div><div id="comment-1869-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

