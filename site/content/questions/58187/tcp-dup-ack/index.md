+++
type = "question"
title = "tcp dup ack"
description = ''' Client -Server -(fin,ack)  Server - Client (ack)  Client - Server (tcp dup ack)  Client - Server (syn)  Server - Client (syn, ack)   What does it mean? can someone explain me this ? especially the tcp dup ack? '''
date = "2016-12-17T07:39:00Z"
lastmod = "2016-12-17T07:39:00Z"
weight = 58187
keywords = [ "dup", "dup-ack", "tcp" ]
aliases = [ "/questions/58187" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [tcp dup ack](/questions/58187/tcp-dup-ack)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58187-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58187-score" class="post-score" title="current number of votes">0</div><span id="post-58187-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><ul><li>Client -Server -(fin,ack)</li><li>Server - Client (ack)</li><li>Client - Server (tcp dup ack)</li><li>Client - Server (syn)</li><li>Server - Client (syn, ack)</li></ul><p>What does it mean? can someone explain me this ? especially the tcp dup ack?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dup" rel="tag" title="see questions tagged &#39;dup&#39;">dup</span> <span class="post-tag tag-link-dup-ack" rel="tag" title="see questions tagged &#39;dup-ack&#39;">dup-ack</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Dec '16, 07:39</strong></p><img src="https://secure.gravatar.com/avatar/b44dffcbc9d550d562a112e2b83e786c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="luna&#39;s gravatar image" /><p><span>luna</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="luna has no accepted answers">0%</span></p></div></div><div id="comments-container-58187" class="comments-container"></div><div id="comment-tools-58187" class="comment-tools"></div><div class="clear"></div><div id="comment-58187-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

