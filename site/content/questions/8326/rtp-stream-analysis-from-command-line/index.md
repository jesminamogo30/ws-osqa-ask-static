+++
type = "question"
title = "RTP Stream Analysis from command line"
description = '''Hi all, I need to save the RTP stream analisys in CSV format from command line and I don&#x27;t know how. I tried typing &quot;tshark -r myrtpfile.cap -q -z rtp,streams&quot; but it just display the statistics. I need the information shown by in table (what it&#x27;s obtained by pressing the &quot;save as CSV&quot;). Thanks in a...'''
date = "2012-01-11T06:47:00Z"
lastmod = "2015-06-25T01:52:00Z"
weight = 8326
keywords = [ "rtp", "analysis" ]
aliases = [ "/questions/8326" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [RTP Stream Analysis from command line](/questions/8326/rtp-stream-analysis-from-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8326-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8326-score" class="post-score" title="current number of votes">0</div><span id="post-8326-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>I need to save the RTP stream analisys in CSV format from command line and I don't know how. I tried typing <code>"tshark -r myrtpfile.cap -q -z rtp,streams"</code> but it just display the statistics. I need the information shown by in table (what it's obtained by pressing the "save as CSV").</p><p>Thanks in advance.</p><p>Kind regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '12, 06:47</strong></p><img src="https://secure.gravatar.com/avatar/45acf87aa9c575978ebe16faab9d06b5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Patt&#39;s gravatar image" /><p><span>Patt</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Patt has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Jan '12, 07:27</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-8326" class="comments-container"><span id="19458"></span><div id="comment-19458" class="comment"><div id="post-19458-score" class="comment-score"></div><div class="comment-text"><p>Hi Patt,I have the same problem. Have you solved? Thanks Cristian</p></div><div id="comment-19458-info" class="comment-info"><span class="comment-age">(13 Mar '13, 08:46)</span> <span class="comment-user userinfo">Cris</span></div></div><span id="43538"></span><div id="comment-43538" class="comment"><div id="post-43538-score" class="comment-score"></div><div class="comment-text"><p>Hi All, i'm also looking for a solution on this one? Has anybody managed to get this working?</p></div><div id="comment-43538-info" class="comment-info"><span class="comment-age">(25 Jun '15, 01:52)</span> <span class="comment-user userinfo">krypton</span></div></div></div><div id="comment-tools-8326" class="comment-tools"></div><div class="clear"></div><div id="comment-8326-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

