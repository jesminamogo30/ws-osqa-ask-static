+++
type = "question"
title = "How to know open IP address for internet behind proxy"
description = '''In my organization, We have been allotted a common proxy address in a common lan coonection for majority in which some of the websites like facebook and youtube are blocked. But few persons have special privilege to use different proxy which allows facebook and youtube. Is their any method to know s...'''
date = "2016-05-09T02:18:00Z"
lastmod = "2016-05-09T02:18:00Z"
weight = 52334
keywords = [ "ip", "proxy", "internet" ]
aliases = [ "/questions/52334" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to know open IP address for internet behind proxy](/questions/52334/how-to-know-open-ip-address-for-internet-behind-proxy)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52334-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52334-score" class="post-score" title="current number of votes">-1</div><span id="post-52334-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In my organization, We have been allotted a common proxy address in a common lan coonection for majority in which some of the websites like facebook and youtube are blocked. But few persons have special privilege to use different proxy which allows facebook and youtube. Is their any method to know such ip addresses that are being used in the same network connection i am connected with. Any help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-proxy" rel="tag" title="see questions tagged &#39;proxy&#39;">proxy</span> <span class="post-tag tag-link-internet" rel="tag" title="see questions tagged &#39;internet&#39;">internet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '16, 02:18</strong></p><img src="https://secure.gravatar.com/avatar/e0c449b01c48ded913c315802c4c8ebb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="monsterdevil&#39;s gravatar image" /><p><span>monsterdevil</span><br />
<span class="score" title="5 reputation points">5</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="monsterdevil has no accepted answers">0%</span></p></div></div><div id="comments-container-52334" class="comments-container"></div><div id="comment-tools-52334" class="comment-tools"></div><div class="clear"></div><div id="comment-52334-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

