+++
type = "question"
title = "/asn1/CAM/packet-CAM-hfarr.c: missing field initializer for &#x27;p_id&#x27; of &#x27;hf:register_info&#x27;"
description = '''Can someone help me? I dont know where i can solve this error  /asn1/CAM/packet-CAM-hfarr.c: missing field initializer for &#x27;p_id&#x27; of &#x27;hf:register_info&#x27; this error occours many times if i try to compile my asn1 dissector. Is the problem in my packet-CAM-template.c? If yes, is there anywhere a good ma...'''
date = "2014-08-28T05:02:00Z"
lastmod = "2014-08-28T05:02:00Z"
weight = 35837
keywords = [ "asn1" ]
aliases = [ "/questions/35837" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [/asn1/CAM/packet-CAM-hfarr.c: missing field initializer for 'p\_id' of 'hf:register\_info'](/questions/35837/asn1campacket-cam-hfarrc-missing-field-initializer-for-p_id-of-hfregister_info)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35837-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35837-score" class="post-score" title="current number of votes">0</div><span id="post-35837-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can someone help me? I dont know where i can solve this error <code>/asn1/CAM/packet-CAM-hfarr.c: missing field initializer for 'p_id' of 'hf:register_info'</code> this error occours many times if i try to compile my asn1 dissector. Is the problem in my packet-CAM-template.c? If yes, is there anywhere a good manual for writing asn1 dissectors?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-asn1" rel="tag" title="see questions tagged &#39;asn1&#39;">asn1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '14, 05:02</strong></p><img src="https://secure.gravatar.com/avatar/f65ac046295141d9f33ce4ac1770b5a0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Venturina&#39;s gravatar image" /><p><span>Venturina</span><br />
<span class="score" title="1 reputation points">1</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Venturina has no accepted answers">0%</span></p></div></div><div id="comments-container-35837" class="comments-container"></div><div id="comment-tools-35837" class="comment-tools"></div><div class="clear"></div><div id="comment-35837-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

