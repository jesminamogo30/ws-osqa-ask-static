+++
type = "question"
title = "analyze traffic to a specific website"
description = '''One of our servers is sending out a bunch of calls to a specific ip address. I know it is via https and I know the call is getUserAccount. I have captured packets via wireshark on all of the suspected servers but my question is: How do I / What is the most efficient way to find the specific info my ...'''
date = "2011-03-14T10:33:00Z"
lastmod = "2011-03-14T20:04:00Z"
weight = 2805
keywords = [ "analyze" ]
aliases = [ "/questions/2805" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [analyze traffic to a specific website](/questions/2805/analyze-traffic-to-a-specific-website)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2805-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2805-score" class="post-score" title="current number of votes">0</div><span id="post-2805-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>One of our servers is sending out a bunch of calls to a specific ip address. I know it is via https and I know the call is getUserAccount. I have captured packets via wireshark on all of the suspected servers but my question is:</p><p>How do I / What is the most efficient way to find the specific info my server is sending to this particular IP?</p><p>I have filtered by ssl and analyzed ssl stream but it still looks unintelligable.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analyze" rel="tag" title="see questions tagged &#39;analyze&#39;">analyze</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Mar '11, 10:33</strong></p><img src="https://secure.gravatar.com/avatar/0027e8271237f8462a78602c76835578?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Khepf&#39;s gravatar image" /><p><span>Khepf</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Khepf has no accepted answers">0%</span></p></div></div><div id="comments-container-2805" class="comments-container"></div><div id="comment-tools-2805" class="comment-tools"></div><div class="clear"></div><div id="comment-2805-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2812"></span>

<div id="answer-container-2812" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2812-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2812-score" class="post-score" title="current number of votes">0</div><span id="post-2812-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>How do you know the 'call' is getUserAccount if it is via HTTPS ?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Mar '11, 20:04</strong></p><img src="https://secure.gravatar.com/avatar/69ac745d2d90272605b1847ea5fe451f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vivekrj&#39;s gravatar image" /><p><span>vivekrj</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vivekrj has no accepted answers">0%</span></p></div></div><div id="comments-container-2812" class="comments-container"></div><div id="comment-tools-2812" class="comment-tools"></div><div class="clear"></div><div id="comment-2812-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

