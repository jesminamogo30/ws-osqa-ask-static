+++
type = "question"
title = "[closed] what factors that determines the criticality of a node in DNS"
description = '''what factors that determines the criticality of a node in DNS'''
date = "2014-06-06T03:25:00Z"
lastmod = "2014-06-06T07:00:00Z"
weight = 33496
keywords = [ "dns", "criticality" ]
aliases = [ "/questions/33496" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] what factors that determines the criticality of a node in DNS](/questions/33496/what-factors-that-determines-the-criticality-of-a-node-in-dns)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33496-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33496-score" class="post-score" title="current number of votes">0</div><span id="post-33496-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what factors that determines the criticality of a node in DNS</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dns" rel="tag" title="see questions tagged &#39;dns&#39;">dns</span> <span class="post-tag tag-link-criticality" rel="tag" title="see questions tagged &#39;criticality&#39;">criticality</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jun '14, 03:25</strong></p><img src="https://secure.gravatar.com/avatar/84a759b8584868e51394ec196d6ed4af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="teju&#39;s gravatar image" /><p><span>teju</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="teju has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>06 Jun '14, 07:27</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-33496" class="comments-container"><span id="33497"></span><div id="comment-33497" class="comment"><div id="post-33497-score" class="comment-score"></div><div class="comment-text"><p>could you please rephrase your question? It is completely unclear what you're asking.</p></div><div id="comment-33497-info" class="comment-info"><span class="comment-age">(06 Jun '14, 03:51)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="33499"></span><div id="comment-33499" class="comment"><div id="post-33499-score" class="comment-score"></div><div class="comment-text"><p>The health of the DNS can be determined with the help of critical nodes.so what are the factors that effect the health of DNS?</p></div><div id="comment-33499-info" class="comment-info"><span class="comment-age">(06 Jun '14, 03:56)</span> <span class="comment-user userinfo">teju</span></div></div><span id="33506"></span><div id="comment-33506" class="comment"><div id="post-33506-score" class="comment-score"></div><div class="comment-text"><p>I think this is not really related to Wireshark, and looks more like an university study to me :-)</p></div><div id="comment-33506-info" class="comment-info"><span class="comment-age">(06 Jun '14, 07:00)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-33496" class="comment-tools"></div><div class="clear"></div><div id="comment-33496-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Jaap 06 Jun '14, 07:27

</div>

</div>

</div>

