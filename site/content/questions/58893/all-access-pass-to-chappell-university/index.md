+++
type = "question"
title = "[closed] All Access Pass to Chappell University"
description = '''Has anyone had access to this or know if it is worth the money or not? '''
date = "2017-01-19T13:07:00Z"
lastmod = "2017-01-20T07:27:00Z"
weight = 58893
keywords = [ "training" ]
aliases = [ "/questions/58893" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] All Access Pass to Chappell University](/questions/58893/all-access-pass-to-chappell-university)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58893-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58893-score" class="post-score" title="current number of votes">0</div><span id="post-58893-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Has anyone had access to this or know if it is worth the money or not?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-training" rel="tag" title="see questions tagged &#39;training&#39;">training</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jan '17, 13:07</strong></p><img src="https://secure.gravatar.com/avatar/6528b7a1b93429c225c495afe7659433?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="elliep&#39;s gravatar image" /><p><span>elliep</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="elliep has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>20 Jan '17, 07:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-58893" class="comments-container"></div><div id="comment-tools-58893" class="comment-tools"></div><div class="clear"></div><div id="comment-58893-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by cmaynard 20 Jan '17, 07:03

</div>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58912"></span>

<div id="answer-container-58912" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58912-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58912-score" class="post-score" title="current number of votes">0</div><span id="post-58912-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This question is best asked elsewhere.</p><p>From the <a href="https://ask.wireshark.org/faq/">faq</a>:</p><p><strong>What kinds of questions can I ask here?</strong></p><p>Questions should be relevant to Wireshark features, protocol analysis, or Wireshark development. Before asking please search for similar questions. You can search for questions by their title or tags.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jan '17, 07:02</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-58912" class="comments-container"><span id="58916"></span><div id="comment-58916" class="comment"><div id="post-58916-score" class="comment-score"></div><div class="comment-text"><p>Do you have an example on where this should be asked?</p></div><div id="comment-58916-info" class="comment-info"><span class="comment-age">(20 Jan '17, 07:22)</span> <span class="comment-user userinfo">elliep</span></div></div><span id="58918"></span><div id="comment-58918" class="comment"><div id="post-58918-score" class="comment-score"></div><div class="comment-text"><p>Sure, as a start, you could try the wireshark-users (or <em>possibly</em> even the wireshark-dev) mailing list? <a href="https://www.wireshark.org/lists/">https://www.wireshark.org/lists/</a></p></div><div id="comment-58918-info" class="comment-info"><span class="comment-age">(20 Jan '17, 07:27)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-58912" class="comment-tools"></div><div class="clear"></div><div id="comment-58912-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

