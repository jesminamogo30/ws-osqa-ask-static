+++
type = "question"
title = "content of send message in wireshark"
description = '''hi everybody i cant see **content message of packet send in wireshark، i want see the string content of message Please anyone has an idea، answer to my question ........ my ip address is 192.168.1.158 and i capture network traffic , i want see the content of file that sent via network, i think that ...'''
date = "2015-01-31T04:30:00Z"
lastmod = "2015-01-31T13:59:00Z"
weight = 39520
keywords = [ "content", "packet" ]
aliases = [ "/questions/39520" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [content of send message in wireshark](/questions/39520/content-of-send-message-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39520-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39520-score" class="post-score" title="current number of votes">0</div><span id="post-39520-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi everybody i cant see **content message of packet send in wireshark، i want see the string content of message Please anyone has an idea، answer to my question</p><p>........</p><p>my ip address is 192.168.1.158 and i capture network traffic , i want see the content of file that sent via network, i think that the name of file is recip.docx , i attach capture file <a href="http://s000.tinyupload.com/?file_id=63758818014877024063">link text</a></p><p>pls help thank u</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-content" rel="tag" title="see questions tagged &#39;content&#39;">content</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jan '15, 04:30</strong></p><img src="https://secure.gravatar.com/avatar/5fa52476743b530b7a918df7eb548f74?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="behzad87&#39;s gravatar image" /><p><span>behzad87</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="behzad87 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Feb '15, 00:26</strong> </span></p></div></div><div id="comments-container-39520" class="comments-container"><span id="39526"></span><div id="comment-39526" class="comment"><div id="post-39526-score" class="comment-score"></div><div class="comment-text"><p>Maybe you could rephrase your question or add more details? It's not at all clear what it is that you are asking.</p></div><div id="comment-39526-info" class="comment-info"><span class="comment-age">(31 Jan '15, 13:59)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-39520" class="comment-tools"></div><div class="clear"></div><div id="comment-39520-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

