+++
type = "question"
title = "Why does Wireshrk 64 bit instalation fail in a WIN enviomnent issue with Libwireshark.dll"
description = '''Hi: While trying to install 64 bit Wirshark in a win environment we keep receiving error: Extract error writing to file Libwireshark.dll. It may be that someone previously started a 32 bit installation and stooped, but am not sure, There is no Libwireshark.dll on the computer and if there is we trie...'''
date = "2016-01-27T05:55:00Z"
lastmod = "2016-01-27T06:22:00Z"
weight = 49543
keywords = [ "install", "libwireshark.dll" ]
aliases = [ "/questions/49543" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why does Wireshrk 64 bit instalation fail in a WIN enviomnent issue with Libwireshark.dll](/questions/49543/why-does-wireshrk-64-bit-instalation-fail-in-a-win-enviomnent-issue-with-libwiresharkdll)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49543-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49543-score" class="post-score" title="current number of votes">0</div><span id="post-49543-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi: While trying to install 64 bit Wirshark in a win environment we keep receiving error: Extract error writing to file Libwireshark.dll.</p><p>It may be that someone previously started a 32 bit installation and stooped, but am not sure, There is no Libwireshark.dll on the computer and if there is we tried to delete it. Keep receiving the same error. How can i get past this issue ? Thanks mikik</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-install" rel="tag" title="see questions tagged &#39;install&#39;">install</span> <span class="post-tag tag-link-libwireshark.dll" rel="tag" title="see questions tagged &#39;libwireshark.dll&#39;">libwireshark.dll</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jan '16, 05:55</strong></p><img src="https://secure.gravatar.com/avatar/f2541ba46ea7e8ae73deaa7c39456de2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mikik&#39;s gravatar image" /><p><span>mikik</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mikik has no accepted answers">0%</span></p></div></div><div id="comments-container-49543" class="comments-container"></div><div id="comment-tools-49543" class="comment-tools"></div><div class="clear"></div><div id="comment-49543-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49547"></span>

<div id="answer-container-49547" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49547-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49547-score" class="post-score" title="current number of votes">0</div><span id="post-49547-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds like a messed up installation. Try the following:</p><ol><li>Reboot the host.</li><li>Uninstall any instances of Wireshark listed in "Add or Remove Programs" or "Programs and Features" as appropriate.</li><li>Confirm that there are no Wireshark directories in either "Program files or "Program Files (x86)". If there are, check 2. above again, and if there are no listed Wireshark installs, then manually delete the directories.</li><li>Reboot the host again.</li><li>Try installing Wireshark again. Note the installer should ask for elevation (admin privs), if it doesn't then try installing again by right-clicking and selecting "Run as Administrator".</li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jan '16, 06:22</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-49547" class="comments-container"></div><div id="comment-tools-49547" class="comment-tools"></div><div class="clear"></div><div id="comment-49547-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

