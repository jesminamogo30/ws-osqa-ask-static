+++
type = "question"
title = "The Flow Graph on ver1.12.1 can not display L2 message such as EAP."
description = '''Hi! Now I am using v1.12.1 wireshark on my laptop PC(windows 7). And I am going to analyse the 802.1x. So, I have tried the &quot;Flow Graph&quot; to check the sequence of 802.1x by the &quot;standard src/dst addr&quot;. However, the &quot;Flow Graph&quot; can not display EAP messages. While, the &quot;Flow Graph&quot; displays the RADIUS...'''
date = "2014-10-16T23:36:00Z"
lastmod = "2014-10-16T23:36:00Z"
weight = 37122
keywords = [ "v1.12.1", "flowgraph" ]
aliases = [ "/questions/37122" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [The Flow Graph on ver1.12.1 can not display L2 message such as EAP.](/questions/37122/the-flow-graph-on-ver1121-can-not-display-l2-message-such-as-eap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37122-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37122-score" class="post-score" title="current number of votes">0</div><span id="post-37122-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi! Now I am using v1.12.1 wireshark on my laptop PC(windows 7). And I am going to analyse the 802.1x. So, I have tried the "Flow Graph" to check the sequence of 802.1x by the "standard src/dst addr". However, the "Flow Graph" can not display EAP messages. While, the "Flow Graph" displays the RADIUS messages. I suspect the "Flow Graph" in v1.12.1 of wireshark has above bugs. Because, the "Flow Graph" in v1.10.7 of wireshark can display EAP messages with the same procedure.</p><p>Please give me the solution of above problem.</p><p>Best Regards, Y. Matsuura(Japanese)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-v1.12.1" rel="tag" title="see questions tagged &#39;v1.12.1&#39;">v1.12.1</span> <span class="post-tag tag-link-flowgraph" rel="tag" title="see questions tagged &#39;flowgraph&#39;">flowgraph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Oct '14, 23:36</strong></p><img src="https://secure.gravatar.com/avatar/18251f95d6da2c66f0d7a737a49f9ed3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yutaka%20Matsuura&#39;s gravatar image" /><p><span>Yutaka Matsuura</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yutaka Matsuura has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Mar '15, 19:05</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-37122" class="comments-container"></div><div id="comment-tools-37122" class="comment-tools"></div><div class="clear"></div><div id="comment-37122-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

