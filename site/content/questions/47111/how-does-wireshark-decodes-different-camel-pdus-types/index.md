+++
type = "question"
title = "how does Wireshark decodes different camel PDUs types ?"
description = '''Hi, How does the Wireshark decide the type of TCAP+camel PDU that needs to be decoded (Generic_sms_PDUs ,GenericSSF_gsmSCF_PDUs etc) ? is this information present in ANS.1 files or it is based upon the value of some parameter in protocol stack? I am currently looking into (/IP/SCTP/MTP3/sccp/Tcap/Ca...'''
date = "2015-10-31T10:55:00Z"
lastmod = "2015-10-31T10:55:00Z"
weight = 47111
keywords = [ "tcap", "asn.1", "pdu", "sccp", "camel" ]
aliases = [ "/questions/47111" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how does Wireshark decodes different camel PDUs types ?](/questions/47111/how-does-wireshark-decodes-different-camel-pdus-types)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47111-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47111-score" class="post-score" title="current number of votes">0</div><span id="post-47111-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>How does the Wireshark decide the type of TCAP+camel PDU that needs to be decoded (Generic_sms_PDUs ,GenericSSF_gsmSCF_PDUs etc) ? is this information present in ANS.1 files or it is based upon the value of some parameter in protocol stack?</p><p>I am currently looking into (/IP/SCTP/MTP3/sccp/Tcap/Camel) stack.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcap" rel="tag" title="see questions tagged &#39;tcap&#39;">tcap</span> <span class="post-tag tag-link-asn.1" rel="tag" title="see questions tagged &#39;asn.1&#39;">asn.1</span> <span class="post-tag tag-link-pdu" rel="tag" title="see questions tagged &#39;pdu&#39;">pdu</span> <span class="post-tag tag-link-sccp" rel="tag" title="see questions tagged &#39;sccp&#39;">sccp</span> <span class="post-tag tag-link-camel" rel="tag" title="see questions tagged &#39;camel&#39;">camel</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '15, 10:55</strong></p><img src="https://secure.gravatar.com/avatar/adb7d1ae53ce8298c0f9b04b6bfebd01?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shashank%20Jain&#39;s gravatar image" /><p><span>Shashank Jain</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shashank Jain has no accepted answers">0%</span></p></div></div><div id="comments-container-47111" class="comments-container"></div><div id="comment-tools-47111" class="comment-tools"></div><div class="clear"></div><div id="comment-47111-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

