+++
type = "question"
title = "Wireshark crashing on OS X when trying to open a file? Can some one please help me how to resolve it?"
description = '''Let me know if you need any other info. This is what I see in the terminal. The domain/default pair of (kCFPreferencesAnyApplication, AppleAquaColorVariant) does not exist 2014-09-19 12:20:36.098 defaults[8199:507]  The domain/default pair of (kCFPreferencesAnyApplication, AppleHighlightColor) does ...'''
date = "2014-09-19T12:31:00Z"
lastmod = "2014-09-20T16:15:00Z"
weight = 36465
keywords = [ "osx", "crash", "wireshark" ]
aliases = [ "/questions/36465" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark crashing on OS X when trying to open a file? Can some one please help me how to resolve it?](/questions/36465/wireshark-crashing-on-os-x-when-trying-to-open-a-file-can-some-one-please-help-me-how-to-resolve-it)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36465-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36465-score" class="post-score" title="current number of votes">0</div><span id="post-36465-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Let me know if you need any other info.</p><p>This is what I see in the terminal.</p><p>The domain/default pair of (kCFPreferencesAnyApplication, AppleAquaColorVariant) does not exist 2014-09-19 12:20:36.098 defaults[8199:507] The domain/default pair of (kCFPreferencesAnyApplication, AppleHighlightColor) does not exist</p><p>(process:8186): Gtk-WARNING **: Locale not supported by C library. Using the fallback 'C' locale.</p><p>(wireshark-bin:8186): Gtk-WARNING **: Unable to locate theme engine in module_path: "clearlooks", Fontconfig warning: ignoring .UTF-8: not a valid language tag</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Sep '14, 12:31</strong></p><img src="https://secure.gravatar.com/avatar/e517ee87d58a1ae8522489cf0ae5baa7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="msk_1227&#39;s gravatar image" /><p><span>msk_1227</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="msk_1227 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Sep '14, 16:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-36465" class="comments-container"></div><div id="comment-tools-36465" class="comment-tools"></div><div class="clear"></div><div id="comment-36465-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36487"></span>

<div id="answer-container-36487" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36487-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36487-score" class="post-score" title="current number of votes">0</div><span id="post-36487-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Crashes should be filed as bugs on <a href="http://bugs.wireshark.org/">the Wireshark Bugzilla</a>.</p><p>Before filing the bug, check in Console (in Applications/Utilities) under "User Diagnostics" to see if there are any crash reports for Wireshark or wireshark-bin. If so, include the text of the crash report in the bug.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Sep '14, 16:15</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-36487" class="comments-container"></div><div id="comment-tools-36487" class="comment-tools"></div><div class="clear"></div><div id="comment-36487-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

