+++
type = "question"
title = "Windows 7 and old versions"
description = '''Hi, The many coworkers at my company run various versions of Wireshark. We&#x27;re upgrading to Windows 7 next year, so we need to know which is the first version which works under Windows 7. I see in old msgs that 1.2.3 (which came out when Win7 was released 2 years ago) works under Windows 7. But did e...'''
date = "2011-11-21T11:49:00Z"
lastmod = "2011-11-22T09:48:00Z"
weight = 7539
keywords = [ "windows7", "old", "wireshark" ]
aliases = [ "/questions/7539" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 7 and old versions](/questions/7539/windows-7-and-old-versions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7539-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7539-score" class="post-score" title="current number of votes">0</div><span id="post-7539-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>The many coworkers at my company run various versions of Wireshark. We're upgrading to Windows 7 next year, so we need to know which is the first version which works under Windows 7. I see in old msgs that 1.2.3 (which came out when Win7 was released 2 years ago) works under Windows 7. But did earlier versions work? Did anyone try?<br />
</p><p>The oldest version we found on our LAN is 0.95. I downloaded Wireshark (Ethereal) 0.95, which does not include WinPcap. Was WinPcap 2.3 the corresponding version? I figure if 0.95 works, the newer versions probably also work.</p><p>By now I'm sure you're wondering why the heck don't we simply upgrade to the latest version. I think the rationale is that for some applications, newer versions are not 100% backwards compatible, and for some tools we have complex scripts; etc that we don't have money to rewrite. One coworker told me an old .dll he wrote for wireshark did not work simply by copying it to the latest wireshark.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-old" rel="tag" title="see questions tagged &#39;old&#39;">old</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Nov '11, 11:49</strong></p><img src="https://secure.gravatar.com/avatar/8d081b07b1dc6a063842198424856041?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Block&#39;s gravatar image" /><p><span>Block</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Block has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:34</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-7539" class="comments-container"></div><div id="comment-tools-7539" class="comment-tools"></div><div class="clear"></div><div id="comment-7539-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7548"></span>

<div id="answer-container-7548" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7548-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7548-score" class="post-score" title="current number of votes">1</div><span id="post-7548-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>According to <a href="http://www.winpcap.org/news.htm">the WinPcap news page</a>:</p><blockquote><p>19 October, 2009 As of today, WinPcap 4.1 is available in the download section of the WinPcap website.</p><p>This release contains a large series of improvements that were gradually added to WinPcap during the various beta's.</p><p>First of all, this version includes full support for x64 platforms, both in the driver and in the user level libraries. Also, the long awaited support for Windows 7 (and Windows Server 2008 R2) has been added to the long list of supported flavors of Windows.</p></blockquote><p>so if you want Windows 7 support, you need WinPcap 4.1 or later (later is better; as that news page notes, 4.1.1 fixes some bugs and 4.1.2 fixes more bugs).</p><p>Yes, this means that if you want to capture traffic on Windows 7 with older versions of Wireshark, you will have to manually install a later version of WinPcap after installing Wireshark.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Nov '11, 17:40</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-7548" class="comments-container"><span id="7544"></span><div id="comment-7544" class="comment"><div id="post-7544-score" class="comment-score"></div><div class="comment-text"><p>I have since installed Wireshark 0.95 with WinPcap 2.3 but no interfaces were listed. I then installed the latest WinPcap and then Wireshark 0.95 worked. I might check other versions of WinPcap tomorrow to see if users will need to upgrade for Win7</p></div><div id="comment-7544-info" class="comment-info"><span class="comment-age">(21 Nov '11, 13:16)</span> <span class="comment-user userinfo">Block</span></div></div><span id="7557"></span><div id="comment-7557" class="comment"><div id="post-7557-score" class="comment-score"></div><div class="comment-text"><p>Thanks. One program/contract at another site wrote a dll which alows them to capture mesages specific to that system/program(cool!). He's concerned his dll won't work under the new wireshark, but now that we realize all that's needed is a new WinPcap, his dll might work afterall. I plan to obtain his dll and test it later today.</p></div><div id="comment-7557-info" class="comment-info"><span class="comment-age">(22 Nov '11, 08:50)</span> <span class="comment-user userinfo">Block</span></div></div><span id="7559"></span><div id="comment-7559" class="comment"><div id="post-7559-score" class="comment-score"></div><div class="comment-text"><p>(I converted your "answers" to "comments" see the FAQ for details)</p><p>Unfortunately the DLL issue will not be solved by a different version of WinPcap. The interface for plugins (assuming with a DLL you mean a plugin) differs from Wireshark version to version. The code needs to be re-compiled for every new major version of Wireshark. The best thing to do is ask the provider of the plugin (DLL) to provide one compiled for the latest version of Wireshark.</p></div><div id="comment-7559-info" class="comment-info"><span class="comment-age">(22 Nov '11, 09:39)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="7561"></span><div id="comment-7561" class="comment"><div id="post-7561-score" class="comment-score"></div><div class="comment-text"><p>A different version of WinPcap "solves" the DLL issue by letting them run older versions of Wireshark and still be able to capture traffic on Windows 7. The ideal would be to have the DLL built for the latest version of Wireshark, so you can get later versions if they include useful features or bug fixes.</p></div><div id="comment-7561-info" class="comment-info"><span class="comment-age">(22 Nov '11, 09:48)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-7548" class="comment-tools"></div><div class="clear"></div><div id="comment-7548-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

