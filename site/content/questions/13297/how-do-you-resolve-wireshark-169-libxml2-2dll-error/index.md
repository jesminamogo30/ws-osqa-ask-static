+++
type = "question"
title = "how do you resolve Wireshark 1.6.9 libxml2-2.dll error?"
description = '''After installing Wireshark 1.6.9 and trying to launch, I get &quot;This application has failed to start because libxml2-2.dll was not found. Re-installing the application may fix this problem.&quot; A re-install does not resolve the issue. Re-installation of Wireshark 1.6.8 works fine.'''
date = "2012-08-01T12:23:00Z"
lastmod = "2012-08-01T13:30:00Z"
weight = 13297
keywords = [ "libxml2-2.dll" ]
aliases = [ "/questions/13297" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how do you resolve Wireshark 1.6.9 libxml2-2.dll error?](/questions/13297/how-do-you-resolve-wireshark-169-libxml2-2dll-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13297-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13297-score" class="post-score" title="current number of votes">0</div><span id="post-13297-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After installing Wireshark 1.6.9 and trying to launch, I get "This application has failed to start because libxml2-2.dll was not found. Re-installing the application may fix this problem." A re-install does not resolve the issue. Re-installation of Wireshark 1.6.8 works fine.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-libxml2-2.dll" rel="tag" title="see questions tagged &#39;libxml2-2.dll&#39;">libxml2-2.dll</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Aug '12, 12:23</strong></p><img src="https://secure.gravatar.com/avatar/3f6050f748452cfef9b6e0f00ca5391b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Macktheknife&#39;s gravatar image" /><p><span>Macktheknife</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Macktheknife has no accepted answers">0%</span></p></div></div><div id="comments-container-13297" class="comments-container"></div><div id="comment-tools-13297" class="comment-tools"></div><div class="clear"></div><div id="comment-13297-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13298"></span>

<div id="answer-container-13298" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13298-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13298-score" class="post-score" title="current number of votes">1</div><span id="post-13298-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is an issue in the 1.6.9 build, 1.6.10 will be released to fix this. See <a href="http://www.wireshark.org/lists/wireshark-dev/201207/msg00235.html">this</a> mailing list message for info, and you can download the 1.6.10 pre-release <a href="http://www.wireshark.org/download/prerelease/">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Aug '12, 13:30</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-13298" class="comments-container"></div><div id="comment-tools-13298" class="comment-tools"></div><div class="clear"></div><div id="comment-13298-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

