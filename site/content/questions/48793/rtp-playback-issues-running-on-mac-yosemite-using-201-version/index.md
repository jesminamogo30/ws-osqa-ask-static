+++
type = "question"
title = "RTP playback issues running on MAC Yosemite using 2.01 version"
description = '''The RTP playback is extremely slow when running on the 2.01 version on MAC Yosemite with 2.01 version. RTP playback also occasionally causes wireshark to crash.'''
date = "2016-01-02T09:44:00Z"
lastmod = "2016-01-02T09:44:00Z"
weight = 48793
keywords = [ "playback", "rtp" ]
aliases = [ "/questions/48793" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [RTP playback issues running on MAC Yosemite using 2.01 version](/questions/48793/rtp-playback-issues-running-on-mac-yosemite-using-201-version)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48793-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48793-score" class="post-score" title="current number of votes">0</div><span id="post-48793-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The RTP playback is extremely slow when running on the 2.01 version on MAC Yosemite with 2.01 version. RTP playback also occasionally causes wireshark to crash.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-playback" rel="tag" title="see questions tagged &#39;playback&#39;">playback</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jan '16, 09:44</strong></p><img src="https://secure.gravatar.com/avatar/94efad9ad4ab87f0976c01a72f80f752?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pooch%20dad&#39;s gravatar image" /><p><span>pooch dad</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pooch dad has no accepted answers">0%</span></p></div></div><div id="comments-container-48793" class="comments-container"></div><div id="comment-tools-48793" class="comment-tools"></div><div class="clear"></div><div id="comment-48793-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

