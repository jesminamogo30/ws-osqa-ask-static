+++
type = "question"
title = "Can only see a bit of iphone traffic, but can see all macbook traffic on third computer"
description = '''0 down vote favorite I have three devices on a wireless network. One is a linux desktop, which is running wireshark, and I am trying to use the desktop to monitor the wireless traffic on my network. The other two devices on the network are an iphone and a macbook pro. I am able to use the desktop to...'''
date = "2013-09-26T00:01:00Z"
lastmod = "2013-09-26T00:01:00Z"
weight = 25258
keywords = [ "wireless", "sniffing", "packet" ]
aliases = [ "/questions/25258" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can only see a bit of iphone traffic, but can see all macbook traffic on third computer](/questions/25258/can-only-see-a-bit-of-iphone-traffic-but-can-see-all-macbook-traffic-on-third-computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25258-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25258-score" class="post-score" title="current number of votes">0</div><span id="post-25258-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>0 down vote favorite</p><p>I have three devices on a wireless network. One is a linux desktop, which is running wireshark, and I am trying to use the desktop to monitor the wireless traffic on my network. The other two devices on the network are an iphone and a macbook pro.</p><p>I am able to use the desktop to monitor traffic going between the router and the macbook pro. This leads be to believe that I successfully put the wireless interface on the desktop into promiscuous mode. I am also able to see the traffic between the iphone if I make the iphone check what networks are available, but normal traffic doesn't appear at all.</p><p>I thought of a number of explanations, but none of them make sense.</p><p>a) The iphone 4s only has the ability to communicate on 802.11N wireless on 2.4 GHz, so perhaps this is why I can't see any of its traffic. This, however, doesn't make sense, because if I was only looking for 5 GHz traffic, I wouldn't expect to be able to see any traffic originating from the iphone.</p><p>b) The iphone is using the cellular network. This doesn't make sense either, since I have the iphone in airplane mode connected to the wireless network only.</p><p>c) The iphone is only using channels that I am not monitoring. This doesn't make sense either, because if channel hopping really was the problem, why would the macbook pro's traffic be visible, but none the iphone's traffic not visible? This would imply that the macbook pro channel hops, while the iphone doesn't, but that the iphone goes back to the channel I happen to be monitoring when it wants to check what networks there are.</p><p>Does anyone have any ideas as to why only certain traffic from the iphone is visible?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '13, 00:01</strong></p><img src="https://secure.gravatar.com/avatar/2d0d9f04cee7e80bacff9760e1b024f3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmw&#39;s gravatar image" /><p><span>cmw</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmw has no accepted answers">0%</span></p></div></div><div id="comments-container-25258" class="comments-container"></div><div id="comment-tools-25258" class="comment-tools"></div><div class="clear"></div><div id="comment-25258-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

