+++
type = "question"
title = "Coverting G722 payload to wav"
description = '''Is there a tools to convert G722 rtp to .wav file??'''
date = "2013-10-31T20:54:00Z"
lastmod = "2013-11-01T05:26:00Z"
weight = 26607
keywords = [ "losl" ]
aliases = [ "/questions/26607" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Coverting G722 payload to wav](/questions/26607/coverting-g722-payload-to-wav)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26607-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26607-score" class="post-score" title="current number of votes">0</div><span id="post-26607-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a tools to convert G722 rtp to .wav file??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-losl" rel="tag" title="see questions tagged &#39;losl&#39;">losl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '13, 20:54</strong></p><img src="https://secure.gravatar.com/avatar/435f7574e1c2108d7fa94ef5c0222ac0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="skorpios78&#39;s gravatar image" /><p><span>skorpios78</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="skorpios78 has no accepted answers">0%</span></p></div></div><div id="comments-container-26607" class="comments-container"></div><div id="comment-tools-26607" class="comment-tools"></div><div class="clear"></div><div id="comment-26607-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26610"></span>

<div id="answer-container-26610" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26610-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26610-score" class="post-score" title="current number of votes">0</div><span id="post-26610-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is there a <strong>tools to convert G722</strong> rtp to .wav file??</p></blockquote><p>well, actually not a Wireshark question....</p><p>However, as we have had similar questions, here is what you can do</p><ul><li>extract the RTP payload: <code>http://ask.wireshark.org/questions/21193/extracting-rtp-payload-and-dumping-to-a-ts-file</code><br />
</li><li>convert the G722 data to whatever you need with <a href="http://sox.sourceforge.net/">sox</a>: <code>http://sox.sourceforge.net/</code></li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Nov '13, 05:26</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-26610" class="comments-container"></div><div id="comment-tools-26610" class="comment-tools"></div><div class="clear"></div><div id="comment-26610-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

