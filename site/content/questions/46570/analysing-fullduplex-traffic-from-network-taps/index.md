+++
type = "question"
title = "Analysing fullduplex traffic from network TAPs"
description = '''Hello Forum i would like to analyze raw-traffic sent/forwarded from a fullduplex-TAP (eg IXIOS, Niagara or DATACOM). Those TAPs do forward the monitored traffic on 2 links to the analyzing station (equipped with a dual interface network card, e.g. Endace DAG). Question: Does Wireshark support such R...'''
date = "2015-10-15T09:13:00Z"
lastmod = "2015-10-15T09:56:00Z"
weight = 46570
keywords = [ "monitoring", "tap", "wireshark" ]
aliases = [ "/questions/46570" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Analysing fullduplex traffic from network TAPs](/questions/46570/analysing-fullduplex-traffic-from-network-taps)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46570-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46570-score" class="post-score" title="current number of votes">0</div><span id="post-46570-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Forum</p><p>i would like to analyze raw-traffic sent/forwarded from a fullduplex-TAP (eg IXIOS, Niagara or DATACOM). Those TAPs do forward the monitored traffic on 2 links to the analyzing station (equipped with<br />
a dual interface network card, e.g. Endace DAG).</p><p>Question: Does Wireshark support such RAW-traffic?</p><p>Thank you very much for every feedback!</p><p>Joe</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-monitoring" rel="tag" title="see questions tagged &#39;monitoring&#39;">monitoring</span> <span class="post-tag tag-link-tap" rel="tag" title="see questions tagged &#39;tap&#39;">tap</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Oct '15, 09:13</strong></p><img src="https://secure.gravatar.com/avatar/c08acf577aad3b14e932ee8f48cf7d20?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joseph123&#39;s gravatar image" /><p><span>joseph123</span><br />
<span class="score" title="11 reputation points">11</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="bronze">●</span><span class="badgecount">12</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joseph123 has no accepted answers">0%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Oct '15, 09:14</strong> </span></p></div></div><div id="comments-container-46570" class="comments-container"></div><div id="comment-tools-46570" class="comment-tools"></div><div class="clear"></div><div id="comment-46570-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="46572"></span>

<div id="answer-container-46572" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46572-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46572-score" class="post-score" title="current number of votes">0</div><span id="post-46572-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>First you can start here: <a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">https://wiki.wireshark.org/CaptureSetup/Ethernet</a></p><pre><code>Does Wireshark support such raw traffic?</code></pre>If you mean with this question: Is it able to capture simultaneously on more than one interface with Wireshark. Then the answer is yes. I think since Version 1.8 it is possible.</div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Oct '15, 09:56</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></div></div><div id="comments-container-46572" class="comments-container"></div><div id="comment-tools-46572" class="comment-tools"></div><div class="clear"></div><div id="comment-46572-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

