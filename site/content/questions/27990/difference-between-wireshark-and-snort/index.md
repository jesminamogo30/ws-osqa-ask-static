+++
type = "question"
title = "Difference between Wireshark and Snort"
description = '''Hi All, I am very new to security field and exploring various tools. i came across two great tools Wireshark and Snort... i found we can set filters in both tools. Can any one please explain the difference between both the tools... Thank you in advance.. '''
date = "2013-12-10T22:08:00Z"
lastmod = "2013-12-11T01:02:00Z"
weight = 27990
keywords = [ "snort", "wireshark" ]
aliases = [ "/questions/27990" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Difference between Wireshark and Snort](/questions/27990/difference-between-wireshark-and-snort)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27990-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27990-score" class="post-score" title="current number of votes">0</div><span id="post-27990-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I am very new to security field and exploring various tools. i came across two great tools Wireshark and Snort... i found we can set filters in both tools. Can any one please explain the difference between both the tools...</p><p>Thank you in advance..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-snort" rel="tag" title="see questions tagged &#39;snort&#39;">snort</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Dec '13, 22:08</strong></p><img src="https://secure.gravatar.com/avatar/904e19785874be39705426e578c4c029?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aditi&#39;s gravatar image" /><p><span>Aditi</span><br />
<span class="score" title="16 reputation points">16</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aditi has no accepted answers">0%</span></p></div></div><div id="comments-container-27990" class="comments-container"></div><div id="comment-tools-27990" class="comment-tools"></div><div class="clear"></div><div id="comment-27990-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27993"></span>

<div id="answer-container-27993" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27993-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27993-score" class="post-score" title="current number of votes">0</div><span id="post-27993-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark reads packets and decodes them in "human readable format" for you to inspect whatever it is that happens in those packets.</p><p>Snort is a intrusion detection systems, which scans for malicious (or other) patterns in packets it sees, kind of like a Virus Scanner, and alerts if it sees something.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Dec '13, 01:02</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-27993" class="comments-container"></div><div id="comment-tools-27993" class="comment-tools"></div><div class="clear"></div><div id="comment-27993-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

