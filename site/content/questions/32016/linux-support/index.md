+++
type = "question"
title = "Linux Support"
description = '''Is there a version of WireShark for Ubuntu Linux?'''
date = "2014-04-20T14:51:00Z"
lastmod = "2014-04-20T15:10:00Z"
weight = 32016
keywords = [ "linux-support", "ubuntu" ]
aliases = [ "/questions/32016" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Linux Support](/questions/32016/linux-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32016-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32016-score" class="post-score" title="current number of votes">0</div><span id="post-32016-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a version of WireShark for Ubuntu Linux?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-linux-support" rel="tag" title="see questions tagged &#39;linux-support&#39;">linux-support</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Apr '14, 14:51</strong></p><img src="https://secure.gravatar.com/avatar/1404397eb3b2239f2104114a1bdef5f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aryncavage322&#39;s gravatar image" /><p><span>aryncavage322</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aryncavage322 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Apr '14, 14:57</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-32016" class="comments-container"></div><div id="comment-tools-32016" class="comment-tools"></div><div class="clear"></div><div id="comment-32016-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32017"></span>

<div id="answer-container-32017" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32017-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32017-score" class="post-score" title="current number of votes">1</div><span id="post-32017-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure, just install it from the standard Ubuntu repository</p><blockquote><p>sudo apt-get install wireshark<br />
</p></blockquote><p>and</p><blockquote><p>sudo apt-get install tshark</p></blockquote><p>Please also read the answer to the following question, for further commands you might need to run on Ubuntu (especially the <strong>setcap</strong> command!).</p><blockquote><p><a href="http://ask.wireshark.org/questions/16343/install-wireshark-on-ubuntu">http://ask.wireshark.org/questions/16343/install-wireshark-on-ubuntu</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Apr '14, 14:55</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Apr '14, 15:00</strong> </span></p></div></div><div id="comments-container-32017" class="comments-container"><span id="32018"></span><div id="comment-32018" class="comment"><div id="post-32018-score" class="comment-score"></div><div class="comment-text"><p>HI Thanks, I am new to the whole thing can you please go over what that is or give a link?</p></div><div id="comment-32018-info" class="comment-info"><span class="comment-age">(20 Apr '14, 15:00)</span> <span class="comment-user userinfo">aryncavage322</span></div></div><span id="32019"></span><div id="comment-32019" class="comment"><div id="post-32019-score" class="comment-score"></div><div class="comment-text"><p>???? a link to what?</p></div><div id="comment-32019-info" class="comment-info"><span class="comment-age">(20 Apr '14, 15:01)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="32020"></span><div id="comment-32020" class="comment"><div id="post-32020-score" class="comment-score"></div><div class="comment-text"><p>Nevermind, I didnt understand what the ubuntu repository was for a second.</p></div><div id="comment-32020-info" class="comment-info"><span class="comment-age">(20 Apr '14, 15:02)</span> <span class="comment-user userinfo">aryncavage322</span></div></div><span id="32021"></span><div id="comment-32021" class="comment"><div id="post-32021-score" class="comment-score"></div><div class="comment-text"><blockquote><p><a href="https://help.ubuntu.com/community/InstallingSoftware">https://help.ubuntu.com/community/InstallingSoftware</a><br />
<a href="https://help.ubuntu.com/12.04/serverguide/apt-get.html">https://help.ubuntu.com/12.04/serverguide/apt-get.html</a></p></blockquote></div><div id="comment-32021-info" class="comment-info"><span class="comment-age">(20 Apr '14, 15:10)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-32017" class="comment-tools"></div><div class="clear"></div><div id="comment-32017-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

