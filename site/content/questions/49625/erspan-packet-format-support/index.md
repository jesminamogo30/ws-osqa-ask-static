+++
type = "question"
title = "ERSPAN Packet Format Support"
description = '''Hello, Does the current (or a planned future) release of Wireshark fully support all ERSPAN packet formats described in the Internet Draft? https://www.ietf.org/id/draft-foschiano-erspan-01.txt Thanks.'''
date = "2016-01-29T07:40:00Z"
lastmod = "2016-02-01T06:49:00Z"
weight = 49625
keywords = [ "erspan" ]
aliases = [ "/questions/49625" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ERSPAN Packet Format Support](/questions/49625/erspan-packet-format-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49625-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49625-score" class="post-score" title="current number of votes">0</div><span id="post-49625-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>Does the current (or a planned future) release of Wireshark fully support all ERSPAN packet formats described in the Internet Draft?</p><p><a href="https://www.ietf.org/id/draft-foschiano-erspan-01.txt">https://www.ietf.org/id/draft-foschiano-erspan-01.txt</a></p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-erspan" rel="tag" title="see questions tagged &#39;erspan&#39;">erspan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jan '16, 07:40</strong></p><img src="https://secure.gravatar.com/avatar/31bb8ff6cb55ec8bbc5fcb099463b2d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mef&#39;s gravatar image" /><p><span>mef</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mef has no accepted answers">0%</span></p></div></div><div id="comments-container-49625" class="comments-container"><span id="49689"></span><div id="comment-49689" class="comment"><div id="post-49689-score" class="comment-score"></div><div class="comment-text"><p>Hi an interesting discussion about ERSPAN and Wireshark can be found here: <a href="https://ask.wireshark.org/questions/46740/strip-erspan-header-from-output">https://ask.wireshark.org/questions/46740/strip-erspan-header-from-output</a></p></div><div id="comment-49689-info" class="comment-info"><span class="comment-age">(01 Feb '16, 06:49)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-49625" class="comment-tools"></div><div class="clear"></div><div id="comment-49625-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49676"></span>

<div id="answer-container-49676" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49676-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49676-score" class="post-score" title="current number of votes">1</div><span id="post-49676-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you file an enhancement <a href="https://bugs.wireshark.org">bug</a> with as much info and sample capture file, it may well be picked up by someone willing to invest their time in it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Feb '16, 02:31</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-49676" class="comments-container"></div><div id="comment-tools-49676" class="comment-tools"></div><div class="clear"></div><div id="comment-49676-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

