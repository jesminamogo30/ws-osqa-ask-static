+++
type = "question"
title = "Normal Wireshark Home page is not showing"
description = '''I have installed wireshark but when i start it normal home page is not showing, development build is written on right side of the page , please help me somebody.'''
date = "2016-02-11T12:35:00Z"
lastmod = "2016-02-12T04:26:00Z"
weight = 50112
keywords = [ "wireshark_crashed", "wireshark" ]
aliases = [ "/questions/50112" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Normal Wireshark Home page is not showing](/questions/50112/normal-wireshark-home-page-is-not-showing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50112-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50112-score" class="post-score" title="current number of votes">0</div><span id="post-50112-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have installed wireshark but when i start it normal home page is not showing, development build is written on right side of the page , please help me somebody.<img src="http://" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark_crashed" rel="tag" title="see questions tagged &#39;wireshark_crashed&#39;">wireshark_crashed</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Feb '16, 12:35</strong></p><img src="https://secure.gravatar.com/avatar/35ecc62a2ab3aacdc70593adf93ccc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Sameer%20A%20Athaley&#39;s gravatar image" /><p><span>Sameer A Ath...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Sameer A Athaley has no accepted answers">0%</span></p></img></div></div><div id="comments-container-50112" class="comments-container"></div><div id="comment-tools-50112" class="comment-tools"></div><div class="clear"></div><div id="comment-50112-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50133"></span>

<div id="answer-container-50133" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50133-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50133-score" class="post-score" title="current number of votes">0</div><span id="post-50133-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not sure to understand what you call "normal Wireshark home page", but it looks like you installed a development version, and not an official one.</p><p>You can find official (non development) Wireshark versions here: <a href="https://www.wireshark.org/#download">https://www.wireshark.org/#download</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Feb '16, 04:26</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-50133" class="comments-container"></div><div id="comment-tools-50133" class="comment-tools"></div><div class="clear"></div><div id="comment-50133-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

