+++
type = "question"
title = "Temp folder not used"
description = '''Hi, Running ubuntu 16.04, wireshark-qt I set the env var TMPDIR to /home/me/tmp. This correctly shows up in the About, but the files are still created in /tmp.  Any ideas? Really looks like a bug.'''
date = "2016-12-06T06:01:00Z"
lastmod = "2016-12-07T10:30:00Z"
weight = 57899
keywords = [ "temp" ]
aliases = [ "/questions/57899" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Temp folder not used](/questions/57899/temp-folder-not-used)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57899-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57899-score" class="post-score" title="current number of votes">0</div><span id="post-57899-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Running ubuntu 16.04, wireshark-qt I set the env var TMPDIR to /home/me/tmp. This correctly shows up in the About, but the files are still created in /tmp.</p><p>Any ideas? Really looks like a bug.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-temp" rel="tag" title="see questions tagged &#39;temp&#39;">temp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Dec '16, 06:01</strong></p><img src="https://secure.gravatar.com/avatar/8ea008e691ded115bd4eaf07313f96bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="maddox&#39;s gravatar image" /><p><span>maddox</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="maddox has no accepted answers">0%</span></p></div></div><div id="comments-container-57899" class="comments-container"><span id="57916"></span><div id="comment-57916" class="comment"><div id="post-57916-score" class="comment-score"></div><div class="comment-text"><p>To which files are you referring when you say "the files"?</p></div><div id="comment-57916-info" class="comment-info"><span class="comment-age">(06 Dec '16, 16:20)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="57917"></span><div id="comment-57917" class="comment"><div id="post-57917-score" class="comment-score"></div><div class="comment-text"><p>And which version of Wireshark is this?</p></div><div id="comment-57917-info" class="comment-info"><span class="comment-age">(06 Dec '16, 16:20)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="57935"></span><div id="comment-57935" class="comment"><div id="post-57935-score" class="comment-score"></div><div class="comment-text"><p>Temp files such as: wireshark_pcapng_eth3_20161207142817_IqfZIR version says Version 2.0.2 (SVN Rev Unknown from unknown)</p><p>Same issue with GTK+ or QT versions.</p></div><div id="comment-57935-info" class="comment-info"><span class="comment-age">(07 Dec '16, 05:30)</span> <span class="comment-user userinfo">maddox</span></div></div><span id="57942"></span><div id="comment-57942" class="comment"><div id="post-57942-score" class="comment-score"></div><div class="comment-text"><p>Was this a version you built yourself, or was it the version from the official Ubuntu package repository?</p></div><div id="comment-57942-info" class="comment-info"><span class="comment-age">(07 Dec '16, 10:30)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-57899" class="comment-tools"></div><div class="clear"></div><div id="comment-57899-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

