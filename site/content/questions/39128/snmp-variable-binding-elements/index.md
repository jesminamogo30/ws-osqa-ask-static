+++
type = "question"
title = "snmp variable binding elements"
description = '''How should the &#x27;snmp.VarBind_element&#x27; display filter be used? I have a frame that has 17 variable-bindings and I was hoping this filter would allow me to look at each one individually but it produces no results. Are there any display options that returns the data for each variable binding? (I am usi...'''
date = "2015-01-14T09:20:00Z"
lastmod = "2015-01-14T09:20:00Z"
weight = 39128
keywords = [ "variable", "binding", "elements", "snmp" ]
aliases = [ "/questions/39128" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [snmp variable binding elements](/questions/39128/snmp-variable-binding-elements)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39128-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39128-score" class="post-score" title="current number of votes">0</div><span id="post-39128-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How should the 'snmp.VarBind_element' display filter be used? I have a frame that has 17 variable-bindings and I was hoping this filter would allow me to look at each one individually but it produces no results. Are there any display options that returns the data for each variable binding? (I am using tshark)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-variable" rel="tag" title="see questions tagged &#39;variable&#39;">variable</span> <span class="post-tag tag-link-binding" rel="tag" title="see questions tagged &#39;binding&#39;">binding</span> <span class="post-tag tag-link-elements" rel="tag" title="see questions tagged &#39;elements&#39;">elements</span> <span class="post-tag tag-link-snmp" rel="tag" title="see questions tagged &#39;snmp&#39;">snmp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '15, 09:20</strong></p><img src="https://secure.gravatar.com/avatar/378dec08c639d6c2c2979b244af9660e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lp4968&#39;s gravatar image" /><p><span>lp4968</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lp4968 has no accepted answers">0%</span></p></div></div><div id="comments-container-39128" class="comments-container"></div><div id="comment-tools-39128" class="comment-tools"></div><div class="clear"></div><div id="comment-39128-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

