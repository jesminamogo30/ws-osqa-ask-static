+++
type = "question"
title = "When message decode in rrc level ,then, the code stream is not been view."
description = '''When message decode in rrc level ,then, the code stream is not been view. condition: WINOWS XP,wireshark-1.8.3. the wonder is that could not been seen in UBUNTU. who can help how to fix the problem?  thanks.'''
date = "2013-01-23T22:23:00Z"
lastmod = "2013-01-28T08:37:00Z"
weight = 17917
keywords = [ "code", "rrc", "stream" ]
aliases = [ "/questions/17917" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [When message decode in rrc level ,then, the code stream is not been view.](/questions/17917/when-message-decode-in-rrc-level-then-the-code-stream-is-not-been-view)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17917-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17917-score" class="post-score" title="current number of votes">0</div><span id="post-17917-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When message decode in rrc level ,then, the code stream is not been view. condition: WINOWS XP,wireshark-1.8.3.</p><p>the wonder is that could not been seen in UBUNTU.</p><p>who can help how to fix the problem? thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-code" rel="tag" title="see questions tagged &#39;code&#39;">code</span> <span class="post-tag tag-link-rrc" rel="tag" title="see questions tagged &#39;rrc&#39;">rrc</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jan '13, 22:23</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-17917" class="comments-container"><span id="18002"></span><div id="comment-18002" class="comment"><div id="post-18002-score" class="comment-score"></div><div class="comment-text"><p>The question is to vauge to answer. Does it work in Ubuntu but not in XP or vice versa. If so is it the same verion with the same preference settings?</p></div><div id="comment-18002-info" class="comment-info"><span class="comment-age">(28 Jan '13, 08:37)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-17917" class="comment-tools"></div><div class="clear"></div><div id="comment-17917-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

