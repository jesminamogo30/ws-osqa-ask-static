+++
type = "question"
title = "USB to Ethernet Adapter and Promiscuous Mode"
description = '''Hello Is anyone aware of any USB to Ethernet adapters whose chipset/Windows drivers can operate in promiscuous mode? I need to replace my aging brick of a laptop and my workplace has given me a choice between a cheap laptop with a screen resolution worse than my old one (old is 1280x1024 and new wou...'''
date = "2015-11-04T02:02:00Z"
lastmod = "2015-11-05T08:41:00Z"
weight = 47218
keywords = [ "promiscuous", "ethernet", "adapter", "usb" ]
aliases = [ "/questions/47218" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [USB to Ethernet Adapter and Promiscuous Mode](/questions/47218/usb-to-ethernet-adapter-and-promiscuous-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47218-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47218-score" class="post-score" title="current number of votes">0</div><span id="post-47218-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>Is anyone aware of any USB to Ethernet adapters whose chipset/Windows drivers can operate in promiscuous mode?</p><p>I need to replace my aging brick of a laptop and my workplace has given me a choice between a cheap laptop with a screen resolution worse than my old one (old is 1280x1024 and new would be 1366x768) or a Microsoft Surface Pro. The Surface Pro has the highest screen res and is the most portable so I'd like to give that a go unfortunately it also has no ethernet port.</p><p>Is promiscuous mode even that necessary for packet sniffing? A basic question I know but I've never had problems capturing what I've needed using whatever computer I've had to hand at the time.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-promiscuous" rel="tag" title="see questions tagged &#39;promiscuous&#39;">promiscuous</span> <span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-adapter" rel="tag" title="see questions tagged &#39;adapter&#39;">adapter</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Nov '15, 02:02</strong></p><img src="https://secure.gravatar.com/avatar/4cba07980be0515c7e7df3276e1ef91f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="llamatron&#39;s gravatar image" /><p><span>llamatron</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="llamatron has no accepted answers">0%</span></p></div></div><div id="comments-container-47218" class="comments-container"></div><div id="comment-tools-47218" class="comment-tools"></div><div class="clear"></div><div id="comment-47218-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="47219"></span>

<div id="answer-container-47219" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47219-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47219-score" class="post-score" title="current number of votes">1</div><span id="post-47219-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'm using these, and they work fine for me in promiscuous mode:</p><p><a href="http://www.amazon.com/Network-Adapter-Anker-Ethernet-Supporting/dp/B00NOP70EC/ref=sr_1_2?ie=UTF8&amp;qid=1446631459&amp;sr=8-2&amp;keywords=anker+ethernet+usb3">http://www.amazon.com/Network-Adapter-Anker-Ethernet-Supporting/dp/B00NOP70EC/ref=sr_1_2?ie=UTF8&amp;qid=1446631459&amp;sr=8-2&amp;keywords=anker+ethernet+usb3</a></p><p>I have to say I'm not owning a Surface Pro, but with my Win8.1 Ultrabook (which has no Ethernet ports) they work without a problem.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Nov '15, 02:05</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Nov '15, 02:07</strong> </span></p></div></div><div id="comments-container-47219" class="comments-container"><span id="47230"></span><div id="comment-47230" class="comment"><div id="post-47230-score" class="comment-score"></div><div class="comment-text"><p>Well that's interesting. I was originally looking at this device which is also by Anker:</p><p><a href="http://www.ianker.com/product/68ANHUB-U3E1A">http://www.ianker.com/product/68ANHUB-U3E1A</a></p><p>It uses Realtek's RTL8153 chipset. I got in touch with Realtek and asked the question and they replied saying no, it's not capable of running in promiscuous mode. I guess the device you've linked to uses a different ethernet chipset.</p><p>Thanks</p></div><div id="comment-47230-info" class="comment-info"><span class="comment-age">(04 Nov '15, 04:57)</span> <span class="comment-user userinfo">llamatron</span></div></div><span id="47235"></span><div id="comment-47235" class="comment"><div id="post-47235-score" class="comment-score"></div><div class="comment-text"><p>Very interesting - I have that exact USB3 hub, too, and just tested it - it works fine in promiscuous mode on my HP Switch SPAN port. Saw lots of traffic (with all protocol bindings disabled), so I'd say it works (using Wireshark 2.0.0rc2).</p><p><code>dumpcap -D</code> also lists the adapter.</p></div><div id="comment-47235-info" class="comment-info"><span class="comment-age">(04 Nov '15, 06:49)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="47285"></span><div id="comment-47285" class="comment"><div id="post-47285-score" class="comment-score"></div><div class="comment-text"><p>I now also tested the other Anker adapter (with the aluminum body) - this one DOES NOT work!</p><p><a href="http://www.amazon.com/Anker-Unibody-Aluminum-Ethernet-Supporting/dp/B00PC0H9IE/ref=sr_1_2?ie=UTF8&amp;qid=1446725700&amp;sr=8-2&amp;keywords=usb3+anker+ethernet">http://www.amazon.com/Anker-Unibody-Aluminum-Ethernet-Supporting/dp/B00PC0H9IE/ref=sr_1_2?ie=UTF8&amp;qid=1446725700&amp;sr=8-2&amp;keywords=usb3+anker+ethernet</a></p></div><div id="comment-47285-info" class="comment-info"><span class="comment-age">(05 Nov '15, 04:15)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="47292"></span><div id="comment-47292" class="comment"><div id="post-47292-score" class="comment-score"></div><div class="comment-text"><p>This one works for me:</p><blockquote><p><a href="http://www.amazon.com/Digitus-DN-3023-Gigabit-Ethernet-Adapter/dp/B008Y6SPN6">http://www.amazon.com/Digitus-DN-3023-Gigabit-Ethernet-Adapter/dp/B008Y6SPN6</a></p></blockquote><p>I uses an Asix AX88179 chipset</p><blockquote><p><a href="http://www.asix.com.tw/products.php?op=pItemdetail&amp;PItemID=131;71;112">http://www.asix.com.tw/products.php?op=pItemdetail&amp;PItemID=131;71;112</a></p></blockquote></div><div id="comment-47292-info" class="comment-info"><span class="comment-age">(05 Nov '15, 08:41)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-47219" class="comment-tools"></div><div class="clear"></div><div id="comment-47219-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

