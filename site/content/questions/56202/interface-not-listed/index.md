+++
type = "question"
title = "Interface Not Listed"
description = '''I have just updated my Wireshark to version 2.2.1 and I do not have my Ethernet Interface listed. I have gone to Capture &amp;gt; Options &amp;gt; Manage Interfaces to see if it is listed but it does not find the Interface. I have tried to refresh my interfaces and still does not find it.'''
date = "2016-10-06T10:53:00Z"
lastmod = "2016-10-06T11:03:00Z"
weight = 56202
keywords = [ "interface", "ethernet", "list" ]
aliases = [ "/questions/56202" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Interface Not Listed](/questions/56202/interface-not-listed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56202-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56202-score" class="post-score" title="current number of votes">0</div><span id="post-56202-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have just updated my Wireshark to version 2.2.1 and I do not have my Ethernet Interface listed. I have gone to Capture &gt; Options &gt; Manage Interfaces to see if it is listed but it does not find the Interface. I have tried to refresh my interfaces and still does not find it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-list" rel="tag" title="see questions tagged &#39;list&#39;">list</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '16, 10:53</strong></p><img src="https://secure.gravatar.com/avatar/449f5a4e0696d8e7d148e40ea5f16613?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MiktaNaji&#39;s gravatar image" /><p><span>MiktaNaji</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MiktaNaji has no accepted answers">0%</span></p></div></div><div id="comments-container-56202" class="comments-container"><span id="56203"></span><div id="comment-56203" class="comment"><div id="post-56203-score" class="comment-score"></div><div class="comment-text"><p>What is your OS?</p><p>Can you post the contents of Help -&gt; About Wireshark?</p></div><div id="comment-56203-info" class="comment-info"><span class="comment-age">(06 Oct '16, 11:03)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-56202" class="comment-tools"></div><div class="clear"></div><div id="comment-56202-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

