+++
type = "question"
title = "Missing Statistics &gt; Summary in Version 2.0.1"
description = '''The Statistics &amp;gt; Summary option seems to be missing in Version 2.0.1. It was the fastest way to get an Avg. bytes/sec on a capture. Am I missing something.'''
date = "2016-02-19T10:11:00Z"
lastmod = "2016-02-19T10:17:00Z"
weight = 50347
keywords = [ "statistics", "summary" ]
aliases = [ "/questions/50347" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Missing Statistics &gt; Summary in Version 2.0.1](/questions/50347/missing-statistics-summary-in-version-201)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50347-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50347-score" class="post-score" title="current number of votes">0</div><span id="post-50347-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The Statistics &gt; Summary option seems to be missing in Version 2.0.1. It was the fastest way to get an Avg. bytes/sec on a capture. Am I missing something.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-summary" rel="tag" title="see questions tagged &#39;summary&#39;">summary</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '16, 10:11</strong></p><img src="https://secure.gravatar.com/avatar/a91c47d46c05f7117e9dfdc2e004f58b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="EvilGnome6&#39;s gravatar image" /><p><span>EvilGnome6</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="EvilGnome6 has no accepted answers">0%</span></p></div></div><div id="comments-container-50347" class="comments-container"></div><div id="comment-tools-50347" class="comment-tools"></div><div class="clear"></div><div id="comment-50347-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50348"></span>

<div id="answer-container-50348" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50348-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50348-score" class="post-score" title="current number of votes">2</div><span id="post-50348-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It was renamed to <em>Statistics → Capture File Properties</em>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Feb '16, 10:17</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-50348" class="comments-container"></div><div id="comment-tools-50348" class="comment-tools"></div><div class="clear"></div><div id="comment-50348-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

