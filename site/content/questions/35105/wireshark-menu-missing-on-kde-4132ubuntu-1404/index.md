+++
type = "question"
title = "Wireshark menu missing on KDE 4.13.2/Ubuntu 14.04"
description = '''The top tool bar is missing. The one with File, Edit, View, and etc. I&#x27;m using KDE Desktop Environment.  Anyone know how to get the tool bar to show?'''
date = "2014-08-03T10:16:00Z"
lastmod = "2016-02-29T07:48:00Z"
weight = 35105
keywords = [ "kde", "menu", "bar", "wireshark" ]
aliases = [ "/questions/35105" ]
osqa_answers = 3
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark menu missing on KDE 4.13.2/Ubuntu 14.04](/questions/35105/wireshark-menu-missing-on-kde-4132ubuntu-1404)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35105-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35105-score" class="post-score" title="current number of votes">0</div><span id="post-35105-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The top tool bar is missing. The one with File, Edit, View, and etc. I'm using KDE Desktop Environment.</p><p>Anyone know how to get the tool bar to show?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-kde" rel="tag" title="see questions tagged &#39;kde&#39;">kde</span> <span class="post-tag tag-link-menu" rel="tag" title="see questions tagged &#39;menu&#39;">menu</span> <span class="post-tag tag-link-bar" rel="tag" title="see questions tagged &#39;bar&#39;">bar</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '14, 10:16</strong></p><img src="https://secure.gravatar.com/avatar/1c95f0f88e2aa54981443557b2ffd866?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="User28942&#39;s gravatar image" /><p><span>User28942</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="User28942 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Aug '14, 10:50</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-35105" class="comments-container"><span id="35107"></span><div id="comment-35107" class="comment"><div id="post-35107-score" class="comment-score"></div><div class="comment-text"><p>Hm... let's see. No information at all about the systems/versions involved !?!</p><p>So, I 'conclude' you are running Wireshark 1.0.3 on SuSE Linux 7.3, with KDE 0.666. Is that right?</p></div><div id="comment-35107-info" class="comment-info"><span class="comment-age">(03 Aug '14, 10:23)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="35110"></span><div id="comment-35110" class="comment"><div id="post-35110-score" class="comment-score"></div><div class="comment-text"><p>KDE 4.13.2, wireshark v1.10.6 and Ubuntu 14.04</p></div><div id="comment-35110-info" class="comment-info"><span class="comment-age">(03 Aug '14, 10:30)</span> <span class="comment-user userinfo">User28942</span></div></div><span id="35112"></span><div id="comment-35112" class="comment"><div id="post-35112-score" class="comment-score"></div><div class="comment-text"><p>The "top toolbar" is called the "menu bar". More recent versions of Ubuntu use the <a href="https://en.wikipedia.org/wiki/Unity_(user_interface)">Unity user interface</a>, at least by default, and that puts the menu bar at the top of the screen, Mac-style.</p><p>Are you saying that the Wireshark window neither has a menu bar at the top of the screen or a menu bar at the top of its window?</p><p>What do other GTK+-based applications do? (Try loading, for example, GEdit onto your machine, and try that.)</p><p>Do KDE applications put the menu bar at the top of the screen or at the top of their window on your system?</p></div><div id="comment-35112-info" class="comment-info"><span class="comment-age">(03 Aug '14, 10:49)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="35113"></span><div id="comment-35113" class="comment"><div id="post-35113-score" class="comment-score"></div><div class="comment-text"><p>KDE applications normally put them at top of the Window. I just try few other programs and seem to be having the same problem so problem seem to be a KDE problem and not problem with Wireshark. Only KDE applications are displaying the menu bar.</p></div><div id="comment-35113-info" class="comment-info"><span class="comment-age">(03 Aug '14, 10:55)</span> <span class="comment-user userinfo">User28942</span></div></div><span id="50581"></span><div id="comment-50581" class="comment"><div id="post-50581-score" class="comment-score"></div><div class="comment-text"><p>I have the same issue, but I'm using the Gnome fallback session in 14.04, not KDE. I switch my session to Unity and everything is kust peachy keen. The Wireshark menu shows up in the top bar just as expected. Go back to Gnome (metacity or compiz, same result) and there is a menu nazi in there saying "no menu for you!"</p></div><div id="comment-50581-info" class="comment-info"><span class="comment-age">(29 Feb '16, 07:47)</span> <span class="comment-user userinfo">Xtreem</span></div></div></div><div id="comment-tools-35105" class="comment-tools"></div><div class="clear"></div><div id="comment-35105-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="35116"></span>

<div id="answer-container-35116" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35116-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35116-score" class="post-score" title="current number of votes">0</div><span id="post-35116-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="User28942 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>KDE applications normally put them at top of the Window. I just try few other programs and seem to be having the same problem so problem seem to be a KDE problem and not problem with Wireshark. Only KDE applications are displaying the menu bar.</p></blockquote><p>Or an Ubuntu GTK+ problem; I'm not sure what code arranges to put the menu bar at the top of the window in Unity, but if the Ubuntu developers modified GTK+ to do that, and if that depends on having a Unity-compatible window manager, and if KWin isn't Unity-compatible, and if that means the menu bar of GTK+ applications doesn't display in KDE, I'd call that an Ubuntu problem - it's not KDE's fault that Canonical decided to Change Things.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Aug '14, 11:04</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-35116" class="comments-container"></div><div id="comment-tools-35116" class="comment-tools"></div><div class="clear"></div><div id="comment-35116-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="50582"></span>

<div id="answer-container-50582" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50582-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50582-score" class="post-score" title="current number of votes">1</div><span id="post-50582-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Actually, just found the issue. It's a problem with appmenu-qt5. <a href="https://bugs.launchpad.net/ubuntu/+source/appmenu-qt5/+bug/1307619">https://bugs.launchpad.net/ubuntu/+source/appmenu-qt5/+bug/1307619</a></p><p>uninstall appmenu-qt5 and the issue is resolved.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Feb '16, 07:48</strong></p><img src="https://secure.gravatar.com/avatar/e2d9d62d9d8995b0a5570235bcb8d5ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Xtreem&#39;s gravatar image" /><p><span>Xtreem</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Xtreem has no accepted answers">0%</span></p></div></div><div id="comments-container-50582" class="comments-container"></div><div id="comment-tools-50582" class="comment-tools"></div><div class="clear"></div><div id="comment-50582-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="35117"></span>

<div id="answer-container-35117" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35117-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35117-score" class="post-score" title="current number of votes">0</div><span id="post-35117-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds like this: <a href="http://www.adminreseau.fr/how-to-enable-the-new-kde-4-10-appmenu-title-bar-button-or-top-screen-menubar/">http://www.adminreseau.fr/how-to-enable-the-new-kde-4-10-appmenu-title-bar-button-or-top-screen-menubar/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Aug '14, 11:04</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span></p></div></div><div id="comments-container-35117" class="comments-container"></div><div id="comment-tools-35117" class="comment-tools"></div><div class="clear"></div><div id="comment-35117-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

