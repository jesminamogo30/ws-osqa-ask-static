+++
type = "question"
title = "SSDP Filter Request"
description = '''http://wiki.wireshark.org/SSDP Is there a chance that we will have an SSDP filter sometime soon?'''
date = "2014-08-24T13:14:00Z"
lastmod = "2014-08-26T07:48:00Z"
weight = 35698
keywords = [ "filter", "ssdp" ]
aliases = [ "/questions/35698" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [SSDP Filter Request](/questions/35698/ssdp-filter-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35698-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35698-score" class="post-score" title="current number of votes">0</div><span id="post-35698-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><a href="http://wiki.wireshark.org/SSDP">http://wiki.wireshark.org/SSDP</a></p><p>Is there a chance that we will have an SSDP filter sometime soon?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-ssdp" rel="tag" title="see questions tagged &#39;ssdp&#39;">ssdp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Aug '14, 13:14</strong></p><img src="https://secure.gravatar.com/avatar/5b1802a3dde015a758fb13baafb1605f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="proj964&#39;s gravatar image" /><p><span>proj964</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="proj964 has no accepted answers">0%</span></p></div></div><div id="comments-container-35698" class="comments-container"></div><div id="comment-tools-35698" class="comment-tools"></div><div class="clear"></div><div id="comment-35698-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35753"></span>

<div id="answer-container-35753" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35753-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35753-score" class="post-score" title="current number of votes">0</div><span id="post-35753-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I'm not aware of anything working on SSDP at all so the chances are probably not very high.</p><p>There is an <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=6190">enhancement request</a> which would cover this but it's pretty old now.</p><p>As with all things Open Source, if you want it, do it! :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Aug '14, 07:48</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-35753" class="comments-container"></div><div id="comment-tools-35753" class="comment-tools"></div><div class="clear"></div><div id="comment-35753-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

