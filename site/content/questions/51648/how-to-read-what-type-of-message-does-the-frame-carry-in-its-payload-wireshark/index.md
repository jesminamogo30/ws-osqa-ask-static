+++
type = "question"
title = "how to read what type of message does the frame carry in its payload? WireShark"
description = '''Assume that you have been using the WireShark network analysis software and you are examining and Ethernet frame. The hexadecimal contents of the Ethernet frame that you have captured start as shown below: FF FF FF FF FF FF 08 24 F5 CE D3 AC 08 06 00 01 08 00 06 04 00 01 08 24 F5 C3 D3 AC 83 78 2A 7...'''
date = "2016-04-13T12:45:00Z"
lastmod = "2016-04-13T14:00:00Z"
weight = 51648
keywords = [ "ethernet", "frame", "type" ]
aliases = [ "/questions/51648" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to read what type of message does the frame carry in its payload? WireShark](/questions/51648/how-to-read-what-type-of-message-does-the-frame-carry-in-its-payload-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51648-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51648-score" class="post-score" title="current number of votes">0</div><span id="post-51648-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Assume that you have been using the WireShark network analysis software and you are examining and Ethernet frame. The hexadecimal contents of the Ethernet frame that you have captured start as shown below: FF FF FF FF FF FF 08 24 F5 CE D3 AC 08 06 00 01 08 00 06 04 00 01 08 24 F5 C3 D3 AC 83 78 2A 7F 00 00 00 00 00 00 83 78 29 6F 00 00 00 00 00 00 question a) What is the source address (sender) in the ethernet frame that you have captured? answer HonHairPr_CE:D3:AC (08:24:F5:CE:D3:AC) Question b) who is the receiver of the frame? Answer Destination IPVmcast_FF (FF:FF:FF:FF:FF:FF) Question c)what type of message does the frame carry in its payload?</p><p>I do not want you to tell me the answer of the 3rd question. I was wondering if you could guide me little bit because because i am lost..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-frame" rel="tag" title="see questions tagged &#39;frame&#39;">frame</span> <span class="post-tag tag-link-type" rel="tag" title="see questions tagged &#39;type&#39;">type</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Apr '16, 12:45</strong></p><img src="https://secure.gravatar.com/avatar/5efba11014fa8d1a081b986683e05e32?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="%CE%A7%CF%81%CE%B7%CF%83%CF%84%CE%BF%CF%82%20%CE%91%CE%BB%CE%B5%CE%BA%CE%BF%CF%82%20%CE%9C%CE%B5%CF%83%CF%84%CE%B2%CE%B9%CF%81%CE%B9%CF%83%CE%B2%CE%B9%CE%BB%CE%B9&#39;s gravatar image" /><p><span>Χρηστος Αλεκ...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Χρηστος Αλεκος Μεστβιρισβιλι has no accepted answers">0%</span></p></div></div><div id="comments-container-51648" class="comments-container"></div><div id="comment-tools-51648" class="comment-tools"></div><div class="clear"></div><div id="comment-51648-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51651"></span>

<div id="answer-container-51651" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51651-score" class="post-score" title="current number of votes">0</div><span id="post-51651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>DuckDuckGo is your friend here, try searching for <a href="https://duckduckgo.com/?q=ethernet+II+frame">Ethernet II frame</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Apr '16, 14:00</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-51651" class="comments-container"></div><div id="comment-tools-51651" class="comment-tools"></div><div class="clear"></div><div id="comment-51651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

