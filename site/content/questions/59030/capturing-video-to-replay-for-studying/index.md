+++
type = "question"
title = "Capturing video to replay for studying"
description = '''Hello Community, I am new to Wireshark. I performed a few Google searches to try and find the answer, but I had no luck. Currently, my job has paid for me to take a certification training through online means. The companies site I log into is using HTTPS and I click on each module to watch the video...'''
date = "2017-01-24T13:49:00Z"
lastmod = "2017-01-24T13:49:00Z"
weight = 59030
keywords = [ "video" ]
aliases = [ "/questions/59030" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capturing video to replay for studying](/questions/59030/capturing-video-to-replay-for-studying)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59030-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59030-score" class="post-score" title="current number of votes">0</div><span id="post-59030-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Community,</p><p>I am new to Wireshark. I performed a few Google searches to try and find the answer, but I had no luck. Currently, my job has paid for me to take a certification training through online means. The companies site I log into is using HTTPS and I click on each module to watch the video. Is there a way to capture the video with Wireshark so I can download a copy of it? I want to turn the video into an audio track so I can listen to it in the car, etc.</p><p>Any help would be appreciated!</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-video" rel="tag" title="see questions tagged &#39;video&#39;">video</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '17, 13:49</strong></p><img src="https://secure.gravatar.com/avatar/627b145ed074ebaa7368338cb7393894?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Curlyp&#39;s gravatar image" /><p><span>Curlyp</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Curlyp has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Jan '17, 13:54</strong> </span></p></div></div><div id="comments-container-59030" class="comments-container"></div><div id="comment-tools-59030" class="comment-tools"></div><div class="clear"></div><div id="comment-59030-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

