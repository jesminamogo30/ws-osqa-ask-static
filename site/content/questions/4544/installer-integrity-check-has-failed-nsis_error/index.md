+++
type = "question"
title = "&quot;Installer integrity check has failed. NSIS_Error&quot;"
description = '''I&#x27;ve just set up on Windows 7 64-bit on my existing PC (wiped the XP). Whenever I install 32-bit or 64-bit WireShark I get the following error message: &quot;Installer integrity check has failed. Common causes include incomplete download and damaged media. Contact the installer&#x27;s author to obtain a new c...'''
date = "2011-06-13T13:22:00Z"
lastmod = "2011-06-28T10:14:00Z"
weight = 4544
keywords = [ "windows7", "installation" ]
aliases = [ "/questions/4544" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ["Installer integrity check has failed. NSIS\_Error"](/questions/4544/installer-integrity-check-has-failed-nsis_error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4544-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4544-score" class="post-score" title="current number of votes">0</div><span id="post-4544-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've just set up on Windows 7 64-bit on my existing PC (wiped the XP). Whenever I install 32-bit or 64-bit WireShark I get the following error message:</p><p>"Installer integrity check has failed. Common causes include incomplete download and damaged media. Contact the installer's author to obtain a new copy. More information at: <a href="http://nsis.sf.net/NSIS_Error">http://nsis.sf.net/NSIS_Error"</a></p><p>I tried the tips at <a href="http://nsis.sf.net">nsis.sf.net</a>, but no luck. Any ideas?</p><p>Thanks, Tom Edelbrok</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '11, 13:22</strong></p><img src="https://secure.gravatar.com/avatar/9a8c4c4b1ca9c7ba559089bd07c755b9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tomedelbrok&#39;s gravatar image" /><p><span>tomedelbrok</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tomedelbrok has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '12, 20:20</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-4544" class="comments-container"><span id="4547"></span><div id="comment-4547" class="comment"><div id="post-4547-score" class="comment-score"></div><div class="comment-text"><p>Question is:</p><ul><li>Where did you get the installer from?</li><li>How did you download the installer?</li></ul></div><div id="comment-4547-info" class="comment-info"><span class="comment-age">(13 Jun '11, 13:55)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="4775"></span><div id="comment-4775" class="comment"><div id="post-4775-score" class="comment-score"></div><div class="comment-text"><p>Seeing this as well with XP with stable release 1.6.0 rc2.</p></div><div id="comment-4775-info" class="comment-info"><span class="comment-age">(27 Jun '11, 16:18)</span> <span class="comment-user userinfo">jmay</span></div></div></div><div id="comment-tools-4544" class="comment-tools"></div><div class="clear"></div><div id="comment-4544-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4787"></span>

<div id="answer-container-4787" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4787-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4787-score" class="post-score" title="current number of votes">0</div><span id="post-4787-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You might want to check the installer(s) you downloaded against the hashes listed in the corresponding "SIGNATURES" file at <a href="http://www.wireshark.org/download/src/all-versions/">http://www.wireshark.org/download/src/all-versions/</a>. For instance, the SHA1 hash for wireshark-win64-1.6.0.exe is 03e4f55e7a03bd713b83e3d7d031de8165c44be9.</p><p>If you're using Windows Microsoft provides a <a href="http://support.microsoft.com/kb/841290">command-line hashing tool</a>, but there are a <a href="http://www.google.com/search?q=windows+md5">bunch of third-party GUI tools available</a>.</p><p>As @Jaap asked, where did the installer come from, and how was it downloaded? Also, do you have a proxy or antivirus tool in place that might modify the installer?</p><p>The official download locations are the ones listed at <a href="http://www.wireshark.org/mirror.html">http://www.wireshark.org/mirror.html</a> and at <a href="http://sourceforge.net/projects/wireshark/">SourceForge</a>. If one of those sites is delivering corrupted files I need to know about it so that the problem can be fixed.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jun '11, 10:14</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-4787" class="comments-container"></div><div id="comment-tools-4787" class="comment-tools"></div><div class="clear"></div><div id="comment-4787-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

