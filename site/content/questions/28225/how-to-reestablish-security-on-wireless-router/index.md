+++
type = "question"
title = "[closed] How to reestablish Security on Wireless Router?"
description = '''Using windows XP and have a D-Link Wireless Router Model # wbr-1310. Notified by neighbor that my internet was not secure. Please tell me step by step way to reestablish password protection again? Also, Is there a way that I can find out if I was high jacked? Thanks'''
date = "2013-12-17T19:05:00Z"
lastmod = "2013-12-18T00:24:00Z"
weight = 28225
keywords = [ "geoteks" ]
aliases = [ "/questions/28225" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to reestablish Security on Wireless Router?](/questions/28225/how-to-reestablish-security-on-wireless-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28225-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28225-score" class="post-score" title="current number of votes">0</div><span id="post-28225-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Using windows XP and have a D-Link Wireless Router Model # wbr-1310. Notified by neighbor that my internet was not secure. Please tell me step by step way to reestablish password protection again? Also, Is there a way that I can find out if I was high jacked? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-geoteks" rel="tag" title="see questions tagged &#39;geoteks&#39;">geoteks</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Dec '13, 19:05</strong></p><img src="https://secure.gravatar.com/avatar/59be7cfc41d9d622fe6d095a45a45082?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="geoteks&#39;s gravatar image" /><p><span>geoteks</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="geoteks has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>18 Dec '13, 00:23</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-28225" class="comments-container"><span id="28237"></span><div id="comment-28237" class="comment"><div id="post-28237-score" class="comment-score"></div><div class="comment-text"><p>Please read the manual of your router.</p></div><div id="comment-28237-info" class="comment-info"><span class="comment-age">(18 Dec '13, 00:24)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28225" class="comment-tools"></div><div class="clear"></div><div id="comment-28225-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by Kurt Knochner 18 Dec '13, 00:23

</div>

</div>

</div>

