+++
type = "question"
title = "Wireshark will not open on my Mac OS X 10.8 system HELP"
description = '''First time user of Wireshark. I am a IT student at Penn State Word Campus. I downloaded Wireshark and also downloaded XQuartz. I tried to open Wireshark, but nothing happens. What&#x27;s going on?  Please help'''
date = "2013-09-04T19:04:00Z"
lastmod = "2013-09-05T11:56:00Z"
weight = 24366
keywords = [ "help" ]
aliases = [ "/questions/24366" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark will not open on my Mac OS X 10.8 system HELP](/questions/24366/wireshark-will-not-open-on-my-mac-os-x-108-system-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24366-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24366-score" class="post-score" title="current number of votes">0</div><span id="post-24366-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>First time user of Wireshark. I am a IT student at Penn State Word Campus. I downloaded Wireshark and also downloaded XQuartz. I tried to open Wireshark, but nothing happens. What's going on?</p><p>Please help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Sep '13, 19:04</strong></p><img src="https://secure.gravatar.com/avatar/1f94c2e9c0f7efe36aa6694e26e172d4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="raf5328&#39;s gravatar image" /><p><span>raf5328</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="raf5328 has no accepted answers">0%</span></p></div></div><div id="comments-container-24366" class="comments-container"><span id="24391"></span><div id="comment-24391" class="comment"><div id="post-24391-score" class="comment-score"></div><div class="comment-text"><p>Are there any messages about Wireshark in the Console app (it's in the Utilities folder under the Applications folder)?</p></div><div id="comment-24391-info" class="comment-info"><span class="comment-age">(05 Sep '13, 11:56)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-24366" class="comment-tools"></div><div class="clear"></div><div id="comment-24366-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

