+++
type = "question"
title = "TDLS and Hotspot2.0 support in Wireshark"
description = '''Hi , Could you please let me know that from which version onward TDLS and Hotspot2.0 ( ANQP) support is available ? Regards Dhanajit'''
date = "2013-11-15T01:17:00Z"
lastmod = "2013-11-15T01:17:00Z"
weight = 27028
keywords = [ "tdls-hotspot2.0" ]
aliases = [ "/questions/27028" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TDLS and Hotspot2.0 support in Wireshark](/questions/27028/tdls-and-hotspot20-support-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27028-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27028-score" class="post-score" title="current number of votes">0</div><span id="post-27028-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi ,</p><p>Could you please let me know that from which version onward TDLS and Hotspot2.0 ( ANQP) support is available ?</p><p>Regards Dhanajit</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tdls-hotspot2.0" rel="tag" title="see questions tagged &#39;tdls-hotspot2.0&#39;">tdls-hotspot2.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '13, 01:17</strong></p><img src="https://secure.gravatar.com/avatar/1def8d90c5110adecdfae5f4a7f83bc8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dhanajit&#39;s gravatar image" /><p><span>Dhanajit</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dhanajit has no accepted answers">0%</span></p></div></div><div id="comments-container-27028" class="comments-container"></div><div id="comment-tools-27028" class="comment-tools"></div><div class="clear"></div><div id="comment-27028-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

