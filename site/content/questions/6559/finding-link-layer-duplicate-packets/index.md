+++
type = "question"
title = "finding link layer duplicate packets"
description = '''I wondered if I can find mac/link layer duplicate packets (same sequence number) like I can do with tcp.analysis.dup_frame with tcp protocol packets'''
date = "2011-09-26T05:15:00Z"
lastmod = "2011-09-26T10:12:00Z"
weight = 6559
keywords = [ "filter", "duplicates", "link" ]
aliases = [ "/questions/6559" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [finding link layer duplicate packets](/questions/6559/finding-link-layer-duplicate-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6559-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6559-score" class="post-score" title="current number of votes">0</div><span id="post-6559-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I wondered if I can find mac/link layer duplicate packets (same sequence number) like I can do with tcp.analysis.dup_frame with tcp protocol packets</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-duplicates" rel="tag" title="see questions tagged &#39;duplicates&#39;">duplicates</span> <span class="post-tag tag-link-link" rel="tag" title="see questions tagged &#39;link&#39;">link</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '11, 05:15</strong></p><img src="https://secure.gravatar.com/avatar/5d64d21de6598960bf2db61f1ca705cc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ddayan&#39;s gravatar image" /><p><span>ddayan</span><br />
<span class="score" title="41 reputation points">41</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="17 badges"><span class="silver">●</span><span class="badgecount">17</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ddayan has no accepted answers">0%</span></p></div></div><div id="comments-container-6559" class="comments-container"><span id="6564"></span><div id="comment-6564" class="comment"><div id="post-6564-score" class="comment-score"></div><div class="comment-text"><p>What sequence numbers are you thinking about?</p></div><div id="comment-6564-info" class="comment-info"><span class="comment-age">(26 Sep '11, 10:12)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-6559" class="comment-tools"></div><div class="clear"></div><div id="comment-6559-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

