+++
type = "question"
title = "Install on Windows 2008 Server but alert some error"
description = ''' While install wireshake setup exe on window 2008 server R2 64bit, BUT popup alert as pic, '''
date = "2016-07-27T20:27:00Z"
lastmod = "2016-07-27T22:43:00Z"
weight = 54390
keywords = [ "windows2008" ]
aliases = [ "/questions/54390" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Install on Windows 2008 Server but alert some error](/questions/54390/install-on-windows-2008-server-but-alert-some-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54390-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54390-score" class="post-score" title="current number of votes">0</div><span id="post-54390-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/install_fail_nHTGZEt.png" alt="alt text" /></p><p>While install wireshake setup exe on window 2008 server R2 64bit, BUT popup alert as pic,</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows2008" rel="tag" title="see questions tagged &#39;windows2008&#39;">windows2008</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jul '16, 20:27</strong></p><img src="https://secure.gravatar.com/avatar/853d7093103a60a3b0083b42b705b99e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="neil_hao&#39;s gravatar image" /><p><span>neil_hao</span><br />
<span class="score" title="26 reputation points">26</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="neil_hao has no accepted answers">0%</span></p></img></div></div><div id="comments-container-54390" class="comments-container"><span id="54393"></span><div id="comment-54393" class="comment"><div id="post-54393-score" class="comment-score">1</div><div class="comment-text"><p>Did you retry the download, something went wrong with this download.</p></div><div id="comment-54393-info" class="comment-info"><span class="comment-age">(27 Jul '16, 22:43)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-54390" class="comment-tools"></div><div class="clear"></div><div id="comment-54390-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

