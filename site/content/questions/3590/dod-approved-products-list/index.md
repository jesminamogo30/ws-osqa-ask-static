+++
type = "question"
title = "DoD Approved Products List?"
description = '''I have been told Wireshark 1.0.6 is on the Department of Defense Approved Products List (APL) but cannot find it. Is there a version of Wireshark that is on the APL? If so, can you please direct me to some supporting documentation?'''
date = "2011-04-18T15:11:00Z"
lastmod = "2011-04-18T15:11:00Z"
weight = 3590
keywords = [ "government" ]
aliases = [ "/questions/3590" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [DoD Approved Products List?](/questions/3590/dod-approved-products-list)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3590-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3590-score" class="post-score" title="current number of votes">0</div><span id="post-3590-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have been told Wireshark 1.0.6 is on the Department of Defense Approved Products List (APL) but cannot find it. Is there a version of Wireshark that is on the APL? If so, can you please direct me to some supporting documentation?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-government" rel="tag" title="see questions tagged &#39;government&#39;">government</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '11, 15:11</strong></p><img src="https://secure.gravatar.com/avatar/3bcbf1b7476aff187fca475bc4ae057f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="parslj2&#39;s gravatar image" /><p><span>parslj2</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="parslj2 has no accepted answers">0%</span></p></div></div><div id="comments-container-3590" class="comments-container"></div><div id="comment-tools-3590" class="comment-tools"></div><div class="clear"></div><div id="comment-3590-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

