+++
type = "question"
title = "What is type 21 error (TLS/SSL) in Wireshark ?"
description = '''I&#x27;m having some annoying traffic (only on SSL websites) and can&#x27;t find a proper cause. Please see here for pcap file: https://www.cloudshark.org/captures/efebf7bba359 One thing is I&#x27;m receiving lots of TCP DUPs (only on SSL sites - not on clearnet parts of the same target IPs!) Tried with several cl...'''
date = "2016-02-03T13:43:00Z"
lastmod = "2016-02-03T13:43:00Z"
weight = 49794
keywords = [ "tls", "ssl", "type", "21", "error" ]
aliases = [ "/questions/49794" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What is type 21 error (TLS/SSL) in Wireshark ?](/questions/49794/what-is-type-21-error-tlsssl-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49794-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49794-score" class="post-score" title="current number of votes">0</div><span id="post-49794-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm having some annoying traffic (only on SSL websites) and can't find a proper cause. Please see here for pcap file: <a href="https://www.cloudshark.org/captures/efebf7bba359">https://www.cloudshark.org/captures/efebf7bba359</a></p><p>One thing is I'm receiving lots of TCP DUPs (only on SSL sites - not on clearnet parts of the same target IPs!) Tried with several client systems aswell as Smartphones and different browsertypes.</p><p>After receiving these DUPS from the internet, clients respond with a Type 21 error. (Doesn't show in the pcap file because it's only a snippet). Is this a common TLS/SSL behaviour? If so, then why does Wireshark hilight it?</p><p>Second question: is there a possible correlation between DUPs on SSL packets and "Encryption Alert, type 21" errors? I can't find alot of info about it... any help is appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tls" rel="tag" title="see questions tagged &#39;tls&#39;">tls</span> <span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-type" rel="tag" title="see questions tagged &#39;type&#39;">type</span> <span class="post-tag tag-link-21" rel="tag" title="see questions tagged &#39;21&#39;">21</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '16, 13:43</strong></p><img src="https://secure.gravatar.com/avatar/653d979a2dc2892e289024f6a619921e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="boiiingg&#39;s gravatar image" /><p><span>boiiingg</span><br />
<span class="score" title="2 reputation points">2</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="boiiingg has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Feb '16, 14:11</strong> </span></p></div></div><div id="comments-container-49794" class="comments-container"></div><div id="comment-tools-49794" class="comment-tools"></div><div class="clear"></div><div id="comment-49794-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

