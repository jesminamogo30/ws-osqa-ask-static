+++
type = "question"
title = "capture between iPhone and Panasanoc TV over wireless"
description = '''Hi everyone, Wireshark is a new thing to me and i am trying to capture the conversation between my iPhone and my Panasonic TV over TCP/IP. For example i have my phone in 192.168.1.11 and my TV in 192.168.1.18, but i am not able to set the wireshark to capture the packets between this two IPs.. I onl...'''
date = "2013-01-11T10:11:00Z"
lastmod = "2013-01-11T10:29:00Z"
weight = 17612
keywords = [ "tv", "wireless", "lan", "iphone" ]
aliases = [ "/questions/17612" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [capture between iPhone and Panasanoc TV over wireless](/questions/17612/capture-between-iphone-and-panasanoc-tv-over-wireless)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17612-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17612-score" class="post-score" title="current number of votes">0</div><span id="post-17612-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone,</p><p>Wireshark is a new thing to me and i am trying to capture the conversation between my iPhone and my Panasonic TV over TCP/IP. For example i have my phone in 192.168.1.11 and my TV in 192.168.1.18, but i am not able to set the wireshark to capture the packets between this two IPs.. I only can see everything that is sent to/from my windows PC (192.168.1.10). Any help is appreciated.</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tv" rel="tag" title="see questions tagged &#39;tv&#39;">tv</span> <span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-lan" rel="tag" title="see questions tagged &#39;lan&#39;">lan</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '13, 10:11</strong></p><img src="https://secure.gravatar.com/avatar/be7bfd353acae128a1a8fdb667ba820a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="uberzone&#39;s gravatar image" /><p><span>uberzone</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="uberzone has no accepted answers">0%</span></p></div></div><div id="comments-container-17612" class="comments-container"></div><div id="comment-tools-17612" class="comment-tools"></div><div class="clear"></div><div id="comment-17612-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17615"></span>

<div id="answer-container-17615" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17615-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17615-score" class="post-score" title="current number of votes">0</div><span id="post-17615-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Here you go:</p><p><a href="http://www.wireshark.org/faq.html#q7.1">http://www.wireshark.org/faq.html#q7.1</a> Also check out question 7.2</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '13, 10:29</strong></p><img src="https://secure.gravatar.com/avatar/63805f079ac429902641cad9d7cd69e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hansangb&#39;s gravatar image" /><p><span>hansangb</span><br />
<span class="score" title="791 reputation points">791</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="19 badges"><span class="bronze">●</span><span class="badgecount">19</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hansangb has 7 accepted answers">12%</span></p></div></div><div id="comments-container-17615" class="comments-container"></div><div id="comment-tools-17615" class="comment-tools"></div><div class="clear"></div><div id="comment-17615-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

