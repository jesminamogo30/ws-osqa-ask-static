+++
type = "question"
title = "sequence counter"
description = '''I would like, not only to see the packets that have a specified pattern (filter like &quot;udp contains XX:YY:ZZ&quot;), but also to know :  How many times i have the pattern in the whole whire shark capture? Becouse, one pattern could be found more than one time in one packet. Any idea? Thanks a lot E.'''
date = "2012-10-01T02:30:00Z"
lastmod = "2012-10-01T03:57:00Z"
weight = 14621
keywords = [ "pattern", "counter" ]
aliases = [ "/questions/14621" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [sequence counter](/questions/14621/sequence-counter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14621-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14621-score" class="post-score" title="current number of votes">0</div><span id="post-14621-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like, not only to see the packets that have a specified pattern (filter like "udp contains XX:YY:ZZ"), but also to know : How many times i have the pattern in the whole whire shark capture? Becouse, one pattern could be found more than one time in one packet.</p><p>Any idea? Thanks a lot E.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pattern" rel="tag" title="see questions tagged &#39;pattern&#39;">pattern</span> <span class="post-tag tag-link-counter" rel="tag" title="see questions tagged &#39;counter&#39;">counter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Oct '12, 02:30</strong></p><img src="https://secure.gravatar.com/avatar/e000446fbe81c043edde486fe2f00147?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ebsws&#39;s gravatar image" /><p><span>ebsws</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ebsws has no accepted answers">0%</span></p></div></div><div id="comments-container-14621" class="comments-container"></div><div id="comment-tools-14621" class="comment-tools"></div><div class="clear"></div><div id="comment-14621-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14623"></span>

<div id="answer-container-14623" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14623-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14623-score" class="post-score" title="current number of votes">0</div><span id="post-14623-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think Wireshark will not be able to count multiple filter hits in one packet for you, it will only show each packet that has the pattern at least once.</p><p>You could use the Search function to find all patterns manually by invoking the search until nothing else is found - but that can be a very robotic task - maybe the command line heroes have an idea how to do this in a faster way :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Oct '12, 03:57</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-14623" class="comments-container"></div><div id="comment-tools-14623" class="comment-tools"></div><div class="clear"></div><div id="comment-14623-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

