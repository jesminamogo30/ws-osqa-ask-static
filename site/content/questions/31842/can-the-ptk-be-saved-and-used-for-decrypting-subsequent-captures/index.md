+++
type = "question"
title = "Can the PTK be saved and used for decrypting subsequent captures?"
description = '''Can the PTK, from the WPA handshake (or handshake packets), be saved and used for decoding subsequent captures? In other words, when raw 802.11 packets are being captured to a ring buffer, if the handshake is saved from a buffer, can it be used to decrypt the other buffers in the ring? '''
date = "2014-04-15T07:30:00Z"
lastmod = "2014-04-15T07:30:00Z"
weight = 31842
keywords = [ "ptk", "handshake", "decrypt", "wpa" ]
aliases = [ "/questions/31842" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can the PTK be saved and used for decrypting subsequent captures?](/questions/31842/can-the-ptk-be-saved-and-used-for-decrypting-subsequent-captures)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31842-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31842-score" class="post-score" title="current number of votes">0</div><span id="post-31842-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can the PTK, from the WPA handshake (or handshake packets), be saved and used for decoding subsequent captures? In other words, when raw 802.11 packets are being captured to a ring buffer, if the handshake is saved from a buffer, can it be used to decrypt the other buffers in the ring?<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ptk" rel="tag" title="see questions tagged &#39;ptk&#39;">ptk</span> <span class="post-tag tag-link-handshake" rel="tag" title="see questions tagged &#39;handshake&#39;">handshake</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span> <span class="post-tag tag-link-wpa" rel="tag" title="see questions tagged &#39;wpa&#39;">wpa</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '14, 07:30</strong></p><img src="https://secure.gravatar.com/avatar/8ad3e2b0f09bb769cd6b23c7fa9149b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Magnumb&#39;s gravatar image" /><p><span>Magnumb</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Magnumb has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-31842" class="comments-container"></div><div id="comment-tools-31842" class="comment-tools"></div><div class="clear"></div><div id="comment-31842-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

