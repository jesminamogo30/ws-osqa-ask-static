+++
type = "question"
title = "Wi-Fi Protected Setup"
description = '''Hi, I´ve got a capture file, how could I find the PIN Authentication Method? I have found this Site http://www.wireshark.org/docs/dfref/w/wps.html but which filter is for the PIN - Authentication? Thanks!'''
date = "2014-04-12T00:06:00Z"
lastmod = "2014-04-12T00:06:00Z"
weight = 31765
keywords = [ "wps" ]
aliases = [ "/questions/31765" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wi-Fi Protected Setup](/questions/31765/wi-fi-protected-setup)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31765-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31765-score" class="post-score" title="current number of votes">0</div><span id="post-31765-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I´ve got a capture file, how could I find the PIN Authentication Method? I have found this Site <a href="http://www.wireshark.org/docs/dfref/w/wps.html">http://www.wireshark.org/docs/dfref/w/wps.html</a> but which filter is for the PIN - Authentication?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wps" rel="tag" title="see questions tagged &#39;wps&#39;">wps</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Apr '14, 00:06</strong></p><img src="https://secure.gravatar.com/avatar/62fecf405b9f290dd6f941634d3fd67c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="steve_r&#39;s gravatar image" /><p><span>steve_r</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="steve_r has no accepted answers">0%</span></p></div></div><div id="comments-container-31765" class="comments-container"></div><div id="comment-tools-31765" class="comment-tools"></div><div class="clear"></div><div id="comment-31765-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

