+++
type = "question"
title = "Wireshark is capturing the probe request frame but no probe response for this request"
description = '''I could send the probe request which is visible on the wireshark but not getting any probe response for this request'''
date = "2012-04-03T04:55:00Z"
lastmod = "2012-05-31T21:23:00Z"
weight = 9910
keywords = [ "proberesponse" ]
aliases = [ "/questions/9910" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark is capturing the probe request frame but no probe response for this request](/questions/9910/wireshark-is-capturing-the-probe-request-frame-but-no-probe-response-for-this-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9910-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9910-score" class="post-score" title="current number of votes">0</div><span id="post-9910-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I could send the probe request which is visible on the wireshark but not getting any probe response for this request</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-proberesponse" rel="tag" title="see questions tagged &#39;proberesponse&#39;">proberesponse</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Apr '12, 04:55</strong></p><img src="https://secure.gravatar.com/avatar/d8831dcb266182a45a79cf0b64565ba6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hel&#39;s gravatar image" /><p><span>hel</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hel has no accepted answers">0%</span></p></div></div><div id="comments-container-9910" class="comments-container"><span id="11517"></span><div id="comment-11517" class="comment"><div id="post-11517-score" class="comment-score"></div><div class="comment-text"><p>So what is your question?</p></div><div id="comment-11517-info" class="comment-info"><span class="comment-age">(31 May '12, 21:23)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-9910" class="comment-tools"></div><div class="clear"></div><div id="comment-9910-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

