+++
type = "question"
title = "Follow TCP Stream"
description = '''I want to use Wireshark to capture the TCP Packets. I read about the tcpflow command. Is there any way I can use Follow TCP Stream using command line? I am using windows 7'''
date = "2014-01-26T22:00:00Z"
lastmod = "2014-01-26T22:00:00Z"
weight = 29173
keywords = [ "follow.tcp.stream" ]
aliases = [ "/questions/29173" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Follow TCP Stream](/questions/29173/follow-tcp-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29173-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29173-score" class="post-score" title="current number of votes">0</div><span id="post-29173-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to use Wireshark to capture the TCP Packets. I read about the <strong>tcpflow</strong> command. Is there any way I can use <code>Follow TCP Stream</code> using command line? I am using windows 7</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-follow.tcp.stream" rel="tag" title="see questions tagged &#39;follow.tcp.stream&#39;">follow.tcp.stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jan '14, 22:00</strong></p><img src="https://secure.gravatar.com/avatar/a4db6695f8ac5f30379ef10315026206?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prakharmohan&#39;s gravatar image" /><p><span>prakharmohan</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prakharmohan has no accepted answers">0%</span></p></div></div><div id="comments-container-29173" class="comments-container"></div><div id="comment-tools-29173" class="comment-tools"></div><div class="clear"></div><div id="comment-29173-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

