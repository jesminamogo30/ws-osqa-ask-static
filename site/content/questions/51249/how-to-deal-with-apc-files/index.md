+++
type = "question"
title = "How to deal with .apc files"
description = '''I want to write a software tool to analyze if my wireless development board work normally, especially in connection.  Now, I have captured .apc files using by Omnipeek, and I want to filter specific src/dst mac address info and analyze the connection process AUTOMATICALLY in my software. I want to k...'''
date = "2016-03-28T23:06:00Z"
lastmod = "2016-03-28T23:06:00Z"
weight = 51249
keywords = [ "winpcap", "airpcap", "802.11", "omnipeek", "libpcap" ]
aliases = [ "/questions/51249" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to deal with .apc files](/questions/51249/how-to-deal-with-apc-files)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51249-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51249-score" class="post-score" title="current number of votes">0</div><span id="post-51249-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to write a software tool to analyze if my wireless development board work normally, especially in connection. Now, I have captured .apc files using by Omnipeek, and I want to filter specific src/dst mac address info and analyze the connection process <strong>AUTOMATICALLY</strong> in my software.</p><p>I want to know which open source <strong>LIB</strong> should I use to read and analyze .apc files. Is <strong>LibPcap</strong>, <strong>WinPcap</strong> or <strong>AirPcap</strong>, or another LIB?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-airpcap" rel="tag" title="see questions tagged &#39;airpcap&#39;">airpcap</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-omnipeek" rel="tag" title="see questions tagged &#39;omnipeek&#39;">omnipeek</span> <span class="post-tag tag-link-libpcap" rel="tag" title="see questions tagged &#39;libpcap&#39;">libpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Mar '16, 23:06</strong></p><img src="https://secure.gravatar.com/avatar/f018c571398d8fd8cc80a22923b4f0ae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xxnsql&#39;s gravatar image" /><p><span>xxnsql</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xxnsql has no accepted answers">0%</span></p></div></div><div id="comments-container-51249" class="comments-container"></div><div id="comment-tools-51249" class="comment-tools"></div><div class="clear"></div><div id="comment-51249-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

