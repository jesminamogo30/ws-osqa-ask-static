+++
type = "question"
title = "wireshark 1.12.4 hangs when I click Export Specified packets option in linux mint 17.1"
description = '''I am using linux mint 17.1 and wireshark 1.12.4 . when I apply filters and click on export specified packets under file tab then wireshark gets stuck. Please let me know how to resolve this issue. '''
date = "2015-05-20T06:47:00Z"
lastmod = "2015-05-25T01:43:00Z"
weight = 42578
keywords = [ "hang" ]
aliases = [ "/questions/42578" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark 1.12.4 hangs when I click Export Specified packets option in linux mint 17.1](/questions/42578/wireshark-1124-hangs-when-i-click-export-specified-packets-option-in-linux-mint-171)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42578-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42578-score" class="post-score" title="current number of votes">0</div><span id="post-42578-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am using linux mint 17.1 and wireshark 1.12.4 . when I apply filters and click on export specified packets under file tab then wireshark gets stuck. Please let me know how to resolve this issue.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hang" rel="tag" title="see questions tagged &#39;hang&#39;">hang</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 May '15, 06:47</strong></p><img src="https://secure.gravatar.com/avatar/e824146039099232ea2a2b0afcae4567?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Thinkt&#39;s gravatar image" /><p><span>Thinkt</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Thinkt has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 May '15, 09:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-42578" class="comments-container"><span id="42594"></span><div id="comment-42594" class="comment"><div id="post-42594-score" class="comment-score"></div><div class="comment-text"><p><em>Crashed</em> or <em>hangs</em>? If it <em>crashes</em> the Wireshark window will disappear and, on UN*Xes such as Linux, you'll probably get a core dump file. If it <em>hangs</em> it continues to run but the window doesn't respond to input.</p></div><div id="comment-42594-info" class="comment-info"><span class="comment-age">(20 May '15, 18:36)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="42610"></span><div id="comment-42610" class="comment"><div id="post-42610-score" class="comment-score"></div><div class="comment-text"><p>wireshark hangs</p></div><div id="comment-42610-info" class="comment-info"><span class="comment-age">(22 May '15, 03:24)</span> <span class="comment-user userinfo">Thinkt</span></div></div><span id="42623"></span><div id="comment-42623" class="comment"><div id="post-42623-score" class="comment-score"></div><div class="comment-text"><p>Do you have any remote file systems mounted (NFS, CIFS, etc.)? If so, can you please umount them and try again?</p></div><div id="comment-42623-info" class="comment-info"><span class="comment-age">(22 May '15, 12:27)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="42647"></span><div id="comment-42647" class="comment"><div id="post-42647-score" class="comment-score"></div><div class="comment-text"><p>unmounting remote file system is not solving the issue. Wireshark still hangs.</p></div><div id="comment-42647-info" class="comment-info"><span class="comment-age">(25 May '15, 01:43)</span> <span class="comment-user userinfo">Thinkt</span></div></div></div><div id="comment-tools-42578" class="comment-tools"></div><div class="clear"></div><div id="comment-42578-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

