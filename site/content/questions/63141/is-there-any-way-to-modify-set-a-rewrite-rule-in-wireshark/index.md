+++
type = "question"
title = "Is there any way to modify, set a rewrite rule in Wireshark?"
description = '''Is there any way to modify a request/response in Wireshark, as one can do with Charles Proxy? I&#x27;m trying to sniff a few OTT devices and I find Wireshark the better option, or only option. However, I need to find and append a &quot;true&quot; value to an http call. Easily done with Charles, as traffic passes t...'''
date = "2017-07-26T10:29:00Z"
lastmod = "2017-07-26T10:58:00Z"
weight = 63141
keywords = [ "charles", "modify", "rewrite" ]
aliases = [ "/questions/63141" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is there any way to modify, set a rewrite rule in Wireshark?](/questions/63141/is-there-any-way-to-modify-set-a-rewrite-rule-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63141-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63141-score" class="post-score" title="current number of votes">0</div><span id="post-63141-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any way to modify a request/response in Wireshark, as one can do with Charles Proxy? I'm trying to sniff a few OTT devices and I find Wireshark the better option, or only option. However, I need to find and append a "true" value to an http call. Easily done with Charles, as traffic passes through it. Any ideas on how one might pull this off in Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-charles" rel="tag" title="see questions tagged &#39;charles&#39;">charles</span> <span class="post-tag tag-link-modify" rel="tag" title="see questions tagged &#39;modify&#39;">modify</span> <span class="post-tag tag-link-rewrite" rel="tag" title="see questions tagged &#39;rewrite&#39;">rewrite</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Jul '17, 10:29</strong></p><img src="https://secure.gravatar.com/avatar/6b37d0e6ab52f6ec1827ce25897d0884?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="thinkadam&#39;s gravatar image" /><p><span>thinkadam</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="thinkadam has no accepted answers">0%</span></p></div></div><div id="comments-container-63141" class="comments-container"></div><div id="comment-tools-63141" class="comment-tools"></div><div class="clear"></div><div id="comment-63141-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63142"></span>

<div id="answer-container-63142" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63142-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63142-score" class="post-score" title="current number of votes">0</div><span id="post-63142-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, Wireshark is a display tool, not a modification tool.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jul '17, 10:31</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-63142" class="comments-container"><span id="63145"></span><div id="comment-63145" class="comment"><div id="post-63145-score" class="comment-score"></div><div class="comment-text"><p>Figured. Thanks Jasper!</p></div><div id="comment-63145-info" class="comment-info"><span class="comment-age">(26 Jul '17, 10:58)</span> <span class="comment-user userinfo">thinkadam</span></div></div></div><div id="comment-tools-63142" class="comment-tools"></div><div class="clear"></div><div id="comment-63142-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

