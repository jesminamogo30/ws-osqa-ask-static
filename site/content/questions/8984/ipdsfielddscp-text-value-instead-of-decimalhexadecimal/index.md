+++
type = "question"
title = "ip.dsfield.dscp &quot;text&quot; value instead of decimal/hexadecimal"
description = '''In Wireshark 1.6.0, ip.dsfield.dscp was displayed once applied as a column either in hexa or in text &quot;Expedited Forwarding&quot; e.g. In Wireshark 1.6.5, the same field only offers decimal or hexadecimal, based on the &quot;show resolved&quot; checked/unchecked. Is there an option to change decimal to text-based? ...'''
date = "2012-02-13T14:47:00Z"
lastmod = "2012-02-13T14:47:00Z"
weight = 8984
keywords = [ "ip.dsfield.dscp", "dscp" ]
aliases = [ "/questions/8984" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [ip.dsfield.dscp "text" value instead of decimal/hexadecimal](/questions/8984/ipdsfielddscp-text-value-instead-of-decimalhexadecimal)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8984-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8984-score" class="post-score" title="current number of votes">0</div><span id="post-8984-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In Wireshark 1.6.0, ip.dsfield.dscp was displayed once applied as a column either in hexa or in text "Expedited Forwarding" e.g. In Wireshark 1.6.5, the same field only offers decimal or hexadecimal, based on the "show resolved" checked/unchecked. Is there an option to change decimal to text-based?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip.dsfield.dscp" rel="tag" title="see questions tagged &#39;ip.dsfield.dscp&#39;">ip.dsfield.dscp</span> <span class="post-tag tag-link-dscp" rel="tag" title="see questions tagged &#39;dscp&#39;">dscp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Feb '12, 14:47</strong></p><img src="https://secure.gravatar.com/avatar/e177d49ca6cc8f53ee58cb3de1c4fbaf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yul_analyzer&#39;s gravatar image" /><p><span>yul_analyzer</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yul_analyzer has no accepted answers">0%</span></p></div></div><div id="comments-container-8984" class="comments-container"></div><div id="comment-tools-8984" class="comment-tools"></div><div class="clear"></div><div id="comment-8984-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

