+++
type = "question"
title = "Wireshark app associated with Safari on OS X"
description = '''I have a problem of the pop-up window saying that Wireshark is open, its window can be.... In fact it was my mistake when I firstly run Wireshark, I clicked &quot;Safari&quot; when it asked to select application. I didn&#x27;t look carefully the question &quot;Where is X11?&#x27; Since then, as soon as I open Wireshark, Saf...'''
date = "2013-02-04T14:37:00Z"
lastmod = "2013-02-04T14:37:00Z"
weight = 18297
keywords = [ "osx", "safari", "wireshark" ]
aliases = [ "/questions/18297" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark app associated with Safari on OS X](/questions/18297/wireshark-app-associated-with-safari-on-os-x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18297-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18297-score" class="post-score" title="current number of votes">0</div><span id="post-18297-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a problem of the pop-up window saying that Wireshark is open, its window can be.... In fact it was my mistake when I firstly run Wireshark, I clicked "Safari" when it asked to select application. I didn't look carefully the question "Where is X11?' Since then, as soon as I open Wireshark, Safari opens and the pop-up window appears. I did above procedure and one I succeeded to run correctly Wireshark, but still if I double click simply Wireshark, still Safari opens (but no more pop up window)</p><p>How can I restore it or uninstall completely so as to install again?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-safari" rel="tag" title="see questions tagged &#39;safari&#39;">safari</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Feb '13, 14:37</strong></p><img src="https://secure.gravatar.com/avatar/5c1cee09d8a1ebcb7f6252d872f8c6ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Haru&#39;s gravatar image" /><p><span>Haru</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Haru has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>05 Feb '13, 02:26</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-18297" class="comments-container"></div><div id="comment-tools-18297" class="comment-tools"></div><div class="clear"></div><div id="comment-18297-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

