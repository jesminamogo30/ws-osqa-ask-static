+++
type = "question"
title = "Responsibilty of protocol dissector"
description = '''Hi, I would like to know how to find out who is responsible or working on a specific protocol dissector. Specifically I am interested in the PROFINET dissector. Thanks'''
date = "2017-07-27T23:34:00Z"
lastmod = "2017-07-28T02:04:00Z"
weight = 63198
keywords = [ "responsibilty", "dissector", "profinet" ]
aliases = [ "/questions/63198" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Responsibilty of protocol dissector](/questions/63198/responsibilty-of-protocol-dissector)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63198-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63198-score" class="post-score" title="current number of votes">0</div><span id="post-63198-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I would like to know how to find out who is responsible or working on a specific protocol dissector. Specifically I am interested in the PROFINET dissector.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-responsibilty" rel="tag" title="see questions tagged &#39;responsibilty&#39;">responsibilty</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-profinet" rel="tag" title="see questions tagged &#39;profinet&#39;">profinet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jul '17, 23:34</strong></p><img src="https://secure.gravatar.com/avatar/e0ed115c4ecb073659a79d490fe6e380?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Andreas%20Toensfeuerborn&#39;s gravatar image" /><p><span>Andreas Toen...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Andreas Toensfeuerborn has no accepted answers">0%</span></p></div></div><div id="comments-container-63198" class="comments-container"></div><div id="comment-tools-63198" class="comment-tools"></div><div class="clear"></div><div id="comment-63198-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63204"></span>

<div id="answer-container-63204" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63204-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63204-score" class="post-score" title="current number of votes">0</div><span id="post-63204-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no formal assignment of dissectors to a certain person / developer, that is not how such a vast FOSS project can operate. I'm sure there's a far better <a href="https://sharkfesteurope.wireshark.org/">SharkFest</a> (shameless plug :) ) presentation on it, but it comes down to the individual programmer who writes the dissector code, and the core-developers accepting it into the repository to be build into Wireshark. Who this individual programmer is varies widely, but usually someone with an interest in that particular protocol. In short, it can be anybody.</p><p>If no further response here, you can always subscribe to the <a href="https://www.wireshark.org/lists/">Wireshark developer mailinglist</a> and drop a note there.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jul '17, 02:04</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-63204" class="comments-container"></div><div id="comment-tools-63204" class="comment-tools"></div><div class="clear"></div><div id="comment-63204-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

