+++
type = "question"
title = "How to colorize VoIP Calls graph selections?"
description = '''I use Wireshark on various Windows OS&#x27;s. I primarily use Wireshark to debug VoIP calls, mainly SIP. When I use the &quot;Telephony-&amp;gt;Voip Calls&quot; to parse out the VoIP calls, then select &quot;Flow&quot; to obtain a graph of a call, when I click on one of the packets it whitens out so I can&#x27;t make it out. How can...'''
date = "2013-08-06T06:07:00Z"
lastmod = "2013-08-08T15:34:00Z"
weight = 23589
keywords = [ "color", "graph", "calls", "voip" ]
aliases = [ "/questions/23589" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to colorize VoIP Calls graph selections?](/questions/23589/how-to-colorize-voip-calls-graph-selections)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23589-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23589-score" class="post-score" title="current number of votes">0</div><span id="post-23589-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use Wireshark on various Windows OS's. I primarily use Wireshark to debug VoIP calls, mainly SIP. When I use the "Telephony-&gt;Voip Calls" to parse out the VoIP calls, then select "Flow" to obtain a graph of a call, when I click on one of the packets it whitens out so I can't make it out. How can I change the color scheme inside a VoIP call graph so that when I click on a packet, I can still see it through the color scheme instead of it being obliterated?<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color" rel="tag" title="see questions tagged &#39;color&#39;">color</span> <span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-calls" rel="tag" title="see questions tagged &#39;calls&#39;">calls</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Aug '13, 06:07</strong></p><img src="https://secure.gravatar.com/avatar/c1d331ea615d8789fcd6f930929c32ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NickD&#39;s gravatar image" /><p><span>NickD</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NickD has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-23589" class="comments-container"><span id="23664"></span><div id="comment-23664" class="comment"><div id="post-23664-score" class="comment-score"></div><div class="comment-text"><blockquote><p>when I click on one of the packets <strong>it whitens out</strong> so I can't make it out.</p></blockquote><p>That does not happen on my system (Win XP, Wireshark 1.10.0). So please add some information about the involved versions/releases.</p></div><div id="comment-23664-info" class="comment-info"><span class="comment-age">(08 Aug '13, 15:34)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-23589" class="comment-tools"></div><div class="clear"></div><div id="comment-23589-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

