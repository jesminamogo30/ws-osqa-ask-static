+++
type = "question"
title = "ethernet vs wireless"
description = '''hello if a connect by ehternet cable the wireshark machine to wireless router of my internet provider can i sniff also the wireless traffic ?'''
date = "2017-10-25T05:43:00Z"
lastmod = "2017-10-25T06:18:00Z"
weight = 64182
keywords = [ "wireless", "ethernet" ]
aliases = [ "/questions/64182" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [ethernet vs wireless](/questions/64182/ethernet-vs-wireless)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64182-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64182-score" class="post-score" title="current number of votes">0</div><span id="post-64182-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello if a connect by ehternet cable the wireshark machine to wireless router of my internet provider can i sniff also the wireless traffic ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Oct '17, 05:43</strong></p><img src="https://secure.gravatar.com/avatar/cfbaa9b6a683a5635d9225852395b656?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scanman&#39;s gravatar image" /><p><span>scanman</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scanman has no accepted answers">0%</span></p></div></div><div id="comments-container-64182" class="comments-container"></div><div id="comment-tools-64182" class="comment-tools"></div><div class="clear"></div><div id="comment-64182-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="64183"></span>

<div id="answer-container-64183" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64183-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64183-score" class="post-score" title="current number of votes">0</div><span id="post-64183-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="scanman has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Probably not. The wired connections of "wireless routers" generally connect internally to a switch so you won't see the wireless traffic.</p><p>Some routers can have alternative firmware loaded (usually some Linux derivative) that would then allow you to capture directly on the router, but if the router is owned by your ISP this might not be possible.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Oct '17, 06:18</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-64183" class="comments-container"></div><div id="comment-tools-64183" class="comment-tools"></div><div class="clear"></div><div id="comment-64183-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

