+++
type = "question"
title = "Proof of completion"
description = '''I am wondering if there are any courses offered via online or through self learning that will provide some type of certificate of completion.'''
date = "2010-12-02T08:57:00Z"
lastmod = "2010-12-02T17:54:00Z"
weight = 1211
keywords = [ "completion" ]
aliases = [ "/questions/1211" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Proof of completion](/questions/1211/proof-of-completion)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1211-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1211-score" class="post-score" title="current number of votes">0</div><span id="post-1211-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am wondering if there are any courses offered via online or through self learning that will provide some type of certificate of completion.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-completion" rel="tag" title="see questions tagged &#39;completion&#39;">completion</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Dec '10, 08:57</strong></p><img src="https://secure.gravatar.com/avatar/9a2f3228c0cb9bbc573709561249204d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="virginialoverr&#39;s gravatar image" /><p><span>virginialoverr</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="virginialoverr has no accepted answers">0%</span></p></div></div><div id="comments-container-1211" class="comments-container"></div><div id="comment-tools-1211" class="comment-tools"></div><div class="clear"></div><div id="comment-1211-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="1215"></span>

<div id="answer-container-1215" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1215-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1215-score" class="post-score" title="current number of votes">0</div><span id="post-1215-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Do you mean http://www.wiresharktraining.com/certification.html ?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Dec '10, 14:52</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div></div><div id="comments-container-1215" class="comments-container"></div><div id="comment-tools-1215" class="comment-tools"></div><div class="clear"></div><div id="comment-1215-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="1217"></span>

<div id="answer-container-1217" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1217-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1217-score" class="post-score" title="current number of votes">0</div><span id="post-1217-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>We have the Core 1 and Core 2 courses online at the new Online Portal. Register for a free account at www.lcuportal.com and select the Courses Available tab to see the courses, syllabus, pricing, etc. The new Online Portal contains a test for each of these courses and, upon successful completion, a printed certificate of completion.</p><p>In addition, you can view your transcript for credit hours information, test scores and more.</p><p>If you are interested in becomming a Wireshark Certified Network Analyst, visit www.wiresharktraining.com/certification (as Marty mentioned above).</p><p>Questions? Email <span class="__cf_email__" data-cfemail="452c2b232a05262d243535202929306b262a286b">[email protected]</span></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Dec '10, 17:54</strong></p><img src="https://secure.gravatar.com/avatar/9b4bb3984350b45aee3eda5cc1c90d36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lchappell&#39;s gravatar image" /><p><span>lchappell ♦</span><br />
<span class="score" title="1206 reputation points"><span>1.2k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="30 badges"><span class="bronze">●</span><span class="badgecount">30</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lchappell has 6 accepted answers">8%</span></p></div></div><div id="comments-container-1217" class="comments-container"></div><div id="comment-tools-1217" class="comment-tools"></div><div class="clear"></div><div id="comment-1217-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

