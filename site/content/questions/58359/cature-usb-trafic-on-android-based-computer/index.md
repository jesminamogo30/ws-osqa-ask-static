+++
type = "question"
title = "Cature USB trafic on Android based computer"
description = '''I would like to capture USB trafic between Android based computer and a network device (USB connection).  1. Is Whire Shark android application support it ?  2. Do I need additional package ?'''
date = "2016-12-27T05:05:00Z"
lastmod = "2016-12-27T05:05:00Z"
weight = 58359
keywords = [ "android", "usb" ]
aliases = [ "/questions/58359" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Cature USB trafic on Android based computer](/questions/58359/cature-usb-trafic-on-android-based-computer)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58359-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58359-score" class="post-score" title="current number of votes">0</div><span id="post-58359-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to capture USB trafic between Android based computer and a network device (USB connection). 1. Is Whire Shark android application support it ? 2. Do I need additional package ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Dec '16, 05:05</strong></p><img src="https://secure.gravatar.com/avatar/c48fc29ee04d7c8262d0ad4f257ad9ce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eyalya&#39;s gravatar image" /><p><span>eyalya</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eyalya has no accepted answers">0%</span></p></div></div><div id="comments-container-58359" class="comments-container"></div><div id="comment-tools-58359" class="comment-tools"></div><div class="clear"></div><div id="comment-58359-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

