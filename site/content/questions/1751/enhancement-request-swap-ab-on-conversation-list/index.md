+++
type = "question"
title = "Enhancement Request: Swap A/B On Conversation List"
description = '''Tried to find the proper forum for enhancement requests, and settled on this one. Forgive me if I guessed poorly. I often use the Conversation List to track conversations between a database server and clients. However, sometimes the server shows up as &quot;PortA&quot; and sometimes as &quot;PortB&quot;, which makes it...'''
date = "2011-01-14T10:10:00Z"
lastmod = "2011-01-14T17:49:00Z"
weight = 1751
keywords = [ "endpoints", "enhancement", "conversationlist" ]
aliases = [ "/questions/1751" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Enhancement Request: Swap A/B On Conversation List](/questions/1751/enhancement-request-swap-ab-on-conversation-list)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1751-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1751-score" class="post-score" title="current number of votes">0</div><span id="post-1751-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Tried to find the proper forum for enhancement requests, and settled on this one. Forgive me if I guessed poorly.</p><p>I often use the Conversation List to track conversations between a database server and clients. However, sometimes the server shows up as "PortA" and sometimes as "PortB", which makes it hard to sort the data by the number of database bytes requested. Now, my guess is that WS picks up the FIRST packet in the conversation and makes the source A and target B. Would be really beneficial to be able to right-click on a line item and swap A for B, to then make sorting the data a bit more meaningful. Swapping wouldn't need to be saved -- could be stored only as long as the data is available in the dialog box.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-endpoints" rel="tag" title="see questions tagged &#39;endpoints&#39;">endpoints</span> <span class="post-tag tag-link-enhancement" rel="tag" title="see questions tagged &#39;enhancement&#39;">enhancement</span> <span class="post-tag tag-link-conversationlist" rel="tag" title="see questions tagged &#39;conversationlist&#39;">conversationlist</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '11, 10:10</strong></p><img src="https://secure.gravatar.com/avatar/a049f24820d77d7b560a4f67f02541b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BtrieveBill&#39;s gravatar image" /><p><span>BtrieveBill</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BtrieveBill has no accepted answers">0%</span></p></div></div><div id="comments-container-1751" class="comments-container"><span id="1756"></span><div id="comment-1756" class="comment"><div id="post-1756-score" class="comment-score"></div><div class="comment-text"><p>The best places for enhancement requests would either be:</p><p>1) the wireshark-users mailing list (if you're not asking for a feature to use when writing Wireshark code) or the wireshark-dev mailing list (if you <em>are</em> asking for such a feature)</p><p>or</p><p>2) <a href="http://bugs.wireshark.org">the Wireshark bugs database</a> - file it as an enhancement.</p></div><div id="comment-1756-info" class="comment-info"><span class="comment-age">(14 Jan '11, 17:49)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-1751" class="comment-tools"></div><div class="clear"></div><div id="comment-1751-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

