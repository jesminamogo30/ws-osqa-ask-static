+++
type = "question"
title = "How can i capture http url and save them into file or DB"
description = '''Please help, i want to capture all outgoing HTTP URL of another computer , just i know its IP adress. can i use this with wireshark? what have i to put as a command ? thank you for helping'''
date = "2015-01-24T17:07:00Z"
lastmod = "2015-01-24T17:07:00Z"
weight = 39387
keywords = [ "url", "capture", "http" ]
aliases = [ "/questions/39387" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can i capture http url and save them into file or DB](/questions/39387/how-can-i-capture-http-url-and-save-them-into-file-or-db)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39387-score" class="post-score" title="current number of votes">0</div><span id="post-39387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Please help,</p><p>i want to capture all outgoing HTTP URL of another computer , just i know its IP adress.</p><p>can i use this with wireshark? what have i to put as a command ?</p><p>thank you for helping</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-url" rel="tag" title="see questions tagged &#39;url&#39;">url</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '15, 17:07</strong></p><img src="https://secure.gravatar.com/avatar/80e28b24a50e283ac5a0fdde7f11948a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ben%20othman&#39;s gravatar image" /><p><span>ben othman</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ben othman has no accepted answers">0%</span></p></div></div><div id="comments-container-39387" class="comments-container"></div><div id="comment-tools-39387" class="comment-tools"></div><div class="clear"></div><div id="comment-39387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

