+++
type = "question"
title = "how to capture from second machine?"
description = '''hello I am trying to capture packages from another computer to get it all on the same PC. the other does not have wireshark installed, also need to install it or is not necessary? If this is possible can someone explain how to do this? '''
date = "2011-09-06T09:00:00Z"
lastmod = "2011-09-06T09:21:00Z"
weight = 6127
keywords = [ "capture" ]
aliases = [ "/questions/6127" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to capture from second machine?](/questions/6127/how-to-capture-from-second-machine)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6127-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6127-score" class="post-score" title="current number of votes">0</div><span id="post-6127-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello I am trying to capture packages from another computer to get it all on the same PC. the other does not have wireshark installed, also need to install it or is not necessary? If this is possible can someone explain how to do this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Sep '11, 09:00</strong></p><img src="https://secure.gravatar.com/avatar/af6285581efb4cafcf9153495fc6cd08?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kixote&#39;s gravatar image" /><p><span>kixote</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kixote has no accepted answers">0%</span></p></div></div><div id="comments-container-6127" class="comments-container"></div><div id="comment-tools-6127" class="comment-tools"></div><div class="clear"></div><div id="comment-6127-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6128"></span>

<div id="answer-container-6128" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6128-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6128-score" class="post-score" title="current number of votes">0</div><span id="post-6128-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The <a href="http://www.wireshark.org/docs/wsug_html_chunked/" title="User&#39;s Guide">documentation</a> is a good place to start. Please read the <a href="http://wiki.wireshark.org/CaptureSetup" title="CaptureSetup">Capture Setup</a> article on the <a href="http://wiki.wireshark.org/" title="http://wiki.wireshark.org/">Wireshark Wiki</a>. If you have a specific question, we can be much more helpful (see the <a href="http://ask.wireshark.org/faq/" title="FAQ">faq</a>).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Sep '11, 09:21</strong></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="multipleinterfaces has 9 accepted answers">12%</span></p></div></div><div id="comments-container-6128" class="comments-container"></div><div id="comment-tools-6128" class="comment-tools"></div><div class="clear"></div><div id="comment-6128-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

