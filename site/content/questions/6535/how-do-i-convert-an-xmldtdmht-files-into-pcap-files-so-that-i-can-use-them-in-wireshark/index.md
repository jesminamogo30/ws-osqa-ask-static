+++
type = "question"
title = "[closed] How do I convert an xml,dtd,mht files into pcap files so that I can use them in wireshark?"
description = '''How do I convert an xml,dtd,mht files into pcap files so that I can use them in wireshark?'''
date = "2011-09-23T22:17:00Z"
lastmod = "2011-09-24T09:16:00Z"
weight = 6535
keywords = [ "conversion", "file" ]
aliases = [ "/questions/6535" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How do I convert an xml,dtd,mht files into pcap files so that I can use them in wireshark?](/questions/6535/how-do-i-convert-an-xmldtdmht-files-into-pcap-files-so-that-i-can-use-them-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6535-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6535-score" class="post-score" title="current number of votes">-1</div><span id="post-6535-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I convert an xml,dtd,mht files into pcap files so that I can use them in wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-conversion" rel="tag" title="see questions tagged &#39;conversion&#39;">conversion</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '11, 22:17</strong></p><img src="https://secure.gravatar.com/avatar/c953cf9b75fef0837c81691a031c1af7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deepanjan&#39;s gravatar image" /><p><span>deepanjan</span><br />
<span class="score" title="0 reputation points">0</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deepanjan has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>24 Sep '11, 15:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-6535" class="comments-container"><span id="6536"></span><div id="comment-6536" class="comment"><div id="post-6536-score" class="comment-score">1</div><div class="comment-text"><p>Most probably by writing code.</p></div><div id="comment-6536-info" class="comment-info"><span class="comment-age">(24 Sep '11, 01:41)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="6538"></span><div id="comment-6538" class="comment"><div id="post-6538-score" class="comment-score"></div><div class="comment-text"><p>You already asked this in <a href="http://ask.wireshark.org/questions/6521/file-conversion">question 6521</a>; no need to ask it again.</p></div><div id="comment-6538-info" class="comment-info"><span class="comment-age">(24 Sep '11, 09:16)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-6535" class="comment-tools"></div><div class="clear"></div><div id="comment-6535-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by helloworld 24 Sep '11, 15:16

</div>

</div>

</div>

