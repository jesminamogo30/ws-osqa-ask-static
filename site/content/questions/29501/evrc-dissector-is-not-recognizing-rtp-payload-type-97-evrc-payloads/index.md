+++
type = "question"
title = "EVRC dissector is not recognizing RTP Payload Type-97 EVRC payloads"
description = '''I have Wireshark 1.10.5 and it is not decoding the EVRC payload in RTP dynamic payload type 97 packets as per RFC3558. My search results seem to indicate Wireshark should automagically detect and decode this but it does not. There are no settings on the EVRC protocol preferences to instruct the diss...'''
date = "2014-02-06T14:56:00Z"
lastmod = "2014-02-06T14:56:00Z"
weight = 29501
keywords = [ "decode", "rfc3558", "evrc", "rtp" ]
aliases = [ "/questions/29501" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [EVRC dissector is not recognizing RTP Payload Type-97 EVRC payloads](/questions/29501/evrc-dissector-is-not-recognizing-rtp-payload-type-97-evrc-payloads)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29501-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29501-score" class="post-score" title="current number of votes">0</div><span id="post-29501-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have Wireshark 1.10.5 and it is not decoding the EVRC payload in RTP dynamic payload type 97 packets as per RFC3558. My search results seem to indicate Wireshark should automagically detect and decode this but it does not. There are no settings on the EVRC protocol preferences to instruct the dissector which RTP payload type to use. How do I get Wireshark to perform this function?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-rfc3558" rel="tag" title="see questions tagged &#39;rfc3558&#39;">rfc3558</span> <span class="post-tag tag-link-evrc" rel="tag" title="see questions tagged &#39;evrc&#39;">evrc</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '14, 14:56</strong></p><img src="https://secure.gravatar.com/avatar/0025921f9cf6cee74091fe5792df456f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vandev1&#39;s gravatar image" /><p><span>vandev1</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vandev1 has no accepted answers">0%</span></p></div></div><div id="comments-container-29501" class="comments-container"></div><div id="comment-tools-29501" class="comment-tools"></div><div class="clear"></div><div id="comment-29501-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

