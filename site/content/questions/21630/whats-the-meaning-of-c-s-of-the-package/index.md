+++
type = "question"
title = "what&#x27;s the meaning of &quot;C： S: &quot; of the package?"
description = '''some packages like these: The following dialog illustrates how a client and server can start a  TLS session: S: &amp;lt;waits for=&quot;&quot; connection=&quot;&quot; on=&quot;&quot; tcp=&quot;&quot; port=&quot;&quot; 25=&quot;&quot;&amp;gt; C: &amp;lt;opens connection=&quot;&quot;&amp;gt; S: 220 mail.imc.org SMTP service ready C: EHLO mail.ietf.org S: 250-mail.imc.org offers a warm ...'''
date = "2013-05-30T20:38:00Z"
lastmod = "2013-05-31T20:16:00Z"
weight = 21630
keywords = [ "packge", "tag" ]
aliases = [ "/questions/21630" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [what's the meaning of "C： S: " of the package?](/questions/21630/whats-the-meaning-of-c-s-of-the-package)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21630-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21630-score" class="post-score" title="current number of votes">0</div><span id="post-21630-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>some packages like these:</p><p>The following dialog illustrates how a client and server can start a TLS session:</p><p>S: &lt;waits for="" connection="" on="" tcp="" port="" 25=""&gt;</p><p>C: &lt;opens connection=""&gt;</p><p>S: 220 mail.imc.org SMTP service ready</p><p>C: EHLO mail.ietf.org</p><p>S: 250-mail.imc.org offers a warm hug of welcome</p><p>S: 250 STARTTLS</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packge" rel="tag" title="see questions tagged &#39;packge&#39;">packge</span> <span class="post-tag tag-link-tag" rel="tag" title="see questions tagged &#39;tag&#39;">tag</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 May '13, 20:38</strong></p><img src="https://secure.gravatar.com/avatar/2963ae435efc2f235180e2ea2f50082a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vivianlawrence&#39;s gravatar image" /><p><span>vivianlawrence</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vivianlawrence has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 May '13, 20:39</strong> </span></p></div></div><div id="comments-container-21630" class="comments-container"></div><div id="comment-tools-21630" class="comment-tools"></div><div class="clear"></div><div id="comment-21630-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="21631"></span>

<div id="answer-container-21631" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21631-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21631-score" class="post-score" title="current number of votes">2</div><span id="post-21631-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Umm, C: is Client and S: is Server, perhaps?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 May '13, 21:08</strong></p><img src="https://secure.gravatar.com/avatar/b260fb38b621169269b5030f1ed6b766?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="griff&#39;s gravatar image" /><p><span>griff</span><br />
<span class="score" title="361 reputation points">361</span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="griff has 2 accepted answers">10%</span></p></div></div><div id="comments-container-21631" class="comments-container"><span id="21637"></span><div id="comment-21637" class="comment"><div id="post-21637-score" class="comment-score"></div><div class="comment-text"><p>Oh it should be. Something must be wrong with my head. I was thinking C means content, and that confused me a lot.</p></div><div id="comment-21637-info" class="comment-info"><span class="comment-age">(31 May '13, 00:48)</span> <span class="comment-user userinfo">vivianlawrence</span></div></div></div><div id="comment-tools-21631" class="comment-tools"></div><div class="clear"></div><div id="comment-21631-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="21686"></span>

<div id="answer-container-21686" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21686-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21686-score" class="post-score" title="current number of votes">1</div><span id="post-21686-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That output is from <a href="http://tools.ietf.org/html/rfc2487">RFC 2487</a>. It's kind of funny to say this, but as per RFC 2487 section 6, the letters S and C refer to an SMTP Server and an SMTP Client.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 May '13, 20:16</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 May '13, 21:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-21686" class="comments-container"></div><div id="comment-tools-21686" class="comment-tools"></div><div class="clear"></div><div id="comment-21686-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

