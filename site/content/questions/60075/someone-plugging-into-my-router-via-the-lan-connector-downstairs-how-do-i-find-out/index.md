+++
type = "question"
title = "Someone plugging into my router via the lan connector downstairs. How do I find out"
description = '''Hi, I am a Newbe to Wireshark. I live in a shared house, with a another Tenant&#x27;s guest causing problems. Alto of targeted Adds are streaming onto my PC from illegal Gambling Betting Websites. Only one person I know has a serious gambling problem and they are a guest of another Tenant. Is there a way...'''
date = "2017-03-14T13:16:00Z"
lastmod = "2017-03-20T23:27:00Z"
weight = 60075
keywords = [ "malware", "pua", "pub" ]
aliases = [ "/questions/60075" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Someone plugging into my router via the lan connector downstairs. How do I find out](/questions/60075/someone-plugging-into-my-router-via-the-lan-connector-downstairs-how-do-i-find-out)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60075-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60075-score" class="post-score" title="current number of votes">0</div><span id="post-60075-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am a Newbe to Wireshark. I live in a shared house, with a another Tenant's guest causing problems.</p><p>Alto of targeted Adds are streaming onto my PC from illegal Gambling Betting Websites. Only one person I know has a serious gambling problem and they are a guest of another Tenant.</p><p>Is there a way I can get proof they are plugging into my router via the lan connector downstairs, when I cannot watch them in person. I access router only by Wifi from another room.</p><p>I already blocked their Laptop's Ip address via wifi already, when I could get the name of their PC (MAC id).</p><p>I think they are also connecting via and Android device, but I have no way of knowing which device belongs to who, only my own. And I cannot block them one by one to find out(wish I could !).</p><p>If I change the password they have a way of getting it again, via a Rouge Tenant. I need to permanently block devices on Router, via unique IDs.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-malware" rel="tag" title="see questions tagged &#39;malware&#39;">malware</span> <span class="post-tag tag-link-pua" rel="tag" title="see questions tagged &#39;pua&#39;">pua</span> <span class="post-tag tag-link-pub" rel="tag" title="see questions tagged &#39;pub&#39;">pub</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Mar '17, 13:16</strong></p><img src="https://secure.gravatar.com/avatar/975b808cf22ac509877fa20f07119915?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="S%20Parxz&#39;s gravatar image" /><p><span>S Parxz</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="S Parxz has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Mar '17, 10:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span></p></div></div><div id="comments-container-60075" class="comments-container"></div><div id="comment-tools-60075" class="comment-tools"></div><div class="clear"></div><div id="comment-60075-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="60092"></span>

<div id="answer-container-60092" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60092-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60092-score" class="post-score" title="current number of votes">1</div><span id="post-60092-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hello S Parxz</p><p>Just to verify the situation:</p><ul><li>Multiple users share one Internet connection</li><li>You are receiving unwanted adds on your PC</li></ul><p>To publish these adds on <em>your</em> PC by connecting a system to your system is technically possible. The <a href="http://neighbor.willhackforsushi.com/hacking-friends.pdf">love-my-neighbor</a> toolkit does this out-of-the-box (for tech-savvy people, at least). Since you mentioned a shared house I would expect a decent amount of laughter, mocking questions or angry stares whenever that toolkit is in use.</p><p>I have investigated similar issues on a number of occasions. In more than 99.5% of cases the unwanted apps were caused by software on the victim PC (that would be yours).</p><p>In all of these cases the user tried to download some desired and legitimate program from a software portal or download site. Unfortunately the installer was bundled to deliver, what anti virus companies call "PUA" or "PUP" (Potentially Unwanted Application or Potentially Unwanted Program).</p><p>The PUAs were installed, before the desired application. One case even involved a copy of Wireshark, that was hosted on a shady web site. Since the desired application will be installed, the user hardly notices the extra stuff.</p><p>One form of PUA is a <a href="https://en.wikipedia.org/wiki/Browser_hijacking">browser hijacker</a>. The adds can be caused by a plugin to your browser, by a separate program, or by a combination of both. I have encountered PUAs with the capability to download more programs and install them as a service, thus compromising the whole computer.</p><p>I strongly suggest the following steps:</p><ul><li>Install all available updates for the operating system on your computer</li><li>Reinstall your workstation with a recent operating system, if you are using Windows XP or another outdated operating system</li><li>Patch all applications (Firefox, Chrome, Flash, Silverlight, PDF reader, office applications etc.)</li><li>Install a virus scanner and load the latest pattern files</li><li>Run a scan on your computer and rigorously kill anything that triggers the virus scanner</li><li>If you use Windows, run the Malicious Software Removal tool provided for free from Microsoft.</li><li>Be vigilant when downloading software from a web site: Always deny the little extras that might be offered by the installer</li><li><strong>Always</strong> stay away from programs that claim to fix any kind of problem, speed up your PC, or perform other tricks</li><li><strong>Never</strong> run programs that will fix any kind of problem or tune your software.</li></ul><p>If in doubt involve an IT specialist to get your system cleaned. A reinstallation might be your best way out.</p><p>Good hunting</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Mar '17, 10:46</strong></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packethunter has 8 accepted answers">8%</span></p></div></div><div id="comments-container-60092" class="comments-container"></div><div id="comment-tools-60092" class="comment-tools"></div><div class="clear"></div><div id="comment-60092-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="60210"></span>

<div id="answer-container-60210" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60210-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60210-score" class="post-score" title="current number of votes">0</div><span id="post-60210-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>LavaSoft's AdAware is good at finding out all kinds of malware.</p><p>The free version will only discover it after it's on your PC.</p><p>The paid version will keep it off of your PC in the first place.</p><p>Cheers,</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '17, 23:27</strong></p><img src="https://secure.gravatar.com/avatar/6c8f0de8cb4ef9ad7093eefe24030e4b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wbenton&#39;s gravatar image" /><p><span>wbenton</span><br />
<span class="score" title="29 reputation points">29</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wbenton has no accepted answers">0%</span></p></div></div><div id="comments-container-60210" class="comments-container"></div><div id="comment-tools-60210" class="comment-tools"></div><div class="clear"></div><div id="comment-60210-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

