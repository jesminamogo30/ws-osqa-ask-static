+++
type = "question"
title = "Errors when trying to do a make"
description = '''I&#x27;ve got this make error now, let me know if you see an easy solution.  $ &quot;makefile&quot;, line 4155: need an operator $ &quot;makefile&quot;, line 4157: need an operator $ make: fatal errors encountered -- cannot continue  not sure what to do besides opening the makefile and taking a look, I get this error execut...'''
date = "2014-03-09T07:20:00Z"
lastmod = "2014-03-09T15:46:00Z"
weight = 30617
keywords = [ "makefile" ]
aliases = [ "/questions/30617" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Errors when trying to do a make](/questions/30617/errors-when-trying-to-do-a-make)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30617-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30617-score" class="post-score" title="current number of votes">0</div><span id="post-30617-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've got this make error now, let me know if you see an easy solution.</p><ol><li>$ "makefile", line 4155: need an operator</li><li>$ "makefile", line 4157: need an operator</li><li>$ make: fatal errors encountered -- cannot continue</li></ol><p>not sure what to do besides opening the makefile and taking a look, I get this error executing make, make install, and make clean</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-makefile" rel="tag" title="see questions tagged &#39;makefile&#39;">makefile</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Mar '14, 07:20</strong></p><img src="https://secure.gravatar.com/avatar/4f713d2d9d447724a6f15177137d0e1f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="earthworm987&#39;s gravatar image" /><p><span>earthworm987</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="earthworm987 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted <strong>09 Mar '14, 15:22</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-30617" class="comments-container"><span id="30625"></span><div id="comment-30625" class="comment"><div id="post-30625-score" class="comment-score"></div><div class="comment-text"><p>Are you building from a tree you've checked out from Git (or Subversion, but the Subversion repository is no longer being maintained, so you shouldn't check out from Subversion any more), a source tree for a release, or something else?</p></div><div id="comment-30625-info" class="comment-info"><span class="comment-age">(09 Mar '14, 15:46)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-30617" class="comment-tools"></div><div class="clear"></div><div id="comment-30617-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

