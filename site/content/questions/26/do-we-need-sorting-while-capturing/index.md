+++
type = "question"
title = "Do we need sorting while capturing?"
description = '''Currently we do not support sorting while capturing very well. Is this a wanted feature?'''
date = "2010-09-10T00:20:00Z"
lastmod = "2010-09-13T17:43:00Z"
weight = 26
keywords = [ "features" ]
aliases = [ "/questions/26" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Do we need sorting while capturing?](/questions/26/do-we-need-sorting-while-capturing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26-score" class="post-score" title="current number of votes">0</div><span id="post-26-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Currently we do not support sorting while capturing very well. Is this a wanted feature?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-features" rel="tag" title="see questions tagged &#39;features&#39;">features</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Sep '10, 00:20</strong></p><img src="https://secure.gravatar.com/avatar/193a8e6acdc05d13fb1e59fbe4eaba1e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stig&#39;s gravatar image" /><p><span>stig ♦</span><br />
<span class="score" title="46 reputation points">46</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stig has no accepted answers">0%</span></p></div></div><div id="comments-container-26" class="comments-container"></div><div id="comment-tools-26" class="comment-tools"></div><div class="clear"></div><div id="comment-26-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="49"></span>

<div id="answer-container-49" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49-score" class="post-score" title="current number of votes">4</div><span id="post-49-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not sure it wouldn't be more confusing than useful - I can usually get to see what I want to see in a real-time capture with the careful use of filters</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Sep '10, 17:43</strong></p><img src="https://secure.gravatar.com/avatar/55ef5cc8aa8d67fe35830d44aa2a1d5b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rednectar&#39;s gravatar image" /><p><span>rednectar</span><br />
<span class="score" title="61 reputation points">61</span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rednectar has no accepted answers">0%</span></p></div></div><div id="comments-container-49" class="comments-container"></div><div id="comment-tools-49" class="comment-tools"></div><div class="clear"></div><div id="comment-49-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="43"></span>

<div id="answer-container-43" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43-score" class="post-score" title="current number of votes">1</div><span id="post-43-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have not seen this type of functionality implemented on any of the tools I am familiar with and so I have a hard time imagining how this would function. The live capture screen is often scrolling by so quickly that any kind of real time sorting would be difficult and may just contribute confusion.</p><p>Implemented in a pop-up window like the "Conversations" window makes more sense and seems to function well today.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Sep '10, 06:20</strong></p><img src="https://secure.gravatar.com/avatar/1d8eda08758411bec29092a0b8220126?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Peter&#39;s gravatar image" /><p><span>Peter</span><br />
<span class="score" title="65 reputation points">65</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Peter has no accepted answers">0%</span></p></div></div><div id="comments-container-43" class="comments-container"><span id="47"></span><div id="comment-47" class="comment"><div id="post-47-score" class="comment-score"></div><div class="comment-text"><p>I've wondered for a while if it would be useful to promote the conversation and/or endpoint views to the main window.</p></div><div id="comment-47-info" class="comment-info"><span class="comment-age">(13 Sep '10, 14:50)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div><span id="48"></span><div id="comment-48" class="comment"><div id="post-48-score" class="comment-score"></div><div class="comment-text"><p>Keeping the GUI from becoming cluttered could be a challenge. Perhaps a check-box to make the Conversations window "always on top"?</p></div><div id="comment-48-info" class="comment-info"><span class="comment-age">(13 Sep '10, 17:29)</span> <span class="comment-user userinfo">Peter</span></div></div></div><div id="comment-tools-43" class="comment-tools"></div><div class="clear"></div><div id="comment-43-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

