+++
type = "question"
title = "Different display Hercules vs Wireshark"
description = '''I am new with Wireshark. My question is related to simple TCP communication between Hercules utility and other peer (another computer). When send some data from Hercules (TCP client), I receive answer. However data, answered I cannot find at Wireshark display. My display filter is ip address of &quot;non...'''
date = "2014-10-14T01:35:00Z"
lastmod = "2014-10-14T04:42:00Z"
weight = 37027
keywords = [ "hercules" ]
aliases = [ "/questions/37027" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Different display Hercules vs Wireshark](/questions/37027/different-display-hercules-vs-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37027-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37027-score" class="post-score" title="current number of votes">0</div><span id="post-37027-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am new with Wireshark. My question is related to simple TCP communication between Hercules utility and other peer (another computer). When send some data from Hercules (TCP client), I receive answer. However data, answered I cannot find at Wireshark display. My display filter is ip address of "non Hercules" end (peer). I can find data send from Hercules, but response data I don't find. Where I go wrong.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hercules" rel="tag" title="see questions tagged &#39;hercules&#39;">hercules</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Oct '14, 01:35</strong></p><img src="https://secure.gravatar.com/avatar/31b4dfb8befab0e47affe11f17589ad0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DiGiorgio&#39;s gravatar image" /><p><span>DiGiorgio</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DiGiorgio has no accepted answers">0%</span></p></div></div><div id="comments-container-37027" class="comments-container"><span id="37033"></span><div id="comment-37033" class="comment"><div id="post-37033-score" class="comment-score"></div><div class="comment-text"><p>Can you see the answer when you clear the display filter? What is the display filter expression you use?</p></div><div id="comment-37033-info" class="comment-info"><span class="comment-age">(14 Oct '14, 04:07)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="37036"></span><div id="comment-37036" class="comment"><div id="post-37036-score" class="comment-score"></div><div class="comment-text"><p>My display filter is ip.addr == 192.168.1.56 (which is "non Hercules" peer. I don't see them without display filter, either. Edit/FindPacket - HexValue -01111f gives nothing, although Hercules displays it.</p></div><div id="comment-37036-info" class="comment-info"><span class="comment-age">(14 Oct '14, 04:42)</span> <span class="comment-user userinfo">DiGiorgio</span></div></div></div><div id="comment-tools-37027" class="comment-tools"></div><div class="clear"></div><div id="comment-37027-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

