+++
type = "question"
title = "Track other ip&#x27;s or mac addresses"
description = '''Hello, I watch the packets tracking, but they only refer to my main network device . I would like to track other devices (smartphones, laptops) that are connected to my wifi network but i see only my ip tracked. Is there a way to manage it?  Is there also a way to log their internet history browsing...'''
date = "2017-04-21T15:09:00Z"
lastmod = "2017-04-22T15:10:00Z"
weight = 60953
keywords = [ "device", "tracking" ]
aliases = [ "/questions/60953" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Track other ip's or mac addresses](/questions/60953/track-other-ips-or-mac-addresses)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60953-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60953-score" class="post-score" title="current number of votes">0</div><span id="post-60953-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I watch the packets tracking, but they only refer to my main network device . I would like to track other devices (smartphones, laptops) that are connected to my wifi network but i see only my ip tracked. Is there a way to manage it? Is there also a way to log their internet history browsing?</p><p>Thanks in advance Theo Koletsis</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-device" rel="tag" title="see questions tagged &#39;device&#39;">device</span> <span class="post-tag tag-link-tracking" rel="tag" title="see questions tagged &#39;tracking&#39;">tracking</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Apr '17, 15:09</strong></p><img src="https://secure.gravatar.com/avatar/9e26c867d98ce40a048ea60de5c4ada9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tkoletsis&#39;s gravatar image" /><p><span>tkoletsis</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tkoletsis has no accepted answers">0%</span></p></div></div><div id="comments-container-60953" class="comments-container"></div><div id="comment-tools-60953" class="comment-tools"></div><div class="clear"></div><div id="comment-60953-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60957"></span>

<div id="answer-container-60957" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60957-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60957-score" class="post-score" title="current number of votes">0</div><span id="post-60957-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You haven't provided much information about your capture environment - OS, Wireshark version, capture hardware, etc. - but if you haven't done so already, I'd suggest reading the information provided at the <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">Wireshark WLAN (IEEE 802.11) capture setup</a> wiki page.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Apr '17, 20:22</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-60957" class="comments-container"><span id="60958"></span><div id="comment-60958" class="comment"><div id="post-60958-score" class="comment-score"></div><div class="comment-text"><p>Yes of course</p><p>Windows 7, wireshark version 2.2.6 The devices on my wifi network are : laptops , tablets, androids</p><p>When wireshark opens and checks for interfaces , finds 3 wireless networks , 1 of them only has traffic, and the address below it is only the ip of my main laptop.</p><p>All the other devices are not appearing, and i am not able to capture their traffic</p><p>Thanks again</p></div><div id="comment-60958-info" class="comment-info"><span class="comment-age">(22 Apr '17, 01:39)</span> <span class="comment-user userinfo">tkoletsis</span></div></div><span id="60960"></span><div id="comment-60960" class="comment"><div id="post-60960-score" class="comment-score"></div><div class="comment-text"><p>i have unchecked "Enabled promiscuous mode on all interfaces" in the capture options, some of the devices appear now , but not all .</p><p>I am not sure what's happening</p></div><div id="comment-60960-info" class="comment-info"><span class="comment-age">(22 Apr '17, 02:20)</span> <span class="comment-user userinfo">tkoletsis</span></div></div><span id="60973"></span><div id="comment-60973" class="comment"><div id="post-60973-score" class="comment-score"></div><div class="comment-text"><p>So did you read the <a href="https://wiki.wireshark.org/CaptureSetup/WLAN#Windows"><strong>Windows</strong> section</a> of the WLAN capture setup wiki page? Your network adaptor needs to be placed in monitor mode; promiscuous mode isn't the answer for capturing the WLAN traffic you seek. The wiki page mentions npcap as a potential solution, or you can try purchasing an AirPcap adaptor. You don't mention if you tried either of these solutions.</p><p>If you don't care which capture tool you use, you could also try using <a href="https://www.microsoft.com/en-us/download/details.aspx?id=44226">Microsoft Message Analyzer</a> or its predecessor <a href="https://www.microsoft.com/en-us/download/details.aspx?id=4865">Microsoft Network Monitor</a> instead of Wireshark; I <em>believe</em> either of them are capable of capturing the traffic you're seeking, but you will have to try one (or both) to find out for sure.</p></div><div id="comment-60973-info" class="comment-info"><span class="comment-age">(22 Apr '17, 15:10)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-60957" class="comment-tools"></div><div class="clear"></div><div id="comment-60957-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

