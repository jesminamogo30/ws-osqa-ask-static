+++
type = "question"
title = "WireShark protocol help"
description = '''hello I have looked into wireshark for a short time now and I saw it&#x27;s an app to secure networks and such. My friend sent me a pcap file today (he told me about wireshark) and asked me to trace him in this pcap file, but i havent managed to. can someone tell me how i can identify him in the pcap fil...'''
date = "2016-09-26T05:36:00Z"
lastmod = "2016-09-26T05:45:00Z"
weight = 55831
keywords = [ "protocol", "help", "wireshark" ]
aliases = [ "/questions/55831" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark protocol help](/questions/55831/wireshark-protocol-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55831-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55831-score" class="post-score" title="current number of votes">0</div><span id="post-55831-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello I have looked into wireshark for a short time now and I saw it's an app to secure networks and such. My friend sent me a pcap file today (he told me about wireshark) and asked me to trace him in this pcap file, but i havent managed to. can someone tell me how i can identify him in the pcap file, thank you.</p><p>this is link: <a href="https://drive.google.com/open?id=0B94k1Bz5s_fRWTBIdDZRQzhXM2s">https://drive.google.com/open?id=0B94k1Bz5s_fRWTBIdDZRQzhXM2s</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '16, 05:36</strong></p><img src="https://secure.gravatar.com/avatar/c165ac5b50ce5245e981d75ad5bdf14f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pacbell86&#39;s gravatar image" /><p><span>pacbell86</span><br />
<span class="score" title="2 reputation points">2</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pacbell86 has no accepted answers">0%</span></p></div></div><div id="comments-container-55831" class="comments-container"><span id="55834"></span><div id="comment-55834" class="comment"><div id="post-55834-score" class="comment-score"></div><div class="comment-text"><p>I like the rephrasing of the question ;)</p></div><div id="comment-55834-info" class="comment-info"><span class="comment-age">(26 Sep '16, 05:45)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-55831" class="comment-tools"></div><div class="clear"></div><div id="comment-55831-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

