+++
type = "question"
title = "Exercise One.pcap letters &quot;b&quot; and &quot;g&quot;"
description = '''I am working on Exercise One.pcap letters &quot;b&quot; and &quot;g&quot; and am having difficulty as I am new to the product. b) What is happening in frames 3, 4, and 5? What exactly is a frame and how do I view these frames? g) After the initial set of packets is received, the client sends out a new request in packet...'''
date = "2013-08-18T09:32:00Z"
lastmod = "2013-08-19T06:57:00Z"
weight = 23839
keywords = [ "new", "user" ]
aliases = [ "/questions/23839" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Exercise One.pcap letters "b" and "g"](/questions/23839/exercise-onepcap-letters-b-and-g)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23839-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23839-score" class="post-score" title="current number of votes">-1</div><span id="post-23839-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am working on Exercise One.pcap letters "b" and "g" and am having difficulty as I am new to the product.</p><p>b) What is happening in frames 3, 4, and 5?</p><p>What exactly is a frame and how do I view these frames?</p><p>g) After the initial set of packets is received, the client sends out a new request in packet 12. This occurs automatically without any action by the user. Why does this occur? See the first "hint" to the left.</p><p>I see line number 12, is that the packet number? What and where is the "hint" on the left?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-new" rel="tag" title="see questions tagged &#39;new&#39;">new</span> <span class="post-tag tag-link-user" rel="tag" title="see questions tagged &#39;user&#39;">user</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Aug '13, 09:32</strong></p><img src="https://secure.gravatar.com/avatar/43f588e40e2934f2d9bf34f7fe623971?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ShawnLacey&#39;s gravatar image" /><p><span>ShawnLacey</span><br />
<span class="score" title="10 reputation points">10</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ShawnLacey has no accepted answers">0%</span></p></div></div><div id="comments-container-23839" class="comments-container"><span id="23840"></span><div id="comment-23840" class="comment"><div id="post-23840-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I am working on Exercise One</p></blockquote><p>That's good!</p><p>However: Without any reference, how are we supposed to know what <strong>Exercise One</strong> is?</p><p>&lt;fun&gt;<br />
Is that something related to the President of the United States? <strong>Exercise One</strong>, sounds like <strong>Airforce One</strong>? Are you trying to solve a problem that is meant to be solved by the President ;-))<br />
&lt;/fun&gt;</p></div><div id="comment-23840-info" class="comment-info"><span class="comment-age">(18 Aug '13, 14:22)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="23844"></span><div id="comment-23844" class="comment"><div id="post-23844-score" class="comment-score"></div><div class="comment-text"><p>Kurt,</p><p>Thanks for getting back to me.</p><p>I was hoping maybe should could shed some light on what they're talking about as far as 1. What exactly is a frame and how do I view these frames?</p><p>2. After the initial set of packets is received, the client sends out a new request. This occurs automatically without any action by the user. Why does this occur? See the first "hint" to the left.</p><p>Any insight at all to what the heck they're talking about?</p></div><div id="comment-23844-info" class="comment-info"><span class="comment-age">(18 Aug '13, 16:23)</span> <span class="comment-user userinfo">ShawnLacey</span></div></div><span id="23848"></span><div id="comment-23848" class="comment"><div id="post-23848-score" class="comment-score"></div><div class="comment-text"><p>Shawn, without the text of the Exercise it is impossible to help you, as there are references to packet/frames and communications. What kind of Exercise is this?</p></div><div id="comment-23848-info" class="comment-info"><span class="comment-age">(19 Aug '13, 06:57)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-23839" class="comment-tools"></div><div class="clear"></div><div id="comment-23839-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

