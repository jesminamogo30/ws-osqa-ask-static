+++
type = "question"
title = "capture Serial port (COM) communications in wireshark"
description = '''Can wireshark capture the communication between the devices,communication made using serial port emulation program(Terminal v1.9b)? Is it possible to add the feature to capture that communication? if you need more information send me an email at badgp11208@gmail.com Any information related could be ...'''
date = "2014-02-27T05:55:00Z"
lastmod = "2014-02-28T07:41:00Z"
weight = 30231
keywords = [ "lua", "serial-port", "wireshark" ]
aliases = [ "/questions/30231" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [capture Serial port (COM) communications in wireshark](/questions/30231/capture-serial-port-com-communications-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30231-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30231-score" class="post-score" title="current number of votes">0</div><span id="post-30231-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can wireshark capture the communication between the devices,communication made using serial port emulation program(Terminal v1.9b)? Is it possible to add the feature to capture that communication?</p><p>if you need more information send me an email at <span class="__cf_email__" data-cfemail="aac8cbcecdda9b9b989a92eacdc7cbc3c684c9c5c7">[email protected]</span></p><p>Any information related could be helpful.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-serial-port" rel="tag" title="see questions tagged &#39;serial-port&#39;">serial-port</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '14, 05:55</strong></p><img src="https://secure.gravatar.com/avatar/a717f574f006b4977952372192a67e7f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amrit&#39;s gravatar image" /><p><span>Amrit</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amrit has no accepted answers">0%</span></p></div></div><div id="comments-container-30231" class="comments-container"></div><div id="comment-tools-30231" class="comment-tools"></div><div class="clear"></div><div id="comment-30231-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30272"></span>

<div id="answer-container-30272" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30272-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30272-score" class="post-score" title="current number of votes">2</div><span id="post-30272-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See my answer to a similar question</p><blockquote><p><a href="http://ask.wireshark.org/questions/23243/how-do-you-capture-serial-com-communications">http://ask.wireshark.org/questions/23243/how-do-you-capture-serial-com-communications</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Feb '14, 07:41</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-30272" class="comments-container"></div><div id="comment-tools-30272" class="comment-tools"></div><div class="clear"></div><div id="comment-30272-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

