+++
type = "question"
title = "libsmi-svn-40773-win32ws.zip will not download"
description = '''Everytime I try to compile WireShark, or download the file from any of the http://anonsvn.wireshark.org/wireshark-win32-libs/tags/2013-03-08/packages/ repositories, it fails. I have tried every version I can fine on the ANONSVN site and each one only downloads 180KB, and fails when I try to open it....'''
date = "2014-06-23T11:19:00Z"
lastmod = "2014-06-24T05:18:00Z"
weight = 34089
keywords = [ "libsmi" ]
aliases = [ "/questions/34089" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [libsmi-svn-40773-win32ws.zip will not download](/questions/34089/libsmi-svn-40773-win32wszip-will-not-download)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34089-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34089-score" class="post-score" title="current number of votes">0</div><span id="post-34089-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Everytime I try to compile WireShark, or download the file from any of the <a href="http://anonsvn.wireshark.org/wireshark-win32-libs/tags/2013-03-08/packages/">http://anonsvn.wireshark.org/wireshark-win32-libs/tags/2013-03-08/packages/</a> repositories, it fails. I have tried every version I can fine on the ANONSVN site and each one only downloads 180KB, and fails when I try to open it. Is there another location I can get this file?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-libsmi" rel="tag" title="see questions tagged &#39;libsmi&#39;">libsmi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jun '14, 11:19</strong></p><img src="https://secure.gravatar.com/avatar/2df1a3a844218721dd632bbfc6a1ab3d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zh00S&#39;s gravatar image" /><p><span>Zh00S</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zh00S has no accepted answers">0%</span></p></div></div><div id="comments-container-34089" class="comments-container"></div><div id="comment-tools-34089" class="comment-tools"></div><div class="clear"></div><div id="comment-34089-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34107"></span>

<div id="answer-container-34107" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34107-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34107-score" class="post-score" title="current number of votes">1</div><span id="post-34107-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Works fine for me using <a href="http://anonsvn.wireshark.org/wireshark-win32-libs/tags/2014-06-05/packages/.">http://anonsvn.wireshark.org/wireshark-win32-libs/tags/2014-06-05/packages/.</a> To obtain further help you'll need to actually post details of the failure, e.g. copy and paste from your command prompt.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jun '14, 23:57</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-34107" class="comments-container"><span id="34113"></span><div id="comment-34113" class="comment"><div id="post-34113-score" class="comment-score"></div><div class="comment-text"><p>SUCCESS!!! Finally found a version that would compile.</p></div><div id="comment-34113-info" class="comment-info"><span class="comment-age">(24 Jun '14, 04:59)</span> <span class="comment-user userinfo">Zh00S</span></div></div><span id="34114"></span><div id="comment-34114" class="comment"><div id="post-34114-score" class="comment-score"></div><div class="comment-text"><p>It also works for me using your original path. I suspect something in your route to the package server has a (broken) cached copy somewhere.</p><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-34114-info" class="comment-info"><span class="comment-age">(24 Jun '14, 05:18)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-34107" class="comment-tools"></div><div class="clear"></div><div id="comment-34107-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

