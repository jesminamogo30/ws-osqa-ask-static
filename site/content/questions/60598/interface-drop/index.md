+++
type = "question"
title = "Interface Drop"
description = '''HI All, My OS is Ubuntu. I&#x27;m sing an Atheros AR9271 USB NIC. I have a clean install of wireshark. When I go to run a capture it runs for apx 10 seconds then I get an error message telling me that the interface that the capture was running on has stopped. The wireshark application then hangs. My netw...'''
date = "2017-04-05T16:06:00Z"
lastmod = "2017-04-06T03:53:00Z"
weight = 60598
keywords = [ "interface", "drop" ]
aliases = [ "/questions/60598" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Interface Drop](/questions/60598/interface-drop)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60598-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60598-score" class="post-score" title="current number of votes">0</div><span id="post-60598-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI All,</p><p>My OS is Ubuntu. I'm sing an Atheros AR9271 USB NIC.</p><p>I have a clean install of wireshark. When I go to run a capture it runs for apx 10 seconds then I get an error message telling me that the interface that the capture was running on has stopped. The wireshark application then hangs. My network wifi connection also drops. I must kill the process to get back up and running.</p><p>The NIC works fine except of course when I run the capture.</p><p>Can anybody help?</p><p>THanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-drop" rel="tag" title="see questions tagged &#39;drop&#39;">drop</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Apr '17, 16:06</strong></p><img src="https://secure.gravatar.com/avatar/ce401dc42fb64cb72fc1939d1f87e94e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TerryH&#39;s gravatar image" /><p><span>TerryH</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TerryH has no accepted answers">0%</span></p></div></div><div id="comments-container-60598" class="comments-container"><span id="60608"></span><div id="comment-60608" class="comment"><div id="post-60608-score" class="comment-score"></div><div class="comment-text"><p>Am I alone on this :/</p><p>Any steps I can try does anybody know?</p></div><div id="comment-60608-info" class="comment-info"><span class="comment-age">(06 Apr '17, 03:36)</span> <span class="comment-user userinfo">TerryH</span></div></div><span id="60609"></span><div id="comment-60609" class="comment"><div id="post-60609-score" class="comment-score"></div><div class="comment-text"><p>Just to expand some more.</p><p>My on board NIC is the same. When I start the capture (monitor mode) after a few seconds I get the same error message. The application hangs. My network connection (to my wifi) stops and starts over and over.</p><p>I have to log off and back on again to get my connection back!!</p><p>What am I doing wrong. This is driving me nuts!!</p></div><div id="comment-60609-info" class="comment-info"><span class="comment-age">(06 Apr '17, 03:53)</span> <span class="comment-user userinfo">TerryH</span></div></div></div><div id="comment-tools-60598" class="comment-tools"></div><div class="clear"></div><div id="comment-60598-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

