+++
type = "question"
title = "programming winPCAP"
description = '''i need to write a program that uses WinPCAP/LibPCAP to parse incoming xml structures being sent. i have no experience doing this. any tips? or places to start?'''
date = "2011-06-15T07:02:00Z"
lastmod = "2011-06-15T14:39:00Z"
weight = 4574
keywords = [ "winpcap" ]
aliases = [ "/questions/4574" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [programming winPCAP](/questions/4574/programming-winpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4574-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4574-score" class="post-score" title="current number of votes">0</div><span id="post-4574-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i need to write a program that uses WinPCAP/LibPCAP to parse incoming xml structures being sent. i have no experience doing this. any tips? or places to start?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jun '11, 07:02</strong></p><img src="https://secure.gravatar.com/avatar/7fcd72cbf41525f2bf00623efa69b5d7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mbands2&#39;s gravatar image" /><p><span>mbands2</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mbands2 has no accepted answers">0%</span></p></div></div><div id="comments-container-4574" class="comments-container"></div><div id="comment-tools-4574" class="comment-tools"></div><div class="clear"></div><div id="comment-4574-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4582"></span>

<div id="answer-container-4582" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4582-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4582-score" class="post-score" title="current number of votes">1</div><span id="post-4582-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Better start of with the <a href="http://www.winpcap.org/devel.htm">Developer Pack</a>, containing documentation.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jun '11, 14:39</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-4582" class="comments-container"></div><div id="comment-tools-4582" class="comment-tools"></div><div class="clear"></div><div id="comment-4582-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

