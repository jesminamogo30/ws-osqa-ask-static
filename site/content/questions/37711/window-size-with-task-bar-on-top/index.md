+++
type = "question"
title = "Window size with task bar on top"
description = '''Using &quot;Wireshark 2 Preview&quot;, I think there is a bug when Wireshark computes the window size. If I have the task bar on top of my display then Wireshark is partly under the task bar. I can attach a screenshot if someone can tell me how.'''
date = "2014-11-09T07:09:00Z"
lastmod = "2014-11-09T07:09:00Z"
weight = 37711
keywords = [ "windowing" ]
aliases = [ "/questions/37711" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Window size with task bar on top](/questions/37711/window-size-with-task-bar-on-top)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37711-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37711-score" class="post-score" title="current number of votes">0</div><span id="post-37711-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Using "Wireshark 2 Preview", I think there is a bug when Wireshark computes the window size. If I have the task bar on top of my display then Wireshark is partly under the task bar. I can attach a screenshot if someone can tell me how.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windowing" rel="tag" title="see questions tagged &#39;windowing&#39;">windowing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Nov '14, 07:09</strong></p><img src="https://secure.gravatar.com/avatar/84de30e481036d5e0b5d5a606f468bce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eddyq&#39;s gravatar image" /><p><span>eddyq</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eddyq has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Nov '14, 07:10</strong> </span></p></div></div><div id="comments-container-37711" class="comments-container"></div><div id="comment-tools-37711" class="comment-tools"></div><div class="clear"></div><div id="comment-37711-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

