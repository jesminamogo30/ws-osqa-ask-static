+++
type = "question"
title = "Differences in Development branch 1.99.3 OSX and stable 1.12.4"
description = '''Apologies if this is an obvious question. How can I find out what the release issues are with OSX 1.99.3? I am comparing it to 1.12.4 on Windows and I notice things like Expert Info are missing and that there doesn&#x27;t seem to be a way to have the Info column include the ICMP Sequence number (as it do...'''
date = "2015-03-11T13:00:00Z"
lastmod = "2015-03-11T14:31:00Z"
weight = 40482
keywords = [ "development", "osx" ]
aliases = [ "/questions/40482" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Differences in Development branch 1.99.3 OSX and stable 1.12.4](/questions/40482/differences-in-development-branch-1993-osx-and-stable-1124)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40482-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40482-score" class="post-score" title="current number of votes">0</div><span id="post-40482-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Apologies if this is an obvious question.</p><p>How can I find out what the release issues are with OSX 1.99.3?</p><p>I am comparing it to 1.12.4 on Windows and I notice things like Expert Info are missing and that there doesn't seem to be a way to have the Info column include the ICMP Sequence number (as it does in the stable Windows release).</p><p>Regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-development" rel="tag" title="see questions tagged &#39;development&#39;">development</span> <span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Mar '15, 13:00</strong></p><img src="https://secure.gravatar.com/avatar/648dfbed062f4e2c392e309ed01ef2f4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="razamattaz&#39;s gravatar image" /><p><span>razamattaz</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="razamattaz has no accepted answers">0%</span></p></div></div><div id="comments-container-40482" class="comments-container"></div><div id="comment-tools-40482" class="comment-tools"></div><div class="clear"></div><div id="comment-40482-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40484"></span>

<div id="answer-container-40484" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40484-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40484-score" class="post-score" title="current number of votes">2</div><span id="post-40484-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The announce mail list has release info. For 1.99.3 see <a href="https://www.wireshark.org/lists/wireshark-announce/201503/msg00002.html">here</a>.</p><p>Note that 1.99.3 is a work in progress and so there may be missing or broken functionality. These aren't generally enumerated anywhere apart from the QT port dev <a href="https://wiki.wireshark.org/Development/QtShark">page</a> on the wiki.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Mar '15, 13:10</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-40484" class="comments-container"><span id="40490"></span><div id="comment-40490" class="comment"><div id="post-40490-score" class="comment-score"></div><div class="comment-text"><p>Thank you for taking the time to answer. I appreciate it.</p></div><div id="comment-40490-info" class="comment-info"><span class="comment-age">(11 Mar '15, 14:31)</span> <span class="comment-user userinfo">razamattaz</span></div></div></div><div id="comment-tools-40484" class="comment-tools"></div><div class="clear"></div><div id="comment-40484-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

