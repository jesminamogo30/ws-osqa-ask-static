+++
type = "question"
title = "wireshark capturing vlan tags on Microsoft 2008 Server Hyper-V"
description = '''Hello, is there any issue with wireshark working with Microsoft 2008 Server Hyper-V to capture vlan tagged frames on a VM? NIC is setup to not script vlan tags.'''
date = "2011-11-02T10:39:00Z"
lastmod = "2011-11-03T18:31:00Z"
weight = 7202
keywords = [ "vlan", "tags" ]
aliases = [ "/questions/7202" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark capturing vlan tags on Microsoft 2008 Server Hyper-V](/questions/7202/wireshark-capturing-vlan-tags-on-microsoft-2008-server-hyper-v)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7202-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7202-score" class="post-score" title="current number of votes">0</div><span id="post-7202-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, is there any issue with wireshark working with Microsoft 2008 Server Hyper-V to capture vlan tagged frames on a VM? NIC is setup to not script vlan tags.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vlan" rel="tag" title="see questions tagged &#39;vlan&#39;">vlan</span> <span class="post-tag tag-link-tags" rel="tag" title="see questions tagged &#39;tags&#39;">tags</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Nov '11, 10:39</strong></p><img src="https://secure.gravatar.com/avatar/4b373234007e7fe3c7144cfd8044ca2d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffStok1967&#39;s gravatar image" /><p><span>JeffStok1967</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffStok1967 has no accepted answers">0%</span></p></div></div><div id="comments-container-7202" class="comments-container"><span id="7210"></span><div id="comment-7210" class="comment"><div id="post-7210-score" class="comment-score"></div><div class="comment-text"><p>Which version of Wireshark are you using?</p></div><div id="comment-7210-info" class="comment-info"><span class="comment-age">(02 Nov '11, 17:38)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="7231"></span><div id="comment-7231" class="comment"><div id="post-7231-score" class="comment-score"></div><div class="comment-text"><p>Does "NIC" refer to the physical NIC on the machine or to a virtual NIC? And what do you mean by "not script vlan tags"? Do you mean "not <em>strip</em> VLAN tags"?</p></div><div id="comment-7231-info" class="comment-info"><span class="comment-age">(03 Nov '11, 18:31)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-7202" class="comment-tools"></div><div class="clear"></div><div id="comment-7202-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

