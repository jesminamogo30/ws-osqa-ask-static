+++
type = "question"
title = "How to filter based on byte sequence."
description = '''I want to filter the frames which start with the sequence &quot;55 55 55 55 55 55 55 D5 00 CE 00 01 00 08 00 CE 00 00 00 01 88 B5&quot;. How to achieve this?'''
date = "2014-09-10T00:21:00Z"
lastmod = "2014-09-10T04:07:00Z"
weight = 36138
keywords = [ "filter", "byte-filter", "display-filter" ]
aliases = [ "/questions/36138" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to filter based on byte sequence.](/questions/36138/how-to-filter-based-on-byte-sequence)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36138-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36138-score" class="post-score" title="current number of votes">0</div><span id="post-36138-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to filter the frames which start with the sequence "55 55 55 55 55 55 55 D5 00 CE 00 01 00 08 00 CE 00 00 00 01 88 B5". How to achieve this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-byte-filter" rel="tag" title="see questions tagged &#39;byte-filter&#39;">byte-filter</span> <span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Sep '14, 00:21</strong></p><img src="https://secure.gravatar.com/avatar/13c01090e672eed966eb0deac4a1abf6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Chetan%20Ragi&#39;s gravatar image" /><p><span>Chetan Ragi</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Chetan Ragi has no accepted answers">0%</span></p></div></div><div id="comments-container-36138" class="comments-container"></div><div id="comment-tools-36138" class="comment-tools"></div><div class="clear"></div><div id="comment-36138-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36147"></span>

<div id="answer-container-36147" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36147-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36147-score" class="post-score" title="current number of votes">1</div><span id="post-36147-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><strong>frame[0:22] == 55:55:55:55:55:55:55:D5:00:CE:00:01:00:08:00:CE:00:00:00:01:88:B5</strong></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Sep '14, 04:07</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-36147" class="comments-container"></div><div id="comment-tools-36147" class="comment-tools"></div><div class="clear"></div><div id="comment-36147-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

