+++
type = "question"
title = "Limit Interface"
description = '''Is it possible to limit which interface can me monitored on a Windows system? Seems easy to do in Linux but we want only one Ethernet adapter to run wireshark/winpcap and the other nics in the system are not allowed.'''
date = "2012-09-05T16:26:00Z"
lastmod = "2012-09-05T16:53:00Z"
weight = 14067
keywords = [ "windows", "administrator", "limit" ]
aliases = [ "/questions/14067" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Limit Interface](/questions/14067/limit-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14067-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14067-score" class="post-score" title="current number of votes">0</div><span id="post-14067-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to limit which interface can me monitored on a Windows system? Seems easy to do in Linux but we want only one Ethernet adapter to run wireshark/winpcap and the other nics in the system are not allowed.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-administrator" rel="tag" title="see questions tagged &#39;administrator&#39;">administrator</span> <span class="post-tag tag-link-limit" rel="tag" title="see questions tagged &#39;limit&#39;">limit</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Sep '12, 16:26</strong></p><img src="https://secure.gravatar.com/avatar/676808ebde646d29ebb9c4710b952591?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="raypitt&#39;s gravatar image" /><p><span>raypitt</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="raypitt has no accepted answers">0%</span></p></div></div><div id="comments-container-14067" class="comments-container"></div><div id="comment-tools-14067" class="comment-tools"></div><div class="clear"></div><div id="comment-14067-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14072"></span>

<div id="answer-container-14072" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14072-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14072-score" class="post-score" title="current number of votes">0</div><span id="post-14072-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's only possible if you build your own version of Wireshark and filter the list of interfaces that is delivered by WinPcap.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Sep '12, 16:53</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-14072" class="comments-container"></div><div id="comment-tools-14072" class="comment-tools"></div><div class="clear"></div><div id="comment-14072-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

