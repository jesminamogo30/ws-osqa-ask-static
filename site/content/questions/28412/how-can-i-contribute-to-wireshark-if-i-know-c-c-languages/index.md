+++
type = "question"
title = "How can i contribute to Wireshark if i know C &amp; C++ languages"
description = '''Hello, Myself a Computer Science Engineering student willing to work with Wireshark, just to enhance my knowledge and know more about industrial coding levels.I had done coding in C and C++ language and also have knowledge about protocols.I&#x27;m willing to work with you,hope you will consider my reques...'''
date = "2013-12-26T11:13:00Z"
lastmod = "2013-12-27T08:34:00Z"
weight = 28412
keywords = [ "c++" ]
aliases = [ "/questions/28412" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can i contribute to Wireshark if i know C & C++ languages](/questions/28412/how-can-i-contribute-to-wireshark-if-i-know-c-c-languages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28412-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28412-score" class="post-score" title="current number of votes">0</div><span id="post-28412-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, Myself a Computer Science Engineering student willing to work with Wireshark, just to enhance my knowledge and know more about industrial coding levels.I had done coding in C and C++ language and also have knowledge about protocols.I'm willing to work with you,hope you will consider my request.</p><p>Shubhra Malhotra <span class="__cf_email__" data-cfemail="2d5e45584f455f4c03404c414542595f4c1f1f6d4a404c4441034e4240">[email protected]</span></p><p>India</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-c++" rel="tag" title="see questions tagged &#39;c++&#39;">c++</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Dec '13, 11:13</strong></p><img src="https://secure.gravatar.com/avatar/3641b82be0834efb20eaf849a25f3c14?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Wireshark%20Contributor&#39;s gravatar image" /><p><span>Wireshark Co...</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Wireshark Contributor has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Dec '13, 11:20</strong> </span></p></div></div><div id="comments-container-28412" class="comments-container"></div><div id="comment-tools-28412" class="comment-tools"></div><div class="clear"></div><div id="comment-28412-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28427"></span>

<div id="answer-container-28427" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28427-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28427-score" class="post-score" title="current number of votes">1</div><span id="post-28427-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I'm willing to work with you,hope you will consider my request.</p></blockquote><p>there is nothing to 'consider', as there is nobody who will 'assign' a piece of work to you ;-)</p><p>So, if you want to contribute, you can</p><ul><li>participate in this Q&amp;A site to help answer questions. That will also help you to learn more about Wireshark</li><li>If you have a good idea how to improve Wireshark, go ahead and implement it, then submit your work (see the <a href="http://www.wireshark.org/docs/wsdg_html_chunked/">Delevoper guide</a> and <a href="http://wiki.wireshark.org/Development">other resources</a> )</li><li>Take on some of the open issues in the bug tracker and fix the problems or implement enhancement requests: <code>https://bugs.wireshark.org/bugzilla/buglist.cgi?advanced&amp;resolution=---&amp;limit=0&amp;order=bug_id%20DESC</code></li></ul><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Dec '13, 03:46</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Dec '13, 04:10</strong> </span></p></div></div><div id="comments-container-28427" class="comments-container"><span id="28445"></span><div id="comment-28445" class="comment"><div id="post-28445-score" class="comment-score"></div><div class="comment-text"><p>Ther is also some stuff here <a href="http://wiki.wireshark.org/GSoC2013?highlight=%28summer%29">http://wiki.wireshark.org/GSoC2013?highlight=%28summer%29</a></p><p>Any help with the port to Qt would be apreciated. NB as we want to improve things in the process discussing ideas on the developers mailing list Before starting might be a good idea.</p></div><div id="comment-28445-info" class="comment-info"><span class="comment-age">(27 Dec '13, 08:27)</span> <span class="comment-user userinfo">Anders ♦</span></div></div><span id="28447"></span><div id="comment-28447" class="comment"><div id="post-28447-score" class="comment-score"></div><div class="comment-text"><blockquote><p>Any help with the port to Qt would be apreciated.</p></blockquote><p>BTW: is there any kind of a 'coordinated' approach for the Qt migration with 'split duties' for certain parts?</p></div><div id="comment-28447-info" class="comment-info"><span class="comment-age">(27 Dec '13, 08:34)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28427" class="comment-tools"></div><div class="clear"></div><div id="comment-28427-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

