+++
type = "question"
title = "how to find the problem of this capture?"
description = '''Hi  I&#x27;m new at wireshark and I have this problem on my network. I have a server that is lossing the connection to the Omnicenter server. Everyday around at 4:07 am the user are complaining that they can&#x27;t access the application. and Omnicenter states that can&#x27;t ping the server. but in reality the se...'''
date = "2016-11-07T15:14:00Z"
lastmod = "2016-11-07T23:35:00Z"
weight = 57107
keywords = [ "wireshark" ]
aliases = [ "/questions/57107" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to find the problem of this capture?](/questions/57107/how-to-find-the-problem-of-this-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57107-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57107-score" class="post-score" title="current number of votes">0</div><span id="post-57107-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I'm new at wireshark and I have this problem on my network.</p><p>I have a server that is lossing the connection to the Omnicenter server. Everyday around at 4:07 am the user are complaining that they can't access the application. and Omnicenter states that can't ping the server.</p><p>but in reality the server is up and running. I'm able to ping it, get in , work in there without losing any connection.<br />
</p><p>users are complaining that they lost connectivity to the application for about 5 min.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/probem_HZeLmjF.png" alt="alt text" /></p><p>I took wireshark capture but I don't see any problem.</p><p>can you help me out, please?</p><p>how to upload the capture?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Nov '16, 15:14</strong></p><img src="https://secure.gravatar.com/avatar/4ae51bef1420f75175541a9330e76417?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helderguzman&#39;s gravatar image" /><p><span>helderguzman</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="helderguzman has no accepted answers">0%</span> </br></p></img></div></div><div id="comments-container-57107" class="comments-container"><span id="57108"></span><div id="comment-57108" class="comment"><div id="post-57108-score" class="comment-score"></div><div class="comment-text"><p><img src="https://osqa-ask.wireshark.org/upfiles/capture_hJsEzKb.png" alt="alt text" /></p><p>here is the capture</p></div><div id="comment-57108-info" class="comment-info"><span class="comment-age">(07 Nov '16, 15:23)</span> <span class="comment-user userinfo">helderguzman</span></div></div><span id="57119"></span><div id="comment-57119" class="comment"><div id="post-57119-score" class="comment-score"></div><div class="comment-text"><p>Well the whole trace is full of problems. And without a capture file it next to impossible to help you and it makes no fun at all. You can use tracewrangler if you want some anomyzation tasks to the tracefile.</p></div><div id="comment-57119-info" class="comment-info"><span class="comment-age">(07 Nov '16, 23:35)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-57107" class="comment-tools"></div><div class="clear"></div><div id="comment-57107-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

