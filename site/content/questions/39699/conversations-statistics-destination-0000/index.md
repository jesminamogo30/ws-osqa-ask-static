+++
type = "question"
title = "conversations statistics - destination 0.0.0.0"
description = '''I am looking at the conversations statistics and I have many conversations from 192.168.1.120 to 0.0.0.0 on port 2001, What kind of traffic is that? I don&#x27;t think it is multicast. Thx'''
date = "2015-02-07T12:56:00Z"
lastmod = "2015-02-09T05:36:00Z"
weight = 39699
keywords = [ "conversations", "stsatistics" ]
aliases = [ "/questions/39699" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [conversations statistics - destination 0.0.0.0](/questions/39699/conversations-statistics-destination-0000)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39699-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39699-score" class="post-score" title="current number of votes">0</div><span id="post-39699-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking at the conversations statistics and I have many conversations from 192.168.1.120 to 0.0.0.0 on port 2001, What kind of traffic is that? I don't think it is multicast. Thx</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-conversations" rel="tag" title="see questions tagged &#39;conversations&#39;">conversations</span> <span class="post-tag tag-link-stsatistics" rel="tag" title="see questions tagged &#39;stsatistics&#39;">stsatistics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Feb '15, 12:56</strong></p><img src="https://secure.gravatar.com/avatar/b68ecd02e11309f135e5caf5e144f2a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jaja&#39;s gravatar image" /><p><span>jaja</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jaja has no accepted answers">0%</span></p></div></div><div id="comments-container-39699" class="comments-container"></div><div id="comment-tools-39699" class="comment-tools"></div><div class="clear"></div><div id="comment-39699-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39710"></span>

<div id="answer-container-39710" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39710-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39710-score" class="post-score" title="current number of votes">0</div><span id="post-39710-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's an (old school) broadcast address (or uninitialized destination host address) and this port is related to various malicious software. Without even stating if this is UDP, TCP or other transport protocol, there's little more to say.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Feb '15, 05:36</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-39710" class="comments-container"></div><div id="comment-tools-39710" class="comment-tools"></div><div class="clear"></div><div id="comment-39710-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

