+++
type = "question"
title = "USBPcapCMD.exe precomiled binaries?"
description = '''are there USBPcapCMD.exe pre-compiled binaries some place? I have windows 8.1, and I want to be able to capture USB Packets Via PowerShell. The Compiling instructions seem complicated.'''
date = "2016-07-07T15:31:00Z"
lastmod = "2016-07-08T03:26:00Z"
weight = 53915
keywords = [ "binary", "usb", "usbpcap" ]
aliases = [ "/questions/53915" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [USBPcapCMD.exe precomiled binaries?](/questions/53915/usbpcapcmdexe-precomiled-binaries)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53915-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53915-score" class="post-score" title="current number of votes">0</div><span id="post-53915-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>are there USBPcapCMD.exe pre-compiled binaries some place? I have windows 8.1, and I want to be able to capture USB Packets Via PowerShell. The Compiling instructions seem complicated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-binary" rel="tag" title="see questions tagged &#39;binary&#39;">binary</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span> <span class="post-tag tag-link-usbpcap" rel="tag" title="see questions tagged &#39;usbpcap&#39;">usbpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jul '16, 15:31</strong></p><img src="https://secure.gravatar.com/avatar/b997637a56fa3812bb5146b3786ee488?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Eric%20Lovejoy&#39;s gravatar image" /><p><span>Eric Lovejoy</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Eric Lovejoy has no accepted answers">0%</span></p></div></div><div id="comments-container-53915" class="comments-container"></div><div id="comment-tools-53915" class="comment-tools"></div><div class="clear"></div><div id="comment-53915-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53926"></span>

<div id="answer-container-53926" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53926-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53926-score" class="post-score" title="current number of votes">0</div><span id="post-53926-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not really a Wireshark question as it's a 3rd party project, but here's a hint: <a href="http://desowin.org/usbpcap/">USBPcap Website</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jul '16, 03:26</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-53926" class="comments-container"></div><div id="comment-tools-53926" class="comment-tools"></div><div class="clear"></div><div id="comment-53926-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

