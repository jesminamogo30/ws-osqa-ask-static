+++
type = "question"
title = "Got TCP Retransmission and TCP Dup ACK from my client to server"
description = '''Hi  I have try to access my game server from one the client, how ever portal is open but i could not join the game server. i have capture packets! from client, its showing game server TCP Retransmission &amp;amp; TCP Dup ACK`. For more information please find the attached file, why it was happened? how ...'''
date = "2016-08-22T05:26:00Z"
lastmod = "2016-08-22T05:26:00Z"
weight = 55045
keywords = [ "wireshark" ]
aliases = [ "/questions/55045" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Got TCP Retransmission and TCP Dup ACK from my client to server](/questions/55045/got-tcp-retransmission-and-tcp-dup-ack-from-my-client-to-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55045-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55045-score" class="post-score" title="current number of votes">-1</div><span id="post-55045-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I have try to access my game server from one the client, how ever portal is open but i could not join the game server. i have capture packets! from client, its showing game server TCP Retransmission &amp; TCP Dup ACK`. For more information please find the attached file, why it was happened? how can i solve this issue?</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Screenshot_2_8WStmZ7.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '16, 05:26</strong></p><img src="https://secure.gravatar.com/avatar/80d4f576bd22e3ff7eb52ffc8d694b13?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vinish1&#39;s gravatar image" /><p><span>vinish1</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vinish1 has no accepted answers">0%</span></p></img></div></div><div id="comments-container-55045" class="comments-container"></div><div id="comment-tools-55045" class="comment-tools"></div><div class="clear"></div><div id="comment-55045-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

