+++
type = "question"
title = "latency issues connection speed lost  im getting my internet hacked manipulated by guys online which started from psn"
description = '''hey i have the same issue kinda as the guy with a similar question a few questions below but and worse for me it stems from a video game on ps4 and theses trying to be so slick and constantly guys are trying to hack manipulate and some how mess with my connection they constantly get on my laptops ps...'''
date = "2014-11-08T11:10:00Z"
lastmod = "2014-11-10T22:10:00Z"
weight = 37700
keywords = [ "wireshark" ]
aliases = [ "/questions/37700" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [latency issues connection speed lost im getting my internet hacked manipulated by guys online which started from psn](/questions/37700/latency-issues-connection-speed-lost-im-getting-my-internet-hacked-manipulated-by-guys-online-which-started-from-psn)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37700-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37700-score" class="post-score" title="current number of votes">0</div><span id="post-37700-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hey i have the same issue kinda as the guy with a similar question a few questions below but and worse for me it stems from a video game on ps4 and theses trying to be so slick and constantly guys are trying to hack manipulate and some how mess with my connection they constantly get on my laptops ps4 tablets and phones on ps4 they manipulate it they stop me lag me it at times seems like they are controlling me in games ive had 2000 plus on latency i can feel it in the controller control sticks start getting stiff they find out my ip through like cain and abel or something then a few of them are doing something to my connection my laptop also gets all kinds of errors when im online on sites like this its like they are trying stop what im doing my typing goes crazy like when im playing i run wire shark and it will say for some reason it shut down my laptop and ps4 all of a sudden sounds like their are working so hard ive been trying get rid of these guys for several months its been so aggravating ive been looking for hackers to get them back but i really just want my connection fixed it always drops drastically im supposed to get 100 upload and 20 down i only get 10 and under all kinds of errors ive had 7 technicians from my provider switched modems countless times and switched providers and still i could use anyone's help thank you its something they are doing sending me something over the net or i dont know im not at all very computer or networking smart</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '14, 11:10</strong></p><img src="https://secure.gravatar.com/avatar/08a002781d656e49c05a3be0352280e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MostUnlikedOnPS4&#39;s gravatar image" /><p><span>MostUnlikedO...</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MostUnlikedOnPS4 has no accepted answers">0%</span></p></div></div><div id="comments-container-37700" class="comments-container"><span id="37719"></span><div id="comment-37719" class="comment"><div id="post-37719-score" class="comment-score"></div><div class="comment-text"><p>Its not easy to identify what you are trying to ask here...</p><p>You have latency problems in a video game you play on your PS4 ? (2000 ms +)</p><p>You ran a wireshark trace on your laptop to analyze those and this shut down your laptop?</p><p>You assume you've been hacked? 'They'are on your laptops, phone, tablet and PS4?</p><p>Your internet connection drops constantly?</p><p>What kind of help do you expect form this wireshark Q&amp;A?</p></div><div id="comment-37719-info" class="comment-info"><span class="comment-age">(10 Nov '14, 00:28)</span> <span class="comment-user userinfo">mrEEde</span></div></div><span id="37743"></span><div id="comment-37743" class="comment"><div id="post-37743-score" class="comment-score"></div><div class="comment-text"><p>I honestly think this might be a troll post. No one (I don't think) could practically think that a hacker can make their controller physically sticky.</p></div><div id="comment-37743-info" class="comment-info"><span class="comment-age">(10 Nov '14, 22:10)</span> <span class="comment-user userinfo">Quadratic</span></div></div></div><div id="comment-tools-37700" class="comment-tools"></div><div class="clear"></div><div id="comment-37700-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

