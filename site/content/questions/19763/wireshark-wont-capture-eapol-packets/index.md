+++
type = "question"
title = "Wireshark wont capture EAPOL packets"
description = '''Hello,  I&#x27;m using Wireshark (1.8.6) on Gentoo Linux and I&#x27;m using a USB Wifi adapter from TP Link (tl-wn725n). The thing is that even though I can get some packets with Wireshark I can&#x27;t get the authentication ones from my own computer (the wireless card that is integrated). I&#x27;m setting all the nece...'''
date = "2013-03-22T15:01:00Z"
lastmod = "2013-03-22T15:01:00Z"
weight = 19763
keywords = [ "sniffing", "eapol", "rtl8192cu", "wpa" ]
aliases = [ "/questions/19763" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark wont capture EAPOL packets](/questions/19763/wireshark-wont-capture-eapol-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19763-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19763-score" class="post-score" title="current number of votes">1</div><span id="post-19763-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I'm using Wireshark (1.8.6) on Gentoo Linux and I'm using a USB Wifi adapter from TP Link (tl-wn725n).</p><p>The thing is that even though I can get some packets with Wireshark I can't get the authentication ones from my own computer (the wireless card that is integrated).</p><p>I'm setting all the necesary preferences on Wireshark, I have the decription key like this: passwd:My-SSID (yes, my SSID has a - on the name)</p><p>I also try to force a monitor mode by using airmon-ng to set it up for me and it has no issues with that either:</p><p>wlan1 Unknown rtl8192cu - [phy2] (monitor mode enabled on mon0)</p><p>What I need to know is that if I'm doing anything wrong with Wireshark or is the USB adapter that is not doing a good job on the sniffing.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-eapol" rel="tag" title="see questions tagged &#39;eapol&#39;">eapol</span> <span class="post-tag tag-link-rtl8192cu" rel="tag" title="see questions tagged &#39;rtl8192cu&#39;">rtl8192cu</span> <span class="post-tag tag-link-wpa" rel="tag" title="see questions tagged &#39;wpa&#39;">wpa</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Mar '13, 15:01</strong></p><img src="https://secure.gravatar.com/avatar/8245387f5da807b0884169a90a859ca7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Joy%20Dragon&#39;s gravatar image" /><p><span>Joy Dragon</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Joy Dragon has no accepted answers">0%</span></p></div></div><div id="comments-container-19763" class="comments-container"></div><div id="comment-tools-19763" class="comment-tools"></div><div class="clear"></div><div id="comment-19763-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

