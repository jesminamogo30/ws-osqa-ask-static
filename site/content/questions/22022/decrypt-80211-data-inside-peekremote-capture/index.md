+++
type = "question"
title = "Decrypt 802.11 data inside peekremote capture"
description = '''I have a peekremote style capture functioning (data from 3 Access points in sniffer mode being sent to my wireshark install on udp5555). Decode As-&amp;gt;Peekremote works to open up the udp 5555 encapsulation and get me the 802.11 headers. I can see EPOL Key exhcanges, but cannot decrypt the 802.11 dat...'''
date = "2013-06-13T11:18:00Z"
lastmod = "2013-06-13T11:18:00Z"
weight = 22022
keywords = [ "wireless", "wpa2", "wpa-pwd", "802.11", "peekremote" ]
aliases = [ "/questions/22022" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt 802.11 data inside peekremote capture](/questions/22022/decrypt-80211-data-inside-peekremote-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-22022-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-22022-score" class="post-score" title="current number of votes">0</div><span id="post-22022-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a peekremote style capture functioning (data from 3 Access points in sniffer mode being sent to my wireshark install on udp5555). Decode As-&gt;Peekremote works to open up the udp 5555 encapsulation and get me the 802.11 headers. I can see EPOL Key exhcanges, but cannot decrypt the 802.11 data after entering the proper wpa-pwd:Passphrase:SSID cobination. (that combination works if I do a raw sniff but I'm using the APs to gather remote data on all 3 channels).</p><p>Am I missing a step or is there some complexity in these multiple layers of encapsulation that causing me to fail to decrypt the 802.11 data?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span> <span class="post-tag tag-link-wpa-pwd" rel="tag" title="see questions tagged &#39;wpa-pwd&#39;">wpa-pwd</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-peekremote" rel="tag" title="see questions tagged &#39;peekremote&#39;">peekremote</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jun '13, 11:18</strong></p><img src="https://secure.gravatar.com/avatar/165cbbb0091043fb6161a83d8d97f187?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="whistj&#39;s gravatar image" /><p><span>whistj</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="whistj has no accepted answers">0%</span></p></div></div><div id="comments-container-22022" class="comments-container"></div><div id="comment-tools-22022" class="comment-tools"></div><div class="clear"></div><div id="comment-22022-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

