+++
type = "question"
title = "how to apply a capture filter?"
description = '''Hi, in the latest version : 1.8.6 I can create a capture filter (Capture --&amp;gt; Capture filter --&amp;gt; new) : port 516 but how can I apply this capture filter. when I start capture, there are not any options to select the filter. and it will capture all the packets using the old version, it can selec...'''
date = "2013-03-14T23:11:00Z"
lastmod = "2013-03-20T05:39:00Z"
weight = 19526
keywords = [ "apply", "capture-filter" ]
aliases = [ "/questions/19526" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to apply a capture filter?](/questions/19526/how-to-apply-a-capture-filter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19526-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19526-score" class="post-score" title="current number of votes">1</div><span id="post-19526-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, in the latest version : 1.8.6 I can create a capture filter (Capture --&gt; Capture filter --&gt; new) : port 516 but how can I apply this capture filter. when I start capture, there are not any options to select the filter. and it will capture all the packets</p><p>using the old version, it can select capture filter.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-apply" rel="tag" title="see questions tagged &#39;apply&#39;">apply</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Mar '13, 23:11</strong></p><img src="https://secure.gravatar.com/avatar/9a1344c64cbb56a3ab0b6767708c508c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="timeriver_wang&#39;s gravatar image" /><p><span>timeriver_wang</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="timeriver_wang has no accepted answers">0%</span></p></div></div><div id="comments-container-19526" class="comments-container"></div><div id="comment-tools-19526" class="comment-tools"></div><div class="clear"></div><div id="comment-19526-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19529"></span>

<div id="answer-container-19529" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19529-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19529-score" class="post-score" title="current number of votes">2</div><span id="post-19529-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://ask.wireshark.org/questions/12452/where-are-the-capture-filter-options-in-wireshark-180">http://ask.wireshark.org/questions/12452/where-are-the-capture-filter-options-in-wireshark-180</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Mar '13, 01:23</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-19529" class="comments-container"><span id="19667"></span><div id="comment-19667" class="comment"><div id="post-19667-score" class="comment-score"></div><div class="comment-text"><p>I find it, thank you very much!</p></div><div id="comment-19667-info" class="comment-info"><span class="comment-age">(19 Mar '13, 23:21)</span> <span class="comment-user userinfo">timeriver_wang</span></div></div><span id="19673"></span><div id="comment-19673" class="comment"><div id="post-19673-score" class="comment-score"></div><div class="comment-text"><p>Please accept SYN-bits answer if it helped ;-)</p></div><div id="comment-19673-info" class="comment-info"><span class="comment-age">(20 Mar '13, 05:39)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-19529" class="comment-tools"></div><div class="clear"></div><div id="comment-19529-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

