+++
type = "question"
title = "Inject a packet on a http stream"
description = '''I used scapy to inject a packet into http stream.The connection was between my browser and apache server on the same machine .After I injected the packet, both the browser and the sever kept sending the same packets continuously untill my computer crashed and I had to restart it .Here is the capture...'''
date = "2014-03-10T09:31:00Z"
lastmod = "2014-03-10T12:34:00Z"
weight = 30656
keywords = [ "http", "inject" ]
aliases = [ "/questions/30656" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Inject a packet on a http stream](/questions/30656/inject-a-packet-on-a-http-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30656-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30656-score" class="post-score" title="current number of votes">0</div><span id="post-30656-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I used scapy to inject a packet into http stream.The connection was between my browser and apache server on the same machine .After I injected the packet, both the browser and the sever kept sending the same packets continuously untill my computer crashed and I had to restart it .<a href="http://www.4shared.com/file/8KPKlIEEce/HTTP.html">Here is the capture file capture file.</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-inject" rel="tag" title="see questions tagged &#39;inject&#39;">inject</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Mar '14, 09:31</strong></p><img src="https://secure.gravatar.com/avatar/509262c01720a8512e1fb644856ddeaf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="judy&#39;s gravatar image" /><p><span>judy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="judy has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Mar '14, 11:54</strong> </span></p></div></div><div id="comments-container-30656" class="comments-container"><span id="30657"></span><div id="comment-30657" class="comment"><div id="post-30657-score" class="comment-score"></div><div class="comment-text"><p>That is no capture file, and what is your question?</p></div><div id="comment-30657-info" class="comment-info"><span class="comment-age">(10 Mar '14, 09:44)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="30661"></span><div id="comment-30661" class="comment"><div id="post-30661-score" class="comment-score"></div><div class="comment-text"><p>I want to know why the server and the browser behave in this weird way</p></div><div id="comment-30661-info" class="comment-info"><span class="comment-age">(10 Mar '14, 12:25)</span> <span class="comment-user userinfo">judy</span></div></div><span id="30662"></span><div id="comment-30662" class="comment"><div id="post-30662-score" class="comment-score"></div><div class="comment-text"><p>maybe because you <strong>injected</strong> something into the connection that made them go nuts!?!</p><p>Without a capture file that is readable and the scapy script, it's impossible to help you.</p></div><div id="comment-30662-info" class="comment-info"><span class="comment-age">(10 Mar '14, 12:34)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-30656" class="comment-tools"></div><div class="clear"></div><div id="comment-30656-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

