+++
type = "question"
title = "SMB &amp; 0xC0000035 STATUS_OBJECT_NAME_COLLISION"
description = '''Hi, I did spot this error thanks to wireshark but i dont have a clue of what the hell i can do to make this error to disapear. It was a copy of a file from a src to a dst and when trying to copy it, got this error telling me that the file name already exist. So what ? This is R/W. Can&#x27;t he write ove...'''
date = "2012-11-16T02:02:00Z"
lastmod = "2012-11-16T04:39:00Z"
weight = 15954
keywords = [ "0xc0000035" ]
aliases = [ "/questions/15954" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SMB & 0xC0000035 STATUS\_OBJECT\_NAME\_COLLISION](/questions/15954/smb-0xc0000035-status_object_name_collision)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15954-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15954-score" class="post-score" title="current number of votes">0</div><span id="post-15954-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I did spot this error thanks to wireshark but i dont have a clue of what the hell i can do to make this error to disapear.</p><p>It was a copy of a file from a src to a dst and when trying to copy it, got this error telling me that the file name already exist.</p><p>So what ? This is R/W. Can't he write over it.</p><p>If you have any idea that would be great !</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-0xc0000035" rel="tag" title="see questions tagged &#39;0xc0000035&#39;">0xc0000035</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Nov '12, 02:02</strong></p><img src="https://secure.gravatar.com/avatar/a955745f1cfe8787e80366c5a5542e15?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gateau&#39;s gravatar image" /><p><span>gateau</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gateau has no accepted answers">0%</span></p></div></div><div id="comments-container-15954" class="comments-container"><span id="15959"></span><div id="comment-15959" class="comment"><div id="post-15959-score" class="comment-score"></div><div class="comment-text"><p>This is more a Windows networking / filesystem question than a Wireshark question. This may not be the right QA site for this.</p></div><div id="comment-15959-info" class="comment-info"><span class="comment-age">(16 Nov '12, 04:39)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-15954" class="comment-tools"></div><div class="clear"></div><div id="comment-15954-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

