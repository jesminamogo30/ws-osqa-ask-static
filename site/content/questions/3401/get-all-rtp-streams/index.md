+++
type = "question"
title = "get all rtp streams"
description = '''Hi guys, i have a capture with ~100 streams, i&#x27;d like to automaticly export them all. -save payload in bothways, in au-format Is this possible? Br Sebastian'''
date = "2011-04-08T02:34:00Z"
lastmod = "2011-04-08T03:44:00Z"
weight = 3401
keywords = [ "auto", "rtp", "stream", "payload" ]
aliases = [ "/questions/3401" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [get all rtp streams](/questions/3401/get-all-rtp-streams)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3401-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3401-score" class="post-score" title="current number of votes">0</div><span id="post-3401-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi guys,</p><p>i have a capture with ~100 streams, i'd like to automaticly export them all. -save payload in bothways, in au-format</p><p>Is this possible?</p><p>Br Sebastian</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-auto" rel="tag" title="see questions tagged &#39;auto&#39;">auto</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span> <span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Apr '11, 02:34</strong></p><img src="https://secure.gravatar.com/avatar/7924f2c86cb4144f9a5dc24dc538be93?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="elefantungen&#39;s gravatar image" /><p><span>elefantungen</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="elefantungen has no accepted answers">0%</span></p></div></div><div id="comments-container-3401" class="comments-container"></div><div id="comment-tools-3401" class="comment-tools"></div><div class="clear"></div><div id="comment-3401-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3403"></span>

<div id="answer-container-3403" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3403-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3403-score" class="post-score" title="current number of votes">0</div><span id="post-3403-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See this <a href="http://ask.wireshark.org/questions/3166/voip-call-recording">question</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Apr '11, 03:44</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-3403" class="comments-container"></div><div id="comment-tools-3403" class="comment-tools"></div><div class="clear"></div><div id="comment-3403-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

