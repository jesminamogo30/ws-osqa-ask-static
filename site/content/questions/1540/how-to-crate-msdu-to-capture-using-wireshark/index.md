+++
type = "question"
title = "how to crate msdu to capture using wireshark?"
description = '''is there anyone show me how to crate msdu to capture using wireshark? help pls, thanks much'''
date = "2010-12-30T09:22:00Z"
lastmod = "2010-12-31T15:36:00Z"
weight = 1540
keywords = [ "capture" ]
aliases = [ "/questions/1540" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to crate msdu to capture using wireshark?](/questions/1540/how-to-crate-msdu-to-capture-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1540-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1540-score" class="post-score" title="current number of votes">0</div><span id="post-1540-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is there anyone show me how to crate msdu to capture using wireshark? help pls, thanks much</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Dec '10, 09:22</strong></p><img src="https://secure.gravatar.com/avatar/4f406866b1740fdc247ba628d2515395?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="haquyen&#39;s gravatar image" /><p><span>haquyen</span><br />
<span class="score" title="1 reputation points">1</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="haquyen has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Dec '10, 05:50</strong> </span></p></div></div><div id="comments-container-1540" class="comments-container"></div><div id="comment-tools-1540" class="comment-tools"></div><div class="clear"></div><div id="comment-1540-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1562"></span>

<div id="answer-container-1562" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1562-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1562-score" class="post-score" title="current number of votes">0</div><span id="post-1562-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There may be some easier ways, but a good place to start is "http://wiki.wireshark.org/Tools". There is a program called tcpreplay that will output a pcap back out on the wire. So you might capture some traffic, open the pcap in a hex editor then replay it out on the wire. That would be a good way to learn as well. Again, there might be easier ways to create frames.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Dec '10, 15:31</strong></p><img src="https://secure.gravatar.com/avatar/e62501f00394530927e4b0c9e86bfb46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Stewart&#39;s gravatar image" /><p><span>Paul Stewart</span><br />
<span class="score" title="301 reputation points">301</span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Stewart has 3 accepted answers">6%</span></p></div></div><div id="comments-container-1562" class="comments-container"><span id="1563"></span><div id="comment-1563" class="comment"><div id="post-1563-score" class="comment-score"></div><div class="comment-text"><p>I am assuming msdu to be MAC service data unit.</p></div><div id="comment-1563-info" class="comment-info"><span class="comment-age">(31 Dec '10, 15:36)</span> <span class="comment-user userinfo">Paul Stewart</span></div></div></div><div id="comment-tools-1562" class="comment-tools"></div><div class="clear"></div><div id="comment-1562-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

