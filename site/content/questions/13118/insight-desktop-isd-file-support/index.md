+++
type = "question"
title = "Insight Desktop (ISD) File Support"
description = '''Wondering when support for the Insight Desktop (.isd) file type might be included in a formal release? While Insight provides its own interface, I would prefer to conduct some analysis with Wireshark. Thank you, Jacob'''
date = "2012-07-30T09:31:00Z"
lastmod = "2012-07-30T12:51:00Z"
weight = 13118
keywords = [ "insight", "desktop" ]
aliases = [ "/questions/13118" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Insight Desktop (ISD) File Support](/questions/13118/insight-desktop-isd-file-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13118-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13118-score" class="post-score" title="current number of votes">0</div><span id="post-13118-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wondering when support for the Insight Desktop (.isd) file type might be included in a formal release? While Insight provides its own interface, I would prefer to conduct some analysis with Wireshark.</p><p>Thank you, Jacob</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-insight" rel="tag" title="see questions tagged &#39;insight&#39;">insight</span> <span class="post-tag tag-link-desktop" rel="tag" title="see questions tagged &#39;desktop&#39;">desktop</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '12, 09:31</strong></p><img src="https://secure.gravatar.com/avatar/e65b82e6cd84f629318bf833b203ca80?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="qcjacobo&#39;s gravatar image" /><p><span>qcjacobo</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="qcjacobo has no accepted answers">0%</span></p></div></div><div id="comments-container-13118" class="comments-container"><span id="13127"></span><div id="comment-13127" class="comment"><div id="post-13127-score" class="comment-score"></div><div class="comment-text"><p>May I ask:<br />
</p><ul><li>What is "Insight Desktop"?</li><li>Can you post the file format specs?</li></ul></div><div id="comment-13127-info" class="comment-info"><span class="comment-age">(30 Jul '12, 12:51)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-13118" class="comment-tools"></div><div class="clear"></div><div id="comment-13118-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13119"></span>

<div id="answer-container-13119" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13119-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13119-score" class="post-score" title="current number of votes">0</div><span id="post-13119-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See Wireshark <a href="http://www.wireshark.org/faq.html#q1.12">FAQ 1.12</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jul '12, 10:01</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span> </br></p></div></div><div id="comments-container-13119" class="comments-container"></div><div id="comment-tools-13119" class="comment-tools"></div><div class="clear"></div><div id="comment-13119-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

