+++
type = "question"
title = "Unable to decode HTTP2 traffic"
description = '''I am unable to decode HTTP2 traffic generated by nghttp client and nghttpd server from  nghttp2 library (https://github.com/tatsuhiro-t/nghttp2). I used plain-text mode (&quot;http&quot; URI scheme, without Upgrade option), sent request to the  server and download small txt file. Logs from the server and the ...'''
date = "2015-04-15T06:52:00Z"
lastmod = "2016-09-24T13:05:00Z"
weight = 41458
keywords = [ "nghttp2", "http2.0" ]
aliases = [ "/questions/41458" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to decode HTTP2 traffic](/questions/41458/unable-to-decode-http2-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41458-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41458-score" class="post-score" title="current number of votes">0</div><span id="post-41458-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am unable to decode HTTP2 traffic generated by nghttp client and nghttpd server from nghttp2 library (<a href="https://github.com/tatsuhiro-t/nghttp2).">https://github.com/tatsuhiro-t/nghttp2).</a></p><p>I used plain-text mode ("http" URI scheme, without Upgrade option), sent request to the server and download small txt file. Logs from the server and the client show, that everything was OK: connection was established and the file was downloaded. However, Wireshark recognized only "magic" frame (i.e. preface) as "HTTP2", any other HTTP2 frames were displayed.</p><p><strong>Useful info</strong></p><p>Linux Ubuntu 14.04 (Virtualbox VM running on Windows 8.1)</p><p>nghttp2 v0.7.9 (client and server applications, server running with --no-tls option, port 80)</p><p>Wireshark 1.9.3</p><p><a href="https://drive.google.com/file/d/0B1wzKgjWOr-USFI2aDlXN0FnaG8/view?usp=sharing">pcap_file</a></p><p><a href="https://drive.google.com/file/d/0B1wzKgjWOr-UWXROZl85bEtGWGM/view?usp=sharing">Wireshark_snapshot</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-nghttp2" rel="tag" title="see questions tagged &#39;nghttp2&#39;">nghttp2</span> <span class="post-tag tag-link-http2.0" rel="tag" title="see questions tagged &#39;http2.0&#39;">http2.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '15, 06:52</strong></p><img src="https://secure.gravatar.com/avatar/f1104aa497b247156611089ccf664b8d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kazimierz_ochodzki&#39;s gravatar image" /><p><span>kazimierz_oc...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kazimierz_ochodzki has no accepted answers">0%</span></p></div></div><div id="comments-container-41458" class="comments-container"><span id="41577"></span><div id="comment-41577" class="comment"><div id="post-41577-score" class="comment-score">1</div><div class="comment-text"><p>With Version 1.9.3 I had the same problem with your tracefile. After I have installed Version 1.9.5 the trace looked fine.</p></div><div id="comment-41577-info" class="comment-info"><span class="comment-age">(19 Apr '15, 10:08)</span> <span class="comment-user userinfo">Christian_R</span></div></div><span id="55796"></span><div id="comment-55796" class="comment"><div id="post-55796-score" class="comment-score"></div><div class="comment-text"><p>Did Kurt's solution solve your problem? I met the exact same issue in version 2.2, but there's no Enable HTTP2 heuristic option there!!!</p></div><div id="comment-55796-info" class="comment-info"><span class="comment-age">(24 Sep '16, 05:41)</span> <span class="comment-user userinfo">laike9m</span></div></div><span id="55798"></span><div id="comment-55798" class="comment"><div id="post-55798-score" class="comment-score"></div><div class="comment-text"><p><span>@laike9m</span> HTTP/2 got multiple heuristics, the Magic packet (enabled by default) and the very weak frame type check (removed since v1.99.9rc0-56-g78ca0af).</p></div><div id="comment-55798-info" class="comment-info"><span class="comment-age">(24 Sep '16, 05:50)</span> <span class="comment-user userinfo">Lekensteyn</span></div></div><span id="55799"></span><div id="comment-55799" class="comment"><div id="post-55799-score" class="comment-score"></div><div class="comment-text"><p><span></span><span>@Lekensteyn</span> So what do you think of my question? I've provided all information I have. Is it a bug or I'm doing something wrong?</p></div><div id="comment-55799-info" class="comment-info"><span class="comment-age">(24 Sep '16, 05:53)</span> <span class="comment-user userinfo">laike9m</span></div></div><span id="55803"></span><div id="comment-55803" class="comment"><div id="post-55803-score" class="comment-score"></div><div class="comment-text"><p><span>@laike9m</span> The heuristics option is gone, that is not a bug. The bug is that http2 detection somehow failed due to an issue in handling the ALPN extension. A <a href="https://code.wireshark.org/review/17907">fix</a> has been developed for the next versions of Wireshark.</p></div><div id="comment-55803-info" class="comment-info"><span class="comment-age">(24 Sep '16, 13:05)</span> <span class="comment-user userinfo">Lekensteyn</span></div></div></div><div id="comment-tools-41458" class="comment-tools"></div><div class="clear"></div><div id="comment-41458-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="41511"></span>

<div id="answer-container-41511" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41511-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41511-score" class="post-score" title="current number of votes">2</div><span id="post-41511-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The heuristic dissector for HTTP2 is disabled by default. If you enable it, the protocol will be detected, at least in your sample capture file.</p><blockquote><p>Edit -&gt; Preferences -&gt; Protocols -&gt; HTTP2 -&gt; Enable HTTP2 heuristic</p></blockquote><p>Hint from the code:</p><pre><code>dissect_http2_heur(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data)
{

    /* It is not easy to write a good http2 heuristic,
       this heuristic is disabled by default
     */

    if (!global_http2_heur)
    {
        return FALSE;
    }</code></pre><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Apr '15, 17:13</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-41511" class="comments-container"></div><div id="comment-tools-41511" class="comment-tools"></div><div class="clear"></div><div id="comment-41511-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="41459"></span>

<div id="answer-container-41459" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41459-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41459-score" class="post-score" title="current number of votes">0</div><span id="post-41459-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Using that version on Windows I had to use "Decode As ..." to decode all traffic to tcp port 80 as HTTP2 to get it all to dissect correctly.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '15, 06:58</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-41459" class="comments-container"></div><div id="comment-tools-41459" class="comment-tools"></div><div class="clear"></div><div id="comment-41459-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

