+++
type = "question"
title = "permission to use &quot;Wireshark&quot;"
description = '''Sir/Ma&#x27;m, As per I directed by my Cisco study material, I request you to grant me permission for using the application for my knowledge and labs going on with cisco for the &quot;CCNA&quot;. This is the first time I came to know about such product. Hope i will gain required knowledge from the product and so a...'''
date = "2011-10-01T08:51:00Z"
lastmod = "2011-10-01T10:26:00Z"
weight = 6674
keywords = [ "request" ]
aliases = [ "/questions/6674" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [permission to use "Wireshark"](/questions/6674/permission-to-use-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6674-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6674-score" class="post-score" title="current number of votes">0</div><span id="post-6674-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Sir/Ma'm, As per I directed by my Cisco study material, I request you to grant me permission for using the application for my knowledge and labs going on with cisco for the "CCNA". This is the first time I came to know about such product. Hope i will gain required knowledge from the product and so about the "WIRESHARE" . I apologize, if I had it wrong since this is my first time when i came across such product only for my Cisco Labs to be a Cisco Certified Network Associate. Thanks! HIMANSHU KUMAR <span class="__cf_email__" data-cfemail="0961606468677a617c2762647b38496e64686065276a6664">[email protected]</span></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Oct '11, 08:51</strong></p><img src="https://secure.gravatar.com/avatar/0c565dc007dac23b4dd0958b588f7351?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HIMANSHU%20KUMAR&#39;s gravatar image" /><p><span>HIMANSHU KUMAR</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HIMANSHU KUMAR has no accepted answers">0%</span></p></div></div><div id="comments-container-6674" class="comments-container"></div><div id="comment-tools-6674" class="comment-tools"></div><div class="clear"></div><div id="comment-6674-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6676"></span>

<div id="answer-container-6676" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6676-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6676-score" class="post-score" title="current number of votes">1</div><span id="post-6676-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Welcome to the world of Free and Open Source Software (<a href="http://en.wikipedia.org/wiki/FOSS">FOSS</a>). What sets us apart from the rest is that <a href="http://www.wireshark.org/faq.html#q1.6">we don't charge for the software or its use</a>, that it's free. The only requirements we have are stated in the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html">General Public License v2</a>, which come down to the requirement of sharing changes to the software with the community.</p><p>So, enjoy your work on CCNA, Wireshark is a great power-tool to look into your network.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Oct '11, 10:26</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-6676" class="comments-container"></div><div id="comment-tools-6676" class="comment-tools"></div><div class="clear"></div><div id="comment-6676-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

