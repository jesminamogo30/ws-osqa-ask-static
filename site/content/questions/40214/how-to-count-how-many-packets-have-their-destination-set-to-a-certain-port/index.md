+++
type = "question"
title = "How to count how many packets have their destination set to a certain port?"
description = '''Is it possible to calculate the number of packets that have their destination port set to 21 for example?'''
date = "2015-03-03T07:00:00Z"
lastmod = "2015-03-03T07:21:00Z"
weight = 40214
keywords = [ "packets", "port" ]
aliases = [ "/questions/40214" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to count how many packets have their destination set to a certain port?](/questions/40214/how-to-count-how-many-packets-have-their-destination-set-to-a-certain-port)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40214-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40214-score" class="post-score" title="current number of votes">0</div><span id="post-40214-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to calculate the number of packets that have their destination port set to 21 for example?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-port" rel="tag" title="see questions tagged &#39;port&#39;">port</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Mar '15, 07:00</strong></p><img src="https://secure.gravatar.com/avatar/027a47022c1202e41842d5b2a1d1202b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="natah&#39;s gravatar image" /><p><span>natah</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="natah has no accepted answers">0%</span></p></div></div><div id="comments-container-40214" class="comments-container"></div><div id="comment-tools-40214" class="comment-tools"></div><div class="clear"></div><div id="comment-40214-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40215"></span>

<div id="answer-container-40215" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40215-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40215-score" class="post-score" title="current number of votes">1</div><span id="post-40215-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sure. Filter on that destination port, e.g. "tcp.dstport==21", and look at the status bar to see how many "displayed" packets you have left.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Mar '15, 07:14</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-40215" class="comments-container"><span id="40217"></span><div id="comment-40217" class="comment"><div id="post-40217-score" class="comment-score"></div><div class="comment-text"><p>Thank you! That was pretty simple) I was looking too far for the answer</p></div><div id="comment-40217-info" class="comment-info"><span class="comment-age">(03 Mar '15, 07:21)</span> <span class="comment-user userinfo">natah</span></div></div></div><div id="comment-tools-40215" class="comment-tools"></div><div class="clear"></div><div id="comment-40215-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

