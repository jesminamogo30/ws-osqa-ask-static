+++
type = "question"
title = "How to find torrent name"
description = '''Hi. Can I find the name of the torrents are downloading with any technique ?'''
date = "2013-03-10T04:06:00Z"
lastmod = "2013-03-10T05:56:00Z"
weight = 19338
keywords = [ "capture.bittorrent", "torrent" ]
aliases = [ "/questions/19338" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to find torrent name](/questions/19338/how-to-find-torrent-name)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19338-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19338-score" class="post-score" title="current number of votes">0</div><span id="post-19338-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi. Can I find the name of the torrents are downloading with any technique ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture.bittorrent" rel="tag" title="see questions tagged &#39;capture.bittorrent&#39;">capture.bittorrent</span> <span class="post-tag tag-link-torrent" rel="tag" title="see questions tagged &#39;torrent&#39;">torrent</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Mar '13, 04:06</strong></p><img src="https://secure.gravatar.com/avatar/8056ddca24a5245ba7e1464ed4443934?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="apant&#39;s gravatar image" /><p><span>apant</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="apant has no accepted answers">0%</span></p></div></div><div id="comments-container-19338" class="comments-container"></div><div id="comment-tools-19338" class="comment-tools"></div><div class="clear"></div><div id="comment-19338-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19339"></span>

<div id="answer-container-19339" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19339-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19339-score" class="post-score" title="current number of votes">0</div><span id="post-19339-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That depends:</p><p>If you have the packets where someone is loading the torrent description file (usually via web browser or via magnet link), you can find the file name inside that file. A file format description can be found on the internet, e.g. at Wikipedia: <a href="http://en.wikipedia.org/wiki/Torrent_file">http://en.wikipedia.org/wiki/Torrent_file</a>. It won't work if the description file is transferred via HTTPS, of course.</p><p>If the torrent program is already loading the actual content, then no, I don't think you can. Nowadays, in most cases torrent transfers are also encrypted for obvious reasons, so you'll only see meaningless bytes being transferred on whatever port the torrent program uses.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Mar '13, 05:56</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Mar '13, 06:01</strong> </span></p></div></div><div id="comments-container-19339" class="comments-container"></div><div id="comment-tools-19339" class="comment-tools"></div><div class="clear"></div><div id="comment-19339-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

