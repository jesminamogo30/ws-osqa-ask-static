+++
type = "question"
title = "What does cause this ARP sweep on PC Windows 7 workstation?"
description = '''  As you can see, this ARP cause a lot of traffic which is slowing CPU switches.'''
date = "2016-11-30T12:23:00Z"
lastmod = "2016-11-30T12:27:00Z"
weight = 57736
keywords = [ "arp" ]
aliases = [ "/questions/57736" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What does cause this ARP sweep on PC Windows 7 workstation?](/questions/57736/what-does-cause-this-arp-sweep-on-pc-windows-7-workstation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57736-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57736-score" class="post-score" title="current number of votes">0</div><span id="post-57736-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/captura3.png" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/captura4.png" alt="alt text" /></p><p>As you can see, this ARP cause a lot of traffic which is slowing CPU switches.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-arp" rel="tag" title="see questions tagged &#39;arp&#39;">arp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Nov '16, 12:23</strong></p><img src="https://secure.gravatar.com/avatar/ab3e4e392c3eba03d2aceb465adad270?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vcossio&#39;s gravatar image" /><p><span>vcossio</span><br />
<span class="score" title="41 reputation points">41</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vcossio has no accepted answers">0%</span></p></img></div></div><div id="comments-container-57736" class="comments-container"><span id="57737"></span><div id="comment-57737" class="comment"><div id="post-57737-score" class="comment-score"></div><div class="comment-text"><p>Just download the pics and you will see them at 100% size. Here it looks blurry.</p><p>Greetings.</p></div><div id="comment-57737-info" class="comment-info"><span class="comment-age">(30 Nov '16, 12:24)</span> <span class="comment-user userinfo">vcossio</span></div></div></div><div id="comment-tools-57736" class="comment-tools"></div><div class="clear"></div><div id="comment-57736-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="57738"></span>

<div id="answer-container-57738" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57738-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57738-score" class="post-score" title="current number of votes">0</div><span id="post-57738-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Looks like an ARP Scan. If you didn´ initiated it, I would like to say the host 10.1.20.142 is compromised.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Nov '16, 12:27</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Nov '16, 12:29</strong> </span></p></div></div><div id="comments-container-57738" class="comments-container"></div><div id="comment-tools-57738" class="comment-tools"></div><div class="clear"></div><div id="comment-57738-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

