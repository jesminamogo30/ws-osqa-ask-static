+++
type = "question"
title = "Calculate throughput and packet loss?"
description = '''Calculate throughput and packet loss?'''
date = "2017-04-28T00:17:00Z"
lastmod = "2017-05-04T05:09:00Z"
weight = 61087
keywords = [ "throughput", "packetloss" ]
aliases = [ "/questions/61087" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Calculate throughput and packet loss?](/questions/61087/calculate-throughput-and-packet-loss)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61087-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61087-score" class="post-score" title="current number of votes">0</div><span id="post-61087-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Calculate throughput and packet loss?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-throughput" rel="tag" title="see questions tagged &#39;throughput&#39;">throughput</span> <span class="post-tag tag-link-packetloss" rel="tag" title="see questions tagged &#39;packetloss&#39;">packetloss</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Apr '17, 00:17</strong></p><img src="https://secure.gravatar.com/avatar/26f23099c93af0588e4af97575d70124?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ashraf1995&#39;s gravatar image" /><p><span>Ashraf1995</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ashraf1995 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Apr '17, 03:02</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-61087" class="comments-container"><span id="61094"></span><div id="comment-61094" class="comment"><div id="post-61094-score" class="comment-score"></div><div class="comment-text"><p>Of what? Help us out by being at least a little more specific on what you're looking for?</p><p>Have you searched for similar questions?</p><p>I've als fixed up the tags on the question as they were completely irrelevant.</p></div><div id="comment-61094-info" class="comment-info"><span class="comment-age">(28 Apr '17, 03:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="61100"></span><div id="comment-61100" class="comment"><div id="post-61100-score" class="comment-score"></div><div class="comment-text"><p>after the capture traffic of wireshark and convert the file to csv and open file in excel , I need method to calculate throughput,packet loss and average delay for "tcp and udp" in excel program.</p></div><div id="comment-61100-info" class="comment-info"><span class="comment-age">(28 Apr '17, 04:48)</span> <span class="comment-user userinfo">Ashraf1995</span></div></div><span id="61101"></span><div id="comment-61101" class="comment"><div id="post-61101-score" class="comment-score">1</div><div class="comment-text"><p>Surely if you're working with Excel then it's no longer a Wireshark question.</p><p>However, Wireshark itself has lots of statistical information available if you load the capture file and look under the Statistics menu.</p></div><div id="comment-61101-info" class="comment-info"><span class="comment-age">(28 Apr '17, 05:19)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="61125"></span><div id="comment-61125" class="comment"><div id="post-61125-score" class="comment-score"></div><div class="comment-text"><p>Please how Calculate or know throughput , average delay and packet loss in tcp,udp of wireshark after capture data?</p></div><div id="comment-61125-info" class="comment-info"><span class="comment-age">(30 Apr '17, 02:06)</span> <span class="comment-user userinfo">Ashraf1995</span></div></div></div><div id="comment-tools-61087" class="comment-tools"></div><div class="clear"></div><div id="comment-61087-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61216"></span>

<div id="answer-container-61216" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61216-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61216-score" class="post-score" title="current number of votes">0</div><span id="post-61216-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Knowing throughput is easy: Statistics - Summary. It will show the throughput for the entire file. If you want to know throughput for some specific traffic, filter it first, then Statistics - Summary, then look in displayed packets.</p><p>To calculate packet loss, depending on the type of traffic, you can either add up the tcp.retransmission and go from there.</p><p>If you have a capture with ICMP, and you want to calculate packet loss, keep the ICMP echos and reply. Then, filter again using ICMP that contains no answers (icmp.no_resp), and look for the percentage of displayed packet. There is your packet loss %.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 May '17, 12:25</strong></p><img src="https://secure.gravatar.com/avatar/21cfa2071214d5dacbb6d0cd9769a6d1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jerioux&#39;s gravatar image" /><p><span>jerioux</span><br />
<span class="score" title="25 reputation points">25</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jerioux has no accepted answers">0%</span></p></div></div><div id="comments-container-61216" class="comments-container"><span id="61217"></span><div id="comment-61217" class="comment"><div id="post-61217-score" class="comment-score"></div><div class="comment-text"><p>Ok I need to calculate throughput, packet loss and average delay of tcp and udp only ?</p></div><div id="comment-61217-info" class="comment-info"><span class="comment-age">(03 May '17, 20:13)</span> <span class="comment-user userinfo">Ashraf1995</span></div></div><span id="61227"></span><div id="comment-61227" class="comment"><div id="post-61227-score" class="comment-score"></div><div class="comment-text"><p>Hello,</p><p>Do you have an existing capture to share with us? I can help you find these information.</p></div><div id="comment-61227-info" class="comment-info"><span class="comment-age">(04 May '17, 05:09)</span> <span class="comment-user userinfo">jerioux</span></div></div></div><div id="comment-tools-61216" class="comment-tools"></div><div class="clear"></div><div id="comment-61216-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

