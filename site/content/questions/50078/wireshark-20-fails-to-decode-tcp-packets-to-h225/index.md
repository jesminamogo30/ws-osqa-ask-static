+++
type = "question"
title = "Wireshark 2.0 Fails to decode tcp packets to H225"
description = '''I recently upgraded to 2.0. It doesn&#x27;t matter what I do in 2.0. I can not decode TCP packets into H225 or Q391 decodes. I do the decode as option and select the protocol and port that i know the packets are, but it wont decode them. I have gone into preferences and made sure the protocol that i want...'''
date = "2016-02-10T14:45:00Z"
lastmod = "2016-02-11T01:41:00Z"
weight = 50078
keywords = [ "decode", "wireshark-2.0", "h225" ]
aliases = [ "/questions/50078" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 2.0 Fails to decode tcp packets to H225](/questions/50078/wireshark-20-fails-to-decode-tcp-packets-to-h225)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50078-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50078-score" class="post-score" title="current number of votes">0</div><span id="post-50078-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I recently upgraded to 2.0. It doesn't matter what I do in 2.0. I can not decode TCP packets into H225 or Q391 decodes.</p><p>I do the decode as option and select the protocol and port that i know the packets are, but it wont decode them.</p><p>I have gone into preferences and made sure the protocol that i want is enabled</p><p>If i take the same capture, and open it with 1.8.6 not only does it automatically decode but the packet is broken down in the H225 and Q391 as i am accustom to</p><p>What am I doing wrong.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-wireshark-2.0" rel="tag" title="see questions tagged &#39;wireshark-2.0&#39;">wireshark-2.0</span> <span class="post-tag tag-link-h225" rel="tag" title="see questions tagged &#39;h225&#39;">h225</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Feb '16, 14:45</strong></p><img src="https://secure.gravatar.com/avatar/4c0f9110b9bbb57a0553feb846c43e87?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cbwillim8770&#39;s gravatar image" /><p><span>cbwillim8770</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cbwillim8770 has no accepted answers">0%</span></p></div></div><div id="comments-container-50078" class="comments-container"><span id="50089"></span><div id="comment-50089" class="comment"><div id="post-50089-score" class="comment-score"></div><div class="comment-text"><p>This is not expected. Could you share the capture of even better fill a bug to <a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a> with a sample pcap attached?</p></div><div id="comment-50089-info" class="comment-info"><span class="comment-age">(11 Feb '16, 01:41)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-50078" class="comment-tools"></div><div class="clear"></div><div id="comment-50078-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

