+++
type = "question"
title = "CustomColumn &quot;data.text&quot;"
description = '''In Wireshark 1.6.4 I have a &quot;costum column&quot; - &quot;data.text&quot; to see my Telegramm-stream What about this information in Wireshark 1.8 --&amp;gt; data.text show&#x27;s me an empty column?'''
date = "2012-09-03T23:48:00Z"
lastmod = "2013-10-23T17:32:00Z"
weight = 14011
keywords = [ "text", "data" ]
aliases = [ "/questions/14011" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [CustomColumn "data.text"](/questions/14011/customcolumn-datatext)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14011-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14011-score" class="post-score" title="current number of votes">0</div><span id="post-14011-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In Wireshark 1.6.4 I have a "costum column" - "data.text" to see my Telegramm-stream</p><p>What about this information in Wireshark 1.8 --&gt; data.text show's me an empty column?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-text" rel="tag" title="see questions tagged &#39;text&#39;">text</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '12, 23:48</strong></p><img src="https://secure.gravatar.com/avatar/83a8b2ead6ab39c6677b453b70f672c7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sniffer&#39;s gravatar image" /><p><span>sniffer</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sniffer has no accepted answers">0%</span></p></div></div><div id="comments-container-14011" class="comments-container"></div><div id="comment-tools-14011" class="comment-tools"></div><div class="clear"></div><div id="comment-14011-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26343"></span>

<div id="answer-container-26343" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26343-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26343-score" class="post-score" title="current number of votes">0</div><span id="post-26343-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Very hard to find.. but:</p><p>Edit &gt; Preferences &gt; Protocols &gt; Data &gt; check "Show data as text"</p><p>Then the custom column using data.text will show text in the column.</p><p>Now I can see the packet data (payload) for any protocol in plain text at a glance!</p><p>(Using Version 1.10.2 (SVN Rev 51934 from /trunk-1.10))</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Oct '13, 17:32</strong></p><img src="https://secure.gravatar.com/avatar/2be00c4204889a653c6d60c9867cf4e9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krb&#39;s gravatar image" /><p><span>krb</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krb has no accepted answers">0%</span></p></div></div><div id="comments-container-26343" class="comments-container"></div><div id="comment-tools-26343" class="comment-tools"></div><div class="clear"></div><div id="comment-26343-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

