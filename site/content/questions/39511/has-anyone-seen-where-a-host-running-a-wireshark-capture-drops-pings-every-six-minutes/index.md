+++
type = "question"
title = "Has anyone seen where a Host running a wireshark capture drops pings every six minutes"
description = '''I am working an issue for my operations team related to false network down alarms. I found that the machines drop a ping request every six minutes like clockwork. All three machines are running WireShark and capturing network traffic at the time the ping is dropped. Is there a process built into Wir...'''
date = "2015-01-30T12:43:00Z"
lastmod = "2015-01-30T12:43:00Z"
weight = 39511
keywords = [ "dropped", "pings" ]
aliases = [ "/questions/39511" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Has anyone seen where a Host running a wireshark capture drops pings every six minutes](/questions/39511/has-anyone-seen-where-a-host-running-a-wireshark-capture-drops-pings-every-six-minutes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39511-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39511-score" class="post-score" title="current number of votes">0</div><span id="post-39511-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am working an issue for my operations team related to false network down alarms.</p><p>I found that the machines drop a ping request every six minutes like clockwork.</p><p>All three machines are running WireShark and capturing network traffic at the time the ping is dropped. Is there a process built into WireShark that would cause this? If so is there a setting that I can change to correct this behavior?</p><p>Two of the machines are Windows 2008 Server (64-bit), the third is a Windows 7 32-bit desktop.</p><p>Thanks...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dropped" rel="tag" title="see questions tagged &#39;dropped&#39;">dropped</span> <span class="post-tag tag-link-pings" rel="tag" title="see questions tagged &#39;pings&#39;">pings</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jan '15, 12:43</strong></p><img src="https://secure.gravatar.com/avatar/0adb36909d5d1ca233e87b92b772f4a7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PaulF&#39;s gravatar image" /><p><span>PaulF</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PaulF has no accepted answers">0%</span></p></div></div><div id="comments-container-39511" class="comments-container"></div><div id="comment-tools-39511" class="comment-tools"></div><div class="clear"></div><div id="comment-39511-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

