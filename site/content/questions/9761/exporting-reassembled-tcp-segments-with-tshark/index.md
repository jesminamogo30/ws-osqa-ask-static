+++
type = "question"
title = "exporting reassembled TCP segments with tshark"
description = '''when wireshark exports to a file, there is a line that says if the packet is a reassembled one, and which other packet it consists of: like this: 10 Reassembled TCP Segments (13611 bytes): #4411(1420), #4412(1420), #4414(1420), #4415(1420), #4416(1420), #4417(1420), #4418(1420), #4419(1420), #4420(1...'''
date = "2012-03-26T04:00:00Z"
lastmod = "2012-03-26T04:00:00Z"
weight = 9761
keywords = [ "export", "tshark", "command-line" ]
aliases = [ "/questions/9761" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [exporting reassembled TCP segments with tshark](/questions/9761/exporting-reassembled-tcp-segments-with-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9761-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9761-score" class="post-score" title="current number of votes">0</div><span id="post-9761-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when wireshark exports to a file, there is a line that says if the packet is a reassembled one, and which other packet it consists of: like this:</p><p>10 Reassembled TCP Segments (13611 bytes): #4411(1420), #4412(1420), #4414(1420), #4415(1420), #4416(1420), #4417(1420), #4418(1420), #4419(1420), #4420(1420), #4421(831)</p><p>I'm trying to get the same information exported using the command line. currently I'm trying tshark. which fields should be included?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-command-line" rel="tag" title="see questions tagged &#39;command-line&#39;">command-line</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '12, 04:00</strong></p><img src="https://secure.gravatar.com/avatar/26beadcd7dc41269e2ef7dd0116069e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gamba&#39;s gravatar image" /><p><span>gamba</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gamba has no accepted answers">0%</span></p></div></div><div id="comments-container-9761" class="comments-container"></div><div id="comment-tools-9761" class="comment-tools"></div><div class="clear"></div><div id="comment-9761-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

