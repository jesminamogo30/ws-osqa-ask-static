+++
type = "question"
title = "Can You help me with ftp.wireshark.org?"
description = '''I can no longer connect to ftp.wireshark.org with many softwares: beyond compare, cuteftp, filezilla and windows explorer. Instead the ftp client from windows command prompt works. I tried this on a couple of pcs. Do You know which settings I can use to connect to that ftp site and download files? F...'''
date = "2015-05-16T05:13:00Z"
lastmod = "2015-05-19T04:12:00Z"
weight = 42436
keywords = [ "ftp" ]
aliases = [ "/questions/42436" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Can You help me with ftp.wireshark.org?](/questions/42436/can-you-help-me-with-ftpwiresharkorg)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42436-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42436-score" class="post-score" title="current number of votes">0</div><span id="post-42436-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can no longer connect to ftp.wireshark.org with many softwares: beyond compare, cuteftp, filezilla and windows explorer. Instead the ftp client from windows command prompt works. I tried this on a couple of pcs. Do You know which settings I can use to connect to that ftp site and download files? For example, with beyond compare it stops after the PASV command, that gives me a message: "15/05/2015 09:12:44 Recv&gt; 227 Entering Passive Mode (45,55,185,126,139,232).". If I disable the Passive Mode, it stops at the LIST command. Any hint?</p><p>Thank You and regards</p><p>Rodolfo Giovanninetti</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 May '15, 05:13</strong></p><img src="https://secure.gravatar.com/avatar/f4b6c22791d869e53ce381279a06fb08?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RodolfoGiovanninetti&#39;s gravatar image" /><p><span>RodolfoGiova...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RodolfoGiovanninetti has no accepted answers">0%</span></p></div></div><div id="comments-container-42436" class="comments-container"></div><div id="comment-tools-42436" class="comment-tools"></div><div class="clear"></div><div id="comment-42436-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42469"></span>

<div id="answer-container-42469" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42469-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42469-score" class="post-score" title="current number of votes">1</div><span id="post-42469-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Can you try it again? The server had a configuration issue which should now be fixed.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 May '15, 10:12</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-42469" class="comments-container"><span id="42531"></span><div id="comment-42531" class="comment"><div id="post-42531-score" class="comment-score"></div><div class="comment-text"><p>Thank You, now it works.</p><p>Regards</p><p>Rodolfo Giovanninetti</p></div><div id="comment-42531-info" class="comment-info"><span class="comment-age">(19 May '15, 01:36)</span> <span class="comment-user userinfo">RodolfoGiova...</span></div></div><span id="42532"></span><div id="comment-42532" class="comment"><div id="post-42532-score" class="comment-score"></div><div class="comment-text"><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-42532-info" class="comment-info"><span class="comment-age">(19 May '15, 04:12)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-42469" class="comment-tools"></div><div class="clear"></div><div id="comment-42469-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

