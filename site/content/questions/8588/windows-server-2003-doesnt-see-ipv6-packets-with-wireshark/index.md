+++
type = "question"
title = "Windows Server 2003 doesn&#x27;t see IPv6 packets with Wireshark"
description = '''This is a VM on an HP G7 server. I know....it&#x27;s an OLD OS, but it is necesary for my application. If I use Linux or XP or anything else, it picks up the V6 packets fine. Seems like it is getting discarded at a low level. Any ideas? -Chris'''
date = "2012-01-24T13:55:00Z"
lastmod = "2012-01-24T13:55:00Z"
weight = 8588
keywords = [ "windows", "2003", "server", "w2k3", "ipv6" ]
aliases = [ "/questions/8588" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Windows Server 2003 doesn't see IPv6 packets with Wireshark](/questions/8588/windows-server-2003-doesnt-see-ipv6-packets-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8588-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8588-score" class="post-score" title="current number of votes">0</div><span id="post-8588-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This is a VM on an HP G7 server. I know....it's an OLD OS, but it is necesary for my application. If I use Linux or XP or anything else, it picks up the V6 packets fine. Seems like it is getting discarded at a low level. Any ideas? -Chris</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-2003" rel="tag" title="see questions tagged &#39;2003&#39;">2003</span> <span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span> <span class="post-tag tag-link-w2k3" rel="tag" title="see questions tagged &#39;w2k3&#39;">w2k3</span> <span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '12, 13:55</strong></p><img src="https://secure.gravatar.com/avatar/f010cbc087141b79679b665bb28469db?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christopher%20Cormier&#39;s gravatar image" /><p><span>Christopher ...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christopher Cormier has no accepted answers">0%</span></p></div></div><div id="comments-container-8588" class="comments-container"></div><div id="comment-tools-8588" class="comment-tools"></div><div class="clear"></div><div id="comment-8588-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

