+++
type = "question"
title = "A way to figure out router admin url with wireshark?"
description = '''Hi all, I have a DG860 Arris router, but the router admin url isn&#x27;t working. I have read the manual, and it says that I have to go to http://192.168.0.1/, but that brings up an unresponsive page.  I&#x27;ve tried other urls like 192.168.1.1, 10.0.0.1, 192.168.2.1, but I get the same result. I have tried ...'''
date = "2013-08-08T09:33:00Z"
lastmod = "2013-08-08T10:29:00Z"
weight = 23657
keywords = [ "router", "administration" ]
aliases = [ "/questions/23657" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [A way to figure out router admin url with wireshark?](/questions/23657/a-way-to-figure-out-router-admin-url-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23657-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23657-score" class="post-score" title="current number of votes">0</div><span id="post-23657-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all, I have a DG860 Arris router, but the router admin url isn't working. I have read the manual, and it says that I have to go to <code>http://192.168.0.1/</code>, but that brings up an unresponsive page.</p><p>I've tried other urls like 192.168.1.1, 10.0.0.1, 192.168.2.1, but I get the same result. I have tried it on wireless and on wired.</p><p>I can't reset the router because my roommates are afraid I'll break it. My roommates don't know how to access the admin url either.</p><p>Is there a way to use wireshark to figure out how to get to the admin url on the router?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-administration" rel="tag" title="see questions tagged &#39;administration&#39;">administration</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Aug '13, 09:33</strong></p><img src="https://secure.gravatar.com/avatar/484c0d5e8e287b45e781f6723596c1d7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="saiooss99&#39;s gravatar image" /><p><span>saiooss99</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="saiooss99 has no accepted answers">0%</span></p></div></div><div id="comments-container-23657" class="comments-container"></div><div id="comment-tools-23657" class="comment-tools"></div><div class="clear"></div><div id="comment-23657-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23659"></span>

<div id="answer-container-23659" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23659-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23659-score" class="post-score" title="current number of votes">0</div><span id="post-23659-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You need the IP, not the URL I guess ;-)</p><p>If you can surf the internet, it usually means that you are <strong>using</strong> the router as a, well, router. Just run "ipconfig" on a command line prompt to check out what your default gateway is and use that IP in your URL.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Aug '13, 10:29</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-23659" class="comments-container"></div><div id="comment-tools-23659" class="comment-tools"></div><div class="clear"></div><div id="comment-23659-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

