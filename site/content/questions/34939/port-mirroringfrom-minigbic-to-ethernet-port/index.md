+++
type = "question"
title = "port mirroring...from miniGBIC to ethernet port."
description = '''Hello  I am trying to port mirror on a cisco switch sg200-26 which has got 26 Ethernet port and 2 mini GBIC for  fiber optics connectivity. I want to mirror one miniGBIC port to one of Ethernet port but its not happening.do we need seprate TAP or any equipment to do this mirroring. kindly help. Rega...'''
date = "2014-07-28T03:58:00Z"
lastmod = "2014-07-28T08:00:00Z"
weight = 34939
keywords = [ "to", "gbic", "mirroring", "ethernet" ]
aliases = [ "/questions/34939" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [port mirroring...from miniGBIC to ethernet port.](/questions/34939/port-mirroringfrom-minigbic-to-ethernet-port)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34939-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34939-score" class="post-score" title="current number of votes">0</div><span id="post-34939-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I am trying to port mirror on a cisco switch sg200-26 which has got 26 Ethernet port and 2 mini GBIC for fiber optics connectivity. I want to mirror one miniGBIC port to one of Ethernet port but its not happening.do we need seprate TAP or any equipment to do this mirroring.</p><p>kindly help.</p><p>Regards</p><p>Rohan</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-to" rel="tag" title="see questions tagged &#39;to&#39;">to</span> <span class="post-tag tag-link-gbic" rel="tag" title="see questions tagged &#39;gbic&#39;">gbic</span> <span class="post-tag tag-link-mirroring" rel="tag" title="see questions tagged &#39;mirroring&#39;">mirroring</span> <span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jul '14, 03:58</strong></p><img src="https://secure.gravatar.com/avatar/d1d4cd7f445ae4cee7bad1f8f5ee242a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gaurnet&#39;s gravatar image" /><p><span>gaurnet</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gaurnet has no accepted answers">0%</span></p></div></div><div id="comments-container-34939" class="comments-container"></div><div id="comment-tools-34939" class="comment-tools"></div><div class="clear"></div><div id="comment-34939-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34943"></span>

<div id="answer-container-34943" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34943-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34943-score" class="post-score" title="current number of votes">0</div><span id="post-34943-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think this is something either someone having the same equipment can answer, or the vendor support. So you might just ask Cisco how this works; I have the SG200-8 which does not have the mini GBIC ports, and it mirrors everything just fine. But since it is not the same switch I don't know how yours works. Better ask Cisco if the device supports mirroring the miniGBIC ports (which I expect it would).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Jul '14, 08:00</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-34943" class="comments-container"></div><div id="comment-tools-34943" class="comment-tools"></div><div class="clear"></div><div id="comment-34943-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

