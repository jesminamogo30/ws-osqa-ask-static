+++
type = "question"
title = "Ubuntu 12.04 can not set wlan0 into monitor mode in wireshark 1.6"
description = '''I can set wlan0 into monitor mode by: iwconfig wlan0 mode monitor. However, I can not check the box of capture packets in monitor mode in wireshark.  The error is &quot;The capabilities of the capture device &quot;mon0&quot; could not be obtained (That device doesn&#x27;t support monitor mode). Please check to make sur...'''
date = "2012-11-06T20:45:00Z"
lastmod = "2012-11-06T20:45:00Z"
weight = 15600
keywords = [ "wireless", "wireshark", "monitor", "mode" ]
aliases = [ "/questions/15600" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ubuntu 12.04 can not set wlan0 into monitor mode in wireshark 1.6](/questions/15600/ubuntu-1204-can-not-set-wlan0-into-monitor-mode-in-wireshark-16)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15600-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15600-score" class="post-score" title="current number of votes">0</div><span id="post-15600-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can set wlan0 into monitor mode by: iwconfig wlan0 mode monitor. However, I can not check the box of capture packets in monitor mode in wireshark.</p><p>The error is "The capabilities of the capture device "mon0" could not be obtained (That device doesn't support monitor mode). Please check to make sure you have sufficient permissions, and that you have the proper interface or pipe specified."</p><p>I also tried airmon-ng start wlan0, the same error happened on mon0.</p><p>Same error happened on iwlagn, iw4965 and rt2800 usb driver.</p><p>How can I do? changed another wireless cards? thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span> <span class="post-tag tag-link-mode" rel="tag" title="see questions tagged &#39;mode&#39;">mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '12, 20:45</strong></p><img src="https://secure.gravatar.com/avatar/5d4b0e49653f79a63f4024c3f377f6bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="geneopenflow&#39;s gravatar image" /><p><span>geneopenflow</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="geneopenflow has no accepted answers">0%</span></p></div></div><div id="comments-container-15600" class="comments-container"></div><div id="comment-tools-15600" class="comment-tools"></div><div class="clear"></div><div id="comment-15600-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

