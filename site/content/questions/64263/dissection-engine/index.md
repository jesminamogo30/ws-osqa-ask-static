+++
type = "question"
title = "Dissection engine"
description = '''Hello, Please, I want to reuse the module &#x27;epan&#x27; in a program for dissect myself captured packets. Can someone tell me how I can proceed? (an example of code would be really appreciated). Thank you verry much.'''
date = "2017-10-26T15:38:00Z"
lastmod = "2017-10-26T15:38:00Z"
weight = 64263
keywords = [ "dissection" ]
aliases = [ "/questions/64263" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Dissection engine](/questions/64263/dissection-engine)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64263-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64263-score" class="post-score" title="current number of votes">0</div><span id="post-64263-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, Please, I want to reuse the module 'epan' in a program for dissect myself captured packets. Can someone tell me how I can proceed? (an example of code would be really appreciated).</p><p>Thank you verry much.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissection" rel="tag" title="see questions tagged &#39;dissection&#39;">dissection</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '17, 15:38</strong></p><img src="https://secure.gravatar.com/avatar/c9bdca7f9a009983556ab57fb897d9e6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="francisyuya&#39;s gravatar image" /><p><span>francisyuya</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="francisyuya has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Oct '17, 01:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-64263" class="comments-container"></div><div id="comment-tools-64263" class="comment-tools"></div><div class="clear"></div><div id="comment-64263-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

