+++
type = "question"
title = "how to capture gzip content web request?"
description = '''Real beginner here, just downloaded Wire Shark 2.4.0. I have application send gzip content to and from server.  how to capture gzip content as a file and test it offline and see if it&#x27;s been correctly created by the application? Thanks for your help. '''
date = "2017-07-22T11:32:00Z"
lastmod = "2017-07-22T16:24:00Z"
weight = 63001
keywords = [ "gzip" ]
aliases = [ "/questions/63001" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to capture gzip content web request?](/questions/63001/how-to-capture-gzip-content-web-request)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63001-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63001-score" class="post-score" title="current number of votes">0</div><span id="post-63001-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Real beginner here, just downloaded Wire Shark 2.4.0.</p><p>I have application send gzip content to and from server.</p><p>how to capture gzip content as a file and test it offline and see if it's been correctly created by the application?</p><p>Thanks for your help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gzip" rel="tag" title="see questions tagged &#39;gzip&#39;">gzip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jul '17, 11:32</strong></p><img src="https://secure.gravatar.com/avatar/a687fb32855dc482b23687ea94aeb66b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yukata&#39;s gravatar image" /><p><span>Yukata</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yukata has no accepted answers">0%</span></p></div></div><div id="comments-container-63001" class="comments-container"></div><div id="comment-tools-63001" class="comment-tools"></div><div class="clear"></div><div id="comment-63001-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63002"></span>

<div id="answer-container-63002" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63002-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63002-score" class="post-score" title="current number of votes">0</div><span id="post-63002-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Based on your question title, I am assuming the gzip file is transmitted via http. If so, you can click on...</p><p><strong>FILE &gt; Export Objects &gt; http</strong></p><p>...and save the gzip file to your pc. If it wasn't transmitted via http, you will need to provide more information.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jul '17, 16:24</strong></p><img src="https://secure.gravatar.com/avatar/bb79e0c62df46ecf47cc004a0a2d3cbc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rooster_50&#39;s gravatar image" /><p><span>Rooster_50</span><br />
<span class="score" title="238 reputation points">238</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="18 badges"><span class="bronze">●</span><span class="badgecount">18</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rooster_50 has 5 accepted answers">15%</span></p></div></div><div id="comments-container-63002" class="comments-container"></div><div id="comment-tools-63002" class="comment-tools"></div><div class="clear"></div><div id="comment-63002-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

