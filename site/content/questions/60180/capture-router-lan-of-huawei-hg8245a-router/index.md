+++
type = "question"
title = "Capture router lan of Huawei HG8245A router"
description = '''I have Home WLAN Router it is Huawei HG8245A for parental monitoring purpose i want to tap the router connection and monitor the inbound and outbound router connection is it possible to do that without netgate, Mikrotik RB, etc ?'''
date = "2017-03-19T10:09:00Z"
lastmod = "2017-03-20T06:42:00Z"
weight = 60180
keywords = [ "router", "lan", "huawei", "capture" ]
aliases = [ "/questions/60180" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture router lan of Huawei HG8245A router](/questions/60180/capture-router-lan-of-huawei-hg8245a-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60180-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60180-score" class="post-score" title="current number of votes">0</div><span id="post-60180-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have Home WLAN Router it is Huawei HG8245A for parental monitoring purpose i want to tap the router connection and monitor the inbound and outbound router connection is it possible to do that without netgate, Mikrotik RB, etc ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-lan" rel="tag" title="see questions tagged &#39;lan&#39;">lan</span> <span class="post-tag tag-link-huawei" rel="tag" title="see questions tagged &#39;huawei&#39;">huawei</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Mar '17, 10:09</strong></p><img src="https://secure.gravatar.com/avatar/160575a5c1f58ed2a414f48245e32ee8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dklainbz&#39;s gravatar image" /><p><span>dklainbz</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dklainbz has no accepted answers">0%</span></p></div></div><div id="comments-container-60180" class="comments-container"></div><div id="comment-tools-60180" class="comment-tools"></div><div class="clear"></div><div id="comment-60180-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60196"></span>

<div id="answer-container-60196" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60196-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60196-score" class="post-score" title="current number of votes">0</div><span id="post-60196-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, but if you want to perform "parental monitoring", then why not use the router's built-in security mechanisms?</p><p>The router should have a security tab, and that has the option to block certain URL's. Below is a link to the router's manual:</p><p><a href="http://enterprise.huawei.com/ilink/enenterprise/download/HW_U_149045">http://enterprise.huawei.com/ilink/enenterprise/download/HW_U_149045</a></p><p>Page 3-22 (in section 3.3.5) describes how to block (i.e., Blacklist) certain URL's.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '17, 06:42</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-60196" class="comments-container"></div><div id="comment-tools-60196" class="comment-tools"></div><div class="clear"></div><div id="comment-60196-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

