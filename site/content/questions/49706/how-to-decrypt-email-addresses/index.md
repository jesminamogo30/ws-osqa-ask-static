+++
type = "question"
title = "How to Decrypt Email Addresses?"
description = '''Hello Everyone, I have been trying to find the first part of an email address in a wireshark capture. Finding the packet was easy, as I knew from looking through all the http packets that it ended in inbox.com. However, once I looked at the login email, it was encrypted (no surprise there, if it was...'''
date = "2016-02-01T20:12:00Z"
lastmod = "2016-02-01T20:12:00Z"
weight = 49706
keywords = [ "decryption", "email" ]
aliases = [ "/questions/49706" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to Decrypt Email Addresses?](/questions/49706/how-to-decrypt-email-addresses)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49706-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49706-score" class="post-score" title="current number of votes">0</div><span id="post-49706-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/WiresharkProb_QYMzJzg.PNG" alt="alt text" />Hello Everyone, I have been trying to find the first part of an email address in a wireshark capture. Finding the packet was easy, as I knew from looking through all the http packets that it ended in inbox.com. However, once I looked at the login email, it was encrypted (no surprise there, if it wasn't that'd be a problem). Is there any way I can decrypt this email address and find out what it is without knowing the key? Or is there something else that I'm missing here? Thanks in advance!</p><p>(screenshot included)<img src="http://" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-email" rel="tag" title="see questions tagged &#39;email&#39;">email</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '16, 20:12</strong></p><img src="https://secure.gravatar.com/avatar/d9a151081bbdcf69cccfb940f82816ff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DanielChen&#39;s gravatar image" /><p><span>DanielChen</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DanielChen has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Feb '16, 20:15</strong> </span></p></div></div><div id="comments-container-49706" class="comments-container"></div><div id="comment-tools-49706" class="comment-tools"></div><div class="clear"></div><div id="comment-49706-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

