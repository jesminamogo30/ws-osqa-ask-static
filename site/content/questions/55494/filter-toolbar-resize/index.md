+++
type = "question"
title = "Filter Toolbar resize?"
description = '''Is there a way to resize the individual components like shorten the currently displayed filter so I can have more filter expressions showing? Edit: Ver 2.2.0'''
date = "2016-09-12T11:13:00Z"
lastmod = "2016-09-13T19:12:00Z"
weight = 55494
keywords = [ "filter", "toolbar", "resize" ]
aliases = [ "/questions/55494" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Filter Toolbar resize?](/questions/55494/filter-toolbar-resize)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55494-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55494-score" class="post-score" title="current number of votes">0</div><span id="post-55494-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there a way to resize the individual components like shorten the currently displayed filter so I can have more filter expressions showing?</p><p>Edit: Ver 2.2.0</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-toolbar" rel="tag" title="see questions tagged &#39;toolbar&#39;">toolbar</span> <span class="post-tag tag-link-resize" rel="tag" title="see questions tagged &#39;resize&#39;">resize</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Sep '16, 11:13</strong></p><img src="https://secure.gravatar.com/avatar/0833f7ef8618ac6b7842265fbaa39861?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="itsme0k&#39;s gravatar image" /><p><span>itsme0k</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="itsme0k has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Sep '16, 11:21</strong> </span></p></div></div><div id="comments-container-55494" class="comments-container"></div><div id="comment-tools-55494" class="comment-tools"></div><div class="clear"></div><div id="comment-55494-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55544"></span>

<div id="answer-container-55544" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55544-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55544-score" class="post-score" title="current number of votes">0</div><span id="post-55544-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I assume that, by "more filter expressions showing", you're referring to the expressions that show on the filter toolbar to the right of the filter text box, the "Expressions..." button, and the "+" button.</p><p>If so, then adding new expressions with the "+" button shortens the filter text box to make room for the new expression's name up to a certain point, but there appears to be a point where it doesn't show any more in the toolbar, it just shows a "&gt;&gt;" that, when clicked, pops up a menu with the additional filters.</p><p>That's the only current option. If you want to suggest that either 1) the filter text box be resizable or 2) the minimum filter text box size be smaller, you could file a request for enhancement on <a href="http://bugs.wireshark.org/">the Wireshark Bugzilla</a>, which is the best place to request new features or changes to existing features (or to report bugs) - it's where we expect them to be filed, rather than on the Q&amp;A site, as they can be tracked better there.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Sep '16, 19:12</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-55544" class="comments-container"></div><div id="comment-tools-55544" class="comment-tools"></div><div class="clear"></div><div id="comment-55544-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

