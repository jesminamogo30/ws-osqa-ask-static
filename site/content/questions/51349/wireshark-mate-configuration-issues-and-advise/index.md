+++
type = "question"
title = "Wireshark MATE configuration issues and advise"
description = '''Hi, I have few questions relating to MATE   Sometimes when I open the wireshark traces, In GOG level some errors coming as undefined attribute with garbled characters. Some of them point to properly defined variables which I defined it as Extract. Why its causing issues. Is it possible to do some tr...'''
date = "2016-04-01T07:02:00Z"
lastmod = "2016-04-01T07:02:00Z"
weight = 51349
keywords = [ "mate", "wireshark", "syntax" ]
aliases = [ "/questions/51349" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark MATE configuration issues and advise](/questions/51349/wireshark-mate-configuration-issues-and-advise)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51349-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51349-score" class="post-score" title="current number of votes">0</div><span id="post-51349-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have few questions relating to MATE</p><ol><li><p>Sometimes when I open the wireshark traces, In GOG level some errors coming as undefined attribute with garbled characters. Some of them point to properly defined variables which I defined it as Extract. Why its causing issues. Is it possible to do some tracing/debugging on similar MATE configuration file issues</p></li><li><p>In PDUs, I extract variables from multiple protocol subtrees. Some PDUs will not have some subtrees, will this cause null AVPLs and cause issues. How can I use extract statement if the protocol/subtree is valid in that PDU. Can I use transform for that purpose ?</p></li><li><p>In GoG, can I match GoPs with multiple common values. If not how can we achieve to group these into a GoG</p></li></ol><p>GoP X (a,b), GoP Y (a,c) -&gt; a in GoP X has multiple values, a in GoP Y has multiple values</p><p>Sometimes when I re-open the same trace from wireshark with same MATE configuration, I see different behaviors (e.g some errors which were there do not appear again). Do I need to clear any cache inorder to wireshark to re-read the MATE configuration when it restarts ? I am storing the wireshark mate file in my desktop, not sure if it makes any difference</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mate" rel="tag" title="see questions tagged &#39;mate&#39;">mate</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-syntax" rel="tag" title="see questions tagged &#39;syntax&#39;">syntax</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '16, 07:02</strong></p><img src="https://secure.gravatar.com/avatar/4a2a1ab8f8fa05aa1d21e5b43f767aae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sshark&#39;s gravatar image" /><p><span>sshark</span><br />
<span class="score" title="6 reputation points">6</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sshark has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Apr '16, 07:06</strong> </span></p></div></div><div id="comments-container-51349" class="comments-container"></div><div id="comment-tools-51349" class="comment-tools"></div><div class="clear"></div><div id="comment-51349-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

