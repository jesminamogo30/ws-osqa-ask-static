+++
type = "question"
title = "Wireshark freezing when restart/show options/List interfaces after stop the running live capture using PIPE file feed data"
description = '''I use &quot;wireshark -k -i pipeFileName&quot; to launch wireshark, My application feeds data to wireshark using pipe file. I stop the running live capture using &quot;Stop&quot; button, When I perform other operations, like &quot;restart/start live capture/show options/List interfaces&quot;, Wireshark always frozen. is it a bug...'''
date = "2011-01-05T23:49:00Z"
lastmod = "2011-01-05T23:49:00Z"
weight = 1645
keywords = [ "feed", "data", "freezing", "pipe", "file" ]
aliases = [ "/questions/1645" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark freezing when restart/show options/List interfaces after stop the running live capture using PIPE file feed data](/questions/1645/wireshark-freezing-when-restartshow-optionslist-interfaces-after-stop-the-running-live-capture-using-pipe-file-feed-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1645-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1645-score" class="post-score" title="current number of votes">0</div><span id="post-1645-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use "wireshark -k -i pipeFileName" to launch wireshark, My application feeds data to wireshark using pipe file. I stop the running live capture using "Stop" button, When I perform other operations, like "restart/start live capture/show options/List interfaces", Wireshark always frozen. is it a bug??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-feed" rel="tag" title="see questions tagged &#39;feed&#39;">feed</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-freezing" rel="tag" title="see questions tagged &#39;freezing&#39;">freezing</span> <span class="post-tag tag-link-pipe" rel="tag" title="see questions tagged &#39;pipe&#39;">pipe</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jan '11, 23:49</strong></p><img src="https://secure.gravatar.com/avatar/9f94dd6c84b70b9abb80a22546d09710?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="brenthuang&#39;s gravatar image" /><p><span>brenthuang</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="brenthuang has no accepted answers">0%</span></p></div></div><div id="comments-container-1645" class="comments-container"></div><div id="comment-tools-1645" class="comment-tools"></div><div class="clear"></div><div id="comment-1645-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

