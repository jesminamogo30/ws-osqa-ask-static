+++
type = "question"
title = "The capture session could not be initiated, please help!!"
description = '''Hi there, Thanks for reading this. I have a problem capturing packets with my Edimax EW-7711USn, i get this message “The capture session could not be initiated” when i try to capture. I think the edimax is not compatible with WireShark or Winpcap. I cannot find a compatibility NIC list anyware?  Bar...'''
date = "2010-11-30T06:45:00Z"
lastmod = "2011-03-28T12:58:00Z"
weight = 1178
keywords = [ "not", "be", "could", "session", "initiated" ]
aliases = [ "/questions/1178" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [The capture session could not be initiated, please help!!](/questions/1178/the-capture-session-could-not-be-initiated-please-help)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1178-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1178-score" class="post-score" title="current number of votes">0</div><span id="post-1178-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there, Thanks for reading this. I have a problem capturing packets with my Edimax EW-7711USn, i get this message “The capture session could not be initiated” when i try to capture. I think the edimax is not compatible with WireShark or Winpcap. I cannot find a compatibility NIC list anyware?</p><p>Bart, holland</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-be" rel="tag" title="see questions tagged &#39;be&#39;">be</span> <span class="post-tag tag-link-could" rel="tag" title="see questions tagged &#39;could&#39;">could</span> <span class="post-tag tag-link-session" rel="tag" title="see questions tagged &#39;session&#39;">session</span> <span class="post-tag tag-link-initiated" rel="tag" title="see questions tagged &#39;initiated&#39;">initiated</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Nov '10, 06:45</strong></p><img src="https://secure.gravatar.com/avatar/b69e7e707718453d6598d3467f70cbf5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bart&#39;s gravatar image" /><p><span>bart</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bart has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Nov '10, 07:22</strong> </span></p></div></div><div id="comments-container-1178" class="comments-container"><span id="3182"></span><div id="comment-3182" class="comment"><div id="post-3182-score" class="comment-score"></div><div class="comment-text"><p>Do you have sufficient privileges? What do you get if you run "tshark -D"? Which OS? Which version of Wireshark? Which version of WinPcap?</p></div><div id="comment-3182-info" class="comment-info"><span class="comment-age">(28 Mar '11, 12:58)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-1178" class="comment-tools"></div><div class="clear"></div><div id="comment-1178-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

