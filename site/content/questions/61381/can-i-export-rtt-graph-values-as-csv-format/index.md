+++
type = "question"
title = "Can I export RTT graph values as CSV format?"
description = '''  I know that I/O graph values can be saved as CSV file.  Can TCP stream graph such as RTT,Throughput values be exported as CSV format?   I&#x27;m trying to create a dissector in lua.   Can the values extracted from TCP stream be exploited by a dissector?   '''
date = "2017-05-13T04:24:00Z"
lastmod = "2017-05-13T04:24:00Z"
weight = 61381
keywords = [ "lua", "dissector", "export-to-csv" ]
aliases = [ "/questions/61381" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can I export RTT graph values as CSV format?](/questions/61381/can-i-export-rtt-graph-values-as-csv-format)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61381-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61381-score" class="post-score" title="current number of votes">0</div><span id="post-61381-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><ul><li><p>I know that I/O graph values can be saved as CSV file. Can TCP stream graph such as RTT,Throughput values be exported as CSV format?</p></li><li><p>I'm trying to create a dissector in lua. Can the values extracted from TCP stream be exploited by a dissector?</p></li></ul></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-export-to-csv" rel="tag" title="see questions tagged &#39;export-to-csv&#39;">export-to-csv</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '17, 04:24</strong></p><img src="https://secure.gravatar.com/avatar/3a702eaa9f4d90c81f74480545063c71?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ngn505&#39;s gravatar image" /><p><span>ngn505</span><br />
<span class="score" title="6 reputation points">6</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ngn505 has no accepted answers">0%</span></p></div></div><div id="comments-container-61381" class="comments-container"></div><div id="comment-tools-61381" class="comment-tools"></div><div class="clear"></div><div id="comment-61381-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

