+++
type = "question"
title = "Help with router information"
description = '''I am constantly hacked and abused on the internet. I&#x27;ve had to figure out a lot of things to get to what was going on and I recently used Wireshark to see if I can find the problem. I use an ethernet cable. There is a Linksys Cisco nat router on it. It is shared by three computers. I have used many ...'''
date = "2011-11-28T08:43:00Z"
lastmod = "2011-12-12T15:47:00Z"
weight = 7675
keywords = [ "router", "hackers" ]
aliases = [ "/questions/7675" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help with router information](/questions/7675/help-with-router-information)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7675-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7675-score" class="post-score" title="current number of votes">0</div><span id="post-7675-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am constantly hacked and abused on the internet. I've had to figure out a lot of things to get to what was going on and I recently used Wireshark to see if I can find the problem. I use an ethernet cable. There is a Linksys Cisco nat router on it. It is shared by three computers. I have used many different types of firewalls and when I block what appears to be the offender, I get no internet service. I did a Wireshark capture and found references to "CompalIn." We don't have any Compal equipment at the house. Can someone explain that to me? I can add more details from the file. Note that my computer is SO HACKED right now that almost all the services are run by netsvc when I am supposedly NOT connected to the internet and have disabled all the internet adapters. I also don't have access to the information in the taskmanager and other scary things. With the adapters uninstalled, Wireshark won't capture anything of course. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-hackers" rel="tag" title="see questions tagged &#39;hackers&#39;">hackers</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Nov '11, 08:43</strong></p><img src="https://secure.gravatar.com/avatar/d390ea629e0c22c5ba9c60f35126874d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Victim&#39;s gravatar image" /><p><span>Victim</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Victim has no accepted answers">0%</span></p></div></div><div id="comments-container-7675" class="comments-container"><span id="7904"></span><div id="comment-7904" class="comment"><div id="post-7904-score" class="comment-score"></div><div class="comment-text"><p>Hmmm... you did a capture, but Wireshark can't capture? Is this a joke? If this is my son writing this - get off the computer and get to homework! &lt;g&gt;</p></div><div id="comment-7904-info" class="comment-info"><span class="comment-age">(11 Dec '11, 20:58)</span> <span class="comment-user userinfo">lchappell ♦</span></div></div><span id="7919"></span><div id="comment-7919" class="comment"><div id="post-7919-score" class="comment-score"></div><div class="comment-text"><p>I'm confused by your response, but I am really mostly annoyed. I used the wireshark program when I was online after hacking number one billion. At that point, wireshark generated a file. The file is rife with odd information. I am asking a question about that. Unless your son is a highly educated woman, I doubt I am your son.</p></div><div id="comment-7919-info" class="comment-info"><span class="comment-age">(12 Dec '11, 12:18)</span> <span class="comment-user userinfo">Victim</span></div></div><span id="7932"></span><div id="comment-7932" class="comment"><div id="post-7932-score" class="comment-score"></div><div class="comment-text"><p>OK to make it short - you're f!"&amp;ed up. If your machine is half as infected as you describe above, there is absolutely NO use in running Wireshark for diagnostics which files on your PC are infected.</p><p>I strongly suggest that you boot your machine from whatever Linux LiveCD, save your important documents, whipe your hard drive and do a fresh reinstall of your OS.</p><p>If you have had another question, please be more precise in specifying WHAT exactly you want to know</p></div><div id="comment-7932-info" class="comment-info"><span class="comment-age">(12 Dec '11, 15:47)</span> <span class="comment-user userinfo">Landi</span></div></div></div><div id="comment-tools-7675" class="comment-tools"></div><div class="clear"></div><div id="comment-7675-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

