+++
type = "question"
title = "Decrypt WEP packets"
description = '''I&#x27;m trying to decode a WEP encrypted SSID. I took a trace with Airpcap and Wireshark 1.6.2 and try to decode the data with the correct key. My problem is, that not all packets are decoded. Is there any reason, why Wireshark cannot decode all packets?'''
date = "2011-09-27T05:30:00Z"
lastmod = "2011-09-27T05:30:00Z"
weight = 6587
keywords = [ "decryption", "wep", "decrypt" ]
aliases = [ "/questions/6587" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypt WEP packets](/questions/6587/decrypt-wep-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6587-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6587-score" class="post-score" title="current number of votes">0</div><span id="post-6587-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to decode a WEP encrypted SSID. I took a trace with Airpcap and Wireshark 1.6.2 and try to decode the data with the correct key. My problem is, that not all packets are decoded. Is there any reason, why Wireshark cannot decode all packets?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-wep" rel="tag" title="see questions tagged &#39;wep&#39;">wep</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Sep '11, 05:30</strong></p><img src="https://secure.gravatar.com/avatar/e5f17e21feaa02bf10f18d203d4b0369?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wmann&#39;s gravatar image" /><p><span>wmann</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wmann has no accepted answers">0%</span></p></div></div><div id="comments-container-6587" class="comments-container"></div><div id="comment-tools-6587" class="comment-tools"></div><div class="clear"></div><div id="comment-6587-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

