+++
type = "question"
title = "No capture while downloading with vbscript"
description = '''Hey, Wireshark shows nothing when I download a file via .vb (VBScript) using M$.XMLHTTP &amp;amp; ADODB.Stream Object. Source: https://gist.github.com/anonymous/9d0d215c8dfc0b5031111ce7ed578778 Using: Windows 10 Version 1607 (Build 14393.351) - Wireshark 2.2.1 (x64) greets'''
date = "2016-11-09T06:25:00Z"
lastmod = "2016-11-09T06:43:00Z"
weight = 57205
keywords = [ "vbscript", "xmlhttp", "stream", "adodb" ]
aliases = [ "/questions/57205" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No capture while downloading with vbscript](/questions/57205/no-capture-while-downloading-with-vbscript)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57205-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57205-score" class="post-score" title="current number of votes">0</div><span id="post-57205-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey,</p><p>Wireshark shows nothing when I download a file via .vb (VBScript) using M$.XMLHTTP &amp; ADODB.Stream Object.</p><p>Source: <a href="https://gist.github.com/anonymous/9d0d215c8dfc0b5031111ce7ed578778">https://gist.github.com/anonymous/9d0d215c8dfc0b5031111ce7ed578778</a></p><p>Using: Windows 10 Version 1607 (Build 14393.351) - Wireshark 2.2.1 (x64)</p><p>greets</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vbscript" rel="tag" title="see questions tagged &#39;vbscript&#39;">vbscript</span> <span class="post-tag tag-link-xmlhttp" rel="tag" title="see questions tagged &#39;xmlhttp&#39;">xmlhttp</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span> <span class="post-tag tag-link-adodb" rel="tag" title="see questions tagged &#39;adodb&#39;">adodb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Nov '16, 06:25</strong></p><img src="https://secure.gravatar.com/avatar/68c0382deb5dd4e916c9013cd3fd60a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jNizM&#39;s gravatar image" /><p><span>jNizM</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jNizM has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Nov '16, 06:29</strong> </span></p></div></div><div id="comments-container-57205" class="comments-container"><span id="57206"></span><div id="comment-57206" class="comment"><div id="post-57206-score" class="comment-score"></div><div class="comment-text"><p>Could be an issue in your capture setup, what URL were you using in the downloader code, i.e. was the download from the local machine or a remote machine?</p></div><div id="comment-57206-info" class="comment-info"><span class="comment-age">(09 Nov '16, 06:43)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-57205" class="comment-tools"></div><div class="clear"></div><div id="comment-57205-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

