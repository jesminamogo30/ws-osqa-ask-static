+++
type = "question"
title = "HELP - Laptop stolen and I want to &quot;search for it&quot; using Wireshark and wireless signal"
description = '''I had the misfortune of having my wife&#x27;s laptop stolen from my home.  The laptop was set to always use a particular SSID (&quot;c1&quot;) and also a particular Channel (I need to check but beleive it was set to &quot;11&quot;) I also know the laptop name is &quot;TOSHIBA-PC&quot;. Using Wireshark and my PCAP usb device, is it po...'''
date = "2012-03-05T14:21:00Z"
lastmod = "2012-03-06T17:03:00Z"
weight = 9369
keywords = [ "locate", "to", "laptop", "stolen", "wireshark" ]
aliases = [ "/questions/9369" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [HELP - Laptop stolen and I want to "search for it" using Wireshark and wireless signal](/questions/9369/help-laptop-stolen-and-i-want-to-search-for-it-using-wireshark-and-wireless-signal)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9369-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9369-score" class="post-score" title="current number of votes">0</div><span id="post-9369-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I had the misfortune of having my wife's laptop stolen from my home.<br />
</p><p>The laptop was set to always use a particular SSID ("c1") and also a particular Channel (I need to check but beleive it was set to "11")</p><p>I also know the laptop name is "TOSHIBA-PC".</p><p>Using Wireshark and my PCAP usb device, is it possible to search for a signal coming from the laptop?</p><p>Any help on settings is appreciated.</p><p>My thought was to drive around the neighborhood and search for the signal and see if it comes up.</p><p>Thanks in advance, Charles</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-locate" rel="tag" title="see questions tagged &#39;locate&#39;">locate</span> <span class="post-tag tag-link-to" rel="tag" title="see questions tagged &#39;to&#39;">to</span> <span class="post-tag tag-link-laptop" rel="tag" title="see questions tagged &#39;laptop&#39;">laptop</span> <span class="post-tag tag-link-stolen" rel="tag" title="see questions tagged &#39;stolen&#39;">stolen</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '12, 14:21</strong></p><img src="https://secure.gravatar.com/avatar/ed5a603bd793e25af2db69ea6e59f042?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="coecpa&#39;s gravatar image" /><p><span>coecpa</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="coecpa has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-9369" class="comments-container"><span id="9370"></span><div id="comment-9370" class="comment"><div id="post-9370-score" class="comment-score"></div><div class="comment-text"><p>Check out: "<a href="http://ask.wireshark.org/questions/3560/stolen-laptop">stolen laptop</a>"</p></div><div id="comment-9370-info" class="comment-info"><span class="comment-age">(05 Mar '12, 14:48)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-9369" class="comment-tools"></div><div class="clear"></div><div id="comment-9369-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9411"></span>

<div id="answer-container-9411" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9411-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9411-score" class="post-score" title="current number of votes">0</div><span id="post-9411-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can try to walk around with a linux laptop with a NIC in monitor mode running</p><pre><code>airodump-ng mon0</code></pre><p>you may pick up probes to SSID "c1" if you do not know the stolen device's MAC</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Mar '12, 17:03</strong></p><img src="https://secure.gravatar.com/avatar/c241cfce7680c690b68422163a98c0d2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="contradictor_&#39;s gravatar image" /><p><span>contradictor_</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="contradictor_ has no accepted answers">0%</span></p></div></div><div id="comments-container-9411" class="comments-container"></div><div id="comment-tools-9411" class="comment-tools"></div><div class="clear"></div><div id="comment-9411-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

