+++
type = "question"
title = "Can I use a Domain login with Remote Packet Capture?"
description = '''I have been experimenting with remote packet capture. I like the feature, however I have not been able to do it with a Domain account. Is there something I am missing or doing wrong? I followed the tutorial found here, and it does work fine when using a Local Administrator account. Other question is...'''
date = "2017-06-01T06:38:00Z"
lastmod = "2017-06-01T06:38:00Z"
weight = 61730
keywords = [ "rpcap" ]
aliases = [ "/questions/61730" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can I use a Domain login with Remote Packet Capture?](/questions/61730/can-i-use-a-domain-login-with-remote-packet-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61730-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61730-score" class="post-score" title="current number of votes">0</div><span id="post-61730-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have been experimenting with remote packet capture. I like the feature, however I have not been able to do it with a Domain account. Is there something I am missing or doing wrong? I followed the tutorial found here, and it does work fine when using a Local Administrator account.</p><p>Other question is does it have to be a Local Admin, or can other permissions / Local groups be applied to the user so that I do not have to use a Local Admin?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rpcap" rel="tag" title="see questions tagged &#39;rpcap&#39;">rpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Jun '17, 06:38</strong></p><img src="https://secure.gravatar.com/avatar/a4a1b2f45befcae1bf9964af228be737?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="solutionsville&#39;s gravatar image" /><p><span>solutionsville</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="solutionsville has no accepted answers">0%</span></p></div></div><div id="comments-container-61730" class="comments-container"></div><div id="comment-tools-61730" class="comment-tools"></div><div class="clear"></div><div id="comment-61730-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

