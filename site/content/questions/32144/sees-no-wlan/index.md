+++
type = "question"
title = "Sees no WLAN."
description = '''Hello, downloaded your program, but she does not see local network sees only local network 2. How to configure? Sorry for bad english, I&#x27;m just Russian'''
date = "2014-04-24T05:49:00Z"
lastmod = "2014-04-26T12:12:00Z"
weight = 32144
keywords = [ "wlan" ]
aliases = [ "/questions/32144" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Sees no WLAN.](/questions/32144/sees-no-wlan)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32144-score" class="post-score" title="current number of votes">0</div><span id="post-32144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, downloaded your program, but she does not see local network sees only local network 2. How to configure? Sorry for bad english, I'm just Russian</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '14, 05:49</strong></p><img src="https://secure.gravatar.com/avatar/f7cbc7d30329423911e1f02a455c12ff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="boyarchuk&#39;s gravatar image" /><p><span>boyarchuk</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="boyarchuk has no accepted answers">0%</span></p></div></div><div id="comments-container-32144" class="comments-container"><span id="32197"></span><div id="comment-32197" class="comment"><div id="post-32197-score" class="comment-score"></div><div class="comment-text"><p>some questions, as you forgot to add that information</p><ul><li>what is your OS and OS version</li><li>what is your wireshark version</li><li>what is the output of the following command: dumpcap -D -M</li></ul></div><div id="comment-32197-info" class="comment-info"><span class="comment-age">(26 Apr '14, 12:12)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-32144" class="comment-tools"></div><div class="clear"></div><div id="comment-32144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

