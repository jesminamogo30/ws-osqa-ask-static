+++
type = "question"
title = "What are career opportunities in wireshark ?"
description = '''Hi,   I am working in 3G wireshark development from the last one year.I want to know about the career opportunities in this , especially in India.Please guide :).'''
date = "2011-07-20T00:27:00Z"
lastmod = "2011-07-20T00:27:00Z"
weight = 5141
keywords = [ "career", "job", "india" ]
aliases = [ "/questions/5141" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What are career opportunities in wireshark ?](/questions/5141/what-are-career-opportunities-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5141-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5141-score" class="post-score" title="current number of votes">0</div><span id="post-5141-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am working in 3G wireshark development from the last one year.I want to know about the career opportunities in this , especially in India.Please guide :).</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-career" rel="tag" title="see questions tagged &#39;career&#39;">career</span> <span class="post-tag tag-link-job" rel="tag" title="see questions tagged &#39;job&#39;">job</span> <span class="post-tag tag-link-india" rel="tag" title="see questions tagged &#39;india&#39;">india</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jul '11, 00:27</strong></p><img src="https://secure.gravatar.com/avatar/044066e1986e9cbe936c9bf9b3f73827?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Shikha%20Chowdhary&#39;s gravatar image" /><p><span>Shikha Chowd...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Shikha Chowdhary has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>21 Jul '11, 03:37</strong> </span></p></div></div><div id="comments-container-5141" class="comments-container"></div><div id="comment-tools-5141" class="comment-tools"></div><div class="clear"></div><div id="comment-5141-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

