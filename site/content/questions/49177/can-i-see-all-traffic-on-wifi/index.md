+++
type = "question"
title = "Can i see all traffic on WIFi?"
description = '''Hello,  I have two laptops and one router. On first laptop i use Windows 7 64 bit with Wireshark and second laptop is Mac. I wanted to make some project for my school and i need to see all traffic on network. It&#x27;s open network without passwords (like in shops) so i wanted to write project on securit...'''
date = "2016-01-13T09:40:00Z"
lastmod = "2016-01-19T13:31:00Z"
weight = 49177
keywords = [ "http", "wifi", "packets", "help", "wireshark" ]
aliases = [ "/questions/49177" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Can i see all traffic on WIFi?](/questions/49177/can-i-see-all-traffic-on-wifi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49177-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49177-score" class="post-score" title="current number of votes">0</div><span id="post-49177-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have two laptops and one router. On first laptop i use Windows 7 64 bit with Wireshark and second laptop is Mac. I wanted to make some project for my school and i need to see all traffic on network. It's open network without passwords (like in shops) so i wanted to write project on security topic and how someone could see your activity on network. For that i need to see all activity on network. Sorry for my bad English and if you could help me i would be very grateful.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jan '16, 09:40</strong></p><img src="https://secure.gravatar.com/avatar/3fd77f3186dc7d7b6e1772fd8f2a48db?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="WiresharkHTTP&#39;s gravatar image" /><p><span>WiresharkHTTP</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="WiresharkHTTP has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Jan '16, 09:42</strong> </span></p></div></div><div id="comments-container-49177" class="comments-container"></div><div id="comment-tools-49177" class="comment-tools"></div><div class="clear"></div><div id="comment-49177-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49178"></span>

<div id="answer-container-49178" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49178-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49178-score" class="post-score" title="current number of votes">1</div><span id="post-49178-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="WiresharkHTTP has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the Wiki page on <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">Wireless capture setup</a> for details on how to capture. You might then also need to look at the <a href="https://wiki.wireshark.org/HowToDecrypt802.11">wireless decryption page</a> to inspect the traffic if it's encrypted.</p><p>Note that wireless traffic capture of other devices is problematic on Windows. You should be able to do that on the Mac though.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Jan '16, 09:45</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-49178" class="comments-container"><span id="49181"></span><div id="comment-49181" class="comment"><div id="post-49181-score" class="comment-score"></div><div class="comment-text"><p>Okey, i tried on Mac OS X but nothing happen. It couldn't run monitor mode and it show some EN10MB problem. I don't know what to do now.</p></div><div id="comment-49181-info" class="comment-info"><span class="comment-age">(13 Jan '16, 10:41)</span> <span class="comment-user userinfo">WiresharkHTTP</span></div></div><span id="49190"></span><div id="comment-49190" class="comment"><div id="post-49190-score" class="comment-score"></div><div class="comment-text"><p>Did you follow the appropriate <a href="https://wiki.wireshark.org/CaptureSetup/WLAN#Mac_OS_X">steps</a> for your version of OSX?</p><p>You'll need to be a lot more specific about the issues you see to allow folks to help.</p></div><div id="comment-49190-info" class="comment-info"><span class="comment-age">(13 Jan '16, 15:20)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="49191"></span><div id="comment-49191" class="comment"><div id="post-49191-score" class="comment-score"></div><div class="comment-text"><p>He's probably running into <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11364">bug 11364</a>.</p></div><div id="comment-49191-info" class="comment-info"><span class="comment-age">(13 Jan '16, 15:24)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="49286"></span><div id="comment-49286" class="comment"><div id="post-49286-score" class="comment-score"></div><div class="comment-text"><p>This is the error what i get when i run wireless connection capture with monitor mode in wireshark. <a href="http://imgur.com/NTgdXgL">http://imgur.com/NTgdXgL</a></p></div><div id="comment-49286-info" class="comment-info"><span class="comment-age">(17 Jan '16, 03:51)</span> <span class="comment-user userinfo">WiresharkHTTP</span></div></div><span id="49287"></span><div id="comment-49287" class="comment"><div id="post-49287-score" class="comment-score"></div><div class="comment-text"><p><span></span><span>@WiresharkHTTP</span>, if you'd read the comments to the <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11364">bug</a> <span></span><span>@Guy Harris</span> has referred to, you'd find out that to overcome the issue, you may either use the legacy GUI or follow <span>@Christian_R</span>'s answer to <a href="https://ask.wireshark.org/questions/42897/how-do-i-turn-on-monitor-mode-in-mac-os-x-with-wireshark-v199">this question about the same issue</a> when using the "new" GUI.</p><p>The step</p><blockquote><p>do not forget setting the Link Layer to Per Packet Info</p></blockquote><p>is the most important one.</p><p>Or you may wait until the functionality gets added to the Qt GUI, but something is telling me you'll rather choose one of the options above ;-)</p></div><div id="comment-49287-info" class="comment-info"><span class="comment-age">(17 Jan '16, 04:29)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="49296"></span><div id="comment-49296" class="comment not_top_scorer"><div id="post-49296-score" class="comment-score"></div><div class="comment-text"><blockquote><p>This is the error what i get when i run wireless connection capture with monitor mode in wireshark.</p></blockquote><p>That's <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11364">bug 11364</a>, as I thought.</p></div><div id="comment-49296-info" class="comment-info"><span class="comment-age">(17 Jan '16, 11:14)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="49390"></span><div id="comment-49390" class="comment not_top_scorer"><div id="post-49390-score" class="comment-score"></div><div class="comment-text"><p>Thank you guys. I did it well. I didn't see that step and i just passed to next. :)</p></div><div id="comment-49390-info" class="comment-info"><span class="comment-age">(19 Jan '16, 13:31)</span> <span class="comment-user userinfo">WiresharkHTTP</span></div></div></div><div id="comment-tools-49178" class="comment-tools"><span class="comments-showing"> showing 5 of 7 </span> <a href="#" class="show-all-comments-link">show 2 more comments</a></div><div class="clear"></div><div id="comment-49178-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

