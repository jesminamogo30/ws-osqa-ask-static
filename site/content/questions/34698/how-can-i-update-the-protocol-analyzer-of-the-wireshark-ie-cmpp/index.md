+++
type = "question"
title = "how can i update the protocol analyzer of the wireshark (ie. cmpp)"
description = '''I was capturing cmpp packages,while i found that wireshark support only cmpp1.0. Nowdays,cmcc offer cmpp2.0 interface for oter corp. to access its sms service. Cmpp2.0 is a little different from Cmpp1.0.So there is some mistake while i use wireshark analyse the packages. So there is the question: Ho...'''
date = "2014-07-15T19:50:00Z"
lastmod = "2014-07-15T23:03:00Z"
weight = 34698
keywords = [ "cmpp", "protocol" ]
aliases = [ "/questions/34698" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how can i update the protocol analyzer of the wireshark (ie. cmpp)](/questions/34698/how-can-i-update-the-protocol-analyzer-of-the-wireshark-ie-cmpp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34698-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34698-score" class="post-score" title="current number of votes">0</div><span id="post-34698-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was capturing cmpp packages,while i found that wireshark support only cmpp1.0. Nowdays,cmcc offer cmpp2.0 interface for oter corp. to access its sms service. Cmpp2.0 is a little different from Cmpp1.0.So there is some mistake while i use wireshark analyse the packages.</p><p>So there is the question: How can i update the cmpp protocol analyzer?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cmpp" rel="tag" title="see questions tagged &#39;cmpp&#39;">cmpp</span> <span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jul '14, 19:50</strong></p><img src="https://secure.gravatar.com/avatar/0493cb026db68080cf1ae4b669790fce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="happydanye&#39;s gravatar image" /><p><span>happydanye</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="happydanye has no accepted answers">0%</span></p></div></div><div id="comments-container-34698" class="comments-container"></div><div id="comment-tools-34698" class="comment-tools"></div><div class="clear"></div><div id="comment-34698-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34700"></span>

<div id="answer-container-34700" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34700-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34700-score" class="post-score" title="current number of votes">0</div><span id="post-34700-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Get the protocol specification of version 2 and update epan/dissectors/packet-copper.c accordingly then send us the patch through gerrit.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Jul '14, 23:03</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-34700" class="comments-container"></div><div id="comment-tools-34700" class="comment-tools"></div><div class="clear"></div><div id="comment-34700-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

