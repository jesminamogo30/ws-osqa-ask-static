+++
type = "question"
title = "why current versions of wireshark cannot decode SafetyNET P Protocol"
description = '''why current versions of wireshark cannot decode SafetyNET P Protocol'''
date = "2017-07-19T08:22:00Z"
lastmod = "2017-07-19T08:32:00Z"
weight = 62883
keywords = [ "safetynet" ]
aliases = [ "/questions/62883" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [why current versions of wireshark cannot decode SafetyNET P Protocol](/questions/62883/why-current-versions-of-wireshark-cannot-decode-safetynet-p-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62883-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62883-score" class="post-score" title="current number of votes">0</div><span id="post-62883-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>why current versions of wireshark cannot decode SafetyNET P Protocol</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-safetynet" rel="tag" title="see questions tagged &#39;safetynet&#39;">safetynet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jul '17, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/8d2e8a7020764f72b992ae79d118a146?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="IbrahimShibli&#39;s gravatar image" /><p><span>IbrahimShibli</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="IbrahimShibli has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jul '17, 08:36</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-62883" class="comments-container"><span id="62884"></span><div id="comment-62884" class="comment"><div id="post-62884-score" class="comment-score"></div><div class="comment-text"><p>Which version, and do you have a sample capture file that fails?</p><p>Can you share a capture in a publicly accessible spot, e.g. <a href="http://cloudshark.org">CloudShark</a>, Google Drive, DropBox etc?</p></div><div id="comment-62884-info" class="comment-info"><span class="comment-age">(19 Jul '17, 08:32)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-62883" class="comment-tools"></div><div class="clear"></div><div id="comment-62883-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

