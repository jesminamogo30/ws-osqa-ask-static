+++
type = "question"
title = "Packet capturing iPhone 5 from Mac"
description = '''I have connected iPhone 5 to mac and got remote virtual interface defined. In wireshark I have set the DLT_USER settings for user 2 (149) to &#x27;eth&#x27; and header size 108 (running on wifi). When capturing data however the info column says &quot;Unknown Frame (Bogus Fragment)&quot;. When trying on a different mac ...'''
date = "2014-07-09T11:49:00Z"
lastmod = "2014-07-09T11:49:00Z"
weight = 34521
keywords = [ "mac", "iphone", "rvictl", "wireshark" ]
aliases = [ "/questions/34521" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Packet capturing iPhone 5 from Mac](/questions/34521/packet-capturing-iphone-5-from-mac)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34521-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34521-score" class="post-score" title="current number of votes">0</div><span id="post-34521-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have connected iPhone 5 to mac and got remote virtual interface defined.<br />
In wireshark I have set the DLT_USER settings for user 2 (149) to 'eth' and header size 108 (running on wifi).</p><p>When capturing data however the info column says "<code>Unknown Frame (Bogus Fragment)</code>".</p><p>When trying on a different mac at a different location this setup works correctly.<br />
What am I missing?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-iphone" rel="tag" title="see questions tagged &#39;iphone&#39;">iphone</span> <span class="post-tag tag-link-rvictl" rel="tag" title="see questions tagged &#39;rvictl&#39;">rvictl</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jul '14, 11:49</strong></p><img src="https://secure.gravatar.com/avatar/1b4475736a03a08f221c2e2b635437b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LKLK&#39;s gravatar image" /><p><span>LKLK</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LKLK has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-34521" class="comments-container"></div><div id="comment-tools-34521" class="comment-tools"></div><div class="clear"></div><div id="comment-34521-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

