+++
type = "question"
title = "wireshark is not able to decode access network identifier option in pmipv6"
description = '''wireshark is not able to decode access network identifier option in pmipv6, is this ie not yet supported?'''
date = "2014-09-22T02:56:00Z"
lastmod = "2014-09-22T05:08:00Z"
weight = 36501
keywords = [ "pmipv6" ]
aliases = [ "/questions/36501" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark is not able to decode access network identifier option in pmipv6](/questions/36501/wireshark-is-not-able-to-decode-access-network-identifier-option-in-pmipv6)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36501-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36501-score" class="post-score" title="current number of votes">0</div><span id="post-36501-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>wireshark is not able to decode access network identifier option in pmipv6, is this ie not yet supported?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pmipv6" rel="tag" title="see questions tagged &#39;pmipv6&#39;">pmipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Sep '14, 02:56</strong></p><img src="https://secure.gravatar.com/avatar/f143afabfeaac289e5262e56ae7f7297?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vikranth%20Posa&#39;s gravatar image" /><p><span>Vikranth Posa</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vikranth Posa has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Sep '14, 12:23</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-36501" class="comments-container"></div><div id="comment-tools-36501" class="comment-tools"></div><div class="clear"></div><div id="comment-36501-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36504"></span>

<div id="answer-container-36504" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36504-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36504-score" class="post-score" title="current number of votes">0</div><span id="post-36504-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please open a bug report and attach a file with the offending packet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Sep '14, 03:54</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-36504" class="comments-container"><span id="36505"></span><div id="comment-36505" class="comment"><div id="post-36505-score" class="comment-score"></div><div class="comment-text"><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10492">Bug 10492</a> Submitted</p></div><div id="comment-36505-info" class="comment-info"><span class="comment-age">(22 Sep '14, 05:08)</span> <span class="comment-user userinfo">Vikranth Posa</span></div></div></div><div id="comment-tools-36504" class="comment-tools"></div><div class="clear"></div><div id="comment-36504-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

