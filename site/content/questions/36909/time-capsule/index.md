+++
type = "question"
title = "Time Capsule"
description = '''Sorry to bother you with this, but I can&#x27;t get answer anywhere.  I&#x27;m running Wireshark, a network diagnostic software, on my macbook pro (mid 2010). I capture packets from my own computer but not from my other devices. I&#x27;m told to put my computer in &quot;promiscuous&quot; mode, and out of monitor. I got to a...'''
date = "2014-10-07T20:01:00Z"
lastmod = "2014-10-08T09:32:00Z"
weight = 36909
keywords = [ "capsule", "time" ]
aliases = [ "/questions/36909" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Time Capsule](/questions/36909/time-capsule)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36909-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36909-score" class="post-score" title="current number of votes">0</div><span id="post-36909-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Sorry to bother you with this, but I can't get answer anywhere. I'm running Wireshark, a network diagnostic software, on my macbook pro (mid 2010). I capture packets from my own computer but not from my other devices. I'm told to put my computer in "promiscuous" mode, and out of monitor. I got to accessories/utilities/preferences/ network/edit Then I given choices on "radio" Automatic, radio g/a, etc. My question is, is this the "promuscuous" mode?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capsule" rel="tag" title="see questions tagged &#39;capsule&#39;">capsule</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '14, 20:01</strong></p><img src="https://secure.gravatar.com/avatar/351c40af27344bc7cce28dd4b06e162c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fentonnyc&#39;s gravatar image" /><p><span>fentonnyc</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fentonnyc has no accepted answers">0%</span></p></div></div><div id="comments-container-36909" class="comments-container"></div><div id="comment-tools-36909" class="comment-tools"></div><div class="clear"></div><div id="comment-36909-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36913"></span>

<div id="answer-container-36913" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36913-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36913-score" class="post-score" title="current number of votes">1</div><span id="post-36913-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the Wiki page on Wireless capture, in particular the sections for <a href="http://wiki.wireshark.org/CaptureSetup/WLAN#Mac_OS_X">Mac OSX</a>.</p><p>BTW, I can't work out what your question has to do with "Time Capsule".</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Oct '14, 02:39</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-36913" class="comments-container"><span id="36926"></span><div id="comment-36926" class="comment"><div id="post-36926-score" class="comment-score"></div><div class="comment-text"><p>Thanks. Obviously TC has nothing to do with my issue, if it supports "pro mode". But thank you for your help.</p></div><div id="comment-36926-info" class="comment-info"><span class="comment-age">(08 Oct '14, 08:49)</span> <span class="comment-user userinfo">fentonnyc</span></div></div><span id="36928"></span><div id="comment-36928" class="comment"><div id="post-36928-score" class="comment-score"></div><div class="comment-text"><p>Promiscuous mode is for the adaptor making the capture, i.e. your macbook, it allows the adaptor to see all traffic, whereas normally it will filter out traffic for other stations.</p></div><div id="comment-36928-info" class="comment-info"><span class="comment-age">(08 Oct '14, 09:17)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="36929"></span><div id="comment-36929" class="comment"><div id="post-36929-score" class="comment-score"></div><div class="comment-text"><p>Thanks again, I feel I'm getting closer. However, now my "root password" is required. I'm thinking it's my computer password, but it does not accept that command. Where does one find their correct "root password"?</p></div><div id="comment-36929-info" class="comment-info"><span class="comment-age">(08 Oct '14, 09:32)</span> <span class="comment-user userinfo">fentonnyc</span></div></div></div><div id="comment-tools-36913" class="comment-tools"></div><div class="clear"></div><div id="comment-36913-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

