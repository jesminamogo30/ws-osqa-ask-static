+++
type = "question"
title = "how to study to use Wireshark ?"
description = '''i&#x27;m interesting in network analysis.i start use Wireshark but i don&#x27;t understand how to use when i get packets. what kind of programming should i study to understand packets from wireshark ?'''
date = "2013-03-31T20:25:00Z"
lastmod = "2013-04-02T09:12:00Z"
weight = 19980
keywords = [ "help" ]
aliases = [ "/questions/19980" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [how to study to use Wireshark ?](/questions/19980/how-to-study-to-use-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19980-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19980-score" class="post-score" title="current number of votes">0</div><span id="post-19980-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i'm interesting in network analysis.i start use Wireshark but i don't understand how to use when i get packets. what kind of programming should i study to understand packets from wireshark ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Mar '13, 20:25</strong></p><img src="https://secure.gravatar.com/avatar/f845687db2ce0bfd0e0866d325bcee3e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="csycisco&#39;s gravatar image" /><p><span>csycisco</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="csycisco has no accepted answers">0%</span></p></div></div><div id="comments-container-19980" class="comments-container"></div><div id="comment-tools-19980" class="comment-tools"></div><div class="clear"></div><div id="comment-19980-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="19997"></span>

<div id="answer-container-19997" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19997-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19997-score" class="post-score" title="current number of votes">1</div><span id="post-19997-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You do not need to study <em>any</em> programming to understand packets. You need to learn about how networks and protocols work. There are tons of ressources out there, including books, but you can learn a lot from free materials as well, e.g.</p><p><a href="http://www.wireshark.org/docs/wsug_html_chunked/">The Wireshark Users Guide</a></p><p><a href="http://www.lovemytool.com/">http://www.lovemytool.com/</a></p><p>and many others.</p><p>Update: I just found the <a href="http://wiresharktraining.com/files/wiresharkjumpstartnotes_combs-chappell_march2013.pdf">Wireshark Jumpstart</a> notes by Laura and Gerald, maybe they help, too.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Apr '13, 15:52</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Apr '13, 16:20</strong> </span></p></div></div><div id="comments-container-19997" class="comments-container"></div><div id="comment-tools-19997" class="comment-tools"></div><div class="clear"></div><div id="comment-19997-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="19985"></span>

<div id="answer-container-19985" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19985-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19985-score" class="post-score" title="current number of votes">0</div><span id="post-19985-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://wiresharkbook.com/wireshark101.html">http://wiresharkbook.com/wireshark101.html</a></p><p>go through this book.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Apr '13, 01:39</strong></p><img src="https://secure.gravatar.com/avatar/6615a61d69b703d89076bb0f18342bbf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m_1607&#39;s gravatar image" /><p><span>m_1607</span><br />
<span class="score" title="35 reputation points">35</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="13 badges"><span class="silver">●</span><span class="badgecount">13</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m_1607 has no accepted answers">0%</span></p></div></div><div id="comments-container-19985" class="comments-container"></div><div id="comment-tools-19985" class="comment-tools"></div><div class="clear"></div><div id="comment-19985-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20019"></span>

<div id="answer-container-20019" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20019-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20019-score" class="post-score" title="current number of votes">0</div><span id="post-20019-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Besides the already given answers, please read the answers to these questions.</p><p><a href="http://ask.wireshark.org/questions/17083/want-to-learn-wireshark">http://ask.wireshark.org/questions/17083/want-to-learn-wireshark</a><br />
<a href="http://ask.wireshark.org/questions/13718/wireshark-packet-analysis-resources">http://ask.wireshark.org/questions/13718/wireshark-packet-analysis-resources</a><br />
<a href="http://ask.wireshark.org/questions/12534/understanding-wireshark">http://ask.wireshark.org/questions/12534/understanding-wireshark</a><br />
</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '13, 09:12</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-20019" class="comments-container"></div><div id="comment-tools-20019" class="comment-tools"></div><div class="clear"></div><div id="comment-20019-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

