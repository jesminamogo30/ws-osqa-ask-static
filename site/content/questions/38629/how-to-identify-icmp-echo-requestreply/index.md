+++
type = "question"
title = "How to Identify ICMP (Echo Request/Reply)"
description = '''Hello, how can Wireshark identify that IP package owns an ICMP-Echo replay and ICMP-Echo request? And also; how do recognizes Wireshark, that after the Ethernet frame is an IP header? Thanks, JapanIT'''
date = "2014-12-18T09:25:00Z"
lastmod = "2014-12-18T09:56:00Z"
weight = 38629
keywords = [ "reply", "icmp", "request", "identify" ]
aliases = [ "/questions/38629" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to Identify ICMP (Echo Request/Reply)](/questions/38629/how-to-identify-icmp-echo-requestreply)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38629-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38629-score" class="post-score" title="current number of votes">0</div><span id="post-38629-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, how can Wireshark identify that IP package owns an ICMP-Echo replay and ICMP-Echo request? And also; how do recognizes Wireshark, that after the Ethernet frame is an IP header?</p><p>Thanks, JapanIT</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reply" rel="tag" title="see questions tagged &#39;reply&#39;">reply</span> <span class="post-tag tag-link-icmp" rel="tag" title="see questions tagged &#39;icmp&#39;">icmp</span> <span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span> <span class="post-tag tag-link-identify" rel="tag" title="see questions tagged &#39;identify&#39;">identify</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Dec '14, 09:25</strong></p><img src="https://secure.gravatar.com/avatar/d55409a3c561df71c498d83f92e37921?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JapanIT&#39;s gravatar image" /><p><span>JapanIT</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JapanIT has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>18 Dec '14, 09:28</strong> </span></p></div></div><div id="comments-container-38629" class="comments-container"></div><div id="comment-tools-38629" class="comment-tools"></div><div class="clear"></div><div id="comment-38629-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38630"></span>

<div id="answer-container-38630" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38630-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38630-score" class="post-score" title="current number of votes">2</div><span id="post-38630-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Is this a study assignment?</p><p>Hint:</p><p>Ethernet headers have a type field and IPv4 headers have a Protocol field. Expand the packets in the packet details pane of Wireshark for the details.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Dec '14, 09:56</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-38630" class="comments-container"></div><div id="comment-tools-38630" class="comment-tools"></div><div class="clear"></div><div id="comment-38630-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

