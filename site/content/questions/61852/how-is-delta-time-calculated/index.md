+++
type = "question"
title = "How is delta time calculated"
description = '''what does Delta time calculate? I am using version 2.2.6. I have other filters which are self explanatory like &quot;Time since previous frame in this TCP stream&quot; and &quot;Time since first frame in this TCP stream&quot;. I have a capture where all these have different values and have some captures where &quot;Delta Ti...'''
date = "2017-06-07T18:53:00Z"
lastmod = "2017-06-07T18:53:00Z"
weight = 61852
keywords = [ "delta_time", "time" ]
aliases = [ "/questions/61852" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How is delta time calculated](/questions/61852/how-is-delta-time-calculated)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61852-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61852-score" class="post-score" title="current number of votes">0</div><span id="post-61852-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what does Delta time calculate? I am using version 2.2.6. I have other filters which are self explanatory like "Time since previous frame in this TCP stream" and "Time since first frame in this TCP stream". I have a capture where all these have different values and have some captures where "Delta Time" and "Time since previous frame in this TCP stream" are equal. So I am confused how Delta time is calculated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delta_time" rel="tag" title="see questions tagged &#39;delta_time&#39;">delta_time</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jun '17, 18:53</strong></p><img src="https://secure.gravatar.com/avatar/3528c086b78e7f53d1a577e5bb0301e3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Deon09&#39;s gravatar image" /><p><span>Deon09</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Deon09 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Jun '17, 16:10</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-61852" class="comments-container"></div><div id="comment-tools-61852" class="comment-tools"></div><div class="clear"></div><div id="comment-61852-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

