+++
type = "question"
title = "Capture 802.11 traffic on my Wired Network"
description = '''I&#x27;m trying to capture all or any 802.11 traffic on my wired network. I simply am trying to ensure if anyone attaches a wireless device to a PC, on my wired network, I can capture and display when it happened and if possible the host or MAC PC so I can track down the user via his or her PC. I tested ...'''
date = "2014-06-23T18:21:00Z"
lastmod = "2014-06-23T18:23:00Z"
weight = 34098
keywords = [ "capture", "802.11" ]
aliases = [ "/questions/34098" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture 802.11 traffic on my Wired Network](/questions/34098/capture-80211-traffic-on-my-wired-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34098-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34098-score" class="post-score" title="current number of votes">0</div><span id="post-34098-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to capture all or any 802.11 traffic on my wired network. I simply am trying to ensure if anyone attaches a wireless device to a PC, on my wired network, I can capture and display when it happened and if possible the host or MAC PC so I can track down the user via his or her PC. I tested a capture using wireshark and inserted a wireless USB Network Card on a PC. After installing the wireless card and enabled it, then connected to a guest wireless network but I have not seen any 820.11 packets on the small network while in promiscuous mode. The PC I'm using to capture data and the PC that had the wireless USB card inserted are two different PCs but on the same subnet.</p><p>Can anyone tell me how to setup a capture for my entire network when any 802.11 network activity occurs? I've read the various types of 802.11 communication types, example; 00 Management 1000 Beacon wlan.fc.type_subtype == 0x08</p><p>A million thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jun '14, 18:21</strong></p><img src="https://secure.gravatar.com/avatar/f6a65dbbe3ac395b71d4891d53d71664?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Johnny%20Mac&#39;s gravatar image" /><p><span>Johnny Mac</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Johnny Mac has no accepted answers">0%</span></p></div></div><div id="comments-container-34098" class="comments-container"></div><div id="comment-tools-34098" class="comment-tools"></div><div class="clear"></div><div id="comment-34098-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34099"></span>

<div id="answer-container-34099" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34099-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34099-score" class="post-score" title="current number of votes">0</div><span id="post-34099-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>When I say MAC above I mean MAC address not a MAC computer.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jun '14, 18:23</strong></p><img src="https://secure.gravatar.com/avatar/f6a65dbbe3ac395b71d4891d53d71664?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Johnny%20Mac&#39;s gravatar image" /><p><span>Johnny Mac</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Johnny Mac has no accepted answers">0%</span></p></div></div><div id="comments-container-34099" class="comments-container"></div><div id="comment-tools-34099" class="comment-tools"></div><div class="clear"></div><div id="comment-34099-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

