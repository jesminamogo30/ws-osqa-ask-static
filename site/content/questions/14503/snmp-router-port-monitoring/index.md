+++
type = "question"
title = "SNMP router port monitoring"
description = '''I have a network setup with about 15 Mac workstations connecting to a router with SNMP. Can wireshark monitor all web traiffic (souce and destination IP) going over router? I want to install Wireshark on a mac workstation with OS 10.4.11 .'''
date = "2012-09-25T03:50:00Z"
lastmod = "2012-09-25T06:12:00Z"
weight = 14503
keywords = [ "snmp" ]
aliases = [ "/questions/14503" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [SNMP router port monitoring](/questions/14503/snmp-router-port-monitoring)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14503-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14503-score" class="post-score" title="current number of votes">0</div><span id="post-14503-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a network setup with about 15 Mac workstations connecting to a router with SNMP. Can wireshark monitor all web traiffic (souce and destination IP) going over router? I want to install Wireshark on a mac workstation with OS 10.4.11 .</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-snmp" rel="tag" title="see questions tagged &#39;snmp&#39;">snmp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Sep '12, 03:50</strong></p><img src="https://secure.gravatar.com/avatar/5a339d9c970ed941c4d91324b3c65409?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="quinny&#39;s gravatar image" /><p><span>quinny</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="quinny has no accepted answers">0%</span></p></div></div><div id="comments-container-14503" class="comments-container"></div><div id="comment-tools-14503" class="comment-tools"></div><div class="clear"></div><div id="comment-14503-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14507"></span>

<div id="answer-container-14507" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14507-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14507-score" class="post-score" title="current number of votes">0</div><span id="post-14507-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't know what SNMP has to do with this, but have a look at <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">capture setup here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Sep '12, 06:12</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-14507" class="comments-container"></div><div id="comment-tools-14507" class="comment-tools"></div><div class="clear"></div><div id="comment-14507-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

