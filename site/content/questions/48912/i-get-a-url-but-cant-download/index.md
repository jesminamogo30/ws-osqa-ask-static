+++
type = "question"
title = "I Get A Url but can&#x27;t download"
description = '''With WirShark I have GET /live/hash/cnRtcDovL2RscnRtcC5jZG4uemhhbnFpLnR2L3pxbGl2ZS8yMDU1Nl84YTZLeQ==?nyt=1452094905832&amp;amp;pt=1&amp;amp;enc=base64&amp;amp;vid=1NBCIKZ0OISEHS404851 HTTP/1.1 Accept: / Host: live.a.302.yunfancdn.com Referer: http://www.zhanqi.tv/foye Range: bytes=541619916-541652251 Ck: 1 Crc:...'''
date = "2016-01-06T07:48:00Z"
lastmod = "2016-01-06T07:48:00Z"
weight = 48912
keywords = [ "download" ]
aliases = [ "/questions/48912" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [I Get A Url but can't download](/questions/48912/i-get-a-url-but-cant-download)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48912-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48912-score" class="post-score" title="current number of votes">0</div><span id="post-48912-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>With WirShark I have</p><p>GET /live/hash/cnRtcDovL2RscnRtcC5jZG4uemhhbnFpLnR2L3pxbGl2ZS8yMDU1Nl84YTZLeQ==?nyt=1452094905832&amp;pt=1&amp;enc=base64&amp;vid=1NBCIKZ0OISEHS404851 HTTP/1.1</p><p>Accept: <em>/</em></p><p>Host: live.a.302.yunfancdn.com</p><p>Referer: <a href="http://www.zhanqi.tv/foye">http://www.zhanqi.tv/foye</a></p><p>Range: bytes=541619916-541652251</p><p>Ck: 1</p><p>Crc: 1</p><p>User-Agent: yunfanbrowser20151210</p><p>Connection: Keep-Alive</p><p>How can I get the file??</p><p><img src="http://i.imgur.com/qs99g5J.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jan '16, 07:48</strong></p><img src="https://secure.gravatar.com/avatar/8bd861c83e523644a17b139a74dc23dc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yoyosnart&#39;s gravatar image" /><p><span>yoyosnart</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yoyosnart has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Jan '16, 07:50</strong> </span></p></div></div><div id="comments-container-48912" class="comments-container"></div><div id="comment-tools-48912" class="comment-tools"></div><div class="clear"></div><div id="comment-48912-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

