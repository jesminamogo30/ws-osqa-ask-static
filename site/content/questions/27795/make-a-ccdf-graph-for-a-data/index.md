+++
type = "question"
title = "[closed] Make a CCDF graph for a data"
description = '''I want to make a CCDF graph by reading data from the following text file(file.txt). CCDF means Complementary Cumulative Distribution Function I have tried to search about CCDF, but I do not really understand about it. So I don&#x27;t know how to plot a CCDF graph. Here is the data(file.txt), which is the...'''
date = "2013-12-04T20:52:00Z"
lastmod = "2013-12-04T20:52:00Z"
weight = 27795
keywords = [ "c#", "python", "java", "perl" ]
aliases = [ "/questions/27795" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Make a CCDF graph for a data](/questions/27795/make-a-ccdf-graph-for-a-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27795-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27795-score" class="post-score" title="current number of votes">0</div><span id="post-27795-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to make a CCDF graph by reading data from the following text file(file.txt).</p><p><strong>CCDF</strong> means <strong>Complementary Cumulative Distribution Function</strong></p><p>I have tried to search about CCDF, but I do not really understand about it.</p><p>So I don't know how to plot a CCDF graph.</p><p>Here is the data(file.txt), which is the inter-arrival times(seconds):</p><pre><code>2.824562000
7.914959000
15.838087000
1.013451000
2.813006000
0.424052000
0.146252000
0.166075000
2.298860000
6.393684000
5.341003000
0.005898000
0.009670000
0.453621000
0.068486000
0.039053000
0.022522000
0.080358000
0.086826000
0.033819000
0.040123000
0.288394000
0.345128000
0.388442000
0.142918000
1.624033000
3.097911000
0.098968000
0.027605000
0.057969000
0.264588000
0.137300000
0.105468000</code></pre><p><strong>How to plot this graph in Java by reading data from the above text file(file.txt)?</strong> Any other programming languages is also ok.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-c#" rel="tag" title="see questions tagged &#39;c#&#39;">c#</span> <span class="post-tag tag-link-python" rel="tag" title="see questions tagged &#39;python&#39;">python</span> <span class="post-tag tag-link-java" rel="tag" title="see questions tagged &#39;java&#39;">java</span> <span class="post-tag tag-link-perl" rel="tag" title="see questions tagged &#39;perl&#39;">perl</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '13, 20:52</strong></p><img src="https://secure.gravatar.com/avatar/e6ba864b6540c6d35e83f05d568d6979?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bella&#39;s gravatar image" /><p><span>bella</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bella has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>04 Dec '13, 23:00</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-27795" class="comments-container"></div><div id="comment-tools-27795" class="comment-tools"></div><div class="clear"></div><div id="comment-27795-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant for a Wireshark site" by Kurt Knochner 04 Dec '13, 23:00

</div>

</div>

</div>

