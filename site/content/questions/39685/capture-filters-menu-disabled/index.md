+++
type = "question"
title = "Capture Filters menu disabled"
description = '''I just installed version 1.99.2 on my Macbook Pro. I want to capture only traffic to/from a specific address, but the &quot;Capture Filters...&quot; selection under the Capture menu is disabled. Is this a problem with the development release I&#x27;m using, or is there possibly some other problem?'''
date = "2015-02-06T07:19:00Z"
lastmod = "2015-02-06T13:13:00Z"
weight = 39685
keywords = [ "filture", "capture" ]
aliases = [ "/questions/39685" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture Filters menu disabled](/questions/39685/capture-filters-menu-disabled)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39685-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39685-score" class="post-score" title="current number of votes">0</div><span id="post-39685-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just installed version 1.99.2 on my Macbook Pro. I want to capture only traffic to/from a specific address, but the "Capture Filters..." selection under the Capture menu is disabled. Is this a problem with the development release I'm using, or is there possibly some other problem?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filture" rel="tag" title="see questions tagged &#39;filture&#39;">filture</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '15, 07:19</strong></p><img src="https://secure.gravatar.com/avatar/a4b2c42b690de82d6f96316cf08754ef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TiggerGTO&#39;s gravatar image" /><p><span>TiggerGTO</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TiggerGTO has no accepted answers">0%</span></p></div></div><div id="comments-container-39685" class="comments-container"></div><div id="comment-tools-39685" class="comment-tools"></div><div class="clear"></div><div id="comment-39685-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39691"></span>

<div id="answer-container-39691" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39691-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39691-score" class="post-score" title="current number of votes">1</div><span id="post-39691-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is this a problem with the development release I'm using</p></blockquote><p>Yes. The development version builds for OS X are Qt-based (so that you don't need X11, and so that it fits a bit better on OS X in general), and the Qt version is still a work in progress, with not all features implemented yet. <a href="http://wiki.wireshark.org/Development/QtShark">The Wireshark Wiki page for the Qt version</a> indicates what's implemented and what's not implemented, with completed items crossed out; the "Capture" item in the list lists the Capture menu items, and the "Capture Filters" item isn't crossed out, indicating that it's not completed yet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Feb '15, 13:13</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-39691" class="comments-container"></div><div id="comment-tools-39691" class="comment-tools"></div><div class="clear"></div><div id="comment-39691-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

