+++
type = "question"
title = "Protocol Analyzer Study Topology using VMware"
description = '''I&#x27;m trying to create a topology to study, but I&#x27;m having difficults to listen your interfaces, actually I&#x27;m not find the interfaces. Would I have to use &quot;bridge interface&quot;? My OS: Windows 10 VmWare 1 - Kali VmWare 2 - Ubuntu'''
date = "2016-11-14T04:48:00Z"
lastmod = "2016-11-14T04:48:00Z"
weight = 57376
keywords = [ "networkinterfaces", "vmware", "wireshark" ]
aliases = [ "/questions/57376" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Protocol Analyzer Study Topology using VMware](/questions/57376/protocol-analyzer-study-topology-using-vmware)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57376-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57376-score" class="post-score" title="current number of votes">0</div><span id="post-57376-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to create a topology to study, but I'm having difficults to listen your interfaces, actually I'm not find the interfaces. Would I have to use "bridge interface"?</p><p>My OS: Windows 10 VmWare 1 - Kali VmWare 2 - Ubuntu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-networkinterfaces" rel="tag" title="see questions tagged &#39;networkinterfaces&#39;">networkinterfaces</span> <span class="post-tag tag-link-vmware" rel="tag" title="see questions tagged &#39;vmware&#39;">vmware</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '16, 04:48</strong></p><img src="https://secure.gravatar.com/avatar/fbfab12a00bede10ec8a392dddc34635?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ThiagoM&#39;s gravatar image" /><p><span>ThiagoM</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ThiagoM has no accepted answers">0%</span></p></div></div><div id="comments-container-57376" class="comments-container"></div><div id="comment-tools-57376" class="comment-tools"></div><div class="clear"></div><div id="comment-57376-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

