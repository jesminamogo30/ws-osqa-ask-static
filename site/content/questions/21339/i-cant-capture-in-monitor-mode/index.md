+++
type = "question"
title = "I can&#x27;t capture in monitor mode"
description = '''hi,  Even i had the same problem, when i gave iwconfig my wifi couldnt recognize any interface and when i select monitor mode in wireshark, it throws a error saying my device doesnt support this. What could be the problem?? Plz help..'''
date = "2013-05-20T22:10:00Z"
lastmod = "2013-05-21T11:18:00Z"
weight = 21339
keywords = [ "monitor-mode" ]
aliases = [ "/questions/21339" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [I can't capture in monitor mode](/questions/21339/i-cant-capture-in-monitor-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21339-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21339-score" class="post-score" title="current number of votes">0</div><span id="post-21339-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi,</p><p>Even i had the same problem, when i gave iwconfig my wifi couldnt recognize any interface and when i select monitor mode in wireshark, it throws a error saying my device doesnt support this. What could be the problem??</p><p>Plz help..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 May '13, 22:10</strong></p><img src="https://secure.gravatar.com/avatar/9ef98f3c4146a785f7ce7564b4f59bcd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="praveen_ind&#39;s gravatar image" /><p><span>praveen_ind</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="praveen_ind has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted <strong>21 May '13, 11:16</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-21339" class="comments-container"><span id="21354"></span><div id="comment-21354" class="comment"><div id="post-21354-score" class="comment-score"></div><div class="comment-text"><p>I assume from <code>iwconfig</code> that you are running on Linux. What version of what Linux distribution is this, and what version of the kernel does it have?</p><p>What is the list of interfaces on your machine, as shown by, for example, <code>ifconfig -a</code>?</p><p>Which interface is your wifi interface?</p><p>What was the <code>iwconfig</code> command you used, and what did it print when you tried to use it?</p></div><div id="comment-21354-info" class="comment-info"><span class="comment-age">(21 May '13, 11:18)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-21339" class="comment-tools"></div><div class="clear"></div><div id="comment-21339-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

