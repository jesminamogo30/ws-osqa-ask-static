+++
type = "question"
title = "Interface localization"
description = '''Hi , I just downloaded the new version; how is it possible to return to the english interface;?I don&#x27;t like the french one . Regards.'''
date = "2016-02-08T04:41:00Z"
lastmod = "2016-02-08T04:59:00Z"
weight = 49960
keywords = [ "interface", "ocalization" ]
aliases = [ "/questions/49960" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Interface localization](/questions/49960/interface-localization)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49960-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49960-score" class="post-score" title="current number of votes">0</div><span id="post-49960-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi , I just downloaded the new version; how is it possible to return to the english interface;?I don't like the french one . Regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-ocalization" rel="tag" title="see questions tagged &#39;ocalization&#39;">ocalization</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Feb '16, 04:41</strong></p><img src="https://secure.gravatar.com/avatar/39cf9ba229af536502739ea1f541378c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Chesnut&#39;s gravatar image" /><p><span>Chesnut</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Chesnut has no accepted answers">0%</span></p></div></div><div id="comments-container-49960" class="comments-container"></div><div id="comment-tools-49960" class="comment-tools"></div><div class="clear"></div><div id="comment-49960-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="49961"></span>

<div id="answer-container-49961" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49961-score" class="post-score" title="current number of votes">0</div><span id="post-49961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="https://ask.wireshark.org/questions/48823/change-the-gui-language">this older answer</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Feb '16, 04:59</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-49961" class="comments-container"></div><div id="comment-tools-49961" class="comment-tools"></div><div class="clear"></div><div id="comment-49961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

