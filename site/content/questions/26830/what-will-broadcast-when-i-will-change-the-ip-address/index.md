+++
type = "question"
title = "what will broadcast when i will change the ip address"
description = '''What will capture when i change the ip address.  I install wireshark and i change the ip address of that pc.  From otherside if i change the pc ip address how do i know that pc has change the ip address. Scenario-1: One pc that i have with wireshark. Ip address is 172.16.15.2 Wireshark is running. N...'''
date = "2013-11-10T21:13:00Z"
lastmod = "2013-11-13T21:33:00Z"
weight = 26830
keywords = [ "network", "wireshark" ]
aliases = [ "/questions/26830" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [what will broadcast when i will change the ip address](/questions/26830/what-will-broadcast-when-i-will-change-the-ip-address)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26830-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26830-score" class="post-score" title="current number of votes">0</div><span id="post-26830-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What will capture when i change the ip address.</p><p>I install wireshark and i change the ip address of that pc.</p><p>From otherside if i change the pc ip address how do i know that pc has change the ip address.</p><p>Scenario-1: One pc that i have with wireshark. Ip address is 172.16.15.2 Wireshark is running. Now i change the ip address 172.16.15.4 which is not in the network.</p><p>Now what will capture wireshark?</p><p>Scenario-2: Now what happen if 172.16.15.4 will in the network?? Does it capture the data with same ip but different mac?</p><p>Scenario-3: What will first happen when the ip address change from both end PC.</p><p>snp</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '13, 21:13</strong></p><img src="https://secure.gravatar.com/avatar/f29d818cceb159a2e684d023882759ba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="snpatel&#39;s gravatar image" /><p><span>snpatel</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="snpatel has no accepted answers">0%</span></p></div></div><div id="comments-container-26830" class="comments-container"></div><div id="comment-tools-26830" class="comment-tools"></div><div class="clear"></div><div id="comment-26830-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26840"></span>

<div id="answer-container-26840" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26840-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26840-score" class="post-score" title="current number of votes">1</div><span id="post-26840-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>sounds a bit like a homework assignment...</p><p>We know nothing about your PC</p><ul><li>what type of OS is it running</li><li>what kind of security software (firewalls, endpoint security, etc.) is installed</li><li>type of interface (ethernet, wifi)</li><li>network configuration of your OS</li></ul><p>As a result, nobody here will be able to answer the questions as they were posted.</p><p><strong>Solution:</strong> Why don't you start Wireshark on your system and watch what is being sent/broadcasted while you change the IP address !?! Simple problem, simple solution ;-))</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Nov '13, 07:16</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Nov '13, 07:18</strong> </span></p></div></div><div id="comments-container-26840" class="comments-container"><span id="26978"></span><div id="comment-26978" class="comment"><div id="post-26978-score" class="comment-score"></div><div class="comment-text"><p>IT not a tutorial or assignment. But i am trying to find out that what is going to happen? I am learning wire shark so it is totally new for me. I install it and<br />
simply capture the packet but it is very difficult that just in few second i get lots of data. But i found capture the packet from destination ip address and ip address to type a ip.dst==ipaddress and ipaddr==ipaddress.</p><p>Now i just want to know that how can i perform the above task for my knowledge? I hope you will help me to find it out.</p><p>Thanks.</p></div><div id="comment-26978-info" class="comment-info"><span class="comment-age">(13 Nov '13, 20:56)</span> <span class="comment-user userinfo">snpatel</span></div></div><span id="26979"></span><div id="comment-26979" class="comment"><div id="post-26979-score" class="comment-score"></div><div class="comment-text"><p>There are all sorts of filters you can run on the data apart from the ip addresses, have a look at following <a href="http://wiki.wireshark.org/DisplayFilters">http://wiki.wireshark.org/DisplayFilters</a></p></div><div id="comment-26979-info" class="comment-info"><span class="comment-age">(13 Nov '13, 21:33)</span> <span class="comment-user userinfo">Ken15</span></div></div></div><div id="comment-tools-26840" class="comment-tools"></div><div class="clear"></div><div id="comment-26840-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

