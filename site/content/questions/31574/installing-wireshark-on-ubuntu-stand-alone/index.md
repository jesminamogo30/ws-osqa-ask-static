+++
type = "question"
title = "installing wireshark on Ubuntu stand alone"
description = '''I&#x27;m having trouble installing Wireshark on a stand alone Ubunto system. Unfortunately there is no possibility for me to connect this box to the internet, however I&#x27;m required to install Wireshark on this system. I&#x27;ve tried numerous methods however nothing seems to work and I&#x27;m at a complete loss. I&#x27;...'''
date = "2014-04-06T13:35:00Z"
lastmod = "2014-04-06T14:47:00Z"
weight = 31574
keywords = [ "installation" ]
aliases = [ "/questions/31574" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [installing wireshark on Ubuntu stand alone](/questions/31574/installing-wireshark-on-ubuntu-stand-alone)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31574-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31574-score" class="post-score" title="current number of votes">0</div><span id="post-31574-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm having trouble installing Wireshark on a stand alone Ubunto system. Unfortunately there is no possibility for me to connect this box to the internet, however I'm required to install Wireshark on this system. I've tried numerous methods however nothing seems to work and I'm at a complete loss. I'm running Ubuntu12.04.2. 32bit, and I can't connect to the internet but I am able to transfer the necessary files via usb or disk. If anyone has any ideas or knows the solution to this problem, it would be much appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '14, 13:35</strong></p><img src="https://secure.gravatar.com/avatar/7a9de2f5ca718a90dcea21e021dc7f3f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="The1Voices&#39;s gravatar image" /><p><span>The1Voices</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="The1Voices has no accepted answers">0%</span></p></div></div><div id="comments-container-31574" class="comments-container"></div><div id="comment-tools-31574" class="comment-tools"></div><div class="clear"></div><div id="comment-31574-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31575"></span>

<div id="answer-container-31575" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31575-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31575-score" class="post-score" title="current number of votes">0</div><span id="post-31575-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>see my answer to the same question, some time ago.</p><blockquote><p><a href="http://ask.wireshark.org/questions/18742/wireshark-for-ubuntu-linux-on-a-stand-alone-machine?page=1&amp;focusedAnswerId=18752#18752">http://ask.wireshark.org/questions/18742/wireshark-for-ubuntu-linux-on-a-stand-alone-machine?page=1&amp;focusedAnswerId=18752#18752</a></p></blockquote><p>Regards<br />
Kurtt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Apr '14, 14:47</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-31575" class="comments-container"></div><div id="comment-tools-31575" class="comment-tools"></div><div class="clear"></div><div id="comment-31575-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

