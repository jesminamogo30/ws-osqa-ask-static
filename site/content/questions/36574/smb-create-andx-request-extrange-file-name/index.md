+++
type = "question"
title = "smb create andx request extrange file name"
description = '''i have issues in my network, programs using mdb files are getting corrupt. examining with wireshark i see a multiple entrys like this: NT Create AndX Request; Path: &#92;my_clientA&#92;2013:&#92;005PebiesnrMkudrfcoIaamtykdDa:$DATA NT Create AndX Response; FID: 0x0000; Error: STATUS_OBJECT_NAME_NOT_FOUND NT Crea...'''
date = "2014-09-24T16:08:00Z"
lastmod = "2014-09-24T16:08:00Z"
weight = 36574
keywords = [ "create", "request", "smb", "andx" ]
aliases = [ "/questions/36574" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [smb create andx request extrange file name](/questions/36574/smb-create-andx-request-extrange-file-name)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36574-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36574-score" class="post-score" title="current number of votes">0</div><span id="post-36574-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i have issues in my network, programs using mdb files are getting corrupt. examining with wireshark i see a multiple entrys like this:</p><p>NT Create AndX Request; Path: \my_clientA\2013:\005PebiesnrMkudrfcoIaamtykdDa:$DATA</p><p>NT Create AndX Response; FID: 0x0000; Error: STATUS_OBJECT_NAME_NOT_FOUND</p><p>NT Create AndX Request; Path: \myclientB\:Docf_\005SummaryInformation:$DATA</p><p>NT Create AndX Response; FID: 0x0000; Error: STATUS_OBJECT_NAME_NOT_FOUND</p><p>NT Create AndX Request; Path: \myclientC\2013\:\005QebiesnrMkudrfcoIaamtykdDa:$DATA</p><p>NT Create AndX Response; FID: 0x0000; Error: STATUS_OBJECT_NAME_NOT_FOUND</p><p>The entry with 005sumaryinformation sounds good but the other two entrys call my attention. Can someone help me to understand this logs ¿?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-create" rel="tag" title="see questions tagged &#39;create&#39;">create</span> <span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span> <span class="post-tag tag-link-smb" rel="tag" title="see questions tagged &#39;smb&#39;">smb</span> <span class="post-tag tag-link-andx" rel="tag" title="see questions tagged &#39;andx&#39;">andx</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '14, 16:08</strong></p><img src="https://secure.gravatar.com/avatar/42a2e825e0b64434600add24dba5eac5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="teki&#39;s gravatar image" /><p><span>teki</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="teki has no accepted answers">0%</span></p></div></div><div id="comments-container-36574" class="comments-container"></div><div id="comment-tools-36574" class="comment-tools"></div><div class="clear"></div><div id="comment-36574-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

