+++
type = "question"
title = "Wireshark 1.6.2 Crashes in SMI.DLL"
description = '''I loaded the latest WireShark (after making sure the last version was completely erased), and tried to run it.  I got the error signature;   AppName: wireshark.exe AppVer: 1.6.2.38931 ModName: smi.dll  ModVer: 0.0.0.0 Offset: 0000265b I have used Wireshark for a while, with earlier versions, with no...'''
date = "2011-09-20T16:36:00Z"
lastmod = "2011-09-21T02:50:00Z"
weight = 6470
keywords = [ "smi.dll", "crash", "smi" ]
aliases = [ "/questions/6470" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 1.6.2 Crashes in SMI.DLL](/questions/6470/wireshark-162-crashes-in-smidll)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6470-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6470-score" class="post-score" title="current number of votes">0</div><span id="post-6470-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I loaded the latest WireShark (after making sure the last version was completely erased), and tried to run it. I got the error signature; AppName: wireshark.exe AppVer: 1.6.2.38931 ModName: smi.dll ModVer: 0.0.0.0 Offset: 0000265b</p><p>I have used Wireshark for a while, with earlier versions, with no problem. I even tried to load an earlier version, but get the same error.</p><p>Anyone with any ideas? C</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-smi.dll" rel="tag" title="see questions tagged &#39;smi.dll&#39;">smi.dll</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span> <span class="post-tag tag-link-smi" rel="tag" title="see questions tagged &#39;smi&#39;">smi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Sep '11, 16:36</strong></p><img src="https://secure.gravatar.com/avatar/4262a630ed6d024f8d68a542ba9f5583?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cbost49&#39;s gravatar image" /><p><span>cbost49</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cbost49 has no accepted answers">0%</span></p></div></div><div id="comments-container-6470" class="comments-container"><span id="6474"></span><div id="comment-6474" class="comment"><div id="post-6474-score" class="comment-score"></div><div class="comment-text"><p>What's the OS you install on?</p><p>Where did you download the installer?</p><p>What was the previous version of Wireshark installed?</p></div><div id="comment-6474-info" class="comment-info"><span class="comment-age">(21 Sep '11, 02:50)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-6470" class="comment-tools"></div><div class="clear"></div><div id="comment-6470-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

