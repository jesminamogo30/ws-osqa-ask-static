+++
type = "question"
title = "tshark reporting packet loss"
description = '''Greetings All; I run tshark to capture date for 45 seconds. At the end of 45 seconds tshark stops and reports: 6504  79 packets dropped I assume that 6504 is the number of packets captured. I am confused as to the 79 packets dropped. Are these packets that are somehow dropped by wireshark (maybe for...'''
date = "2012-03-28T13:32:00Z"
lastmod = "2012-03-30T16:28:00Z"
weight = 9827
keywords = [ "vbox", "tshark", "dropped" ]
aliases = [ "/questions/9827" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [tshark reporting packet loss](/questions/9827/tshark-reporting-packet-loss)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9827-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9827-score" class="post-score" title="current number of votes">0</div><span id="post-9827-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Greetings All;</p><p>I run tshark to capture date for 45 seconds. At the end of 45 seconds tshark stops and reports:</p><p>6504 79 packets dropped</p><p>I assume that 6504 is the number of packets captured.</p><p>I am confused as to the 79 packets dropped. Are these packets that are somehow dropped by wireshark (maybe for memory reasons ?), or are these packets it believes are somehow dropped in the network.</p><p>I am running virtual box guest on a win 7 host, and running wireshark within the guest. The guest is in "bridged mode" so it has its own network ip. Is there anything I need to be real careful about when doing this ??</p><p>thanks as always; Walter</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vbox" rel="tag" title="see questions tagged &#39;vbox&#39;">vbox</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-dropped" rel="tag" title="see questions tagged &#39;dropped&#39;">dropped</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Mar '12, 13:32</strong></p><img src="https://secure.gravatar.com/avatar/2b12f1f0687101a1dd8f75db884aed8e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wakelt&#39;s gravatar image" /><p><span>wakelt</span><br />
<span class="score" title="13 reputation points">13</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wakelt has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Mar '12, 16:29</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-9827" class="comments-container"></div><div id="comment-tools-9827" class="comment-tools"></div><div class="clear"></div><div id="comment-9827-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9874"></span>

<div id="answer-container-9874" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9874-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9874-score" class="post-score" title="current number of votes">0</div><span id="post-9874-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="wakelt has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I believe the accepted answer to question 2095, <a href="http://ask.wireshark.org/questions/2095/what-does-packets-dropped-really-mean"><em>What does "packets dropped" really mean?</em></a> should help you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Mar '12, 16:28</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-9874" class="comments-container"></div><div id="comment-tools-9874" class="comment-tools"></div><div class="clear"></div><div id="comment-9874-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

