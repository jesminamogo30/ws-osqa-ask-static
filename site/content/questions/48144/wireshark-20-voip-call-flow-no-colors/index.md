+++
type = "question"
title = "Wireshark 2.0 VOIP call flow - no colors?"
description = '''I have grown accustomed to the very useful colorized VOIP call flows from Wireshark 1.x. Just tried 2.0 and the call flow has no IP addresses at the tops of the columns and there are no colors identifying different calls. Have these features been removed or are there any options for re-enabling? Tha...'''
date = "2015-12-01T08:41:00Z"
lastmod = "2015-12-01T18:15:00Z"
weight = 48144
keywords = [ "flow", "call", "voip" ]
aliases = [ "/questions/48144" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 2.0 VOIP call flow - no colors?](/questions/48144/wireshark-20-voip-call-flow-no-colors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48144-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48144-score" class="post-score" title="current number of votes">0</div><span id="post-48144-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have grown accustomed to the very useful colorized VOIP call flows from Wireshark 1.x. Just tried 2.0 and the call flow has no IP addresses at the tops of the columns and there are no colors identifying different calls. Have these features been removed or are there any options for re-enabling?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span> <span class="post-tag tag-link-call" rel="tag" title="see questions tagged &#39;call&#39;">call</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Dec '15, 08:41</strong></p><img src="https://secure.gravatar.com/avatar/9cd703b5b9d03185be7eb78158af312b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wmclend&#39;s gravatar image" /><p><span>wmclend</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wmclend has no accepted answers">0%</span></p></div></div><div id="comments-container-48144" class="comments-container"></div><div id="comment-tools-48144" class="comment-tools"></div><div class="clear"></div><div id="comment-48144-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48145"></span>

<div id="answer-container-48145" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48145-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48145-score" class="post-score" title="current number of votes">0</div><span id="post-48145-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Those are features not ported to the new Qt UI yet. Bug <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11710">11710</a> is already tracking some of them.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Dec '15, 08:49</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-48145" class="comments-container"><span id="48171"></span><div id="comment-48171" class="comment"><div id="post-48171-score" class="comment-score"></div><div class="comment-text"><p>But it doesn't seem to be tracking this one; please file another bug on this one.</p></div><div id="comment-48171-info" class="comment-info"><span class="comment-age">(01 Dec '15, 18:15)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-48145" class="comment-tools"></div><div class="clear"></div><div id="comment-48145-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

