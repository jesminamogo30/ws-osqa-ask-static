+++
type = "question"
title = "Wireshark 2.0.4 Crashes on Windows 10 in Live Capture Analysis using remote capture"
description = '''Hi I am using Wireshark 2.0.4 to capture packets in Linux Env using remote capture. But the Wireshark crashes after 3+ hours and couldnot able to save the PCAP file. Please find the attachements asa below   Request your help. Regards Dinesh Sadu'''
date = "2016-06-14T07:09:00Z"
lastmod = "2016-11-04T08:44:00Z"
weight = 53437
keywords = [ "windows", "crash", "wireshark" ]
aliases = [ "/questions/53437" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 2.0.4 Crashes on Windows 10 in Live Capture Analysis using remote capture](/questions/53437/wireshark-204-crashes-on-windows-10-in-live-capture-analysis-using-remote-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53437-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53437-score" class="post-score" title="current number of votes">0</div><span id="post-53437-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am using Wireshark 2.0.4 to capture packets in Linux Env using remote capture. But the Wireshark crashes after 3+ hours and couldnot able to save the PCAP file.</p><p>Please find the attachements asa below</p><p><img src="https://osqa-ask.wireshark.org/upfiles/AASKUntitled.jpg" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/ASKCapture.JPG" alt="alt text" /></p><p>Request your help.</p><p>Regards Dinesh Sadu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jun '16, 07:09</strong></p><img src="https://secure.gravatar.com/avatar/04334c27cb629065a13d61a61b611038?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dinesh%20Babu%20Sadu&#39;s gravatar image" /><p><span>Dinesh Babu ...</span><br />
<span class="score" title="16 reputation points">16</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dinesh Babu Sadu has no accepted answers">0%</span></p></img></div></div><div id="comments-container-53437" class="comments-container"></div><div id="comment-tools-53437" class="comment-tools"></div><div class="clear"></div><div id="comment-53437-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53439"></span>

<div id="answer-container-53439" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53439-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53439-score" class="post-score" title="current number of votes">0</div><span id="post-53439-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12501">bug 12501</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jun '16, 07:29</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></img></div></div><div id="comments-container-53439" class="comments-container"><span id="56986"></span><div id="comment-56986" class="comment"><div id="post-56986-score" class="comment-score"></div><div class="comment-text"><p>Any update on the fix for upcoming Wireshark releases</p></div><div id="comment-56986-info" class="comment-info"><span class="comment-age">(04 Nov '16, 08:36)</span> <span class="comment-user userinfo">Dinesh Babu ...</span></div></div><span id="56987"></span><div id="comment-56987" class="comment"><div id="post-56987-score" class="comment-score"></div><div class="comment-text"><p>Please have a look at <a href="https://blog.packet-foo.com/2013/05/the-notorious-wireshark-out-of-memory-problem/">https://blog.packet-foo.com/2013/05/the-notorious-wireshark-out-of-memory-problem/</a> and <a href="https://blog.wireshark.org/2014/07/to-infinity-and-beyond-capturing-forever-with-tshark/">https://blog.wireshark.org/2014/07/to-infinity-and-beyond-capturing-forever-with-tshark/</a></p></div><div id="comment-56987-info" class="comment-info"><span class="comment-age">(04 Nov '16, 08:44)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-53439" class="comment-tools"></div><div class="clear"></div><div id="comment-53439-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

