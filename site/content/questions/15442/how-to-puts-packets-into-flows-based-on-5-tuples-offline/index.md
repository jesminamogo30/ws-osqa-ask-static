+++
type = "question"
title = "How to puts packets into flows based on 5-tuples offline"
description = '''Hi I have some pcap files I need to puts these files in flows base instead of packets base by using 5-tuple (Src port, dst port,src ip, dst ip, protocol). is there any C code can do that, and calculate the statistical features such as mean interarrival time for the flow  great thank'''
date = "2012-10-31T20:17:00Z"
lastmod = "2012-10-31T20:17:00Z"
weight = 15442
keywords = [ "statistics" ]
aliases = [ "/questions/15442" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to puts packets into flows based on 5-tuples offline](/questions/15442/how-to-puts-packets-into-flows-based-on-5-tuples-offline)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15442-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15442-score" class="post-score" title="current number of votes">0</div><span id="post-15442-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I have some pcap files I need to puts these files in flows base instead of packets base by using 5-tuple (Src port, dst port,src ip, dst ip, protocol). is there any C code can do that, and calculate the statistical features such as mean interarrival time for the flow great thank</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Oct '12, 20:17</strong></p><img src="https://secure.gravatar.com/avatar/27a37dd0db96baea7c1384b21b668120?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hy2012&#39;s gravatar image" /><p><span>hy2012</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hy2012 has no accepted answers">0%</span></p></div></div><div id="comments-container-15442" class="comments-container"></div><div id="comment-tools-15442" class="comment-tools"></div><div class="clear"></div><div id="comment-15442-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

