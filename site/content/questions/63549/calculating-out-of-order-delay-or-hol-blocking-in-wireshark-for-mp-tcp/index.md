+++
type = "question"
title = "Calculating Out-Of-Order Delay or HOL blocking in Wireshark for MP-TCP"
description = '''I am looking into calculating the out-of-order delay for MP-TCP packets in wireshark. It seems that MP-TCP dissector and analyzer in wireshark does not provide such kind of an analysis. So my question, what is the easiest way to write a small script in wireshark to analyze a capture file and provide...'''
date = "2017-09-01T12:07:00Z"
lastmod = "2017-09-01T12:07:00Z"
weight = 63549
keywords = [ "dissector", "mptcp", "analysis" ]
aliases = [ "/questions/63549" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Calculating Out-Of-Order Delay or HOL blocking in Wireshark for MP-TCP](/questions/63549/calculating-out-of-order-delay-or-hol-blocking-in-wireshark-for-mp-tcp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63549-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63549-score" class="post-score" title="current number of votes">0</div><span id="post-63549-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking into calculating the out-of-order delay for MP-TCP packets in wireshark. It seems that MP-TCP dissector and analyzer in wireshark does not provide such kind of an analysis. So my question, what is the easiest way to write a small script in wireshark to analyze a capture file and provide me with this summary?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span> <span class="post-tag tag-link-mptcp" rel="tag" title="see questions tagged &#39;mptcp&#39;">mptcp</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Sep '17, 12:07</strong></p><img src="https://secure.gravatar.com/avatar/566cfe38b17a31f0dc825c86538cf3d4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Hany%20Assasa&#39;s gravatar image" /><p><span>Hany Assasa</span><br />
<span class="score" title="21 reputation points">21</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Hany Assasa has no accepted answers">0%</span></p></div></div><div id="comments-container-63549" class="comments-container"></div><div id="comment-tools-63549" class="comment-tools"></div><div class="clear"></div><div id="comment-63549-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

