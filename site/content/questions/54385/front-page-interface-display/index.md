+++
type = "question"
title = "Front page interface display"
description = '''Hi, When I first open the application, I can see the list of interfaces and a moving graph showing traffic going through it. If I double click on one of them to see the packets, it works without any issues but If I stop the capture and close it without saving, it goes back to the first page but I no...'''
date = "2016-07-27T16:33:00Z"
lastmod = "2016-07-27T17:25:00Z"
weight = 54385
keywords = [ "interface", "frontpage" ]
aliases = [ "/questions/54385" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Front page interface display](/questions/54385/front-page-interface-display)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54385-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54385-score" class="post-score" title="current number of votes">0</div><span id="post-54385-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>When I first open the application, I can see the list of interfaces and a moving graph showing traffic going through it. If I double click on one of them to see the packets, it works without any issues but If I stop the capture and close it without saving, it goes back to the first page but I noticed that the interfaces no longer show the moving graph. I tried to press F5 and refresh the interfaces but it still did not work. I am using version 2.0.5 (v2.0.5-0-ga3be9c6 from master-2.0)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-frontpage" rel="tag" title="see questions tagged &#39;frontpage&#39;">frontpage</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jul '16, 16:33</strong></p><img src="https://secure.gravatar.com/avatar/32818cd5b7bcc4ce235033502f9fbd85?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="chris26344&#39;s gravatar image" /><p><span>chris26344</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="chris26344 has no accepted answers">0%</span></p></div></div><div id="comments-container-54385" class="comments-container"></div><div id="comment-tools-54385" class="comment-tools"></div><div class="clear"></div><div id="comment-54385-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54387"></span>

<div id="answer-container-54387" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54387-score" class="post-score" title="current number of votes">1</div><span id="post-54387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That sounds like a bug. Please file a bug on <a href="http://bugs.wireshark.org">the Wireshark Bugzilla</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jul '16, 17:25</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-54387" class="comments-container"></div><div id="comment-tools-54387" class="comment-tools"></div><div class="clear"></div><div id="comment-54387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

