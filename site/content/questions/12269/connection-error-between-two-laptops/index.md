+++
type = "question"
title = "connection error between two laptop&#x27;s"
description = '''Hi everybody, I am using Wireshark ver.1.6.5 and tried to get a remote connection on other PC on my local network but i could&#x27;nt able to do that and got a error &quot; Can&#x27;t get list of interfaces: Is the server properly installed on X.X.X.X ?&quot; i need to explain that i created a user on my remote pc and ...'''
date = "2012-06-28T05:18:00Z"
lastmod = "2012-06-29T15:19:00Z"
weight = 12269
keywords = [ "bingo" ]
aliases = [ "/questions/12269" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [connection error between two laptop's](/questions/12269/connection-error-between-two-laptops)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12269-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12269-score" class="post-score" title="current number of votes">0</div><span id="post-12269-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everybody,</p><p>I am using Wireshark ver.1.6.5 and tried to get a remote connection on other PC on my local network but i could'nt able to do that and got a error " Can't get list of interfaces: Is the server properly installed on X.X.X.X ?" i need to explain that i created a user on my remote pc and added to Remote Packet Capture Protocol v.0(experimental) service and also restarted this service more that one. The connection between 2 PC's is Wireless and both are using a different Antivirus.( maybe need to know )</p><p>Thanks a lot</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bingo" rel="tag" title="see questions tagged &#39;bingo&#39;">bingo</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jun '12, 05:18</strong></p><img src="https://secure.gravatar.com/avatar/47a7e698e00a2e039822404128b814f9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mm23&#39;s gravatar image" /><p><span>mm23</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mm23 has no accepted answers">0%</span></p></div></div><div id="comments-container-12269" class="comments-container"><span id="12339"></span><div id="comment-12339" class="comment"><div id="post-12339-score" class="comment-score"></div><div class="comment-text"><p>I upgraded both of my Laptop to new Wireshark version(1.8.0) and try to do that again but this problem exist now.</p></div><div id="comment-12339-info" class="comment-info"><span class="comment-age">(29 Jun '12, 15:16)</span> <span class="comment-user userinfo">mm23</span></div></div></div><div id="comment-tools-12269" class="comment-tools"></div><div class="clear"></div><div id="comment-12269-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12320"></span>

<div id="answer-container-12320" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12320-score" class="post-score" title="current number of votes">0</div><span id="post-12320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can always open up a second Wireshark and capture the rpcap session you're trying. That can direct you to potential problems, like firewalls.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jun '12, 08:47</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-12320" class="comments-container"><span id="12340"></span><div id="comment-12340" class="comment"><div id="post-12340-score" class="comment-score"></div><div class="comment-text"><p>Hi Jaap,</p><p>It will be good idea. At the first i disabled Firewall on my remote Laptop but did not worked and then tried to disable all antivirus protection on remote too but result was the same. I need to tell you that i did not stopped firewall on both of them due to security.</p></div><div id="comment-12340-info" class="comment-info"><span class="comment-age">(29 Jun '12, 15:19)</span> <span class="comment-user userinfo">mm23</span></div></div></div><div id="comment-tools-12320" class="comment-tools"></div><div class="clear"></div><div id="comment-12320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

