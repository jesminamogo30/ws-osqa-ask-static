+++
type = "question"
title = "Errors when installing Wireshark 1.8.5 on Ubuntu 10.10"
description = '''in ubuntu10.10 i have tried to install wireshark 1.8.5 by following ways:  from synaptic manager sudo apt-get install wireshark and download wireshark1.8.5.tar in that ./configure make make install  but it does not get installed. Each time it shows errors. plz help me.'''
date = "2013-02-06T11:42:00Z"
lastmod = "2013-02-06T14:04:00Z"
weight = 18372
keywords = [ "ubuntu" ]
aliases = [ "/questions/18372" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Errors when installing Wireshark 1.8.5 on Ubuntu 10.10](/questions/18372/errors-when-installing-wireshark-185-on-ubuntu-1010)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18372-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18372-score" class="post-score" title="current number of votes">0</div><span id="post-18372-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>in ubuntu10.10 i have tried to install wireshark 1.8.5 by following ways:</p><ol><li>from synaptic manager</li><li>sudo apt-get install wireshark</li><li>and download wireshark1.8.5.tar in that ./configure make make install</li></ol><p>but it does not get installed. Each time it shows errors. plz help me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '13, 11:42</strong></p><img src="https://secure.gravatar.com/avatar/7057e134863df3773690cc409a94ae08?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yumwel&#39;s gravatar image" /><p><span>yumwel</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yumwel has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Feb '13, 16:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-18372" class="comments-container"><span id="18377"></span><div id="comment-18377" class="comment"><div id="post-18377-score" class="comment-score"></div><div class="comment-text"><p>What are the errors you got for those three attempts? We can't help you without knowing what the errors are.</p></div><div id="comment-18377-info" class="comment-info"><span class="comment-age">(06 Feb '13, 14:04)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-18372" class="comment-tools"></div><div class="clear"></div><div id="comment-18372-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

