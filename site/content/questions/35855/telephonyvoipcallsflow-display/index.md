+++
type = "question"
title = "Telephony/VoipCalls/Flow Display"
description = '''This question relates to the Graph Analysis window that is displayed when one chooses Telephony/VoipCalls/Flow on a particular call. The middle column, when displayed on an associates computer, shows more information than when displayed on my computer. Specifically, on some records on my pc, I just ...'''
date = "2014-08-28T19:20:00Z"
lastmod = "2014-08-28T21:31:00Z"
weight = 35855
keywords = [ "voipcalls", "flow" ]
aliases = [ "/questions/35855" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Telephony/VoipCalls/Flow Display](/questions/35855/telephonyvoipcallsflow-display)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35855-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35855-score" class="post-score" title="current number of votes">0</div><span id="post-35855-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This question relates to the Graph Analysis window that is displayed when one chooses Telephony/VoipCalls/Flow on a particular call.</p><p>The middle column, when displayed on an associates computer, shows more information than when displayed on my computer. Specifically, on some records on my pc, I just see RTP (telephone .. and the rest is cut off. On another computer this column is wider and displays RTP (telephone event) DTMF Two 2. I have made the column wider on my pc but it does not show this additional information.</p><p>I sure would like to know how to get this additional information displayed.</p><p>Thank you for your help.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/FlowGraph_1.gif" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/FlowGraph2.gif" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voipcalls" rel="tag" title="see questions tagged &#39;voipcalls&#39;">voipcalls</span> <span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '14, 19:20</strong></p><img src="https://secure.gravatar.com/avatar/a40258ab32ba13a839853b1e0ade00ab?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DiverAllen&#39;s gravatar image" /><p><span>DiverAllen</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DiverAllen has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Aug '14, 19:23</strong> </span></p></div></div><div id="comments-container-35855" class="comments-container"><span id="35856"></span><div id="comment-35856" class="comment"><div id="post-35856-score" class="comment-score"></div><div class="comment-text"><p>Is it the same wireshark version on the two computers?</p></div><div id="comment-35856-info" class="comment-info"><span class="comment-age">(28 Aug '14, 21:31)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-35855" class="comment-tools"></div><div class="clear"></div><div id="comment-35855-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

