+++
type = "question"
title = "How can WinPcap be installed unatttended and silently on Windows"
description = '''We want to pre-stage WireShark on our servers.  Doing this we have it all prepared when we need to do some tracing. Also we can delegate the right to trace/troubleshoot without having people being permanently administrators on the Window servers. We would like to be able to distribute WinPcap and Wi...'''
date = "2012-03-08T23:56:00Z"
lastmod = "2012-03-09T09:36:00Z"
weight = 9452
keywords = [ "winpcap", "unattended", "silently" ]
aliases = [ "/questions/9452" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How can WinPcap be installed unatttended and silently on Windows](/questions/9452/how-can-winpcap-be-installed-unatttended-and-silently-on-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9452-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9452-score" class="post-score" title="current number of votes">0</div><span id="post-9452-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We want to pre-stage WireShark on our servers. Doing this we have it all prepared when we need to do some tracing. Also we can delegate the right to trace/troubleshoot without having people being permanently administrators on the Window servers.</p><p>We would like to be able to distribute WinPcap and WireShark independently and maintain/upgrade them indecently.</p><ol><li>Out of the box WinPcap has no option to install unattended and silently – is this deliberately or?</li><li>It would be a great help to have the following functionally regarding WinPcap a. Install WinPcap unattended/silently b. If a version of WinPcap is already installed an option to upgrade WinPcap unattended/silently would be nice c. Uninstall the current installed version unattended/silently</li><li>Wireshark has the option to install unattended/silently using the /S switch. The /S switch is documented as a “toggle” switch but it does not always seems to work for uninstallation. Just like WinPcap is would be a great help to have the following possibilities a. Install WireShark unattended/silently (using /S switch seems to work OK) b. If a version of WireShark is already installed the upgrade to a new version using /S switch seems to work OK c. Uninstall the current installed version unattended/silently. This seems not to work.</li></ol><p>Hope someone can give some directions regarding the WinPcap issue.</p><p>TIA</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-unattended" rel="tag" title="see questions tagged &#39;unattended&#39;">unattended</span> <span class="post-tag tag-link-silently" rel="tag" title="see questions tagged &#39;silently&#39;">silently</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Mar '12, 23:56</strong></p><img src="https://secure.gravatar.com/avatar/41ceec8c367e45ba2a9f88727b6de498?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ITO&#39;s gravatar image" /><p><span>ITO</span><br />
<span class="score" title="-1 reputation points">-1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ITO has no accepted answers">0%</span></p></div></div><div id="comments-container-9452" class="comments-container"></div><div id="comment-tools-9452" class="comment-tools"></div><div class="clear"></div><div id="comment-9452-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9458"></span>

<div id="answer-container-9458" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9458-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9458-score" class="post-score" title="current number of votes">0</div><span id="post-9458-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Get <a href="http://www.riverbed.com/us/products/cascade/winpcap_pro.php">WinPcap Pro</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Mar '12, 08:32</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-9458" class="comments-container"><span id="9459"></span><div id="comment-9459" class="comment"><div id="post-9459-score" class="comment-score"></div><div class="comment-text"><p>Hi Jaap</p><p>Short answer - I have already tried . . . here is the answer from Janice Spampinato.</p><p>===================================</p><p>We used to offer WinPcap Pro for this but just recently discontinued it and there are no alternative products around, as far as I know.</p><p>Best to You, Janice</p></div><div id="comment-9459-info" class="comment-info"><span class="comment-age">(09 Mar '12, 09:36)</span> <span class="comment-user userinfo">ITO</span></div></div></div><div id="comment-tools-9458" class="comment-tools"></div><div class="clear"></div><div id="comment-9458-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

