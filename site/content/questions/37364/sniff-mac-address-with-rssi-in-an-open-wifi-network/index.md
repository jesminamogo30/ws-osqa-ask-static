+++
type = "question"
title = "Sniff MAC address with RSSI in an open wifi Network"
description = '''HI, I have a project and it required me to perform sniffing for MAC address with RSSI information in an open Wifi network.  I have downloaded the Wireshark, but i am not sure if it can perform the task stated above. Can anyone help and advise?'''
date = "2014-10-26T20:23:00Z"
lastmod = "2014-10-26T20:23:00Z"
weight = 37364
keywords = [ "sniffer" ]
aliases = [ "/questions/37364" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Sniff MAC address with RSSI in an open wifi Network](/questions/37364/sniff-mac-address-with-rssi-in-an-open-wifi-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37364-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37364-score" class="post-score" title="current number of votes">0</div><span id="post-37364-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>HI, I have a project and it required me to perform sniffing for MAC address with RSSI information in an open Wifi network. I have downloaded the Wireshark, but i am not sure if it can perform the task stated above. Can anyone help and advise?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffer" rel="tag" title="see questions tagged &#39;sniffer&#39;">sniffer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '14, 20:23</strong></p><img src="https://secure.gravatar.com/avatar/e9cd4eb09689cd01f01aa2cffb749d12?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Novice&#39;s gravatar image" /><p><span>Novice</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Novice has no accepted answers">0%</span></p></div></div><div id="comments-container-37364" class="comments-container"></div><div id="comment-tools-37364" class="comment-tools"></div><div class="clear"></div><div id="comment-37364-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

