+++
type = "question"
title = "How to see the E-DCH UL DATA FRAME"
description = '''I have a pcap trace file. How to see the E-DCH UL DATA FRAME? Should I load some special protocol stack to Wireshark, or just configure the filters properly?'''
date = "2014-01-13T09:42:00Z"
lastmod = "2014-01-14T03:44:00Z"
weight = 28844
keywords = [ "e-dch", "data", "frame" ]
aliases = [ "/questions/28844" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to see the E-DCH UL DATA FRAME](/questions/28844/how-to-see-the-e-dch-ul-data-frame)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28844-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28844-score" class="post-score" title="current number of votes">0</div><span id="post-28844-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a pcap trace file. How to see the E-DCH UL DATA FRAME? Should I load some special protocol stack to Wireshark, or just configure the filters properly?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-e-dch" rel="tag" title="see questions tagged &#39;e-dch&#39;">e-dch</span> <span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-frame" rel="tag" title="see questions tagged &#39;frame&#39;">frame</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jan '14, 09:42</strong></p><img src="https://secure.gravatar.com/avatar/9d52e88dc9de22a0c815571bc8f671cb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Bob1668&#39;s gravatar image" /><p><span>Bob1668</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Bob1668 has no accepted answers">0%</span></p></div></div><div id="comments-container-28844" class="comments-container"><span id="28857"></span><div id="comment-28857" class="comment"><div id="post-28857-score" class="comment-score"></div><div class="comment-text"><p>What protocol are you refering to (RANAP, NBAP , RNSAP...)? How is the protocol transported,? Which layers do you see in the trace? What version of Wireshark are you using?</p></div><div id="comment-28857-info" class="comment-info"><span class="comment-age">(14 Jan '14, 03:44)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-28844" class="comment-tools"></div><div class="clear"></div><div id="comment-28844-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

