+++
type = "question"
title = "Acme A-SBC - Med Ext and Sig Ext"
description = '''Hi. I know this is not the best place to ask, but so far, using Google, I didn&#x27;t find any answer. Appending to A-SBC, what does Med Ext and Sig Ext mean?'''
date = "2011-08-03T15:22:00Z"
lastmod = "2011-08-03T15:43:00Z"
weight = 5455
keywords = [ "sbc", "ext", "sig", "acme", "med" ]
aliases = [ "/questions/5455" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Acme A-SBC - Med Ext and Sig Ext](/questions/5455/acme-a-sbc-med-ext-and-sig-ext)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5455-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5455-score" class="post-score" title="current number of votes">0</div><span id="post-5455-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi.</p><p>I know this is not the best place to ask, but so far, using Google, I didn't find any answer.</p><p>Appending to A-SBC, what does Med Ext and Sig Ext mean?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sbc" rel="tag" title="see questions tagged &#39;sbc&#39;">sbc</span> <span class="post-tag tag-link-ext" rel="tag" title="see questions tagged &#39;ext&#39;">ext</span> <span class="post-tag tag-link-sig" rel="tag" title="see questions tagged &#39;sig&#39;">sig</span> <span class="post-tag tag-link-acme" rel="tag" title="see questions tagged &#39;acme&#39;">acme</span> <span class="post-tag tag-link-med" rel="tag" title="see questions tagged &#39;med&#39;">med</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '11, 15:22</strong></p><img src="https://secure.gravatar.com/avatar/13231e33ab17a93476f7b98c9d5b272a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wired&#39;s gravatar image" /><p><span>wired</span><br />
<span class="score" title="44 reputation points">44</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wired has one accepted answer">9%</span></p></div></div><div id="comments-container-5455" class="comments-container"></div><div id="comment-tools-5455" class="comment-tools"></div><div class="clear"></div><div id="comment-5455-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="5456"></span>

<div id="answer-container-5456" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5456-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5456-score" class="post-score" title="current number of votes">0</div><span id="post-5456-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I found the answer - Sig = Signalling, Med = Media and Ext = External.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Aug '11, 15:43</strong></p><img src="https://secure.gravatar.com/avatar/13231e33ab17a93476f7b98c9d5b272a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wired&#39;s gravatar image" /><p><span>wired</span><br />
<span class="score" title="44 reputation points">44</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wired has one accepted answer">9%</span></p></div></div><div id="comments-container-5456" class="comments-container"></div><div id="comment-tools-5456" class="comment-tools"></div><div class="clear"></div><div id="comment-5456-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

