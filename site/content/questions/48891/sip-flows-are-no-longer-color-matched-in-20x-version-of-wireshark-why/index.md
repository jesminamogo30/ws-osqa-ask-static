+++
type = "question"
title = "SIP Flows are no longer color matched in 2.0.x version of WireShark - why?"
description = '''SIP Flows are no longer color matched in 2.0.x version of WireShark - why?'''
date = "2016-01-05T15:08:00Z"
lastmod = "2016-01-06T05:16:00Z"
weight = 48891
keywords = [ "27543" ]
aliases = [ "/questions/48891" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [SIP Flows are no longer color matched in 2.0.x version of WireShark - why?](/questions/48891/sip-flows-are-no-longer-color-matched-in-20x-version-of-wireshark-why)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48891-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48891-score" class="post-score" title="current number of votes">0</div><span id="post-48891-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>SIP Flows are no longer color matched in 2.0.x version of WireShark - why?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-27543" rel="tag" title="see questions tagged &#39;27543&#39;">27543</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jan '16, 15:08</strong></p><img src="https://secure.gravatar.com/avatar/1864a2113be3df565291df712aea1dbf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RomanDS&#39;s gravatar image" /><p><span>RomanDS</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RomanDS has no accepted answers">0%</span></p></div></div><div id="comments-container-48891" class="comments-container"></div><div id="comment-tools-48891" class="comment-tools"></div><div class="clear"></div><div id="comment-48891-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="48904"></span>

<div id="answer-container-48904" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48904-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48904-score" class="post-score" title="current number of votes">0</div><span id="post-48904-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Because this has not been ported to Qt based UI yet. See also <a href="https://ask.wireshark.org/questions/48144/wireshark-20-voip-call-flow-no-colors">https://ask.wireshark.org/questions/48144/wireshark-20-voip-call-flow-no-colors</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jan '16, 05:16</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-48904" class="comments-container"></div><div id="comment-tools-48904" class="comment-tools"></div><div class="clear"></div><div id="comment-48904-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

