+++
type = "question"
title = "RRC filters"
description = '''Hello to everybody, I&#x27;ve found something strange in rrc filters expression, in several cases the same filter abbreviation have different type, this can be a problem and/or can cause a crash? for example: { &amp;amp;hf_rrc_criticalExtensions_117,  { &quot;criticalExtensions&quot;, &quot;rrc.criticalExtensions&quot;,  FT_UIN...'''
date = "2012-09-25T05:47:00Z"
lastmod = "2012-09-25T05:47:00Z"
weight = 14506
keywords = [ "filter", "duplicate", "rrc" ]
aliases = [ "/questions/14506" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [RRC filters](/questions/14506/rrc-filters)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14506-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14506-score" class="post-score" title="current number of votes">0</div><span id="post-14506-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello to everybody, I've found something strange in rrc filters expression, in several cases the same filter abbreviation have different type, this can be a problem and/or can cause a crash?</p><p>for example:</p><p>{ &amp;hf_rrc_criticalExtensions_117, { "criticalExtensions", "rrc.criticalExtensions", FT_UINT32, BASE_DEC, VALS(rrc_T_criticalExtensions_117_vals), 0, "T_criticalExtensions_117", HFILL }},</p><p>and</p><p>{ &amp;hf_rrc_criticalExtensions_118, { "criticalExtensions", "rrc.criticalExtensions", FT_NONE, BASE_NONE, NULL, 0, "T_criticalExtensions_118", HFILL }},</p><p>Lucio</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-duplicate" rel="tag" title="see questions tagged &#39;duplicate&#39;">duplicate</span> <span class="post-tag tag-link-rrc" rel="tag" title="see questions tagged &#39;rrc&#39;">rrc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Sep '12, 05:47</strong></p><img src="https://secure.gravatar.com/avatar/3badef3f7a64dd34ca4824360fdd27bf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="luxxx&#39;s gravatar image" /><p><span>luxxx</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="luxxx has no accepted answers">0%</span></p></div></div><div id="comments-container-14506" class="comments-container"></div><div id="comment-tools-14506" class="comment-tools"></div><div class="clear"></div><div id="comment-14506-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

