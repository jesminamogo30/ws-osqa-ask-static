+++
type = "question"
title = "Not able to install winpcap 4.1.3"
description = '''My problem is that I have uninstalled 4.1.2 and yet - when I try to install 4.1.3 - I get the message: A previous version of WinPcap has been detected on this system and cannot be removed because in use by another application. What shall I do? Now I can&#x27;t run WireShark because I don&#x27;t have WinPcap i...'''
date = "2013-09-23T02:09:00Z"
lastmod = "2014-10-29T05:36:00Z"
weight = 25105
keywords = [ "wincap" ]
aliases = [ "/questions/25105" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Not able to install winpcap 4.1.3](/questions/25105/not-able-to-install-winpcap-413)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25105-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25105-score" class="post-score" title="current number of votes">0</div><span id="post-25105-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My problem is that I have uninstalled 4.1.2 and yet - when I try to install 4.1.3 - I get the message:</p><p>A previous version of WinPcap has been detected on this system and cannot be removed because in use by another application.</p><p>What shall I do? Now I can't run WireShark because I don't have WinPcap in my system ...</p><p>:o) Truls</p><hr /><p>Truls Hjelle, M. Sc. in Mathematics CCIE #22714 Routing &amp; Switching Mobile (+47) 4500 2417 mailto:<span class="__cf_email__" data-cfemail="2c585e59405f6c5f5942480144464940404902435e4b">[email protected]</span></p><pre><code>Hjelle Datacom Consulting
Org.nr. NO 911 849 577 MVA</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wincap" rel="tag" title="see questions tagged &#39;wincap&#39;">wincap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '13, 02:09</strong></p><img src="https://secure.gravatar.com/avatar/3b0e0b28c53c4c5c2d08acd468455dc3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TruHje&#39;s gravatar image" /><p><span>TruHje</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TruHje has no accepted answers">0%</span></p></div></div><div id="comments-container-25105" class="comments-container"><span id="25106"></span><div id="comment-25106" class="comment"><div id="post-25106-score" class="comment-score"></div><div class="comment-text"><p>did you try this:</p><blockquote><p><a href="http://bit.ly/1fsWDJJ">http://bit.ly/1fsWDJJ</a></p></blockquote></div><div id="comment-25106-info" class="comment-info"><span class="comment-age">(23 Sep '13, 02:24)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="37412"></span><div id="comment-37412" class="comment"><div id="post-37412-score" class="comment-score"></div><div class="comment-text"><p>Kurt, you are just part of the problem now, because when I <em>WAS</em> googling for a solution, I come to this post and follow your stupid link and waste time watching the stupid lmgtfy.com crap.</p><p>Here is my solution <a href="https://ask.wireshark.org/questions/9229/winpcap-will-not-install?page=1&amp;focusedAnswerId=37410#37410">https://ask.wireshark.org/questions/9229/winpcap-will-not-install?page=1&amp;focusedAnswerId=37410#37410</a></p></div><div id="comment-37412-info" class="comment-info"><span class="comment-age">(28 Oct '14, 09:25)</span> <span class="comment-user userinfo">J4E</span></div></div><span id="37433"></span><div id="comment-37433" class="comment"><div id="post-37433-score" class="comment-score"></div><div class="comment-text"><p>I don't see where my link is stupid in any way. That google search returns possible solutions. If they don't work in your environment, who can blame google for that?</p></div><div id="comment-37433-info" class="comment-info"><span class="comment-age">(29 Oct '14, 05:36)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-25105" class="comment-tools"></div><div class="clear"></div><div id="comment-25105-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

