+++
type = "question"
title = "cant launch wireshark help me asap! please"
description = '''could someone help me? everytime i try to launch it i get this.. and i cant solve it with the help thing , i&#x27;ve searched everywhere on google but couldn&#x27;t find the answer to this.  The capture session could not be initiated on interface &#x27; }&#x27; (failed to set hardware filter to promiscuous mode). Pleas...'''
date = "2015-08-28T08:05:00Z"
lastmod = "2015-08-28T08:12:00Z"
weight = 45469
keywords = [ "cant" ]
aliases = [ "/questions/45469" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [cant launch wireshark help me asap! please](/questions/45469/cant-launch-wireshark-help-me-asap-please)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45469-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45469-score" class="post-score" title="current number of votes">0</div><span id="post-45469-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>could someone help me? everytime i try to launch it i get this.. and i cant solve it with the help thing , i've searched everywhere on google but couldn't find the answer to this.</p><blockquote><p>The capture session could not be initiated on interface ' }' (failed to set hardware filter to promiscuous mode).</p><p>Please check that "}" is the proper interface.</p><p>Help can be found at:</p><pre><code>   http://wiki.wireshark.org/WinPcap
   http://wiki.wireshark.org/CaptureSetup</code></pre></blockquote></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cant" rel="tag" title="see questions tagged &#39;cant&#39;">cant</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '15, 08:05</strong></p><img src="https://secure.gravatar.com/avatar/81d763d0ff53eb24b532af6460dc131d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yong%20pal&#39;s gravatar image" /><p><span>yong pal</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yong pal has no accepted answers">0%</span></p></div></div><div id="comments-container-45469" class="comments-container"><span id="45470"></span><div id="comment-45470" class="comment"><div id="post-45470-score" class="comment-score"></div><div class="comment-text"><p>As always! We need more information, so</p><ul><li>what is your OS and OS version</li><li>what is your Wireshark version</li><li>how did you start Wireshark (please include CLI options if you have used them)</li><li>what is the output of <strong>dumpcap -D -M</strong></li></ul></div><div id="comment-45470-info" class="comment-info"><span class="comment-age">(28 Aug '15, 08:12)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-45469" class="comment-tools"></div><div class="clear"></div><div id="comment-45469-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

