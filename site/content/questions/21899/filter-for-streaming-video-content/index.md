+++
type = "question"
title = "filter for streaming video content"
description = '''i want to filter out streaming media...audio,video,flash. will http.type.content=video or flash catch the offenders and sites?? or is there more to it. I think I should be seeing more, than what I am getting returned.'''
date = "2013-06-10T12:37:00Z"
lastmod = "2013-06-11T03:18:00Z"
weight = 21899
keywords = [ "media", "filters" ]
aliases = [ "/questions/21899" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [filter for streaming video content](/questions/21899/filter-for-streaming-video-content)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21899-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21899-score" class="post-score" title="current number of votes">0</div><span id="post-21899-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i want to filter out streaming media...audio,video,flash. will http.type.content=video or flash catch the offenders and sites?? or is there more to it. I think I should be seeing more, than what I am getting returned.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-media" rel="tag" title="see questions tagged &#39;media&#39;">media</span> <span class="post-tag tag-link-filters" rel="tag" title="see questions tagged &#39;filters&#39;">filters</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jun '13, 12:37</strong></p><img src="https://secure.gravatar.com/avatar/e9b4d52e2849052f2f616cc40aadee99?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mickeyhr&#39;s gravatar image" /><p><span>mickeyhr</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mickeyhr has no accepted answers">0%</span></p></div></div><div id="comments-container-21899" class="comments-container"></div><div id="comment-tools-21899" class="comment-tools"></div><div class="clear"></div><div id="comment-21899-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21913"></span>

<div id="answer-container-21913" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21913-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21913-score" class="post-score" title="current number of votes">0</div><span id="post-21913-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>or is there more to it.</p></blockquote><p>There is much more to it, as there is no <strong>unique</strong> standard for "audio/video streaming" and thus you cannot rely just on the Content-Type header. Unfortunately there are many different forms of streaming solutions, each using different methods and encodings, so it's hard to define a "fit all" filter. Sorry....</p><p>Anyway, your filter is a good start.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jun '13, 03:18</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-21913" class="comments-container"></div><div id="comment-tools-21913" class="comment-tools"></div><div class="clear"></div><div id="comment-21913-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

