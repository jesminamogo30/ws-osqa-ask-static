+++
type = "question"
title = "Is there a way to list all User Access Tables and formats?"
description = '''Wireshark provides some information about its internals, accessed via View -&amp;gt; Internals -&amp;gt; (or just Internals in older versions), but it doesn&#x27;t list the User Access Tables that I can see. I don&#x27;t find it using tshark -G ? either. Is there a way to find out what all the User Access Tables are ...'''
date = "2017-03-08T07:28:00Z"
lastmod = "2017-03-08T08:14:00Z"
weight = 59927
keywords = [ "internals", "uat", "command-line", "preferences" ]
aliases = [ "/questions/59927" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is there a way to list all User Access Tables and formats?](/questions/59927/is-there-a-way-to-list-all-user-access-tables-and-formats)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59927-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59927-score" class="post-score" title="current number of votes">0</div><span id="post-59927-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark provides some information about its internals, accessed via <em>View -&gt; Internals -&gt;</em> (or just <em>Internals</em> in older versions), but it doesn't list the User Access Tables that I can see. I don't find it using <code>tshark -G ?</code> either.</p><p>Is there a way to find out what all the User Access Tables are and their formats?</p><p>(I ask because I had to search the code to find the name of the <code>esp_sa</code> table when answering a question over at stackoverflow, namely <a href="http://stackoverflow.com/questions/42666665/wireshark-2-2-5-how-to-set-esp-preference-from-command-line">"wireshark 2.2.5 - how to set ESP preference from command line"</a>.)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-internals" rel="tag" title="see questions tagged &#39;internals&#39;">internals</span> <span class="post-tag tag-link-uat" rel="tag" title="see questions tagged &#39;uat&#39;">uat</span> <span class="post-tag tag-link-command-line" rel="tag" title="see questions tagged &#39;command-line&#39;">command-line</span> <span class="post-tag tag-link-preferences" rel="tag" title="see questions tagged &#39;preferences&#39;">preferences</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Mar '17, 07:28</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-59927" class="comments-container"><span id="59928"></span><div id="comment-59928" class="comment"><div id="post-59928-score" class="comment-score"></div><div class="comment-text"><p>I don't know the answer to your question, but the ESP preference question was also asked on <a href="https://ask.wireshark.org/questions/59907/wireshark-225-how-to-set-esp-preference-from-command-line">here</a>.</p></div><div id="comment-59928-info" class="comment-info"><span class="comment-age">(08 Mar '17, 08:08)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="59930"></span><div id="comment-59930" class="comment"><div id="post-59930-score" class="comment-score"></div><div class="comment-text"><p>Thanks, I obviously missed it here.</p></div><div id="comment-59930-info" class="comment-info"><span class="comment-age">(08 Mar '17, 08:14)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-59927" class="comment-tools"></div><div class="clear"></div><div id="comment-59927-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

