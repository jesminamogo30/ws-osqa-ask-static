+++
type = "question"
title = "rpcapd version for Linux (RHEL-6.4) for performing remote capture from Windows (wireshark-1.12.4)"
description = '''Hi all I am performing remote capture on Linux machine (RHEL-6.4) from Windows machine (Win 7). I am running Wireshark-1.12.4 on Windows machine and rpcapd on remote Linux machine. I am able to get the interface list of the remote machine, but the link type is shown as &quot;unknown&quot;.  I have tried the s...'''
date = "2015-04-28T05:25:00Z"
lastmod = "2015-05-19T00:49:00Z"
weight = 41914
keywords = [ "rpcapd", "remote-capture", "link-type", "capture", "wireshark" ]
aliases = [ "/questions/41914" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [rpcapd version for Linux (RHEL-6.4) for performing remote capture from Windows (wireshark-1.12.4)](/questions/41914/rpcapd-version-for-linux-rhel-64-for-performing-remote-capture-from-windows-wireshark-1124)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41914-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41914-score" class="post-score" title="current number of votes">0</div><span id="post-41914-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all</p><p>I am performing remote capture on Linux machine (RHEL-6.4) from Windows machine (Win 7). I am running Wireshark-1.12.4 on Windows machine and rpcapd on remote Linux machine. I am able to get the interface list of the remote machine, but the link type is shown as "unknown".</p><p>I have tried the same in Wireshark-1.8.4 it works fine without issues, So, wanted to know what could be the issue ?. Should i use a specific rpcapd version to support with Wireshark-1.12.x ? If so, can anyone point to the proper version of rpcapd.</p><p>Thank you. Dinesh Sadu.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rpcapd" rel="tag" title="see questions tagged &#39;rpcapd&#39;">rpcapd</span> <span class="post-tag tag-link-remote-capture" rel="tag" title="see questions tagged &#39;remote-capture&#39;">remote-capture</span> <span class="post-tag tag-link-link-type" rel="tag" title="see questions tagged &#39;link-type&#39;">link-type</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Apr '15, 05:25</strong></p><img src="https://secure.gravatar.com/avatar/04334c27cb629065a13d61a61b611038?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dinesh%20Babu%20Sadu&#39;s gravatar image" /><p><span>Dinesh Babu ...</span><br />
<span class="score" title="16 reputation points">16</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dinesh Babu Sadu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 May '15, 01:01</strong> </span></p></div></div><div id="comments-container-41914" class="comments-container"><span id="42527"></span><div id="comment-42527" class="comment"><div id="post-42527-score" class="comment-score"></div><div class="comment-text"><p>Any Update .. ??</p></div><div id="comment-42527-info" class="comment-info"><span class="comment-age">(19 May '15, 00:49)</span> <span class="comment-user userinfo">Dinesh Babu ...</span></div></div></div><div id="comment-tools-41914" class="comment-tools"></div><div class="clear"></div><div id="comment-41914-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

