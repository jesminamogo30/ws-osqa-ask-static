+++
type = "question"
title = "If tshark can support &quot;export objects&quot; like wireshark for GUI"
description = '''Dear All. I have a question about tshark. Could you explain me why tshark doesn&#x27;t support &quot;export objects&quot; like wireshark? I couldn&#x27;t find anything comment related to my question. Please, advice me. Thank you'''
date = "2013-11-13T11:59:00Z"
lastmod = "2016-12-15T16:52:00Z"
weight = 26959
keywords = [ "tshark" ]
aliases = [ "/questions/26959" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [If tshark can support "export objects" like wireshark for GUI](/questions/26959/if-tshark-can-support-export-objects-like-wireshark-for-gui)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26959-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26959-score" class="post-score" title="current number of votes">0</div><span id="post-26959-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear All.</p><p>I have a question about tshark. Could you explain me why tshark doesn't support "export objects" like wireshark? I couldn't find anything comment related to my question. Please, advice me.</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Nov '13, 11:59</strong></p><img src="https://secure.gravatar.com/avatar/c428d445ddba52204f7718897b1acfb8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="honeycap78&#39;s gravatar image" /><p><span>honeycap78</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="honeycap78 has no accepted answers">0%</span></p></div></div><div id="comments-container-26959" class="comments-container"></div><div id="comment-tools-26959" class="comment-tools"></div><div class="clear"></div><div id="comment-26959-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="26989"></span>

<div id="answer-container-26989" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26989-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26989-score" class="post-score" title="current number of votes">1</div><span id="post-26989-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Could you explain me <strong>why tshark doesn't support "export objects"</strong> like wireshark? I</p></blockquote><p>because nobody had the time and/or interest to implement that feature yet.</p><p>If the "export objects" feature in GUI does not work for you (because you want to automate it), there are other (CLI) tools.</p><blockquote><p><a href="http://code.google.com/p/nfex/">http://code.google.com/p/nfex/</a><br />
<a href="https://isc.sans.edu/diary/Tools+for+extracting+files+from+pcaps/6961">https://isc.sans.edu/diary/Tools+for+extracting+files+from+pcaps/6961</a></p></blockquote><p>Maybe one of those will help you.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Nov '13, 04:22</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-26989" class="comments-container"></div><div id="comment-tools-26989" class="comment-tools"></div><div class="clear"></div><div id="comment-26989-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="58156"></span>

<div id="answer-container-58156" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58156-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58156-score" class="post-score" title="current number of votes">1</div><span id="post-58156-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As of Wireshark 2.3.0, you can export HTTP objects with tshark. (Wireshark 2.3.0 hasn't been released yet, so you can grab a daily build from <a href="https://www.wireshark.org/download/automated/win32/">here</a>.)</p><p>To extract HTTP objects from the command-line, run the following command:</p><pre><code>tshark -r mypcap.pcap --export-objects &quot;http,destdir&quot;</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Dec '16, 16:52</strong></p><img src="https://secure.gravatar.com/avatar/8df259c952186aa93179732461b8d1e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="moshe&#39;s gravatar image" /><p><span>moshe</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="moshe has no accepted answers">0%</span></p></div></div><div id="comments-container-58156" class="comments-container"></div><div id="comment-tools-58156" class="comment-tools"></div><div class="clear"></div><div id="comment-58156-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

