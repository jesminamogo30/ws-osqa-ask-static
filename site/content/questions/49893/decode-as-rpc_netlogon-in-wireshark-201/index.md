+++
type = "question"
title = "Decode as RPC_Netlogon in wireshark 2.0.1"
description = '''Hello All, Got a quick question. Recently moved on to Wireshark 2.0.1 and tried to review netlogon events. However when I filtered the traffic, none showed up. In previous versions of wireshark I could right click on the traffic and select decode as. Then in the next window under DCE-RPC, I could se...'''
date = "2016-02-05T05:53:00Z"
lastmod = "2016-02-05T05:53:00Z"
weight = 49893
keywords = [ "decode", "rpc_netlogon", "2.0.1" ]
aliases = [ "/questions/49893" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decode as RPC\_Netlogon in wireshark 2.0.1](/questions/49893/decode-as-rpc_netlogon-in-wireshark-201)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49893-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49893-score" class="post-score" title="current number of votes">0</div><span id="post-49893-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello All,</p><p>Got a quick question. Recently moved on to Wireshark 2.0.1 and tried to review netlogon events. However when I filtered the traffic, none showed up. In previous versions of wireshark I could right click on the traffic and select decode as. Then in the next window under DCE-RPC, I could select RPC_Netlogon as the option. In 2.0.1, I do not have that option. Anyone know how to accomplish this? If not I guess I will have to revert back to 1.10.8 to continue my work.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-rpc_netlogon" rel="tag" title="see questions tagged &#39;rpc_netlogon&#39;">rpc_netlogon</span> <span class="post-tag tag-link-2.0.1" rel="tag" title="see questions tagged &#39;2.0.1&#39;">2.0.1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Feb '16, 05:53</strong></p><img src="https://secure.gravatar.com/avatar/b4ff1cc5dd6b076eeff39f6d39b28e46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bgartama&#39;s gravatar image" /><p><span>bgartama</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bgartama has no accepted answers">0%</span></p></div></div><div id="comments-container-49893" class="comments-container"></div><div id="comment-tools-49893" class="comment-tools"></div><div class="clear"></div><div id="comment-49893-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

