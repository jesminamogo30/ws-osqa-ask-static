+++
type = "question"
title = "some fp message can&#x27;t been decode."
description = '''On wireshark1.8.5. the frame:2724 can&#x27;t been decode to fp level. my step: 1,filter use sctp. 2,decode the sctp:data frame as nbap. many fp over udp have been decoded. but some are can&#x27;t. could you tell me how to attach a *.pcap in here? Thanks!'''
date = "2013-02-05T01:57:00Z"
lastmod = "2013-02-05T02:15:00Z"
weight = 18311
keywords = [ "rlc", "nbap" ]
aliases = [ "/questions/18311" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [some fp message can't been decode.](/questions/18311/some-fp-message-cant-been-decode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18311-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18311-score" class="post-score" title="current number of votes">0</div><span id="post-18311-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>On wireshark1.8.5. the frame:2724 can't been decode to fp level.</p><p>my step: 1,filter use sctp. 2,decode the sctp:data frame as nbap.</p><p>many fp over udp have been decoded. but some are can't.</p><p>could you tell me how to attach a *.pcap in here?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rlc" rel="tag" title="see questions tagged &#39;rlc&#39;">rlc</span> <span class="post-tag tag-link-nbap" rel="tag" title="see questions tagged &#39;nbap&#39;">nbap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Feb '13, 01:57</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-18311" class="comments-container"><span id="18314"></span><div id="comment-18314" class="comment"><div id="post-18314-score" class="comment-score"></div><div class="comment-text"><p>Post it to a file hosting site, preferably one that doesn't have a download delay or other annoying feature. Many folks use <a href="http://cloudshark.org/">Cloudshark</a> which has the added benefit of decoding the capture as well. Only post the capture if you are happy that the contents can be made public.</p></div><div id="comment-18314-info" class="comment-info"><span class="comment-age">(05 Feb '13, 02:15)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-18311" class="comment-tools"></div><div class="clear"></div><div id="comment-18311-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

