+++
type = "question"
title = "How can I decode email message from Outlook 2010 to Exchange server 2003?"
description = '''I am trying to capture and decode email message body (client App is Outlook 2010 and server is Exchange server 2003), wireshark capture packets with &quot;DCERPC&quot; protocol and reassemble data packets, but can&#x27;t decode App protocol, that is MAPI. How can i decode App data from DCERPC Stub-Data field?'''
date = "2012-01-15T01:20:00Z"
lastmod = "2012-01-15T01:20:00Z"
weight = 8390
keywords = [ "outlook2010", "exchangeserver2003", "mapi", "dcerpc" ]
aliases = [ "/questions/8390" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How can I decode email message from Outlook 2010 to Exchange server 2003?](/questions/8390/how-can-i-decode-email-message-from-outlook-2010-to-exchange-server-2003)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8390-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8390-score" class="post-score" title="current number of votes">0</div><span id="post-8390-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to capture and decode email message body (client App is Outlook 2010 and server is Exchange server 2003), wireshark capture packets with "DCERPC" protocol and reassemble data packets, but can't decode App protocol, that is <strong>MAPI</strong>. How can i decode App data from DCERPC Stub-Data field?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-outlook2010" rel="tag" title="see questions tagged &#39;outlook2010&#39;">outlook2010</span> <span class="post-tag tag-link-exchangeserver2003" rel="tag" title="see questions tagged &#39;exchangeserver2003&#39;">exchangeserver2003</span> <span class="post-tag tag-link-mapi" rel="tag" title="see questions tagged &#39;mapi&#39;">mapi</span> <span class="post-tag tag-link-dcerpc" rel="tag" title="see questions tagged &#39;dcerpc&#39;">dcerpc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '12, 01:20</strong></p><img src="https://secure.gravatar.com/avatar/fb06349a79ad05caab9198a54249daf8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mohammadmot&#39;s gravatar image" /><p><span>mohammadmot</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mohammadmot has no accepted answers">0%</span></p></div></div><div id="comments-container-8390" class="comments-container"></div><div id="comment-tools-8390" class="comment-tools"></div><div class="clear"></div><div id="comment-8390-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

