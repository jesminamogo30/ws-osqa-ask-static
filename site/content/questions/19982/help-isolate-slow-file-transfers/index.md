+++
type = "question"
title = "help Isolate slow file transfers"
description = '''We are facing slow file transfer issue with Lync 2010 client. Whenever two peer to Peer clients they share file rate at which file transfer takes place is 2 mbps But if we try to share same file using Skype we get 15 mbps speed. need your help how we can isolate why Lync is slown in file sharing as ...'''
date = "2013-03-31T22:06:00Z"
lastmod = "2013-04-08T19:58:00Z"
weight = 19982
keywords = [ "wireshark" ]
aliases = [ "/questions/19982" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [help Isolate slow file transfers](/questions/19982/help-isolate-slow-file-transfers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19982-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19982-score" class="post-score" title="current number of votes">0</div><span id="post-19982-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We are facing slow file transfer issue with Lync 2010 client. Whenever two peer to Peer clients they share file rate at which file transfer takes place is 2 mbps But if we try to share same file using Skype we get 15 mbps speed.</p><p>need your help how we can isolate why Lync is slown in file sharing as compared to Skype.</p><p>Thanks a lot.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Mar '13, 22:06</strong></p><img src="https://secure.gravatar.com/avatar/6615a61d69b703d89076bb0f18342bbf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="m_1607&#39;s gravatar image" /><p><span>m_1607</span><br />
<span class="score" title="35 reputation points">35</span><span title="12 badges"><span class="badge1">●</span><span class="badgecount">12</span></span><span title="13 badges"><span class="silver">●</span><span class="badgecount">13</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="m_1607 has no accepted answers">0%</span></p></div></div><div id="comments-container-19982" class="comments-container"><span id="20213"></span><div id="comment-20213" class="comment"><div id="post-20213-score" class="comment-score"></div><div class="comment-text"><p>can you please add some information about your network?</p><ul><li>where are the two systems located (same network, behind a firewall/vpn, etc.) while they share a file via Lync and Skype</li><li>How did you measure the throughput?</li></ul></div><div id="comment-20213-info" class="comment-info"><span class="comment-age">(08 Apr '13, 16:03)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="20227"></span><div id="comment-20227" class="comment"><div id="post-20227-score" class="comment-score"></div><div class="comment-text"><p>Thet are on same subnet on same switch.</p><p>I measure output through software called DU meter,</p></div><div id="comment-20227-info" class="comment-info"><span class="comment-age">(08 Apr '13, 19:58)</span> <span class="comment-user userinfo">m_1607</span></div></div></div><div id="comment-tools-19982" class="comment-tools"></div><div class="clear"></div><div id="comment-19982-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

