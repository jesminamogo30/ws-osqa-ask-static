+++
type = "question"
title = "Windows7 traffic on TCP destination port 3261"
description = '''Hi all. Using Wireshark on a Win7 workstation I realize that there are network traffics from the host (192.168.1.84) to a apparently unknow host that has the IP address 192.168.200.200 The picture I posted it&#x27;s from the Win7 workstation (192.168.1.84) where I started Wireshark. I wish to find WHICH ...'''
date = "2016-09-11T06:36:00Z"
lastmod = "2016-09-11T06:36:00Z"
weight = 55470
keywords = [ "traffic-analysis", "windows7" ]
aliases = [ "/questions/55470" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Windows7 traffic on TCP destination port 3261](/questions/55470/windows7-traffic-on-tcp-destination-port-3261)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55470-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55470-score" class="post-score" title="current number of votes">0</div><span id="post-55470-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all.</p><p>Using Wireshark on a Win7 workstation I realize that there are network traffics from the host (192.168.1.84) to a apparently unknow host that has the IP address 192.168.200.200</p><p>The picture I posted it's from the Win7 workstation (192.168.1.84) where I started Wireshark.</p><p>I wish to find WHICH program or service inside the Win7 workstation is attempting a TCP connection to 192.168.200.200...that is unreachable and (I think) inexistent !</p><p><img src="https://osqa-ask.wireshark.org/upfiles/oooooooooooooooo.png" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/netstat_output_filtering_3261_port_connection_aXEiyQY.png" alt="alt text" /></p><p><img src="https://osqa-ask.wireshark.org/upfiles/TCPView_output.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic-analysis" rel="tag" title="see questions tagged &#39;traffic-analysis&#39;">traffic-analysis</span> <span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Sep '16, 06:36</strong></p><img src="https://secure.gravatar.com/avatar/0224a73ebdf1be8cdd71717081e11bae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mdimuzio&#39;s gravatar image" /><p><span>mdimuzio</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mdimuzio has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Sep '16, 02:38</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></img></div></div><div id="comments-container-55470" class="comments-container"></div><div id="comment-tools-55470" class="comment-tools"></div><div class="clear"></div><div id="comment-55470-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

