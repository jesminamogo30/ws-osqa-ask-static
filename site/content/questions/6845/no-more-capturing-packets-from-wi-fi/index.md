+++
type = "question"
title = "No more capturing packets from Wi-Fi"
description = '''Look, I&#x27;ve got a problem. My computer is MacBook Pro 13/2011. I used wireshark for a long time and everything was perfect. I was catching packets of Wi-Fi, analyzing them. But something changed and now, when I use HTTP protocol filter I can see only m-search which doesn&#x27;t help me at all. I can see a...'''
date = "2011-10-11T06:20:00Z"
lastmod = "2011-10-11T06:20:00Z"
weight = 6845
keywords = [ "packets", "wifi", "http" ]
aliases = [ "/questions/6845" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No more capturing packets from Wi-Fi](/questions/6845/no-more-capturing-packets-from-wi-fi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6845-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6845-score" class="post-score" title="current number of votes">0</div><span id="post-6845-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Look, I've got a problem. My computer is MacBook Pro 13/2011. I used wireshark for a long time and everything was perfect. I was catching packets of Wi-Fi, analyzing them. But something changed and now, when I use HTTP protocol filter I can see only m-search which doesn't help me at all. I can see all packets but only of my own computer. Got Mac OS X 10.7.2 , I've installed Windows and tried wireshark there but the result was the same. Can somebody help me please? P.S. I couldn't find the answer to my question, but if it was already asked, please share the link</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Oct '11, 06:20</strong></p><img src="https://secure.gravatar.com/avatar/9d164546a0bc7db828b0dd23e80178f2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="iMalov&#39;s gravatar image" /><p><span>iMalov</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="iMalov has no accepted answers">0%</span></p></div></div><div id="comments-container-6845" class="comments-container"></div><div id="comment-tools-6845" class="comment-tools"></div><div class="clear"></div><div id="comment-6845-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

