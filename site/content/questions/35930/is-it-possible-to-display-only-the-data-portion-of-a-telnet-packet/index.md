+++
type = "question"
title = "Is it possible to display only the data portion of a telnet packet?"
description = '''Hello, I am working on an assignment while also getting a self-taught crash-course on wireshark filters. One of the things I want/need to do is find data captured via telnet while displaying the minimal amount of traffic required to capture it. What I have done is watched all network packets, found ...'''
date = "2014-09-02T08:25:00Z"
lastmod = "2014-09-02T08:25:00Z"
weight = 35930
keywords = [ "filter", "telnet", "display-filter" ]
aliases = [ "/questions/35930" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to display only the data portion of a telnet packet?](/questions/35930/is-it-possible-to-display-only-the-data-portion-of-a-telnet-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35930-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35930-score" class="post-score" title="current number of votes">0</div><span id="post-35930-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I am working on an assignment while also getting a self-taught crash-course on wireshark filters. One of the things I want/need to do is find data captured via telnet while displaying the minimal amount of traffic required to capture it. What I have done is watched all network packets, found their similarities, and made that my (very detailed) filter.</p><p>My question is this - is there a way to display only the data portion of a packet in wireshark? I know how to follow a TCP stream but that gives a lot of information that is unnecessary, and isn't the goal of the assignment. It seems like there would be a simple way to display only the data, but I have not found it yet.</p><p>Thanks for you time</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-telnet" rel="tag" title="see questions tagged &#39;telnet&#39;">telnet</span> <span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Sep '14, 08:25</strong></p><img src="https://secure.gravatar.com/avatar/3565196ad606b84aad0b04feac213426?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="armyofone&#39;s gravatar image" /><p><span>armyofone</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="armyofone has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Sep '14, 08:26</strong> </span></p></div></div><div id="comments-container-35930" class="comments-container"></div><div id="comment-tools-35930" class="comment-tools"></div><div class="clear"></div><div id="comment-35930-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

