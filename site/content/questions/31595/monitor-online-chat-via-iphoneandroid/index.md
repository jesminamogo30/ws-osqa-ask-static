+++
type = "question"
title = "monitor online chat via iphone/android"
description = '''Is there any way to monitor online chat or any traffic that passes through my network, from a iphone or android?'''
date = "2014-04-07T07:53:00Z"
lastmod = "2014-04-09T14:52:00Z"
weight = 31595
keywords = [ "wondering" ]
aliases = [ "/questions/31595" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [monitor online chat via iphone/android](/questions/31595/monitor-online-chat-via-iphoneandroid)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31595-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31595-score" class="post-score" title="current number of votes">0</div><span id="post-31595-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any way to monitor online chat or any traffic that passes through my network, from a iphone or android?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wondering" rel="tag" title="see questions tagged &#39;wondering&#39;">wondering</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Apr '14, 07:53</strong></p><img src="https://secure.gravatar.com/avatar/2a5de63b7d9a99d9c9eb5ee7d59422a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Demon117&#39;s gravatar image" /><p><span>Demon117</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Demon117 has no accepted answers">0%</span></p></div></div><div id="comments-container-31595" class="comments-container"></div><div id="comment-tools-31595" class="comment-tools"></div><div class="clear"></div><div id="comment-31595-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31600"></span>

<div id="answer-container-31600" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31600-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31600-score" class="post-score" title="current number of votes">0</div><span id="post-31600-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Where there's a will, there's a way.</p><p>Are the phones connected to your wireless access point?</p><p>If you don't have a device in your network that supports port mirroring you will need a hub or network tap behind the AP.</p><p>Please see <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">http://wiki.wireshark.org/CaptureSetup/Ethernet</a> for more details.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Apr '14, 12:19</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div></div><div id="comments-container-31600" class="comments-container"><span id="31673"></span><div id="comment-31673" class="comment"><div id="post-31673-score" class="comment-score"></div><div class="comment-text"><p>Thank you. But im a novice at this and need help. I've read the link you provided but still a little lost. I have a linksys E2500 wireless router. What will I need to do this.<br />
</p></div><div id="comment-31673-info" class="comment-info"><span class="comment-age">(09 Apr '14, 08:49)</span> <span class="comment-user userinfo">Demon117</span></div></div><span id="31690"></span><div id="comment-31690" class="comment"><div id="post-31690-score" class="comment-score"></div><div class="comment-text"><p>You need to buy some hardware. An old hub or a network tap or a switch that supports port mirroring. The hub will be the cheapest, but the port speed is usually 10MB so it can be a bottleneck if you have a high speed internet connection. An ASCII network diagram would look like this:</p><blockquote><p>Client ---wifi--- E2500 ---cable--- HUB ---cable--- ISP</p></blockquote><p>You then connect the PC with wireshark to the hub.</p><p>Before setting everything up you should also check if the chat software uses encryption.</p></div><div id="comment-31690-info" class="comment-info"><span class="comment-age">(09 Apr '14, 14:52)</span> <span class="comment-user userinfo">Roland</span></div></div></div><div id="comment-tools-31600" class="comment-tools"></div><div class="clear"></div><div id="comment-31600-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

