+++
type = "question"
title = "using separators with -o &quot;gui.column.format&quot;"
description = '''Is it possible to have separators using the method above?  Normally I like to use -T fields -E but when pcap&#x27;s are corrupt or ended in the middle of a packet transfer, the -T fields -E option doesn&#x27;t work. The only thing that I have seen work with those files are the -o &quot;gui.column.format&quot; option. T...'''
date = "2014-04-14T09:18:00Z"
lastmod = "2014-04-14T09:18:00Z"
weight = 31796
keywords = [ "pcap", "fields", "-o", "separator", "columns" ]
aliases = [ "/questions/31796" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [using separators with -o "gui.column.format"](/questions/31796/using-separators-with-o-guicolumnformat)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31796-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31796-score" class="post-score" title="current number of votes">0</div><span id="post-31796-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is it possible to have separators using the method above?</p><p>Normally I like to use -T fields -E but when pcap's are corrupt or ended in the middle of a packet transfer, the -T fields -E option doesn't work. The only thing that I have seen work with those files are the -o "gui.column.format" option. The only problem is that I write scripts to parse the data and without a separator the task gets rather tricky.</p><p>If anyone knows of a way let me know. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-fields" rel="tag" title="see questions tagged &#39;fields&#39;">fields</span> <span class="post-tag tag-link--o" rel="tag" title="see questions tagged &#39;-o&#39;">-o</span> <span class="post-tag tag-link-separator" rel="tag" title="see questions tagged &#39;separator&#39;">separator</span> <span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Apr '14, 09:18</strong></p><img src="https://secure.gravatar.com/avatar/fbc5b3a06e0bdd9408c2356da21566c5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Nefarii&#39;s gravatar image" /><p><span>Nefarii</span><br />
<span class="score" title="31 reputation points">31</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Nefarii has one accepted answer">100%</span></p></div></div><div id="comments-container-31796" class="comments-container"></div><div id="comment-tools-31796" class="comment-tools"></div><div class="clear"></div><div id="comment-31796-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

