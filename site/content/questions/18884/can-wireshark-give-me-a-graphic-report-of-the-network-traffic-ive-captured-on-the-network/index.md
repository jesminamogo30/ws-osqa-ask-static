+++
type = "question"
title = "Can wireshark give me a graphic report of the network traffic i&#x27;ve captured on the network?"
description = '''i&#x27;d like to know if wireshark has the capability to print out a graphic report of the network traffic that i&#x27;ve captured. Please advise... '''
date = "2013-02-26T11:01:00Z"
lastmod = "2013-02-26T11:26:00Z"
weight = 18884
keywords = [ "reports" ]
aliases = [ "/questions/18884" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can wireshark give me a graphic report of the network traffic i've captured on the network?](/questions/18884/can-wireshark-give-me-a-graphic-report-of-the-network-traffic-ive-captured-on-the-network)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18884-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18884-score" class="post-score" title="current number of votes">0</div><span id="post-18884-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i'd like to know if wireshark has the capability to print out a graphic report of the network traffic that i've captured. Please advise...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reports" rel="tag" title="see questions tagged &#39;reports&#39;">reports</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Feb '13, 11:01</strong></p><img src="https://secure.gravatar.com/avatar/7d9c000a8cbc85308c175e682d8dd71f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="theronin&#39;s gravatar image" /><p><span>theronin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="theronin has no accepted answers">0%</span></p></div></div><div id="comments-container-18884" class="comments-container"></div><div id="comment-tools-18884" class="comment-tools"></div><div class="clear"></div><div id="comment-18884-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18889"></span>

<div id="answer-container-18889" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18889-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18889-score" class="post-score" title="current number of votes">0</div><span id="post-18889-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, there are several ways to get a 'report'. It depends on what you need.</p><blockquote><p><code>Statistics -&gt; Summary</code><br />
<code>Statistics -&gt; Conversations</code><br />
<code>Statistics -&gt; IO Graph</code><br />
<code>Statistics -&gt; TCP StreamGraph</code><br />
</p></blockquote><p>There are also the reporting functions of tshark. See man page for the option -z.</p><blockquote><p><code>http://www.wireshark.org/docs/man-pages/tshark.html</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Feb '13, 11:26</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Feb '13, 11:29</strong> </span></p></div></div><div id="comments-container-18889" class="comments-container"></div><div id="comment-tools-18889" class="comment-tools"></div><div class="clear"></div><div id="comment-18889-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

