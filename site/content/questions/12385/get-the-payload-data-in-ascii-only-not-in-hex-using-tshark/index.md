+++
type = "question"
title = "get the payload data in ascii only not in hex using tshark"
description = '''Hi, I captured some traffic on my linux, and l want to be able using tshark to get the data in the ascii format with out the hex format. is there some flag i can use for that? (i only found -x which gives both ascii and hex) thanks'''
date = "2012-07-03T00:29:00Z"
lastmod = "2012-07-03T00:29:00Z"
weight = 12385
keywords = [ "ascii", "tshark" ]
aliases = [ "/questions/12385" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [get the payload data in ascii only not in hex using tshark](/questions/12385/get-the-payload-data-in-ascii-only-not-in-hex-using-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12385-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12385-score" class="post-score" title="current number of votes">0</div><span id="post-12385-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I captured some traffic on my linux, and l want to be able using tshark to get the data in the ascii format with out the hex format. is there some flag i can use for that? (i only found -x which gives both ascii and hex)</p><p>thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ascii" rel="tag" title="see questions tagged &#39;ascii&#39;">ascii</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '12, 00:29</strong></p><img src="https://secure.gravatar.com/avatar/cd32499a16a71ad8594db755a1d97faf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="silvik&#39;s gravatar image" /><p><span>silvik</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="silvik has no accepted answers">0%</span></p></div></div><div id="comments-container-12385" class="comments-container"></div><div id="comment-tools-12385" class="comment-tools"></div><div class="clear"></div><div id="comment-12385-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

