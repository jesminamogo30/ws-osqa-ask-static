+++
type = "question"
title = "what is lte hmi(human-machine interface) protocol?"
description = '''what is lte based hmi protocol name? what protocol use?'''
date = "2016-08-15T22:09:00Z"
lastmod = "2016-08-16T04:41:00Z"
weight = 54847
keywords = [ "protocol" ]
aliases = [ "/questions/54847" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [what is lte hmi(human-machine interface) protocol?](/questions/54847/what-is-lte-hmihuman-machine-interface-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54847-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54847-score" class="post-score" title="current number of votes">0</div><span id="post-54847-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what is lte based hmi protocol name? what protocol use?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-protocol" rel="tag" title="see questions tagged &#39;protocol&#39;">protocol</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Aug '16, 22:09</strong></p><img src="https://secure.gravatar.com/avatar/fdbc74c8e71fbf2dc5cdd7106c111174?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jungks3165&#39;s gravatar image" /><p><span class="suspended-user">jungks3165</span><br />
(suspended)<br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jungks3165 has no accepted answers">0%</span></p></div></div><div id="comments-container-54847" class="comments-container"><span id="54853"></span><div id="comment-54853" class="comment"><div id="post-54853-score" class="comment-score"></div><div class="comment-text"><p>What is this? A homework question?</p></div><div id="comment-54853-info" class="comment-info"><span class="comment-age">(16 Aug '16, 04:41)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-54847" class="comment-tools"></div><div class="clear"></div><div id="comment-54847-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

