+++
type = "question"
title = "BGAN protocol"
description = '''Does wireshark support BGAN (UDP transport)?'''
date = "2014-05-13T07:17:00Z"
lastmod = "2014-05-14T09:48:00Z"
weight = 32761
keywords = [ "bgan" ]
aliases = [ "/questions/32761" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [BGAN protocol](/questions/32761/bgan-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32761-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32761-score" class="post-score" title="current number of votes">0</div><span id="post-32761-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Does wireshark support BGAN (UDP transport)?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bgan" rel="tag" title="see questions tagged &#39;bgan&#39;">bgan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 May '14, 07:17</strong></p><img src="https://secure.gravatar.com/avatar/48d6c18308c16eb2b1729149aa2cf849?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="svy&#39;s gravatar image" /><p><span>svy</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="svy has no accepted answers">0%</span></p></div></div><div id="comments-container-32761" class="comments-container"><span id="32801"></span><div id="comment-32801" class="comment"><div id="post-32801-score" class="comment-score"></div><div class="comment-text"><p>Just out of curiosity: What is BGAN ? Broadband Global Area Network ?</p></div><div id="comment-32801-info" class="comment-info"><span class="comment-age">(14 May '14, 09:48)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-32761" class="comment-tools"></div><div class="clear"></div><div id="comment-32761-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32762"></span>

<div id="answer-container-32762" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32762-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32762-score" class="post-score" title="current number of votes">0</div><span id="post-32762-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No there is no dissector for this protocol. Is there a protocol specification publicly available?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 May '14, 07:48</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-32762" class="comments-container"><span id="32791"></span><div id="comment-32791" class="comment"><div id="post-32791-score" class="comment-score"></div><div class="comment-text"><p>I believe protocol specification is not publicly avaialble</p></div><div id="comment-32791-info" class="comment-info"><span class="comment-age">(14 May '14, 02:56)</span> <span class="comment-user userinfo">svy</span></div></div></div><div id="comment-tools-32762" class="comment-tools"></div><div class="clear"></div><div id="comment-32762-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

