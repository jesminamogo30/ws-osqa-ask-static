+++
type = "question"
title = "Wireshark doesnt reconize my wireless adapter Realtek"
description = '''Hello, I dont know what happens, but Wireshark doesnt reconize my wireless adapter Realtek RTL8188CE Wireless LAN 802.11n . My adapter is OK, I can access my access point and I can run others applications. Please, any hint? I installed WS version 1.6.7 and Winpcap 1.4.2. thanks! Claudia'''
date = "2012-04-16T20:26:00Z"
lastmod = "2012-04-17T09:02:00Z"
weight = 10206
keywords = [ "realtek" ]
aliases = [ "/questions/10206" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark doesnt reconize my wireless adapter Realtek](/questions/10206/wireshark-doesnt-reconize-my-wireless-adapter-realtek)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10206-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10206-score" class="post-score" title="current number of votes">0</div><span id="post-10206-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I dont know what happens, but Wireshark doesnt reconize my wireless adapter Realtek RTL8188CE Wireless LAN 802.11n . My adapter is OK, I can access my access point and I can run others applications. Please, any hint? I installed WS version 1.6.7 and Winpcap 1.4.2.</p><p>thanks!</p><p>Claudia</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-realtek" rel="tag" title="see questions tagged &#39;realtek&#39;">realtek</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Apr '12, 20:26</strong></p><img src="https://secure.gravatar.com/avatar/d692f060d34e20dd521706674f78d63b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="claudia1974&#39;s gravatar image" /><p><span>claudia1974</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="claudia1974 has no accepted answers">0%</span></p></div></div><div id="comments-container-10206" class="comments-container"></div><div id="comment-tools-10206" class="comment-tools"></div><div class="clear"></div><div id="comment-10206-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="10207"></span>

<div id="answer-container-10207" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10207-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10207-score" class="post-score" title="current number of votes">0</div><span id="post-10207-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Maybe <a href="http://ask.wireshark.org/questions/831/wireless-adaptor-not-seen-by-wireshark">this thread</a> has an answer for you?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Apr '12, 23:07</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-10207" class="comments-container"><span id="10216"></span><div id="comment-10216" class="comment"><div id="post-10216-score" class="comment-score"></div><div class="comment-text"><p>Hi Jasper, I´ll read this topic. Thanks for your hint!</p></div><div id="comment-10216-info" class="comment-info"><span class="comment-age">(17 Apr '12, 05:19)</span> <span class="comment-user userinfo">claudia1974</span></div></div><span id="10218"></span><div id="comment-10218" class="comment"><div id="post-10218-score" class="comment-score"></div><div class="comment-text"><p>In particular, <a href="http://ask.wireshark.org/questions/831/wireless-adaptor-not-seen-by-wireshark/832">this answer</a>, says that "new style" Wi-Fi adapter drivers cause the adapter to be reported as "Microsoft" drivers rather than, for example, a "Realtek RTL8188CE" driver - do you have an adapter that shows up with a description of "Microsoft"?</p></div><div id="comment-10218-info" class="comment-info"><span class="comment-age">(17 Apr '12, 09:02)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-10207" class="comment-tools"></div><div class="clear"></div><div id="comment-10207-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="10208"></span>

<div id="answer-container-10208" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10208-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10208-score" class="post-score" title="current number of votes">0</div><span id="post-10208-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Wireless LAN ... Winpcap</p></blockquote><p>Windows and 802.11 <a href="http://www.winpcap.org/misc/faq.htm#Q-16">is, unfortunately, a bad combination</a>; if you want to capture on a WLAN, either run an operating system other than Windows, use an <a href="http://www.riverbed.com/us/products/cascade/wireshark_enhancements/airpcap.php">AirPcap adapter</a>, or use a capture program that uses a different driver, such as <a href="http://www.microsoft.com/download/en/details.aspx?id=4865">Microsoft Network Monitor</a> or <a href="http://www.tamos.com/products/commwifi/">TamoSoft Commview for WiFi</a> or <a href="http://www.wildpackets.com/products/omnipeek_network_analyzer/wireless_network_analysis">WildPackets OmniPeek</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Apr '12, 23:10</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-10208" class="comments-container"><span id="10215"></span><div id="comment-10215" class="comment"><div id="post-10215-score" class="comment-score"></div><div class="comment-text"><p>Hello friend, thanks for your assistance, I´ll test other drive!</p></div><div id="comment-10215-info" class="comment-info"><span class="comment-age">(17 Apr '12, 05:19)</span> <span class="comment-user userinfo">claudia1974</span></div></div></div><div id="comment-tools-10208" class="comment-tools"></div><div class="clear"></div><div id="comment-10208-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

