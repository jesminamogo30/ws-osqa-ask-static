+++
type = "question"
title = "How to see more details in VOIP/graph analysis?"
description = '''Hi, when I open a graph analysis (flow) for a voip call it will display the SIP messages in the middle but with only appx. 20 characters per line. I can enlarge the whole window and also can move the right divider but the content of the middle part will always stay same and so will be difficult to r...'''
date = "2015-08-25T05:26:00Z"
lastmod = "2015-08-25T05:26:00Z"
weight = 45342
keywords = [ "graph", "analysys" ]
aliases = [ "/questions/45342" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to see more details in VOIP/graph analysis?](/questions/45342/how-to-see-more-details-in-voipgraph-analysis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45342-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45342-score" class="post-score" title="current number of votes">0</div><span id="post-45342-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, when I open a graph analysis (flow) for a voip call it will display the SIP messages in the middle but with only appx. 20 characters per line. I can enlarge the whole window and also can move the right divider but the content of the middle part will always stay same and so will be difficult to read (e.g. often strings are broken with ... at the end even the middle part does have enough space. Does this only happen for me? Is it possible to fix in future releases?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-analysys" rel="tag" title="see questions tagged &#39;analysys&#39;">analysys</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Aug '15, 05:26</strong></p><img src="https://secure.gravatar.com/avatar/3a4fc4a30a2f5ff383358f0509a832fd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="crossbike0815&#39;s gravatar image" /><p><span>crossbike0815</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="crossbike0815 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Aug '15, 05:30</strong> </span></p></div></div><div id="comments-container-45342" class="comments-container"></div><div id="comment-tools-45342" class="comment-tools"></div><div class="clear"></div><div id="comment-45342-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

