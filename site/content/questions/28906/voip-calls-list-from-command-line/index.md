+++
type = "question"
title = "VoIP calls List From Command Line"
description = '''Hi, I&#x27;m using Wireshark to sniff the network and detect the VoIP calls. Detected VoIP calls can be seen from GUI (Telephony-&amp;gt;VoIP Calls). Now I want to get this list from command line. I searched through wireshark documents, but couldn&#x27;t find a command to do that. I&#x27;m using the commands like tsha...'''
date = "2014-01-15T04:29:00Z"
lastmod = "2014-01-15T04:29:00Z"
weight = 28906
keywords = [ "sip", "voip" ]
aliases = [ "/questions/28906" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [VoIP calls List From Command Line](/questions/28906/voip-calls-list-from-command-line)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28906-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28906-score" class="post-score" title="current number of votes">0</div><span id="post-28906-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I'm using Wireshark to sniff the network and detect the VoIP calls. Detected VoIP calls can be seen from GUI (Telephony-&gt;VoIP Calls).</p><p>Now I want to get this list from command line. I searched through wireshark documents, but couldn't find a command to do that.</p><p>I'm using the commands like</p><p>tshark -r myFile -R "sip.CSeq.method eq INVITE"</p><p>Is there a command to show that voip call list from command line, or do i have to parse the outputs and create my own list?</p><p>Any help would be greatly appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '14, 04:29</strong></p><img src="https://secure.gravatar.com/avatar/0154f7521fe65ab8a0c68edad341e985?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="utq&#39;s gravatar image" /><p><span>utq</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="utq has no accepted answers">0%</span></p></div></div><div id="comments-container-28906" class="comments-container"></div><div id="comment-tools-28906" class="comment-tools"></div><div class="clear"></div><div id="comment-28906-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

