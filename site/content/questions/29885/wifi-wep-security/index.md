+++
type = "question"
title = "wifi wep security"
description = '''anyone know how to crack a wifi wep security?'''
date = "2014-02-15T06:00:00Z"
lastmod = "2014-02-15T09:30:00Z"
weight = 29885
keywords = [ "wifi", "wireshark" ]
aliases = [ "/questions/29885" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [wifi wep security](/questions/29885/wifi-wep-security)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29885-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29885-score" class="post-score" title="current number of votes">0</div><span id="post-29885-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>anyone know how to crack a wifi wep security?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '14, 06:00</strong></p><img src="https://secure.gravatar.com/avatar/1cd90cd1b855e701ea39059f887dd018?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="serbesamoves&#39;s gravatar image" /><p><span>serbesamoves</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="serbesamoves has no accepted answers">0%</span></p></div></div><div id="comments-container-29885" class="comments-container"></div><div id="comment-tools-29885" class="comment-tools"></div><div class="clear"></div><div id="comment-29885-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29886"></span>

<div id="answer-container-29886" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29886-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29886-score" class="post-score" title="current number of votes">1</div><span id="post-29886-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://www.wikihow.com/Break-WEP-Encryption">http://www.wikihow.com/Break-WEP-Encryption</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Feb '14, 06:02</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-29886" class="comments-container"><span id="29887"></span><div id="comment-29887" class="comment"><div id="post-29887-score" class="comment-score"></div><div class="comment-text"><p>is this working?</p></div><div id="comment-29887-info" class="comment-info"><span class="comment-age">(15 Feb '14, 06:12)</span> <span class="comment-user userinfo">serbesamoves</span></div></div><span id="29890"></span><div id="comment-29890" class="comment"><div id="post-29890-score" class="comment-score"></div><div class="comment-text"><p>Sure. The "BackTrack" live CD is now called "Kali", but breaking WEP is no rocket science.</p></div><div id="comment-29890-info" class="comment-info"><span class="comment-age">(15 Feb '14, 09:30)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-29886" class="comment-tools"></div><div class="clear"></div><div id="comment-29886-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

