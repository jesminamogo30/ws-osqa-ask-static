+++
type = "question"
title = "How do I turn on monitor mode in Mac OS X with Wireshark v1.99?"
description = '''How do I turn on monitor mode in Mac OS X with Wireshark v1.99?'''
date = "2015-06-04T12:55:00Z"
lastmod = "2015-06-05T03:12:00Z"
weight = 42897
keywords = [ "wireshark1.99", "macosx", "wlan", "monitor-mode", "capture-setup" ]
aliases = [ "/questions/42897" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How do I turn on monitor mode in Mac OS X with Wireshark v1.99?](/questions/42897/how-do-i-turn-on-monitor-mode-in-mac-os-x-with-wireshark-v199)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42897-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42897-score" class="post-score" title="current number of votes">0</div><span id="post-42897-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I turn on monitor mode in Mac OS X with Wireshark v1.99?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark1.99" rel="tag" title="see questions tagged &#39;wireshark1.99&#39;">wireshark1.99</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-wlan" rel="tag" title="see questions tagged &#39;wlan&#39;">wlan</span> <span class="post-tag tag-link-monitor-mode" rel="tag" title="see questions tagged &#39;monitor-mode&#39;">monitor-mode</span> <span class="post-tag tag-link-capture-setup" rel="tag" title="see questions tagged &#39;capture-setup&#39;">capture-setup</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '15, 12:55</strong></p><img src="https://secure.gravatar.com/avatar/643016a5caf72098dc71453e85f884f4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Joe%20C&#39;s gravatar image" /><p><span>Joe C</span><br />
<span class="score" title="4 reputation points">4</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Joe C has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jun '15, 17:27</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-42897" class="comments-container"></div><div id="comment-tools-42897" class="comment-tools"></div><div class="clear"></div><div id="comment-42897-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42899"></span>

<div id="answer-container-42899" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42899-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42899-score" class="post-score" title="current number of votes">1</div><span id="post-42899-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Joe C has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><ol><li>Open capture dialog</li><li>Choose Wifi Interface</li><li>Then scroll to the right side until the column "Monitor Mode" appears double click the value in your interface row and choose enabled.</li><li><p>Then click on the start button.</p><p>And do not forget setting the Link Layer to Per Packet Info</p></li></ol><p><img src="https://osqa-ask.wireshark.org/upfiles/WiresharkInfo_a4xPXs5.png" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jun '15, 17:14</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>04 Jun '15, 17:16</strong> </span></p></div></div><div id="comments-container-42899" class="comments-container"><span id="42910"></span><div id="comment-42910" class="comment"><div id="post-42910-score" class="comment-score"></div><div class="comment-text"><p>There are no scroll bars so I did not see a monitor mode as an option. Thanks.</p></div><div id="comment-42910-info" class="comment-info"><span class="comment-age">(05 Jun '15, 03:12)</span> <span class="comment-user userinfo">Joe C</span></div></div></div><div id="comment-tools-42899" class="comment-tools"></div><div class="clear"></div><div id="comment-42899-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

