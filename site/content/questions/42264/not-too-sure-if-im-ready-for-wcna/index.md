+++
type = "question"
title = "Not too sure if I&#x27;m ready for WCNA"
description = '''There just seems to be an immense lack of resources when compared to Cisco certs. Not much material to prepare with except for the huge book which I have and also some YouTube videos.  With the CCNA, I could just use Boson.com and some other sources for information. I have learned a lot in WireShark...'''
date = "2015-05-09T17:54:00Z"
lastmod = "2015-05-10T12:55:00Z"
weight = 42264
keywords = [ "wcna", "exam", "certification" ]
aliases = [ "/questions/42264" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Not too sure if I'm ready for WCNA](/questions/42264/not-too-sure-if-im-ready-for-wcna)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42264-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42264-score" class="post-score" title="current number of votes">0</div><span id="post-42264-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>There just seems to be an immense lack of resources when compared to Cisco certs. Not much material to prepare with except for the huge book which I have and also some YouTube videos.</p><p>With the CCNA, I could just use Boson.com and some other sources for information. I have learned a lot in WireShark, but there are still things I don't completely 100% understand, so I'm afraid to register for the exam. Sadly the WireShark school is only available to people who pass, whereas netacad is available to anyone interested.</p><p>How do I know if I'm ready?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wcna" rel="tag" title="see questions tagged &#39;wcna&#39;">wcna</span> <span class="post-tag tag-link-exam" rel="tag" title="see questions tagged &#39;exam&#39;">exam</span> <span class="post-tag tag-link-certification" rel="tag" title="see questions tagged &#39;certification&#39;">certification</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '15, 17:54</strong></p><img src="https://secure.gravatar.com/avatar/4784c5fb1a0142030d51a339706a456c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Beldum&#39;s gravatar image" /><p><span>Beldum</span><br />
<span class="score" title="49 reputation points">49</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="16 badges"><span class="bronze">●</span><span class="badgecount">16</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Beldum has no accepted answers">0%</span></p></div></div><div id="comments-container-42264" class="comments-container"></div><div id="comment-tools-42264" class="comment-tools"></div><div class="clear"></div><div id="comment-42264-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="42266"></span>

<div id="answer-container-42266" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42266-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42266-score" class="post-score" title="current number of votes">2</div><span id="post-42266-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The "Wireshark Network Analysis (2nd edition)" book covers the curriculum you'd be expected to know for it. If you're strong in that material you should be ok. The exam itself is pretty fair and straightforward in my opinion, at least compared to most other vendors I've gone through (Cisco, Oracle, Juniper, etc.).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 May '15, 18:53</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-42266" class="comments-container"><span id="42267"></span><div id="comment-42267" class="comment"><div id="post-42267-score" class="comment-score"></div><div class="comment-text"><p><span>@Quadratic</span>, how would you compare it to the CCNA? I want to be one of the cool kids (not a kid just joking) and pass the WCNA on my first try. But the thing is some wireshark files when I see like 80,000 packets I don't know where to start.</p></div><div id="comment-42267-info" class="comment-info"><span class="comment-age">(09 May '15, 19:41)</span> <span class="comment-user userinfo">Beldum</span></div></div><span id="42268"></span><div id="comment-42268" class="comment"><div id="post-42268-score" class="comment-score"></div><div class="comment-text"><p>Some protocols like HIP I don't fully 100% understand as well.</p></div><div id="comment-42268-info" class="comment-info"><span class="comment-age">(09 May '15, 19:41)</span> <span class="comment-user userinfo">Beldum</span></div></div><span id="42283"></span><div id="comment-42283" class="comment"><div id="post-42283-score" class="comment-score"></div><div class="comment-text"><p>Well, "where to start" is always going to depend on what you're trying to accomplish. There are a lot of common problems that would cause you to open a trace file, and you should have a logical methodology for approaching each of them. For common "needle in a haystack" problems with packet captures, if you can define the "needle" within the context of a display filter, or a single TCP session that you can zoom in on, etc., as long as you are objective-oriented about opening a packet capture and as long as you're familiar with the tools at your disposal to do these things within Wireshark, you should be ok.</p><p>For the CCNA comparison, I found it far easier than Cisco exams but it's hard to fairly compare them. I wrote the CCNA when I was pretty new to the industry, so it felt "harder" but it might have really just been "newer". I wrote the WCNA after spending years as a network planner, building probe systems, and having my eyes on packet bytes every day for years, so that exam was extremely straightforward to me but that's after using the tool every day.</p></div><div id="comment-42283-info" class="comment-info"><span class="comment-age">(10 May '15, 12:55)</span> <span class="comment-user userinfo">Quadratic</span></div></div></div><div id="comment-tools-42266" class="comment-tools"></div><div class="clear"></div><div id="comment-42266-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="42280"></span>

<div id="answer-container-42280" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42280-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42280-score" class="post-score" title="current number of votes">0</div><span id="post-42280-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You should read/understand the book Quadratic mentioned and for practice questions you can use "Wireshark Certified Network Analyst Exam Prep Guide (2nd Edition)". I also recommend watching all the <a href="http://sharkfest.wireshark.org/retrospective.html">Sharkfest</a> presentations.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 May '15, 07:49</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div></div><div id="comments-container-42280" class="comments-container"></div><div id="comment-tools-42280" class="comment-tools"></div><div class="clear"></div><div id="comment-42280-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

