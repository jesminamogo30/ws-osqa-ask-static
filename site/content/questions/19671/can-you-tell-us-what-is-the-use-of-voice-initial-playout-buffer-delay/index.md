+++
type = "question"
title = "Can you tell us what is the use of voice initial playout buffer delay?"
description = '''Can you tell us what is the use of voice initial playout buffer delay?'''
date = "2013-03-20T04:35:00Z"
lastmod = "2013-03-20T05:03:00Z"
weight = 19671
keywords = [ "buffer" ]
aliases = [ "/questions/19671" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Can you tell us what is the use of voice initial playout buffer delay?](/questions/19671/can-you-tell-us-what-is-the-use-of-voice-initial-playout-buffer-delay)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19671-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19671-score" class="post-score" title="current number of votes">0</div><span id="post-19671-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can you tell us what is the use of voice initial playout buffer delay?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-buffer" rel="tag" title="see questions tagged &#39;buffer&#39;">buffer</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '13, 04:35</strong></p><img src="https://secure.gravatar.com/avatar/e12d9d0c390836527ae1e83372d959a2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="archu&#39;s gravatar image" /><p><span>archu</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="archu has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Mar '13, 04:36</strong> </span></p></div></div><div id="comments-container-19671" class="comments-container"></div><div id="comment-tools-19671" class="comment-tools"></div><div class="clear"></div><div id="comment-19671-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19672"></span>

<div id="answer-container-19672" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19672-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19672-score" class="post-score" title="current number of votes">0</div><span id="post-19672-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It determines how long speech data is hold up before playout commences. This allows for speech packets to arrive 'late' without interfering with the playout of the speech, resulting in a better listening experience.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '13, 05:03</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-19672" class="comments-container"></div><div id="comment-tools-19672" class="comment-tools"></div><div class="clear"></div><div id="comment-19672-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

