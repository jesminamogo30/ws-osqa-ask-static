+++
type = "question"
title = "MIPv6 protocol: wireshark is showing malformed packet for mobility option - &quot;3GPP Specific PMIPv6 error code&quot;"
description = '''For MIPv6 protocol, wireshark is showing malformed packet when I try to send the mobility option - &quot;3GPP Specific PMIPv6 error code&quot; according to the specification: 3GPP TS 29.275 V10.7.0 (2013-03), Page 68 in any of the following messages: Proxy Binding Acknowledgement (PBA) or Binding Revocation A...'''
date = "2013-10-17T05:33:00Z"
lastmod = "2013-10-17T07:19:00Z"
weight = 26124
keywords = [ "mipv6" ]
aliases = [ "/questions/26124" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [MIPv6 protocol: wireshark is showing malformed packet for mobility option - "3GPP Specific PMIPv6 error code"](/questions/26124/mipv6-protocol-wireshark-is-showing-malformed-packet-for-mobility-option-3gpp-specific-pmipv6-error-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26124-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26124-score" class="post-score" title="current number of votes">0</div><span id="post-26124-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>For MIPv6 protocol, wireshark is showing malformed packet when I try to send the mobility option - "3GPP Specific PMIPv6 error code" according to the specification: 3GPP TS 29.275 V10.7.0 (2013-03), Page 68 in any of the following messages: Proxy Binding Acknowledgement (PBA) or Binding Revocation Acknowledgment (BRA). The packet is encoded exactly as described in the specification, but wireshark is showing the aforementioned mobility option as "malformed packet".</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mipv6" rel="tag" title="see questions tagged &#39;mipv6&#39;">mipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Oct '13, 05:33</strong></p><img src="https://secure.gravatar.com/avatar/db88b39e14133eadf405ae26ac7151a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Arindam&#39;s gravatar image" /><p><span>Arindam</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Arindam has no accepted answers">0%</span></p></div></div><div id="comments-container-26124" class="comments-container"></div><div id="comment-tools-26124" class="comment-tools"></div><div class="clear"></div><div id="comment-26124-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26128"></span>

<div id="answer-container-26128" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26128-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26128-score" class="post-score" title="current number of votes">1</div><span id="post-26128-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Allready fixed in <a href="http://anonsvn.wireshark.org/viewvc?view=revision&amp;revision=52218">http://anonsvn.wireshark.org/viewvc?view=revision&amp;revision=52218</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Oct '13, 07:19</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-26128" class="comments-container"></div><div id="comment-tools-26128" class="comment-tools"></div><div class="clear"></div><div id="comment-26128-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

