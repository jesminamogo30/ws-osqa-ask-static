+++
type = "question"
title = "packet analysis"
description = '''I have just started with wireshark can any one help me out please?? how can i figure out transport layer and application layer protocol&#x27;s of any particular packet?? '''
date = "2013-03-23T08:18:00Z"
lastmod = "2013-03-23T16:00:00Z"
weight = 19770
keywords = [ "analysis", "packet" ]
aliases = [ "/questions/19770" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [packet analysis](/questions/19770/packet-analysis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19770-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19770-score" class="post-score" title="current number of votes">0</div><span id="post-19770-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have just started with wireshark can any one help me out please??</p><p>how can i figure out transport layer and application layer protocol's of any particular packet??</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Mar '13, 08:18</strong></p><img src="https://secure.gravatar.com/avatar/0f19aa9efeeb7a1409b85d75ad0ca07c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ark&#39;s gravatar image" /><p><span>ark</span><br />
<span class="score" title="16 reputation points">16</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ark has no accepted answers">0%</span></p></div></div><div id="comments-container-19770" class="comments-container"></div><div id="comment-tools-19770" class="comment-tools"></div><div class="clear"></div><div id="comment-19770-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19775"></span>

<div id="answer-container-19775" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19775-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19775-score" class="post-score" title="current number of votes">0</div><span id="post-19775-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>In the packet list pane follow the column "Protocol" to figure out transport layer and application layer details of particular packet.Once you identify the packets of your interest you can go to packet details pane and dig further in to header/data details.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Mar '13, 09:48</strong></p><img src="https://secure.gravatar.com/avatar/2b038237e64839261fcc88e9fdef2b68?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="krishnayeddula&#39;s gravatar image" /><p><span>krishnayeddula</span><br />
<span class="score" title="629 reputation points">629</span><span title="35 badges"><span class="badge1">●</span><span class="badgecount">35</span></span><span title="41 badges"><span class="silver">●</span><span class="badgecount">41</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="krishnayeddula has 3 accepted answers">6%</span></p></div></div><div id="comments-container-19775" class="comments-container"><span id="19778"></span><div id="comment-19778" class="comment"><div id="post-19778-score" class="comment-score"></div><div class="comment-text"><p>thanq...!! which would be the best material to learn wireshark and any tutorial videos, please let me knew if u knew any</p></div><div id="comment-19778-info" class="comment-info"><span class="comment-age">(23 Mar '13, 15:20)</span> <span class="comment-user userinfo">ark</span></div></div><span id="19780"></span><div id="comment-19780" class="comment"><div id="post-19780-score" class="comment-score"></div><div class="comment-text"><p>There's a boatload of info on <a href="http://www.wireshark.org/docs/">http://www.wireshark.org/docs/</a></p></div><div id="comment-19780-info" class="comment-info"><span class="comment-age">(23 Mar '13, 16:00)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-19775" class="comment-tools"></div><div class="clear"></div><div id="comment-19775-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

