+++
type = "question"
title = "How  create WiresharkPortable installation kit ?"
description = '''Hi,  could you suggest how create installation kit for &quot;Wireshark Portable&quot;, please ? (I&#x27;m able to create &quot;ordinary&quot; installation by nmake /f Makefile.nmake packaging, however I very need &quot;portable&quot; version). Thank you a lot in advance '''
date = "2013-05-06T03:09:00Z"
lastmod = "2013-05-06T05:02:00Z"
weight = 20982
keywords = [ "portable" ]
aliases = [ "/questions/20982" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How create WiresharkPortable installation kit ?](/questions/20982/how-create-wiresharkportable-installation-kit)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20982-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20982-score" class="post-score" title="current number of votes">0</div><span id="post-20982-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, could you suggest how create installation kit for "Wireshark Portable", please ? (I'm able to create "ordinary" installation by nmake /f Makefile.nmake packaging, however I very need "portable" version). Thank you a lot in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-portable" rel="tag" title="see questions tagged &#39;portable&#39;">portable</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 May '13, 03:09</strong></p><img src="https://secure.gravatar.com/avatar/3799a296db769e662c5a90d85c6999c7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yuriy&#39;s gravatar image" /><p><span>Yuriy</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yuriy has no accepted answers">0%</span></p></div></div><div id="comments-container-20982" class="comments-container"></div><div id="comment-tools-20982" class="comment-tools"></div><div class="clear"></div><div id="comment-20982-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20983"></span>

<div id="answer-container-20983" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20983-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20983-score" class="post-score" title="current number of votes">1</div><span id="post-20983-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>nmake -f Makefile.nmake packaging_papps</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 May '13, 04:44</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-20983" class="comments-container"><span id="20984"></span><div id="comment-20984" class="comment"><div id="post-20984-score" class="comment-score"></div><div class="comment-text"><p>Thank you a lot !</p></div><div id="comment-20984-info" class="comment-info"><span class="comment-age">(06 May '13, 05:02)</span> <span class="comment-user userinfo">Yuriy</span></div></div></div><div id="comment-tools-20983" class="comment-tools"></div><div class="clear"></div><div id="comment-20983-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

