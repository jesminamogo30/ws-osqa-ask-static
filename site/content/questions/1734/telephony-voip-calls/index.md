+++
type = "question"
title = "Telephony ... VOIP calls"
description = '''When I select Telephony and then VOIP calls, I get a neat report of all calls, start and end time. But it does not allow me to save the results into a file for post processing - to calculate the time taken for each call etc. How to save the results to a file?'''
date = "2011-01-13T03:00:00Z"
lastmod = "2011-01-13T03:08:00Z"
weight = 1734
keywords = [ "voip" ]
aliases = [ "/questions/1734" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Telephony ... VOIP calls](/questions/1734/telephony-voip-calls)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1734-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1734-score" class="post-score" title="current number of votes">0</div><span id="post-1734-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I select Telephony and then VOIP calls, I get a neat report of all calls, start and end time. But it does not allow me to save the results into a file for post processing - to calculate the time taken for each call etc. How to save the results to a file?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jan '11, 03:00</strong></p><img src="https://secure.gravatar.com/avatar/b3e4e15ab930152f97eaa86ffe09f689?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sree&#39;s gravatar image" /><p><span>sree</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sree has no accepted answers">0%</span></p></div></div><div id="comments-container-1734" class="comments-container"></div><div id="comment-tools-1734" class="comment-tools"></div><div class="clear"></div><div id="comment-1734-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1736"></span>

<div id="answer-container-1736" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1736-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1736-score" class="post-score" title="current number of votes">0</div><span id="post-1736-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't believe you can. You want to add a feature request at http://bugs.wireshark.org</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Jan '11, 03:08</strong></p><img src="https://secure.gravatar.com/avatar/57fbbe2a1e14ccc2a681a28886e5a484?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="martyvis&#39;s gravatar image" /><p><span>martyvis</span><br />
<span class="score" title="891 reputation points">891</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="martyvis has 5 accepted answers">7%</span></p></div></div><div id="comments-container-1736" class="comments-container"></div><div id="comment-tools-1736" class="comment-tools"></div><div class="clear"></div><div id="comment-1736-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

