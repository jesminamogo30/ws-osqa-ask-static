+++
type = "question"
title = "Cannot capture SIP and MSRP"
description = '''Hi, started using OS X 10.9.2 (13C64) and I cannot capture anything else just IPv4... I tried several builds of wireshark but unsuccessfully. I did cofiguration with wireshark : Edit/Preference/Protocol/DLT_USER/User 2(DLT=149) : payload protocol : ip header size 122 /, but also doesn&#x27;t wokr.. I tri...'''
date = "2014-04-17T13:30:00Z"
lastmod = "2014-04-24T02:29:00Z"
weight = 31944
keywords = [ "ipv4os", "10.9.2sipmsrp", "x" ]
aliases = [ "/questions/31944" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot capture SIP and MSRP](/questions/31944/cannot-capture-sip-and-msrp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31944-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31944-score" class="post-score" title="current number of votes">0</div><span id="post-31944-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, started using OS X 10.9.2 (13C64) and I cannot capture anything else just IPv4... I tried several builds of wireshark but unsuccessfully. I did cofiguration with wireshark : Edit/Preference/Protocol/DLT_USER/User 2(DLT=149) : payload protocol : ip header size 122 /, but also doesn't wokr..</p><p>I tried to capture traces from iphone on 3G connection ... Please help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipv4os" rel="tag" title="see questions tagged &#39;ipv4os&#39;">ipv4os</span> <span class="post-tag tag-link-10.9.2sipmsrp" rel="tag" title="see questions tagged &#39;10.9.2sipmsrp&#39;">10.9.2sipmsrp</span> <span class="post-tag tag-link-x" rel="tag" title="see questions tagged &#39;x&#39;">x</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Apr '14, 13:30</strong></p><img src="https://secure.gravatar.com/avatar/facbe982b985c0064221f28a2f0c5bb9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gaitors&#39;s gravatar image" /><p><span>Gaitors</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gaitors has no accepted answers">0%</span></p></div></div><div id="comments-container-31944" class="comments-container"></div><div id="comment-tools-31944" class="comment-tools"></div><div class="clear"></div><div id="comment-31944-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32139"></span>

<div id="answer-container-32139" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32139-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32139-score" class="post-score" title="current number of votes">0</div><span id="post-32139-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I found an answer that ip header size should be 108 ! works for me!</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Apr '14, 02:29</strong></p><img src="https://secure.gravatar.com/avatar/facbe982b985c0064221f28a2f0c5bb9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gaitors&#39;s gravatar image" /><p><span>Gaitors</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gaitors has no accepted answers">0%</span></p></div></div><div id="comments-container-32139" class="comments-container"></div><div id="comment-tools-32139" class="comment-tools"></div><div class="clear"></div><div id="comment-32139-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

