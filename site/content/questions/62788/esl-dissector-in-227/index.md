+++
type = "question"
title = "ESL Dissector in 2.2.7"
description = '''A long time ago in a Wireshark far, far away... there used to be an ESL Dissector enable setting via Preferences for EtherCAT Stack Link. I can&#x27;t seem to find it in Wireshark 2.2.7, does it exist? '''
date = "2017-07-14T14:55:00Z"
lastmod = "2017-07-14T15:14:00Z"
weight = 62788
keywords = [ "esl", "ethercat", "wireshark" ]
aliases = [ "/questions/62788" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [ESL Dissector in 2.2.7](/questions/62788/esl-dissector-in-227)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62788-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62788-score" class="post-score" title="current number of votes">0</div><span id="post-62788-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>A long time ago in a Wireshark far, far away... there used to be an ESL Dissector enable setting via Preferences for EtherCAT Stack Link. I can't seem to find it in Wireshark 2.2.7, does it exist?</p><p><img src="https://infosys.beckhoff.com/content/1033/et2000/Images/gif/1309678347__Web.gif" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-esl" rel="tag" title="see questions tagged &#39;esl&#39;">esl</span> <span class="post-tag tag-link-ethercat" rel="tag" title="see questions tagged &#39;ethercat&#39;">ethercat</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jul '17, 14:55</strong></p><img src="https://secure.gravatar.com/avatar/02a7d4ad353a04926abc25f7b4cc7762?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="0w8_States&#39;s gravatar image" /><p><span>0w8_States</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="0w8_States has no accepted answers">0%</span></p></img></div></div><div id="comments-container-62788" class="comments-container"></div><div id="comment-tools-62788" class="comment-tools"></div><div class="clear"></div><div id="comment-62788-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="62791"></span>

<div id="answer-container-62791" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62791-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62791-score" class="post-score" title="current number of votes">0</div><span id="post-62791-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="0w8_States has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look in the menu Analyze | Enabled protocols... There you'll find the protocol and the switch to enable it, as it should be, not in the preferences.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jul '17, 15:14</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-62791" class="comments-container"></div><div id="comment-tools-62791" class="comment-tools"></div><div class="clear"></div><div id="comment-62791-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

