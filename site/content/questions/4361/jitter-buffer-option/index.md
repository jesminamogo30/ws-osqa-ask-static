+++
type = "question"
title = "jitter buffer option"
description = '''I am simulating VoIP calls and capture traffic from the caller side. I cannot find the jitter buffer parameter in the SDP packets exchanged.'''
date = "2011-06-03T03:33:00Z"
lastmod = "2011-06-06T08:38:00Z"
weight = 4361
keywords = [ "buffer", "sdp", "jitter", "voip" ]
aliases = [ "/questions/4361" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [jitter buffer option](/questions/4361/jitter-buffer-option)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4361-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4361-score" class="post-score" title="current number of votes">0</div><span id="post-4361-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am simulating VoIP calls and capture traffic from the caller side. I cannot find the jitter buffer parameter in the SDP packets exchanged.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-buffer" rel="tag" title="see questions tagged &#39;buffer&#39;">buffer</span> <span class="post-tag tag-link-sdp" rel="tag" title="see questions tagged &#39;sdp&#39;">sdp</span> <span class="post-tag tag-link-jitter" rel="tag" title="see questions tagged &#39;jitter&#39;">jitter</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jun '11, 03:33</strong></p><img src="https://secure.gravatar.com/avatar/510e2d19e5cf4596d2afb59467c68c6f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lefkothea%20Vaitsi&#39;s gravatar image" /><p><span>Lefkothea Va...</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lefkothea Vaitsi has no accepted answers">0%</span></p></div></div><div id="comments-container-4361" class="comments-container"></div><div id="comment-tools-4361" class="comment-tools"></div><div class="clear"></div><div id="comment-4361-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4371"></span>

<div id="answer-container-4371" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4371-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4371-score" class="post-score" title="current number of votes">1</div><span id="post-4371-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://tools.ietf.org/html/rfc4566">RFC 4566</a>, there are no jitter buffer parameters in SDP. You may find relevant information in <a href="http://tools.ietf.org/html/rfc3550#page-42">RTCP</a> packets.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jun '11, 06:39</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-4371" class="comments-container"><span id="4407"></span><div id="comment-4407" class="comment"><div id="post-4407-score" class="comment-score"></div><div class="comment-text"><p>Thank you for the answer I will have a look at these RFCs</p></div><div id="comment-4407-info" class="comment-info"><span class="comment-age">(06 Jun '11, 08:38)</span> <span class="comment-user userinfo">Lefkothea Va...</span></div></div></div><div id="comment-tools-4371" class="comment-tools"></div><div class="clear"></div><div id="comment-4371-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

