+++
type = "question"
title = "possible to only install Wireshark legacy during silent install ?"
description = '''is it possible to only install Wireshark legacy during an silent install? the reason i ask is since i wanted to created an chocolatey script for Wireshark legacy, so people would have the option of installing the legacy interface if they choose to. Thanks '''
date = "2016-09-23T13:43:00Z"
lastmod = "2016-09-23T13:43:00Z"
weight = 55777
keywords = [ "gtk", "silent", "tshark", "wireshark" ]
aliases = [ "/questions/55777" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [possible to only install Wireshark legacy during silent install ?](/questions/55777/possible-to-only-install-wireshark-legacy-during-silent-install)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55777-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55777-score" class="post-score" title="current number of votes">0</div><span id="post-55777-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>is it possible to only install Wireshark legacy during an silent install? the reason i ask is since i wanted to created an chocolatey script for Wireshark legacy, so people would have the option of installing the legacy interface if they choose to.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtk" rel="tag" title="see questions tagged &#39;gtk&#39;">gtk</span> <span class="post-tag tag-link-silent" rel="tag" title="see questions tagged &#39;silent&#39;">silent</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Sep '16, 13:43</strong></p><img src="https://secure.gravatar.com/avatar/83a4ceb541517c850c4d2993ada76df8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Dedsec1&#39;s gravatar image" /><p><span>Dedsec1</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Dedsec1 has no accepted answers">0%</span></p></div></div><div id="comments-container-55777" class="comments-container"></div><div id="comment-tools-55777" class="comment-tools"></div><div class="clear"></div><div id="comment-55777-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

