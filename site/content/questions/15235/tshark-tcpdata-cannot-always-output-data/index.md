+++
type = "question"
title = "tshark: tcp.data cannot always output data"
description = '''I captured 802.11 trace with both MAC and network layer data. I use tshark to output the packet detail data tshark -T fields -e frame.number -e wlan.sa -e wlan.da -e tcp.data But, for some packets, the tcp.data can output the data content, but not all of them. I cannot figure it out. I can use wires...'''
date = "2012-10-24T18:56:00Z"
lastmod = "2012-10-24T18:56:00Z"
weight = 15235
keywords = [ "tcp.data", "tshark" ]
aliases = [ "/questions/15235" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [tshark: tcp.data cannot always output data](/questions/15235/tshark-tcpdata-cannot-always-output-data)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15235-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15235-score" class="post-score" title="current number of votes">0</div><span id="post-15235-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I captured 802.11 trace with both MAC and network layer data.</p><p>I use tshark to output the packet detail data</p><p>tshark -T fields -e frame.number -e <a href="http://wlan.sa">wlan.sa</a> -e wlan.da -e tcp.data</p><p>But, for some packets, the tcp.data can output the data content, but not all of them. I cannot figure it out.</p><p>I can use wireshark to browse the trace and check the data with no problem at all.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp.data" rel="tag" title="see questions tagged &#39;tcp.data&#39;">tcp.data</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Oct '12, 18:56</strong></p><img src="https://secure.gravatar.com/avatar/1e45625a30c4031868d0842ba61edc19?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Xian&#39;s gravatar image" /><p><span>Xian</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Xian has no accepted answers">0%</span></p></div></div><div id="comments-container-15235" class="comments-container"></div><div id="comment-tools-15235" class="comment-tools"></div><div class="clear"></div><div id="comment-15235-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

