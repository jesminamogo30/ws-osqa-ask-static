+++
type = "question"
title = "Wireshark &quot;No Font Found&quot; Solaris 8"
description = '''Hi, I am facing the below error after installing Wireshark on Solaris 8. Please help me in resolving this error. Thanks &amp;amp; Regards, Pacchi usr/local/bin/wireshark -f No fonts found; this probably means that the fontconfig library is not correctly configured. You may need to edit the fonts.conf co...'''
date = "2010-11-10T11:47:00Z"
lastmod = "2010-11-15T09:45:00Z"
weight = 902
keywords = [ "wireshark" ]
aliases = [ "/questions/902" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark "No Font Found" Solaris 8](/questions/902/wireshark-no-font-found-solaris-8)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-902-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-902-score" class="post-score" title="current number of votes">0</div><span id="post-902-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am facing the below error after installing Wireshark on Solaris 8. Please help me in resolving this error.</p><p>Thanks &amp; Regards, Pacchi</p><p>usr/local/bin/wireshark -f No fonts found; this probably means that the fontconfig library is not correctly configured. You may need to edit the fonts.conf configuration file. More information about fontconfig can be found in the fontconfig(3) manual page and on http://fontconfig.org</p><h1 id="usrlocalbinwireshark--v">/usr/local/bin/wireshark -v</h1><p>wireshark 1.2.10</p><p>Copyright 1998-2010 Gerald Combs <span><span class="__cf_email__" data-cfemail="e88f8d9a89848ca89f819a8d9b80899a83c6879a8f">[email protected]</span></span> and contributors. This is free software; see the source for copying conditions. There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</p><p>Compiled with GTK+ 2.12.0, (32-bit) with GLib 2.20.4, with libpcap 1.1.1, with libz 1.2.5, without POSIX capabilities, with libpcre 8.10, without SMI, without c-ares, with ADNS, without Lua, with GnuTLS 1.7.11, with Gcrypt 1.4.5, without Kerberos, with GeoIP, without PortAudio, without AirPcap.</p><p>Running on SunOS 5.8, with libpcap version 1.1.1, GnuTLS 1.4.1, Gcrypt 1.2.4.</p><p>Built using gcc 3.4.6.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Nov '10, 11:47</strong></p><img src="https://secure.gravatar.com/avatar/4eab05a810f8217d82cc72e49c7c4d3e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pacchie&#39;s gravatar image" /><p><span>pacchie</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pacchie has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Nov '10, 07:49</strong> </span></p></div></div><div id="comments-container-902" class="comments-container"><span id="958"></span><div id="comment-958" class="comment"><div id="post-958-score" class="comment-score"></div><div class="comment-text"><p>What happens when you run "gtk-demo"?</p></div><div id="comment-958-info" class="comment-info"><span class="comment-age">(15 Nov '10, 09:45)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-902" class="comment-tools"></div><div class="clear"></div><div id="comment-902-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

