+++
type = "question"
title = "EtherCAT datagram datas (ecat.data) limited view in packet details. How to increase?"
description = '''Hi All, I have to handle EtherCAT datagrams, which include ecat.data portions. I need to export this ecat.data portions into csv. files, but they are limited in the Packet Details Pane to 96 byte. All datas are shown in the Packet Bytes Pane (no limitation). I need to export several telegrams, so I ...'''
date = "2015-08-03T03:27:00Z"
lastmod = "2015-08-03T03:27:00Z"
weight = 44760
keywords = [ "ethercat" ]
aliases = [ "/questions/44760" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [EtherCAT datagram datas (ecat.data) limited view in packet details. How to increase?](/questions/44760/ethercat-datagram-datas-ecatdata-limited-view-in-packet-details-how-to-increase)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44760-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44760-score" class="post-score" title="current number of votes">0</div><span id="post-44760-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All,</p><p>I have to handle EtherCAT datagrams, which include ecat.data portions. I need to export this ecat.data portions into csv. files, but they are limited in the Packet Details Pane to 96 byte. All datas are shown in the Packet Bytes Pane (no limitation). I need to export several telegrams, so I can not export each PAcket Byte manually. How can I increase the number of bytes shown in the Packet Details Pane?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ethercat" rel="tag" title="see questions tagged &#39;ethercat&#39;">ethercat</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '15, 03:27</strong></p><img src="https://secure.gravatar.com/avatar/08486dc497bd134c031585eedd1ea81c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JMSCH&#39;s gravatar image" /><p><span>JMSCH</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JMSCH has no accepted answers">0%</span></p></div></div><div id="comments-container-44760" class="comments-container"></div><div id="comment-tools-44760" class="comment-tools"></div><div class="clear"></div><div id="comment-44760-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

