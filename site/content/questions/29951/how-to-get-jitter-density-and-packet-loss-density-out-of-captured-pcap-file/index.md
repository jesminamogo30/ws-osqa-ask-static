+++
type = "question"
title = "How to get Jitter density and Packet Loss Density out of captured pcap file?"
description = '''I have captured Pcap files. I want to know what is Jitter Density and Packet Loss Density. Can some one share Method / script to analyze this? Also explain how exactly we can get this.'''
date = "2014-02-17T22:20:00Z"
lastmod = "2014-02-18T00:08:00Z"
weight = 29951
keywords = [ "pcap", "jitter", "tshark" ]
aliases = [ "/questions/29951" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to get Jitter density and Packet Loss Density out of captured pcap file?](/questions/29951/how-to-get-jitter-density-and-packet-loss-density-out-of-captured-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29951-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29951-score" class="post-score" title="current number of votes">1</div><span id="post-29951-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured Pcap files. I want to know what is Jitter Density and Packet Loss Density. Can some one share Method / script to analyze this? Also explain how exactly we can get this.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-jitter" rel="tag" title="see questions tagged &#39;jitter&#39;">jitter</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '14, 22:20</strong></p><img src="https://secure.gravatar.com/avatar/6ae04f88f6d16f23f2751ae8df45d1f8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ravikumar%20Mantripragada&#39;s gravatar image" /><p><span>Ravikumar Ma...</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ravikumar Mantripragada has no accepted answers">0%</span></p></div></div><div id="comments-container-29951" class="comments-container"><span id="29958"></span><div id="comment-29958" class="comment"><div id="post-29958-score" class="comment-score"></div><div class="comment-text"><p>By jitter <strong>density</strong> do you mean the statistical density function?</p></div><div id="comment-29958-info" class="comment-info"><span class="comment-age">(18 Feb '14, 00:08)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29951" class="comment-tools"></div><div class="clear"></div><div id="comment-29951-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

