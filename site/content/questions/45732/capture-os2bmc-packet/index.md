+++
type = "question"
title = "Capture OS2BMC Packet"
description = '''Hi Everyone! Hope this message finds you all at your best. I need a help ... how do I capture OS2BMC packet in my network? How do I know which MAC ID (NIC Card) is sending this packet? Regards, Tanjim'''
date = "2015-09-09T02:18:00Z"
lastmod = "2015-09-09T02:18:00Z"
weight = 45732
keywords = [ "os2bmc" ]
aliases = [ "/questions/45732" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Capture OS2BMC Packet](/questions/45732/capture-os2bmc-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45732-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45732-score" class="post-score" title="current number of votes">0</div><span id="post-45732-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Everyone!</p><p>Hope this message finds you all at your best.</p><p>I need a help ... how do I capture OS2BMC packet in my network? How do I know which MAC ID (NIC Card) is sending this packet?</p><p>Regards, Tanjim</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-os2bmc" rel="tag" title="see questions tagged &#39;os2bmc&#39;">os2bmc</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '15, 02:18</strong></p><img src="https://secure.gravatar.com/avatar/44f51ebb9725d438148ca703d6a610fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zeead&#39;s gravatar image" /><p><span>zeead</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zeead has no accepted answers">0%</span></p></div></div><div id="comments-container-45732" class="comments-container"></div><div id="comment-tools-45732" class="comment-tools"></div><div class="clear"></div><div id="comment-45732-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

