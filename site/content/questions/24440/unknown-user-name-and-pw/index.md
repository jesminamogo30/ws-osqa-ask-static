+++
type = "question"
title = "Unknown user name and P/W"
description = '''I have an IP camera (IP-06-3) it is hard wired to my router when I connect to it using its IP address it asks for a user name and password I was given the camera so I do not know the answers.  Using Wireshark Is it possible to discover the answers to the log on details or remove it so that I can acc...'''
date = "2013-09-06T15:51:00Z"
lastmod = "2013-12-09T15:25:00Z"
weight = 24440
keywords = [ "logon" ]
aliases = [ "/questions/24440" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Unknown user name and P/W](/questions/24440/unknown-user-name-and-pw)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24440-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24440-score" class="post-score" title="current number of votes">0</div><span id="post-24440-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have an IP camera (IP-06-3) it is hard wired to my router when I connect to it using its IP address it asks for a user name and password I was given the camera so I do not know the answers. Using Wireshark Is it possible to discover the answers to the log on details or remove it so that I can access the camera. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-logon" rel="tag" title="see questions tagged &#39;logon&#39;">logon</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Sep '13, 15:51</strong></p><img src="https://secure.gravatar.com/avatar/aee24b4ce5e92d8fc91b0576912c3c9e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lesmilton1951&#39;s gravatar image" /><p><span>Lesmilton1951</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lesmilton1951 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Sep '13, 15:56</strong> </span></p></div></div><div id="comments-container-24440" class="comments-container"><span id="24536"></span><div id="comment-24536" class="comment"><div id="post-24536-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I have an IP camera (IP-06-3) <strong>it is hard wired to my router</strong></p></blockquote><p>what does that mean? Is the camera controlled by the router in some way?</p><p>If yes, then Wireshark may help to get access to the camera, if the communication between router and camera is not encrypted.</p></div><div id="comment-24536-info" class="comment-info"><span class="comment-age">(10 Sep '13, 14:56)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="24548"></span><div id="comment-24548" class="comment"><div id="post-24548-score" class="comment-score"></div><div class="comment-text"><p>Hi Thank you for looking at my question, by hard wired I mean the camera is connected to my router by a cable that is plugged into my router and into the camera as apposed to connected by wireless. The camera is still controled by computer.</p></div><div id="comment-24548-info" class="comment-info"><span class="comment-age">(10 Sep '13, 23:47)</span> <span class="comment-user userinfo">Lesmilton1951</span></div></div><span id="24578"></span><div id="comment-24578" class="comment"><div id="post-24578-score" class="comment-score"></div><div class="comment-text"><blockquote><p>The camera is still controled by computer.</p></blockquote><p>O.K. then see the answer of <span>@cmaynard</span>.</p></div><div id="comment-24578-info" class="comment-info"><span class="comment-age">(11 Sep '13, 09:10)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-24440" class="comment-tools"></div><div class="clear"></div><div id="comment-24440-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="24441"></span>

<div id="answer-container-24441" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24441-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24441-score" class="post-score" title="current number of votes">2</div><span id="post-24441-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, I don't think Wireshark will help you here. I think you're better off trying to review the user manual to what the default username and password is and try that. If that fails, there may be a way to reset it back to its factory defaults and then go from there.</p><p>From documents posted at <a href="http://www.shixin-china.com/1074.html">http://www.shixin-china.com/1074.html</a>, it looks like the default username/password might be either admin/admin or admin and no password at all. I did not see a way to reset it to factory defaults, but I may have missed it. If all else fails, you could try to <a href="http://www.shixin-china.com/604.html">contact</a> the manufacturer.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Sep '13, 17:07</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-24441" class="comments-container"><span id="24535"></span><div id="comment-24535" class="comment"><div id="post-24535-score" class="comment-score"></div><div class="comment-text"><p>Hi Chris thanks a lot for taking the time to look into my question. Les</p></div><div id="comment-24535-info" class="comment-info"><span class="comment-age">(10 Sep '13, 14:27)</span> <span class="comment-user userinfo">Lesmilton1951</span></div></div><span id="24540"></span><div id="comment-24540" class="comment"><div id="post-24540-score" class="comment-score"></div><div class="comment-text"><p>You're welcome, but did you really mean to award me 10 reputation points? That leaves you with only 1. I feel like you should keep the points, but unfortunately I don't know how to give them back.</p></div><div id="comment-24540-info" class="comment-info"><span class="comment-age">(10 Sep '13, 15:22)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="24549"></span><div id="comment-24549" class="comment"><div id="post-24549-score" class="comment-score"></div><div class="comment-text"><p>I am a complete newbe to wire shark and did not understand the points workings. Hay Ho Live and Learn</p></div><div id="comment-24549-info" class="comment-info"><span class="comment-age">(10 Sep '13, 23:48)</span> <span class="comment-user userinfo">Lesmilton1951</span></div></div><span id="24575"></span><div id="comment-24575" class="comment"><div id="post-24575-score" class="comment-score"></div><div class="comment-text"><p>Please add a simple answer below so I can award you your points back. I will then delete your answer and some of these comments afterwards.</p><p>You're not alone in the confusion/misunderstanding about awarding points. We might add a note about it on the FAQ page, but I predict that most people wouldn't read it, so my preference would be to simply get rid of it altogether.</p></div><div id="comment-24575-info" class="comment-info"><span class="comment-age">(11 Sep '13, 06:57)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="24583"></span><div id="comment-24583" class="comment"><div id="post-24583-score" class="comment-score"></div><div class="comment-text"><p>The latest firmwear for this camera is 3.3.CV91.NOB9722.bin</p></div><div id="comment-24583-info" class="comment-info"><span class="comment-age">(11 Sep '13, 10:20)</span> <span class="comment-user userinfo">Lesmilton1951</span></div></div></div><div id="comment-tools-24441" class="comment-tools"></div><div class="clear"></div><div id="comment-24441-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="27963"></span>

<div id="answer-container-27963" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27963-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27963-score" class="post-score" title="current number of votes">0</div><span id="post-27963-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There should be on username for administrator and its password on sticker at bottom of camera. "admin" and some 8-digit hexnumber, example admin and 021abffe.</p><p>There is a same sticker inside case. However these admin and 021abffe (example) may not work if administrator username has changed from "admin" to something or his password has changed. Then it is possible to reset admin/password to these defaults using utility and upgrade firmware. It can also be "upgrade" to same firmware version again. It may ask "device password" and this is passwd from sticker or the one you was told.</p><p>The "utility" and firmware can be found <a href="http://www.shixin-china.com/1074.html">http://www.shixin-china.com/1074.html</a> (has newer firmware) or <a href="http://www.icamview.co.uk/orderonline/Downloads.htm.">http://www.icamview.co.uk/orderonline/Downloads.htm.</a> (has newer "utility")</p><p>Be careful choosing the right firmware. Utility shows cameras current firmware.</p><p>I had same kind of situation with ip-06a except i was not given any username/passwd. Those from sticker did not work. I tried to upgrade firmware to same version which device already had and after that those admin/passwd from sticker worked.</p><p>If there is "device password" changed or used it may be not possible to do anything from outside. Then one option is read firmware from hardware, do some hexediting and write firmware back. Or write new firmware to chip by hardware programmer. These may not work if passwd is in different place.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Dec '13, 15:25</strong></p><img src="https://secure.gravatar.com/avatar/a22ec72d5af50e2dd9b469d2f6dabffa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="second_one&#39;s gravatar image" /><p><span>second_one</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="second_one has no accepted answers">0%</span></p></div></div><div id="comments-container-27963" class="comments-container"></div><div id="comment-tools-27963" class="comment-tools"></div><div class="clear"></div><div id="comment-27963-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

