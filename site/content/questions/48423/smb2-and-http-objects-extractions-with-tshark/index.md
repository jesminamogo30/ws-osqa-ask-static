+++
type = "question"
title = "SMB2 and HTTP objects extractions with Tshark"
description = '''Hi all I&#x27;m automating the analysis of a trace using Powershell and Tshark. I can filters and perform nearly everything I need. However, I can&#x27;t find a way to automate the export of the SMB2 and HTTP objects. I can do it in the GUI but I would like to do it automatically with a script in command line...'''
date = "2015-12-10T06:59:00Z"
lastmod = "2015-12-10T06:59:00Z"
weight = 48423
keywords = [ "export", "objects", "smb2", "tshark", "http" ]
aliases = [ "/questions/48423" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SMB2 and HTTP objects extractions with Tshark](/questions/48423/smb2-and-http-objects-extractions-with-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48423-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48423-score" class="post-score" title="current number of votes">0</div><span id="post-48423-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all</p><p>I'm automating the analysis of a trace using Powershell and Tshark. I can filters and perform nearly everything I need. However, I can't find a way to automate the export of the SMB2 and HTTP objects. I can do it in the GUI but I would like to do it automatically with a script in command line.</p><p>Does anyone know if it can be possible and how?</p><p>Thanks in advance</p><p>Osito</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span> <span class="post-tag tag-link-objects" rel="tag" title="see questions tagged &#39;objects&#39;">objects</span> <span class="post-tag tag-link-smb2" rel="tag" title="see questions tagged &#39;smb2&#39;">smb2</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Dec '15, 06:59</strong></p><img src="https://secure.gravatar.com/avatar/0e9b510379013638f59658b49d7d38cb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="osito&#39;s gravatar image" /><p><span>osito</span><br />
<span class="score" title="0 reputation points">0</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="osito has one accepted answer">50%</span></p></div></div><div id="comments-container-48423" class="comments-container"></div><div id="comment-tools-48423" class="comment-tools"></div><div class="clear"></div><div id="comment-48423-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

