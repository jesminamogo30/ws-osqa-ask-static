+++
type = "question"
title = "Network game testing via Wireshark"
description = '''Hi Wireshark Experts, How can i check/inspect the performance of my network game via Wireshark and specifically the Graphs available in Wireshark,i want to test the performance of my network game.  i see the Throughput and RoundTriptime and other graphs, the problem i m facing and the ambiguity i ha...'''
date = "2011-06-10T03:58:00Z"
lastmod = "2011-06-10T09:00:00Z"
weight = 4492
keywords = [ "lan", "testing", "game", "network", "wireshark" ]
aliases = [ "/questions/4492" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Network game testing via Wireshark](/questions/4492/network-game-testing-via-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4492-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4492-score" class="post-score" title="current number of votes">0</div><span id="post-4492-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Wireshark Experts,</p><p>How can i check/inspect the performance of my network game via Wireshark and specifically the Graphs available in Wireshark,i want to test the performance of my network game.</p><p>i see the Throughput and RoundTriptime and other graphs, <em>the problem i m facing and the ambiguity i have is that</em>; are those graphs reflecting or are they based on the data of my application</p><p>or</p><p>they are based on the overall network performance and data of that host/machine and if they are; how to set or how to get the graphs reflecting only my application relevant information,</p><p><em>currently i m filtering the port and ip of my client but still confused that server is showing the graph of that client to/from traffic or of overall traffic.</em></p><p><strong>which kind of graphs can be helpful and relevant and give me the idea of network performance of my game? i m really newbie to networking so i m unable to decide.</strong></p><p>i m using RMI and other network library for serialization and as netwrok layer and i want to compare the performance and delivery of data with both...</p><p>another question is it wireshark can really tell me about the performance at application layer like serialzation used or can it tell me flaws at application layer.</p><p>thanks,</p><p>Jibbylala</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lan" rel="tag" title="see questions tagged &#39;lan&#39;">lan</span> <span class="post-tag tag-link-testing" rel="tag" title="see questions tagged &#39;testing&#39;">testing</span> <span class="post-tag tag-link-game" rel="tag" title="see questions tagged &#39;game&#39;">game</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jun '11, 03:58</strong></p><img src="https://secure.gravatar.com/avatar/534e16ad9100e78e9fb04a7de786c773?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="static%20void%20main&#39;s gravatar image" /><p><span>static void ...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="static void main has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Jun '11, 08:47</strong> </span></p></div></div><div id="comments-container-4492" class="comments-container"><span id="4497"></span><div id="comment-4497" class="comment"><div id="post-4497-score" class="comment-score"></div><div class="comment-text"><p>You seem to have several fairly vague questions here. Have you thought about the kind of information you really want to examine? Without knowing at least what sort of information you want to look for, it will be difficult for the community to provide you with a usable answer, just like when you asked your question on SO. What performance information are you looking for? Does your application have a custom protocol you want to analyze? If you can be more specific, you will get more specific answers.</p></div><div id="comment-4497-info" class="comment-info"><span class="comment-age">(10 Jun '11, 06:24)</span> <span class="comment-user userinfo">multipleinte...</span></div></div><span id="4507"></span><div id="comment-4507" class="comment"><div id="post-4507-score" class="comment-score"></div><div class="comment-text"><p>i m now getting bored by this question everywhere,i don't know... if i m that much expert in networking why i m going to ask this questrion ...which things could characterrized the performance of a network application, the things like the bandwidth consumption and latency, the delivery and receiving of bytes/number and delay involved and here i asked specifically about the wireshark graphs are they relevant and can give me idea about all of this metrics</p></div><div id="comment-4507-info" class="comment-info"><span class="comment-age">(10 Jun '11, 08:37)</span> <span class="comment-user userinfo">static void ...</span></div></div></div><div id="comment-tools-4492" class="comment-tools"></div><div class="clear"></div><div id="comment-4492-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4508"></span>

<div id="answer-container-4508" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4508-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4508-score" class="post-score" title="current number of votes">2</div><span id="post-4508-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The visualization tools provided by Wireshark relate to the packets which are present in the analyzed capture. If you can cull out packets unrelated to your game, you could reasonably expect to view statistics more or less related directly to your application.</p><p>It sounds like you already know how to capture packets and examine them using Wireshark, so at this point it becomes a slightly less straightforward task of examining that data as an expert. As recommended to you <a href="http://stackoverflow.com/questions/6298367/network-game-testing-via-wireshark/6298781#6298781">on StackOverflow</a>, experiment with the different displays to find out how they work. Try reading the relevant <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChStatistics.html">sections</a> of the <a href="http://www.wireshark.org/docs/wsug_html_chunked/index.html">manual</a>, or check out the explanations of <a href="http://wiki.wireshark.org/TcpPduTime">these</a> <a href="http://wiki.wireshark.org/Multicast_streams">features</a> on the Wireshark <a href="http://wiki.wireshark.org/Statistics">wiki</a>. If you have a better idea of how these tools work and what, specifically, you want to examine, it will be easier to use Wireshark to accomplish what you want.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Jun '11, 09:00</strong></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="multipleinterfaces has 9 accepted answers">12%</span></p></div></div><div id="comments-container-4508" class="comments-container"></div><div id="comment-tools-4508" class="comment-tools"></div><div class="clear"></div><div id="comment-4508-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

