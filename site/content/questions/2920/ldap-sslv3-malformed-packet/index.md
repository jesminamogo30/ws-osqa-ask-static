+++
type = "question"
title = "LDAP SSLv3 malformed packet"
description = '''Hi, I am doing a LDAP SSL v3 bind and wireshark tells me the server hello packet is malformed. However, everything works fine. What could be the reason for that? (I use the dev. version 1.5, but also older versions show this message). thanks for help. JAB'''
date = "2011-03-18T07:26:00Z"
lastmod = "2011-03-18T13:17:00Z"
weight = 2920
keywords = [ "ssl", "server", "hello", "malformed", "ldap" ]
aliases = [ "/questions/2920" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [LDAP SSLv3 malformed packet](/questions/2920/ldap-sslv3-malformed-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2920-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2920-score" class="post-score" title="current number of votes">0</div><span id="post-2920-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am doing a LDAP SSL v3 bind and wireshark tells me the server hello packet is malformed. However, everything works fine. What could be the reason for that? (I use the dev. version 1.5, but also older versions show this message). thanks for help. JAB</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span> <span class="post-tag tag-link-hello" rel="tag" title="see questions tagged &#39;hello&#39;">hello</span> <span class="post-tag tag-link-malformed" rel="tag" title="see questions tagged &#39;malformed&#39;">malformed</span> <span class="post-tag tag-link-ldap" rel="tag" title="see questions tagged &#39;ldap&#39;">ldap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Mar '11, 07:26</strong></p><img src="https://secure.gravatar.com/avatar/e9ce39b937399c265bd8f3e28235765d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JAB&#39;s gravatar image" /><p><span>JAB</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JAB has no accepted answers">0%</span></p></div></div><div id="comments-container-2920" class="comments-container"></div><div id="comment-tools-2920" class="comment-tools"></div><div class="clear"></div><div id="comment-2920-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2929"></span>

<div id="answer-container-2929" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2929-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2929-score" class="post-score" title="current number of votes">0</div><span id="post-2929-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Well, that's pretty hard to tell without the packet to look at. Could you post the hexdump of the server hello packet here?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Mar '11, 13:17</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-2929" class="comments-container"></div><div id="comment-tools-2929" class="comment-tools"></div><div class="clear"></div><div id="comment-2929-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

