+++
type = "question"
title = "How do I get to the proper opening screen?"
description = '''New to Wireshark; not to Windows (25+ years as an admin). Just downloaded Version 2.2.0 (v2.2.0-0-g5368c50 from master-2.2), installed on Windows 7 64-bit. The opening screen of Wireshark is not what I expected, nor what I see in online demos or tutorials. It&#x27;s a pretty spartan screen with a blue re...'''
date = "2016-09-10T11:34:00Z"
lastmod = "2016-09-10T11:52:00Z"
weight = 55465
keywords = [ "screen", "opening" ]
aliases = [ "/questions/55465" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do I get to the proper opening screen?](/questions/55465/how-do-i-get-to-the-proper-opening-screen)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55465-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55465-score" class="post-score" title="current number of votes">0</div><span id="post-55465-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>New to Wireshark; not to Windows (25+ years as an admin).</p><p>Just downloaded Version 2.2.0 (v2.2.0-0-g5368c50 from master-2.2), installed on Windows 7 64-bit. The opening screen of Wireshark is not what I expected, nor what I see in online demos or tutorials. It's a pretty spartan screen with a blue rectangle stating "Welcome to Wireshark"; followed by the title "Capture"; then a line stating: ... using this filter: [box to enter a capture filter]; then next line: Wireless Network Connection, then next line: Local ARea Connection; then next line: Wireless Network Connection 2; then next line: Cisco remote capture; then next line: Random packet generator; then last line of: SSH remote capture.</p><p>So how do I launch Wireshark and get the proper opening screen???</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-screen" rel="tag" title="see questions tagged &#39;screen&#39;">screen</span> <span class="post-tag tag-link-opening" rel="tag" title="see questions tagged &#39;opening&#39;">opening</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Sep '16, 11:34</strong></p><img src="https://secure.gravatar.com/avatar/d62506b07d41ac7141aeaaf5e8b288d6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Watcher28&#39;s gravatar image" /><p><span>Watcher28</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Watcher28 has no accepted answers">0%</span></p></div></div><div id="comments-container-55465" class="comments-container"></div><div id="comment-tools-55465" class="comment-tools"></div><div class="clear"></div><div id="comment-55465-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55466"></span>

<div id="answer-container-55466" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55466-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55466-score" class="post-score" title="current number of votes">1</div><span id="post-55466-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark is constantly evolving and the screen you can see is the state-of-the art one while the tutorials you refer to have been created some time ago. The documentation updates also focus more at functionality than at the user interface layout.</p><p>The current opening screen takes you right where you want to be - if you want to analyse an existing file, you are likely to double-click the file. If you open Wireshark directly instead, you are supposed to be going to start capturing, which is exactly what the opening screen offers you. If you do nothing but double-click one of the lines representing capture sources (mostly network interfaces), you start capturing from that source straight away.</p><p>It is true that the relationship between the capture filter field and the capture source is not really self-explaining but you'll get used to it. <a href="https://ask.wireshark.org/questions/52472/capture-filter-error/52487">This Answer</a> to an older Question describes how it works.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Sep '16, 11:52</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Sep '16, 12:13</strong> </span></p></div></div><div id="comments-container-55466" class="comments-container"></div><div id="comment-tools-55466" class="comment-tools"></div><div class="clear"></div><div id="comment-55466-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

