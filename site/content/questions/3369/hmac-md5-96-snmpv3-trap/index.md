+++
type = "question"
title = "HMAC-MD5-96 SNMPv3 TRAP"
description = '''Hi, Is it possible to decrypt HMAC-MD5-96 SNMP Traps (Port 162) in Wirehark? I just want to check the structure of the trap to make sure it is formatted correctly. I have tried adding in the authentication and privacy string into the SNMP Protocol Preferences in the format x..x.. etc but it does not...'''
date = "2011-04-06T04:04:00Z"
lastmod = "2011-05-11T04:25:00Z"
weight = 3369
keywords = [ "hmac", "snmpv3" ]
aliases = [ "/questions/3369" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [HMAC-MD5-96 SNMPv3 TRAP](/questions/3369/hmac-md5-96-snmpv3-trap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3369-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3369-score" class="post-score" title="current number of votes">0</div><span id="post-3369-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Is it possible to decrypt HMAC-MD5-96 SNMP Traps (Port 162) in Wirehark? I just want to check the structure of the trap to make sure it is formatted correctly. I have tried adding in the authentication and privacy string into the SNMP Protocol Preferences in the format x..x.. etc but it does not decode the packet correctly.</p><p>Any advice/guideance would be appreciated.</p><p>Regards James.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hmac" rel="tag" title="see questions tagged &#39;hmac&#39;">hmac</span> <span class="post-tag tag-link-snmpv3" rel="tag" title="see questions tagged &#39;snmpv3&#39;">snmpv3</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '11, 04:04</strong></p><img src="https://secure.gravatar.com/avatar/c6cd7a907a90f40dc4d5eaeafdb43e55?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="James&#39;s gravatar image" /><p><span>James</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="James has no accepted answers">0%</span></p></div></div><div id="comments-container-3369" class="comments-container"></div><div id="comment-tools-3369" class="comment-tools"></div><div class="clear"></div><div id="comment-3369-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4030"></span>

<div id="answer-container-4030" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4030-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4030-score" class="post-score" title="current number of votes">0</div><span id="post-4030-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>hello, may you can provide me with some snmp trap packets?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 May '11, 04:25</strong></p><img src="https://secure.gravatar.com/avatar/06fef891974d3ec10a291520692bcfa7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dawen&#39;s gravatar image" /><p><span>dawen</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dawen has no accepted answers">0%</span></p></div></div><div id="comments-container-4030" class="comments-container"></div><div id="comment-tools-4030" class="comment-tools"></div><div class="clear"></div><div id="comment-4030-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

