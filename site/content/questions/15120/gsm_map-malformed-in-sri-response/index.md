+++
type = "question"
title = "gsm_map Malformed (in SRI response)"
description = '''Hi I have problem with GSM_MAP in SRI response message, the message come with gsm_map Malformed. can someone help me how i can see the message in full view. [Malformed Packet: GSM_MAP] Expert Info (Error/Malformed): Malformed Packet (Exception occurred) Malformed Packet (Exception occurred) Severity...'''
date = "2012-10-21T02:42:00Z"
lastmod = "2012-10-21T04:53:00Z"
weight = 15120
keywords = [ "gsm_map" ]
aliases = [ "/questions/15120" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [gsm\_map Malformed (in SRI response)](/questions/15120/gsm_map-malformed-in-sri-response)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15120-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15120-score" class="post-score" title="current number of votes">0</div><span id="post-15120-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I have problem with GSM_MAP in SRI response message, the message come with gsm_map Malformed. can someone help me how i can see the message in full view.</p><p>[Malformed Packet: GSM_MAP] Expert Info (Error/Malformed): Malformed Packet (Exception occurred) Malformed Packet (Exception occurred) Severity level: Error Group: Malformed</p><p>I have wireshark version 1.8.3</p><p>Thanks a lot</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gsm_map" rel="tag" title="see questions tagged &#39;gsm_map&#39;">gsm_map</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '12, 02:42</strong></p><img src="https://secure.gravatar.com/avatar/b00c42a55fedd350133c09c1902552fc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LIOR&#39;s gravatar image" /><p><span>LIOR</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LIOR has no accepted answers">0%</span></p></div></div><div id="comments-container-15120" class="comments-container"></div><div id="comment-tools-15120" class="comment-tools"></div><div class="clear"></div><div id="comment-15120-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15124"></span>

<div id="answer-container-15124" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15124-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15124-score" class="post-score" title="current number of votes">0</div><span id="post-15124-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That sounds lika bug, please open a bug report at <a href="https://bugs.wireshark.org/">https://bugs.wireshark.org/</a> attaching a small trace file illustrating the problem. Regards Anders</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Oct '12, 04:53</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-15124" class="comments-container"></div><div id="comment-tools-15124" class="comment-tools"></div><div class="clear"></div><div id="comment-15124-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

