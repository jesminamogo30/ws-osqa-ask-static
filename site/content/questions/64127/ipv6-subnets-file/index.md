+++
type = "question"
title = "IPV6 Subnets file"
description = '''At this moment I can only assign a name to an IPv6 /128 address on the &quot;hosts&quot; file, what happens if I want to assign a name on Wireshark to a group of IPv6 based on the network /32, /48, /64, /112? Wireshark can do the capture and filter for IPv6 network, but it cannot assign a name to an specify n...'''
date = "2017-10-23T14:08:00Z"
lastmod = "2017-10-24T04:29:00Z"
weight = 64127
keywords = [ "subnets", "hosts", "ipv6" ]
aliases = [ "/questions/64127" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [IPV6 Subnets file](/questions/64127/ipv6-subnets-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64127-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64127-score" class="post-score" title="current number of votes">0</div><span id="post-64127-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>At this moment I can only assign a name to an IPv6 /128 address on the "hosts" file, what happens if I want to assign a name on Wireshark to a group of IPv6 based on the network /32, /48, /64, /112? Wireshark can do the capture and filter for IPv6 network, but it cannot assign a name to an specify network in IPv6 as we do with IPV4 using the "subnets" file? Any suggestion?.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-subnets" rel="tag" title="see questions tagged &#39;subnets&#39;">subnets</span> <span class="post-tag tag-link-hosts" rel="tag" title="see questions tagged &#39;hosts&#39;">hosts</span> <span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Oct '17, 14:08</strong></p><img src="https://secure.gravatar.com/avatar/68e0b1dbe1c46eac43ed29e609ec9529?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Reginal&#39;s gravatar image" /><p><span>Reginal</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Reginal has no accepted answers">0%</span></p></div></div><div id="comments-container-64127" class="comments-container"></div><div id="comment-tools-64127" class="comment-tools"></div><div class="clear"></div><div id="comment-64127-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="64143"></span>

<div id="answer-container-64143" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-64143-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-64143-score" class="post-score" title="current number of votes">0</div><span id="post-64143-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Currently there is no such thing. If you want to have this feature you can either <a href="https://bugs.wireshark.org/bugzilla/">file an enhancement bug</a> or <a href="https://wiki.wireshark.org/CreatingPatches">create a change</a> yourself.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Oct '17, 04:29</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-64143" class="comments-container"></div><div id="comment-tools-64143" class="comment-tools"></div><div class="clear"></div><div id="comment-64143-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

