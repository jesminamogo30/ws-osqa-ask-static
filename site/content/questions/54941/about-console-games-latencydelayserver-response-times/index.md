+++
type = "question"
title = "About Console games Latency/Delay/Server Response times"
description = '''Hello, I am on a school issued project to test top 10 console games on XboxOne and PS4. We have set up a hub for consoles internet connections and mirroring the traffic to a laptop running Wireshark filtering for the consoles IP&#x27;s. My question would be is it possible to measure Latency(or Ping), Del...'''
date = "2016-08-18T02:50:00Z"
lastmod = "2016-08-18T02:50:00Z"
weight = 54941
keywords = [ "delay", "latency", "wireshark", "response_time", "server" ]
aliases = [ "/questions/54941" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [About Console games Latency/Delay/Server Response times](/questions/54941/about-console-games-latencydelayserver-response-times)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54941-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54941-score" class="post-score" title="current number of votes">0</div><span id="post-54941-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I am on a school issued project to test top 10 console games on XboxOne and PS4. We have set up a hub for consoles internet connections and mirroring the traffic to a laptop running Wireshark filtering for the consoles IP's.</p><p>My question would be is it possible to measure Latency(or Ping), Delay or Server response time solely by saving the Wireshark capture files, and analyzing them later? For my understanding for UDP packets this could be relatively challenging(?) I know games usually display the ping but this data is not recordable or saved anywhere, so that's out of the question.</p><p>It would be relatively important for the project to be able to measure data like this. We are video capturing with a high speed camera each test iteration. Any insight or shed of light to if it is even possible to do would be really appreciated. Sorry if this question isn't clear enough, I am willing and hope for questions to arise on information that I maybe have forgot to ask or provide in this post.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-delay" rel="tag" title="see questions tagged &#39;delay&#39;">delay</span> <span class="post-tag tag-link-latency" rel="tag" title="see questions tagged &#39;latency&#39;">latency</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span> <span class="post-tag tag-link-response_time" rel="tag" title="see questions tagged &#39;response_time&#39;">response_time</span> <span class="post-tag tag-link-server" rel="tag" title="see questions tagged &#39;server&#39;">server</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Aug '16, 02:50</strong></p><img src="https://secure.gravatar.com/avatar/230069e70e95296ad1a51a56a710386f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tchtony&#39;s gravatar image" /><p><span>tchtony</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tchtony has no accepted answers">0%</span></p></div></div><div id="comments-container-54941" class="comments-container"></div><div id="comment-tools-54941" class="comment-tools"></div><div class="clear"></div><div id="comment-54941-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

