+++
type = "question"
title = "Unable to detect TCP using Airpcap"
description = '''Good Morning, I have the following setup for which i am sniffing in promiscuous mode (using AirPcapNx).  AP----TCP/UDP Uplink / Downlink------&amp;gt; Client   PC -----&amp;gt; AirPcapNx (configured to sniff in the same channel at the AP client connection) .  The AirPcapNx is hosted on a Windows PC, the ada...'''
date = "2015-01-21T16:15:00Z"
lastmod = "2015-01-21T16:15:00Z"
weight = 39346
keywords = [ "udp", "airpcap", "tcp", "wireshark" ]
aliases = [ "/questions/39346" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to detect TCP using Airpcap](/questions/39346/unable-to-detect-tcp-using-airpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39346-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39346-score" class="post-score" title="current number of votes">0</div><span id="post-39346-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Good Morning,</p><p>I have the following setup for which i am sniffing in promiscuous mode (using AirPcapNx).</p><p>AP----TCP/UDP Uplink / Downlink------&gt; Client</p><pre><code>           PC -----&gt; AirPcapNx (configured to sniff in the same channel at the AP client connection) .</code></pre><p>The AirPcapNx is hosted on a Windows PC, the adapter on the windows PC is down.</p><p>If my memory serves me right i used the same setup but with MacBOOK Pro susbsituted for AirPcapNx, then i was able to sniff all the traffic.</p><p>Can't AirPcapNx detect TCP traffic. Any setting i need to be aware of.</p><p>Thanks Bharat C P<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-airpcap" rel="tag" title="see questions tagged &#39;airpcap&#39;">airpcap</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '15, 16:15</strong></p><img src="https://secure.gravatar.com/avatar/e689f2131ff3f3113d0b2cee2f420ebf?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BharatNT2IE&#39;s gravatar image" /><p><span>BharatNT2IE</span><br />
<span class="score" title="6 reputation points">6</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BharatNT2IE has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-39346" class="comments-container"></div><div id="comment-tools-39346" class="comment-tools"></div><div class="clear"></div><div id="comment-39346-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

