+++
type = "question"
title = "nature of address calling indicator"
description = '''Hi all. I&#x27;m using wireshark version 1.25 . I create from my pcap file a csv file using &quot;tshark....&quot; with several filed like : CIC , DATE, calling number , etc. One filed is nature of address of the calling number , but I noticed that it&#x27;s write in my csv file The Nature Of address of the Location nu...'''
date = "2011-06-04T07:06:00Z"
lastmod = "2011-06-04T07:06:00Z"
weight = 4372
keywords = [ "of", "calling", "address", "indicator", "nature" ]
aliases = [ "/questions/4372" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [nature of address calling indicator](/questions/4372/nature-of-address-calling-indicator)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4372-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4372-score" class="post-score" title="current number of votes">0</div><span id="post-4372-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all. I'm using wireshark version 1.25 . I create from my pcap file a csv file using "tshark...." with several filed like : CIC , DATE, calling number , etc. One filed is nature of address of the calling number , but I noticed that it's write in my csv file The Nature Of address of the Location number section and not from Calling Party Number section. This behavior cause to miss the right calling number because I don't know if the calling number is intenational or national number. Do you know how can I sprate between Nature of address indicator of the caling number and Nature of address indicator of the Location number? Thanks Oren</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-of" rel="tag" title="see questions tagged &#39;of&#39;">of</span> <span class="post-tag tag-link-calling" rel="tag" title="see questions tagged &#39;calling&#39;">calling</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span> <span class="post-tag tag-link-indicator" rel="tag" title="see questions tagged &#39;indicator&#39;">indicator</span> <span class="post-tag tag-link-nature" rel="tag" title="see questions tagged &#39;nature&#39;">nature</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jun '11, 07:06</strong></p><img src="https://secure.gravatar.com/avatar/1dac4f3eee7f8ba0a280070916de7cce?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="oren&#39;s gravatar image" /><p><span>oren</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="oren has no accepted answers">0%</span></p></div></div><div id="comments-container-4372" class="comments-container"></div><div id="comment-tools-4372" class="comment-tools"></div><div class="clear"></div><div id="comment-4372-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

