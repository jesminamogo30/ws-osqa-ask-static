+++
type = "question"
title = "RTP payload header dissector - ST2022"
description = '''I am looking for a dissector to look into the RTP payload header that is part of the ST2022-6 standard. Can someone point me to one if it exists? thanks in advance'''
date = "2016-08-29T10:37:00Z"
lastmod = "2016-08-29T13:27:00Z"
weight = 55172
keywords = [ "header", "rtp", "payload" ]
aliases = [ "/questions/55172" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RTP payload header dissector - ST2022](/questions/55172/rtp-payload-header-dissector-st2022)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55172-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55172-score" class="post-score" title="current number of votes">0</div><span id="post-55172-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking for a dissector to look into the RTP payload header that is part of the ST2022-6 standard. Can someone point me to one if it exists?</p><p>thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-header" rel="tag" title="see questions tagged &#39;header&#39;">header</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span> <span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Aug '16, 10:37</strong></p><img src="https://secure.gravatar.com/avatar/f354bd866fd93a94e1c1570f0ae16a4e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="shempington&#39;s gravatar image" /><p><span>shempington</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="shempington has no accepted answers">0%</span></p></div></div><div id="comments-container-55172" class="comments-container"></div><div id="comment-tools-55172" class="comment-tools"></div><div class="clear"></div><div id="comment-55172-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55175"></span>

<div id="answer-container-55175" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55175-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55175-score" class="post-score" title="current number of votes">0</div><span id="post-55175-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There's a LUA dissector on the <a href="https://wiki.wireshark.org/Contrib">Wiki contrib page</a>, called SMPTE-2022-6.lua. It's stated to be made for Wireshark 1.10, so may need adaptation for newer Wireshark versions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Aug '16, 13:27</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-55175" class="comments-container"></div><div id="comment-tools-55175" class="comment-tools"></div><div class="clear"></div><div id="comment-55175-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

