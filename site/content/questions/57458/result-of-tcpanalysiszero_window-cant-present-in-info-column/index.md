+++
type = "question"
title = "result of tcp.analysis.zero_window can&#x27;t present in info column"
description = '''I can&#x27;t see results of tcp.analysis.zero_window can&#x27;t present in info column in wireshark v2.2.2 on mac sierra 10.12.1. but, strange things is the results is displayed on windows 7 where is installed same wireshark version. How can i show the results of tcp.analysis.zero_window filter. please give m...'''
date = "2016-11-18T20:20:00Z"
lastmod = "2016-11-18T20:20:00Z"
weight = 57458
keywords = [ "zero_window" ]
aliases = [ "/questions/57458" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [result of tcp.analysis.zero\_window can't present in info column](/questions/57458/result-of-tcpanalysiszero_window-cant-present-in-info-column)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57458-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57458-score" class="post-score" title="current number of votes">0</div><span id="post-57458-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can't see results of tcp.analysis.zero_window can't present in info column in wireshark v2.2.2 on mac sierra 10.12.1. but, strange things is the results is displayed on windows 7 where is installed same wireshark version. How can i show the results of tcp.analysis.zero_window filter.</p><p>please give me information.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-zero_window" rel="tag" title="see questions tagged &#39;zero_window&#39;">zero_window</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '16, 20:20</strong></p><img src="https://secure.gravatar.com/avatar/e3514ba1e376cda6bdb85b7a155b87e6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="YJKIM&#39;s gravatar image" /><p><span>YJKIM</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="YJKIM has no accepted answers">0%</span></p></div></div><div id="comments-container-57458" class="comments-container"></div><div id="comment-tools-57458" class="comment-tools"></div><div class="clear"></div><div id="comment-57458-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

