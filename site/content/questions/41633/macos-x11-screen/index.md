+++
type = "question"
title = "MacOS X11 screen."
description = '''Hi to all, I&#x27;ve Yosemite 10.10.3, last update, and I&#x27;ve installed XQuartz-2.7.7. The first time it ask me where is X11 and I give it the Utility&#92;X11 path. The application GUI starts with a strange problem: the window is not adjustable in height! Only in width. I can&#x27;t move up the window for extend i...'''
date = "2015-04-21T07:14:00Z"
lastmod = "2015-04-22T02:00:00Z"
weight = 41633
keywords = [ "xquartz", "yosemite" ]
aliases = [ "/questions/41633" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [MacOS X11 screen.](/questions/41633/macos-x11-screen)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41633-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41633-score" class="post-score" title="current number of votes">0</div><span id="post-41633-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi to all,</p><p>I've Yosemite 10.10.3, last update, and I've installed XQuartz-2.7.7.</p><p>The first time it ask me where is X11 and I give it the Utility\X11 path.</p><p>The application GUI starts with a strange problem: the window is not adjustable in height! Only in width. I can't move up the window for extend it. See image.<img src="https://osqa-ask.wireshark.org/upfiles/Schermata_2015-04-21_alle_16.08.39_L0lYSIf.png" alt="alt text" /></p><p>Regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-xquartz" rel="tag" title="see questions tagged &#39;xquartz&#39;">xquartz</span> <span class="post-tag tag-link-yosemite" rel="tag" title="see questions tagged &#39;yosemite&#39;">yosemite</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Apr '15, 07:14</strong></p><img src="https://secure.gravatar.com/avatar/4349b5e3b9b0249e6e191096ed9d77f8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Michele%20Mastropieri&#39;s gravatar image" /><p><span>Michele Mast...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Michele Mastropieri has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Apr '15, 00:58</strong> </span></p></div></div><div id="comments-container-41633" class="comments-container"><span id="41646"></span><div id="comment-41646" class="comment"><div id="post-41646-score" class="comment-score"></div><div class="comment-text"><p>What happens if you open an xterm window? Can you adjust its height?</p></div><div id="comment-41646-info" class="comment-info"><span class="comment-age">(21 Apr '15, 14:13)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="41659"></span><div id="comment-41659" class="comment"><div id="post-41659-score" class="comment-score"></div><div class="comment-text"><p>xterm windows have same problem! I can't move it in up direction.</p></div><div id="comment-41659-info" class="comment-info"><span class="comment-age">(22 Apr '15, 00:53)</span> <span class="comment-user userinfo">Michele Mast...</span></div></div></div><div id="comment-tools-41633" class="comment-tools"></div><div class="clear"></div><div id="comment-41633-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41662"></span>

<div id="answer-container-41662" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41662-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41662-score" class="post-score" title="current number of votes">0</div><span id="post-41662-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sounds like an X11 problem; you should probably <a href="https://xquartz.macosforge.org/auth/login/?next=/trac/newticket">file a bug against XQuartz</a> (you may have to register first; see the "Register" link in the upper-right corner).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Apr '15, 02:00</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Apr '15, 02:00</strong> </span></p></div></div><div id="comments-container-41662" class="comments-container"></div><div id="comment-tools-41662" class="comment-tools"></div><div class="clear"></div><div id="comment-41662-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

