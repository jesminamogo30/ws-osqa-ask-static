+++
type = "question"
title = "Capture network traffic for wired C.E. devices"
description = '''I often need to capture traffic for various consumer electronic devices to determine what API calls are being made along with the response.  This is quite easy to do with my MacBook and wifi capable devices. I simply connect the ethernet cable to my laptop and share my ethernet connection over my wi...'''
date = "2013-03-08T16:08:00Z"
lastmod = "2013-03-09T04:05:00Z"
weight = 19322
keywords = [ "traffic", "wired", "devices" ]
aliases = [ "/questions/19322" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Capture network traffic for wired C.E. devices](/questions/19322/capture-network-traffic-for-wired-ce-devices)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19322-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19322-score" class="post-score" title="current number of votes">0</div><span id="post-19322-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I often need to capture traffic for various consumer electronic devices to determine what API calls are being made along with the response.</p><p>This is quite easy to do with my MacBook and wifi capable devices. I simply connect the ethernet cable to my laptop and share my ethernet connection over my wifi. I connect the C.E. device through the shared wifi on my MacBook and capture all traffic on my computer with WireShark.</p><p>I need to do the same for devices that are not wifi capable and need to connect with ethernet. How can I route all wired device traffic through my computer so I can capture it with WireShark?</p><p>Thanks in advance for your help!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-wired" rel="tag" title="see questions tagged &#39;wired&#39;">wired</span> <span class="post-tag tag-link-devices" rel="tag" title="see questions tagged &#39;devices&#39;">devices</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Mar '13, 16:08</strong></p><img src="https://secure.gravatar.com/avatar/d32dc7b83ced776e44b0fa2916d88c21?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="evanoh&#39;s gravatar image" /><p><span>evanoh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="evanoh has no accepted answers">0%</span></p></div></div><div id="comments-container-19322" class="comments-container"></div><div id="comment-tools-19322" class="comment-tools"></div><div class="clear"></div><div id="comment-19322-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19330"></span>

<div id="answer-container-19330" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19330-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19330-score" class="post-score" title="current number of votes">0</div><span id="post-19330-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>All your options are explained <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Mar '13, 04:05</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-19330" class="comments-container"></div><div id="comment-tools-19330" class="comment-tools"></div><div class="clear"></div><div id="comment-19330-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

