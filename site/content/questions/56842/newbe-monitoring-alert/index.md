+++
type = "question"
title = "[closed] Newbe- monitoring alert???"
description = '''Packet comment called monitoring alert ? Help plz'''
date = "2016-10-30T19:09:00Z"
lastmod = "2016-11-04T11:24:00Z"
weight = 56842
keywords = [ "#packet", "#monitor" ]
aliases = [ "/questions/56842" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Newbe- monitoring alert???](/questions/56842/newbe-monitoring-alert)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56842-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56842-score" class="post-score" title="current number of votes">-1</div><span id="post-56842-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Packet comment called monitoring alert ? Help plz</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-#packet" rel="tag" title="see questions tagged &#39;#packet&#39;">#packet</span> <span class="post-tag tag-link-#monitor" rel="tag" title="see questions tagged &#39;#monitor&#39;">#monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Oct '16, 19:09</strong></p><img src="https://secure.gravatar.com/avatar/f61874d37252f74ff5d68c087d6fac3f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jnettspagett&#39;s gravatar image" /><p><span>Jnettspagett</span><br />
<span class="score" title="5 reputation points">5</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jnettspagett has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>10 Jan '17, 06:18</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-56842" class="comments-container"><span id="56857"></span><div id="comment-56857" class="comment"><div id="post-56857-score" class="comment-score"></div><div class="comment-text"><p>You've got to give some more information than this, we're not wizards who can magically pear at your setup and instantly know what you have. <em>You</em> are our eyes on this matter, so describe (in full) what you see.</p></div><div id="comment-56857-info" class="comment-info"><span class="comment-age">(31 Oct '16, 02:46)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="56979"></span><div id="comment-56979" class="comment"><div id="post-56979-score" class="comment-score"></div><div class="comment-text"><p>Right... so that part that says 'new' you didnt think ment i dont know what more and you should be more descriptive. Im not a wizard of mind reading.</p></div><div id="comment-56979-info" class="comment-info"><span class="comment-age">(04 Nov '16, 04:47)</span> <span class="comment-user userinfo">Jnettspagett</span></div></div><span id="56991"></span><div id="comment-56991" class="comment"><div id="post-56991-score" class="comment-score"></div><div class="comment-text"><blockquote><p>I'm not a wizard of mind reading.</p></blockquote><p>Nor are we, that's what <span>@Jaap</span> was trying to say :-)</p><p>A "packet comment" is normally a text field which you add by right-clicking a frame in the packet list pane and choosing the <code>Packet Comment...</code> item from the context menu. If this field is not empty, it is shown as the very first "layer" in the packet dissection pane when that frame is chosen in the packet list, even before the <code>frame</code> "layer". I.e. it does not normally exist in any capture file until someone manually adds it there.</p><p>So your question can be read in many ways - "has commenting a packet caused Wireshark to report a monitoring alert" or "how did a packet comment whose contents is 'monitoring alert' appear in my capture".</p><p>But it can also be read as "in the dissection tree, there is a line saying 'monitoring alert', what does that line mean" if you didn't know that "packet comment" has a particular meaning in the context of Wireshark.</p><p>So please use more words to describe what is your problem, including what is the contents of the frame, and best of all, upload the capture to Cloudshark or to any plain file publishing service such as Google Drive or Dropbox or... and edit your Question with a login-free link to that file and with the number of the frame in it at which we should look to see the issue.</p></div><div id="comment-56991-info" class="comment-info"><span class="comment-age">(04 Nov '16, 11:24)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-56842" class="comment-tools"></div><div class="clear"></div><div id="comment-56842-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Not enough information to answer the question and requested information hasn't been sent." by JeffMorriss 10 Jan '17, 06:18

</div>

</div>

</div>

