+++
type = "question"
title = "short timeouts"
description = '''Hello, I had some connection problems for like 2 minutes few times in a row only when online on steam(cs:go etc.). I might be ddosed so i used this: https://www.youtube.com/watch?v=oWCYwQ76k3M and managed to open this window in wireshark: http://scr.hu/33qj/xqdkj I don&#x27;t even know what am I doing bu...'''
date = "2016-11-30T04:38:00Z"
lastmod = "2016-11-30T04:38:00Z"
weight = 57727
keywords = [ "ddos" ]
aliases = [ "/questions/57727" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [short timeouts](/questions/57727/short-timeouts)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57727-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57727-score" class="post-score" title="current number of votes">0</div><span id="post-57727-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I had some connection problems for like 2 minutes few times in a row only when online on steam(cs:go etc.). I might be ddosed so i used this: <a href="https://www.youtube.com/watch?v=oWCYwQ76k3M">https://www.youtube.com/watch?v=oWCYwQ76k3M</a> and managed to open this window in wireshark: <a href="http://scr.hu/33qj/xqdkj">http://scr.hu/33qj/xqdkj</a> I don't even know what am I doing but thought this looks suspicious so I hope You guys can tell me in I'm being wrong. If u need me to click sth else I'll gladly do that!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ddos" rel="tag" title="see questions tagged &#39;ddos&#39;">ddos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Nov '16, 04:38</strong></p><img src="https://secure.gravatar.com/avatar/d5526bf78d91a0d4096833b053b9d88d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="r3yxa&#39;s gravatar image" /><p><span>r3yxa</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="r3yxa has no accepted answers">0%</span></p></div></div><div id="comments-container-57727" class="comments-container"></div><div id="comment-tools-57727" class="comment-tools"></div><div class="clear"></div><div id="comment-57727-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

