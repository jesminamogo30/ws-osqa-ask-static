+++
type = "question"
title = "Running wireshark 1.12.4 Version on my mac, but there is no interface can be used for capturing"
description = '''I have already install X11, but I do not have any available interfaces to use to capture the packets. Can anyone help me? My mac OS is 10.10.2, the version of wireshark is V1.12.4-0.'''
date = "2015-03-10T16:51:00Z"
lastmod = "2015-03-11T13:08:00Z"
weight = 40452
keywords = [ "not", "mac", "networkinterfaces", "can", "find" ]
aliases = [ "/questions/40452" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Running wireshark 1.12.4 Version on my mac, but there is no interface can be used for capturing](/questions/40452/running-wireshark-1124-version-on-my-mac-but-there-is-no-interface-can-be-used-for-capturing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40452-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40452-score" class="post-score" title="current number of votes">0</div><span id="post-40452-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have already install X11, but I do not have any available interfaces to use to capture the packets. Can anyone help me? My mac OS is 10.10.2, the version of wireshark is V1.12.4-0.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-networkinterfaces" rel="tag" title="see questions tagged &#39;networkinterfaces&#39;">networkinterfaces</span> <span class="post-tag tag-link-can" rel="tag" title="see questions tagged &#39;can&#39;">can</span> <span class="post-tag tag-link-find" rel="tag" title="see questions tagged &#39;find&#39;">find</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Mar '15, 16:51</strong></p><img src="https://secure.gravatar.com/avatar/8eaad4b94df63fe4dc46839ca506563f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pu%20Zhao&#39;s gravatar image" /><p><span>Pu Zhao</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pu Zhao has no accepted answers">0%</span></p></div></div><div id="comments-container-40452" class="comments-container"></div><div id="comment-tools-40452" class="comment-tools"></div><div class="clear"></div><div id="comment-40452-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40483"></span>

<div id="answer-container-40483" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40483-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40483-score" class="post-score" title="current number of votes">0</div><span id="post-40483-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try using 1.99.3, this worked for me. I found that 1.12.4 would start but then close without ever giving me a capture window or proper GUI (despite having X11 installed)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Mar '15, 13:08</strong></p><img src="https://secure.gravatar.com/avatar/648dfbed062f4e2c392e309ed01ef2f4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="razamattaz&#39;s gravatar image" /><p><span>razamattaz</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="razamattaz has no accepted answers">0%</span></p></div></div><div id="comments-container-40483" class="comments-container"></div><div id="comment-tools-40483" class="comment-tools"></div><div class="clear"></div><div id="comment-40483-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

