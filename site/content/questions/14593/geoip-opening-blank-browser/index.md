+++
type = "question"
title = "GeoIP opening blank browser"
description = '''I have tried in IE, Chrome and IE but the web browser is always blank.'''
date = "2012-09-28T07:46:00Z"
lastmod = "2012-09-28T08:55:00Z"
weight = 14593
keywords = [ "geoip" ]
aliases = [ "/questions/14593" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [GeoIP opening blank browser](/questions/14593/geoip-opening-blank-browser)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14593-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14593-score" class="post-score" title="current number of votes">0</div><span id="post-14593-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have tried in IE, Chrome and IE but the web browser is always blank.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-geoip" rel="tag" title="see questions tagged &#39;geoip&#39;">geoip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Sep '12, 07:46</strong></p><img src="https://secure.gravatar.com/avatar/865c8ff61c8626647de2b885a59567e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rob123&#39;s gravatar image" /><p><span>rob123</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rob123 has no accepted answers">0%</span></p></div></div><div id="comments-container-14593" class="comments-container"></div><div id="comment-tools-14593" class="comment-tools"></div><div class="clear"></div><div id="comment-14593-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14595"></span>

<div id="answer-container-14595" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14595-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14595-score" class="post-score" title="current number of votes">3</div><span id="post-14595-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This is a known bug, <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5016">bug 5016</a>, which will be fixed in the next release, version 1.8.3, currently <a href="http://wiki.wireshark.org/Development/Roadmap">scheduled to be released</a> around October 1, 2012.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Sep '12, 07:54</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Sep '12, 07:56</strong> </span></p></div></div><div id="comments-container-14595" class="comments-container"><span id="14602"></span><div id="comment-14602" class="comment"><div id="post-14602-score" class="comment-score"></div><div class="comment-text"><p>Thanks I will download the new version on monday :)</p></div><div id="comment-14602-info" class="comment-info"><span class="comment-age">(28 Sep '12, 08:51)</span> <span class="comment-user userinfo">rob123</span></div></div><span id="14604"></span><div id="comment-14604" class="comment"><div id="post-14604-score" class="comment-score"></div><div class="comment-text"><p>I converted your "answer" to a comment as that's how this site works. Please see the FAQ fro more info.</p><p>If the answer solves your issue please "accept" it so that others can see the good answers by clicking the "checkmark" icon.</p></div><div id="comment-14604-info" class="comment-info"><span class="comment-age">(28 Sep '12, 08:55)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-14595" class="comment-tools"></div><div class="clear"></div><div id="comment-14595-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

