+++
type = "question"
title = "What is that? is it right captured file?"
description = '''I have captured on Android device. but i an not sure is right or not protocol is only SLL. wire shark doesn&#x27;t show anything. please help me Thanks'''
date = "2011-01-16T15:56:00Z"
lastmod = "2011-01-17T12:57:00Z"
weight = 1768
keywords = [ "sll" ]
aliases = [ "/questions/1768" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What is that? is it right captured file?](/questions/1768/what-is-that-is-it-right-captured-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1768-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1768-score" class="post-score" title="current number of votes">0</div><span id="post-1768-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured on Android device. but i an not sure is right or not protocol is only SLL. wire shark doesn't show anything.</p><p>please help me</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sll" rel="tag" title="see questions tagged &#39;sll&#39;">sll</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jan '11, 15:56</strong></p><img src="https://secure.gravatar.com/avatar/41da58c7ab0112ba05406da70f9ea168?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="taehunzzang&#39;s gravatar image" /><p><span>taehunzzang</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="taehunzzang has no accepted answers">0%</span></p></div></div><div id="comments-container-1768" class="comments-container"></div><div id="comment-tools-1768" class="comment-tools"></div><div class="clear"></div><div id="comment-1768-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1774"></span>

<div id="answer-container-1774" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1774-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1774-score" class="post-score" title="current number of votes">0</div><span id="post-1774-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>SLL, sure, that's Linux Cooked capture. You should try a <a href="http://www.wireshark.org/download/automated/">recent Wireshark development build</a> and see what comes out there.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Jan '11, 12:57</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1774" class="comments-container"></div><div id="comment-tools-1774" class="comment-tools"></div><div class="clear"></div><div id="comment-1774-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

