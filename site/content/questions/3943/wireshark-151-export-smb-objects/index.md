+++
type = "question"
title = "Wireshark 1.5.1: Export SMB Objects"
description = '''Wireshark 1.5.1 can export SMB objects. Jose Pico submitted a patch to add this feature to Wireshark. The white paper: A tool for capturing SMB files with Wireshark by David Perez &amp;amp; Jose Pico is freely available. The white paper describes the plug-in they have created, the identifying of the SMB...'''
date = "2011-05-05T08:03:00Z"
lastmod = "2011-05-05T11:09:00Z"
weight = 3943
keywords = [ "info", "smb", "white-paper" ]
aliases = [ "/questions/3943" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 1.5.1: Export SMB Objects](/questions/3943/wireshark-151-export-smb-objects)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3943-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3943-score" class="post-score" title="current number of votes">1</div><span id="post-3943-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><a href="http://www.wireshark.org/download.html">Wireshark 1.5.1</a> can export SMB objects.<br />
Jose Pico submitted a <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=4451">patch</a> to add this feature to Wireshark.<br />
The white paper: <a href="http://www.taddong.com/docs/WP_SMBPlugin.pdf">A tool for capturing SMB files with Wireshark</a> by David Perez &amp; Jose Pico is freely available.</p><p>The white paper describes the plug-in they have created, the identifying of the SMB streams and gives an explanation of the columns in the "Wireshark: Export SMB object list"</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-info" rel="tag" title="see questions tagged &#39;info&#39;">info</span> <span class="post-tag tag-link-smb" rel="tag" title="see questions tagged &#39;smb&#39;">smb</span> <span class="post-tag tag-link-white-paper" rel="tag" title="see questions tagged &#39;white-paper&#39;">white-paper</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 May '11, 08:03</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span> </br></br></p></div></div><div id="comments-container-3943" class="comments-container"><span id="3946"></span><div id="comment-3946" class="comment"><div id="post-3946-score" class="comment-score"></div><div class="comment-text"><p>So ... what's the question exactly? Am I missing something?</p></div><div id="comment-3946-info" class="comment-info"><span class="comment-age">(05 May '11, 08:32)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="3950"></span><div id="comment-3950" class="comment"><div id="post-3950-score" class="comment-score"></div><div class="comment-text"><p>Oops, no question.<br />
I thought it's useful information for the community.<br />
Should I delete this "question"?</p></div><div id="comment-3950-info" class="comment-info"><span class="comment-age">(05 May '11, 08:53)</span> <span class="comment-user userinfo">joke</span></div></div><span id="3951"></span><div id="comment-3951" class="comment"><div id="post-3951-score" class="comment-score"></div><div class="comment-text"><p>I agree it's useful information; I just didn't think it fit the Q&amp;A format of <a href="http://ask.wireshark.org/">ask</a>. In the past, I've shared what I thought was useful information to the community on the wireshark-dev and/or -users mailing list. Maybe those would be more appropriate? A post on <a href="http://www.lovemytool.com/">lovemytool</a> might also be appropriate. BTW, I like that site and your articles. :)</p></div><div id="comment-3951-info" class="comment-info"><span class="comment-age">(05 May '11, 09:04)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="3958"></span><div id="comment-3958" class="comment"><div id="post-3958-score" class="comment-score"></div><div class="comment-text"><p>I'll post it elsewhere.<br />
Thank you for your feedback.<br />
<br />
Great to hear that you enjoy reading my articles:)</p></div><div id="comment-3958-info" class="comment-info"><span class="comment-age">(05 May '11, 11:09)</span> <span class="comment-user userinfo">joke</span></div></div></div><div id="comment-tools-3943" class="comment-tools"></div><div class="clear"></div><div id="comment-3943-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

