+++
type = "question"
title = "Sort Timestamps on two interfaces."
description = '''Hey Folks I have done a Shark trace from to interfaces of a call trace going through 2 different capture points. My question is this.... When I choose both capture links and trace them... When I look at the timestamp on the left side of the &quot;Graph Analysts&quot; I notice that it doesn&#x27;t totally inter wea...'''
date = "2015-07-30T06:42:00Z"
lastmod = "2015-07-30T12:25:00Z"
weight = 44634
keywords = [ "timestamps" ]
aliases = [ "/questions/44634" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Sort Timestamps on two interfaces.](/questions/44634/sort-timestamps-on-two-interfaces)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44634-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44634-score" class="post-score" title="current number of votes">0</div><span id="post-44634-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hey Folks</p><p>I have done a Shark trace from to interfaces of a call trace going through 2 different capture points.</p><p>My question is this....</p><p>When I choose both capture links and trace them... When I look at the timestamp on the left side of the "Graph Analysts" I notice that it doesn't totally inter weave the lines of data....</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2015-07-30_at_9.39.35_AM.png" alt="alt text" /></p><p>IS there anyway I can trap this better, Or Sort by the timestamps better?</p><p>-David</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamps" rel="tag" title="see questions tagged &#39;timestamps&#39;">timestamps</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '15, 06:42</strong></p><img src="https://secure.gravatar.com/avatar/f6a7b70ac13ce7dbb92a738d63b56123?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dwsaunders&#39;s gravatar image" /><p><span>dwsaunders</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dwsaunders has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Jul '15, 08:13</strong> </span></p></div></div><div id="comments-container-44634" class="comments-container"><span id="44645"></span><div id="comment-44645" class="comment"><div id="post-44645-score" class="comment-score">1</div><div class="comment-text"><p>Maybe this could help you: <a href="https://blog.packet-foo.com/2015/04/deep-dive-frame-timestamps/">https://blog.packet-foo.com/2015/04/deep-dive-frame-timestamps/</a></p></div><div id="comment-44645-info" class="comment-info"><span class="comment-age">(30 Jul '15, 12:25)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-44634" class="comment-tools"></div><div class="clear"></div><div id="comment-44634-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

