+++
type = "question"
title = "Any way to merge preexisting local account with Twitter/Facebook/OpenID?"
description = '''I started participating here before the site implemented OAUTH support (e.g. login with Twitter, Facebook, OpenID identity, etc.)... Is there any way to associate/merge my preexisting local username (wesmorgan1) with my Twitter handle (also wesmorgan1)? When I try to log in with Twitter, I get an HT...'''
date = "2014-02-06T13:37:00Z"
lastmod = "2014-02-19T11:08:00Z"
weight = 29498
keywords = [ "website", "login", "site" ]
aliases = [ "/questions/29498" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Any way to merge preexisting local account with Twitter/Facebook/OpenID?](/questions/29498/any-way-to-merge-preexisting-local-account-with-twitterfacebookopenid)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29498-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29498-score" class="post-score" title="current number of votes">0</div><span id="post-29498-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I started participating here before the site implemented OAUTH support (e.g. login with Twitter, Facebook, OpenID identity, etc.)...</p><p>Is there any way to associate/merge my preexisting local username (wesmorgan1) with my Twitter handle (also wesmorgan1)? When I try to log in with Twitter, I get an HTTP 500 error code...</p><p>I'd like to keep my identity, history, badges, etc...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-website" rel="tag" title="see questions tagged &#39;website&#39;">website</span> <span class="post-tag tag-link-login" rel="tag" title="see questions tagged &#39;login&#39;">login</span> <span class="post-tag tag-link-site" rel="tag" title="see questions tagged &#39;site&#39;">site</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '14, 13:37</strong></p><img src="https://secure.gravatar.com/avatar/11ea89c2fd5a5830c69d0574a51b8142?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wesmorgan1&#39;s gravatar image" /><p><span>wesmorgan1</span><br />
<span class="score" title="411 reputation points">411</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wesmorgan1 has 2 accepted answers">4%</span></p></div></div><div id="comments-container-29498" class="comments-container"><span id="29499"></span><div id="comment-29499" class="comment"><div id="post-29499-score" class="comment-score"></div><div class="comment-text"><p>IOW: you no longer want to use your ask.wireshark.org "local username and password" to login ?</p></div><div id="comment-29499-info" class="comment-info"><span class="comment-age">(06 Feb '14, 14:19)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-29498" class="comment-tools"></div><div class="clear"></div><div id="comment-29498-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29520"></span>

<div id="answer-container-29520" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29520-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29520-score" class="post-score" title="current number of votes">1</div><span id="post-29520-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="wesmorgan1 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can do it by adding a new authentication provider (Facebook) to your user settings.</p><p>Click on your name, then choose:</p><blockquote><p>User tools -&gt; authentication settings -&gt; Add new provider -&gt; Facebook</p></blockquote><p>As soon as you have authenticated with Facebook, you will see the Facebook authentication provider. The username on Facebook and Wireshark can be different - I just tested it.</p><p>From now on, you can authenticate with both methods (username/password) and via your Facebook account.</p><p>I've just done it, and did not realize any problems.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Feb '14, 03:17</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Feb '14, 03:18</strong> </span></p></div></div><div id="comments-container-29520" class="comments-container"><span id="30026"></span><div id="comment-30026" class="comment"><div id="post-30026-score" class="comment-score"></div><div class="comment-text"><p>Tried that - attempting to login with my Twitter account failed with an HTTP 500 error.</p></div><div id="comment-30026-info" class="comment-info"><span class="comment-age">(19 Feb '14, 10:42)</span> <span class="comment-user userinfo">wesmorgan1</span></div></div><span id="30027"></span><div id="comment-30027" class="comment"><div id="post-30027-score" class="comment-score"></div><div class="comment-text"><p>Can you please try with a Facebook account? That worked for me.</p></div><div id="comment-30027-info" class="comment-info"><span class="comment-age">(19 Feb '14, 10:44)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="30028"></span><div id="comment-30028" class="comment"><div id="post-30028-score" class="comment-score"></div><div class="comment-text"><p>Facebook worked...</p></div><div id="comment-30028-info" class="comment-info"><span class="comment-age">(19 Feb '14, 11:05)</span> <span class="comment-user userinfo">wesmorgan1</span></div></div><span id="30029"></span><div id="comment-30029" class="comment"><div id="post-30029-score" class="comment-score"></div><div class="comment-text"><p>O.K. looks like a problem with the Twitter authentication provider. Please contact <span>@Gerald Combs</span>.</p></div><div id="comment-30029-info" class="comment-info"><span class="comment-age">(19 Feb '14, 11:08)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29520" class="comment-tools"></div><div class="clear"></div><div id="comment-29520-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

