+++
type = "question"
title = "capturing adodb traffic to external website"
description = '''I am attempting to capture traffic from an VB macro that downloads a file from an external website. I am using XMLHTTP object and GET then creating an ADODB.Stream to download the file. I cannot see this traffic in wireshark but can see http and SSH traffic (from putty and chrome) and the only filte...'''
date = "2016-10-12T08:34:00Z"
lastmod = "2016-10-12T22:08:00Z"
weight = 56322
keywords = [ "capture" ]
aliases = [ "/questions/56322" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [capturing adodb traffic to external website](/questions/56322/capturing-adodb-traffic-to-external-website)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-56322-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-56322-score" class="post-score" title="current number of votes">0</div><span id="post-56322-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am attempting to capture traffic from an VB macro that downloads a file from an external website. I am using XMLHTTP object and GET then creating an ADODB.Stream to download the file. I cannot see this traffic in wireshark but can see http and SSH traffic (from putty and chrome) and the only filter is on the internet IP address. All protocols are on. Anyone know how I can capture this traffic?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Oct '16, 08:34</strong></p><img src="https://secure.gravatar.com/avatar/e92f3bafe6309146287c2e876a850cb7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pcapper&#39;s gravatar image" /><p><span>Pcapper</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pcapper has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Oct '16, 08:35</strong> </span></p></div></div><div id="comments-container-56322" class="comments-container"><span id="56330"></span><div id="comment-56330" class="comment"><div id="post-56330-score" class="comment-score"></div><div class="comment-text"><p>You say you download a file from an external website. I assume you have some URL for that, consisting of a hostname and path. Then you say you filter on internet IP address. I would assume the URL resolves or is redirected to another IP address then what you filter for?</p></div><div id="comment-56330-info" class="comment-info"><span class="comment-age">(12 Oct '16, 22:08)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-56322" class="comment-tools"></div><div class="clear"></div><div id="comment-56322-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

