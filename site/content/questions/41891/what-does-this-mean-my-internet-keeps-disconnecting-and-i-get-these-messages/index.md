+++
type = "question"
title = "What does this mean? My internet keeps disconnecting and I get these messages."
description = '''https://puu.sh/hsDHF/ece4a52209.png I&#x27;ve made a screenshot of the most important lines (I think) If you want to take a look at the whole file, just tell me.'''
date = "2015-04-27T08:26:00Z"
lastmod = "2015-04-27T08:26:00Z"
weight = 41891
keywords = [ "disconnect", "interpretation" ]
aliases = [ "/questions/41891" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [What does this mean? My internet keeps disconnecting and I get these messages.](/questions/41891/what-does-this-mean-my-internet-keeps-disconnecting-and-i-get-these-messages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41891-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41891-score" class="post-score" title="current number of votes">0</div><span id="post-41891-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p><a href="https://puu.sh/hsDHF/ece4a52209.png">https://puu.sh/hsDHF/ece4a52209.png</a></p><p>I've made a screenshot of the most important lines (I think) If you want to take a look at the whole file, just tell me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-disconnect" rel="tag" title="see questions tagged &#39;disconnect&#39;">disconnect</span> <span class="post-tag tag-link-interpretation" rel="tag" title="see questions tagged &#39;interpretation&#39;">interpretation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Apr '15, 08:26</strong></p><img src="https://secure.gravatar.com/avatar/c8c0554fa1fc4f41ba177bc9c1213446?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Konstamokh&#39;s gravatar image" /><p><span>Konstamokh</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Konstamokh has no accepted answers">0%</span></p></div></div><div id="comments-container-41891" class="comments-container"></div><div id="comment-tools-41891" class="comment-tools"></div><div class="clear"></div><div id="comment-41891-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

