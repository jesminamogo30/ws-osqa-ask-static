+++
type = "question"
title = "Firewall ACL Rules missing from Tools menu"
description = '''I can&#x27;t find the firewall rules ACL in the tolls, sistem: windows 10. Someone can say me if this future is been deleted or I mistake something in the installation, please?'''
date = "2016-04-15T09:15:00Z"
lastmod = "2016-05-24T11:41:00Z"
weight = 51701
keywords = [ "firewall" ]
aliases = [ "/questions/51701" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Firewall ACL Rules missing from Tools menu](/questions/51701/firewall-acl-rules-missing-from-tools-menu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51701-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51701-score" class="post-score" title="current number of votes">0</div><span id="post-51701-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can't find the firewall rules ACL in the tolls, sistem: windows 10. Someone can say me if this future is been deleted or I mistake something in the installation, please?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-firewall" rel="tag" title="see questions tagged &#39;firewall&#39;">firewall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Apr '16, 09:15</strong></p><img src="https://secure.gravatar.com/avatar/6051fdfe6c025bfc9146a60d303f91b6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="h22&#39;s gravatar image" /><p><span>h22</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="h22 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 May '16, 11:44</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-51701" class="comments-container"></div><div id="comment-tools-51701" class="comment-tools"></div><div class="clear"></div><div id="comment-51701-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51705"></span>

<div id="answer-container-51705" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51705-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51705-score" class="post-score" title="current number of votes">2</div><span id="post-51705-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That hasn't yet been implemented in the Qt version.</p><p>The functionality should still be in the legacy GTK version though, wireshark-gtk.exe, installed right alongside wireshark.exe. You should have a legacy shortcut on the Start menu somewhere.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Apr '16, 10:33</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-51705" class="comments-container"><span id="51707"></span><div id="comment-51707" class="comment"><div id="post-51707-score" class="comment-score"></div><div class="comment-text"><p>I have a to-do list item to see if this could be implemented as a Lua script. If someone else wants to take a stab at it they're more than welcome.</p></div><div id="comment-51707-info" class="comment-info"><span class="comment-age">(15 Apr '16, 14:07)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div><span id="51708"></span><div id="comment-51708" class="comment"><div id="post-51708-score" class="comment-score"></div><div class="comment-text"><p>This item seems to be missing from the Qt development <a href="https://wiki.wireshark.org/Development/QtShark">page</a>.</p></div><div id="comment-51708-info" class="comment-info"><span class="comment-age">(15 Apr '16, 15:42)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="51714"></span><div id="comment-51714" class="comment"><div id="post-51714-score" class="comment-score"></div><div class="comment-text"><p>ok, I try it. Thanks very much</p></div><div id="comment-51714-info" class="comment-info"><span class="comment-age">(15 Apr '16, 22:31)</span> <span class="comment-user userinfo">h22</span></div></div><span id="52868"></span><div id="comment-52868" class="comment"><div id="post-52868-score" class="comment-score"></div><div class="comment-text"><blockquote><p>This item seems to be missing from the Qt development <a href="https://wiki.wireshark.org/Development/QtShark">page</a>.</p></blockquote><p>I just added the Tools menu to that page, with an open item for the ACL rules.</p></div><div id="comment-52868-info" class="comment-info"><span class="comment-age">(24 May '16, 11:38)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="52869"></span><div id="comment-52869" class="comment"><div id="post-52869-score" class="comment-score"></div><div class="comment-text"><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12469">Bug 12469</a> filed.</p></div><div id="comment-52869-info" class="comment-info"><span class="comment-age">(24 May '16, 11:41)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-51705" class="comment-tools"></div><div class="clear"></div><div id="comment-51705-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

