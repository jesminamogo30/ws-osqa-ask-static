+++
type = "question"
title = "decode H264 stream with wireshark"
description = '''hi, i have a h264 stream over RTP. i use a wireshark plugin(lua) to extract this h264  ( https://github.com/volvet/h264extractor ) . but after extraction the video does not play correctly, (Distortion in video).can any one help me?'''
date = "2017-05-27T01:26:00Z"
lastmod = "2017-05-27T01:26:00Z"
weight = 61651
keywords = [ "h264", "lua" ]
aliases = [ "/questions/61651" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [decode H264 stream with wireshark](/questions/61651/decode-h264-stream-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61651-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61651-score" class="post-score" title="current number of votes">0</div><span id="post-61651-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, i have a h264 stream over RTP. i use a wireshark plugin(lua) to extract this h264 ( <a href="https://github.com/volvet/h264extractor">https://github.com/volvet/h264extractor</a> ) . but after extraction the video does not play correctly, (Distortion in video).can any one help me?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-h264" rel="tag" title="see questions tagged &#39;h264&#39;">h264</span> <span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '17, 01:26</strong></p><img src="https://secure.gravatar.com/avatar/28d5dc133c31193058a99892f00a0213?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ghader&#39;s gravatar image" /><p><span>ghader</span><br />
<span class="score" title="61 reputation points">61</span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ghader has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 May '17, 01:27</strong> </span></p></div></div><div id="comments-container-61651" class="comments-container"></div><div id="comment-tools-61651" class="comment-tools"></div><div class="clear"></div><div id="comment-61651-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

