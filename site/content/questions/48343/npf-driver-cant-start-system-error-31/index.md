+++
type = "question"
title = "NPF driver can&#x27;t start &quot;System error 31&quot;"
description = '''I have tried every online solution , run as admin, re-install the npf.sys appears in windows/system32/driver and msinfo&amp;gt;software environment&amp;gt;system drivers&amp;gt;npf but the driver can&#x27;t be start there is no NetGroup Packet Filter Driver in my Device manager&amp;gt;Non-Plug and Play driver really nee...'''
date = "2015-12-07T22:18:00Z"
lastmod = "2016-04-11T17:36:00Z"
weight = 48343
keywords = [ "error31", "npfdriver", "npf", "winpcap" ]
aliases = [ "/questions/48343" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [NPF driver can't start "System error 31"](/questions/48343/npf-driver-cant-start-system-error-31)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-48343-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-48343-score" class="post-score" title="current number of votes">0</div><span id="post-48343-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have tried every online solution , run as admin, re-install the npf.sys appears in windows/system32/driver and msinfo&gt;software environment&gt;system drivers&gt;npf but the driver can't be start there is no NetGroup Packet Filter Driver in my Device manager&gt;Non-Plug and Play driver</p><p>really need help with this !!</p><p><img src="http://www.uppic.com/uploads/14495549901.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-error31" rel="tag" title="see questions tagged &#39;error31&#39;">error31</span> <span class="post-tag tag-link-npfdriver" rel="tag" title="see questions tagged &#39;npfdriver&#39;">npfdriver</span> <span class="post-tag tag-link-npf" rel="tag" title="see questions tagged &#39;npf&#39;">npf</span> <span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Dec '15, 22:18</strong></p><img src="https://secure.gravatar.com/avatar/c1892543a887ac875cc66d5f307c157c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Biigg%20Sakun&#39;s gravatar image" /><p><span>Biigg Sakun</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Biigg Sakun has no accepted answers">0%</span></p></img></div></div><div id="comments-container-48343" class="comments-container"></div><div id="comment-tools-48343" class="comment-tools"></div><div class="clear"></div><div id="comment-48343-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51570"></span>

<div id="answer-container-51570" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51570-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51570-score" class="post-score" title="current number of votes">0</div><span id="post-51570-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>SYstem error 31 means:</p><hr /><p>ERROR_GEN_FAILURE</p><p>31 (0x1F)</p><p>A device attached to the system is not functioning.</p><hr /><p>It's very obscure to understand what it actually means.</p><p>You can try Npcap: <a href="https://github.com/nmap/npcap/releases">https://github.com/nmap/npcap/releases</a>, it is a fork based on WinPcap and have built its own drivers. So it will possibly solve your issue.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Apr '16, 17:36</strong></p><img src="https://secure.gravatar.com/avatar/0f8ec58f46e4af3a67f768675c20aac8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Yang%20Luo&#39;s gravatar image" /><p><span>Yang Luo</span><br />
<span class="score" title="91 reputation points">91</span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Yang Luo has one accepted answer">4%</span></p></div></div><div id="comments-container-51570" class="comments-container"></div><div id="comment-tools-51570" class="comment-tools"></div><div class="clear"></div><div id="comment-51570-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

