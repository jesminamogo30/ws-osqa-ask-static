+++
type = "question"
title = "Manual address resolve"
description = '''Hi I don&#x27;t seem to be able to manually resolve addresses in wireshark (ver 1.8.1) anymore. When I bring up the manual resolve dialouge the OK button is greyed out. I &#x27;ve tried various combinations in the Name resolution in preferences but nothing seems to make any difference. Any suggestions?'''
date = "2012-08-30T02:42:00Z"
lastmod = "2012-08-30T06:18:00Z"
weight = 13961
keywords = [ "resolve", "manual", "address" ]
aliases = [ "/questions/13961" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Manual address resolve](/questions/13961/manual-address-resolve)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13961-score" class="post-score" title="current number of votes">0</div><span id="post-13961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I don't seem to be able to manually resolve addresses in wireshark (ver 1.8.1) anymore. When I bring up the manual resolve dialouge the OK button is greyed out. I 've tried various combinations in the Name resolution in preferences but nothing seems to make any difference. Any suggestions?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-resolve" rel="tag" title="see questions tagged &#39;resolve&#39;">resolve</span> <span class="post-tag tag-link-manual" rel="tag" title="see questions tagged &#39;manual&#39;">manual</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Aug '12, 02:42</strong></p><img src="https://secure.gravatar.com/avatar/d6485e947a9adb5eb230d9450bfecb35?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mr_P&#39;s gravatar image" /><p><span>Mr_P</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mr_P has no accepted answers">0%</span></p></div></div><div id="comments-container-13961" class="comments-container"></div><div id="comment-tools-13961" class="comment-tools"></div><div class="clear"></div><div id="comment-13961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13963"></span>

<div id="answer-container-13963" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13963-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13963-score" class="post-score" title="current number of votes">0</div><span id="post-13963-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This works for me. Did you enter an IP address and a host name? The OK button won't be filled in until you do so. Oh, and network name resolution has to be enabled (if it's not enabled then the Manual Address Resolve dialog's checkbox to enable it will be active so you can enable it directly in that dialog).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Aug '12, 06:18</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-13963" class="comments-container"></div><div id="comment-tools-13963" class="comment-tools"></div><div class="clear"></div><div id="comment-13963-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

