+++
type = "question"
title = "Why can&#x27;t I post my question?"
description = '''I have a question that I&#x27;ve attempted to post 10 times with no luck. What am I doing wrong?'''
date = "2017-03-29T09:29:00Z"
lastmod = "2017-03-29T09:32:00Z"
weight = 60411
keywords = [ "general" ]
aliases = [ "/questions/60411" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why can't I post my question?](/questions/60411/why-cant-i-post-my-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60411-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60411-score" class="post-score" title="current number of votes">0</div><span id="post-60411-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a question that I've attempted to post 10 times with no luck. What am I doing wrong?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-general" rel="tag" title="see questions tagged &#39;general&#39;">general</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Mar '17, 09:29</strong></p><img src="https://secure.gravatar.com/avatar/227017211e0730ebab8facb3b68278a0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="profiteam&#39;s gravatar image" /><p><span>profiteam</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="profiteam has no accepted answers">0%</span></p></div></div><div id="comments-container-60411" class="comments-container"></div><div id="comment-tools-60411" class="comment-tools"></div><div class="clear"></div><div id="comment-60411-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60413"></span>

<div id="answer-container-60413" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60413-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60413-score" class="post-score" title="current number of votes">1</div><span id="post-60413-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I just figured out that it didn't like the link I had included in the question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Mar '17, 09:32</strong></p><img src="https://secure.gravatar.com/avatar/227017211e0730ebab8facb3b68278a0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="profiteam&#39;s gravatar image" /><p><span>profiteam</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="profiteam has no accepted answers">0%</span></p></div></div><div id="comments-container-60413" class="comments-container"></div><div id="comment-tools-60413" class="comment-tools"></div><div class="clear"></div><div id="comment-60413-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

