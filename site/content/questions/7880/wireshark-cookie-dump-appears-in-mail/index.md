+++
type = "question"
title = "&quot;wireshark cookie dump&quot; appears in mail"
description = '''I&#x27;ve recently installed Wireshark on my Macbook Pro. Since doing so, whenever I head over to http://jsfiddle.net, all of the input fields are pre-populated with Wireshark Cookie Dump: OKCancel So I went ahead and did a Google search for &quot;Wireshark Cookie Dump&quot; and discovered thousands of results wit...'''
date = "2011-12-09T09:56:00Z"
lastmod = "2011-12-09T10:56:00Z"
weight = 7880
keywords = [ "mail", "cookie", "dump" ]
aliases = [ "/questions/7880" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# ["wireshark cookie dump" appears in mail](/questions/7880/wireshark-cookie-dump-appears-in-mail)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7880-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7880-score" class="post-score" title="current number of votes">0</div><span id="post-7880-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've recently installed Wireshark on my Macbook Pro. Since doing so, whenever I head over to http://jsfiddle.net, all of the input fields are pre-populated with</p><p>Wireshark Cookie Dump:</p><p>OKCancel</p><p>So I went ahead and did a Google search for "Wireshark Cookie Dump" and discovered thousands of results with the exact same problem at various sites across the internets.</p><p>Why on earth is this happening, and how can it be stopped?</p><p>FYI, I'm running Firefox 6.0.2 as my browser, and this doesn't seem to be happening in any of my other browsers (Safari, Opera)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mail" rel="tag" title="see questions tagged &#39;mail&#39;">mail</span> <span class="post-tag tag-link-cookie" rel="tag" title="see questions tagged &#39;cookie&#39;">cookie</span> <span class="post-tag tag-link-dump" rel="tag" title="see questions tagged &#39;dump&#39;">dump</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Dec '11, 09:56</strong></p><img src="https://secure.gravatar.com/avatar/1b06029bc75b7802b9c2b54a4c048230?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="neeraj797&#39;s gravatar image" /><p><span>neeraj797</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="neeraj797 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Dec '11, 10:56</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-7880" class="comments-container"></div><div id="comment-tools-7880" class="comment-tools"></div><div class="clear"></div><div id="comment-7880-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7885"></span>

<div id="answer-container-7885" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7885-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7885-score" class="post-score" title="current number of votes">1</div><span id="post-7885-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's probably happening because the <a href="http://dustint.com/post/12/cookie-injection-using-greasemonkey">Cookie Injector</a> script is either installed on your machine or perhaps being served up to your machine by jsfiddle.net. Wireshark does not itself install that script.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Dec '11, 10:56</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-7885" class="comments-container"></div><div id="comment-tools-7885" class="comment-tools"></div><div class="clear"></div><div id="comment-7885-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

