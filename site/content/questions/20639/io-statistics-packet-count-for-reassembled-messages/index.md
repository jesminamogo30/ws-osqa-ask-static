+++
type = "question"
title = "I/O Statistics packet count for reassembled messages"
description = '''How does Wireshark count packets for reassembled Diameter/SCTP messages? Reassembly is enabled for both layers. If a single Diameter message takes 2 packets, are both packets counted? If the graph is filtered by diameter.cmd.code and this would include multi-packet reassembled messages, are all pack...'''
date = "2013-04-19T10:15:00Z"
lastmod = "2013-04-19T10:15:00Z"
weight = 20639
keywords = [ "reassembly", "statistics", "packet", "diameter" ]
aliases = [ "/questions/20639" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [I/O Statistics packet count for reassembled messages](/questions/20639/io-statistics-packet-count-for-reassembled-messages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20639-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20639-score" class="post-score" title="current number of votes">1</div><span id="post-20639-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How does Wireshark count packets for reassembled Diameter/SCTP messages? Reassembly is enabled for both layers. If a single Diameter message takes 2 packets, are both packets counted? If the graph is filtered by diameter.cmd.code and this would include multi-packet reassembled messages, are all packets counted, or just the first packet (that actually contains the cmd.code)? Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reassembly" rel="tag" title="see questions tagged &#39;reassembly&#39;">reassembly</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-diameter" rel="tag" title="see questions tagged &#39;diameter&#39;">diameter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Apr '13, 10:15</strong></p><img src="https://secure.gravatar.com/avatar/a74b1445bc69c36c507f98d314022292?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lkravitz&#39;s gravatar image" /><p><span>lkravitz</span><br />
<span class="score" title="26 reputation points">26</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lkravitz has no accepted answers">0%</span></p></div></div><div id="comments-container-20639" class="comments-container"></div><div id="comment-tools-20639" class="comment-tools"></div><div class="clear"></div><div id="comment-20639-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

