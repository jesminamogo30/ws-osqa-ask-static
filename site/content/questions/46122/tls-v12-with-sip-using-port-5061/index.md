+++
type = "question"
title = "TLS V1.2 with SIP using port 5061"
description = '''TLS V1.2 with SIP using port 5061 how do I decrypt a tcdump captured from a phone device TLS 1.2 has sip messages using port 5061 but cant see the SIP messages..a friend has decrypted his and I cant get his help in doing mine so please advice me how to decrypt this I have wireshark 1.12.3 on windows...'''
date = "2015-09-24T12:20:00Z"
lastmod = "2015-09-24T12:20:00Z"
weight = 46122
keywords = [ "tls" ]
aliases = [ "/questions/46122" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TLS V1.2 with SIP using port 5061](/questions/46122/tls-v12-with-sip-using-port-5061)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46122-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46122-score" class="post-score" title="current number of votes">0</div><span id="post-46122-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>TLS V1.2 with SIP using port 5061</p><p>how do I decrypt a tcdump captured from a phone device TLS 1.2 has sip messages using port 5061 but cant see the SIP messages..a friend has decrypted his and I cant get his help in doing mine so please advice me how to decrypt this I have wireshark 1.12.3 on windows 7</p><p>thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tls" rel="tag" title="see questions tagged &#39;tls&#39;">tls</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Sep '15, 12:20</strong></p><img src="https://secure.gravatar.com/avatar/f99f7bed11b392bc2db111bca92fe901?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rashidaq&#39;s gravatar image" /><p><span>rashidaq</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rashidaq has no accepted answers">0%</span></p></div></div><div id="comments-container-46122" class="comments-container"></div><div id="comment-tools-46122" class="comment-tools"></div><div class="clear"></div><div id="comment-46122-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

