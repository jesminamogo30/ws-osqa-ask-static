+++
type = "question"
title = "Colorized Packet List"
description = '''Hi I would like to modify the colour frame in the packet list when I click the message in packet bytes pane. The default color of the background is light'''
date = "2014-09-02T00:37:00Z"
lastmod = "2014-09-02T00:37:00Z"
weight = 35923
keywords = [ "rules", "colouring" ]
aliases = [ "/questions/35923" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Colorized Packet List](/questions/35923/colorized-packet-list)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35923-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35923-score" class="post-score" title="current number of votes">0</div><span id="post-35923-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I would like to modify the colour frame in the packet list when I click the message in packet bytes pane. The default color of the background is light</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rules" rel="tag" title="see questions tagged &#39;rules&#39;">rules</span> <span class="post-tag tag-link-colouring" rel="tag" title="see questions tagged &#39;colouring&#39;">colouring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Sep '14, 00:37</strong></p><img src="https://secure.gravatar.com/avatar/5318038b31cc44ad026905167c9b1824?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="steve21&#39;s gravatar image" /><p><span>steve21</span><br />
<span class="score" title="11 reputation points">11</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="steve21 has no accepted answers">0%</span></p></div></div><div id="comments-container-35923" class="comments-container"></div><div id="comment-tools-35923" class="comment-tools"></div><div class="clear"></div><div id="comment-35923-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

