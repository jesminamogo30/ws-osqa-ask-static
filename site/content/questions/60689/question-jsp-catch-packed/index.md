+++
type = "question"
title = "question jsp, catch packed"
description = '''I&#x27;m looking an answer, I catch the packed send from server to online application. Does the program send a file in .jsp format, is there a way to decode the package? Or set options to capture before they are converted in jsp? The application to which the packages are sent is in flash. Sorry for my En...'''
date = "2017-04-09T14:58:00Z"
lastmod = "2017-04-09T14:58:00Z"
weight = 60689
keywords = [ "paxket", "flash", "jsp" ]
aliases = [ "/questions/60689" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [question jsp, catch packed](/questions/60689/question-jsp-catch-packed)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60689-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60689-score" class="post-score" title="current number of votes">0</div><span id="post-60689-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm looking an answer, I catch the packed send from server to online application. Does the program send a file in .jsp format, is there a way to decode the package? Or set options to capture before they are converted in jsp? The application to which the packages are sent is in flash. Sorry for my English, text is from translator;)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-paxket" rel="tag" title="see questions tagged &#39;paxket&#39;">paxket</span> <span class="post-tag tag-link-flash" rel="tag" title="see questions tagged &#39;flash&#39;">flash</span> <span class="post-tag tag-link-jsp" rel="tag" title="see questions tagged &#39;jsp&#39;">jsp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Apr '17, 14:58</strong></p><img src="https://secure.gravatar.com/avatar/737b78782c1111496e9c14503f1e1386?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kalelowicz&#39;s gravatar image" /><p><span>Kalelowicz</span><br />
<span class="score" title="4 reputation points">4</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kalelowicz has no accepted answers">0%</span></p></div></div><div id="comments-container-60689" class="comments-container"></div><div id="comment-tools-60689" class="comment-tools"></div><div class="clear"></div><div id="comment-60689-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

