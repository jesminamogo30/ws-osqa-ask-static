+++
type = "question"
title = "g728 silence bit"
description = '''I am trying to insert silence between voice packets for G728 codec. I need help for the silence hex value. '''
date = "2014-02-12T01:06:00Z"
lastmod = "2014-02-12T01:06:00Z"
weight = 29738
keywords = [ "g728", "rtp" ]
aliases = [ "/questions/29738" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [g728 silence bit](/questions/29738/g728-silence-bit)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29738-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29738-score" class="post-score" title="current number of votes">0</div><span id="post-29738-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to insert silence between voice packets for G728 codec. I need help for the silence hex value.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-g728" rel="tag" title="see questions tagged &#39;g728&#39;">g728</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Feb '14, 01:06</strong></p><img src="https://secure.gravatar.com/avatar/4e7b8d9cab960a29a748b0e218c6af9f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rahulhgowda&#39;s gravatar image" /><p><span>rahulhgowda</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rahulhgowda has no accepted answers">0%</span></p></div></div><div id="comments-container-29738" class="comments-container"></div><div id="comment-tools-29738" class="comment-tools"></div><div class="clear"></div><div id="comment-29738-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

