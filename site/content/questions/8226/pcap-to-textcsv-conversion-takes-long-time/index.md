+++
type = "question"
title = "pcap to text/csv conversion takes long time"
description = '''I have captured traffic in pcap file and trying to convert the pcap file into txt/csv. I am using tshark -V -r infile.pcap &amp;gt; outputfile.txt/csv. I am observing that, the time taken to convert 100MB of pcap file takes more than average 10min on Xeon Dual core 2GB RAM system. This is the performanc...'''
date = "2012-01-04T21:26:00Z"
lastmod = "2012-01-04T21:26:00Z"
weight = 8226
keywords = [ "pcap" ]
aliases = [ "/questions/8226" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [pcap to text/csv conversion takes long time](/questions/8226/pcap-to-textcsv-conversion-takes-long-time)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8226-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8226-score" class="post-score" title="current number of votes">0</div><span id="post-8226-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured traffic in pcap file and trying to convert the pcap file into txt/csv. I am using tshark -V -r infile.pcap &gt; outputfile.txt/csv. I am observing that, the time taken to convert 100MB of pcap file takes more than average 10min on Xeon Dual core 2GB RAM system. This is the performance bottleneck for me as I want the time taken to convert the file at least linear with time taken to capture 100 MB of data on 100 MB NIC.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '12, 21:26</strong></p><img src="https://secure.gravatar.com/avatar/cfea1835076816f7343264e516e5f94c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bytecracker&#39;s gravatar image" /><p><span>bytecracker</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bytecracker has no accepted answers">0%</span></p></div></div><div id="comments-container-8226" class="comments-container"></div><div id="comment-tools-8226" class="comment-tools"></div><div class="clear"></div><div id="comment-8226-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

