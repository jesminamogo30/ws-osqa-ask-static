+++
type = "question"
title = "Download tutorial &quot;Introduction to Wireshark&quot;"
description = '''Hi Administrator I have a very slow internet connection so it is not possible to stream everytime, so I would like to know if it is possible to download the tutorial &quot;Introduction to Wireshark&quot; ?. I look forward to your reply. Regards, Anish'''
date = "2011-01-20T01:51:00Z"
lastmod = "2011-01-21T00:07:00Z"
weight = 1827
keywords = [ "download" ]
aliases = [ "/questions/1827" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Download tutorial "Introduction to Wireshark"](/questions/1827/download-tutorial-introduction-to-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1827-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1827-score" class="post-score" title="current number of votes">0</div><span id="post-1827-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Administrator</p><p>I have a very slow internet connection so it is not possible to stream everytime, so I would like to know if it is possible to download the tutorial "Introduction to Wireshark" ?. I look forward to your reply.</p><p>Regards, Anish</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jan '11, 01:51</strong></p><img src="https://secure.gravatar.com/avatar/5e79034316ef4d452e4d5a17e512b973?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="anishjp&#39;s gravatar image" /><p><span>anishjp</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="anishjp has no accepted answers">0%</span></p></div></div><div id="comments-container-1827" class="comments-container"></div><div id="comment-tools-1827" class="comment-tools"></div><div class="clear"></div><div id="comment-1827-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1835"></span>

<div id="answer-container-1835" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1835-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1835-score" class="post-score" title="current number of votes">0</div><span id="post-1835-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="anishjp has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's this <a href="http://media-2.cacetech.com/video/wireshark/introduction-to-wireshark/Introduction%20To%20Wireshark_controller.swf">SWF file</a>. Hope it helps.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jan '11, 12:23</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1835" class="comments-container"><span id="1843"></span><div id="comment-1843" class="comment"><div id="post-1843-score" class="comment-score"></div><div class="comment-text"><p>Hey Dude I know that its a SWF file!. What i need to know is, how can i download that?, or a similar tutorial which i can download!</p></div><div id="comment-1843-info" class="comment-info"><span class="comment-age">(20 Jan '11, 23:11)</span> <span class="comment-user userinfo">anishjp</span></div></div><span id="1844"></span><div id="comment-1844" class="comment"><div id="post-1844-score" class="comment-score"></div><div class="comment-text"><p>Well dude, that's the URL of the file. wget it.</p></div><div id="comment-1844-info" class="comment-info"><span class="comment-age">(21 Jan '11, 00:07)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-1835" class="comment-tools"></div><div class="clear"></div><div id="comment-1835-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

