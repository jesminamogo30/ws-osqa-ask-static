+++
type = "question"
title = "how to parse custom data filed of one protocol in lua"
description = '''Hi all, when try to parse custom data filed of LLC when ftype is Supervisory Frames, i don&#x27;t know how to just parse data filed of one protocol. Following picture is format of LLC, data field is what i want to parse. 1) Can i get data filed of LLC directly, then call my custom diseector? 2) If 1 is n...'''
date = "2014-07-30T18:19:00Z"
lastmod = "2014-07-30T19:45:00Z"
weight = 35025
keywords = [ "lua", "dissector" ]
aliases = [ "/questions/35025" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to parse custom data filed of one protocol in lua](/questions/35025/how-to-parse-custom-data-filed-of-one-protocol-in-lua)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35025-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35025-score" class="post-score" title="current number of votes">0</div><span id="post-35025-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><pre><code>Hi all,
when try to parse custom data filed of LLC when ftype is Supervisory Frames, i don&#39;t know how to just parse data filed of one protocol. Following picture is format of LLC, data field is what i want to parse.
1) Can i get data filed of LLC directly, then call my custom diseector?
2) If 1 is no, in http://wiki.wireshark.org/Lua/Dissectors, it shows add dissector to table of one UPD port. How could i add my custom dissector? And to which port? 
Thank you.</code></pre><p><img src="http://" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-dissector" rel="tag" title="see questions tagged &#39;dissector&#39;">dissector</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '14, 18:19</strong></p><img src="https://secure.gravatar.com/avatar/7b6f62723a894576f644d5e2f51933e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wireshark_xg&#39;s gravatar image" /><p><span>wireshark_xg</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wireshark_xg has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Jul '14, 19:44</strong> </span></p></div></div><div id="comments-container-35025" class="comments-container"></div><div id="comment-tools-35025" class="comment-tools"></div><div class="clear"></div><div id="comment-35025-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35026"></span>

<div id="answer-container-35026" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35026-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35026-score" class="post-score" title="current number of votes">0</div><span id="post-35026-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><img src="https://osqa-ask.wireshark.org/upfiles/llc-frame-format_2.jpg" alt="alt text" /></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jul '14, 19:45</strong></p><img src="https://secure.gravatar.com/avatar/7b6f62723a894576f644d5e2f51933e8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wireshark_xg&#39;s gravatar image" /><p><span>wireshark_xg</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wireshark_xg has no accepted answers">0%</span></p></img></div></div><div id="comments-container-35026" class="comments-container"></div><div id="comment-tools-35026" class="comment-tools"></div><div class="clear"></div><div id="comment-35026-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

