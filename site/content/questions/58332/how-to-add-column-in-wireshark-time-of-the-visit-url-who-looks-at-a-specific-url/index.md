+++
type = "question"
title = "How to add column in wireshark, time of the visit url, who looks at a specific url ...."
description = ''' As in the main window set to display the time of the visit URL column? If the server with installed wireshaek, people connected on the vpn, then how to display the information in the main window of the program, who looks at a specific url? And how to display the user agent url viewed in the log of ...'''
date = "2016-12-25T22:01:00Z"
lastmod = "2016-12-25T22:01:00Z"
weight = 58332
keywords = [ "column", "costumozed" ]
aliases = [ "/questions/58332" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to add column in wireshark, time of the visit url, who looks at a specific url ....](/questions/58332/how-to-add-column-in-wireshark-time-of-the-visit-url-who-looks-at-a-specific-url)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58332-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58332-score" class="post-score" title="current number of votes">0</div><span id="post-58332-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><ol><li>As in the main window set to display the time of the visit URL column?</li><li>If the server with installed wireshaek, people connected on the vpn, then how to display the information in the main window of the program, who looks at a specific url?</li><li>And how to display the user agent url viewed in the log of the main window of the program?</li><li>If possible fix URLs of images, videos, banners and so on. That is all that load a page of the website or application?</li></ol></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-column" rel="tag" title="see questions tagged &#39;column&#39;">column</span> <span class="post-tag tag-link-costumozed" rel="tag" title="see questions tagged &#39;costumozed&#39;">costumozed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Dec '16, 22:01</strong></p><img src="https://secure.gravatar.com/avatar/ebef6356dd73fc6e18fa2c3340d7fd31?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Saldor&#39;s gravatar image" /><p><span>Saldor</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Saldor has no accepted answers">0%</span></p></div></div><div id="comments-container-58332" class="comments-container"></div><div id="comment-tools-58332" class="comment-tools"></div><div class="clear"></div><div id="comment-58332-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

