+++
type = "question"
title = "Wireshark Network Interface"
description = '''I am not able to see any interface in the interface list in capture options.Should I manually enter an interface?If yes, then what should i enter ? I am using ethernet connection.'''
date = "2011-09-08T22:08:00Z"
lastmod = "2011-09-08T22:43:00Z"
weight = 6227
keywords = [ "networkinterface" ]
aliases = [ "/questions/6227" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Network Interface](/questions/6227/wireshark-network-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6227-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6227-score" class="post-score" title="current number of votes">0</div><span id="post-6227-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am not able to see any interface in the interface list in capture options.Should I manually enter an interface?If yes, then what should i enter ? I am using ethernet connection.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-networkinterface" rel="tag" title="see questions tagged &#39;networkinterface&#39;">networkinterface</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Sep '11, 22:08</strong></p><img src="https://secure.gravatar.com/avatar/1aba5490fedfad91e7a2c50de5d628e1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aditya&#39;s gravatar image" /><p><span>aditya</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aditya has no accepted answers">0%</span></p></div></div><div id="comments-container-6227" class="comments-container"><span id="6228"></span><div id="comment-6228" class="comment"><div id="post-6228-score" class="comment-score"></div><div class="comment-text"><p>Google is your friend here.</p></div><div id="comment-6228-info" class="comment-info"><span class="comment-age">(08 Sep '11, 22:43)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-6227" class="comment-tools"></div><div class="clear"></div><div id="comment-6227-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

