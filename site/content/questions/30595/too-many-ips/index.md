+++
type = "question"
title = "Too many IPs"
description = '''So when I go to run Wireshark and pull an IP address of somebody I am in a call with ON SKYPE, even with a filter on that has worked in the past, it keeps pulling up old IPs of people I am no longer in a call with. I am currently testing it while in a call with NOBODY and it is pulling 6 different I...'''
date = "2014-03-07T23:16:00Z"
lastmod = "2014-03-08T00:25:00Z"
weight = 30595
keywords = [ "skype" ]
aliases = [ "/questions/30595" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Too many IPs](/questions/30595/too-many-ips)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30595-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30595-score" class="post-score" title="current number of votes">0</div><span id="post-30595-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>So when I go to run Wireshark and pull an IP address of somebody I am in a call with ON SKYPE, even with a filter on that has worked in the past, it keeps pulling up old IPs of people I am no longer in a call with. I am currently testing it while in a call with NOBODY and it is pulling 6 different IPs repeatedly as if I were in a call with them right now. How can I clear out the history, or stop this from happening again? I dont want the IPs blacklisted in any way, incase they are those that I might pull in the future, but I want them to not be spammed when I am just trying to pull a single IP of somebody I am in a call with. PLEASE HELP :(</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-skype" rel="tag" title="see questions tagged &#39;skype&#39;">skype</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Mar '14, 23:16</strong></p><img src="https://secure.gravatar.com/avatar/c46fc5a8b4af6a9370ed868b12666758?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Skratch&#39;s gravatar image" /><p><span>Skratch</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Skratch has no accepted answers">0%</span></p></div></div><div id="comments-container-30595" class="comments-container"><span id="30596"></span><div id="comment-30596" class="comment"><div id="post-30596-score" class="comment-score"></div><div class="comment-text"><blockquote><p>How can I clear out the history, or stop this from happening again?</p></blockquote><p>Please ask the vendor of your software how you can do that!!</p></div><div id="comment-30596-info" class="comment-info"><span class="comment-age">(08 Mar '14, 00:25)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-30595" class="comment-tools"></div><div class="clear"></div><div id="comment-30595-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

