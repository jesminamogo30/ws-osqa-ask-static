+++
type = "question"
title = "Why wireshark(v 1.12.4) isn&#x27;t showing menubar in ubuntu(v 14.04)?"
description = '''After downloading wireshark_1.12.4.tar.b2z. I have extracted that and run make and configure commands and opened wireshark it isnot showing menubar as shown in below figure what could be the reason behind that? '''
date = "2015-04-12T02:17:00Z"
lastmod = "2015-04-12T08:02:00Z"
weight = 41391
keywords = [ "installation", "ubuntu", "startup", "wireshark" ]
aliases = [ "/questions/41391" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why wireshark(v 1.12.4) isn't showing menubar in ubuntu(v 14.04)?](/questions/41391/why-wiresharkv-1124-isnt-showing-menubar-in-ubuntuv-1404)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41391-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41391-score" class="post-score" title="current number of votes">0</div><span id="post-41391-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After downloading wireshark_1.12.4.tar.b2z. I have extracted that and run make and configure commands and opened wireshark it isnot showing menubar as shown in below figure what could be the reason behind that? <img src="https://osqa-ask.wireshark.org/upfiles/unknown_erro.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span> <span class="post-tag tag-link-startup" rel="tag" title="see questions tagged &#39;startup&#39;">startup</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Apr '15, 02:17</strong></p><img src="https://secure.gravatar.com/avatar/8efce51fbbf3dbd6c9b9132056f45eb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ankit&#39;s gravatar image" /><p><span>ankit</span><br />
<span class="score" title="65 reputation points">65</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ankit has one accepted answer">25%</span></p></img></div></div><div id="comments-container-41391" class="comments-container"></div><div id="comment-tools-41391" class="comment-tools"></div><div class="clear"></div><div id="comment-41391-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41394"></span>

<div id="answer-container-41394" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41394-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41394-score" class="post-score" title="current number of votes">0</div><span id="post-41394-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The menu should appear when you put the cursor on the menu bar (at least that's how it behaves with my Ubuntu 10.10).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Apr '15, 06:32</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-41394" class="comments-container"><span id="41396"></span><div id="comment-41396" class="comment"><div id="post-41396-score" class="comment-score"></div><div class="comment-text"><p>I have put my cursor though it isnot showing menubar</p></div><div id="comment-41396-info" class="comment-info"><span class="comment-age">(12 Apr '15, 08:02)</span> <span class="comment-user userinfo">ankit</span></div></div></div><div id="comment-tools-41394" class="comment-tools"></div><div class="clear"></div><div id="comment-41394-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

