+++
type = "question"
title = "my network adapter doesn&#x27;t support any wifi hack soft"
description = '''hai guys,my network adapters are:&quot;QUALCOMM ATHEROS AR5BWB222&quot;, and some like &quot;MICROSOFT KERNAL DEBUG NETWORK ADAPTOR&quot;IN WIN 7&amp;amp;WIN 8 had 4gb ram, INTEL I3 PROCESSOR at ACER WITH supported drivers for both respectively and adapters are working fine,I had tried many many wifi hack soft like wire sh...'''
date = "2013-06-09T07:56:00Z"
lastmod = "2013-06-09T08:39:00Z"
weight = 21843
keywords = [ "supported", "drivers", "un", "hack" ]
aliases = [ "/questions/21843" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [my network adapter doesn't support any wifi hack soft](/questions/21843/my-network-adapter-doesnt-support-any-wifi-hack-soft)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21843-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21843-score" class="post-score" title="current number of votes">0</div><span id="post-21843-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hai guys,my network adapters are:"QUALCOMM ATHEROS AR5BWB222", and some like "MICROSOFT KERNAL DEBUG NETWORK ADAPTOR"IN WIN 7&amp;WIN 8 had 4gb ram, INTEL I3 PROCESSOR at ACER WITH supported drivers for both respectively and adapters are working fine,I had tried many many wifi hack soft like <strong><em>wire shark,port analyzer,network stumbler,commview for wifi,air crack,winpcap,airpcap,ether detect,cfos</em></strong>.but neither soft didn't capture any packets,I WANT ANY SOFT THAT CAN CAPTURE PACKETS AND SAVE.BECAUSE WITH THE HELP OF AIRCRACK I CAN CRACK WEP.HELP ME,THOSE ARE <em>REGARDED GREATFULLY BY ME</em></p><p>i knew about backtrack 5,but it's too emberassing for me,advice me other soft.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-supported" rel="tag" title="see questions tagged &#39;supported&#39;">supported</span> <span class="post-tag tag-link-drivers" rel="tag" title="see questions tagged &#39;drivers&#39;">drivers</span> <span class="post-tag tag-link-un" rel="tag" title="see questions tagged &#39;un&#39;">un</span> <span class="post-tag tag-link-hack" rel="tag" title="see questions tagged &#39;hack&#39;">hack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Jun '13, 07:56</strong></p><img src="https://secure.gravatar.com/avatar/79211afa88f63af9b262e88e8d47665d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tyson&#39;s gravatar image" /><p><span>tyson</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tyson has no accepted answers">0%</span></p></div></div><div id="comments-container-21843" class="comments-container"></div><div id="comment-tools-21843" class="comment-tools"></div><div class="clear"></div><div id="comment-21843-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="21844"></span>

<div id="answer-container-21844" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21844-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21844-score" class="post-score" title="current number of votes">0</div><span id="post-21844-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>So... maybe instead of bursting upper case sentences you start by giving us the error message from aircrack when you try to enable monitor mode for your wireless NIC. Since aircrack works charming for almost every hardware I'd really like to know why it is not able to do what you're looking for.</p><p>as I suppose you already know since you already tested aircrack, <a href="http://lmgtfy.com/?q=aircrack+monitor+mode">here</a> is the how-to just in case...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jun '13, 08:32</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Jun '13, 08:32</strong> </span></p></div></div><div id="comments-container-21844" class="comments-container"></div><div id="comment-tools-21844" class="comment-tools"></div><div class="clear"></div><div id="comment-21844-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="21846"></span>

<div id="answer-container-21846" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21846-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21846-score" class="post-score" title="current number of votes">0</div><span id="post-21846-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://www.microsoft.com/en-us/download/details.aspx?id=4865">Network Monitor</a> from MS?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Jun '13, 08:39</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-21846" class="comments-container"></div><div id="comment-tools-21846" class="comment-tools"></div><div class="clear"></div><div id="comment-21846-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

