+++
type = "question"
title = "GPRS-NS in MPLS, MPLS/IP, MPLS/GRE"
description = '''Hello, seems tool dont offer me Decode as &quot;GPRS-NS&quot; protocol inside MPLS packet. In fact we have GPRS-NS packets transferred in MPLS network but can&#x27;t inspect them. I can sent an example of tcpdump file.'''
date = "2012-12-15T05:45:00Z"
lastmod = "2012-12-15T07:07:00Z"
weight = 16926
keywords = [ "gprs-ns", "mpls" ]
aliases = [ "/questions/16926" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [GPRS-NS in MPLS, MPLS/IP, MPLS/GRE](/questions/16926/gprs-ns-in-mpls-mplsip-mplsgre)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16926-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16926-score" class="post-score" title="current number of votes">0</div><span id="post-16926-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, seems tool dont offer me Decode as "GPRS-NS" protocol inside MPLS packet. In fact we have GPRS-NS packets transferred in MPLS network but can't inspect them. I can sent an example of tcpdump file.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gprs-ns" rel="tag" title="see questions tagged &#39;gprs-ns&#39;">gprs-ns</span> <span class="post-tag tag-link-mpls" rel="tag" title="see questions tagged &#39;mpls&#39;">mpls</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Dec '12, 05:45</strong></p><img src="https://secure.gravatar.com/avatar/94bf46c61525840ac1e2b37bc8c27a36?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vlradler&#39;s gravatar image" /><p><span>vlradler</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vlradler has no accepted answers">0%</span></p></div></div><div id="comments-container-16926" class="comments-container"><span id="16927"></span><div id="comment-16927" class="comment"><div id="post-16927-score" class="comment-score"></div><div class="comment-text"><p>Please file an enhancement request at <a href="http://bugs.wireshark.org">bugs.wireshark.org</a> and attach an example capture file.</p></div><div id="comment-16927-info" class="comment-info"><span class="comment-age">(15 Dec '12, 07:07)</span> <span class="comment-user userinfo">Bill Meier ♦♦</span></div></div></div><div id="comment-tools-16926" class="comment-tools"></div><div class="clear"></div><div id="comment-16926-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

