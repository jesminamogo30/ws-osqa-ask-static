+++
type = "question"
title = "RDP session analysis"
description = '''I happened to be capturing traffic on a Windows XP system while someone from China (113.108.139.62) was attempting to hack in over TCP 3389 https://www.cloudshark.org/captures/3bb89c8bbe61 Can RDP traffic captured in this file be presented as a series of screens that were shown to the client? Is the...'''
date = "2015-01-07T09:49:00Z"
lastmod = "2015-01-07T10:04:00Z"
weight = 38927
keywords = [ "rdp" ]
aliases = [ "/questions/38927" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [RDP session analysis](/questions/38927/rdp-session-analysis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38927-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38927-score" class="post-score" title="current number of votes">0</div><span id="post-38927-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I happened to be capturing traffic on a Windows XP system while someone from China (113.108.139.62) was attempting to hack in over TCP 3389</p><p><a href="https://www.cloudshark.org/captures/3bb89c8bbe61">https://www.cloudshark.org/captures/3bb89c8bbe61</a></p><p>Can RDP traffic captured in this file be presented as a series of screens that were shown to the client? Is there a fingerprint of the software used on the hacking system?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rdp" rel="tag" title="see questions tagged &#39;rdp&#39;">rdp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jan '15, 09:49</strong></p><img src="https://secure.gravatar.com/avatar/bcfdf26904f3a8a9fb69c7ca0dc5e7b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="net_tech&#39;s gravatar image" /><p><span>net_tech</span><br />
<span class="score" title="116 reputation points">116</span><span title="30 badges"><span class="badge1">●</span><span class="badgecount">30</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="net_tech has 2 accepted answers">13%</span></p></div></div><div id="comments-container-38927" class="comments-container"></div><div id="comment-tools-38927" class="comment-tools"></div><div class="clear"></div><div id="comment-38927-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38928"></span>

<div id="answer-container-38928" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38928-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38928-score" class="post-score" title="current number of votes">1</div><span id="post-38928-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="net_tech has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That's not an easy task to do, but you may want to read the following blog post:</p><p><a href="http://www.contextis.co.uk/resources/blog/rdp-replay/">http://www.contextis.co.uk/resources/blog/rdp-replay/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Jan '15, 10:04</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-38928" class="comments-container"></div><div id="comment-tools-38928" class="comment-tools"></div><div class="clear"></div><div id="comment-38928-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

