+++
type = "question"
title = "Application Data Packet still doesn&#x27;t decrypted even if correct SSL keys are applied"
description = '''When I looked at the packet content windows, the application layer shows Encrypted application data: &amp;lt;gibberish string&amp;gt;. I did try to check/uncheck the two settings there when importing the ssl keys into the wireshark:  Reassemble SSL records spanning multiple SSL records Reassemble SSL Applic...'''
date = "2017-06-22T02:09:00Z"
lastmod = "2017-06-22T07:05:00Z"
weight = 62228
keywords = [ "ssl", "ssl_decrypt" ]
aliases = [ "/questions/62228" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Application Data Packet still doesn't decrypted even if correct SSL keys are applied](/questions/62228/application-data-packet-still-doesnt-decrypted-even-if-correct-ssl-keys-are-applied)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62228-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62228-score" class="post-score" title="current number of votes">0</div><span id="post-62228-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I looked at the packet content windows, the application layer shows <code>Encrypted application data: &lt;gibberish string&gt;</code>.</p><p>I did try to check/uncheck the two settings there when importing the ssl keys into the wireshark:</p><ul><li>Reassemble SSL records spanning multiple SSL records</li><li>Reassemble SSL Application Data Spanning multiple SSL records</li></ul><p>Still no luck.</p><p>Anyone has any idea why or has experienced this before?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-ssl_decrypt" rel="tag" title="see questions tagged &#39;ssl_decrypt&#39;">ssl_decrypt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Jun '17, 02:09</strong></p><img src="https://secure.gravatar.com/avatar/c1e2e51b48939f05f0d29e40b64909ad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="waikeatahlok&#39;s gravatar image" /><p><span>waikeatahlok</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="waikeatahlok has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>22 Jun '17, 07:04</strong> </span></p><img src="https://secure.gravatar.com/avatar/285b1f0f4caadc088a38c40aea22feba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lekensteyn&#39;s gravatar image" /><p><span>Lekensteyn</span><br />
<span class="score" title="2213 reputation points"><span>2.2k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="24 badges"><span class="bronze">●</span><span class="badgecount">24</span></span></p></div></div><div id="comments-container-62228" class="comments-container"><span id="62235"></span><div id="comment-62235" class="comment"><div id="post-62235-score" class="comment-score"></div><div class="comment-text"><p>What version of Wireshark are you running, and on what platform are you running it?</p></div><div id="comment-62235-info" class="comment-info"><span class="comment-age">(22 Jun '17, 07:05)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-62228" class="comment-tools"></div><div class="clear"></div><div id="comment-62228-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

