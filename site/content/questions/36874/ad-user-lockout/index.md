+++
type = "question"
title = "AD user lockout"
description = '''A Windows 7 users (user A) is using Outlook 2007 to access MS Exchange 2013 mailbox. In addition to the primary mailbox user has a secondary mailbox connected from another user (user B). user B is disabled in AD and user A has full mailbox access to user&#x27;s B mailbox. whenever user A attempts to acce...'''
date = "2014-10-06T13:35:00Z"
lastmod = "2014-10-06T13:35:00Z"
weight = 36874
keywords = [ "authentication", "kerboros" ]
aliases = [ "/questions/36874" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [AD user lockout](/questions/36874/ad-user-lockout)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36874-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36874-score" class="post-score" title="current number of votes">0</div><span id="post-36874-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>A Windows 7 users (user A) is using Outlook 2007 to access MS Exchange 2013 mailbox. In addition to the primary mailbox user has a secondary mailbox connected from another user (user B). user B is disabled in AD and user A has full mailbox access to user's B mailbox.</p><p>whenever user A attempts to access secondary mailbox he locks his AD account out.</p><p>Hotfix published here <a href="http://support2.microsoft.com/kb/2598366">http://support2.microsoft.com/kb/2598366</a> has been applied to user's A computer.</p><p>is there a way to tell what option was requested by the client which received KRB5KDC_ERR_BADOPTION response from the domain controller?</p><p>Thanks you</p><p><img src="https://osqa-ask.wireshark.org/upfiles/lockout.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-authentication" rel="tag" title="see questions tagged &#39;authentication&#39;">authentication</span> <span class="post-tag tag-link-kerboros" rel="tag" title="see questions tagged &#39;kerboros&#39;">kerboros</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Oct '14, 13:35</strong></p><img src="https://secure.gravatar.com/avatar/bcfdf26904f3a8a9fb69c7ca0dc5e7b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="net_tech&#39;s gravatar image" /><p><span>net_tech</span><br />
<span class="score" title="116 reputation points">116</span><span title="30 badges"><span class="badge1">●</span><span class="badgecount">30</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="net_tech has 2 accepted answers">13%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Oct '14, 13:52</strong> </span></p></div></div><div id="comments-container-36874" class="comments-container"></div><div id="comment-tools-36874" class="comment-tools"></div><div class="clear"></div><div id="comment-36874-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

