+++
type = "question"
title = "HL7 Protocols"
description = '''I need to monitor HL7 traffic to see why the connection is dropping. I need to know what protocols to look for from the IP. Any help would be great. Thank You Brian'''
date = "2014-07-23T06:50:00Z"
lastmod = "2014-07-25T01:20:00Z"
weight = 34852
keywords = [ "hl7" ]
aliases = [ "/questions/34852" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [HL7 Protocols](/questions/34852/hl7-protocols)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34852-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34852-score" class="post-score" title="current number of votes">0</div><span id="post-34852-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I need to monitor HL7 traffic to see why the connection is dropping. I need to know what protocols to look for from the IP.</p><p>Any help would be great.</p><p>Thank You Brian</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-hl7" rel="tag" title="see questions tagged &#39;hl7&#39;">hl7</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jul '14, 06:50</strong></p><img src="https://secure.gravatar.com/avatar/d5605527f84c4ff885c8586248a5b470?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bsfoote&#39;s gravatar image" /><p><span>bsfoote</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bsfoote has no accepted answers">0%</span></p></div></div><div id="comments-container-34852" class="comments-container"></div><div id="comment-tools-34852" class="comment-tools"></div><div class="clear"></div><div id="comment-34852-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34906"></span>

<div id="answer-container-34906" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34906-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34906-score" class="post-score" title="current number of votes">0</div><span id="post-34906-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No one can tell you because HL7 (a layer 7 protocol) does not say anything about the underlying transport and/or network layer, it could be anything. That's the beauty of a layered protocol design, but that doesn't help you. So you need to give more specifics about the specific implementation, then maybe someone knows.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jul '14, 01:20</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-34906" class="comments-container"></div><div id="comment-tools-34906" class="comment-tools"></div><div class="clear"></div><div id="comment-34906-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

