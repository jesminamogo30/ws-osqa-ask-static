+++
type = "question"
title = "What installer should I use on Windows 7?"
description = '''Currently, I am working with Windows7. What installer package should I use?'''
date = "2011-12-08T12:33:00Z"
lastmod = "2011-12-08T13:08:00Z"
weight = 7850
keywords = [ "windows7", "installation" ]
aliases = [ "/questions/7850" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [What installer should I use on Windows 7?](/questions/7850/what-installer-should-i-use-on-windows-7)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7850-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7850-score" class="post-score" title="current number of votes">0</div><span id="post-7850-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Currently, I am working with Windows7. What installer package should I use?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-installation" rel="tag" title="see questions tagged &#39;installation&#39;">installation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '11, 12:33</strong></p><img src="https://secure.gravatar.com/avatar/ca12af52c612e3673fbb5ac6ea602015?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gred&#39;s gravatar image" /><p><span>gred</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gred has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Dec '11, 13:03</strong> </span></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span></p></div></div><div id="comments-container-7850" class="comments-container"></div><div id="comment-tools-7850" class="comment-tools"></div><div class="clear"></div><div id="comment-7850-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7851"></span>

<div id="answer-container-7851" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7851-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7851-score" class="post-score" title="current number of votes">1</div><span id="post-7851-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SYN-bit has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You should use either the 32-Bit or 64-Bit Windows installer from the <a href="http://www.wireshark.org/download.html" title="Downloads">Wireshark Downloads</a> page as appropriate for your architecture.</p><p>You can tell whether you are running a 32-Bit or 64-Bit version of Windows by going to <code>Control Panel -&gt; System</code> and checking the <code>System Type</code> value listed there. The 32-Bit version of Wireshark will run quite happily on a 64-Bit version of Windows, so you are also free to install that.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Dec '11, 12:56</strong></p><img src="https://secure.gravatar.com/avatar/fe1cf996b30e896dc95ca3cd47ac7406?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="multipleinterfaces&#39;s gravatar image" /><p><span>multipleinte...</span><br />
<span class="score" title="1321 reputation points"><span>1.3k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="40 badges"><span class="bronze">●</span><span class="badgecount">40</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="multipleinterfaces has 9 accepted answers">12%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>08 Dec '11, 13:11</strong> </span></p></div></div><div id="comments-container-7851" class="comments-container"><span id="7852"></span><div id="comment-7852" class="comment"><div id="post-7852-score" class="comment-score">2</div><div class="comment-text"><p>The 32 bit installer will work whatever your version of Windows. The 64 bit version will only run on 64 bit Windows. Unless you really have a need to open enormous captures, the 32 bit installer will do most folks fine.</p></div><div id="comment-7852-info" class="comment-info"><span class="comment-age">(08 Dec '11, 13:08)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-7851" class="comment-tools"></div><div class="clear"></div><div id="comment-7851-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

