+++
type = "question"
title = "duplicate packets from virtual machine"
description = '''I have a windows 7 virtual machine running on a windows 8.1 host. The virtual network adapter in the virtual machine is bridged to the ethernet adapter on the host. I am using wireshark to monitor the ethernet adapter on the host. It seems that any packets originating from the virtual machine are ca...'''
date = "2016-02-29T15:33:00Z"
lastmod = "2016-02-29T15:33:00Z"
weight = 50590
keywords = [ "machine", "duplicate", "packets", "virtual" ]
aliases = [ "/questions/50590" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [duplicate packets from virtual machine](/questions/50590/duplicate-packets-from-virtual-machine)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50590-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50590-score" class="post-score" title="current number of votes">0</div><span id="post-50590-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a windows 7 virtual machine running on a windows 8.1 host. The virtual network adapter in the virtual machine is bridged to the ethernet adapter on the host. I am using wireshark to monitor the ethernet adapter on the host. It seems that any packets originating from the virtual machine are captured in wireshark twice. What is the reason for this? Can I fix this somehow so that duplicate frames aren't captured?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-machine" rel="tag" title="see questions tagged &#39;machine&#39;">machine</span> <span class="post-tag tag-link-duplicate" rel="tag" title="see questions tagged &#39;duplicate&#39;">duplicate</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-virtual" rel="tag" title="see questions tagged &#39;virtual&#39;">virtual</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Feb '16, 15:33</strong></p><img src="https://secure.gravatar.com/avatar/14ccb8cde6ee9aa4d5d110fbbcbe9db8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="iamthebull&#39;s gravatar image" /><p><span>iamthebull</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="iamthebull has no accepted answers">0%</span></p></div></div><div id="comments-container-50590" class="comments-container"></div><div id="comment-tools-50590" class="comment-tools"></div><div class="clear"></div><div id="comment-50590-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

