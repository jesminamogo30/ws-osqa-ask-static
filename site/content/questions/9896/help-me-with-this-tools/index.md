+++
type = "question"
title = "help me with this tools"
description = '''Hi friends.please help me to solve these questions about wireshark: 1.how can i get communication rate with the other devices,2.a brief explanation about round trip time,throughput and time sequence in their vertical axis,3.a brief explanation of tcp,udp,http packet rate in io graph , 4. analysis of...'''
date = "2012-04-02T05:46:00Z"
lastmod = "2012-04-02T06:06:00Z"
weight = 9896
keywords = [ "ask.wireshark" ]
aliases = [ "/questions/9896" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [help me with this tools](/questions/9896/help-me-with-this-tools)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9896-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9896-score" class="post-score" title="current number of votes">0</div><span id="post-9896-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi friends.please help me to solve these questions about wireshark: 1.how can i get communication rate with the other devices,2.a brief explanation about round trip time,throughput and time sequence in their vertical axis,3.a brief explanation of tcp,udp,http packet rate in io graph , 4. analysis of the network while connecting with a ssl connection and comparing it with the one without ssl,5.analysis of using tcp and udp port addresses. thankx alot</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ask.wireshark" rel="tag" title="see questions tagged &#39;ask.wireshark&#39;">ask.wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Apr '12, 05:46</strong></p><img src="https://secure.gravatar.com/avatar/1afc18cf08cadae99ed3095e02cf57df?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Alisha2265&#39;s gravatar image" /><p><span>Alisha2265</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Alisha2265 has no accepted answers">0%</span></p></div></div><div id="comments-container-9896" class="comments-container"></div><div id="comment-tools-9896" class="comment-tools"></div><div class="clear"></div><div id="comment-9896-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9897"></span>

<div id="answer-container-9897" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9897-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9897-score" class="post-score" title="current number of votes">1</div><span id="post-9897-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Let me guess, homework assignment? :-)</p><ol><li>take a look at Statistics/Conversations</li><li>take a look at Statistics/TCP Stream Graphs</li><li>take a look at Statistics/IO Graph, and try to interpret the axis and settings below the graph</li><li>capture each and you'll see that SSL has a handshake with encryption parameter negotiation; plus you'll be unable to read the remaining packet payload (for example by using Follow TCP Stream)</li><li>probably someone wants you to realize how to filter on tcp and udp ports, like tcp.port==80 etc.</li></ol><p>I'm not trying to lecture you, but you should try to figure some things out by yourself, or your teacher/professor will have no trouble at all testing your skills by asking very simple questions you probably won't be able to answer ;-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Apr '12, 06:06</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-9897" class="comments-container"></div><div id="comment-tools-9897" class="comment-tools"></div><div class="clear"></div><div id="comment-9897-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

