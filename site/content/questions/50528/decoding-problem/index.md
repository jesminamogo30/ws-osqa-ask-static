+++
type = "question"
title = "Decoding problem"
description = '''I have collected *.pcap file (It is contains PTP 1588v2 Sync messages framed on IP/UDP). This file is decoded/recognized correctly (in terms of protocols, destination IP address etc) on one laptop, when if runs on second laptop doesn’t decode protocols and IP (in this case I have noted that under me...'''
date = "2016-02-26T01:24:00Z"
lastmod = "2016-07-21T09:38:00Z"
weight = 50528
keywords = [ "decode" ]
aliases = [ "/questions/50528" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding problem](/questions/50528/decoding-problem)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50528-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50528-score" class="post-score" title="current number of votes">0</div><span id="post-50528-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have collected *.pcap file (It is contains PTP 1588v2 Sync messages framed on IP/UDP). This file is decoded/recognized correctly (in terms of protocols, destination IP address etc) on one laptop, when if runs on second laptop doesn’t decode protocols and IP (in this case I have noted that under menu Analyze/ Decodes As seems not enable). Both wiresharek and WinCap versions are the same 1.12. Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Feb '16, 01:24</strong></p><img src="https://secure.gravatar.com/avatar/48efb9f9be016ebe3736ec3c9ee173d6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LSM&#39;s gravatar image" /><p><span>LSM</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LSM has no accepted answers">0%</span></p></div></div><div id="comments-container-50528" class="comments-container"></div><div id="comment-tools-50528" class="comment-tools"></div><div class="clear"></div><div id="comment-50528-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54225"></span>

<div id="answer-container-54225" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54225-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54225-score" class="post-score" title="current number of votes">0</div><span id="post-54225-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I know this question is a few months old now, but if it's still unresolved, I'd recommend checking and comparing your <code>preferences</code> and <code>decode_as_entries</code> files on the two laptops. These files can be found in the "Personal configuration" folder, which you can locate via: "Help -&gt; About Wireshark -&gt; Folders -&gt; Personal configuration".</p><p>If after that you still can't figure out why Wireshark won't decode the packets as PTP on one laptop, then you could try posting a capture file and provide a link so others can take a look at it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jul '16, 09:38</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-54225" class="comments-container"></div><div id="comment-tools-54225" class="comment-tools"></div><div class="clear"></div><div id="comment-54225-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

