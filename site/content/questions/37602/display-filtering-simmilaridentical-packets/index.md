+++
type = "question"
title = "Display Filtering Simmilar/Identical Packets"
description = '''I am newer to Wireshark and i apologize if this question is a bit naive. I have everything working well and it has been very smooth. What would make it perfect is a way to filter out the repeating results. Instead of getting 50 packets with the same to/from ip addresses, only show that once. So that...'''
date = "2014-11-05T23:16:00Z"
lastmod = "2014-11-05T23:16:00Z"
weight = 37602
keywords = [ "display-filter" ]
aliases = [ "/questions/37602" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Display Filtering Simmilar/Identical Packets](/questions/37602/display-filtering-simmilaridentical-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37602-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37602-score" class="post-score" title="current number of votes">0</div><span id="post-37602-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am newer to Wireshark and i apologize if this question is a bit naive. I have everything working well and it has been very smooth. What would make it perfect is a way to filter out the repeating results. Instead of getting 50 packets with the same to/from ip addresses, only show that once. So that i am left with 6 lines of information with 3 of my ip to destination and 3 ip to me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Nov '14, 23:16</strong></p><img src="https://secure.gravatar.com/avatar/242f49cbc76cd9fb7531022a8a56993b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cajunnotstirred&#39;s gravatar image" /><p><span>cajunnotstirred</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cajunnotstirred has no accepted answers">0%</span></p></div></div><div id="comments-container-37602" class="comments-container"></div><div id="comment-tools-37602" class="comment-tools"></div><div class="clear"></div><div id="comment-37602-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

