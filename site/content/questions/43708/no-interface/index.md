+++
type = "question"
title = "No interface"
description = '''I run LINUX MINT on a HP260 Installed WireShark. It says NO INTERFACE FOUND. Please help. Wim'''
date = "2015-06-30T02:00:00Z"
lastmod = "2015-06-30T03:04:00Z"
weight = 43708
keywords = [ "interface", "found", "no" ]
aliases = [ "/questions/43708" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [No interface](/questions/43708/no-interface)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43708-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43708-score" class="post-score" title="current number of votes">0</div><span id="post-43708-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I run LINUX MINT on a HP260</p><p>Installed WireShark. It says NO INTERFACE FOUND.</p><p>Please help.</p><p>Wim</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interface" rel="tag" title="see questions tagged &#39;interface&#39;">interface</span> <span class="post-tag tag-link-found" rel="tag" title="see questions tagged &#39;found&#39;">found</span> <span class="post-tag tag-link-no" rel="tag" title="see questions tagged &#39;no&#39;">no</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jun '15, 02:00</strong></p><img src="https://secure.gravatar.com/avatar/fb7248ebbb4692b9314b8ccd45a7e5f4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="paysan&#39;s gravatar image" /><p><span>paysan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="paysan has no accepted answers">0%</span></p></div></div><div id="comments-container-43708" class="comments-container"></div><div id="comment-tools-43708" class="comment-tools"></div><div class="clear"></div><div id="comment-43708-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43713"></span>

<div id="answer-container-43713" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43713-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43713-score" class="post-score" title="current number of votes">0</div><span id="post-43713-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Asked and answered before, e.g. <a href="https://ask.wireshark.org/questions/7523/ubuntu-machine-no-interfaces-listed">here</a> and <a href="https://ask.wireshark.org/questions/7976/wireshark-setup-linux-for-nonroot-user">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jun '15, 03:04</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-43713" class="comments-container"></div><div id="comment-tools-43713" class="comment-tools"></div><div class="clear"></div><div id="comment-43713-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

