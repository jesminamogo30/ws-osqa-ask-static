+++
type = "question"
title = "Decryption Key Warning"
description = '''Every time I open Wireshark I get a Decryption key warning. WARNING! Decryption keys specified in Wireshark&#x27;s preferences file differ from those specified for AirPCap adapter. I have uninstalled and reinstalled both Wireshark and AirPCap. I still get the warning.'''
date = "2014-07-20T16:05:00Z"
lastmod = "2014-07-20T16:05:00Z"
weight = 34794
keywords = [ "decryption", "airpcap", "warning" ]
aliases = [ "/questions/34794" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decryption Key Warning](/questions/34794/decryption-key-warning)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34794-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34794-score" class="post-score" title="current number of votes">0</div><span id="post-34794-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Every time I open Wireshark I get a Decryption key warning. WARNING! Decryption keys specified in Wireshark's preferences file differ from those specified for AirPCap adapter. I have uninstalled and reinstalled both Wireshark and AirPCap. I still get the warning.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-airpcap" rel="tag" title="see questions tagged &#39;airpcap&#39;">airpcap</span> <span class="post-tag tag-link-warning" rel="tag" title="see questions tagged &#39;warning&#39;">warning</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jul '14, 16:05</strong></p><img src="https://secure.gravatar.com/avatar/a412941850bb020c6dfd9338308ad7da?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="liquidt007&#39;s gravatar image" /><p><span>liquidt007</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="liquidt007 has no accepted answers">0%</span></p></div></div><div id="comments-container-34794" class="comments-container"></div><div id="comment-tools-34794" class="comment-tools"></div><div class="clear"></div><div id="comment-34794-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

