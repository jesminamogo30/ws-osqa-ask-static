+++
type = "question"
title = "new user need help with layers"
description = '''Can you show me a diagarm that has each layer labeled?  2 data layer 3 Network layer 4 trasnport layer 5 application layer'''
date = "2012-09-11T12:24:00Z"
lastmod = "2012-09-12T02:14:00Z"
weight = 14189
keywords = [ "layers" ]
aliases = [ "/questions/14189" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [new user need help with layers](/questions/14189/new-user-need-help-with-layers)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14189-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14189-score" class="post-score" title="current number of votes">0</div><span id="post-14189-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can you show me a diagarm that has each layer labeled? 2 data layer 3 Network layer 4 trasnport layer 5 application layer</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-layers" rel="tag" title="see questions tagged &#39;layers&#39;">layers</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Sep '12, 12:24</strong></p><img src="https://secure.gravatar.com/avatar/eee4988fdccf2cf12f84dbe2800ac9d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Marcia&#39;s gravatar image" /><p><span>Marcia</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Marcia has no accepted answers">0%</span></p></div></div><div id="comments-container-14189" class="comments-container"></div><div id="comment-tools-14189" class="comment-tools"></div><div class="clear"></div><div id="comment-14189-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="14191"></span>

<div id="answer-container-14191" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14191-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14191-score" class="post-score" title="current number of votes">0</div><span id="post-14191-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Is this what you're looking for?</p><p><a href="http://www.telecommunications-tutorials.com/tutorial-OSI-7-layer-model.htm">http://www.telecommunications-tutorials.com/tutorial-OSI-7-layer-model.htm</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Sep '12, 13:27</strong></p><img src="https://secure.gravatar.com/avatar/9578c59c1cbb579b64d330ef5a62d766?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sreiner&#39;s gravatar image" /><p><span>sreiner</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sreiner has no accepted answers">0%</span></p></div></div><div id="comments-container-14191" class="comments-container"></div><div id="comment-tools-14191" class="comment-tools"></div><div class="clear"></div><div id="comment-14191-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="14199"></span>

<div id="answer-container-14199" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14199-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14199-score" class="post-score" title="current number of votes">0</div><span id="post-14199-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This may be a more applicable <a href="http://www.tcpipguide.com/free/t_TCPIPArchitectureandtheTCPIPModel-2.htm">diagram</a>, it shows the strict OSI model side-by-side to the somewhat loosely defined TCP/IP model.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Sep '12, 02:14</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-14199" class="comments-container"></div><div id="comment-tools-14199" class="comment-tools"></div><div class="clear"></div><div id="comment-14199-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

