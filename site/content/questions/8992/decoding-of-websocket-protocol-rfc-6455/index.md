+++
type = "question"
title = "Decoding of WebSocket protocol (RFC 6455)"
description = '''Is there any plan for WireShark to provide decoding of the new WebSocket protocol (RFC 6455)? This would be most useful for debugging code that uses websockets. At present it appears that websocket traffic gets decoded as malformed packets.'''
date = "2012-02-14T03:00:00Z"
lastmod = "2012-02-14T03:18:00Z"
weight = 8992
keywords = [ "rfc6455", "websockets" ]
aliases = [ "/questions/8992" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Decoding of WebSocket protocol (RFC 6455)](/questions/8992/decoding-of-websocket-protocol-rfc-6455)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8992-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8992-score" class="post-score" title="current number of votes">0</div><span id="post-8992-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any plan for WireShark to provide decoding of the new WebSocket protocol (RFC 6455)? This would be most useful for debugging code that uses websockets. At present it appears that websocket traffic gets decoded as malformed packets.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rfc6455" rel="tag" title="see questions tagged &#39;rfc6455&#39;">rfc6455</span> <span class="post-tag tag-link-websockets" rel="tag" title="see questions tagged &#39;websockets&#39;">websockets</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Feb '12, 03:00</strong></p><img src="https://secure.gravatar.com/avatar/cc2cbbf1ee095b37e4cb23404c57c851?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="theRat&#39;s gravatar image" /><p><span>theRat</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="theRat has no accepted answers">0%</span></p></div></div><div id="comments-container-8992" class="comments-container"></div><div id="comment-tools-8992" class="comment-tools"></div><div class="clear"></div><div id="comment-8992-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8993"></span>

<div id="answer-container-8993" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8993-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8993-score" class="post-score" title="current number of votes">0</div><span id="post-8993-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There doesn't appear to be anything in current trunk or bugzilla. I suggest you file an enhancement request on <a href="https://bugs.wireshark.org/bugzilla/">Wireshark Bugzilla</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Feb '12, 03:18</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-8993" class="comments-container"></div><div id="comment-tools-8993" class="comment-tools"></div><div class="clear"></div><div id="comment-8993-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

