+++
type = "question"
title = "help with .cap file deciphering errors and or issues"
description = '''I have a .cap file that needs to be looked at and im kind of running into a wall in finding what may be errors or traffic that is not suppose to occur or have issues with. i have attached the .cap file please provide me with any assistance thank you! oops how do i attach the .cap file ?'''
date = "2014-01-07T08:47:00Z"
lastmod = "2014-01-12T14:56:00Z"
weight = 28640
keywords = [ "capture", "file" ]
aliases = [ "/questions/28640" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [help with .cap file deciphering errors and or issues](/questions/28640/help-with-cap-file-deciphering-errors-and-or-issues)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28640-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28640-score" class="post-score" title="current number of votes">0</div><span id="post-28640-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a .cap file that needs to be looked at and im kind of running into a wall in finding what may be errors or traffic that is not suppose to occur or have issues with. i have attached the .cap file please provide me with any assistance thank you! oops how do i attach the .cap file ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Jan '14, 08:47</strong></p><img src="https://secure.gravatar.com/avatar/973b97152b94fad4ab62d575c3135c03?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gow75&#39;s gravatar image" /><p><span>gow75</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gow75 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Jan '14, 08:49</strong> </span></p></div></div><div id="comments-container-28640" class="comments-container"><span id="28645"></span><div id="comment-28645" class="comment"><div id="post-28645-score" class="comment-score"></div><div class="comment-text"><p>you could upload it to <a href="http://www.cloudshark.org">http://www.cloudshark.org</a> and post the link here.</p></div><div id="comment-28645-info" class="comment-info"><span class="comment-age">(07 Jan '14, 10:39)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="28830"></span><div id="comment-28830" class="comment"><div id="post-28830-score" class="comment-score"></div><div class="comment-text"><p><span>@gow75</span>: any interest in solving the problem?</p><p>Otherwise we might want to close the question.</p></div><div id="comment-28830-info" class="comment-info"><span class="comment-age">(12 Jan '14, 14:56)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28640" class="comment-tools"></div><div class="clear"></div><div id="comment-28640-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

