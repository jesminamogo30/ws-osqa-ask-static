+++
type = "question"
title = "Is it possible to display 802.11 frames?"
description = '''Hi, I am sniffing traffic on a specific channel in monitor mode and I am normally capturing 802.11 frames without a problem. Yet sometimes I see packets from protocols like ICMP, SSDP, ARP etc. which probably come from a open networks in that channel. What I am trying to do is display only the 802.1...'''
date = "2014-02-25T02:56:00Z"
lastmod = "2014-02-25T02:56:00Z"
weight = 30174
keywords = [ "filter", "802.11", "protocols" ]
aliases = [ "/questions/30174" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to display 802.11 frames?](/questions/30174/is-it-possible-to-display-80211-frames)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30174-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30174-score" class="post-score" title="current number of votes">0</div><span id="post-30174-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am sniffing traffic on a specific channel in monitor mode and I am normally capturing 802.11 frames without a problem. Yet sometimes I see packets from protocols like ICMP, SSDP, ARP etc. which probably come from a open networks in that channel.</p><p>What I am trying to do is display only the 802.11 part as though the data portion of all packets would be impossible to 'translate'. Note I am not trying to filter out frames based on their protocol. I am trying to stop wireshark from going into the extra job of translating the data portion of open networks.</p><p>Is that possible? Thanks for the help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-protocols" rel="tag" title="see questions tagged &#39;protocols&#39;">protocols</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Feb '14, 02:56</strong></p><img src="https://secure.gravatar.com/avatar/4ad359972c0b3475c35dd93f1f8ff259?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BadAcidTrip&#39;s gravatar image" /><p><span>BadAcidTrip</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BadAcidTrip has no accepted answers">0%</span></p></div></div><div id="comments-container-30174" class="comments-container"></div><div id="comment-tools-30174" class="comment-tools"></div><div class="clear"></div><div id="comment-30174-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

