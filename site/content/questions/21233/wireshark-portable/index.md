+++
type = "question"
title = "WireShark Portable"
description = '''Love the program and I was wondering if there is a way to make it to automatically remove Pcap without the prompt when you quit the program.'''
date = "2013-05-17T11:46:00Z"
lastmod = "2013-05-19T15:46:00Z"
weight = 21233
keywords = [ "wireshark" ]
aliases = [ "/questions/21233" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WireShark Portable](/questions/21233/wireshark-portable)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21233-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21233-score" class="post-score" title="current number of votes">0</div><span id="post-21233-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Love the program and I was wondering if there is a way to make it to automatically remove Pcap without the prompt when you quit the program.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 May '13, 11:46</strong></p><img src="https://secure.gravatar.com/avatar/481b8e782d5721b6224dc6d00d262948?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bowmarc&#39;s gravatar image" /><p><span>bowmarc</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bowmarc has no accepted answers">0%</span></p></div></div><div id="comments-container-21233" class="comments-container"></div><div id="comment-tools-21233" class="comment-tools"></div><div class="clear"></div><div id="comment-21233-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="21255"></span>

<div id="answer-container-21255" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21255-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21255-score" class="post-score" title="current number of votes">0</div><span id="post-21255-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to Edit -&gt; Preferences and select the "User Interface" item on the left side tree. Then deselect "Confirm unsaved capture files". That should do it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 May '13, 16:42</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-21255" class="comments-container"><span id="21273"></span><div id="comment-21273" class="comment"><div id="post-21273-score" class="comment-score"></div><div class="comment-text"><p><span>@Jasper</span>, is it possible that the OP is talking about the Portable version of Wireshark removing WinPCap on exit?</p></div><div id="comment-21273-info" class="comment-info"><span class="comment-age">(19 May '13, 14:32)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="21274"></span><div id="comment-21274" class="comment"><div id="post-21274-score" class="comment-score"></div><div class="comment-text"><p>I wasn't aware that there is a prompt to remove WinPCAP when exiting the portable version, so I assumed he's talking about an unsaved capture file instead. I just checked an my 1.8.7 portable version doesn't ask to remove WinPCAP.</p></div><div id="comment-21274-info" class="comment-info"><span class="comment-age">(19 May '13, 14:45)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="21275"></span><div id="comment-21275" class="comment"><div id="post-21275-score" class="comment-score"></div><div class="comment-text"><p>I was just guessing that the portable version might try to clean up in a thorough manner on exit. I don't have a machine that doesn't have Wireshark on to test the portable version.</p></div><div id="comment-21275-info" class="comment-info"><span class="comment-age">(19 May '13, 15:09)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="21277"></span><div id="comment-21277" class="comment"><div id="post-21277-score" class="comment-score"></div><div class="comment-text"><p>If you run Wireshark Portable on a Windows machine that does not have WinPcap installed, Wireshark will offer to install WinPcap. If you allow it do so, it will offer to remove WinPcap when you exit Wireshark.</p><p>If WinPcap is already installed, Wireshark will not offer to install it or to remove it on exit.</p></div><div id="comment-21277-info" class="comment-info"><span class="comment-age">(19 May '13, 15:46)</span> <span class="comment-user userinfo">Jim Aragon</span></div></div></div><div id="comment-tools-21255" class="comment-tools"></div><div class="clear"></div><div id="comment-21255-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

