+++
type = "question"
title = "can Wireshark software work for 10base-T hubs that conforms to IEEE 802.3"
description = '''kindly reply for the following queries :  1) Wireshark software work for 10base-T hubs that conforms to IEEE 802.3 , ethernet. 2) can we get the report of the network load, bandwidth utilization, 3) can we get screen shots for reference.'''
date = "2012-04-19T01:49:00Z"
lastmod = "2012-04-19T01:56:00Z"
weight = 10268
keywords = [ "mb300" ]
aliases = [ "/questions/10268" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can Wireshark software work for 10base-T hubs that conforms to IEEE 802.3](/questions/10268/can-wireshark-software-work-for-10base-t-hubs-that-conforms-to-ieee-8023)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10268-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10268-score" class="post-score" title="current number of votes">0</div><span id="post-10268-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>kindly reply for the following queries :</p><p>1) Wireshark software work for 10base-T hubs that conforms to IEEE 802.3 , ethernet. 2) can we get the report of the network load, bandwidth utilization, 3) can we get screen shots for reference.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mb300" rel="tag" title="see questions tagged &#39;mb300&#39;">mb300</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Apr '12, 01:49</strong></p><img src="https://secure.gravatar.com/avatar/486509368f86593b33bca7821244889b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tech&#39;s gravatar image" /><p><span>tech</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tech has no accepted answers">0%</span></p></div></div><div id="comments-container-10268" class="comments-container"></div><div id="comment-tools-10268" class="comment-tools"></div><div class="clear"></div><div id="comment-10268-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="10269"></span>

<div id="answer-container-10269" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10269-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10269-score" class="post-score" title="current number of votes">0</div><span id="post-10269-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>And another homework assignment... :-)</p><p>1) Sure</p><p>2) yes, take a look at the Statistics menu: I/O Graph, Conversations, End Stations</p><p>3) depending on your Desktop/OS, sure. The I/O Graph can be saved to a picture file as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '12, 01:56</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-10269" class="comments-container"></div><div id="comment-tools-10269" class="comment-tools"></div><div class="clear"></div><div id="comment-10269-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

