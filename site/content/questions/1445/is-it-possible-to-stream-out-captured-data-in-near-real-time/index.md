+++
type = "question"
title = "Is it possible to stream out captured data in near real-time?"
description = '''I would like to be able to use the &quot;follow tcp stream&quot; feature, but instead of saving it to a file, I would like to continuously output the conversation and pipe it into another program. Is this possible? If so, how would I do it?'''
date = "2010-12-21T18:11:00Z"
lastmod = "2010-12-21T22:59:00Z"
weight = 1445
keywords = [ "capture", "output", "stream", "real-time" ]
aliases = [ "/questions/1445" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Is it possible to stream out captured data in near real-time?](/questions/1445/is-it-possible-to-stream-out-captured-data-in-near-real-time)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1445-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1445-score" class="post-score" title="current number of votes">0</div><span id="post-1445-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I would like to be able to use the "follow tcp stream" feature, but instead of saving it to a file, I would like to continuously output the conversation and pipe it into another program. Is this possible? If so, how would I do it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-output" rel="tag" title="see questions tagged &#39;output&#39;">output</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span> <span class="post-tag tag-link-real-time" rel="tag" title="see questions tagged &#39;real-time&#39;">real-time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Dec '10, 18:11</strong></p><img src="https://secure.gravatar.com/avatar/b4e2150a7b07454285f269dfea748f56?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="harrym&#39;s gravatar image" /><p><span>harrym</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="harrym has no accepted answers">0%</span></p></div></div><div id="comments-container-1445" class="comments-container"></div><div id="comment-tools-1445" class="comment-tools"></div><div class="clear"></div><div id="comment-1445-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1448"></span>

<div id="answer-container-1448" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1448-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1448-score" class="post-score" title="current number of votes">0</div><span id="post-1448-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark won't do that for you, maybe netcat or tcpflow might. See <a href="http://wiki.wireshark.org/Tools">the tools wiki page</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Dec '10, 22:59</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1448" class="comments-container"></div><div id="comment-tools-1448" class="comment-tools"></div><div class="clear"></div><div id="comment-1448-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

