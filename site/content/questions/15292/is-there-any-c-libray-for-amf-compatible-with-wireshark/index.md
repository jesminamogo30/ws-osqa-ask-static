+++
type = "question"
title = "Is there any  C libray for amf compatible with wireshark ?????"
description = '''I want a c library for deserializing amf0 and amf3 packet. I have gone through libamfx, as it is in cpp its hard to understand. Is there any other library compatible with wireshark?????? '''
date = "2012-10-26T04:00:00Z"
lastmod = "2012-10-26T04:00:00Z"
weight = 15292
keywords = [ "library", "amf" ]
aliases = [ "/questions/15292" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Is there any C libray for amf compatible with wireshark ?????](/questions/15292/is-there-any-c-libray-for-amf-compatible-with-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15292-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15292-score" class="post-score" title="current number of votes">0</div><span id="post-15292-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want a c library for deserializing amf0 and amf3 packet. I have gone through libamfx, as it is in cpp its hard to understand. Is there any other library compatible with wireshark??????</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-library" rel="tag" title="see questions tagged &#39;library&#39;">library</span> <span class="post-tag tag-link-amf" rel="tag" title="see questions tagged &#39;amf&#39;">amf</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Oct '12, 04:00</strong></p><img src="https://secure.gravatar.com/avatar/b0ed262c234b0aa9fae2e5b2d51b14c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Akhil&#39;s gravatar image" /><p><span>Akhil</span><br />
<span class="score" title="53 reputation points">53</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="28 badges"><span class="silver">●</span><span class="badgecount">28</span></span><span title="31 badges"><span class="bronze">●</span><span class="badgecount">31</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Akhil has no accepted answers">0%</span></p></div></div><div id="comments-container-15292" class="comments-container"></div><div id="comment-tools-15292" class="comment-tools"></div><div class="clear"></div><div id="comment-15292-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

