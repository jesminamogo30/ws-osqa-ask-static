+++
type = "question"
title = "WirelessChipset"
description = '''Hi all. I just installed WireShark today. And in the interfaces menu, I couldnot find my wirelesscard. It could not recognise. I have &quot;Atheros AR5B97&quot; wirelesschipset. I need your help. Ps. I don&#x27;t have a card called MICROSOFT but it shows me. I was seekin&#x27; in this forum. Saw someone was talkin&#x27; abo...'''
date = "2012-06-27T01:18:00Z"
lastmod = "2012-06-27T11:29:00Z"
weight = 12227
keywords = [ "ar5b97", "atheros" ]
aliases = [ "/questions/12227" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WirelessChipset](/questions/12227/wirelesschipset)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12227-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12227-score" class="post-score" title="current number of votes">0</div><span id="post-12227-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all. I just installed WireShark today. And in the interfaces menu, I couldnot find my wirelesscard. It could not recognise. I have "Atheros AR5B97" wirelesschipset. I need your help.</p><p>Ps. I don't have a card called MICROSOFT but it shows me. I was seekin' in this forum. Saw someone was talkin' about this.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ar5b97" rel="tag" title="see questions tagged &#39;ar5b97&#39;">ar5b97</span> <span class="post-tag tag-link-atheros" rel="tag" title="see questions tagged &#39;atheros&#39;">atheros</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Jun '12, 01:18</strong></p><img src="https://secure.gravatar.com/avatar/dd01d6a72d2f28314134ade7a202bb73?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mebeyce&#39;s gravatar image" /><p><span>mebeyce</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mebeyce has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-12227" class="comments-container"></div><div id="comment-tools-12227" class="comment-tools"></div><div class="clear"></div><div id="comment-12227-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12238"></span>

<div id="answer-container-12238" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12238-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12238-score" class="post-score" title="current number of votes">0</div><span id="post-12238-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Can you please post the output of:</p><blockquote><p><code>ipconfig /all</code><br />
<code>dumpcap -D</code><br />
<code>dumpcap -v</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Jun '12, 11:29</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-12238" class="comments-container"></div><div id="comment-tools-12238" class="comment-tools"></div><div class="clear"></div><div id="comment-12238-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

