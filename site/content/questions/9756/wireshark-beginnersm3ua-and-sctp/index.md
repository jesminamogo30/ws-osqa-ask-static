+++
type = "question"
title = "Wireshark beginners(m3ua and sctp)"
description = '''Hello, I want to start capturing m3ua and sctp files but i dont know how to start can anybody out there help me? Thanks, Legrand'''
date = "2012-03-26T01:23:00Z"
lastmod = "2012-03-26T01:47:00Z"
weight = 9756
keywords = [ "sctp", "m3ua" ]
aliases = [ "/questions/9756" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark beginners(m3ua and sctp)](/questions/9756/wireshark-beginnersm3ua-and-sctp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9756-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9756-score" class="post-score" title="current number of votes">0</div><span id="post-9756-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I want to start capturing m3ua and sctp files but i dont know how to start can anybody out there help me? Thanks, Legrand</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sctp" rel="tag" title="see questions tagged &#39;sctp&#39;">sctp</span> <span class="post-tag tag-link-m3ua" rel="tag" title="see questions tagged &#39;m3ua&#39;">m3ua</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Mar '12, 01:23</strong></p><img src="https://secure.gravatar.com/avatar/39e6581ddbc730297526a8b798c9404f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="legrand83&#39;s gravatar image" /><p><span>legrand83</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="legrand83 has no accepted answers">0%</span></p></div></div><div id="comments-container-9756" class="comments-container"></div><div id="comment-tools-9756" class="comment-tools"></div><div class="clear"></div><div id="comment-9756-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9757"></span>

<div id="answer-container-9757" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9757-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9757-score" class="post-score" title="current number of votes">0</div><span id="post-9757-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can filter by <code>gsm_map</code>. This will include both layers, <code>sctp</code> and <code>m3ua</code>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Mar '12, 01:47</strong></p><img src="https://secure.gravatar.com/avatar/aab36e75e2a1b09199da99501429f49e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Eugene%20S&#39;s gravatar image" /><p><span>Eugene S</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Eugene S has no accepted answers">0%</span></p></div></div><div id="comments-container-9757" class="comments-container"></div><div id="comment-tools-9757" class="comment-tools"></div><div class="clear"></div><div id="comment-9757-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

