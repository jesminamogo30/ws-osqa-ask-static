+++
type = "question"
title = "Expert Info (Warn/Undecoded): No extended source address - can&#x27;t decrypt"
description = '''I am getting packets captured 802.15.4 frame which contains 6LOWPAN data. But, wireshark is not able to decrypt it and error in the packet info is: &quot;Expert Info (Warn/Undecoded): No extended source address - can&#x27;t decrypt&quot; The same wireshark being used by my other team mate is able to see packets de...'''
date = "2016-09-19T05:43:00Z"
lastmod = "2016-09-19T05:43:00Z"
weight = 55650
keywords = [ "802.15.4", "6lowpan", "ipv6" ]
aliases = [ "/questions/55650" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Expert Info (Warn/Undecoded): No extended source address - can't decrypt](/questions/55650/expert-info-warnundecoded-no-extended-source-address-cant-decrypt)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55650-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55650-score" class="post-score" title="current number of votes">0</div><span id="post-55650-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am getting packets captured 802.15.4 frame which contains 6LOWPAN data. But, wireshark is not able to decrypt it and error in the packet info is: "Expert Info (Warn/Undecoded): No extended source address - can't decrypt"</p><p>The same wireshark being used by my other team mate is able to see packets decrypted properly. THis is strange. Are there any setting need to be done for this?</p><p>Attached is the image of what I am seeing in wireshark: <img src="https://osqa-ask.wireshark.org/upfiles/Wireshark_SS.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-802.15.4" rel="tag" title="see questions tagged &#39;802.15.4&#39;">802.15.4</span> <span class="post-tag tag-link-6lowpan" rel="tag" title="see questions tagged &#39;6lowpan&#39;">6lowpan</span> <span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Sep '16, 05:43</strong></p><img src="https://secure.gravatar.com/avatar/cb52b7b4b3aefda06953d48de6f9f296?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ketan&#39;s gravatar image" /><p><span>ketan</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ketan has no accepted answers">0%</span></p></img></div></div><div id="comments-container-55650" class="comments-container"></div><div id="comment-tools-55650" class="comment-tools"></div><div class="clear"></div><div id="comment-55650-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

