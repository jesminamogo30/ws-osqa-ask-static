+++
type = "question"
title = "all ip list"
description = '''Hi all, you can show on the screen a list of all active ip in LAN including printers? thank you very much.. greetings'''
date = "2013-01-05T01:55:00Z"
lastmod = "2013-01-05T08:37:00Z"
weight = 17472
keywords = [ "ip", "list" ]
aliases = [ "/questions/17472" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [all ip list](/questions/17472/all-ip-list)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17472-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17472-score" class="post-score" title="current number of votes">0</div><span id="post-17472-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all, you can show on the screen a list of all active ip in LAN including printers? thank you very much.. greetings</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-list" rel="tag" title="see questions tagged &#39;list&#39;">list</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Jan '13, 01:55</strong></p><img src="https://secure.gravatar.com/avatar/2f91944e31b9dd424853f8f2770b76d6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Andy&#39;s gravatar image" /><p><span>Andy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Andy has no accepted answers">0%</span></p></div></div><div id="comments-container-17472" class="comments-container"></div><div id="comment-tools-17472" class="comment-tools"></div><div class="clear"></div><div id="comment-17472-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="17473"></span>

<div id="answer-container-17473" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17473-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17473-score" class="post-score" title="current number of votes">2</div><span id="post-17473-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you set up your capture in a way that you can see all traffic (see: <a href="http://wiki.wireshark.org/CaptureSetup">http://wiki.wireshark.org/CaptureSetup</a>), then wireshark can show you a list of all IP addresses that were seen the network during capture. Go to "Statistics -&gt; Endpoints" and click on the IP tab.</p><p>UPDATE: Or you can use <a href="http://nmap.org/">nmap</a> to actively scan for (live) systems on your network...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jan '13, 03:11</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 Jan '13, 03:13</strong> </span></p></div></div><div id="comments-container-17473" class="comments-container"><span id="17475"></span><div id="comment-17475" class="comment"><div id="post-17475-score" class="comment-score"></div><div class="comment-text"><p>Or if you want to see them in a conversation pair, use Statistics -&gt; Conversations, and click on the IP tab.</p></div><div id="comment-17475-info" class="comment-info"><span class="comment-age">(05 Jan '13, 08:37)</span> <span class="comment-user userinfo">hansangb</span></div></div></div><div id="comment-tools-17473" class="comment-tools"></div><div class="clear"></div><div id="comment-17473-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

