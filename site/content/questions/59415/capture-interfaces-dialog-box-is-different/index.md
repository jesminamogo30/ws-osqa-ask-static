+++
type = "question"
title = "Capture Interfaces dialog box is different"
description = '''I am trying to learn Wireshark. https://www.wireshark.org/docs/wsug_html_chunked/ChCapInterfaceSection.html states that the “Capture Interfaces” dialog box looks like:  When actually running Wireshark, it looking like the following:  I recognize that the manual is 2.1 and the latest is 2.2.4, howeve...'''
date = "2017-02-14T10:18:00Z"
lastmod = "2017-02-15T18:17:00Z"
weight = 59415
keywords = [ "manual", "guide", "beginner" ]
aliases = [ "/questions/59415" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Capture Interfaces dialog box is different](/questions/59415/capture-interfaces-dialog-box-is-different)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59415-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59415-score" class="post-score" title="current number of votes">0</div><span id="post-59415-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to learn Wireshark.</p><p><a href="https://www.wireshark.org/docs/wsug_html_chunked/ChCapInterfaceSection.html">https://www.wireshark.org/docs/wsug_html_chunked/ChCapInterfaceSection.html</a> states that the “Capture Interfaces” dialog box looks like:</p><p><img src="https://www.wireshark.org/docs/wsug_html_chunked/wsug_graphics/ws-capture-interfaces-win32.png" alt="alt text" /></p><p>When actually running Wireshark, it looking like the following:</p><p><img src="https://osqa-ask.wireshark.org/upfiles/Capture_vo0t09l.PNG" alt="alt text" /></p><p>I recognize that the manual is 2.1 and the latest is 2.2.4, however, these look very different.</p><p>How must I modify the instructions on the manual to use 2.2.4 regarding the capture interface dialog? Are there any 2.2.4 specific instructions?</p><p>EDIT. Just realized when I went to the next page <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChCapCaptureOptions.html">https://www.wireshark.org/docs/wsug_html_chunked/ChCapCaptureOptions.html</a> , there was something more similar to what I am seeing. Why didn't I see the “Capture Interfaces” dialog box when selecting <code>Capture → Options…</code>, but am seeing the “Capture Options” dialog box</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-manual" rel="tag" title="see questions tagged &#39;manual&#39;">manual</span> <span class="post-tag tag-link-guide" rel="tag" title="see questions tagged &#39;guide&#39;">guide</span> <span class="post-tag tag-link-beginner" rel="tag" title="see questions tagged &#39;beginner&#39;">beginner</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Feb '17, 10:18</strong></p><img src="https://secure.gravatar.com/avatar/938ce772de83dc07272b66f4b12a5453?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NotionCommotion&#39;s gravatar image" /><p><span>NotionCommotion</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NotionCommotion has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Feb '17, 06:09</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></img></div></div><div id="comments-container-59415" class="comments-container"><span id="59417"></span><div id="comment-59417" class="comment"><div id="post-59417-score" class="comment-score"></div><div class="comment-text"><p>I'm not exactly sure what your question is, but the new Capture -&gt; Options... box contains both the interface as well as the options tabs. They have combined everything into 1 window making it more user friendly than previous versions.</p></div><div id="comment-59417-info" class="comment-info"><span class="comment-age">(14 Feb '17, 10:27)</span> <span class="comment-user userinfo">csereno</span></div></div><span id="59418"></span><div id="comment-59418" class="comment"><div id="post-59418-score" class="comment-score"></div><div class="comment-text"><p>That documentation does look out of date. The wiki might help you more. <a href="https://wiki.wireshark.org/">https://wiki.wireshark.org/</a></p></div><div id="comment-59418-info" class="comment-info"><span class="comment-age">(14 Feb '17, 10:32)</span> <span class="comment-user userinfo">csereno</span></div></div><span id="59419"></span><div id="comment-59419" class="comment"><div id="post-59419-score" class="comment-score"></div><div class="comment-text"><p>That documentation looks out of date <em>for the new GUI</em>. The GTK+ GUI, or legacy GUI as it's sometimes called, does look like the first dialog shown.</p><p>Yes, we are developers first and documentation writers second (or third, fourth, or what not). This problem will be here for a while.</p></div><div id="comment-59419-info" class="comment-info"><span class="comment-age">(14 Feb '17, 11:55)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="59459"></span><div id="comment-59459" class="comment"><div id="post-59459-score" class="comment-score"></div><div class="comment-text"><p>Thanks csereno and Jaap. I am now good. On to the next thing!</p></div><div id="comment-59459-info" class="comment-info"><span class="comment-age">(15 Feb '17, 18:17)</span> <span class="comment-user userinfo">NotionCommotion</span></div></div></div><div id="comment-tools-59415" class="comment-tools"></div><div class="clear"></div><div id="comment-59415-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59433"></span>

<div id="answer-container-59433" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59433-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59433-score" class="post-score" title="current number of votes">2</div><span id="post-59433-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="NotionCommotion has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As you may have noticed the WSUG is a bit out of date. The major difference you'll find is that older screenshots use the old Gtk UI while newer ones use the, well, newer Qt-based UI.</p><p>As for your question, the Qt UI has given us the ability (or otherwise caused us) to rethink a lot of the dialogs and interfaces. So rather than having separate "Capture options" and "Capture interfaces" dialogs there now one unified dialog that serves both functions (less dialogs means simpler which hopefully means easier/more intuitive to use).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Feb '17, 06:12</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-59433" class="comments-container"><span id="59458"></span><div id="comment-59458" class="comment"><div id="post-59458-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jeff. Wasn't sure it it was out of date, or I was misinterpreting. All good now.</p></div><div id="comment-59458-info" class="comment-info"><span class="comment-age">(15 Feb '17, 18:17)</span> <span class="comment-user userinfo">NotionCommotion</span></div></div></div><div id="comment-tools-59433" class="comment-tools"></div><div class="clear"></div><div id="comment-59433-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

