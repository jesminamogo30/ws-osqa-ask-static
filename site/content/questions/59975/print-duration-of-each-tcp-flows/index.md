+++
type = "question"
title = "Print duration of each tcp flows"
description = '''I have a pcap of TCP flows. I want to know which flow last longest. So I am seeking a way to print duration of each TCP flow. Thank in advance'''
date = "2017-03-09T21:19:00Z"
lastmod = "2017-03-09T23:41:00Z"
weight = 59975
keywords = [ "tcp" ]
aliases = [ "/questions/59975" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Print duration of each tcp flows](/questions/59975/print-duration-of-each-tcp-flows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59975-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59975-score" class="post-score" title="current number of votes">0</div><span id="post-59975-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a pcap of TCP flows. I want to know which flow last longest. So I am seeking a way to print duration of each TCP flow.</p><p>Thank in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Mar '17, 21:19</strong></p><img src="https://secure.gravatar.com/avatar/1e2f3c5c40d4244c928115525bf4dd26?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fucai1116&#39;s gravatar image" /><p><span>fucai1116</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fucai1116 has no accepted answers">0%</span></p></div></div><div id="comments-container-59975" class="comments-container"></div><div id="comment-tools-59975" class="comment-tools"></div><div class="clear"></div><div id="comment-59975-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59976"></span>

<div id="answer-container-59976" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59976-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59976-score" class="post-score" title="current number of votes">4</div><span id="post-59976-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="fucai1116 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to "Statistics - Conversations", select "TCP" and sort by "Duration" column:</p><p><img src="https://osqa-ask.wireshark.org/upfiles/tcp_duration_3SfUv5p.jpg" alt="alt text" /></p><p>Then if needed you can right click on the flow, select "Apply as filter - selected - A &lt;--&gt; B" and see this flow in main Wireshark window.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Mar '17, 23:41</strong></p><img src="https://secure.gravatar.com/avatar/1e22670f8d643ca08d658b80a6782932?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Packet_vlad&#39;s gravatar image" /><p><span>Packet_vlad</span><br />
<span class="score" title="436 reputation points">436</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Packet_vlad has 5 accepted answers">20%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Mar '17, 23:47</strong> </span></p></div></div><div id="comments-container-59976" class="comments-container"></div><div id="comment-tools-59976" class="comment-tools"></div><div class="clear"></div><div id="comment-59976-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

