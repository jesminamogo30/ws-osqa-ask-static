+++
type = "question"
title = "how to use wireshark with USB 3G"
description = '''i&#x27;m using USB 3G, i don&#x27;t find how to use wireshark with usb 3g'''
date = "2015-01-31T05:02:00Z"
lastmod = "2015-01-31T07:37:00Z"
weight = 39521
keywords = [ "usb3g" ]
aliases = [ "/questions/39521" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to use wireshark with USB 3G](/questions/39521/how-to-use-wireshark-with-usb-3g)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39521-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39521-score" class="post-score" title="current number of votes">0</div><span id="post-39521-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i'm using USB 3G, i don't find how to use wireshark with usb 3g</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-usb3g" rel="tag" title="see questions tagged &#39;usb3g&#39;">usb3g</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jan '15, 05:02</strong></p><img src="https://secure.gravatar.com/avatar/4358e36ff157880a3c73178db03f204c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="%C4%90%E1%BB%97%20Tr%E1%BA%A7n%20Minh%20Hi%E1%BA%BFu&#39;s gravatar image" /><p><span>Đỗ Trần Minh...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Đỗ Trần Minh Hiếu has no accepted answers">0%</span></p></div></div><div id="comments-container-39521" class="comments-container"></div><div id="comment-tools-39521" class="comment-tools"></div><div class="clear"></div><div id="comment-39521-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39522"></span>

<div id="answer-container-39522" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39522-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39522-score" class="post-score" title="current number of votes">0</div><span id="post-39522-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://wiki.wireshark.org/CaptureSetup/USB">http://wiki.wireshark.org/CaptureSetup/USB</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Jan '15, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-39522" class="comments-container"></div><div id="comment-tools-39522" class="comment-tools"></div><div class="clear"></div><div id="comment-39522-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

