+++
type = "question"
title = "Which VOIP IP is his!!?!"
description = '''So im new to the ip grabber thing and id like to know when i get into a call with 1 friend like alot of ip&#x27;s and stuff show up id like to know how i know which one is his? but i just cant figure it out'''
date = "2017-08-01T21:07:00Z"
lastmod = "2017-08-02T00:23:00Z"
weight = 63318
keywords = [ "ip", "voip" ]
aliases = [ "/questions/63318" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Which VOIP IP is his!!?!](/questions/63318/which-voip-ip-is-his)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63318-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63318-score" class="post-score" title="current number of votes">0</div><span id="post-63318-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>So im new to the ip grabber thing and id like to know when i get into a call with 1 friend like alot of ip's and stuff show up id like to know how i know which one is his? but i just cant figure it out</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-voip" rel="tag" title="see questions tagged &#39;voip&#39;">voip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Aug '17, 21:07</strong></p><img src="https://secure.gravatar.com/avatar/26a64384560a38089194ae72f563a997?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kelpo&#39;s gravatar image" /><p><span>kelpo</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kelpo has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Aug '17, 03:31</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-63318" class="comments-container"><span id="63322"></span><div id="comment-63322" class="comment"><div id="post-63322-score" class="comment-score"></div><div class="comment-text"><p>fr nobody 14 views but no answers or comments...</p></div><div id="comment-63322-info" class="comment-info"><span class="comment-age">(01 Aug '17, 23:54)</span> <span class="comment-user userinfo">kelpo</span></div></div></div><div id="comment-tools-63318" class="comment-tools"></div><div class="clear"></div><div id="comment-63318-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="63324"></span>

<div id="answer-container-63324" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63324-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63324-score" class="post-score" title="current number of votes">0</div><span id="post-63324-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If the "call" from that friend comes in from some network service (Voice-over-IP service, some network game), the chance that it comes directly from that friend's IP address is close to zero. Most likely it comes from one of the IPs of the service, like calls from all other friends in the same service.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Aug '17, 00:23</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-63324" class="comments-container"></div><div id="comment-tools-63324" class="comment-tools"></div><div class="clear"></div><div id="comment-63324-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

