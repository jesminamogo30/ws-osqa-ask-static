+++
type = "question"
title = "Possible to somehow mark a packet permanently?"
description = '''Hello, As far as I know it is possible to mark packets but as the user guide states the changes will not persist after you close wireshark. I would like to know if anyone has figured out a way to bypass that so that. Moreover I would like to know if it possible to mark specific packets groups in spe...'''
date = "2014-11-06T15:45:00Z"
lastmod = "2014-11-06T21:18:00Z"
weight = 37628
keywords = [ "save", "pcap", "tshark", "mark" ]
aliases = [ "/questions/37628" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Possible to somehow mark a packet permanently?](/questions/37628/possible-to-somehow-mark-a-packet-permanently)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37628-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37628-score" class="post-score" title="current number of votes">0</div><span id="post-37628-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>As far as I know it is possible to mark packets but as the user guide states the changes will not persist after you close wireshark. I would like to know if anyone has figured out a way to bypass that so that.</p><p>Moreover I would like to know if it possible to mark specific packets groups in specific ways/tags/colours. For example, packets</p><p>Ideally, at some point I would also like to include this flag at tshark command line and export the marks along with other packet headers into a text file.</p><p>Do you think that it might be possible somehow? Any pointers?</p><p>Much appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-save" rel="tag" title="see questions tagged &#39;save&#39;">save</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-mark" rel="tag" title="see questions tagged &#39;mark&#39;">mark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Nov '14, 15:45</strong></p><img src="https://secure.gravatar.com/avatar/4ad359972c0b3475c35dd93f1f8ff259?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BadAcidTrip&#39;s gravatar image" /><p><span>BadAcidTrip</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BadAcidTrip has no accepted answers">0%</span></p></div></div><div id="comments-container-37628" class="comments-container"></div><div id="comment-tools-37628" class="comment-tools"></div><div class="clear"></div><div id="comment-37628-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="37634"></span>

<div id="answer-container-37634" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37634-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37634-score" class="post-score" title="current number of votes">0</div><span id="post-37634-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No that's not possible and the file format pcap-ng does not have an option to do that I think. But adding a packet comment might give you part of what you want q's that can be saved in an pcap-ng file.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Nov '14, 21:18</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-37634" class="comments-container"></div><div id="comment-tools-37634" class="comment-tools"></div><div class="clear"></div><div id="comment-37634-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

