+++
type = "question"
title = "Export Control"
description = '''Do you have an export control tag/data for the Wireshark product?'''
date = "2012-11-14T10:39:00Z"
lastmod = "2012-11-14T11:15:00Z"
weight = 15909
keywords = [ "control", "export" ]
aliases = [ "/questions/15909" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Export Control](/questions/15909/export-control)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15909-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15909-score" class="post-score" title="current number of votes">0</div><span id="post-15909-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Do you have an export control tag/data for the Wireshark product?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-control" rel="tag" title="see questions tagged &#39;control&#39;">control</span> <span class="post-tag tag-link-export" rel="tag" title="see questions tagged &#39;export&#39;">export</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Nov '12, 10:39</strong></p><img src="https://secure.gravatar.com/avatar/c6f7c91fc0d3c6944191c9593dec1202?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="allybean2001&#39;s gravatar image" /><p><span>allybean2001</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="allybean2001 has no accepted answers">0%</span></p></div></div><div id="comments-container-15909" class="comments-container"></div><div id="comment-tools-15909" class="comment-tools"></div><div class="clear"></div><div id="comment-15909-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15911"></span>

<div id="answer-container-15911" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15911-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15911-score" class="post-score" title="current number of votes">2</div><span id="post-15911-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://www.wireshark.org/export.html">http://www.wireshark.org/export.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Nov '12, 11:15</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-15911" class="comments-container"></div><div id="comment-tools-15911" class="comment-tools"></div><div class="clear"></div><div id="comment-15911-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

