+++
type = "question"
title = "Why doesn&#x27;t Wireshark see my AirPcap device?"
description = '''I have a USB AirPcap device and pcap was installed when I installed wireshark, however when I plug in the device win xp says it finds new hardware and asks for a driver and wireshark does not reconize the device.'''
date = "2014-04-18T08:42:00Z"
lastmod = "2014-04-18T09:14:00Z"
weight = 31967
keywords = [ "9593322" ]
aliases = [ "/questions/31967" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why doesn't Wireshark see my AirPcap device?](/questions/31967/why-doesnt-wireshark-see-my-airpcap-device)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31967-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31967-score" class="post-score" title="current number of votes">0</div><span id="post-31967-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a USB AirPcap device and pcap was installed when I installed wireshark, however when I plug in the device win xp says it finds new hardware and asks for a driver and wireshark does not reconize the device.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-9593322" rel="tag" title="see questions tagged &#39;9593322&#39;">9593322</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '14, 08:42</strong></p><img src="https://secure.gravatar.com/avatar/1bdbb257ec91b4fbb20b024aa8c7fe08?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Thacker&#39;s gravatar image" /><p><span>Paul Thacker</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Thacker has no accepted answers">0%</span></p></div></div><div id="comments-container-31967" class="comments-container"></div><div id="comment-tools-31967" class="comment-tools"></div><div class="clear"></div><div id="comment-31967-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="31968"></span>

<div id="answer-container-31968" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31968-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31968-score" class="post-score" title="current number of votes">0</div><span id="post-31968-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Whoops, found my problem. Wireshark installed pcap 4.3, however that version doesn't support XP. Went back and installed 4.1.1 and that seemed to work so far.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Apr '14, 09:14</strong></p><img src="https://secure.gravatar.com/avatar/1bdbb257ec91b4fbb20b024aa8c7fe08?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Thacker&#39;s gravatar image" /><p><span>Paul Thacker</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Thacker has no accepted answers">0%</span></p></div></div><div id="comments-container-31968" class="comments-container"></div><div id="comment-tools-31968" class="comment-tools"></div><div class="clear"></div><div id="comment-31968-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

