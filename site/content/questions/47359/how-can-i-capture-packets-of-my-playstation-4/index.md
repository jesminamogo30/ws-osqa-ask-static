+++
type = "question"
title = "how can I capture packets of my Playstation 4?"
description = '''Hello, I downloaded Wireshark on my PC Windows 7 Home Premium. In the CAPTURE form is visible only the interface of my PC, ETH2. It&#x27;s not visible the interface ETH1 where connected my PS4. How can I see packets of PS4? Thanks a lot,  Simone.'''
date = "2015-11-07T07:02:00Z"
lastmod = "2015-11-07T10:03:00Z"
weight = 47359
keywords = [ "wireshark" ]
aliases = [ "/questions/47359" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how can I capture packets of my Playstation 4?](/questions/47359/how-can-i-capture-packets-of-my-playstation-4)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47359-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47359-score" class="post-score" title="current number of votes">0</div><span id="post-47359-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I downloaded Wireshark on my PC Windows 7 Home Premium. In the CAPTURE form is visible only the interface of my PC, ETH2. It's not visible the interface ETH1 where connected my PS4. How can I see packets of PS4?</p><p>Thanks a lot, Simone.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Nov '15, 07:02</strong></p><img src="https://secure.gravatar.com/avatar/74d2487e335579decc6e48e046f1e399?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Simone&#39;s gravatar image" /><p><span>Simone</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Simone has no accepted answers">0%</span></p></div></div><div id="comments-container-47359" class="comments-container"><span id="47360"></span><div id="comment-47360" class="comment"><div id="post-47360-score" class="comment-score"></div><div class="comment-text"><p>A good point to start is here: <a href="https://wiki.wireshark.org/CaptureSetup/Ethernet">https://wiki.wireshark.org/CaptureSetup/Ethernet</a></p></div><div id="comment-47360-info" class="comment-info"><span class="comment-age">(07 Nov '15, 10:03)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-47359" class="comment-tools"></div><div class="clear"></div><div id="comment-47359-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

