+++
type = "question"
title = "Presentation Contexts Negotiation"
description = '''There are any way to display the Presentation Contexts Negotiation in wireshark ?'''
date = "2016-12-17T05:16:00Z"
lastmod = "2016-12-18T07:43:00Z"
weight = 58179
keywords = [ "ber", "asn.1", "layer", "presentation", "wireshark" ]
aliases = [ "/questions/58179" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Presentation Contexts Negotiation](/questions/58179/presentation-contexts-negotiation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58179-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58179-score" class="post-score" title="current number of votes">0</div><span id="post-58179-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>There are any way to display the Presentation Contexts Negotiation in wireshark ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ber" rel="tag" title="see questions tagged &#39;ber&#39;">ber</span> <span class="post-tag tag-link-asn.1" rel="tag" title="see questions tagged &#39;asn.1&#39;">asn.1</span> <span class="post-tag tag-link-layer" rel="tag" title="see questions tagged &#39;layer&#39;">layer</span> <span class="post-tag tag-link-presentation" rel="tag" title="see questions tagged &#39;presentation&#39;">presentation</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Dec '16, 05:16</strong></p><img src="https://secure.gravatar.com/avatar/d73efcf999c160e68b35d389609c4dd2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="IBrahim%20El-Khalil&#39;s gravatar image" /><p><span>IBrahim El-K...</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="IBrahim El-Khalil has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>17 Dec '16, 13:57</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-58179" class="comments-container"><span id="58210"></span><div id="comment-58210" class="comment"><div id="post-58210-score" class="comment-score"></div><div class="comment-text"><p>You're talking DICOM, right?</p></div><div id="comment-58210-info" class="comment-info"><span class="comment-age">(18 Dec '16, 06:52)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="58212"></span><div id="comment-58212" class="comment"><div id="post-58212-score" class="comment-score"></div><div class="comment-text"><p>no, i talk in general , i want to show the context presentation (ASN.1 and bre)</p></div><div id="comment-58212-info" class="comment-info"><span class="comment-age">(18 Dec '16, 07:43)</span> <span class="comment-user userinfo">IBrahim El-K...</span></div></div></div><div id="comment-tools-58179" class="comment-tools"></div><div class="clear"></div><div id="comment-58179-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

