+++
type = "question"
title = "RTP stream repetitively shown in SIP call flow with Wireshark 1.8.2"
description = '''Hi, I&#x27;ve been using call flow window of wireshark since quite a long time. It used to show the RTP stream as one item. It worked perfect and the only thing I was missing was the time stamp. Now I&#x27;ve updated to version 1.8.2 of Wireshark and since then it shows the RTP stream repetitevly, even though...'''
date = "2012-08-23T01:46:00Z"
lastmod = "2012-08-23T11:05:00Z"
weight = 13838
keywords = [ "sip", "flow" ]
aliases = [ "/questions/13838" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RTP stream repetitively shown in SIP call flow with Wireshark 1.8.2](/questions/13838/rtp-stream-repetitively-shown-in-sip-call-flow-with-wireshark-182)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13838-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13838-score" class="post-score" title="current number of votes">0</div><span id="post-13838-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I've been using call flow window of wireshark since quite a long time. It used to show the RTP stream as one item. It worked perfect and the only thing I was missing was the time stamp. Now I've updated to version 1.8.2 of Wireshark and since then it shows the RTP stream repetitevly, even though it's clearly the same stream, which can be seen at the time stamp. It's kind of annoying because now a lot of scrolling is necessary to get to the SIP messages. Does anybody know if this is a bug? If so, where could I report? thanks in advance Roland</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-flow" rel="tag" title="see questions tagged &#39;flow&#39;">flow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '12, 01:46</strong></p><img src="https://secure.gravatar.com/avatar/52ad30c06760b5d9ee6ddd1c881db694?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rolstein&#39;s gravatar image" /><p><span>rolstein</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rolstein has no accepted answers">0%</span></p></div></div><div id="comments-container-13838" class="comments-container"></div><div id="comment-tools-13838" class="comment-tools"></div><div class="clear"></div><div id="comment-13838-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13849"></span>

<div id="answer-container-13849" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13849-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13849-score" class="post-score" title="current number of votes">0</div><span id="post-13849-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I have just tried in V1.8.2 and it is exactly as it has always been. It just lists one RTP packet for each stream. Can you post a screenshot of what you are seeing?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Aug '12, 11:05</strong></p><img src="https://secure.gravatar.com/avatar/030196d67dc4e2b8f4ecff65eefdb63e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KeithFrench&#39;s gravatar image" /><p><span>KeithFrench</span><br />
<span class="score" title="121 reputation points">121</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KeithFrench has no accepted answers">0%</span></p></div></div><div id="comments-container-13849" class="comments-container"></div><div id="comment-tools-13849" class="comment-tools"></div><div class="clear"></div><div id="comment-13849-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

