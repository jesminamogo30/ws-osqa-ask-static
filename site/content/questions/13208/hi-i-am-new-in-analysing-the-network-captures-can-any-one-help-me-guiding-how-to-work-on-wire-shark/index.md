+++
type = "question"
title = "Hi, I am new in analysing the network captures. Can any one help me guiding how to work on Wire shark"
description = '''Hi, Can any one help me guiding how to work on Wire shark'''
date = "2012-07-31T23:14:00Z"
lastmod = "2012-08-01T04:17:00Z"
weight = 13208
keywords = [ "help" ]
aliases = [ "/questions/13208" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Hi, I am new in analysing the network captures. Can any one help me guiding how to work on Wire shark](/questions/13208/hi-i-am-new-in-analysing-the-network-captures-can-any-one-help-me-guiding-how-to-work-on-wire-shark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13208-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13208-score" class="post-score" title="current number of votes">0</div><span id="post-13208-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Can any one help me guiding how to work on Wire shark</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 Jul '12, 23:14</strong></p><img src="https://secure.gravatar.com/avatar/7808fbf17ad6c9b02c373590b7b33dd2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pavan1621&#39;s gravatar image" /><p><span>pavan1621</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pavan1621 has no accepted answers">0%</span></p></div></div><div id="comments-container-13208" class="comments-container"></div><div id="comment-tools-13208" class="comment-tools"></div><div class="clear"></div><div id="comment-13208-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="13230"></span>

<div id="answer-container-13230" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-13230-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-13230-score" class="post-score" title="current number of votes">0</div><span id="post-13230-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are plenty of resources listed on the <a href="http://www.wireshark.org/docs/">Wireshark website</a>. I suggest you study them or enrole in a Wireshark University course.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Aug '12, 04:17</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-13230" class="comments-container"></div><div id="comment-tools-13230" class="comment-tools"></div><div class="clear"></div><div id="comment-13230-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

