+++
type = "question"
title = "Interested in capturing wifi packets in promiscous mode on Windows?"
description = '''I am making a program that takes packets from one network source and sending it to receivers, also with other protocols.  It would in particular allow to see packets in wireshark on windows if you can capture it e.g. with Commview WiFi drivers (they support many cards with atheros chipset). Or captu...'''
date = "2012-11-15T17:02:00Z"
lastmod = "2012-11-15T17:02:00Z"
weight = 15948
keywords = [ "windows", "monior-mode", "capturing" ]
aliases = [ "/questions/15948" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Interested in capturing wifi packets in promiscous mode on Windows?](/questions/15948/interested-in-capturing-wifi-packets-in-promiscous-mode-on-windows)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15948-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15948-score" class="post-score" title="current number of votes">0</div><span id="post-15948-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am making a program that takes packets from one network source and sending it to receivers, also with other protocols.</p><p>It would in particular allow to see packets in wireshark on windows if you can capture it e.g. with Commview WiFi drivers (they support many cards with atheros chipset). Or capture packets in backtrack on VM with Windows as host OS (you can forward your network cart to the VM, but only if it's a USB device and you are using VMWare VM). Or using aircrack on windows. The main thing is that you have any program that can capture packets and send them over network.</p><p>Is someone interested?</p><p>The only way i found out how wireshark can get packets not from WinPCAP is emulating RPCAP server - which loses many data about the packets (speed/signal strength) and is not documented at all.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-windows" rel="tag" title="see questions tagged &#39;windows&#39;">windows</span> <span class="post-tag tag-link-monior-mode" rel="tag" title="see questions tagged &#39;monior-mode&#39;">monior-mode</span> <span class="post-tag tag-link-capturing" rel="tag" title="see questions tagged &#39;capturing&#39;">capturing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Nov '12, 17:02</strong></p><img src="https://secure.gravatar.com/avatar/a00c8faaeb8a556d49ded6b3aa1eb51e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="xpeh&#39;s gravatar image" /><p><span>xpeh</span><br />
<span class="score" title="-3 reputation points">-3</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="xpeh has no accepted answers">0%</span></p></div></div><div id="comments-container-15948" class="comments-container"></div><div id="comment-tools-15948" class="comment-tools"></div><div class="clear"></div><div id="comment-15948-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

