+++
type = "question"
title = "can&#x27;t capture 11n data packet"
description = '''I have wireshark 1.0.5. I can capture 802.11g/a data packet but can&#x27;t capture 802.11n data packet. is it normal?'''
date = "2011-08-26T17:45:00Z"
lastmod = "2011-08-26T20:09:00Z"
weight = 5895
keywords = [ "brcmbg" ]
aliases = [ "/questions/5895" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [can't capture 11n data packet](/questions/5895/cant-capture-11n-data-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-5895-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-5895-score" class="post-score" title="current number of votes">0</div><span id="post-5895-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have wireshark 1.0.5. I can capture 802.11g/a data packet but can't capture 802.11n data packet. is it normal?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-brcmbg" rel="tag" title="see questions tagged &#39;brcmbg&#39;">brcmbg</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Aug '11, 17:45</strong></p><img src="https://secure.gravatar.com/avatar/c130c4a47c97445138541da301566802?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bgu&#39;s gravatar image" /><p><span>bgu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bgu has no accepted answers">0%</span></p></div></div><div id="comments-container-5895" class="comments-container"><span id="5898"></span><div id="comment-5898" class="comment"><div id="post-5898-score" class="comment-score"></div><div class="comment-text"><p>What does the "brcmbg" tag mean? I presume it doesn't mean your machine has a Broadcom 802.11b/802.11g adapter; if it did, the reason why you can't capture 802.11n packets would be obvious, i.e. "your adapter doesn't support 802.11n, just 802.11b and 802.11g".</p><p>What type of wireless network adapter <em>do</em> you have?</p></div><div id="comment-5898-info" class="comment-info"><span class="comment-age">(26 Aug '11, 20:09)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-5895" class="comment-tools"></div><div class="clear"></div><div id="comment-5895-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

