+++
type = "question"
title = "Missing data in PCAP.  What am I doing wrong?"
description = '''I have a pcap file saved to my pc. If I provide this pcap to someone else, they can open it and filter for SIP and Q931 data and see the info. If I open it, I only see UDP data and my filter for SIP and Q931 filters zero results. I&#x27;ve tried this on version .99, 1.2.2, 1.6.2 and 1.6.3. The person who...'''
date = "2011-11-08T13:56:00Z"
lastmod = "2011-11-08T14:30:00Z"
weight = 7292
keywords = [ "q931", "sip", "pcap" ]
aliases = [ "/questions/7292" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Missing data in PCAP. What am I doing wrong?](/questions/7292/missing-data-in-pcap-what-am-i-doing-wrong)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7292-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7292-score" class="post-score" title="current number of votes">0</div><span id="post-7292-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a pcap file saved to my pc. If I provide this pcap to someone else, they can open it and filter for SIP and Q931 data and see the info. If I open it, I only see UDP data and my filter for SIP and Q931 filters zero results. I've tried this on version .99, 1.2.2, 1.6.2 and 1.6.3. The person who can see the data is also running 1.6.2. Has anyone run into this as well?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-q931" rel="tag" title="see questions tagged &#39;q931&#39;">q931</span> <span class="post-tag tag-link-sip" rel="tag" title="see questions tagged &#39;sip&#39;">sip</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '11, 13:56</strong></p><img src="https://secure.gravatar.com/avatar/018f9a9aad09d5a0a4e2dc178412628c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="David%20Gray&#39;s gravatar image" /><p><span>David Gray</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="David Gray has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Feb '12, 19:21</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-7292" class="comments-container"></div><div id="comment-tools-7292" class="comment-tools"></div><div class="clear"></div><div id="comment-7292-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7293"></span>

<div id="answer-container-7293" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7293-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7293-score" class="post-score" title="current number of votes">0</div><span id="post-7293-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Most probably your preferences are different from the preferences of the other person. You might want to create a new profile with default settings to see whether that makes a difference.</p><p>You can do this by creating a new profile that is based on the profile "Default"</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Nov '11, 14:03</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-7293" class="comments-container"><span id="7296"></span><div id="comment-7296" class="comment"><div id="post-7296-score" class="comment-score"></div><div class="comment-text"><p>Hmmm.. I was using default already. I tried setting another profile, but that didn't work either. I did go find an old pcap file to make sure I wasn't filtering out SIP, and could see that data fine. This is bazaar...</p></div><div id="comment-7296-info" class="comment-info"><span class="comment-age">(08 Nov '11, 14:30)</span> <span class="comment-user userinfo">David Gray</span></div></div></div><div id="comment-tools-7293" class="comment-tools"></div><div class="clear"></div><div id="comment-7293-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

