+++
type = "question"
title = "Filter a password from a client that use a freemailprovider?"
description = '''How can I for example filter a password from I client that is loging on www.hotmail.com? Which filter do I use?'''
date = "2012-10-23T04:58:00Z"
lastmod = "2012-10-27T04:59:00Z"
weight = 15185
keywords = [ "password" ]
aliases = [ "/questions/15185" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Filter a password from a client that use a freemailprovider?](/questions/15185/filter-a-password-from-a-client-that-use-a-freemailprovider)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15185-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15185-score" class="post-score" title="current number of votes">0</div><span id="post-15185-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I for example filter a password from I client that is loging on <a href="http://www.hotmail.com">www.hotmail.com</a>? Which filter do I use?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-password" rel="tag" title="see questions tagged &#39;password&#39;">password</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Oct '12, 04:58</strong></p><img src="https://secure.gravatar.com/avatar/138aa869c0284e18802057d83c031754?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="marsal&#39;s gravatar image" /><p><span>marsal</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="marsal has no accepted answers">0%</span></p></div></div><div id="comments-container-15185" class="comments-container"><span id="15188"></span><div id="comment-15188" class="comment"><div id="post-15188-score" class="comment-score"></div><div class="comment-text"><p>what do you want to achieve?</p></div><div id="comment-15188-info" class="comment-info"><span class="comment-age">(23 Oct '12, 05:37)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="15288"></span><div id="comment-15288" class="comment"><div id="post-15288-score" class="comment-score"></div><div class="comment-text"><p>No at the school we got the work,nothing illegal at all. The qestion is if that is possible to see or not (using Wireshark). I have got my virtual clients I have to log on my own Mail account and see if these informations are noticed and readable (to human eye) by Wireshark!? Thanks</p></div><div id="comment-15288-info" class="comment-info"><span class="comment-age">(26 Oct '12, 01:35)</span> <span class="comment-user userinfo">marsal</span></div></div></div><div id="comment-tools-15185" class="comment-tools"></div><div class="clear"></div><div id="comment-15185-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="15186"></span>

<div id="answer-container-15186" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-15186-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-15186-score" class="post-score" title="current number of votes">1</div><span id="post-15186-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You probably can't, simply because - even if you manage to capture the relevant packets - they'll be SSL encrypted. You can decrypt them if you get the SSL encryption keys, but my guess is that you're trying to take a peek at passwords that are not your own, so you can't.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Oct '12, 05:04</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-15186" class="comments-container"><span id="15289"></span><div id="comment-15289" class="comment"><div id="post-15289-score" class="comment-score"></div><div class="comment-text"><p>No at the school we got the work,nothing illegal at all. The qestion is if that is possible to see or not (using Wireshark). I have got my virtual clients I have to log on my own Mail account and see if these informations are noticed and readable (to human eye) by Wireshark!? Thanks</p></div><div id="comment-15289-info" class="comment-info"><span class="comment-age">(26 Oct '12, 01:35)</span> <span class="comment-user userinfo">marsal</span></div></div><span id="15290"></span><div id="comment-15290" class="comment"><div id="post-15290-score" class="comment-score">1</div><div class="comment-text"><p>yes if the protocol is unencrypted AND you are able to capture the packets (which can be tricky to do in switched networks). And no if the protocol is encrypted, like https or imap/s, smtp/s, pop3/s.</p></div><div id="comment-15290-info" class="comment-info"><span class="comment-age">(26 Oct '12, 03:04)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="15306"></span><div id="comment-15306" class="comment"><div id="post-15306-score" class="comment-score"></div><div class="comment-text"><p>Can you tell me please which filter do I use?</p></div><div id="comment-15306-info" class="comment-info"><span class="comment-age">(27 Oct '12, 00:24)</span> <span class="comment-user userinfo">marsal</span></div></div><span id="15310"></span><div id="comment-15310" class="comment"><div id="post-15310-score" class="comment-score">1</div><div class="comment-text"><p>It depends on the protocol used, but if it is HTTP (not HTTPS) you could filter on things like http.request.method=="POST" to see all data transfer requests that use the POST method (which is most common for forms being transmitted). Obviously, this won't work for encrypted HTTP.</p><p>After filtering on the POST requests you need to investigate if any of them contains password form details; usually this happens in one of the first posts since the login happens early in the process of receiving mails.</p></div><div id="comment-15310-info" class="comment-info"><span class="comment-age">(27 Oct '12, 02:44)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="15312"></span><div id="comment-15312" class="comment"><div id="post-15312-score" class="comment-score"></div><div class="comment-text"><p>Thanks a lot!</p></div><div id="comment-15312-info" class="comment-info"><span class="comment-age">(27 Oct '12, 04:59)</span> <span class="comment-user userinfo">marsal</span></div></div></div><div id="comment-tools-15186" class="comment-tools"></div><div class="clear"></div><div id="comment-15186-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

