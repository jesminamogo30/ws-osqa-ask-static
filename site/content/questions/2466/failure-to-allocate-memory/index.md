+++
type = "question"
title = "Failure to Allocate Memory?"
description = '''I own a gaming server company with many connections. In one minute time the file size gets to be from 70MB to 190MB, which is no issue when it prunes the last files. But the program after a while does not buffer the files. I will select 7 and it will continue until an error comes up. Failure to allo...'''
date = "2011-02-21T21:19:00Z"
lastmod = "2011-02-22T11:17:00Z"
weight = 2466
keywords = [ "keithm" ]
aliases = [ "/questions/2466" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Failure to Allocate Memory?](/questions/2466/failure-to-allocate-memory)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2466-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2466-score" class="post-score" title="current number of votes">0</div><span id="post-2466-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I own a gaming server company with many connections. In one minute time the file size gets to be from 70MB to 190MB, which is no issue when it prunes the last files. But the program after a while does not buffer the files. I will select 7 and it will continue until an error comes up. Failure to allocate memory. Can someone please help?</p><p>Thank you for your time.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-keithm" rel="tag" title="see questions tagged &#39;keithm&#39;">keithm</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Feb '11, 21:19</strong></p><img src="https://secure.gravatar.com/avatar/492fe6accb7e6f72a24fe643478a1cb2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="froZend&#39;s gravatar image" /><p><span>froZend</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="froZend has no accepted answers">0%</span></p></div></div><div id="comments-container-2466" class="comments-container"><span id="2468"></span><div id="comment-2468" class="comment"><div id="post-2468-score" class="comment-score"></div><div class="comment-text"><p>Have you read <a href="http://wiki.wireshark.org/KnownBugs/OutOfMemory">http://wiki.wireshark.org/KnownBugs/OutOfMemory</a></p></div><div id="comment-2468-info" class="comment-info"><span class="comment-age">(21 Feb '11, 22:21)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="2492"></span><div id="comment-2492" class="comment"><div id="post-2492-score" class="comment-score"></div><div class="comment-text"><p>Thank you Jaap</p></div><div id="comment-2492-info" class="comment-info"><span class="comment-age">(22 Feb '11, 11:17)</span> <span class="comment-user userinfo">froZend</span></div></div></div><div id="comment-tools-2466" class="comment-tools"></div><div class="clear"></div><div id="comment-2466-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

