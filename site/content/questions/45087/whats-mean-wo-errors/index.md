+++
type = "question"
title = "what&#x27;s mean w/o errors?"
description = '''what&#x27;s mean w/o errors? menu - capture - interface - details - statistics Directed bytes transmitted w/o erros Directed pakets transmitted w/o erros i have a very many count w/o erros What&#x27;s mean w/o? is this without?'''
date = "2015-08-13T20:24:00Z"
lastmod = "2015-08-14T07:37:00Z"
weight = 45087
keywords = [ "statistics" ]
aliases = [ "/questions/45087" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [what's mean w/o errors?](/questions/45087/whats-mean-wo-errors)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45087-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45087-score" class="post-score" title="current number of votes">0</div><span id="post-45087-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what's mean w/o errors?</p><p>menu - capture - interface - details - statistics</p><p>Directed bytes transmitted w/o erros Directed pakets transmitted w/o erros</p><p>i have a very many count w/o erros What's mean w/o? is this without?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '15, 20:24</strong></p><img src="https://secure.gravatar.com/avatar/aa856757f60178a3bbbb12a4068e45bc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="beginerkd&#39;s gravatar image" /><p><span>beginerkd</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="beginerkd has no accepted answers">0%</span></p></div></div><div id="comments-container-45087" class="comments-container"></div><div id="comment-tools-45087" class="comment-tools"></div><div class="clear"></div><div id="comment-45087-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45112"></span>

<div id="answer-container-45112" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45112-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45112-score" class="post-score" title="current number of votes">1</div><span id="post-45112-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, w/o means without e.g. Directed bytes transmitted without errors.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Aug '15, 07:37</strong></p><img src="https://secure.gravatar.com/avatar/721b9692d2a30fc3b386b7fae9a44220?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Roland&#39;s gravatar image" /><p><span>Roland</span><br />
<span class="score" title="764 reputation points">764</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Roland has 9 accepted answers">13%</span></p></div></div><div id="comments-container-45112" class="comments-container"></div><div id="comment-tools-45112" class="comment-tools"></div><div class="clear"></div><div id="comment-45112-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

