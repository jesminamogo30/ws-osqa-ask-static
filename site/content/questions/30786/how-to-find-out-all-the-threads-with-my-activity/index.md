+++
type = "question"
title = "how to find out all the threads with my activity?"
description = '''Hi, I remember long time ago I asked some questions in a thread created by another member, but I cannot find it anymore?  Please let me know how to find out such threads? thank you'''
date = "2014-03-13T21:28:00Z"
lastmod = "2014-03-15T06:53:00Z"
weight = 30786
keywords = [ "find" ]
aliases = [ "/questions/30786" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to find out all the threads with my activity?](/questions/30786/how-to-find-out-all-the-threads-with-my-activity)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30786-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30786-score" class="post-score" title="current number of votes">0</div><span id="post-30786-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I remember long time ago I asked some questions in a thread created by another member, but I cannot find it anymore?</p><p>Please let me know how to find out such threads?</p><p>thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-find" rel="tag" title="see questions tagged &#39;find&#39;">find</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Mar '14, 21:28</strong></p><img src="https://secure.gravatar.com/avatar/2d1a8885858c8435654658b25f489bd9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SteveZhou&#39;s gravatar image" /><p><span>SteveZhou</span><br />
<span class="score" title="191 reputation points">191</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SteveZhou has no accepted answers">0%</span></p></div></div><div id="comments-container-30786" class="comments-container"></div><div id="comment-tools-30786" class="comment-tools"></div><div class="clear"></div><div id="comment-30786-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30787"></span>

<div id="answer-container-30787" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30787-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30787-score" class="post-score" title="current number of votes">0</div><span id="post-30787-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Click your name, then click on "Recent Activity" to see events including responses to questions. For threads you started, that would be the "Overview" tab.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Mar '14, 21:39</strong></p><img src="https://secure.gravatar.com/avatar/f533c5f20f9c9afbf4b03de08a100e11?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quadratic&#39;s gravatar image" /><p><span>Quadratic</span><br />
<span class="score" title="1885 reputation points"><span>1.9k</span></span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quadratic has 23 accepted answers">13%</span></p></div></div><div id="comments-container-30787" class="comments-container"><span id="30814"></span><div id="comment-30814" class="comment"><div id="post-30814-score" class="comment-score"></div><div class="comment-text"><p>Looks like not all of the events are in the recent activity tab.</p></div><div id="comment-30814-info" class="comment-info"><span class="comment-age">(15 Mar '14, 06:53)</span> <span class="comment-user userinfo">SteveZhou</span></div></div></div><div id="comment-tools-30787" class="comment-tools"></div><div class="clear"></div><div id="comment-30787-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

