+++
type = "question"
title = "Packets from other websites"
description = '''When going to a website and capturing the packets why are there so many other TCP/UDP connections beyond the requested website? Also, why are there packets from other areas of the requested site?'''
date = "2017-01-28T21:29:00Z"
lastmod = "2017-01-29T05:13:00Z"
weight = 59126
keywords = [ "multiple", "packets", "websites" ]
aliases = [ "/questions/59126" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packets from other websites](/questions/59126/packets-from-other-websites)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59126-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59126-score" class="post-score" title="current number of votes">0</div><span id="post-59126-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When going to a website and capturing the packets why are there so many other TCP/UDP connections beyond the requested website? Also, why are there packets from other areas of the requested site?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-multiple" rel="tag" title="see questions tagged &#39;multiple&#39;">multiple</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-websites" rel="tag" title="see questions tagged &#39;websites&#39;">websites</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Jan '17, 21:29</strong></p><img src="https://secure.gravatar.com/avatar/3c654ad9d6bfb572e9ea0e4e0edeb5ff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rwbates5&#39;s gravatar image" /><p><span>rwbates5</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rwbates5 has no accepted answers">0%</span></p></div></div><div id="comments-container-59126" class="comments-container"></div><div id="comment-tools-59126" class="comment-tools"></div><div class="clear"></div><div id="comment-59126-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="59130"></span>

<div id="answer-container-59130" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59130-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59130-score" class="post-score" title="current number of votes">1</div><span id="post-59130-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Without having seen any of your captured packets, it's most likely traffic from other websites, e.g. for banners, ads, twitter/facebook/G+ "like buttons", etc.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Jan '17, 05:13</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-59130" class="comments-container"></div><div id="comment-tools-59130" class="comment-tools"></div><div class="clear"></div><div id="comment-59130-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

