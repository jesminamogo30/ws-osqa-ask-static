+++
type = "question"
title = "how to got max. delta"
description = '''i have trouble with rtp data that i have captured. why always i got 0 in my max. delta and max jitter? thank you for the answer. '''
date = "2015-01-14T23:12:00Z"
lastmod = "2015-01-17T23:07:00Z"
weight = 39141
keywords = [ "max", "delta" ]
aliases = [ "/questions/39141" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to got max. delta](/questions/39141/how-to-got-max-delta)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39141-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39141-score" class="post-score" title="current number of votes">0</div><span id="post-39141-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i have trouble with rtp data that i have captured. why always i got 0 in my max. delta and max jitter? thank you for the answer.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-max" rel="tag" title="see questions tagged &#39;max&#39;">max</span> <span class="post-tag tag-link-delta" rel="tag" title="see questions tagged &#39;delta&#39;">delta</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '15, 23:12</strong></p><img src="https://secure.gravatar.com/avatar/e46160402deaf64389e1ce193e50f39e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Arif%20Hermantoro&#39;s gravatar image" /><p><span>Arif Hermantoro</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Arif Hermantoro has no accepted answers">0%</span></p></div></div><div id="comments-container-39141" class="comments-container"><span id="39143"></span><div id="comment-39143" class="comment"><div id="post-39143-score" class="comment-score"></div><div class="comment-text"><p>Did you capture at the client, or very close to the client? If so, that could explain it.</p><p>Regards<br />
Kurt</p></div><div id="comment-39143-info" class="comment-info"><span class="comment-age">(15 Jan '15, 00:35)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="39239"></span><div id="comment-39239" class="comment"><div id="post-39239-score" class="comment-score"></div><div class="comment-text"><p>thank you for the answer, it's helpful</p><p>Regards Arif</p></div><div id="comment-39239-info" class="comment-info"><span class="comment-age">(17 Jan '15, 23:07)</span> <span class="comment-user userinfo">Arif Hermantoro</span></div></div></div><div id="comment-tools-39141" class="comment-tools"></div><div class="clear"></div><div id="comment-39141-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

