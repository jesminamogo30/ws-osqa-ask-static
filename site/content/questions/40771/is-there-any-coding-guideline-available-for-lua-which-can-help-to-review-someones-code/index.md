+++
type = "question"
title = "[closed] Is there any coding guideline available for LUA which can help to review someone&#x27;s code?"
description = '''There has to be some concrete coding guidelines for reviewing LUA code. Which follows some concrete rules.'''
date = "2015-03-22T20:43:00Z"
lastmod = "2015-03-24T04:12:00Z"
weight = 40771
keywords = [ "lua", "wireshark" ]
aliases = [ "/questions/40771" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Is there any coding guideline available for LUA which can help to review someone's code?](/questions/40771/is-there-any-coding-guideline-available-for-lua-which-can-help-to-review-someones-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40771-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40771-score" class="post-score" title="current number of votes">0</div><span id="post-40771-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>There has to be some concrete coding guidelines for reviewing LUA code. Which follows some concrete rules.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Mar '15, 20:43</strong></p><img src="https://secure.gravatar.com/avatar/8efce51fbbf3dbd6c9b9132056f45eb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ankit&#39;s gravatar image" /><p><span>ankit</span><br />
<span class="score" title="65 reputation points">65</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ankit has one accepted answer">25%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>24 Mar '15, 04:12</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-40771" class="comments-container"><span id="40782"></span><div id="comment-40782" class="comment"><div id="post-40782-score" class="comment-score"></div><div class="comment-text"><p>are you asking about Lua code guidelines in general or within a Wireshark dissector, written in Lua?</p></div><div id="comment-40782-info" class="comment-info"><span class="comment-age">(23 Mar '15, 09:54)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="40798"></span><div id="comment-40798" class="comment"><div id="post-40798-score" class="comment-score"></div><div class="comment-text"><p>I am asking about lua code guidelines in general</p></div><div id="comment-40798-info" class="comment-info"><span class="comment-age">(24 Mar '15, 03:56)</span> <span class="comment-user userinfo">ankit</span></div></div><span id="40799"></span><div id="comment-40799" class="comment"><div id="post-40799-score" class="comment-score"></div><div class="comment-text"><p>That's not a topic for this site then, best asked on a Lua forum.</p></div><div id="comment-40799-info" class="comment-info"><span class="comment-age">(24 Mar '15, 04:12)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-40771" class="comment-tools"></div><div class="clear"></div><div id="comment-40771-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by grahamb 24 Mar '15, 04:12

</div>

</div>

</div>

