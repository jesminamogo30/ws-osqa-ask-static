+++
type = "question"
title = "How do I download the wire shark tutorials videos given on the homepage"
description = '''How do I download the wire shark tutorials videos given on the homepage? Thanks'''
date = "2011-04-10T08:22:00Z"
lastmod = "2011-04-10T11:41:00Z"
weight = 3422
keywords = [ "download", "videos", "wireshark" ]
aliases = [ "/questions/3422" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How do I download the wire shark tutorials videos given on the homepage](/questions/3422/how-do-i-download-the-wire-shark-tutorials-videos-given-on-the-homepage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3422-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3422-score" class="post-score" title="current number of votes">0</div><span id="post-3422-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I download the wire shark tutorials videos given on the homepage?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span> <span class="post-tag tag-link-videos" rel="tag" title="see questions tagged &#39;videos&#39;">videos</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Apr '11, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/bfd05326e1b331d78ea5a654f2d848ba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pankaj&#39;s gravatar image" /><p><span>pankaj</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pankaj has no accepted answers">0%</span></p></div></div><div id="comments-container-3422" class="comments-container"><span id="3423"></span><div id="comment-3423" class="comment"><div id="post-3423-score" class="comment-score"></div><div class="comment-text"><p>I want to download this video and similar videos http://wiresharkdownloads.riverbed.com/video/wireshark/introduction-to-wireshark/</p><p>Thanks</p></div><div id="comment-3423-info" class="comment-info"><span class="comment-age">(10 Apr '11, 08:23)</span> <span class="comment-user userinfo">pankaj</span></div></div></div><div id="comment-tools-3422" class="comment-tools"></div><div class="clear"></div><div id="comment-3422-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3425"></span>

<div id="answer-container-3425" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3425-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3425-score" class="post-score" title="current number of votes">1</div><span id="post-3425-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You can use Wireshark to do the trick.<br />
<br />
Launch Wireshark and start capturing.<br />
<br />
Open your browser and clear the cache.<br />
For instance in Mozilla Firefox go to:<br />
- Tools<br />
- Clear Recent History...<br />
Select<br />
- Cache<br />
Hit Clear Now<br />
<br />
Paste the <a href="http://wiresharkdownloads.riverbed.com/video/wireshark/introduction-to-wireshark/">url</a> in the browser address bar, hit enter and start the video.<br />
<br />
When you've watched the video, stop the capture and save the capture file.<br />
You can also stop capturing, when the video is loaded and you see the following packet in Wireshark:<br />
23826 0.000065 69.4.231.52 10.10.10.10 HTTP HTTP/1.1 200 OK (video/mp4)<br />
<br />
Next go to:<br />
- File<br />
- Export<br />
- Objects<br />
- HTTP<br />
- select Filename: Introduction%20To%20Wireshark.mp4<br />
- hit Save As to save the file<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>10 Apr '11, 11:41</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span> </br></br></p></div></div><div id="comments-container-3425" class="comments-container"></div><div id="comment-tools-3425" class="comment-tools"></div><div class="clear"></div><div id="comment-3425-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

