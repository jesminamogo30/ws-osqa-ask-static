+++
type = "question"
title = "Simple function question"
description = '''Ok, here is the short version... I have a small home network. Very basic setup. 3 wireless devices in &quot;my&quot; environment going through a Linksys E1500. The modem is going into said Linksys. I share internet with a couple of people upstairs. I have run a CAT5 cable upstairs to them. They have a DLink r...'''
date = "2013-12-07T15:25:00Z"
lastmod = "2013-12-07T15:25:00Z"
weight = 27899
keywords = [ "ethernet", "bandwidth", "monitor" ]
aliases = [ "/questions/27899" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Simple function question](/questions/27899/simple-function-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27899-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27899-score" class="post-score" title="current number of votes">0</div><span id="post-27899-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Ok, here is the short version... I have a small home network. Very basic setup. 3 wireless devices in "my" environment going through a Linksys E1500. The modem is going into said Linksys. I share internet with a couple of people upstairs. I have run a CAT5 cable upstairs to them. They have a DLink router and 3 or 4 wireless devices in their environment. (Only accessing the internet from their Dlink router) All I want to do is be able to monitor the amount of the downloads in gigs going thru the ethernet cable to them upstairs. I have a download cap of 65 gigs a month before my ISP starts charging extra. Every month we are blowing through the cap and I know it's not me. Is there a way to use Wireshark to simply monitor the amount of traffic going thru my Linksys router? (Wifi vs ethernet port) Not interested in piles of information about what they are downloading or intruding on their privacy... Just want the raw upload/downloads in gigs for a set period of time. (monthly would be preferred)</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Dec '13, 15:25</strong></p><img src="https://secure.gravatar.com/avatar/01d2d2bf87582820071c415c387eceb4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jester&#39;s gravatar image" /><p><span>Jester</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jester has no accepted answers">0%</span></p></div></div><div id="comments-container-27899" class="comments-container"></div><div id="comment-tools-27899" class="comment-tools"></div><div class="clear"></div><div id="comment-27899-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

