+++
type = "question"
title = "What does this mean ?"
description = '''I use a Realtek RTL8187 USB adapter and it seems not to be recognized by Wireshark. The capture session could not be initiated (failed to set hardware filter to promiscuous mode). Please check that &quot;DeviceNPF_{FF58589B-5BF6-4A78-988F-87B508471370}&quot; is the proper interface. Help can be found at:  htt...'''
date = "2011-01-04T13:38:00Z"
lastmod = "2011-01-04T15:00:00Z"
weight = 1622
keywords = [ "adapter", "rtl8187", "usb" ]
aliases = [ "/questions/1622" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What does this mean ?](/questions/1622/what-does-this-mean)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1622-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1622-score" class="post-score" title="current number of votes">0</div><span id="post-1622-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I use a Realtek RTL8187 USB adapter and it seems not to be recognized by Wireshark.</p><p>The capture session could not be initiated (failed to set hardware filter to promiscuous mode).</p><p>Please check that "DeviceNPF_{FF58589B-5BF6-4A78-988F-87B508471370}" is the proper interface.</p><p>Help can be found at:</p><pre><code>   http://wiki.wireshark.org/WinPcap
   http://wiki.wireshark.org/CaptureSetup</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-adapter" rel="tag" title="see questions tagged &#39;adapter&#39;">adapter</span> <span class="post-tag tag-link-rtl8187" rel="tag" title="see questions tagged &#39;rtl8187&#39;">rtl8187</span> <span class="post-tag tag-link-usb" rel="tag" title="see questions tagged &#39;usb&#39;">usb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '11, 13:38</strong></p><img src="https://secure.gravatar.com/avatar/8878cc91f7579026f9a9e314c04fdda2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="TEEH&#39;s gravatar image" /><p><span>TEEH</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="TEEH has no accepted answers">0%</span></p></div></div><div id="comments-container-1622" class="comments-container"></div><div id="comment-tools-1622" class="comment-tools"></div><div class="clear"></div><div id="comment-1622-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1625"></span>

<div id="answer-container-1625" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1625-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1625-score" class="post-score" title="current number of votes">0</div><span id="post-1625-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Open the Capture Options dialog and uncheck "Capture packets in promiscuous mode". Then start your capture again.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jan '11, 15:00</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-1625" class="comments-container"></div><div id="comment-tools-1625" class="comment-tools"></div><div class="clear"></div><div id="comment-1625-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

