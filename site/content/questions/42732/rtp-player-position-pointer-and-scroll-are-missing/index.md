+++
type = "question"
title = "RTP Player position pointer and scroll are missing"
description = '''The last several versions of Wireshark has broken the RTP Play window in that when you play they audio the current position marker is gone and doesn&#x27;t scroll the window as it plays streams longer than the window as it did in previous versions. The graphic below is an attempt to show that the point i...'''
date = "2015-05-28T08:33:00Z"
lastmod = "2015-05-28T13:55:00Z"
weight = 42732
keywords = [ "player", "audio", "rtp_in_graph" ]
aliases = [ "/questions/42732" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RTP Player position pointer and scroll are missing](/questions/42732/rtp-player-position-pointer-and-scroll-are-missing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42732-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42732-score" class="post-score" title="current number of votes">0</div><span id="post-42732-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The last several versions of Wireshark has broken the RTP Play window in that when you play they audio the current position marker is gone and doesn't scroll the window as it plays streams longer than the window as it did in previous versions. The graphic below is an attempt to show that the point is gone and the window doesn't scroll.</p><p><img src="https://osqa-ask.wireshark.org/upfiles/wireshark_o3eHaqP.png" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-player" rel="tag" title="see questions tagged &#39;player&#39;">player</span> <span class="post-tag tag-link-audio" rel="tag" title="see questions tagged &#39;audio&#39;">audio</span> <span class="post-tag tag-link-rtp_in_graph" rel="tag" title="see questions tagged &#39;rtp_in_graph&#39;">rtp_in_graph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 May '15, 08:33</strong></p><img src="https://secure.gravatar.com/avatar/ec0a89cff5945cd5b852bcd7c8f355b9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="michaelheinrich1&#39;s gravatar image" /><p><span>michaelheinr...</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="michaelheinrich1 has no accepted answers">0%</span></p></img></div></div><div id="comments-container-42732" class="comments-container"></div><div id="comment-tools-42732" class="comment-tools"></div><div class="clear"></div><div id="comment-42732-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42735"></span>

<div id="answer-container-42735" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42735-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42735-score" class="post-score" title="current number of votes">2</div><span id="post-42735-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes, that's a <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=11125">bug</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 May '15, 13:55</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-42735" class="comments-container"></div><div id="comment-tools-42735" class="comment-tools"></div><div class="clear"></div><div id="comment-42735-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

