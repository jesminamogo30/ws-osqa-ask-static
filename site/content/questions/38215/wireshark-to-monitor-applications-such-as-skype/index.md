+++
type = "question"
title = "Wireshark to monitor applications such as Skype"
description = '''I was wondering if wireshark could help me monitor and analyze traffic on to my Skype ID.It looks rather silly but it has created a lot of nuisance coz someone had compromised my account few times and the skype customer center did not give me a convincing solution.'''
date = "2014-11-27T10:21:00Z"
lastmod = "2014-11-27T16:43:00Z"
weight = 38215
keywords = [ "skype" ]
aliases = [ "/questions/38215" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark to monitor applications such as Skype](/questions/38215/wireshark-to-monitor-applications-such-as-skype)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38215-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38215-score" class="post-score" title="current number of votes">0</div><span id="post-38215-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was wondering if wireshark could help me monitor and analyze traffic on to my Skype ID.It looks rather silly but it has created a lot of nuisance coz someone had compromised my account few times and the skype customer center did not give me a convincing solution.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-skype" rel="tag" title="see questions tagged &#39;skype&#39;">skype</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Nov '14, 10:21</strong></p><img src="https://secure.gravatar.com/avatar/4d91ba0f6003f18cc9653ef85012409b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="comrade&#39;s gravatar image" /><p><span>comrade</span><br />
<span class="score" title="3 reputation points">3</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="comrade has no accepted answers">0%</span></p></div></div><div id="comments-container-38215" class="comments-container"></div><div id="comment-tools-38215" class="comment-tools"></div><div class="clear"></div><div id="comment-38215-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38218"></span>

<div id="answer-container-38218" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38218-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38218-score" class="post-score" title="current number of votes">0</div><span id="post-38218-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Skype traffic is (heavily) encrypted, so you can obviously monitor Skype traffic, but you won't be able to decrypt and read the communication. Sorry.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Nov '14, 16:13</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-38218" class="comments-container"><span id="38219"></span><div id="comment-38219" class="comment"><div id="post-38219-score" class="comment-score"></div><div class="comment-text"><p>Thanks for answering the question Kurt , But i still feel if i could find a solution to check whenever specifically login to my Skype ID coz i do understand we cannot monitor Skype network traffic as a whole.</p></div><div id="comment-38219-info" class="comment-info"><span class="comment-age">(27 Nov '14, 16:43)</span> <span class="comment-user userinfo">comrade</span></div></div></div><div id="comment-tools-38218" class="comment-tools"></div><div class="clear"></div><div id="comment-38218-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

