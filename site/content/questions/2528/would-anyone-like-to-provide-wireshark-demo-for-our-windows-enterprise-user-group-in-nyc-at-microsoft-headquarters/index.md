+++
type = "question"
title = "Would anyone like to provide WireShark demo for our Windows Enterprise User Group in NYC at Microsoft headquarters??"
description = '''Hello, Every month our user group (NYeWin) tries to get a vendor or guru of a product to demonstrate a tool or product for the Enterprise IT admins. We are interested in having WireShark presented in an upcoming meeting. Preferably March 3rd. Our meetings are hosted in the Microsoft office headquart...'''
date = "2011-02-23T11:25:00Z"
lastmod = "2011-02-25T15:39:00Z"
weight = 2528
keywords = [ "wireshark" ]
aliases = [ "/questions/2528" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Would anyone like to provide WireShark demo for our Windows Enterprise User Group in NYC at Microsoft headquarters??](/questions/2528/would-anyone-like-to-provide-wireshark-demo-for-our-windows-enterprise-user-group-in-nyc-at-microsoft-headquarters)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2528-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2528-score" class="post-score" title="current number of votes">0</div><span id="post-2528-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, Every month our user group (NYeWin) tries to get a vendor or guru of a product to demonstrate a tool or product for the Enterprise IT admins. We are interested in having WireShark presented in an upcoming meeting. Preferably March 3rd. Our meetings are hosted in the Microsoft office headquarters in mid-town Manhattan.</p><p>Please let me know if you are interested in presenting.</p><p>Thanks, --Eric Fellen eric815[at]yahoo[dot]com</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '11, 11:25</strong></p><img src="https://secure.gravatar.com/avatar/a1fb414bc49c2bca0c986b167539b3d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="NJTechGuy&#39;s gravatar image" /><p><span>NJTechGuy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="NJTechGuy has no accepted answers">0%</span></p></div></div><div id="comments-container-2528" class="comments-container"><span id="2568"></span><div id="comment-2568" class="comment"><div id="post-2568-score" class="comment-score"></div><div class="comment-text"><p>Eric, I'm in NYC. I can revive some presentations that I did in previous Sharkfest events. Take a look at at the presentations that I did and let me know if you'd like me to present. I have to check my schedule on Monday, but I believe I'm available. You can email me at my gmail address: <span class="__cf_email__" data-cfemail="98f0f9f6ebf9f6ffd8b6b6b6b6b6">[email protected]</span></p><p>Thanks.</p><p>Hansang</p></div><div id="comment-2568-info" class="comment-info"><span class="comment-age">(25 Feb '11, 15:39)</span> <span class="comment-user userinfo">hansangb</span></div></div></div><div id="comment-tools-2528" class="comment-tools"></div><div class="clear"></div><div id="comment-2528-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

