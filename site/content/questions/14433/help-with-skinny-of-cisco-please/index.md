+++
type = "question"
title = "Help with Skinny of Cisco Please."
description = '''Hello! very good day. I dared to write in your forum to ask because I have a problem and I have spent a long time looking for the solution but I can not solve the problem. I commented: I need to record calls that are generated in a Cisco Call Manager using Wireshark. So I did the following: Enable a...'''
date = "2012-09-21T08:42:00Z"
lastmod = "2012-09-21T08:42:00Z"
weight = 14433
keywords = [ "skinny", "mirror", "cisco", "wireshark" ]
aliases = [ "/questions/14433" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Help with Skinny of Cisco Please.](/questions/14433/help-with-skinny-of-cisco-please)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14433-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14433-score" class="post-score" title="current number of votes">0</div><span id="post-14433-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello! very good day. I dared to write in your forum to ask because I have a problem and I have spent a long time looking for the solution but I can not solve the problem. I commented: I need to record calls that are generated in a Cisco Call Manager using Wireshark.</p><p>So I did the following:</p><p>Enable a mirror port on my Cisco switch so I could IM traffic esuchar Call Manager Operating.-----Operating Install Ubuntu on a machine to install Wireshark ----- Operating Connect the port mirror my Ubuntu ---------- ----- Running</p><p>I'm making catches with tcpdump:</p><p>tcpdump-n-e-i eth1-s 0-w xxxx host captura.cap</p><p>So far everything seems fine. Upon opening the file on my Wireshark I captured packets between them I see: TCP, SKYNI, UDP. But when I try to decode calls with telephone mudulo not decode anything. So is that no longer do or I am doing wrong.</p><p>This happens to me with cisco phone model: Cisco UC Phone 3905</p><p>Help Please:</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-skinny" rel="tag" title="see questions tagged &#39;skinny&#39;">skinny</span> <span class="post-tag tag-link-mirror" rel="tag" title="see questions tagged &#39;mirror&#39;">mirror</span> <span class="post-tag tag-link-cisco" rel="tag" title="see questions tagged &#39;cisco&#39;">cisco</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Sep '12, 08:42</strong></p><img src="https://secure.gravatar.com/avatar/92165628dc4f14c84f0008d9d0fec8a4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="andreslavariega&#39;s gravatar image" /><p><span>andreslavariega</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="andreslavariega has no accepted answers">0%</span></p></div></div><div id="comments-container-14433" class="comments-container"></div><div id="comment-tools-14433" class="comment-tools"></div><div class="clear"></div><div id="comment-14433-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

