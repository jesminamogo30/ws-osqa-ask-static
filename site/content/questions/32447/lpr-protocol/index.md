+++
type = "question"
title = "LPR Protocol"
description = '''Hi, I need help in LPR protocol. I sent a PDF file for printing through drivers. My file size is 6.73 KB. I want to analyse how file is divided into packets and sent, &amp;amp; check if the whole file is tansmitted. Can someone please help me. Thanks in advance'''
date = "2014-05-03T03:24:00Z"
lastmod = "2014-05-03T15:23:00Z"
weight = 32447
keywords = [ "lpr" ]
aliases = [ "/questions/32447" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [LPR Protocol](/questions/32447/lpr-protocol)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32447-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32447-score" class="post-score" title="current number of votes">0</div><span id="post-32447-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I need help in LPR protocol. I sent a PDF file for printing through drivers. My file size is 6.73 KB. I want to analyse how file is divided into packets and sent, &amp; check if the whole file is tansmitted. Can someone please help me.</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lpr" rel="tag" title="see questions tagged &#39;lpr&#39;">lpr</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 May '14, 03:24</strong></p><img src="https://secure.gravatar.com/avatar/74ce4120eedfb6c2b3f3e960a824cff0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hgeekd&#39;s gravatar image" /><p><span>hgeekd</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hgeekd has no accepted answers">0%</span></p></div></div><div id="comments-container-32447" class="comments-container"></div><div id="comment-tools-32447" class="comment-tools"></div><div class="clear"></div><div id="comment-32447-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="32455"></span>

<div id="answer-container-32455" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-32455-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-32455-score" class="post-score" title="current number of votes">0</div><span id="post-32455-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://tools.ietf.org/html/rfc1179">RFC 1179</a> describes the LPR protocol; Wireshark includes a dissector for it, so it should show you the content of LPR packets, and reading RFC 1179 should help you understand what the content of those packets means.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 May '14, 15:23</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-32455" class="comments-container"></div><div id="comment-tools-32455" class="comment-tools"></div><div class="clear"></div><div id="comment-32455-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

