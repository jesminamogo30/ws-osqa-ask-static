+++
type = "question"
title = "Unable to save after time shift"
description = '''I&#x27;m trying to merge different pcap files. Meanwhile, I&#x27;m trying to synchronize them and let&#x27;s say I want them all to start at the 2017-01-01 00:00:00. i.e. my zero should be this date. I successfully do that with time shift tool but when I save as and reopen the same window the time shift is reset t...'''
date = "2017-03-27T07:30:00Z"
lastmod = "2017-03-27T12:49:00Z"
weight = 60357
keywords = [ "time_shift", "time" ]
aliases = [ "/questions/60357" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Unable to save after time shift](/questions/60357/unable-to-save-after-time-shift)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60357-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60357-score" class="post-score" title="current number of votes">0</div><span id="post-60357-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm trying to merge different pcap files. Meanwhile, I'm trying to synchronize them and let's say I want them all to start at the 2017-01-01 00:00:00. i.e. my zero should be this date. I successfully do that with time shift tool but when I save as and reopen the same window the time shift is reset to the time before. Thank you for the help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-time_shift" rel="tag" title="see questions tagged &#39;time_shift&#39;">time_shift</span> <span class="post-tag tag-link-time" rel="tag" title="see questions tagged &#39;time&#39;">time</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Mar '17, 07:30</strong></p><img src="https://secure.gravatar.com/avatar/503ee16ee4e1ecf7c2b95479ad936ca2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nimaaaa&#39;s gravatar image" /><p><span>nimaaaa</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nimaaaa has no accepted answers">0%</span></p></div></div><div id="comments-container-60357" class="comments-container"></div><div id="comment-tools-60357" class="comment-tools"></div><div class="clear"></div><div id="comment-60357-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="60364"></span>

<div id="answer-container-60364" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-60364-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-60364-score" class="post-score" title="current number of votes">1</div><span id="post-60364-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>To have a permanent time shift use the tool <code>editcap</code>. The options <code>-t</code> and/or <code>-S</code> are for time shifting operations.</p><p>To merge pcap files use the <code>mergecap</code> utility.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Mar '17, 12:49</strong></p><img src="https://secure.gravatar.com/avatar/11cda2a4be5391632a5b28af1927307b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Uli&#39;s gravatar image" /><p><span>Uli</span><br />
<span class="score" title="903 reputation points">903</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Uli has 16 accepted answers">29%</span></p></div></div><div id="comments-container-60364" class="comments-container"></div><div id="comment-tools-60364" class="comment-tools"></div><div class="clear"></div><div id="comment-60364-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

