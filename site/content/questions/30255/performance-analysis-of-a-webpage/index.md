+++
type = "question"
title = "Performance analysis of a webpage"
description = '''How can I conduct a thorough tcp analysis of a webpage, for performance issues? i.e.  I fire up Wireshark --&amp;gt; I access a webpage --&amp;gt; I navigate around --&amp;gt; I close Wireshark --&amp;gt; Now I want to analyze.'''
date = "2014-02-27T18:08:00Z"
lastmod = "2014-02-28T07:34:00Z"
weight = 30255
keywords = [ "performance", "speed", "latency" ]
aliases = [ "/questions/30255" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Performance analysis of a webpage](/questions/30255/performance-analysis-of-a-webpage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30255-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30255-score" class="post-score" title="current number of votes">0</div><span id="post-30255-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How can I conduct a thorough tcp analysis of a webpage, for performance issues?</p><p>i.e. I fire up Wireshark --&gt; I access a webpage --&gt; I navigate around --&gt; I close Wireshark --&gt;</p><p>Now I want to analyze.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-performance" rel="tag" title="see questions tagged &#39;performance&#39;">performance</span> <span class="post-tag tag-link-speed" rel="tag" title="see questions tagged &#39;speed&#39;">speed</span> <span class="post-tag tag-link-latency" rel="tag" title="see questions tagged &#39;latency&#39;">latency</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Feb '14, 18:08</strong></p><img src="https://secure.gravatar.com/avatar/252720ad4bc9c6d2eb31ce7bab0fdd41?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Puneet&#39;s gravatar image" /><p><span>Puneet</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Puneet has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Feb '14, 18:09</strong> </span></p></div></div><div id="comments-container-30255" class="comments-container"></div><div id="comment-tools-30255" class="comment-tools"></div><div class="clear"></div><div id="comment-30255-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="30270"></span>

<div id="answer-container-30270" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30270-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30270-score" class="post-score" title="current number of votes">0</div><span id="post-30270-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Did you try one of these?</p><blockquote><p><a href="https://www.google.com/?q=wireshark%20performance%20analysis%20web%20traffic">https://www.google.com/?q=wireshark%20performance%20analysis%20web%20traffic</a><br />
<a href="https://www.google.com/?q=youtube%20wireshark%20performance%20analysis">https://www.google.com/?q=youtube%20wireshark%20performance%20analysis</a><br />
<a href="http://www.wiresharkbook.com/">http://www.wiresharkbook.com/</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Feb '14, 07:34</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Feb '14, 07:35</strong> </span></p></div></div><div id="comments-container-30270" class="comments-container"></div><div id="comment-tools-30270" class="comment-tools"></div><div class="clear"></div><div id="comment-30270-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

