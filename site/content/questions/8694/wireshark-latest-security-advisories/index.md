+++
type = "question"
title = "Wireshark latest security advisories"
description = '''Can I get the Wireshark latest security advisories link? From this http://www.wireshark.org/security/, I am able to get up to 2011 security advisories only. 2012 security advisories are not listed in the above link. Is there any any updated link for 2012 security advisories?'''
date = "2012-01-30T03:57:00Z"
lastmod = "2012-11-28T16:35:00Z"
weight = 8694
keywords = [ "security", "advisories", "latest", "wireshark" ]
aliases = [ "/questions/8694" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark latest security advisories](/questions/8694/wireshark-latest-security-advisories)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8694-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8694-score" class="post-score" title="current number of votes">0</div><span id="post-8694-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count">1</div></div></td><td><div id="item-right"><div class="question-body"><p>Can I get the Wireshark latest security advisories link? From this <a href="http://www.wireshark.org/security/">http://www.wireshark.org/security/</a>, I am able to get up to 2011 security advisories only. 2012 security advisories are not listed in the above link. Is there any any updated link for 2012 security advisories?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-security" rel="tag" title="see questions tagged &#39;security&#39;">security</span> <span class="post-tag tag-link-advisories" rel="tag" title="see questions tagged &#39;advisories&#39;">advisories</span> <span class="post-tag tag-link-latest" rel="tag" title="see questions tagged &#39;latest&#39;">latest</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jan '12, 03:57</strong></p><img src="https://secure.gravatar.com/avatar/67a2471305cf3fef7f5a60f63583563e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Prabhakar1621&#39;s gravatar image" /><p><span>Prabhakar1621</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Prabhakar1621 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Jan '12, 05:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-8694" class="comments-container"><span id="16046"></span><div id="comment-16046" class="comment"><div id="post-16046-score" class="comment-score"></div><div class="comment-text"><p>Can I get the Wireshark latest security advisories link? From this <a href="http://www.wireshark.org/security/,">http://www.wireshark.org/security/,</a> I am able to get up to wnpa-sec-2012-25 security advisory only. Avobe wnpa-sec-2012-25 security advisories are not listed in the above link.</p><p>I think page is not updated.</p></div><div id="comment-16046-info" class="comment-info"><span class="comment-age">(19 Nov '12, 02:35)</span> <span class="comment-user userinfo">Prabhakar1621</span></div></div><span id="16407"></span><div id="comment-16407" class="comment"><div id="post-16407-score" class="comment-score"></div><div class="comment-text"><p>I automated this part of the release process (it had to be done manually until now) so this hopefully shouldn't be a problem in the future.</p></div><div id="comment-16407-info" class="comment-info"><span class="comment-age">(28 Nov '12, 16:35)</span> <span class="comment-user userinfo">Gerald Combs ♦♦</span></div></div></div><div id="comment-tools-8694" class="comment-tools"></div><div class="clear"></div><div id="comment-8694-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="8699"></span>

<div id="answer-container-8699" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8699-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8699-score" class="post-score" title="current number of votes">2</div><span id="post-8699-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Prabhakar1621 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think the only one missing for 2012 can be found here <a href="http://www.debian.org/security/2012/dsa-2395">http://www.debian.org/security/2012/dsa-2395</a>. The <a href="http://www.wireshark.org/security/">Wireshark Security Advisories</a> page should probably be updated though.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jan '12, 05:56</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-8699" class="comments-container"><span id="8702"></span><div id="comment-8702" class="comment"><div id="post-8702-score" class="comment-score"></div><div class="comment-text"><p>Thanks for the update</p></div><div id="comment-8702-info" class="comment-info"><span class="comment-age">(30 Jan '12, 08:20)</span> <span class="comment-user userinfo">Prabhakar1621</span></div></div></div><div id="comment-tools-8699" class="comment-tools"></div><div class="clear"></div><div id="comment-8699-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="8701"></span>

<div id="answer-container-8701" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8701-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8701-score" class="post-score" title="current number of votes">0</div><span id="post-8701-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>They're listed now. Sorry for the inconvenience.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jan '12, 08:12</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-8701" class="comments-container"></div><div id="comment-tools-8701" class="comment-tools"></div><div class="clear"></div><div id="comment-8701-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

