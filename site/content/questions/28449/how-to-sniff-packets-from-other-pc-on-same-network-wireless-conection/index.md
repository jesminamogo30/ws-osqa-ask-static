+++
type = "question"
title = "How to sniff packets from other pc on same network ? [Wireless conection]"
description = '''Hello , Im connected by wireless on the same network as my phone , and my 2 other computers. But when i start sniffing network, I can only get my Computer&#x27;s packets , and not other&#x27;s computers packets. How can i do? Thanks.'''
date = "2013-12-27T09:30:00Z"
lastmod = "2013-12-27T15:20:00Z"
weight = 28449
keywords = [ "help", "network", "wireshark" ]
aliases = [ "/questions/28449" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to sniff packets from other pc on same network ? \[Wireless conection\]](/questions/28449/how-to-sniff-packets-from-other-pc-on-same-network-wireless-conection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28449-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28449-score" class="post-score" title="current number of votes">0</div><span id="post-28449-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello , Im connected by wireless on the same network as my phone , and my 2 other computers. But when i start sniffing network, I can only get my Computer's packets , and not other's computers packets.</p><p>How can i do? Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Dec '13, 09:30</strong></p><img src="https://secure.gravatar.com/avatar/487a8af862f47fb5d39b4e6a8ce2b838?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DZhelp&#39;s gravatar image" /><p><span>DZhelp</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DZhelp has no accepted answers">0%</span></p></div></div><div id="comments-container-28449" class="comments-container"></div><div id="comment-tools-28449" class="comment-tools"></div><div class="clear"></div><div id="comment-28449-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28453"></span>

<div id="answer-container-28453" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28453-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28453-score" class="post-score" title="current number of votes">0</div><span id="post-28453-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I think that this is good place to start - <a href="http://wiki.wireshark.org/CaptureSetup/WLAN">Capture WLAN</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Dec '13, 15:20</strong></p><img src="https://secure.gravatar.com/avatar/94630d1ea1108afeafb344e884044d15?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Boaz%20Galil&#39;s gravatar image" /><p><span>Boaz Galil</span><br />
<span class="score" title="56 reputation points">56</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Boaz Galil has no accepted answers">0%</span></p></div></div><div id="comments-container-28453" class="comments-container"></div><div id="comment-tools-28453" class="comment-tools"></div><div class="clear"></div><div id="comment-28453-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

