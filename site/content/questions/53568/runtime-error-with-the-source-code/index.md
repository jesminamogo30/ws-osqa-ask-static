+++
type = "question"
title = "Runtime Error with the source code"
description = '''Hello everyone, I would to debug my Wireshark version but I always have the message &quot;Runtime Error&quot; right after the &quot;Loading Configuration Files&quot;. I have started again with the source code from the version 2.0.4 to see if I didn&#x27;t make a mistake but the same message appear when I build it. Do you ha...'''
date = "2016-06-20T01:28:00Z"
lastmod = "2016-06-20T06:40:00Z"
weight = 53568
keywords = [ "runtime" ]
aliases = [ "/questions/53568" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Runtime Error with the source code](/questions/53568/runtime-error-with-the-source-code)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53568-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53568-score" class="post-score" title="current number of votes">0</div><span id="post-53568-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone,</p><p>I would to debug my Wireshark version but I always have the message "Runtime Error" right after the "Loading Configuration Files". I have started again with the source code from the version 2.0.4 to see if I didn't make a mistake but the same message appear when I build it. Do you have any idea how I can fixe it please ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-runtime" rel="tag" title="see questions tagged &#39;runtime&#39;">runtime</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jun '16, 01:28</strong></p><img src="https://secure.gravatar.com/avatar/1e089af1440cad240cf3e9651ec1b2fd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="stezerow&#39;s gravatar image" /><p><span>stezerow</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="stezerow has no accepted answers">0%</span></p></div></div><div id="comments-container-53568" class="comments-container"></div><div id="comment-tools-53568" class="comment-tools"></div><div class="clear"></div><div id="comment-53568-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="53573"></span>

<div id="answer-container-53573" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53573-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53573-score" class="post-score" title="current number of votes">0</div><span id="post-53573-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You probably have a bug in the init or register functions of your dissector try running a debugger during start of the application or check your code.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Jun '16, 06:40</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-53573" class="comments-container"></div><div id="comment-tools-53573" class="comment-tools"></div><div class="clear"></div><div id="comment-53573-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

