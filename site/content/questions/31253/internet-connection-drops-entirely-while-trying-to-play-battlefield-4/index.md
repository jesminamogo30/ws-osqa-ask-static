+++
type = "question"
title = "Internet connection drops entirely while trying to play Battlefield 4"
description = '''This issue relates to the video game Battlefield 4 on the Xbox 360, PC and Xbox One Since December 20th, I have experienced the most unusual connection issue. Every couple of days (sometimes 2-3 days in a row), my Internet connection drops entirely while trying to play Battlefield 4. It started happ...'''
date = "2014-03-28T13:42:00Z"
lastmod = "2014-04-01T03:15:00Z"
weight = 31253
keywords = [ "battlefield", "4", "xbox" ]
aliases = [ "/questions/31253" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Internet connection drops entirely while trying to play Battlefield 4](/questions/31253/internet-connection-drops-entirely-while-trying-to-play-battlefield-4)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31253-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31253-score" class="post-score" title="current number of votes">0</div><span id="post-31253-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This issue relates to the video game Battlefield 4 on the Xbox 360, PC and Xbox One</p><p>Since December 20th, I have experienced the most unusual connection issue. Every couple of days (sometimes 2-3 days in a row), my Internet connection drops entirely while trying to play Battlefield 4. It started happening after midnight up until roughly 6 AM and was fine after that but the problem soon began to occur at all hours of the day.</p><p>A brief summary of what happens when the issue arises: Sudden noticeable and then heavy lag causes me to be disconnected from the game server (including EA's online service) and Xbox LIVE, at which point the Internet for my home drops altogether (total connection loss for all devices in the home). I have to wait 1/2 minutes for it to recover and reconnect. This issue relates to Battlefield 4 only - and I'm sure of this as I can play any other game (including Battlefield 3) online without any problems after my Internet disconnects due to Battlefield 4. So again, the issue arises with this game and this game only.</p><p>I thought it was isolated to the Xbox 360 but the same happens when I'm trying to play on PC. On top of that, a friend of mine purchased an Xbox One and the same is happening to him on that platform (he also gets booted offline on Xbox 360). The thing is, he resides in the United States whereas I do not, so this is obviously not just a Virgin Media-related issue. A group of my friends in the US (different ISPs to each other) suffer from this issue along with me.</p><p>We contacted Microsoft but they told us to talk to the publisher (EA), who had me run a bunch of tests and open ports, etc which did not help (if port forwarding was the issue, we wouldn't be able to play at all). I even went as far as getting in touch with the game developer (DICE, owned by EA) and the "go to" guy for such problems at the company said he had no idea what was causing this and therefore could not help. We have no where left to turn and so I hope somebody here can help.</p><p>I've attempted numerous things in the hopes of reaching a solution, all of which have failed. They include (but are not limited to):</p><ul><li><p>Turning UPnP off</p></li><li><p>Assigning the console's IP to the DMZ</p></li><li><p>Port-forwarding (required when UPnP is disabled)</p></li><li><p>Changing MTU from 1500 to 1492/and lower</p></li><li><p>Connecting to Xbox LIVE by connecting the console directly to modem</p></li><li><p>Disabling the router's firewall and IP flood/port scan detection (known to cause problems)</p></li><li><p>Enabling IPSec Passthrough</p></li></ul><p>I'm sure I'm missing one or two more things I've tried here but rest assured, I've exhausted everything that I could think of. As stated previously, this happens to my friends in the USA, too. If we're playing together when the issue occurs, we all get kicked off one after the other. I know it is not strictly related to my ISP, but if anyone has any idea why this is happening or at least has something else for me to try, please let me know.</p><p>As of the past 2 weeks or so, I'm no longer getting kicked offline when trying to play on PC but the issue persists on Xbox 360. To add to this, a friend of mine moved house and got a new ISP but still suffers. My plan is to install a 2nd network card into my PC and connect my console to it, then bridge that connection to my PC's primary network adapter and try to play that way and hope to get kicked while Wireshark is capturing on the console's network adapter in order to see what happens before/during/after the disconnect. However, I'm thinking that seeing as I can play fine on PC now, playing on console through my PC's NIC may allow me to stay online without problems. This connection method (dual NICs) is not ideal for my friends, however.</p><p>Any ideas?</p><p>Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-battlefield" rel="tag" title="see questions tagged &#39;battlefield&#39;">battlefield</span> <span class="post-tag tag-link-4" rel="tag" title="see questions tagged &#39;4&#39;">4</span> <span class="post-tag tag-link-xbox" rel="tag" title="see questions tagged &#39;xbox&#39;">xbox</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Mar '14, 13:42</strong></p><img src="https://secure.gravatar.com/avatar/4f2dd488f081d625273910b221f549bd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Watch7ower&#39;s gravatar image" /><p><span>Watch7ower</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Watch7ower has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Mar '14, 20:43</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-31253" class="comments-container"></div><div id="comment-tools-31253" class="comment-tools"></div><div class="clear"></div><div id="comment-31253-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="31261"></span>

<div id="answer-container-31261" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31261-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31261-score" class="post-score" title="current number of votes">0</div><span id="post-31261-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>but if anyone has any idea why this is happening or at least has something else for me to try, please let me know.</p></blockquote><p>based on your description I conclude: a bug in the EA game server or their networking equipment (router, firewall, load balancer, etc.). As they have no idea <del>how their system works</del> what causes the problem (see your comments), you have no way to fix it yourself. Switching ISP and network cards won't help. Unless a lot more people are affected by the problem, EA won't do anything due to a bad cost/efficiency ratio. So your best option for now: Play a different game.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Mar '14, 14:54</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-31261" class="comments-container"></div><div id="comment-tools-31261" class="comment-tools"></div><div class="clear"></div><div id="comment-31261-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="31280"></span>

<div id="answer-container-31280" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31280-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31280-score" class="post-score" title="current number of votes">0</div><span id="post-31280-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>dont know if this will help cause mine started this mess last night. I had to erase all my multiplayer updates except the newest one bout week ago, My camo wasnt workin and color all jacked all startin after getting second assault. I may try this again but only should have to delete one update but do not know if will work this time because my camo works and screen fine but it is disconnectin me as well but just affectin my 360 not my pc.</p><pre><code>                 Kaleb</code></pre></div><div class="answer-controls post-controls"><div class="community-wiki">This answer is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Mar '14, 09:03</strong></p><img src="https://secure.gravatar.com/avatar/6bfe1563b525dbc70c3215d06c767e40?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Haze&#39;s gravatar image" /><p><span>Haze</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Haze has no accepted answers">0%</span></p></div></div><div id="comments-container-31280" class="comments-container"><span id="31343"></span><div id="comment-31343" class="comment"><div id="post-31343-score" class="comment-score"></div><div class="comment-text"><p>Erasing the multiplayer updates (past ones) only helped with the constant freezing after update #8. This is a completely different issue, unfortunateky.</p><p>Kurt: I connected my Xbox console to a 2nd network card and then bridged the connection with my PC's primary adapter and ran Wireshark on the Xbox's adapter. If I post the Wireshark log of where my router appears to reset itself during Battlefield 4, do you think you could take a look at it and possibly spot the exact cause? Thanks in advance.</p></div><div id="comment-31343-info" class="comment-info"><span class="comment-age">(01 Apr '14, 03:15)</span> <span class="comment-user userinfo">Watch7ower</span></div></div></div><div id="comment-tools-31280" class="comment-tools"></div><div class="clear"></div><div id="comment-31280-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

