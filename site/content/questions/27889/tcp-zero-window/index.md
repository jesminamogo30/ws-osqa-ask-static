+++
type = "question"
title = "TCP zero window"
description = '''While transfering the data from primary to Data recovery (DR)through replication router we are geting the error message of TCP zero window.Please help to find the issue whether issue at replication router that is in network or server.'''
date = "2013-12-07T07:00:00Z"
lastmod = "2013-12-07T08:18:00Z"
weight = 27889
keywords = [ "zero", "tcp", "tcpwindowsize" ]
aliases = [ "/questions/27889" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [TCP zero window](/questions/27889/tcp-zero-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27889-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27889-score" class="post-score" title="current number of votes">0</div><span id="post-27889-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>While transfering the data from primary to Data recovery (DR)through replication router we are geting the error message of TCP zero window.Please help to find the issue whether issue at replication router that is in network or server.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-zero" rel="tag" title="see questions tagged &#39;zero&#39;">zero</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span> <span class="post-tag tag-link-tcpwindowsize" rel="tag" title="see questions tagged &#39;tcpwindowsize&#39;">tcpwindowsize</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Dec '13, 07:00</strong></p><img src="https://secure.gravatar.com/avatar/c065e5a432e5c3619cb589ec27dd0433?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aathiinfy&#39;s gravatar image" /><p><span>Aathiinfy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aathiinfy has no accepted answers">0%</span></p></div></div><div id="comments-container-27889" class="comments-container"></div><div id="comment-tools-27889" class="comment-tools"></div><div class="clear"></div><div id="comment-27889-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="27891"></span>

<div id="answer-container-27891" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-27891-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-27891-score" class="post-score" title="current number of votes">0</div><span id="post-27891-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Zero window is reported by the receiver, so the problem is on the receiver side, which indicates that the receiver does not have enough processing power on handling more data. You should focus on receiver side host troubleshooting. Is there any resource intensive workload running on it?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Dec '13, 08:18</strong></p><img src="https://secure.gravatar.com/avatar/2d1a8885858c8435654658b25f489bd9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SteveZhou&#39;s gravatar image" /><p><span>SteveZhou</span><br />
<span class="score" title="191 reputation points">191</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SteveZhou has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Dec '13, 18:45</strong> </span></p></div></div><div id="comments-container-27891" class="comments-container"></div><div id="comment-tools-27891" class="comment-tools"></div><div class="clear"></div><div id="comment-27891-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

