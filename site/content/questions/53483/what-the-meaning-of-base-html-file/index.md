+++
type = "question"
title = "what the meaning of &quot;base html file&quot;?"
description = '''what the meaning of &quot;base html file&quot;?'''
date = "2016-06-15T16:04:00Z"
lastmod = "2016-06-16T14:37:00Z"
weight = 53483
keywords = [ "request", "html", "http" ]
aliases = [ "/questions/53483" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [what the meaning of "base html file"?](/questions/53483/what-the-meaning-of-base-html-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53483-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53483-score" class="post-score" title="current number of votes">0</div><span id="post-53483-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>what the meaning of "base html file"?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span> <span class="post-tag tag-link-html" rel="tag" title="see questions tagged &#39;html&#39;">html</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jun '16, 16:04</strong></p><img src="https://secure.gravatar.com/avatar/754ca1bde18ee4512b33f7a569be5b82?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Omar%20Ramadan&#39;s gravatar image" /><p><span>Omar Ramadan</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Omar Ramadan has no accepted answers">0%</span></p></div></div><div id="comments-container-53483" class="comments-container"><span id="53489"></span><div id="comment-53489" class="comment"><div id="post-53489-score" class="comment-score"></div><div class="comment-text"><p>How does this relate to Wireshark? If you're looking for information on web site layout and HTTP protocol you're in the wrong place.</p></div><div id="comment-53489-info" class="comment-info"><span class="comment-age">(16 Jun '16, 02:10)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="53506"></span><div id="comment-53506" class="comment"><div id="post-53506-score" class="comment-score"></div><div class="comment-text"><p>It relates to Wireshark in such a way that the same OP has posted here a similar question a couple of days ago, which was related to a homework assignment related to Wireshark and the term "base html file" was used in that assignment. As it is not a widely used term with a commonly understood meaning in the area of packet capturing, the context of the whole assignment is necessary to understand what the teacher issuing that assignment had in mind when using these words.</p><p>So <span></span><span>@Omar Ramadan</span>, please paste the whole text of the assignment rather than just the single sentence, we may find out what you're actually asked to do.</p><p>But <span></span><span>@Jaap</span> may have hit the nail's head, maybe you are supposed to find out from the capture that several http requests are necessary to fetch all data the browser needs to render a page, and the teacher calls the initially requested html file, which contains links to the other ones (pictures, advertisements) that need to be fetched, a "base" html file?</p></div><div id="comment-53506-info" class="comment-info"><span class="comment-age">(16 Jun '16, 14:37)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-53483" class="comment-tools"></div><div class="clear"></div><div id="comment-53483-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

