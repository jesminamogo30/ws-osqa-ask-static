+++
type = "question"
title = "[closed] How to capture GRE packets on MAC 10.7.x"
description = '''Hi, I intallled Wireshark(1.6.4) into my Mac book air and am using 3rd party USB Ethernet adapter(LUA3-U2-AGT). I would like to capture GRE packets in this environment, but I cannot see them.. Is this a problem of NIC? or my setting on Wireshark? Does anyone have a resolution for that? Thanks in adv...'''
date = "2011-12-08T18:37:00Z"
lastmod = "2011-12-08T22:07:00Z"
weight = 7859
keywords = [ "gre" ]
aliases = [ "/questions/7859" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] How to capture GRE packets on MAC 10.7.x](/questions/7859/how-to-capture-gre-packets-on-mac-107x)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7859-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7859-score" class="post-score" title="current number of votes">0</div><span id="post-7859-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I intallled Wireshark(1.6.4) into my Mac book air and am using 3rd party USB Ethernet adapter(LUA3-U2-AGT). I would like to capture GRE packets in this environment, but I cannot see them.. Is this a problem of NIC? or my setting on Wireshark? Does anyone have a resolution for that?</p><p>Thanks in advance, Yutaka</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gre" rel="tag" title="see questions tagged &#39;gre&#39;">gre</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Dec '11, 18:37</strong></p><img src="https://secure.gravatar.com/avatar/cdf7f8a446b771aa265866d5668312ac?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="YUTAKA&#39;s gravatar image" /><p><span>YUTAKA</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="YUTAKA has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>09 Dec '11, 12:46</strong> </span></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span></p></div></div><div id="comments-container-7859" class="comments-container"><span id="7860"></span><div id="comment-7860" class="comment"><div id="post-7860-score" class="comment-score"></div><div class="comment-text"><p>I doubt it's a Wireshark problem. If GRE packets are present, Wireshark should capture them, assuming you haven't specified a capture filter that will otherwise filter them out, of course.</p><p>Have you tried contacting the <a href="http://buffalo.jp/products/catalog/network/lua3-u2-agt/">manufacturer</a> of the device?</p></div><div id="comment-7860-info" class="comment-info"><span class="comment-age">(08 Dec '11, 20:03)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="7861"></span><div id="comment-7861" class="comment"><div id="post-7861-score" class="comment-score"></div><div class="comment-text"><p>Sorry, I resolved this by myself. My testing environment was wrong. (had used a switch, not a hub..) the default setting is fine to see GRE packets.</p></div><div id="comment-7861-info" class="comment-info"><span class="comment-age">(08 Dec '11, 22:07)</span> <span class="comment-user userinfo">YUTAKA</span></div></div></div><div id="comment-tools-7859" class="comment-tools"></div><div class="clear"></div><div id="comment-7859-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "OtherPoster found out solution himself..." by SYN-bit 09 Dec '11, 12:46

</div>

</div>

</div>

