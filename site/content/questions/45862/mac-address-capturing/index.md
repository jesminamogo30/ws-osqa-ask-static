+++
type = "question"
title = "MAC Address capturing"
description = '''I have a business that is frequently burglarized, despite the video system and alarm. In an effort to capture these criminals I was thinking of finding a way to utilize a way to capture the MAC address of a suspects phone that is on them while in the business.  Does any one have a simple way to capt...'''
date = "2015-09-15T14:11:00Z"
lastmod = "2015-09-15T14:19:00Z"
weight = 45862
keywords = [ "sniffing", "mac", "capturing" ]
aliases = [ "/questions/45862" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [MAC Address capturing](/questions/45862/mac-address-capturing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45862-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45862-score" class="post-score" title="current number of votes">0</div><span id="post-45862-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a business that is frequently burglarized, despite the video system and alarm. In an effort to capture these criminals I was thinking of finding a way to utilize a way to capture the MAC address of a suspects phone that is on them while in the business.</p><p>Does any one have a simple way to capturing this so I can give it to the investigators?</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sniffing" rel="tag" title="see questions tagged &#39;sniffing&#39;">sniffing</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-capturing" rel="tag" title="see questions tagged &#39;capturing&#39;">capturing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Sep '15, 14:11</strong></p><img src="https://secure.gravatar.com/avatar/ae1277ba61ee53cc63ed8795be530e4f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wagsnjc1&#39;s gravatar image" /><p><span>wagsnjc1</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wagsnjc1 has no accepted answers">0%</span></p></div></div><div id="comments-container-45862" class="comments-container"></div><div id="comment-tools-45862" class="comment-tools"></div><div class="clear"></div><div id="comment-45862-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45863"></span>

<div id="answer-container-45863" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45863-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45863-score" class="post-score" title="current number of votes">0</div><span id="post-45863-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>They'd need to have a wireless system on and visible. Blutooth usually is passive unless pairing, WiFi might work if you have an access point they connect to, but for that it would need to be open (which brings a whole different kind of trouble to your business).</p><p>But let's say that any of this happens - you'll still not going to be able to do much with the MAC addresses, simply because there is no record of MACs to persons or even phones.</p><p>In the end it's basically like a license plate, but without the following problems:</p><ul><li>no registry at all, so you can't look up the owner</li><li>MACs can easily be forged (similar to a stolen license plate)</li><li>criminals might not have any wireless visible to you (car without a license plate)</li></ul><p>Sorry for the bad news.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 Sep '15, 14:19</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-45863" class="comments-container"></div><div id="comment-tools-45863" class="comment-tools"></div><div class="clear"></div><div id="comment-45863-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

