+++
type = "question"
title = "[closed] Anyway to use LUA Tools with Tshark?"
description = '''There is a really nice LUA for MPEG extraction for Wireshark. Is there a way to use that same LUA in Tshark (or any for that matter)?'''
date = "2016-08-08T12:52:00Z"
lastmod = "2016-08-08T13:47:00Z"
weight = 54673
keywords = [ "lua", "tshark", "mpeg_dump" ]
aliases = [ "/questions/54673" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Anyway to use LUA Tools with Tshark?](/questions/54673/anyway-to-use-lua-tools-with-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54673-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54673-score" class="post-score" title="current number of votes">0</div><span id="post-54673-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>There is a really nice LUA for MPEG extraction for Wireshark. Is there a way to use that same LUA in Tshark (or any for that matter)?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-mpeg_dump" rel="tag" title="see questions tagged &#39;mpeg_dump&#39;">mpeg_dump</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Aug '16, 12:52</strong></p><img src="https://secure.gravatar.com/avatar/7c5474f71399d22bfbee5a0e86550fc0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rchapoteau&#39;s gravatar image" /><p><span>rchapoteau</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rchapoteau has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>08 Aug '16, 13:46</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-54673" class="comments-container"><span id="54674"></span><div id="comment-54674" class="comment"><div id="post-54674-score" class="comment-score"></div><div class="comment-text"><p>Asking the same question using different wording words doesn't help.</p></div><div id="comment-54674-info" class="comment-info"><span class="comment-age">(08 Aug '16, 13:47)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-54673" class="comment-tools"></div><div class="clear"></div><div id="comment-54673-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by sindy 08 Aug '16, 13:46

</div>

</div>

</div>

