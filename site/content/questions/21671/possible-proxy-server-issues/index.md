+++
type = "question"
title = "Possible Proxy server issues"
description = '''Noob middleware guys here. We have a websphere app that works fine externally, but from within the office it consistantly gets &#x27;page cannot be displayed&#x27; errors. We suspect the app and proxy server are not playing well together. A network guy traced all machines involved for me as I recreated the is...'''
date = "2013-05-31T08:52:00Z"
lastmod = "2013-05-31T08:52:00Z"
weight = 21671
keywords = [ "websphere", "proxy" ]
aliases = [ "/questions/21671" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Possible Proxy server issues](/questions/21671/possible-proxy-server-issues)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21671-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21671-score" class="post-score" title="current number of votes">0</div><span id="post-21671-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Noob middleware guys here. We have a websphere app that works fine externally, but from within the office it consistantly gets 'page cannot be displayed' errors. We suspect the app and proxy server are not playing well together. A network guy traced all machines involved for me as I recreated the issue. I've opened them in Wireshark ... and am stuck. Can anyone suggest the best way to filter, pick out errors, anything I should try first to start troubleshooting. Using Fiddler, I did find some 407 and 502 (proxy server related) errors. Thanks !</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-websphere" rel="tag" title="see questions tagged &#39;websphere&#39;">websphere</span> <span class="post-tag tag-link-proxy" rel="tag" title="see questions tagged &#39;proxy&#39;">proxy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>31 May '13, 08:52</strong></p><img src="https://secure.gravatar.com/avatar/3f1f34dfa33262a061808a3644795002?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wasguy&#39;s gravatar image" /><p><span>wasguy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wasguy has no accepted answers">0%</span></p></div></div><div id="comments-container-21671" class="comments-container"></div><div id="comment-tools-21671" class="comment-tools"></div><div class="clear"></div><div id="comment-21671-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

