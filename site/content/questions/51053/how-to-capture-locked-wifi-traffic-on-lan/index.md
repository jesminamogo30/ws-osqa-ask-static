+++
type = "question"
title = "How to capture locked wifi traffic on LAN"
description = '''hello guys i will be very thank full to you if you help me out, i am new user and first time here. i am confused about how to capture locked Wifi networks. We can capture traffic of open wireless or connected wifi but i cant capture traffic of locked wifi networks.Please help me . Thank you so much ...'''
date = "2016-03-20T09:19:00Z"
lastmod = "2016-03-20T11:08:00Z"
weight = 51053
keywords = [ "capture", "wifi", "traffic", "analysis", "spoofing" ]
aliases = [ "/questions/51053" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture locked wifi traffic on LAN](/questions/51053/how-to-capture-locked-wifi-traffic-on-lan)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51053-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51053-score" class="post-score" title="current number of votes">0</div><span id="post-51053-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello guys i will be very thank full to you if you help me out, i am new user and first time here. i am confused about how to capture locked Wifi networks. We can capture traffic of open wireless or connected wifi but i cant capture traffic of locked wifi networks.Please help me . Thank you so much if you help me out.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span> <span class="post-tag tag-link-spoofing" rel="tag" title="see questions tagged &#39;spoofing&#39;">spoofing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '16, 09:19</strong></p><img src="https://secure.gravatar.com/avatar/0c15e6e818836eddbb074dd877ab44a6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ayyan%20Haider&#39;s gravatar image" /><p><span>Ayyan Haider</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ayyan Haider has no accepted answers">0%</span></p></div></div><div id="comments-container-51053" class="comments-container"></div><div id="comment-tools-51053" class="comment-tools"></div><div class="clear"></div><div id="comment-51053-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="51054"></span>

<div id="answer-container-51054" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51054-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51054-score" class="post-score" title="current number of votes">0</div><span id="post-51054-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Look first <a href="https://wiki.wireshark.org/CaptureSetup/WLAN">here</a> and then <a href="https://wiki.wireshark.org/HowToDecrypt802.11">here</a>. If something is not clear after reading these two articles, use the <code>add a comment</code> button below the answer for the additional questions.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Mar '16, 11:08</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-51054" class="comments-container"></div><div id="comment-tools-51054" class="comment-tools"></div><div class="clear"></div><div id="comment-51054-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

