+++
type = "question"
title = "MAC listing"
description = '''Hi all I have a question regarding the free OUI lookup tool here: https://www.wireshark.org/tools/oui-lookup.html form what I understand its database is located at: https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob_plain;f=manuf However there is an entry in the database (text file) whi...'''
date = "2015-10-13T06:24:00Z"
lastmod = "2015-10-14T06:32:00Z"
weight = 46497
keywords = [ "mac", "lookup", "oui", "address" ]
aliases = [ "/questions/46497" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [MAC listing](/questions/46497/mac-listing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46497-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46497-score" class="post-score" title="current number of votes">0</div><span id="post-46497-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all I have a question regarding the free OUI lookup tool here: <a href="https://www.wireshark.org/tools/oui-lookup.html">https://www.wireshark.org/tools/oui-lookup.html</a></p><p>form what I understand its database is located at: <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob_plain;f=manuf">https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob_plain;f=manuf</a></p><p>However there is an entry in the database (text file) which is not available when I search it in the lookup tool! how frequent does the lookup tool update its database to match the text file and add the new entries?</p><p>p.s.: the manufacturer I am looking for has an OUI36 public listing.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-lookup" rel="tag" title="see questions tagged &#39;lookup&#39;">lookup</span> <span class="post-tag tag-link-oui" rel="tag" title="see questions tagged &#39;oui&#39;">oui</span> <span class="post-tag tag-link-address" rel="tag" title="see questions tagged &#39;address&#39;">address</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Oct '15, 06:24</strong></p><img src="https://secure.gravatar.com/avatar/ac4dca50309e977fd991f401bd8cfbaa?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="my_immortal&#39;s gravatar image" /><p><span>my_immortal</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="my_immortal has no accepted answers">0%</span></p></div></div><div id="comments-container-46497" class="comments-container"><span id="46520"></span><div id="comment-46520" class="comment"><div id="post-46520-score" class="comment-score"></div><div class="comment-text"><blockquote><p>p.s.: the manufacturer I am looking for has an OUI36 public listing.</p></blockquote><p>There are a lot of entries in the file... Do you mind to tell us that manufacturer?</p></div><div id="comment-46520-info" class="comment-info"><span class="comment-age">(13 Oct '15, 13:56)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="46545"></span><div id="comment-46545" class="comment"><div id="post-46545-score" class="comment-score"></div><div class="comment-text"><p>This happened for many manufacturers. I thought that the syncing happens instantaneously with the .txt file on wireshark's website. Is is true? or it takes some time for that to happen?</p></div><div id="comment-46545-info" class="comment-info"><span class="comment-age">(14 Oct '15, 00:38)</span> <span class="comment-user userinfo">my_immortal</span></div></div><span id="46546"></span><div id="comment-46546" class="comment"><div id="post-46546-score" class="comment-score"></div><div class="comment-text"><p>See for example this manufacturer extracted from the txt file 70:B3:D5:83:C0:00/36 Sinoembe # Sinoembed</p></div><div id="comment-46546-info" class="comment-info"><span class="comment-age">(14 Oct '15, 01:17)</span> <span class="comment-user userinfo">my_immortal</span></div></div></div><div id="comment-tools-46497" class="comment-tools"></div><div class="clear"></div><div id="comment-46497-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="46548"></span>

<div id="answer-container-46548" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46548-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46548-score" class="post-score" title="current number of votes">0</div><span id="post-46548-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I did a lookup for 70:B3:D5:83:C0:00 and did not get the desired result. It works for other OUIs. Maybe the OUI lookup tool at <a href="https://www.wireshark.org/tools/oui-lookup.html">https://www.wireshark.org/tools/oui-lookup.html</a> does <strong>not</strong> use the same database as Wireshark, or there is a bug in the lookup tool. Please contact <span>@Gerald Combs</span>, who owns/runs the website.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Oct '15, 05:15</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-46548" class="comments-container"></div><div id="comment-tools-46548" class="comment-tools"></div><div class="clear"></div><div id="comment-46548-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="46549"></span>

<div id="answer-container-46549" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-46549-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-46549-score" class="post-score" title="current number of votes">0</div><span id="post-46549-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Why not try the IEEE Registration Authority website?</p><p><a href="https://regauth.standards.ieee.org/standards-ra-web/pub/view.html#registries">https://regauth.standards.ieee.org/standards-ra-web/pub/view.html#registries</a></p><p>It shows that OUI registered to: Sinoembed</p><p>RM1105,No 2078,YueLiangWan Road,NanShan District ShenZhen GuangDong CN 518000</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Oct '15, 06:30</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-46549" class="comments-container"><span id="46550"></span><div id="comment-46550" class="comment"><div id="post-46550-score" class="comment-score"></div><div class="comment-text"><p>Yeah you are right. But for some reason I want it to be shown on Wireshark's lookup tool. Keeping in mind that it is already listed in their text database.</p></div><div id="comment-46550-info" class="comment-info"><span class="comment-age">(14 Oct '15, 06:32)</span> <span class="comment-user userinfo">my_immortal</span></div></div></div><div id="comment-tools-46549" class="comment-tools"></div><div class="clear"></div><div id="comment-46549-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

