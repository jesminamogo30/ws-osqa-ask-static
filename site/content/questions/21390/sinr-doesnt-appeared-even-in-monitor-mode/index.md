+++
type = "question"
title = "SINR doesn&#x27;t appeared even in monitor mode"
description = '''Hi everyone. I&#x27;m currently try to sniff a WiFi SINR value using TCPDump. I put my adapter on monitor mode already but when i try to capture any traffic. Nothing is captured. Can anyone give me a solution? [root@dhcp7154 ~]# iwconfig lo no wireless extensions. eth0 no wireless extensions. wifi0 no wi...'''
date = "2013-05-22T22:10:00Z"
lastmod = "2013-05-22T22:10:00Z"
weight = 21390
keywords = [ "tcpdump", "sinr" ]
aliases = [ "/questions/21390" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [SINR doesn't appeared even in monitor mode](/questions/21390/sinr-doesnt-appeared-even-in-monitor-mode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-21390-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-21390-score" class="post-score" title="current number of votes">0</div><span id="post-21390-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone. I'm currently try to sniff a WiFi SINR value using TCPDump. I put my adapter on monitor mode already but when i try to capture any traffic. Nothing is captured. Can anyone give me a solution?</p><p>[<span class="__cf_email__" data-cfemail="493b26263d092d212a397e787c7d">[email protected]</span> ~]# iwconfig lo no wireless extensions.</p><p>eth0 no wireless extensions.</p><p>wifi0 no wireless extensions.</p><p>pan0 no wireless extensions.</p><p>ath0 IEEE 802.11g ESSID:"" Nickname:"" Mode:Monitor Channel:0 Access Point: Not-Associated Bit Rate:0 kb/s Tx-Power:18 dBm Sensitivity=1/1 Retry:off RTS thr:off Fragment thr:off Encryption key:off Power Management:off Link Quality=0/70 Signal level=-98 dBm Noise level=-98 dBm Rx invalid nwid:0 Rx invalid crypt:0 Rx invalid frag:0 Tx excessive retries:0 Invalid misc:0 Missed beacon:0</p><p>[<span class="__cf_email__" data-cfemail="53213c3c2713373b302364626667">[email protected]</span> ~]# tcpdump -i ath0 -s 0 -w stress.pcap tcpdump: bind: Network is down</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcpdump" rel="tag" title="see questions tagged &#39;tcpdump&#39;">tcpdump</span> <span class="post-tag tag-link-sinr" rel="tag" title="see questions tagged &#39;sinr&#39;">sinr</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 May '13, 22:10</strong></p><img src="https://secure.gravatar.com/avatar/a3291ae3aa2fd3100059945d1afa121c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tyanium&#39;s gravatar image" /><p><span>Tyanium</span><br />
<span class="score" title="1 reputation points">1</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tyanium has no accepted answers">0%</span></p></div></div><div id="comments-container-21390" class="comments-container"></div><div id="comment-tools-21390" class="comment-tools"></div><div class="clear"></div><div id="comment-21390-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

