+++
type = "question"
title = "[closed] Wireshark crashes when choosing &quot;Follow Udp stream&quot; in context menu"
description = '''Wireshark crashes when choosing &quot;Follow Udp stream&quot; in context menu'''
date = "2015-08-13T20:55:00Z"
lastmod = "2015-08-14T15:08:00Z"
weight = 45088
keywords = [ "crash" ]
aliases = [ "/questions/45088" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Wireshark crashes when choosing "Follow Udp stream" in context menu](/questions/45088/wireshark-crashes-when-choosing-follow-udp-stream-in-context-menu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45088-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45088-score" class="post-score" title="current number of votes">0</div><span id="post-45088-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wireshark crashes when choosing "Follow Udp stream" in context menu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-crash" rel="tag" title="see questions tagged &#39;crash&#39;">crash</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '15, 20:55</strong></p><img src="https://secure.gravatar.com/avatar/b35575d9849483a83cad0d8527cf5eb4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vitaly%20Trizna&#39;s gravatar image" /><p><span>Vitaly Trizna</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vitaly Trizna has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>19 Aug '15, 01:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/285b1f0f4caadc088a38c40aea22feba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lekensteyn&#39;s gravatar image" /><p><span>Lekensteyn</span><br />
<span class="score" title="2213 reputation points"><span>2.2k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="24 badges"><span class="bronze">●</span><span class="badgecount">24</span></span></p></div></div><div id="comments-container-45088" class="comments-container"><span id="45097"></span><div id="comment-45097" class="comment"><div id="post-45097-score" class="comment-score"></div><div class="comment-text"><p>Each post should have a clear, specific question in the title field. Please rephrase the title as a proper question.</p><p>What is your OS and which version and what is your Wireshark version?</p></div><div id="comment-45097-info" class="comment-info"><span class="comment-age">(14 Aug '15, 01:31)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="45122"></span><div id="comment-45122" class="comment"><div id="post-45122-score" class="comment-score"></div><div class="comment-text"><p>"Wireshark crashes" means "Wireshark has a bug", so you should file a bug on <a href="http://bugs.wireshark.org">the Wireshark Bugzilla</a>. When filing a bug, give as many details as you can - including, as Jaap says, what OS and version of that OS you're running Wireshark on and what version of Wireshark you're using. If the OS gives details about the crash, provide those as well.</p></div><div id="comment-45122-info" class="comment-info"><span class="comment-age">(14 Aug '15, 15:08)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-45088" class="comment-tools"></div><div class="clear"></div><div id="comment-45088-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic (please file a bug on https://bugs.wireshark.org/ with appropriate version information and error messages)" by Lekensteyn 19 Aug '15, 01:51

</div>

</div>

</div>

