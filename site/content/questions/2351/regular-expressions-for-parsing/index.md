+++
type = "question"
title = "Regular Expressions for parsing"
description = '''Wondering if regex is a viable option for parsing message bodies for a custom protocol. I&#x27;m programming in C and using standard C-strings(char arrays) with the built in functions would rather cumbersome and problematic for the kind of parsing I&#x27;m doing. Regular expressions would make it much easier ...'''
date = "2011-02-15T11:14:00Z"
lastmod = "2011-03-21T21:27:00Z"
weight = 2351
keywords = [ "reguar", "expressions", "parsing" ]
aliases = [ "/questions/2351" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Regular Expressions for parsing](/questions/2351/regular-expressions-for-parsing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2351-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2351-score" class="post-score" title="current number of votes">1</div><span id="post-2351-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Wondering if regex is a viable option for parsing message bodies for a custom protocol. I'm programming in C and using standard C-strings(char arrays) with the built in functions would rather cumbersome and problematic for the kind of parsing I'm doing.</p><p>Regular expressions would make it much easier for us.</p><p>So my questions are:</p><p>-Is it a realistic option?</p><p>-If so would I need to find my own libraries or are there ones already included with the wireshark source?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reguar" rel="tag" title="see questions tagged &#39;reguar&#39;">reguar</span> <span class="post-tag tag-link-expressions" rel="tag" title="see questions tagged &#39;expressions&#39;">expressions</span> <span class="post-tag tag-link-parsing" rel="tag" title="see questions tagged &#39;parsing&#39;">parsing</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '11, 11:14</strong></p><img src="https://secure.gravatar.com/avatar/3d3535b19a6debac9e2b855465a2027b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Rodayo&#39;s gravatar image" /><p><span>Rodayo</span><br />
<span class="score" title="61 reputation points">61</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Rodayo has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Feb '11, 11:14</strong> </span></p></div></div><div id="comments-container-2351" class="comments-container"></div><div id="comment-tools-2351" class="comment-tools"></div><div class="clear"></div><div id="comment-2351-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2999"></span>

<div id="answer-container-2999" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2999-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2999-score" class="post-score" title="current number of votes">1</div><span id="post-2999-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark uses GLib, which has its own <a href="http://library.gnome.org/devel/glib/2.24/glib-Perl-compatible-regular-expressions.html">Perl-compatible regular expressions API</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Mar '11, 21:27</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-2999" class="comments-container"></div><div id="comment-tools-2999" class="comment-tools"></div><div class="clear"></div><div id="comment-2999-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

