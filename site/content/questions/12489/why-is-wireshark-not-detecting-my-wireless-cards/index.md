+++
type = "question"
title = "Why is Wireshark not detecting my wireless cards?"
description = '''I&#x27;ve downloaded Wireshark on Ubuntu from the Ubuntu Software Centre. There are no interfaces found at all even after I restart the wireless interfaces. What would the problem be? How do I fix it?'''
date = "2012-07-06T13:11:00Z"
lastmod = "2012-07-06T13:24:00Z"
weight = 12489
keywords = [ "interfaces", "ubuntu" ]
aliases = [ "/questions/12489" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Why is Wireshark not detecting my wireless cards?](/questions/12489/why-is-wireshark-not-detecting-my-wireless-cards)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12489-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12489-score" class="post-score" title="current number of votes">0</div><span id="post-12489-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've downloaded Wireshark on Ubuntu from the Ubuntu Software Centre. There are no interfaces found at all even after I restart the wireless interfaces. What would the problem be? How do I fix it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-interfaces" rel="tag" title="see questions tagged &#39;interfaces&#39;">interfaces</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Jul '12, 13:11</strong></p><img src="https://secure.gravatar.com/avatar/57f1948869346480c99ad2d94437dfe0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="carla123&#39;s gravatar image" /><p><span>carla123</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="carla123 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>06 Jul '12, 13:25</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-12489" class="comments-container"><span id="12491"></span><div id="comment-12491" class="comment"><div id="post-12491-score" class="comment-score"></div><div class="comment-text"><p>duplicate: <a href="http://ask.wireshark.org/questions/7523/ubuntu-machine-no-interfaces-listed">http://ask.wireshark.org/questions/7523/ubuntu-machine-no-interfaces-listed</a></p></div><div id="comment-12491-info" class="comment-info"><span class="comment-age">(06 Jul '12, 13:24)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-12489" class="comment-tools"></div><div class="clear"></div><div id="comment-12489-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12490"></span>

<div id="answer-container-12490" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12490-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12490-score" class="post-score" title="current number of votes">0</div><span id="post-12490-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Are you attempting to capture as a non-root user? Are you a member of the wireshark group? See the Debian <a href="http://anonscm.debian.org/viewvc/collab-maint/ext-maint/wireshark/trunk/debian/README.Debian?view=markup">readme</a> for more info.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Jul '12, 13:18</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-12490" class="comments-container"></div><div id="comment-tools-12490" class="comment-tools"></div><div class="clear"></div><div id="comment-12490-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

