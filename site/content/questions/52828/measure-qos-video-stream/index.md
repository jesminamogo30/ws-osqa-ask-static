+++
type = "question"
title = "measure qos video stream"
description = '''hi, i need your help, i want to measure qos (throughput, delay, etc) video stream from http://myasiantv.se/drama/cheese-in-the-trap/episode-1/, but i still confused to start, how to know ip&#x27;s that video stream because i need to filter it'''
date = "2016-05-23T01:15:00Z"
lastmod = "2016-05-23T01:15:00Z"
weight = 52828
keywords = [ "videostream", "qos" ]
aliases = [ "/questions/52828" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [measure qos video stream](/questions/52828/measure-qos-video-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52828-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52828-score" class="post-score" title="current number of votes">0</div><span id="post-52828-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi, i need your help, i want to measure qos (throughput, delay, etc) video stream from <a href="http://myasiantv.se/drama/cheese-in-the-trap/episode-1/,">http://myasiantv.se/drama/cheese-in-the-trap/episode-1/,</a> but i still confused to start, how to know ip's that video stream because i need to filter it</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-videostream" rel="tag" title="see questions tagged &#39;videostream&#39;">videostream</span> <span class="post-tag tag-link-qos" rel="tag" title="see questions tagged &#39;qos&#39;">qos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '16, 01:15</strong></p><img src="https://secure.gravatar.com/avatar/cf83b45ca97b315e083cd575a0d00043?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ndmnd&#39;s gravatar image" /><p><span>ndmnd</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ndmnd has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>23 May '16, 01:15</strong> </span></p></div></div><div id="comments-container-52828" class="comments-container"></div><div id="comment-tools-52828" class="comment-tools"></div><div class="clear"></div><div id="comment-52828-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

