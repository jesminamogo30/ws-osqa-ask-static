+++
type = "question"
title = "Filtering data from generated text file with TShark and reveiling PayLoad"
description = '''Hi, I am seeing all packets going through my router, I would like to intercept only those with TCP protocol or with keywords of my choice (like SEQ) or by IP address, how can I do that? also, I would like to see the packets Payload But im not getting any Payload information with the TShark output li...'''
date = "2016-12-04T04:49:00Z"
lastmod = "2016-12-04T04:49:00Z"
weight = 57830
keywords = [ "tsharkfilter", "tcppackets", "packets", "tshark", "payload" ]
aliases = [ "/questions/57830" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Filtering data from generated text file with TShark and reveiling PayLoad](/questions/57830/filtering-data-from-generated-text-file-with-tshark-and-reveiling-payload)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57830-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57830-score" class="post-score" title="current number of votes">0</div><span id="post-57830-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am seeing all packets going through my router, I would like to intercept only those with TCP protocol or with keywords of my choice (like SEQ) or by IP address, how can I do that?</p><p>also, I would like to see the packets Payload But im not getting any Payload information with the TShark output list, any suggestions?</p><p>thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tsharkfilter" rel="tag" title="see questions tagged &#39;tsharkfilter&#39;">tsharkfilter</span> <span class="post-tag tag-link-tcppackets" rel="tag" title="see questions tagged &#39;tcppackets&#39;">tcppackets</span> <span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-payload" rel="tag" title="see questions tagged &#39;payload&#39;">payload</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '16, 04:49</strong></p><img src="https://secure.gravatar.com/avatar/34e92c458583e7a88b7fc96fb424c50d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="eyal360&#39;s gravatar image" /><p><span>eyal360</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="eyal360 has no accepted answers">0%</span></p></div></div><div id="comments-container-57830" class="comments-container"></div><div id="comment-tools-57830" class="comment-tools"></div><div class="clear"></div><div id="comment-57830-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

