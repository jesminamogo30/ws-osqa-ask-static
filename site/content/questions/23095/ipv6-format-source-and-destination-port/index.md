+++
type = "question"
title = "IPv6 format source and destination port"
description = '''Hi,  I am using text2pcap utility to decode the trace message to create libcap capture file. But when I am receiving the trace packets with source and destination addresses in IPv6 format, in that case libcap is not generating proper and on wireshark I am getting &quot;Ethernet Check Sequesnce Incorrect&quot;...'''
date = "2013-07-18T00:20:00Z"
lastmod = "2013-07-18T01:04:00Z"
weight = 23095
keywords = [ "ipv6" ]
aliases = [ "/questions/23095" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [IPv6 format source and destination port](/questions/23095/ipv6-format-source-and-destination-port)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23095-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23095-score" class="post-score" title="current number of votes">0</div><span id="post-23095-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am using text2pcap utility to decode the trace message to create libcap capture file. But when I am receiving the trace packets with source and destination addresses in IPv6 format, in that case libcap is not generating proper and on wireshark I am getting "Ethernet Check Sequesnce Incorrect" error, also the IP addresses are not displayed in IPv6 format.</p><p>Does any one has hex dump of trace packets which has IPv6 format ip addresses?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jul '13, 00:20</strong></p><img src="https://secure.gravatar.com/avatar/c83ca22a760e356093e591f491b6744f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KumarM&#39;s gravatar image" /><p><span>KumarM</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KumarM has no accepted answers">0%</span></p></div></div><div id="comments-container-23095" class="comments-container"></div><div id="comment-tools-23095" class="comment-tools"></div><div class="clear"></div><div id="comment-23095-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="23096"></span>

<div id="answer-container-23096" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23096-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23096-score" class="post-score" title="current number of votes">0</div><span id="post-23096-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There are some captures with IPv6 frames on the Wiki <a href="http://wiki.wireshark.org/SampleCaptures">Sample Captures</a> page. You could export some frames from those as text to compare.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '13, 01:04</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-23096" class="comments-container"></div><div id="comment-tools-23096" class="comment-tools"></div><div class="clear"></div><div id="comment-23096-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

