+++
type = "question"
title = "SharkFest Europe - the missing videos"
description = '''Hi, Thank you very much for uploading videos/material from the Europe conference. I have noticed that some of the videos have not been uploaded (Laura session,etc.) Any chance that the missing videos will be upload as well? https://sharkfesteurope.wireshark.org/sf16eu.html'''
date = "2016-12-27T08:08:00Z"
lastmod = "2016-12-29T01:30:00Z"
weight = 58366
keywords = [ "sharkfest" ]
aliases = [ "/questions/58366" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [SharkFest Europe - the missing videos](/questions/58366/sharkfest-europe-the-missing-videos)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58366-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58366-score" class="post-score" title="current number of votes">1</div><span id="post-58366-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Thank you very much for uploading videos/material from the Europe conference. I have noticed that some of the videos have not been uploaded (Laura session,etc.) Any chance that the missing videos will be upload as well?</p><p><a href="https://sharkfesteurope.wireshark.org/sf16eu.html">https://sharkfesteurope.wireshark.org/sf16eu.html</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-sharkfest" rel="tag" title="see questions tagged &#39;sharkfest&#39;">sharkfest</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Dec '16, 08:08</strong></p><img src="https://secure.gravatar.com/avatar/94630d1ea1108afeafb344e884044d15?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Boaz%20Galil&#39;s gravatar image" /><p><span>Boaz Galil</span><br />
<span class="score" title="56 reputation points">56</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Boaz Galil has no accepted answers">0%</span></p></div></div><div id="comments-container-58366" class="comments-container"></div><div id="comment-tools-58366" class="comment-tools"></div><div class="clear"></div><div id="comment-58366-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58368"></span>

<div id="answer-container-58368" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58368-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58368-score" class="post-score" title="current number of votes">0</div><span id="post-58368-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As I know recording has been performed only in one conference hall out of two, so missing videos do not exist and therefore will not be added unfortunately.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Dec '16, 09:42</strong></p><img src="https://secure.gravatar.com/avatar/1e22670f8d643ca08d658b80a6782932?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Packet_vlad&#39;s gravatar image" /><p><span>Packet_vlad</span><br />
<span class="score" title="436 reputation points">436</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Packet_vlad has 5 accepted answers">20%</span></p></div></div><div id="comments-container-58368" class="comments-container"><span id="58418"></span><div id="comment-58418" class="comment"><div id="post-58418-score" class="comment-score"></div><div class="comment-text"><p>Yes, only talks in the big Athene room were recorded. For talks in the classroom 4 there was no recording. Also there was no recording of Laura's “Troubleshooting with Wireshark” course too.</p></div><div id="comment-58418-info" class="comment-info"><span class="comment-age">(29 Dec '16, 01:30)</span> <span class="comment-user userinfo">Uli</span></div></div></div><div id="comment-tools-58368" class="comment-tools"></div><div class="clear"></div><div id="comment-58368-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

