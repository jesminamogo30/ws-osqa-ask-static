+++
type = "question"
title = "Decrypting TLSv1.2 traffic"
description = '''I have a PCAP of TLSv1.2 traffic that I am attempting to decrypt. Since I have access to the server, I was able to copy it&#x27;s private key. I would like to ask what should I do know to be able to decrypt this traffic. I did add the key to the properties page for the SSL protocol under the key section....'''
date = "2017-01-25T20:43:00Z"
lastmod = "2017-01-26T22:34:00Z"
weight = 59073
keywords = [ "ssl", "tlsv1.2", "decrypt" ]
aliases = [ "/questions/59073" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decrypting TLSv1.2 traffic](/questions/59073/decrypting-tlsv12-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-59073-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-59073-score" class="post-score" title="current number of votes">0</div><span id="post-59073-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a PCAP of TLSv1.2 traffic that I am attempting to decrypt. Since I have access to the server, I was able to copy it's private key. I would like to ask what should I do know to be able to decrypt this traffic. I did add the key to the properties page for the SSL protocol under the key section. The election that I provided was the server IP address, port 443 and protocol http with the link to the location of the .key file. After doing this I saved it and returned to the data and proceeded to follow the stream but to no avail since the data did not decrypt. Is there something that I am missing. Should I be using a file with a different extension??</p><p>Any assistance would be greatly appreciated.</p><p>Thank you Jesus</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-tlsv1.2" rel="tag" title="see questions tagged &#39;tlsv1.2&#39;">tlsv1.2</span> <span class="post-tag tag-link-decrypt" rel="tag" title="see questions tagged &#39;decrypt&#39;">decrypt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jan '17, 20:43</strong></p><img src="https://secure.gravatar.com/avatar/f76e660895fd30cdecf30c8c53f1adae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jdpadro&#39;s gravatar image" /><p><span>jdpadro</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jdpadro has no accepted answers">0%</span></p></div></div><div id="comments-container-59073" class="comments-container"><span id="59094"></span><div id="comment-59094" class="comment"><div id="post-59094-score" class="comment-score"></div><div class="comment-text"><p>you should create the log file and post it here as that would help everyone to understand whats going on. In the SSL protocol preferences, there is a field where you can specify where to save the log file.</p></div><div id="comment-59094-info" class="comment-info"><span class="comment-age">(26 Jan '17, 22:34)</span> <span class="comment-user userinfo">koundi</span></div></div></div><div id="comment-tools-59073" class="comment-tools"></div><div class="clear"></div><div id="comment-59073-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

