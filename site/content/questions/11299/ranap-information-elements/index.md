+++
type = "question"
title = "RANAP INFORMATION ELEMENTS"
description = '''Hello,can somebody out there give me an exemple of ranap captured packets and tell me how to find exactly the information elements?i m really new to this Thank you'''
date = "2012-05-23T18:52:00Z"
lastmod = "2012-05-24T01:21:00Z"
weight = 11299
keywords = [ "ranap" ]
aliases = [ "/questions/11299" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [RANAP INFORMATION ELEMENTS](/questions/11299/ranap-information-elements)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11299-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11299-score" class="post-score" title="current number of votes">0</div><span id="post-11299-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,can somebody out there give me an exemple of ranap captured packets and tell me how to find exactly the information elements?i m really new to this Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ranap" rel="tag" title="see questions tagged &#39;ranap&#39;">ranap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 May '12, 18:52</strong></p><img src="https://secure.gravatar.com/avatar/39e6581ddbc730297526a8b798c9404f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="legrand83&#39;s gravatar image" /><p><span>legrand83</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="legrand83 has no accepted answers">0%</span></p></div></div><div id="comments-container-11299" class="comments-container"></div><div id="comment-tools-11299" class="comment-tools"></div><div class="clear"></div><div id="comment-11299-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="11302"></span>

<div id="answer-container-11302" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11302-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11302-score" class="post-score" title="current number of votes">0</div><span id="post-11302-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Sample Capture: <a href="http://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;do=view&amp;target=3gpp_mc.cap">http://wiki.wireshark.org/SampleCaptures?action=AttachFile&amp;do=view&amp;target=3gpp_mc.cap</a></p><p>Load that file into Wireshark. Then select frame #3 and open the detailed information MTP 3, GSM or whatever you need.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 May '12, 01:21</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-11302" class="comments-container"></div><div id="comment-tools-11302" class="comment-tools"></div><div class="clear"></div><div id="comment-11302-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

