+++
type = "question"
title = "Can wireshark tell me bitrate and frame rate with RTP stream?"
description = '''I&#x27;ve already captured RTP stream I want to study, currently I can get things like total packet, lost packets, sequence errors with RTP stream analysis. Is it possible that wireshark tells me bitrate and frame rate? Thanks~!'''
date = "2015-04-27T15:18:00Z"
lastmod = "2015-04-28T08:55:00Z"
weight = 41900
keywords = [ "bitrate", "rtp" ]
aliases = [ "/questions/41900" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Can wireshark tell me bitrate and frame rate with RTP stream?](/questions/41900/can-wireshark-tell-me-bitrate-and-frame-rate-with-rtp-stream)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41900-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41900-score" class="post-score" title="current number of votes">0</div><span id="post-41900-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I've already captured RTP stream I want to study, currently I can get things like total packet, lost packets, sequence errors with RTP stream analysis.</p><p>Is it possible that wireshark tells me bitrate and frame rate? Thanks~!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bitrate" rel="tag" title="see questions tagged &#39;bitrate&#39;">bitrate</span> <span class="post-tag tag-link-rtp" rel="tag" title="see questions tagged &#39;rtp&#39;">rtp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Apr '15, 15:18</strong></p><img src="https://secure.gravatar.com/avatar/d10e76912ae0a0d745f3451d29395d86?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="DiveDave&#39;s gravatar image" /><p><span>DiveDave</span><br />
<span class="score" title="21 reputation points">21</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="DiveDave has one accepted answer">100%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Apr '15, 15:18</strong> </span></p></div></div><div id="comments-container-41900" class="comments-container"></div><div id="comment-tools-41900" class="comment-tools"></div><div class="clear"></div><div id="comment-41900-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41920"></span>

<div id="answer-container-41920" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41920-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41920-score" class="post-score" title="current number of votes">1</div><span id="post-41920-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="DiveDave has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It can tell you the IP bandwidth it consumes. For the contents (like bitrate / framerate) this depends on the profile the RTP stream is created for. Wireshark has no knowledge of this.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>28 Apr '15, 08:55</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-41920" class="comments-container"></div><div id="comment-tools-41920" class="comment-tools"></div><div class="clear"></div><div id="comment-41920-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

