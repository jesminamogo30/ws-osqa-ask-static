+++
type = "question"
title = "What does the SMB Pipe protocol do?"
description = '''Looking for a description on what the SMB Pipe protocol does. Thanks!'''
date = "2013-01-30T13:15:00Z"
lastmod = "2013-01-30T13:23:00Z"
weight = 18128
keywords = [ "smb" ]
aliases = [ "/questions/18128" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What does the SMB Pipe protocol do?](/questions/18128/what-does-the-smb-pipe-protocol-do)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18128-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18128-score" class="post-score" title="current number of votes">0</div><span id="post-18128-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Looking for a description on what the SMB Pipe protocol does.</p><p>Thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-smb" rel="tag" title="see questions tagged &#39;smb&#39;">smb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jan '13, 13:15</strong></p><img src="https://secure.gravatar.com/avatar/a29e83638930a36751c5a18e11067093?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Honest%20Tea&#39;s gravatar image" /><p><span>Honest Tea</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Honest Tea has no accepted answers">0%</span></p></div></div><div id="comments-container-18128" class="comments-container"></div><div id="comment-tools-18128" class="comment-tools"></div><div class="clear"></div><div id="comment-18128-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18129"></span>

<div id="answer-container-18129" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18129-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18129-score" class="post-score" title="current number of votes">1</div><span id="post-18129-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From the dissector code</p><blockquote><p><code>* Routines for SMB named pipe packet dissection</code><br />
</p></blockquote><p>So, the dissector detects and dissects access to named pipes through SMB.</p><p>For a brief description of SMB named pipes, see here:<br />
</p><blockquote><p><code>http://msdn.microsoft.com/en-us/library/cc239733.aspx</code><br />
</p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Jan '13, 13:23</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 Jan '13, 13:24</strong> </span></p></div></div><div id="comments-container-18129" class="comments-container"></div><div id="comment-tools-18129" class="comment-tools"></div><div class="clear"></div><div id="comment-18129-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

