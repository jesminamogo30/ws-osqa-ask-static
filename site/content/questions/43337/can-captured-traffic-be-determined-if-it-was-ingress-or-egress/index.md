+++
type = "question"
title = "can captured traffic be determined if it was Ingress or egress"
description = '''Hi there Can a captured packet be determined if it is Ingress or egress in relation to a switch if port mirroring was used to capture traffic? Thank you'''
date = "2015-06-18T12:03:00Z"
lastmod = "2015-06-18T13:03:00Z"
weight = 43337
keywords = [ "traffic" ]
aliases = [ "/questions/43337" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can captured traffic be determined if it was Ingress or egress](/questions/43337/can-captured-traffic-be-determined-if-it-was-ingress-or-egress)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43337-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43337-score" class="post-score" title="current number of votes">0</div><span id="post-43337-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there</p><p>Can a captured packet be determined if it is Ingress or egress in relation to a switch if port mirroring was used to capture traffic?</p><p>Thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '15, 12:03</strong></p><img src="https://secure.gravatar.com/avatar/e6076057904575714a9e76205e6f735b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zerowire&#39;s gravatar image" /><p><span>zerowire</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zerowire has no accepted answers">0%</span></p></div></div><div id="comments-container-43337" class="comments-container"></div><div id="comment-tools-43337" class="comment-tools"></div><div class="clear"></div><div id="comment-43337-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="43339"></span>

<div id="answer-container-43339" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43339-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43339-score" class="post-score" title="current number of votes">1</div><span id="post-43339-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>There is no way to see that in the capture file, as the same frame is <strong>ingress</strong> on the switch port where the frame enters the switch and <strong>egress</strong> where is leaves the switch. Unless you know the switch port mirroring configuration, you can't tell the difference.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jun '15, 13:03</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-43339" class="comments-container"></div><div id="comment-tools-43339" class="comment-tools"></div><div class="clear"></div><div id="comment-43339-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

