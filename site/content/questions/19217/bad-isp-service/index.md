+++
type = "question"
title = "Bad ISP service"
description = '''Hi I am a new wireshark user. I would like to know if there is anyway that I can use wireshark to measure bandwidth usage. IE our provider is telling us we are getting x amount of bandwidth but our users keep complaining the internet is slow. Any ideas would be great.'''
date = "2013-03-06T07:29:00Z"
lastmod = "2013-03-06T08:21:00Z"
weight = 19217
keywords = [ "wan" ]
aliases = [ "/questions/19217" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Bad ISP service](/questions/19217/bad-isp-service)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19217-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19217-score" class="post-score" title="current number of votes">0</div><span id="post-19217-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I am a new wireshark user. I would like to know if there is anyway that I can use wireshark to measure bandwidth usage. IE our provider is telling us we are getting x amount of bandwidth but our users keep complaining the internet is slow. Any ideas would be great.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wan" rel="tag" title="see questions tagged &#39;wan&#39;">wan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Mar '13, 07:29</strong></p><img src="https://secure.gravatar.com/avatar/8d478a78f115211020dfe6907f635bef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nycjay01&#39;s gravatar image" /><p><span>nycjay01</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nycjay01 has no accepted answers">0%</span></p></div></div><div id="comments-container-19217" class="comments-container"></div><div id="comment-tools-19217" class="comment-tools"></div><div class="clear"></div><div id="comment-19217-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19218"></span>

<div id="answer-container-19218" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19218-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19218-score" class="post-score" title="current number of votes">0</div><span id="post-19218-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Not really, Wireshark is a network packet analysis tool concentrating on the contents of packets, not the overall flow rate. Wireshark does have some statistics about data transfer but they aren't what you really want.</p><p>There are plenty of other tools that can measure your link speed, e.g. <a href="http://www.speedtest.net/">Speedtest</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Mar '13, 07:42</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-19218" class="comments-container"><span id="19219"></span><div id="comment-19219" class="comment"><div id="post-19219-score" class="comment-score"></div><div class="comment-text"><p>rats :-) I tried all the basic ones already I was hoping wireshark would give some deeper info...</p></div><div id="comment-19219-info" class="comment-info"><span class="comment-age">(06 Mar '13, 07:53)</span> <span class="comment-user userinfo">nycjay01</span></div></div><span id="19222"></span><div id="comment-19222" class="comment"><div id="post-19222-score" class="comment-score"></div><div class="comment-text"><p>Exactly what is it you're trying to find out about your network and why aren't the "basic" tools sufficient?</p></div><div id="comment-19222-info" class="comment-info"><span class="comment-age">(06 Mar '13, 08:21)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-19218" class="comment-tools"></div><div class="clear"></div><div id="comment-19218-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

