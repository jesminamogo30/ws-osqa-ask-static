+++
type = "question"
title = "How to capture all the traffic that passes from my router?"
description = '''Here is the problem. I have one router (Thomson TG585 v8) and two PCs connected to it with ethernet cables. What i want to do is to monitor the traffic of the second computer from the first. Is wireshark the tool i need and if yes how exactly am i going to do that ? Thank you in advance.'''
date = "2011-12-18T11:39:00Z"
lastmod = "2012-11-23T14:14:00Z"
weight = 8031
keywords = [ "router", "traffic", "monitor" ]
aliases = [ "/questions/8031" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture all the traffic that passes from my router?](/questions/8031/how-to-capture-all-the-traffic-that-passes-from-my-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8031-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8031-score" class="post-score" title="current number of votes">0</div><span id="post-8031-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Here is the problem. I have one router (Thomson TG585 v8) and two PCs connected to it with ethernet cables. What i want to do is to monitor the traffic of the second computer from the first. Is wireshark the tool i need and if yes how exactly am i going to do that ? Thank you in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-traffic" rel="tag" title="see questions tagged &#39;traffic&#39;">traffic</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Dec '11, 11:39</strong></p><img src="https://secure.gravatar.com/avatar/5cb38cacbf12bda58675389d8c303596?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="deneb&#39;s gravatar image" /><p><span>deneb</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="deneb has no accepted answers">0%</span></p></div></div><div id="comments-container-8031" class="comments-container"></div><div id="comment-tools-8031" class="comment-tools"></div><div class="clear"></div><div id="comment-8031-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="8032"></span>

<div id="answer-container-8032" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8032-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8032-score" class="post-score" title="current number of votes">0</div><span id="post-8032-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's unlikely that you'll be able to capture the traffic you want with your current setup as I'm pretty certain your router is a switch</p><p>See <a href="http://wiki.wireshark.org/CaptureSetup/Ethernet#Switched_Ethernet">here</a> for info on capturing on switched networks.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Dec '11, 12:12</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-8032" class="comments-container"></div><div id="comment-tools-8032" class="comment-tools"></div><div class="clear"></div><div id="comment-8032-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="16262"></span>

<div id="answer-container-16262" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16262-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16262-score" class="post-score" title="current number of votes">0</div><span id="post-16262-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Thomson router have a facility called port mirroring which will turn a switch port into a 'monitor' port. You can only monitor a single port. If you google 'thomson router port mirroring' you may get a PDF of the commands to set this up from CLI/Telnet.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Nov '12, 14:14</strong></p><img src="https://secure.gravatar.com/avatar/35dca366e05b756c006661c667d07183?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LavaGiant&#39;s gravatar image" /><p><span>LavaGiant</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LavaGiant has no accepted answers">0%</span></p></div></div><div id="comments-container-16262" class="comments-container"></div><div id="comment-tools-16262" class="comment-tools"></div><div class="clear"></div><div id="comment-16262-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

