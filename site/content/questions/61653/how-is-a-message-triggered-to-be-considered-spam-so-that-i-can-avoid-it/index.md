+++
type = "question"
title = "How is a message triggered to be considered spam, so that I can avoid it?"
description = '''I had a rather long post (lots of html formatting, etc). Akismet considered it to be spam and I was advised to contact the forum administrator. I looked around several times, but failed to see a link I could follow to contact a forum administrator. So, I was wondering what the triggers were that fla...'''
date = "2017-05-27T01:35:00Z"
lastmod = "2017-05-29T22:52:00Z"
weight = 61653
keywords = [ "spamissue" ]
aliases = [ "/questions/61653" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How is a message triggered to be considered spam, so that I can avoid it?](/questions/61653/how-is-a-message-triggered-to-be-considered-spam-so-that-i-can-avoid-it)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61653-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61653-score" class="post-score" title="current number of votes">0</div><span id="post-61653-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I had a rather long post (lots of html formatting, etc). Akismet considered it to be spam and I was advised to contact the forum administrator.</p><p>I looked around several times, but failed to see a link I could follow to contact a forum administrator.</p><p>So, I was wondering what the triggers were that flag a post as being potential spam, so that I can remove them if they're in the post? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-spamissue" rel="tag" title="see questions tagged &#39;spamissue&#39;">spamissue</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '17, 01:35</strong></p><img src="https://secure.gravatar.com/avatar/84329f95c80e854a31aeee2a61880b99?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Airsniffer&#39;s gravatar image" /><p><span>Airsniffer</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Airsniffer has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>30 May '17, 02:39</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-61653" class="comments-container"></div><div id="comment-tools-61653" class="comment-tools"></div><div class="clear"></div><div id="comment-61653-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61687"></span>

<div id="answer-container-61687" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61687-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61687-score" class="post-score" title="current number of votes">0</div><span id="post-61687-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I've no specific info on what causes a question to be flagged, but I think an excess of links and a low original poster "karma" might be involved.</p><p>Try posting in plain text, an admin will reformat if required.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 May '17, 07:55</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-61687" class="comments-container"><span id="61688"></span><div id="comment-61688" class="comment"><div id="post-61688-score" class="comment-score"></div><div class="comment-text"><p>Perhaps Wireshark Q&amp;A has a custom comment blacklist and one or more words being used is on that list?</p><p>A quick google search found an example of such a list: <a href="https://digwp.com/2010/02/stop-spammers-custom-blacklist/">https://digwp.com/2010/02/stop-spammers-custom-blacklist/</a></p><p>An even bigger example of a blacklist is linked from here: <a href="https://github.com/splorp/wordpress-comment-blacklist">https://github.com/splorp/wordpress-comment-blacklist</a></p><p>I still don't know if Wireshark is using such a list, but I suspect so.</p></div><div id="comment-61688-info" class="comment-info"><span class="comment-age">(29 May '17, 08:16)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div><span id="61689"></span><div id="comment-61689" class="comment"><div id="post-61689-score" class="comment-score"></div><div class="comment-text"><p>Thanks, grahamb. Going off of what you mentioned, it may be the HTML formatting that's flagging the post. I will try to post a very short version of it and see how that works.</p><p>I tried to upvote your response, but a message told me that I needed around 15 points and that I only have 6 so far.</p></div><div id="comment-61689-info" class="comment-info"><span class="comment-age">(29 May '17, 22:52)</span> <span class="comment-user userinfo">Airsniffer</span></div></div></div><div id="comment-tools-61687" class="comment-tools"></div><div class="clear"></div><div id="comment-61687-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

