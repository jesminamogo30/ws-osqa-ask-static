+++
type = "question"
title = "Ip scan in window"
description = '''I want to scan IP address of device which I forgot . how to scan IP of that device using wireshark 1.0.0'''
date = "2016-01-18T20:26:00Z"
lastmod = "2016-01-19T00:49:00Z"
weight = 49348
keywords = [ "device", "ip", "of", "scan" ]
aliases = [ "/questions/49348" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Ip scan in window](/questions/49348/ip-scan-in-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49348-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49348-score" class="post-score" title="current number of votes">0</div><span id="post-49348-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to scan IP address of device which I forgot . how to scan IP of that device using wireshark 1.0.0</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-device" rel="tag" title="see questions tagged &#39;device&#39;">device</span> <span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-of" rel="tag" title="see questions tagged &#39;of&#39;">of</span> <span class="post-tag tag-link-scan" rel="tag" title="see questions tagged &#39;scan&#39;">scan</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jan '16, 20:26</strong></p><img src="https://secure.gravatar.com/avatar/29ae218b73b40f4f108b24d73462e086?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kumudnitp&#39;s gravatar image" /><p><span>Kumudnitp</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kumudnitp has no accepted answers">0%</span></p></div></div><div id="comments-container-49348" class="comments-container"></div><div id="comment-tools-49348" class="comment-tools"></div><div class="clear"></div><div id="comment-49348-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="49349"></span>

<div id="answer-container-49349" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49349-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49349-score" class="post-score" title="current number of votes">0</div><span id="post-49349-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It's not clear from your question exactly what you're trying to accomplish. The following should get you going in the right direction.</p><p>First, Wireshark 1.0.0 is a very old version. I recommend you upgrade to the latest version (2.0.1 at this time). Wireshark is free, and easy to install. You can get the latest copy from wireshark.org.</p><p>Once you have the current version, instructions for How To Set Up a Capture can be found at <a href="https://wiki.wireshark.org/CaptureSetup">https://wiki.wireshark.org/CaptureSetup</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jan '16, 21:25</strong></p><img src="https://secure.gravatar.com/avatar/b260fb38b621169269b5030f1ed6b766?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="griff&#39;s gravatar image" /><p><span>griff</span><br />
<span class="score" title="361 reputation points">361</span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="griff has 2 accepted answers">10%</span></p></div></div><div id="comments-container-49349" class="comments-container"></div><div id="comment-tools-49349" class="comment-tools"></div><div class="clear"></div><div id="comment-49349-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="49358"></span>

<div id="answer-container-49358" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49358-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49358-score" class="post-score" title="current number of votes">0</div><span id="post-49358-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I <em>assume</em> it is the usual case of a blackbox which you've taken from the shelf after a year or more and want to connect to it. If so, it all depends whether that box was configured to want something from the network (like e.g. NTP) or not after being powered up. If it did, take a switch or hub, connect your capture device's NIC to one of its ports and let it go up so that you could capture the other device's traffic as soon as its own Ethernet port gets up. Then start capturing, and then connect the blackbox to another port of the switch/hub. And now wait for an ARP request which is not sent from capturing device's own MAC address. That ARP request, if it ever comes, will indicate the IP address of the blackbox and usually ask for the default gateway's MAC address, so you may be able to guess the subnet mask from the two taken together.</p><p>If it is a router, this may work but still be useless because the router would send ARP requests at its WAN interface but management access through the WAN interface would be blocked.</p><p>If so, Wireshark and passive watching would not be enough and you'd have to use some tool actively attempting to log in or at least ping. Broadcast ping may help speed this process up if the device's IP stack responds to broadcast pings, which is not always the case. So if you have another device of the same type which you can use to check this, it may help reduce the time you'll need.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jan '16, 00:49</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jan '16, 03:58</strong> </span></p></div></div><div id="comments-container-49358" class="comments-container"></div><div id="comment-tools-49358" class="comment-tools"></div><div class="clear"></div><div id="comment-49358-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

