+++
type = "question"
title = "My macintosh won&#x27;t start after trying to install Wireshark"
description = '''Hello, I have a MacBook 10.5.6 and after tried to install Wireshark It asked me to put a folder to my &quot;OpenonStartup&quot; mac folder, after this I restarted, and my computer warned me to only allow that WireShark Foldar into my StartupFolder if I trusted it. I click &quot;Allowed&quot; and prompted for a Restart....'''
date = "2010-11-16T10:59:00Z"
lastmod = "2010-11-16T11:08:00Z"
weight = 975
keywords = [ "apple", "startup", "troubleshooting", "restart", "macintosh" ]
aliases = [ "/questions/975" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [My macintosh won't start after trying to install Wireshark](/questions/975/my-macintosh-wont-start-after-trying-to-install-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-975-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-975-score" class="post-score" title="current number of votes">0</div><span id="post-975-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I have a MacBook 10.5.6 and after tried to install Wireshark It asked me to put a folder to my "OpenonStartup" mac folder, after this I restarted, and my computer warned me to only allow that WireShark Foldar into my StartupFolder if I trusted it. I click "Allowed" and prompted for a Restart.</p><p>After the restart, I will get the greyish screen with the apple for like 5 seconds, then my computer will turn back off.</p><p>I've tried to start it several times, but with no gain.</p><p>Is this a common problem? Can you guys help me solve it? Thanks in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-apple" rel="tag" title="see questions tagged &#39;apple&#39;">apple</span> <span class="post-tag tag-link-startup" rel="tag" title="see questions tagged &#39;startup&#39;">startup</span> <span class="post-tag tag-link-troubleshooting" rel="tag" title="see questions tagged &#39;troubleshooting&#39;">troubleshooting</span> <span class="post-tag tag-link-restart" rel="tag" title="see questions tagged &#39;restart&#39;">restart</span> <span class="post-tag tag-link-macintosh" rel="tag" title="see questions tagged &#39;macintosh&#39;">macintosh</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Nov '10, 10:59</strong></p><img src="https://secure.gravatar.com/avatar/0ebb8e9ddbbf3ac74ebca976d2059dfb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MarsComet&#39;s gravatar image" /><p><span>MarsComet</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MarsComet has no accepted answers">0%</span></p></div></div><div id="comments-container-975" class="comments-container"><span id="976"></span><div id="comment-976" class="comment"><div id="post-976-score" class="comment-score"></div><div class="comment-text"><p>The thing I put at the Startup Folder is called "ChmodBPF"</p></div><div id="comment-976-info" class="comment-info"><span class="comment-age">(16 Nov '10, 11:08)</span> <span class="comment-user userinfo">MarsComet</span></div></div></div><div id="comment-tools-975" class="comment-tools"></div><div class="clear"></div><div id="comment-975-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

