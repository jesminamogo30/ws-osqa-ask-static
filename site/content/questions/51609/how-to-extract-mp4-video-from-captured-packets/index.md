+++
type = "question"
title = "How to extract mp4 video from captured packets"
description = '''I have captured TCP packets between my Parrot drone 2.0 and the Ipad, now after extensive research we came to a conclusion that port 5555 is where the video packets being sent from the drone. Using wireshark I was able to capture all the packets and save them as .pcap extension, but I can&#x27;t extract ...'''
date = "2016-04-12T11:01:00Z"
lastmod = "2016-04-15T13:23:00Z"
weight = 51609
keywords = [ "packets", "mp4", "pcap" ]
aliases = [ "/questions/51609" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to extract mp4 video from captured packets](/questions/51609/how-to-extract-mp4-video-from-captured-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-51609-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-51609-score" class="post-score" title="current number of votes">0</div><span id="post-51609-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have captured TCP packets between my Parrot drone 2.0 and the Ipad, now after extensive research we came to a conclusion that port 5555 is where the video packets being sent from the drone.</p><p>Using wireshark I was able to capture all the packets and save them as .pcap extension, but I can't extract the video from the packets and play it in VLC, is there any way where I can extract the video from the packets and play the video using VLC or any other software?, Is there any available tools for this purpose or should I use something different than wireshark were its possible to stream while capturing?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-mp4" rel="tag" title="see questions tagged &#39;mp4&#39;">mp4</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Apr '16, 11:01</strong></p><img src="https://secure.gravatar.com/avatar/6d25cb106464a69d9469c9fa6b5cfb6b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Saad%20Marouane&#39;s gravatar image" /><p><span>Saad Marouane</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Saad Marouane has no accepted answers">0%</span></p></div></div><div id="comments-container-51609" class="comments-container"><span id="51706"></span><div id="comment-51706" class="comment"><div id="post-51706-score" class="comment-score"></div><div class="comment-text"><p>What version of Wireshark are you analyzing the capture? Is it the new Wireshark (Qt) or the legacy (GTK+)?</p><p>I ask because I have not been able to extract any video using the new Wireshark (Qt). You can determine the Wireshark version by selecting: 1. Help 2. About Wireshark 3. Look for the Compiled version (3rd section, usually). It will say either Qt or GTK+</p><p>I would recommend using the GTK+ version for now</p></div><div id="comment-51706-info" class="comment-info"><span class="comment-age">(15 Apr '16, 13:23)</span> <span class="comment-user userinfo">Amato_C</span></div></div></div><div id="comment-tools-51609" class="comment-tools"></div><div class="clear"></div><div id="comment-51609-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

