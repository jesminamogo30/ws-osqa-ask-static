+++
type = "question"
title = "How to calculate jitter, delay, packet loss and throughput in the FTP and HTTP using wireshark?"
description = '''Hi everyone, I&#x27;m doing research on the value of QOS on FTP and HTTP. How do I know jitter, delay, packet loss and throughput in the FTP and HTTP using wireshark? and how to filter in wireshark? thank you'''
date = "2016-12-19T23:18:00Z"
lastmod = "2016-12-19T23:18:00Z"
weight = 58252
keywords = [ "ftp", "http", "wireshark" ]
aliases = [ "/questions/58252" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to calculate jitter, delay, packet loss and throughput in the FTP and HTTP using wireshark?](/questions/58252/how-to-calculate-jitter-delay-packet-loss-and-throughput-in-the-ftp-and-http-using-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58252-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58252-score" class="post-score" title="current number of votes">0</div><span id="post-58252-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone, I'm doing research on the value of QOS on FTP and HTTP. How do I know jitter, delay, packet loss and throughput in the FTP and HTTP using wireshark? and how to filter in wireshark? thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Dec '16, 23:18</strong></p><img src="https://secure.gravatar.com/avatar/f8262ffe01b75ac52d7d7e0c958ac518?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aya&#39;s gravatar image" /><p><span>Aya</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aya has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Dec '16, 23:19</strong> </span></p></div></div><div id="comments-container-58252" class="comments-container"></div><div id="comment-tools-58252" class="comment-tools"></div><div class="clear"></div><div id="comment-58252-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

