+++
type = "question"
title = "how to interpret the second grey line in tcprace graph?"
description = '''Hi, I&#x27;m now studying the graph representation of tcptrace. There are three lines shown up when I zoom in the trace. I current understanding for the two lines is as following figures, and what is the 3rd one? is it graphing the receiver host&#x27;s ACK number? '''
date = "2014-08-09T09:35:00Z"
lastmod = "2014-08-09T10:02:00Z"
weight = 35347
keywords = [ "tcptrace" ]
aliases = [ "/questions/35347" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [how to interpret the second grey line in tcprace graph?](/questions/35347/how-to-interpret-the-second-grey-line-in-tcprace-graph)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35347-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35347-score" class="post-score" title="current number of votes">0</div><span id="post-35347-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I'm now studying the graph representation of tcptrace. There are three lines shown up when I zoom in the trace. I current understanding for the two lines is as following figures, and what is the 3rd one? is it graphing the receiver host's ACK number? <img src="https://osqa-ask.wireshark.org/upfiles/8-10-2014_12-09-31_AM.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcptrace" rel="tag" title="see questions tagged &#39;tcptrace&#39;">tcptrace</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Aug '14, 09:35</strong></p><img src="https://secure.gravatar.com/avatar/2d1a8885858c8435654658b25f489bd9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SteveZhou&#39;s gravatar image" /><p><span>SteveZhou</span><br />
<span class="score" title="191 reputation points">191</span><span title="27 badges"><span class="badge1">●</span><span class="badgecount">27</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SteveZhou has no accepted answers">0%</span></p></img></div></div><div id="comments-container-35347" class="comments-container"></div><div id="comment-tools-35347" class="comment-tools"></div><div class="clear"></div><div id="comment-35347-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35348"></span>

<div id="answer-container-35348" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35348-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35348-score" class="post-score" title="current number of votes">3</div><span id="post-35348-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="SteveZhou has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>check this one - <a href="http://packetbomb.com/understanding-the-tcptrace-time-sequence-graph-in-wireshark/">http://packetbomb.com/understanding-the-tcptrace-time-sequence-graph-in-wireshark/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Aug '14, 10:02</strong></p><img src="https://secure.gravatar.com/avatar/6f9cdab5081b4272d1abf703a2689372?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kishan%20pandey&#39;s gravatar image" /><p><span>kishan pandey</span><br />
<span class="score" title="221 reputation points">221</span><span title="28 badges"><span class="badge1">●</span><span class="badgecount">28</span></span><span title="29 badges"><span class="silver">●</span><span class="badgecount">29</span></span><span title="36 badges"><span class="bronze">●</span><span class="badgecount">36</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kishan pandey has 2 accepted answers">28%</span></p></div></div><div id="comments-container-35348" class="comments-container"></div><div id="comment-tools-35348" class="comment-tools"></div><div class="clear"></div><div id="comment-35348-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

