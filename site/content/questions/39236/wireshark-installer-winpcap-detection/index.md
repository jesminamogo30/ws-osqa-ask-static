+++
type = "question"
title = "Wireshark Installer WinPcap Detection"
description = '''I think there is a minor problem about Wireshark Installer&#x27;s WinPcap Detection. I encountered with this issue while I was doing following steps:  My PC had not have any Wireshark or Winpcap version at first. Then I downloaded WinPcap 4.1.3 and I installed WinPcap 4.1.3 to my PC. After then I downloa...'''
date = "2015-01-17T12:40:00Z"
lastmod = "2015-01-18T10:40:00Z"
weight = 39236
keywords = [ "detection", "winpcap", "installer", "wireshark" ]
aliases = [ "/questions/39236" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Installer WinPcap Detection](/questions/39236/wireshark-installer-winpcap-detection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39236-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39236-score" class="post-score" title="current number of votes">0</div><span id="post-39236-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I think there is a minor problem about Wireshark Installer's WinPcap Detection. I encountered with this issue while I was doing following steps:</p><ol><li>My PC had not have any Wireshark or Winpcap version at first.</li><li>Then I downloaded WinPcap 4.1.3 and I installed WinPcap 4.1.3 to my PC.</li><li>After then I downloaded Wireshark-win64-1.12.3.exe. I started to installed it. At the beginning, installer page says: "WinPcap is currently not installed". So I had to choose Install Winpcap checkbox.</li><li>Then WinPcap setup page gives me "WinPcap 4.1.3 is already installed on this machine. Press ok if you want to force ..." warning.</li></ol><p>How do we solve this issue? Why Wireshark Installer does not detect WinPcap?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-detection" rel="tag" title="see questions tagged &#39;detection&#39;">detection</span> <span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span> <span class="post-tag tag-link-installer" rel="tag" title="see questions tagged &#39;installer&#39;">installer</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jan '15, 12:40</strong></p><img src="https://secure.gravatar.com/avatar/6257a856e7271c04dd39469c7a5332ee?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BirolCapa&#39;s gravatar image" /><p><span>BirolCapa</span><br />
<span class="score" title="30 reputation points">30</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BirolCapa has no accepted answers">0%</span></p></div></div><div id="comments-container-39236" class="comments-container"></div><div id="comment-tools-39236" class="comment-tools"></div><div class="clear"></div><div id="comment-39236-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39242"></span>

<div id="answer-container-39242" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39242-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39242-score" class="post-score" title="current number of votes">0</div><span id="post-39242-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please file a bug report at <a href="https://bugs.wireshark.org/bugzilla/">https://bugs.wireshark.org/bugzilla/</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jan '15, 07:45</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-39242" class="comments-container"><span id="39251"></span><div id="comment-39251" class="comment"><div id="post-39251-score" class="comment-score"></div><div class="comment-text"><p>I reported it.</p></div><div id="comment-39251-info" class="comment-info"><span class="comment-age">(18 Jan '15, 09:41)</span> <span class="comment-user userinfo">BirolCapa</span></div></div><span id="39252"></span><div id="comment-39252" class="comment"><div id="post-39252-score" class="comment-score"></div><div class="comment-text"><p>For reference it's <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10867">Bug: 10867</a></p></div><div id="comment-39252-info" class="comment-info"><span class="comment-age">(18 Jan '15, 10:40)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-39242" class="comment-tools"></div><div class="clear"></div><div id="comment-39242-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

