+++
type = "question"
title = "monitor all traffic travelling through router"
description = '''How do i connect wireshark to a router, so that i can view the packets that are flowing through the router?'''
date = "2011-09-05T23:08:00Z"
lastmod = "2011-09-06T00:57:00Z"
weight = 6103
keywords = [ "wireshark" ]
aliases = [ "/questions/6103" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [monitor all traffic travelling through router](/questions/6103/monitor-all-traffic-travelling-through-router)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6103-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6103-score" class="post-score" title="current number of votes">0</div><span id="post-6103-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do i connect wireshark to a router, so that i can view the packets that are flowing through the router?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Sep '11, 23:08</strong></p><img src="https://secure.gravatar.com/avatar/f66ee1b7fb150d2a90025ad945cd6e1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Elahe&#39;s gravatar image" /><p><span>Elahe</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Elahe has no accepted answers">0%</span></p></div></div><div id="comments-container-6103" class="comments-container"></div><div id="comment-tools-6103" class="comment-tools"></div><div class="clear"></div><div id="comment-6103-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="6113"></span>

<div id="answer-container-6113" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6113-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6113-score" class="post-score" title="current number of votes">0</div><span id="post-6113-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Have a look at the Capture setup page on the wiki, <a href="http://wiki.wireshark.org/CaptureSetup">HERE</a>. Note that most routers are switches, and if you can't force the router to span or mirror all traffic onto a monitoring port you won't be able to see all traffic.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Sep '11, 00:57</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-6113" class="comments-container"></div><div id="comment-tools-6113" class="comment-tools"></div><div class="clear"></div><div id="comment-6113-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

