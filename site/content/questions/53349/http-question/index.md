+++
type = "question"
title = "HTTP question"
description = '''i&#x27;m facing this question in my homework and i don&#x27;t find an answer for 3 days... the question is &quot;with Base HTML file How many things you Requested from the server ??&quot; i don&#x27;t know how to know it  please help.. '''
date = "2016-06-10T09:43:00Z"
lastmod = "2016-06-21T01:35:00Z"
weight = 53349
keywords = [ "http" ]
aliases = [ "/questions/53349" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [HTTP question](/questions/53349/http-question)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53349-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53349-score" class="post-score" title="current number of votes">-1</div><span id="post-53349-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>i'm facing this question in my homework and i don't find an answer for 3 days... the question is "with Base HTML file How many things you Requested from the server ??" i don't know how to know it please help..</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jun '16, 09:43</strong></p><img src="https://secure.gravatar.com/avatar/754ca1bde18ee4512b33f7a569be5b82?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Omar%20Ramadan&#39;s gravatar image" /><p><span>Omar Ramadan</span><br />
<span class="score" title="0 reputation points">0</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Omar Ramadan has no accepted answers">0%</span></p></div></div><div id="comments-container-53349" class="comments-container"><span id="53351"></span><div id="comment-53351" class="comment"><div id="post-53351-score" class="comment-score">1</div><div class="comment-text"><p>42 (which is the Answer to the Ultimate Question of Life, The Universe, and Everything). No really, I don't understand the question.</p></div><div id="comment-53351-info" class="comment-info"><span class="comment-age">(10 Jun '16, 14:57)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="53507"></span><div id="comment-53507" class="comment"><div id="post-53507-score" class="comment-score">1</div><div class="comment-text"><p>Hm, looking at it here, I guess my assumption at the other question was right. You are supposed to use Wireshark to extract from the captured packets the html document which the client has requested initially (which the teacher seems to call a Base HTML file), and to find how many http:// links (to other objects to be rendered in the browser window) are inside that document.</p></div><div id="comment-53507-info" class="comment-info"><span class="comment-age">(16 Jun '16, 14:49)</span> <span class="comment-user userinfo">sindy</span></div></div><span id="53537"></span><div id="comment-53537" class="comment"><div id="post-53537-score" class="comment-score"></div><div class="comment-text"><p>I think he wants "HTTP Get request messages"</p></div><div id="comment-53537-info" class="comment-info"><span class="comment-age">(17 Jun '16, 08:36)</span> <span class="comment-user userinfo">Omar Ramadan</span></div></div><span id="53581"></span><div id="comment-53581" class="comment"><div id="post-53581-score" class="comment-score"></div><div class="comment-text"><p>Well, I'd guess that if he explicitly asks "how many things from the base HTML file", there would be a catch that there would be also additional http GETs in the capture which would not be triggered by the contents of the base file. So he probably wants you to use the feature of Wireshark which allows you to export the base file from the packet exchange, and to analyse its contents for references to other "things" (pictures, style files, flash animations, whatever).</p></div><div id="comment-53581-info" class="comment-info"><span class="comment-age">(21 Jun '16, 01:35)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-53349" class="comment-tools"></div><div class="clear"></div><div id="comment-53349-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

