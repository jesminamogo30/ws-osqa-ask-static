+++
type = "question"
title = "black lines are gone on 2nd live capture"
description = '''Hi there, yesterday I installed Wireshark and ran a first check. Like 25 black lines and some red too appeared. When I ran a 2nd round hours later on all interfaces they were gone. Last time of live capture everthing seems to be normal apart from a lot of ARP&#x27;s. Is this normal a normal pattern for W...'''
date = "2015-02-26T04:44:00Z"
lastmod = "2015-02-26T04:48:00Z"
weight = 40095
keywords = [ "arp", "lines", "black", "wireshark" ]
aliases = [ "/questions/40095" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [black lines are gone on 2nd live capture](/questions/40095/black-lines-are-gone-on-2nd-live-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40095-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40095-score" class="post-score" title="current number of votes">0</div><span id="post-40095-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there, yesterday I installed Wireshark and ran a first check. Like 25 black lines and some red too appeared. When I ran a 2nd round hours later on all interfaces they were gone. Last time of live capture everthing seems to be normal apart from a lot of ARP's. Is this normal a normal pattern for Wireshark. Thanx</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-arp" rel="tag" title="see questions tagged &#39;arp&#39;">arp</span> <span class="post-tag tag-link-lines" rel="tag" title="see questions tagged &#39;lines&#39;">lines</span> <span class="post-tag tag-link-black" rel="tag" title="see questions tagged &#39;black&#39;">black</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Feb '15, 04:44</strong></p><img src="https://secure.gravatar.com/avatar/d2cae4ae6e0e8fa01f1f75a63be620da?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Quasi&#39;s gravatar image" /><p><span>Quasi</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Quasi has no accepted answers">0%</span></p></div></div><div id="comments-container-40095" class="comments-container"></div><div id="comment-tools-40095" class="comment-tools"></div><div class="clear"></div><div id="comment-40095-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40097"></span>

<div id="answer-container-40097" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40097-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40097-score" class="post-score" title="current number of votes">0</div><span id="post-40097-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>Is this normal a normal pattern for Wireshark.</p></blockquote><p>Yes.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Feb '15, 04:48</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-40097" class="comments-container"></div><div id="comment-tools-40097" class="comment-tools"></div><div class="clear"></div><div id="comment-40097-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

