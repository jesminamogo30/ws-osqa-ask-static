+++
type = "question"
title = "how to analyze unreliable downloads"
description = '''For weeks I have had trouble getting my computers to reliably download from the web. Browsers work fine, and my internal network also seems to work fine. But pretty much everything I download from the web comes in as corrupt. IE eihter complains that the file is corrupt, Mozilla stops the download a...'''
date = "2016-02-11T16:19:00Z"
lastmod = "2016-02-11T16:19:00Z"
weight = 50117
keywords = [ "downloads", "corrupted" ]
aliases = [ "/questions/50117" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [how to analyze unreliable downloads](/questions/50117/how-to-analyze-unreliable-downloads)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50117-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50117-score" class="post-score" title="current number of votes">0</div><span id="post-50117-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>For weeks I have had trouble getting my computers to reliably download from the web. Browsers work fine, and my internal network also seems to work fine. But pretty much everything I download from the web comes in as corrupt. IE eihter complains that the file is corrupt, Mozilla stops the download after a few seconds and tells me that the download was interrupted, When I do get a file, the checksum is wrong, larger files (videos) cannot be run, etc. This happens on at least 3 computers connected to my network (All Windows, one Win7, one Win8, one WinServer 2011). To get to the bottom of this, I ran Wireshark for some downloads, and the logs do show some issues, but I don't know where to go from here.</p><p>The log shows the computer happily running along with lines that I think show the progress. Then suddenly black lines appear: "TCP Dup" and "TCP previous segment not captured"(how can I post the lines here?)</p><p>So something is going wrong. How do I further analyze that and, more importantly, fix that?</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-downloads" rel="tag" title="see questions tagged &#39;downloads&#39;">downloads</span> <span class="post-tag tag-link-corrupted" rel="tag" title="see questions tagged &#39;corrupted&#39;">corrupted</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Feb '16, 16:19</strong></p><img src="https://secure.gravatar.com/avatar/2a6d8feeba784a7ae046658a3efe82ed?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mb_1&#39;s gravatar image" /><p><span>mb_1</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mb_1 has no accepted answers">0%</span></p></div></div><div id="comments-container-50117" class="comments-container"></div><div id="comment-tools-50117" class="comment-tools"></div><div class="clear"></div><div id="comment-50117-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

