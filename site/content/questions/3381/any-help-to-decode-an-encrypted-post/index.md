+++
type = "question"
title = "Any help to decode an encrypted POST"
description = '''I may have some data loss to Romania. How can I decode what I have magaged to capture? Thanks in advance. Please teach me so I can apply this to future captures. And maybe help others.  POST /news/ HTTP/1.1 Accept: */* User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727...'''
date = "2011-04-06T14:18:00Z"
lastmod = "2011-04-27T14:42:00Z"
weight = 3381
keywords = [ "decode", "zeus-bot" ]
aliases = [ "/questions/3381" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Any help to decode an encrypted POST](/questions/3381/any-help-to-decode-an-encrypted-post)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3381-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3381-score" class="post-score" title="current number of votes">0</div><span id="post-3381-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I may have some data loss to Romania. How can I decode what I have magaged to capture? Thanks in advance. Please teach me so I can apply this to future captures. And maybe help others.</p><pre><code>POST /news/ HTTP/1.1
Accept: */*
User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; InfoPath.1; .NET4.0C; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)
Host: zsmoupnlyotrokq.info
Content-Length: 292
Connection: Keep-Alive
Cache-Control: no-cache

.y..:......9#.T.&#39;...&quot;...`...F...N.....D..W
... ..6..u...)[email protected]=$.#.M....5&quot;..\s.....}@F....0a\..a.ci..rr.2D..GJp..zP..!..oW..u...w.p..XCR...d.^..&amp;wO.t_.^.m...
.3.w...a.....13.,Jh86*.AS#G.m.k..t.B../.]o.jOB..T....R...~a.K[9..Ps...|,&amp;../....Q.R?H.k&gt;..&quot;..._[... ]Yu....E.I....M.`.}..*...</code></pre><p>Their response ack the data</p><pre><code>HTTP/1.1 200 OK
Server: nginx/0.6.32
Date: Wed, 06 Apr 2011 22:51:07 GMT
Content-Type: text/html
Connection: close
X-Powered-By: PHP/5.2.6-1+lenny9
Vary: Accept-Encoding
Content-Length: 196

.s....r.~....b2
.*..3...p...\.....
l.g.[..|.u.U~..W...m...[..kW.g.+.q..F3.9s_.&amp;.8..Bm9.....U.......ip.&quot;.d..._........E...$..
9= ..7|..G[h.d..[[email protected]~.N.Jp....&amp;..DS..2...x..u...
K....B&gt;..c../=..T....</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-decode" rel="tag" title="see questions tagged &#39;decode&#39;">decode</span> <span class="post-tag tag-link-zeus-bot" rel="tag" title="see questions tagged &#39;zeus-bot&#39;">zeus-bot</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Apr '11, 14:18</strong></p><img src="https://secure.gravatar.com/avatar/1307e9c0b8d6dde0fdadb1cc05eb75e7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Onebusytech&#39;s gravatar image" /><p><span>Onebusytech</span><br />
<span class="score" title="1 reputation points">1</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Onebusytech has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> retagged <strong>09 Apr '11, 07:35</strong> </span></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span></p></div></div><div id="comments-container-3381" class="comments-container"></div><div id="comment-tools-3381" class="comment-tools"></div><div class="clear"></div><div id="comment-3381-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3409"></span>

<div id="answer-container-3409" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3409-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3409-score" class="post-score" title="current number of votes">1</div><span id="post-3409-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Jasper has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What you got here is trouble.</p><p>The request goes out to the domain zsmoupnlyotrokq.info. At this time the domain does not exist (nslookup returns "non-existent domain".</p><p>The most remarkable website <a href="http://www.abuse.ch" title="abuse.ch">abuse.ch</a> also hosts the <a href="http://zeustracker.abuse.ch" title="Zeustracker">Zeustracker</a>, a website dedicated to tracking computers infected with the Zeus trojan horse.</p><p>As the domain zsmoupnlyotrokq.info does not exist Zeustracker is currently not reporting it. However, the google cache for Zeustracker still has the entry:</p><p><img src="http://www.synerity.com/images/Wireshark-zeustracker-archive.png" title="Google archive" alt="Zeustracker Screenshot from Google archive" /></p><p>So your trace file shows the encrypted command &amp; control traffic for Zeus.</p><p>Alas, decrypting Zeus traffic is not for the faint of heart. Unfortunately, Zeus does not use a single master key. Finding the key for your infection requires a decent amount of reverse engineering. One possible approach is listed in a blog at <a href="http://blog.threatexpert.com/2009/09/time-to-revisit-zeus-almighty.html" title="threadexpert.com">threadexpert.com</a>.</p><p>Depending on your version of Zeus this information could already be outdated.</p><p>Good hunting, you have a worthy target! :-)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Apr '11, 07:19</strong></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packethunter has 8 accepted answers">8%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Apr '11, 07:34</strong> </span></p></div></div><div id="comments-container-3409" class="comments-container"><span id="3760"></span><div id="comment-3760" class="comment"><div id="post-3760-score" class="comment-score"></div><div class="comment-text"><p>Thanks for your time and assistance in reviewing this for me. I suspected as much. But it does help to strengthen my message that they should take this very seriously. I advised them to inform the user of the risks and possible loss of personal information that could have occurred.</p></div><div id="comment-3760-info" class="comment-info"><span class="comment-age">(27 Apr '11, 13:48)</span> <span class="comment-user userinfo">Onebusytech</span></div></div><span id="3763"></span><div id="comment-3763" class="comment"><div id="post-3763-score" class="comment-score"></div><div class="comment-text"><p>You might want to accept the answer then to get the question off the open questions list ;-)</p></div><div id="comment-3763-info" class="comment-info"><span class="comment-age">(27 Apr '11, 14:42)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-3409" class="comment-tools"></div><div class="clear"></div><div id="comment-3409-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

