+++
type = "question"
title = "wireshark 2.2x - I/O graph guide"
description = '''Hi Experts, I tried searching internet for I/O graph guide (for wireshark 2.2.x versions). However I could not find any helpful content. I went through wireshark 2.0 release video posted on youtube. But that too contains minimal info on I/O graph. I need your advise to about latest guide / blog / vi...'''
date = "2016-11-27T21:08:00Z"
lastmod = "2016-11-27T21:08:00Z"
weight = 57670
keywords = [ "performance", "iographs" ]
aliases = [ "/questions/57670" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [wireshark 2.2x - I/O graph guide](/questions/57670/wireshark-22x-io-graph-guide)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57670-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57670-score" class="post-score" title="current number of votes">0</div><span id="post-57670-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Experts,</p><p>I tried searching internet for I/O graph guide (for wireshark 2.2.x versions). However I could not find any helpful content. I went through wireshark 2.0 release video posted on youtube. But that too contains minimal info on I/O graph.</p><p>I need your advise to about latest guide / blog / video posts / articles available wherein I can learn to exploit power of I/O graphs in latest wireshark versions ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-performance" rel="tag" title="see questions tagged &#39;performance&#39;">performance</span> <span class="post-tag tag-link-iographs" rel="tag" title="see questions tagged &#39;iographs&#39;">iographs</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Nov '16, 21:08</strong></p><img src="https://secure.gravatar.com/avatar/d1e5efe891c907bf6be8231eca9db31a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Vijay%20Gharge&#39;s gravatar image" /><p><span>Vijay Gharge</span><br />
<span class="score" title="36 reputation points">36</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="20 badges"><span class="bronze">●</span><span class="badgecount">20</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Vijay Gharge has no accepted answers">0%</span></p></div></div><div id="comments-container-57670" class="comments-container"></div><div id="comment-tools-57670" class="comment-tools"></div><div class="clear"></div><div id="comment-57670-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

