+++
type = "question"
title = "menu &amp; icons fonts too big!"
description = '''Hi, I&#x27;m running Wireshark 1.12 on Ubuntu Linux, but my screen is very small, 10&#x27;inc of a netbook. All dropdown menus, setting menus and icons are too big, and don&#x27;t fit in the monitor. I practically can&#x27;t see any settings! Please, how can I lower dimensions of font menus, settings and main icons? Th...'''
date = "2015-04-01T18:02:00Z"
lastmod = "2015-04-01T18:02:00Z"
weight = 41115
keywords = [ "font" ]
aliases = [ "/questions/41115" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [menu & icons fonts too big!](/questions/41115/menu-icons-fonts-too-big)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41115-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41115-score" class="post-score" title="current number of votes">0</div><span id="post-41115-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I'm running Wireshark 1.12 on Ubuntu Linux, but my screen is very small, 10'inc of a netbook. All dropdown menus, setting menus and icons are too big, and don't fit in the monitor. I practically can't see any settings!<br />
Please, how can I lower dimensions of font menus, settings and main icons?<br />
Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-font" rel="tag" title="see questions tagged &#39;font&#39;">font</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Apr '15, 18:02</strong></p><img src="https://secure.gravatar.com/avatar/054a5bf4cea5a2e02665f19ce9dde42a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nikita&#39;s gravatar image" /><p><span>nikita</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nikita has no accepted answers">0%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>01 Apr '15, 18:12</strong> </span></p></div></div><div id="comments-container-41115" class="comments-container"></div><div id="comment-tools-41115" class="comment-tools"></div><div class="clear"></div><div id="comment-41115-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

