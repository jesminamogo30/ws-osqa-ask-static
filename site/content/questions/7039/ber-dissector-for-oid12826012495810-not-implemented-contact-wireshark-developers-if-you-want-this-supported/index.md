+++
type = "question"
title = "BER: Dissector for OID:1.2.826.0.1249.58.1.0 not implemented. Contact Wireshark developers if you want this supported"
description = '''How Can i solve it?'''
date = "2011-10-22T05:54:00Z"
lastmod = "2011-10-23T22:27:00Z"
weight = 7039
keywords = [ "not", "implement" ]
aliases = [ "/questions/7039" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [BER: Dissector for OID:1.2.826.0.1249.58.1.0 not implemented. Contact Wireshark developers if you want this supported](/questions/7039/ber-dissector-for-oid12826012495810-not-implemented-contact-wireshark-developers-if-you-want-this-supported)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7039-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7039-score" class="post-score" title="current number of votes">0</div><span id="post-7039-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How Can i solve it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-implement" rel="tag" title="see questions tagged &#39;implement&#39;">implement</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Oct '11, 05:54</strong></p><img src="https://secure.gravatar.com/avatar/54fde54c7e53400f87cc8655edd91498?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JasonOliveira&#39;s gravatar image" /><p><span>JasonOliveira</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JasonOliveira has no accepted answers">0%</span></p></div></div><div id="comments-container-7039" class="comments-container"></div><div id="comment-tools-7039" class="comment-tools"></div><div class="clear"></div><div id="comment-7039-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="7041"></span>

<div id="answer-container-7041" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-7041-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-7041-score" class="post-score" title="current number of votes">0</div><span id="post-7041-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><span>Contact the Wireshark developers</span>, give them whatever information you have about that OID - <a href="http://www.oid-info.com/cgi-bin/display?oid=1.2.826.0.1249.58.1.0&amp;action=display">the oid-info repository doesn't have information about it, and I'm not sure why an Ericsson OID would be under "gb"</a> - and hope that somebody has enough information to implement a dissector for it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Oct '11, 10:35</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-7041" class="comments-container"><span id="7052"></span><div id="comment-7052" class="comment"><div id="post-7052-score" class="comment-score"></div><div class="comment-text"><p>It's probabbly indicating a proprietary/private extension to whatever protocol you find that OID in. Dissecting it would be difficult without the ASN.1 description of that extension.</p></div><div id="comment-7052-info" class="comment-info"><span class="comment-age">(23 Oct '11, 22:27)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-7041" class="comment-tools"></div><div class="clear"></div><div id="comment-7041-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

