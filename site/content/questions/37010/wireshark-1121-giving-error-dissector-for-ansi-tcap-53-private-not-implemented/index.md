+++
type = "question"
title = "Wireshark 1.12.1 giving error &quot;Dissector for ANSI TCAP 53 private not implemented&quot;"
description = '''Trace captured is ANSI MAP/TCAP over SCCP ITU . Wireshark is giving dissector error &quot;dissector for ANSI tcap private 53 not found&quot;. Message is SMS Delivery Point to point. I have checked that ssn&#x27;s in tcap and ansi-tcap are not overlapped.'''
date = "2014-10-13T06:12:00Z"
lastmod = "2014-10-13T06:53:00Z"
weight = 37010
keywords = [ "ansi_map" ]
aliases = [ "/questions/37010" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark 1.12.1 giving error "Dissector for ANSI TCAP 53 private not implemented"](/questions/37010/wireshark-1121-giving-error-dissector-for-ansi-tcap-53-private-not-implemented)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-37010-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-37010-score" class="post-score" title="current number of votes">0</div><span id="post-37010-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Trace captured is ANSI MAP/TCAP over SCCP ITU . Wireshark is giving dissector error "dissector for ANSI tcap private 53 not found". Message is SMS Delivery Point to point. I have checked that ssn's in tcap and ansi-tcap are not overlapped.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ansi_map" rel="tag" title="see questions tagged &#39;ansi_map&#39;">ansi_map</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Oct '14, 06:12</strong></p><img src="https://secure.gravatar.com/avatar/3ae8e2ce3978a98f24cc0bf1324cf8ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Aarya&#39;s gravatar image" /><p><span>Aarya</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Aarya has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Oct '14, 06:32</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-37010" class="comments-container"><span id="37011"></span><div id="comment-37011" class="comment"><div id="post-37011-score" class="comment-score"></div><div class="comment-text"><p>Something with the "family"? We probably need to see the capture to figure out what's going wrong.</p></div><div id="comment-37011-info" class="comment-info"><span class="comment-age">(13 Oct '14, 06:53)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-37010" class="comment-tools"></div><div class="clear"></div><div id="comment-37010-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

