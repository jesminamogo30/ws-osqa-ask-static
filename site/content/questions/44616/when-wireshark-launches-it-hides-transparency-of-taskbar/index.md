+++
type = "question"
title = "When wireshark launches it hides transparency of taskbar"
description = '''Hi, When wireshark launches it disables the transparency of taskbar in windows 7.and when i close wireshark taskbar will be visible transparent.What could be the problem? I am using wireshark v1.12.6.'''
date = "2015-07-29T20:23:00Z"
lastmod = "2015-07-29T20:23:00Z"
weight = 44616
keywords = [ "taskbar", "visibility", "wireshark" ]
aliases = [ "/questions/44616" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [When wireshark launches it hides transparency of taskbar](/questions/44616/when-wireshark-launches-it-hides-transparency-of-taskbar)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44616-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44616-score" class="post-score" title="current number of votes">0</div><span id="post-44616-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, When wireshark launches it disables the transparency of taskbar in windows 7.and when i close wireshark taskbar will be visible transparent.What could be the problem? I am using wireshark v1.12.6.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-taskbar" rel="tag" title="see questions tagged &#39;taskbar&#39;">taskbar</span> <span class="post-tag tag-link-visibility" rel="tag" title="see questions tagged &#39;visibility&#39;">visibility</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jul '15, 20:23</strong></p><img src="https://secure.gravatar.com/avatar/8efce51fbbf3dbd6c9b9132056f45eb5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="ankit&#39;s gravatar image" /><p><span>ankit</span><br />
<span class="score" title="65 reputation points">65</span><span title="23 badges"><span class="badge1">●</span><span class="badgecount">23</span></span><span title="23 badges"><span class="silver">●</span><span class="badgecount">23</span></span><span title="28 badges"><span class="bronze">●</span><span class="badgecount">28</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="ankit has one accepted answer">25%</span></p></div></div><div id="comments-container-44616" class="comments-container"></div><div id="comment-tools-44616" class="comment-tools"></div><div class="clear"></div><div id="comment-44616-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

