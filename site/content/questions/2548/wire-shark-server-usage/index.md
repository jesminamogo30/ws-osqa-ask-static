+++
type = "question"
title = "Wire Shark server usage"
description = '''Hi,  How much Power and Space does a wireshark server require? What are the different interfaces which can be monitored via Wireshark? What are the protocols which can be decoded and monitored?  Regards Sachin Mishra'''
date = "2011-02-24T05:22:00Z"
lastmod = "2011-02-24T05:41:00Z"
weight = 2548
keywords = [ "business" ]
aliases = [ "/questions/2548" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wire Shark server usage](/questions/2548/wire-shark-server-usage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2548-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2548-score" class="post-score" title="current number of votes">0</div><span id="post-2548-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><ol><li>How much Power and Space does a wireshark server require?</li><li>What are the different interfaces which can be monitored via Wireshark?</li><li>What are the protocols which can be decoded and monitored?</li></ol><p>Regards Sachin Mishra</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-business" rel="tag" title="see questions tagged &#39;business&#39;">business</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Feb '11, 05:22</strong></p><img src="https://secure.gravatar.com/avatar/8f6164a5cdb037d29fcbaabf8524e0eb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sachin&#39;s gravatar image" /><p><span>sachin</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sachin has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>24 Feb '11, 05:42</strong> </span></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span></p></div></div><div id="comments-container-2548" class="comments-container"></div><div id="comment-tools-2548" class="comment-tools"></div><div class="clear"></div><div id="comment-2548-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2549"></span>

<div id="answer-container-2549" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2549-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2549-score" class="post-score" title="current number of votes">0</div><span id="post-2549-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><ol><li><p>Wireshark is software, as such it does not take power and space. But of course the machine that you will be running it on will have a power footprint and space requirements. See the website of your hardware supplier for the details.</p></li><li><p>Wireshark uses winpcap (on windows systems) or libpcap (on all other systems) to capture the packets. So any interface that is supported by winpcap/libpcap can be monitored.</p></li><li><p>Wireshark is able to decode 1000+ protocols, see: <a href="http://www.wireshark.org/docs/dfref/">http://www.wireshark.org/docs/dfref/</a></p></li></ol></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Feb '11, 05:41</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-2549" class="comments-container"></div><div id="comment-tools-2549" class="comment-tools"></div><div class="clear"></div><div id="comment-2549-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

