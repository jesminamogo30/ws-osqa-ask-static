+++
type = "question"
title = "Wireshark Network Usage"
description = '''network usage'''
date = "2011-02-20T05:45:00Z"
lastmod = "2011-02-20T14:35:00Z"
weight = 2437
keywords = [ "usage", "network" ]
aliases = [ "/questions/2437" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark Network Usage](/questions/2437/wireshark-network-usage)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2437-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2437-score" class="post-score" title="current number of votes">0</div><span id="post-2437-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>network usage</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-usage" rel="tag" title="see questions tagged &#39;usage&#39;">usage</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Feb '11, 05:45</strong></p><img src="https://secure.gravatar.com/avatar/3663a0847911e3d51dd416506b75cd10?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SiSi&#39;s gravatar image" /><p><span>SiSi</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SiSi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Feb '11, 09:09</strong> </span></p></div></div><div id="comments-container-2437" class="comments-container"></div><div id="comment-tools-2437" class="comment-tools"></div><div class="clear"></div><div id="comment-2437-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2438"></span>

<div id="answer-container-2438" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2438-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2438-score" class="post-score" title="current number of votes">2</div><span id="post-2438-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hmmm... looks like we might need to set a few things straight :-)</p><p>A) Wireshark is not commercial software, it's open source</p><p>B) Wireshark is not monitoring software, it is analysis software. And in order to do so, it can monitor the network for a (short) period of time to capture network traffic</p><p>C) As it is not actively involved in the network, it does not create network traffic (unless you configure it to do reverse DNS lookups). It therefore does not have an influence on network downtime by itself. However, with the right expertise, one might use Wireshark to detect problems in the network <strong>before</strong> they create downtime, making it possible to prevent downtime.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 Feb '11, 06:01</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-2438" class="comments-container"><span id="2441"></span><div id="comment-2441" class="comment"><div id="post-2441-score" class="comment-score"></div><div class="comment-text"><p>lol, amazing you got that much of an answer for that little of a question :-)</p></div><div id="comment-2441-info" class="comment-info"><span class="comment-age">(20 Feb '11, 13:19)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="2442"></span><div id="comment-2442" class="comment"><div id="post-2442-score" class="comment-score"></div><div class="comment-text"><p>hahaha... check the revisions of the question and all will become clear... maybe ;-)</p></div><div id="comment-2442-info" class="comment-info"><span class="comment-age">(20 Feb '11, 14:01)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="2444"></span><div id="comment-2444" class="comment"><div id="post-2444-score" class="comment-score"></div><div class="comment-text"><p>Right... I didn't try that one, I already thought you have some kind of mentalist skills :-)</p></div><div id="comment-2444-info" class="comment-info"><span class="comment-age">(20 Feb '11, 14:35)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-2438" class="comment-tools"></div><div class="clear"></div><div id="comment-2438-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

