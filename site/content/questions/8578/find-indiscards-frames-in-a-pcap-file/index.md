+++
type = "question"
title = "Find InDiscards frames in a pcap file"
description = '''How to find InDiscards frames in a pcap file'''
date = "2012-01-24T04:27:00Z"
lastmod = "2012-01-24T04:27:00Z"
weight = 8578
keywords = [ "indiscards" ]
aliases = [ "/questions/8578" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Find InDiscards frames in a pcap file](/questions/8578/find-indiscards-frames-in-a-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8578-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8578-score" class="post-score" title="current number of votes">0</div><span id="post-8578-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to find InDiscards frames in a pcap file</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-indiscards" rel="tag" title="see questions tagged &#39;indiscards&#39;">indiscards</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '12, 04:27</strong></p><img src="https://secure.gravatar.com/avatar/2edf1b78b9ad2f842b2c28b44b51849c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Raghu&#39;s gravatar image" /><p><span>Raghu</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Raghu has no accepted answers">0%</span></p></div></div><div id="comments-container-8578" class="comments-container"></div><div id="comment-tools-8578" class="comment-tools"></div><div class="clear"></div><div id="comment-8578-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

