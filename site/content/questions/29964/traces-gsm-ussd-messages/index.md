+++
type = "question"
title = "Traces GSM USSD messages"
description = '''Hi All, I work for a GSM operator. I am troubleshooting a USSD issue on my network. I don&#x27;t know which filter i should use to see the messages relative to my USSD request. Please help me.'''
date = "2014-02-18T05:46:00Z"
lastmod = "2014-02-18T14:29:00Z"
weight = 29964
keywords = [ "telecom" ]
aliases = [ "/questions/29964" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Traces GSM USSD messages](/questions/29964/traces-gsm-ussd-messages)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29964-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29964-score" class="post-score" title="current number of votes">0</div><span id="post-29964-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi All, I work for a GSM operator. I am troubleshooting a USSD issue on my network. I don't know which filter i should use to see the messages relative to my USSD request. Please help me.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-telecom" rel="tag" title="see questions tagged &#39;telecom&#39;">telecom</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Feb '14, 05:46</strong></p><img src="https://secure.gravatar.com/avatar/ddeba0cb55f7414027b34d7157b29e1b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="maximengame&#39;s gravatar image" /><p><span>maximengame</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="maximengame has no accepted answers">0%</span></p></div></div><div id="comments-container-29964" class="comments-container"><span id="29982"></span><div id="comment-29982" class="comment"><div id="post-29982-score" class="comment-score"></div><div class="comment-text"><p>If it doesn't contain sensitive data, could you upload an example packet capture file and post the link (<a href="http://cloudshark.org/">http://cloudshark.org/</a> )?</p><p>One common setting to change in order to decode SS7 packets correctly is the MTP3 version (Edit &gt; Preferences &gt; Protocols &gt; MTP3). Set that to ITU, ANSI, China, Japan, etc., accordingly.</p></div><div id="comment-29982-info" class="comment-info"><span class="comment-age">(18 Feb '14, 14:29)</span> <span class="comment-user userinfo">Quadratic</span></div></div></div><div id="comment-tools-29964" class="comment-tools"></div><div class="clear"></div><div id="comment-29964-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

