+++
type = "question"
title = "How to filter voip calls ( trac incoming calls )"
description = '''Hello friends  I have a question about voip calls tracing in wireshark  how i can trac calls in wiershark to i can differ retail voip traffic from wholesale traffic in switch  thank you'''
date = "2015-04-30T06:30:00Z"
lastmod = "2015-04-30T07:29:00Z"
weight = 41969
keywords = [ "wireshark" ]
aliases = [ "/questions/41969" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to filter voip calls ( trac incoming calls )](/questions/41969/how-to-filter-voip-calls-trac-incoming-calls)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41969-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41969-score" class="post-score" title="current number of votes">0</div><span id="post-41969-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello friends I have a question about voip calls tracing in wireshark how i can trac calls in wiershark to i can differ retail voip traffic from wholesale traffic in switch thank you</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Apr '15, 06:30</strong></p><img src="https://secure.gravatar.com/avatar/7a0dbd60873e7324e50751572556f6a6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Abood%20Abu%20Amer&#39;s gravatar image" /><p><span>Abood Abu Amer</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Abood Abu Amer has no accepted answers">0%</span></p></div></div><div id="comments-container-41969" class="comments-container"><span id="41971"></span><div id="comment-41971" class="comment"><div id="post-41971-score" class="comment-score"></div><div class="comment-text"><blockquote><p>can differ <strong>retail</strong> voip traffic from <strong>wholesale</strong> traffic</p></blockquote><p>can you please add more details about that and your environment.</p></div><div id="comment-41971-info" class="comment-info"><span class="comment-age">(30 Apr '15, 07:23)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-41969" class="comment-tools"></div><div class="clear"></div><div id="comment-41969-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="41972"></span>

<div id="answer-container-41972" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-41972-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-41972-score" class="post-score" title="current number of votes">0</div><span id="post-41972-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you look at the packet dissections you'll see that there are packets related to signalling (ie. SIP, SDP, H225, H245, MEGACO, AIX or others), packets that carry media related to these calls (usually in RTP, sometimes others) and packets related to other traffic. Wireshark analysis gives you the option to have it find VoIP calls and show the related signalling, and/or even the media.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>30 Apr '15, 07:29</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-41972" class="comments-container"></div><div id="comment-tools-41972" class="comment-tools"></div><div class="clear"></div><div id="comment-41972-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

