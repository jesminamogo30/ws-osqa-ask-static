+++
type = "question"
title = "GLIBC_2.15&#x27; not found"
description = '''when i start to run wireshark1.8.3 on ubuntu 11.10. I get those error. and the program is stop. 1:wireshark: error while loading shared libraries: libcares.so.2: cannot open shared object file: No such file or directory 2:wireshark: /lib/i386-linux-gnu/libc.so.6: version `GLIBC_2.15&#x27; not found (requ...'''
date = "2013-01-21T23:05:00Z"
lastmod = "2013-02-05T20:29:00Z"
weight = 17851
keywords = [ "glibc_2.15", "ubuntu" ]
aliases = [ "/questions/17851" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [GLIBC\_2.15' not found](/questions/17851/glibc_215-not-found)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17851-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17851-score" class="post-score" title="current number of votes">0</div><span id="post-17851-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when i start to run wireshark1.8.3 on ubuntu 11.10. I get those error. and the program is stop. 1:wireshark: error while loading shared libraries: libcares.so.2: cannot open shared object file: No such file or directory 2:wireshark: /lib/i386-linux-gnu/libc.so.6: version `GLIBC_2.15' not found (required by wireshark)</p><p>could you tell me how to fix this? thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-glibc_2.15" rel="tag" title="see questions tagged &#39;glibc_2.15&#39;">glibc_2.15</span> <span class="post-tag tag-link-ubuntu" rel="tag" title="see questions tagged &#39;ubuntu&#39;">ubuntu</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Jan '13, 23:05</strong></p><img src="https://secure.gravatar.com/avatar/f6eeed42d5aadabfed2ca2cb1faabff1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smilezuzu&#39;s gravatar image" /><p><span>smilezuzu</span><br />
<span class="score" title="20 reputation points">20</span><span title="32 badges"><span class="badge1">●</span><span class="badgecount">32</span></span><span title="32 badges"><span class="silver">●</span><span class="badgecount">32</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smilezuzu has no accepted answers">0%</span></p></div></div><div id="comments-container-17851" class="comments-container"></div><div id="comment-tools-17851" class="comment-tools"></div><div class="clear"></div><div id="comment-17851-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="17857"></span>

<div id="answer-container-17857" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17857-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17857-score" class="post-score" title="current number of votes">0</div><span id="post-17857-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You should install GLIBC_2.15 perhaps?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Jan '13, 06:37</strong></p><img src="https://secure.gravatar.com/avatar/74ba4ba7a26d5efda01b6ae18bbe48e4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ewgenijkkg&#39;s gravatar image" /><p><span>Ewgenijkkg</span><br />
<span class="score" title="66 reputation points">66</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ewgenijkkg has 3 accepted answers">60%</span></p></div></div><div id="comments-container-17857" class="comments-container"><span id="18160"></span><div id="comment-18160" class="comment"><div id="post-18160-score" class="comment-score"></div><div class="comment-text"><p>how can i install GLIBC_2.15? could tell me?</p></div><div id="comment-18160-info" class="comment-info"><span class="comment-age">(31 Jan '13, 02:21)</span> <span class="comment-user userinfo">smilezuzu</span></div></div></div><div id="comment-tools-17857" class="comment-tools"></div><div class="clear"></div><div id="comment-17857-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="18204"></span>

<div id="answer-container-18204" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18204-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18204-score" class="post-score" title="current number of votes">0</div><span id="post-18204-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>How did you install Wireshark?</p><p>The Synaptic Package Manager, as far as I know, uses the Debian <a href="https://en.wikipedia.org/wiki/Advanced_Packaging_Tool">APT</a> mechanism, so installing wireshark with it should cause the right version of GLib etc. to be installed. However, on my Ubuntu 12.04 virtual machine, you can only install Wireshark 1.6.x, not 1.8.x.</p><p>The Ubuntu App Store^W^WSoftware Center also has Wireshark, but it also has only Wireshark 1.6.x, not 1.8.x.</p><p>If you're using somebody else's package repository, it <em>should</em> have been set up to install the appropriate dependencies, including GLib. If it wasn't, it's not a very good package repository.</p><p>If you're running somebody else's non-packaged binary, whoever made that binary will need to make one that works on your Ubuntu machine or will need to tell you how to install the libraries you need.</p><p>If you're running a version you built from source code, you should try rebuilding it on your Ubuntu machine.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>31 Jan '13, 18:34</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>31 Jan '13, 18:34</strong> </span></p></div></div><div id="comments-container-18204" class="comments-container"><span id="18211"></span><div id="comment-18211" class="comment"><div id="post-18211-score" class="comment-score"></div><div class="comment-text"><p>This a my build version wireshark1.8.3. I have added a rlc_AM deci function.</p><p>follow the step: in ubuntu 12.04 i386 1, sudo ./configure 2, sudo make 3, sudo checkinstall</p><p>and then,I get a *.deb.</p><p>When I install it in another ubuntu 12.04 i386</p><p>1,dpkg -i *.deb. 2,wireshark</p><p>error:wireshark: /lib/i386-linux-gnu/libc.so.6: version `GLIBC_2.15' not found (required by wireshark)</p></div><div id="comment-18211-info" class="comment-info"><span class="comment-age">(31 Jan '13, 19:43)</span> <span class="comment-user userinfo">smilezuzu</span></div></div><span id="18214"></span><div id="comment-18214" class="comment"><div id="post-18214-score" class="comment-score"></div><div class="comment-text"><p>So these are both 32-bit machines?</p><p>You <em>REALLY</em> don't want to do <code>sudo ./configure; sudo make</code>. There is <em>NO</em> need to run the configure script, or the build, as root, and you really don't want to give root privileges to anything that doesn't need root privileges. Leave the <code>sudo</code> out of those commands.</p><p>What happens if, after the make, you just do <code>./wireshark</code>, on the machine on which you built it? Does that work?</p></div><div id="comment-18214-info" class="comment-info"><span class="comment-age">(31 Jan '13, 20:35)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="18310"></span><div id="comment-18310" class="comment"><div id="post-18310-score" class="comment-score"></div><div class="comment-text"><p>1,Yes,both of these machines is 32-bit. 2,"leave the sudo" just down the current command to normal authority. not do anything. 3,yes,on the machines which built it, it run correctly.</p></div><div id="comment-18310-info" class="comment-info"><span class="comment-age">(05 Feb '13, 01:50)</span> <span class="comment-user userinfo">smilezuzu</span></div></div><span id="18350"></span><div id="comment-18350" class="comment"><div id="post-18350-score" class="comment-score"></div><div class="comment-text"><blockquote><p>yes,on the machines which built it, it run correctly.</p></blockquote><p>Perhaps the machine on which you built it and the machine on which you're trying to run it have different versions of the C library; if that's the case, then you may not be able to run programs built on the first machine on the second machine. Are they both running the same version of Ubuntu, with both machines having been updated to the same level (with Update Manager)?</p></div><div id="comment-18350-info" class="comment-info"><span class="comment-age">(05 Feb '13, 20:29)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-18204" class="comment-tools"></div><div class="clear"></div><div id="comment-18204-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

