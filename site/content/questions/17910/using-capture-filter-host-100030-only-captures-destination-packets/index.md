+++
type = "question"
title = "Using Capture Filter &quot;host 10.0.0.30&quot; only captures Destination packets"
description = '''I am trying to capture packets to and from host 10.0.0.30. In the Capture Options Dialogue box I entered a Capture Filter with Filter String: host 10.0.0.30 When viewing the captured File I only see packets that were destined for the 10.0.0.30 address. Do I need to do something different to get both...'''
date = "2013-01-23T14:36:00Z"
lastmod = "2013-01-23T17:01:00Z"
weight = 17910
keywords = [ "capture-filter" ]
aliases = [ "/questions/17910" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Using Capture Filter "host 10.0.0.30" only captures Destination packets](/questions/17910/using-capture-filter-host-100030-only-captures-destination-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-17910-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-17910-score" class="post-score" title="current number of votes">0</div><span id="post-17910-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am trying to capture packets to and from host 10.0.0.30. In the Capture Options Dialogue box I entered a Capture Filter with Filter String: host 10.0.0.30 When viewing the captured File I only see packets that were destined for the 10.0.0.30 address. Do I need to do something different to get both directions? FYI, when I look at a capture file with no Capture Options I see packets to and from the 10.0.0.30 address. Unfortunately I am capturing a VERY busy interface and get gig's of data every second, so capturing without a filture is not workable...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jan '13, 14:36</strong></p><img src="https://secure.gravatar.com/avatar/96db258782e6cacce6404f536ec2906e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kporter5302&#39;s gravatar image" /><p><span>kporter5302</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kporter5302 has no accepted answers">0%</span></p></div></div><div id="comments-container-17910" class="comments-container"><span id="17914"></span><div id="comment-17914" class="comment"><div id="post-17914-score" class="comment-score"></div><div class="comment-text"><p>Are you running the program doing the capturing on host 10.0.0.30, or on some other host (passive sniffing)?</p><p>If it's on host 10.0.0.30, what version of what operating system is it running?</p></div><div id="comment-17914-info" class="comment-info"><span class="comment-age">(23 Jan '13, 17:01)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-17910" class="comment-tools"></div><div class="clear"></div><div id="comment-17910-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

