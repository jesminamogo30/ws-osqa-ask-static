+++
type = "question"
title = "IP Stats when root"
description = '''Hello, I just noticed that in the 2.0 GUI IPv4 &amp;amp; IPv6 statistics disappear when I run Wireshark with sudo privileges. Is that something that is intentional, or is it a weird glitch? (Wireshark V 2.3.0rc0-1989-g3952052) 0xB'''
date = "2017-01-08T10:12:00Z"
lastmod = "2017-01-08T15:09:00Z"
weight = 58596
keywords = [ "ip", "statistics", "sudo" ]
aliases = [ "/questions/58596" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [IP Stats when root](/questions/58596/ip-stats-when-root)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58596-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58596-score" class="post-score" title="current number of votes">0</div><span id="post-58596-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I just noticed that in the 2.0 GUI IPv4 &amp; IPv6 statistics disappear when I run Wireshark with sudo privileges. Is that something that is intentional, or is it a weird glitch? (Wireshark V 2.3.0rc0-1989-g3952052)</p><p>0xB</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-sudo" rel="tag" title="see questions tagged &#39;sudo&#39;">sudo</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jan '17, 10:12</strong></p><img src="https://secure.gravatar.com/avatar/b7abadc19faf42f27c2c2feeae249e1f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="0xbismarck&#39;s gravatar image" /><p><span>0xbismarck</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="0xbismarck has one accepted answer">100%</span></p></div></div><div id="comments-container-58596" class="comments-container"><span id="58597"></span><div id="comment-58597" class="comment"><div id="post-58597-score" class="comment-score"></div><div class="comment-text"><p>I don't think it's intentional, but it's a good indication to <strong>NOT</strong> run Wireshark as root.</p><p>Please raise an issue on the <a href="https://bugs.wireshark.org">Wireshark Bugzilla</a>.</p></div><div id="comment-58597-info" class="comment-info"><span class="comment-age">(08 Jan '17, 11:18)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="58603"></span><div id="comment-58603" class="comment"><div id="post-58603-score" class="comment-score"></div><div class="comment-text"><p>Thanks, done. <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=13310">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=13310</a></p></div><div id="comment-58603-info" class="comment-info"><span class="comment-age">(08 Jan '17, 15:09)</span> <span class="comment-user userinfo">0xbismarck</span></div></div></div><div id="comment-tools-58596" class="comment-tools"></div><div class="clear"></div><div id="comment-58596-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

