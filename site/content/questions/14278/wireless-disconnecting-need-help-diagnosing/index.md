+++
type = "question"
title = "Wireless disconnecting - need help diagnosing"
description = '''We have an application that runs over wireless. One component of the application which is web based randomly disconnects. The other parts of the application do not and no other applications experience issues. I sent a Wireshark capture to the vendor but they only say it is on our network. The funny ...'''
date = "2012-09-14T14:36:00Z"
lastmod = "2012-09-17T13:43:00Z"
weight = 14278
keywords = [ "disconnection" ]
aliases = [ "/questions/14278" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless disconnecting - need help diagnosing](/questions/14278/wireless-disconnecting-need-help-diagnosing)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14278-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14278-score" class="post-score" title="current number of votes">0</div><span id="post-14278-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>We have an application that runs over wireless. One component of the application which is web based randomly disconnects. The other parts of the application do not and no other applications experience issues. I sent a Wireshark capture to the vendor but they only say it is on our network. The funny thing is that this is happening at three hospitals that I work at, all with the same application but different types of wireless access points, laptops, switches, etc. I am not experienced enough with WS to say for certain that it is their app. Can one of you experts help me out here? Can I upload the capture and have someone look at it? Thanks in advance for your help.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-disconnection" rel="tag" title="see questions tagged &#39;disconnection&#39;">disconnection</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Sep '12, 14:36</strong></p><img src="https://secure.gravatar.com/avatar/1674308ccb435ff45d8cf5a49c322e5f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="awreaper&#39;s gravatar image" /><p><span>awreaper</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="awreaper has no accepted answers">0%</span></p></div></div><div id="comments-container-14278" class="comments-container"></div><div id="comment-tools-14278" class="comment-tools"></div><div class="clear"></div><div id="comment-14278-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14336"></span>

<div id="answer-container-14336" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14336-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14336-score" class="post-score" title="current number of votes">0</div><span id="post-14336-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>One component of the application which is web based randomly disconnects</p></blockquote><p>Some questions:</p><ul><li>What do you mean exactly by "disconnects"?</li><li>Is it a disconnect from the Wifi network (i doubt that) or a connection problem?</li><li>Are there any error messages?</li><li>If it's a web application, why do occasional connection problems disrupt the whole web application?</li></ul><blockquote><p>Can I upload the capture and have someone look at it?</p></blockquote><p>Well, you can if you want to post your internal data on a public system/server/platform. If you don't care, just choose one of the one-click hoster and upload the file there. Then post the link here. You should choose one, where you can delete the file with a special link, otherwise the data will stay online forever ;-)</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Sep '12, 13:43</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-14336" class="comments-container"></div><div id="comment-tools-14336" class="comment-tools"></div><div class="clear"></div><div id="comment-14336-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

