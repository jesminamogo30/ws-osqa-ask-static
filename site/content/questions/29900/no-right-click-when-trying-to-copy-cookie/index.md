+++
type = "question"
title = "No Right Click When Trying to Copy Cookie"
description = '''I can&#x27;t copy a cookie when I am trying to. No contextual menu pops up. I am using a mac and X11. I am clicking command + click to right click....anything I can do? '''
date = "2014-02-15T21:53:00Z"
lastmod = "2014-02-15T21:53:00Z"
weight = 29900
keywords = [ "asap" ]
aliases = [ "/questions/29900" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [No Right Click When Trying to Copy Cookie](/questions/29900/no-right-click-when-trying-to-copy-cookie)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29900-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29900-score" class="post-score" title="current number of votes">0</div><span id="post-29900-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I can't copy a cookie when I am trying to. No contextual menu pops up. I am using a mac and X11. I am clicking command + click to right click....anything I can do?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-asap" rel="tag" title="see questions tagged &#39;asap&#39;">asap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Feb '14, 21:53</strong></p><img src="https://secure.gravatar.com/avatar/02d8509222e19e680b5bc5616718449e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="QwertyHacker&#39;s gravatar image" /><p><span>QwertyHacker</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="QwertyHacker has no accepted answers">0%</span></p></div></div><div id="comments-container-29900" class="comments-container"></div><div id="comment-tools-29900" class="comment-tools"></div><div class="clear"></div><div id="comment-29900-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

