+++
type = "question"
title = "Can&#x27;t find: peflag in wireshark 1.6.5"
description = '''I tried to make build for 64-bit Windows 7 with wireshark 1.6.5. The following command: nmake -f Makefile.nmake verify_tools gave me an error: can&#x27;t find: peflags The ERROR: These application(s) are either not installed or simply can&#x27;t be found in the current PATH: How to resolve it?'''
date = "2012-01-13T00:43:00Z"
lastmod = "2012-01-13T06:40:00Z"
weight = 8366
keywords = [ "1.6.5" ]
aliases = [ "/questions/8366" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Can't find: peflag in wireshark 1.6.5](/questions/8366/cant-find-peflag-in-wireshark-165)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8366-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8366-score" class="post-score" title="current number of votes">0</div><span id="post-8366-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I tried to make build for 64-bit Windows 7 with wireshark 1.6.5. The following command: nmake -f Makefile.nmake verify_tools gave me an error: can't find: peflags The ERROR: These application(s) are either not installed or simply can't be found in the current PATH: How to resolve it?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-1.6.5" rel="tag" title="see questions tagged &#39;1.6.5&#39;">1.6.5</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jan '12, 00:43</strong></p><img src="https://secure.gravatar.com/avatar/b7bdcb1b20e2c4bba13948b04439d544?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="vish&#39;s gravatar image" /><p><span>vish</span><br />
<span class="score" title="0 reputation points">0</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="vish has no accepted answers">0%</span></p></div></div><div id="comments-container-8366" class="comments-container"></div><div id="comment-tools-8366" class="comment-tools"></div><div class="clear"></div><div id="comment-8366-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8369"></span>

<div id="answer-container-8369" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8369-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8369-score" class="post-score" title="current number of votes">1</div><span id="post-8369-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Jaap has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Try running your Cygwin <a href="http://cygwin.com/setup.exe">setup.exe</a> and installing the latest <a href="http://cygwin.com/packages/rebase/">rebase</a> package, currently <a href="http://cygwin.com/packages/rebase/rebase-4.0.1-1">4.0.1-1</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>13 Jan '12, 06:40</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-8369" class="comments-container"></div><div id="comment-tools-8369" class="comment-tools"></div><div class="clear"></div><div id="comment-8369-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

