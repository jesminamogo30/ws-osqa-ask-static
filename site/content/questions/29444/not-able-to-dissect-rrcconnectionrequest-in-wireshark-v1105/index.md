+++
type = "question"
title = "Not able to dissect rrcConnectionRequest in wireshark v1.10.5"
description = '''Hi, I am not able to dissect rrcConnectionRequest/Setup in wireshark version 1.10.5 while I was able to dissect rrcConnectionSetupcomplete and other RRC messages. can anybody help me out or suggest that any settings need to correct or the dissector itself is buggy? there is an dissecting problem in ...'''
date = "2014-02-04T23:15:00Z"
lastmod = "2014-02-05T14:09:00Z"
weight = 29444
keywords = [ "rrc.ul.ccch" ]
aliases = [ "/questions/29444" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Not able to dissect rrcConnectionRequest in wireshark v1.10.5](/questions/29444/not-able-to-dissect-rrcconnectionrequest-in-wireshark-v1105)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29444-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29444-score" class="post-score" title="current number of votes">0</div><span id="post-29444-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I am not able to dissect rrcConnectionRequest/Setup in wireshark version 1.10.5 while I was able to dissect rrcConnectionSetupcomplete and other RRC messages. can anybody help me out or suggest that any settings need to correct or the dissector itself is buggy? there is an dissecting problem in all rrc.ul.ccch and rrc.dl.ccch messages.</p><p>I can provide the pcap file as well if any body interested.</p><p>Thanks &amp; Regards- Ashutosh Gupta</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-rrc.ul.ccch" rel="tag" title="see questions tagged &#39;rrc.ul.ccch&#39;">rrc.ul.ccch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Feb '14, 23:15</strong></p><img src="https://secure.gravatar.com/avatar/8899f7065620a5d13d6acc19fc5acd88?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ashutosh%20Gupta&#39;s gravatar image" /><p><span>Ashutosh Gupta</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ashutosh Gupta has no accepted answers">0%</span></p></div></div><div id="comments-container-29444" class="comments-container"><span id="29445"></span><div id="comment-29445" class="comment"><div id="post-29445-score" class="comment-score"></div><div class="comment-text"><p>Please post the capture file somwhere (Google drive, dropbox, cloudshark.org or mega.co.NZ)</p></div><div id="comment-29445-info" class="comment-info"><span class="comment-age">(04 Feb '14, 23:30)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29444" class="comment-tools"></div><div class="clear"></div><div id="comment-29444-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="29475"></span>

<div id="answer-container-29475" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29475-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29475-score" class="post-score" title="current number of votes">0</div><span id="post-29475-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See my reply to your post on -dev mailing list: <a href="http://www.wireshark.org/lists/wireshark-dev/201402/msg00026.html">http://www.wireshark.org/lists/wireshark-dev/201402/msg00026.html</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Feb '14, 14:09</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-29475" class="comments-container"></div><div id="comment-tools-29475" class="comment-tools"></div><div class="clear"></div><div id="comment-29475-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

