+++
type = "question"
title = "MSS modified by comcast throttling upload?"
description = '''This last month upload speed has often been half speed. I see uploads with a length of 5-600, instead of normal. I don&#x27;t see MSS being set that small by the server in TCP options - 1380 is normal. Download tests show full speed at all times. Any guidance on trying to prove Comcast is messing with th...'''
date = "2017-05-14T12:17:00Z"
lastmod = "2017-05-14T12:30:00Z"
weight = 61391
keywords = [ "segment", "size", "lenth", "packet", "mss" ]
aliases = [ "/questions/61391" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [MSS modified by comcast throttling upload?](/questions/61391/mss-modified-by-comcast-throttling-upload)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61391-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61391-score" class="post-score" title="current number of votes">0</div><span id="post-61391-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This last month upload speed has often been half speed. I see uploads with a length of 5-600, instead of normal. I don't see MSS being set that small by the server in TCP options - 1380 is normal. Download tests show full speed at all times. Any guidance on trying to prove Comcast is messing with the MSS? Sure looks like that. Thanks much in advance. <img src="https://osqa-ask.wireshark.org/upfiles/Screen_Shot_2017-05-14_at_11.38.07_AM.jpg" alt="alt text" /></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-segment" rel="tag" title="see questions tagged &#39;segment&#39;">segment</span> <span class="post-tag tag-link-size" rel="tag" title="see questions tagged &#39;size&#39;">size</span> <span class="post-tag tag-link-lenth" rel="tag" title="see questions tagged &#39;lenth&#39;">lenth</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span> <span class="post-tag tag-link-mss" rel="tag" title="see questions tagged &#39;mss&#39;">mss</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 May '17, 12:17</strong></p><img src="https://secure.gravatar.com/avatar/52ff5d6b59bd5798a667a6f346a52421?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packetlevel&#39;s gravatar image" /><p><span>packetlevel</span><br />
<span class="score" title="1 reputation points">1</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packetlevel has no accepted answers">0%</span></p></img></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 May '17, 12:18</strong> </span></p></div></div><div id="comments-container-61391" class="comments-container"><span id="61392"></span><div id="comment-61392" class="comment"><div id="post-61392-score" class="comment-score"></div><div class="comment-text"><p>Can share us a trace?</p></div><div id="comment-61392-info" class="comment-info"><span class="comment-age">(14 May '17, 12:30)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-61391" class="comment-tools"></div><div class="clear"></div><div id="comment-61391-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

