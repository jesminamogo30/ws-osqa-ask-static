+++
type = "question"
title = "Modifying Packet Header"
description = '''Hi, I am working on a project for a professor to collect smb packet headers on university network. Due to security reasons uid, mid, file path, etc. needs to be one-way encrypted.  I was wondering if anyone can help me figure out how i can modify these header fields in the dissected tree directly th...'''
date = "2011-03-25T04:55:00Z"
lastmod = "2011-03-27T11:07:00Z"
weight = 3114
keywords = [ "packets", "university", "modify", "smb" ]
aliases = [ "/questions/3114" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Modifying Packet Header](/questions/3114/modifying-packet-header)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3114-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3114-score" class="post-score" title="current number of votes">0</div><span id="post-3114-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am working on a project for a professor to collect smb packet headers on university network. Due to security reasons uid, mid, file path, etc. needs to be one-way encrypted.</p><p>I was wondering if anyone can help me figure out how i can modify these header fields in the dissected tree directly that is returned by libwireshark (edt-&gt;tree) before i can save this header to a pcap or a simple xml file with header in hex format.</p><p>Thank You</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-packets" rel="tag" title="see questions tagged &#39;packets&#39;">packets</span> <span class="post-tag tag-link-university" rel="tag" title="see questions tagged &#39;university&#39;">university</span> <span class="post-tag tag-link-modify" rel="tag" title="see questions tagged &#39;modify&#39;">modify</span> <span class="post-tag tag-link-smb" rel="tag" title="see questions tagged &#39;smb&#39;">smb</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '11, 04:55</strong></p><img src="https://secure.gravatar.com/avatar/13c36288e917b3f65ff098cd5004c6fd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hetul317&#39;s gravatar image" /><p><span>hetul317</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hetul317 has no accepted answers">0%</span></p></div></div><div id="comments-container-3114" class="comments-container"></div><div id="comment-tools-3114" class="comment-tools"></div><div class="clear"></div><div id="comment-3114-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3155"></span>

<div id="answer-container-3155" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3155-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3155-score" class="post-score" title="current number of votes">0</div><span id="post-3155-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You cannot change data with Wireshark. You should add a program that modifies the files after you have saved them.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Mar '11, 11:07</strong></p><img src="https://secure.gravatar.com/avatar/585595b6a24df9b742ebc186788e9a8e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="harper&#39;s gravatar image" /><p><span>harper</span><br />
<span class="score" title="31 reputation points">31</span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="harper has no accepted answers">0%</span></p></div></div><div id="comments-container-3155" class="comments-container"></div><div id="comment-tools-3155" class="comment-tools"></div><div class="clear"></div><div id="comment-3155-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

