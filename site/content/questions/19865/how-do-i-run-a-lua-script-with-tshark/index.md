+++
type = "question"
title = "How do I run a Lua script with tshark?"
description = '''I get an error when I try to run a Lua script with: tshark -q -z io,phs -Xlua_script:trace_stats.lua -r &amp;lt;trace.pcap&amp;gt;  What is wrong with this command?'''
date = "2013-03-27T05:32:00Z"
lastmod = "2013-05-05T07:59:00Z"
weight = 19865
keywords = [ "lua", "wireshark" ]
aliases = [ "/questions/19865" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How do I run a Lua script with tshark?](/questions/19865/how-do-i-run-a-lua-script-with-tshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19865-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19865-score" class="post-score" title="current number of votes">0</div><span id="post-19865-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I get an error when I try to run a Lua script with:</p><pre><code>tshark -q -z io,phs -Xlua_script:trace_stats.lua -r &lt;trace.pcap&gt;</code></pre><p>What is wrong with this command?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Mar '13, 05:32</strong></p><img src="https://secure.gravatar.com/avatar/583b809745fa45690fa8b950c5d28714?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Ashraf&#39;s gravatar image" /><p><span>Ashraf</span><br />
<span class="score" title="16 reputation points">16</span><span title="6 badges"><span class="badge1">●</span><span class="badgecount">6</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="10 badges"><span class="bronze">●</span><span class="badgecount">10</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Ashraf has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>27 Mar '13, 15:41</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-19865" class="comments-container"><span id="19867"></span><div id="comment-19867" class="comment"><div id="post-19867-score" class="comment-score"></div><div class="comment-text"><p>What is the error message?</p></div><div id="comment-19867-info" class="comment-info"><span class="comment-age">(27 Mar '13, 06:35)</span> <span class="comment-user userinfo">helloworld</span></div></div><span id="19883"></span><div id="comment-19883" class="comment"><div id="post-19883-score" class="comment-score"></div><div class="comment-text"><p>Works fine for me.<br />
Can you do a "tshark -v" and post the output here? Also post the exact error it printed out please.</p></div><div id="comment-19883-info" class="comment-info"><span class="comment-age">(27 Mar '13, 12:21)</span> <span class="comment-user userinfo">Hadriel</span></div></div><span id="20958"></span><div id="comment-20958" class="comment"><div id="post-20958-score" class="comment-score"></div><div class="comment-text"><p>thanks for helping me helloworld and Hardiel</p></div><div id="comment-20958-info" class="comment-info"><span class="comment-age">(05 May '13, 07:59)</span> <span class="comment-user userinfo">Ashraf</span></div></div></div><div id="comment-tools-19865" class="comment-tools"></div><div class="clear"></div><div id="comment-19865-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

