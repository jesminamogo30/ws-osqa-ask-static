+++
type = "question"
title = "problem with H225. Bad asn1 file ?"
description = '''Hello  I try to send a message coded in H225 in asn1 (my message is generated with the Binary Note Encoder librairie).  I used the ans1 file h2250v7.asn link to download it Wireshark don&#x27;t understand my message and I got malformed packet error.  Which version is used by Wireshark to decode h225 mess...'''
date = "2011-09-01T09:48:00Z"
lastmod = "2011-09-02T07:20:00Z"
weight = 6048
keywords = [ "h225", "h323", "asn1" ]
aliases = [ "/questions/6048" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [problem with H225. Bad asn1 file ?](/questions/6048/problem-with-h225-bad-asn1-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6048-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6048-score" class="post-score" title="current number of votes">0</div><span id="post-6048-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello</p><p>I try to send a message coded in H225 in asn1 (my message is generated with the Binary Note Encoder librairie). I used the ans1 file h2250v7.asn <a href="http://www.packetizer.com/ipmc/h323/standards.html">link to download it</a></p><p>Wireshark don't understand my message and I got malformed packet error.</p><p>Which version is used by Wireshark to decode h225 message ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-h225" rel="tag" title="see questions tagged &#39;h225&#39;">h225</span> <span class="post-tag tag-link-h323" rel="tag" title="see questions tagged &#39;h323&#39;">h323</span> <span class="post-tag tag-link-asn1" rel="tag" title="see questions tagged &#39;asn1&#39;">asn1</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Sep '11, 09:48</strong></p><img src="https://secure.gravatar.com/avatar/3cd9b535e533e22d4c8598c56b8a9e71?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guillaume%20Ansquer&#39;s gravatar image" /><p><span>Guillaume An...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guillaume Ansquer has no accepted answers">0%</span></p></div></div><div id="comments-container-6048" class="comments-container"></div><div id="comment-tools-6048" class="comment-tools"></div><div class="clear"></div><div id="comment-6048-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="6054"></span>

<div id="answer-container-6054" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6054-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6054-score" class="post-score" title="current number of votes">0</div><span id="post-6054-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Hi, The verios is -- Taken from ITU ASN.1 database -- http://www.itu.int/ITU-T/formal-language/itu-t/h/h225-0/2009/H323-MESSAGES.asn Did you use PER encoding?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>01 Sep '11, 22:28</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-6054" class="comments-container"></div><div id="comment-tools-6054" class="comment-tools"></div><div class="clear"></div><div id="comment-6054-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="6056"></span>

<div id="answer-container-6056" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6056-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6056-score" class="post-score" title="current number of votes">0</div><span id="post-6056-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Thanks for this document.</p><p>It's looks similar than the one I use. I use PER encoding.</p><p>Maybe my problem is the "...," in the asn1 file. My compiler for generating java source don't want them so I suppress them. What is the use of them in asn1 ?</p><p>(exemple:</p><p>H323-UU-PDU ::= SEQUENCE { h323-message-body CHOICE {setup Setup-UUIE,<br />
callProceeding CallProceeding-UUIE,<br />
connect Connect-UUIE, alerting Alerting-UUIE,<br />
information Information-UUIE,<br />
releaseComplete ReleaseComplete-UUIE, facility Facility-UUIE,<br />
...,<br />
progress Progress-UUIE,<br />
empty NULL, -- used when a Facility message is sent,--</p></div><div class="answer-controls post-controls"><div class="community-wiki">This answer is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Sep '11, 01:21</strong></p><img src="https://secure.gravatar.com/avatar/3cd9b535e533e22d4c8598c56b8a9e71?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guillaume%20Ansquer&#39;s gravatar image" /><p><span>Guillaume An...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guillaume Ansquer has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-6056" class="comments-container"><span id="6062"></span><div id="comment-6062" class="comment"><div id="post-6062-score" class="comment-score"></div><div class="comment-text"><p>That may very well be your problem, you may want to check with X.690 but without looking at the spec, "...," means that the SEQUENCE may be extended, if that is the case there is an extension bit set and other encoding details.</p></div><div id="comment-6062-info" class="comment-info"><span class="comment-age">(02 Sep '11, 07:20)</span> <span class="comment-user userinfo">Anders ♦</span></div></div></div><div id="comment-tools-6056" class="comment-tools"></div><div class="clear"></div><div id="comment-6056-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

