+++
type = "question"
title = "Copy-&gt;Bytes not working"
description = '''Copy -&amp;gt; Bytes menu not working. Its not even there. Tried 1.99.1 on OSX and Linux and git-master on Linux. http://i60.tinypic.com/wvvd77.png'''
date = "2015-01-04T15:07:00Z"
lastmod = "2015-01-05T03:45:00Z"
weight = 38883
keywords = [ "copy" ]
aliases = [ "/questions/38883" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Copy-&gt;Bytes not working](/questions/38883/copy-bytes-not-working)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38883-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38883-score" class="post-score" title="current number of votes">0</div><span id="post-38883-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Copy -&gt; Bytes menu not working. Its not even there. Tried 1.99.1 on OSX and Linux and git-master on Linux.</p><p><img></img><a href="http://i60.tinypic.com/wvvd77.png">http://i60.tinypic.com/wvvd77.png</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-copy" rel="tag" title="see questions tagged &#39;copy&#39;">copy</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Jan '15, 15:07</strong></p><img src="https://secure.gravatar.com/avatar/9a8a9d10495c0449cb5db2104f90ea41?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="updatelee&#39;s gravatar image" /><p><span>updatelee</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="updatelee has no accepted answers">0%</span></p></img></div></div><div id="comments-container-38883" class="comments-container"></div><div id="comment-tools-38883" class="comment-tools"></div><div class="clear"></div><div id="comment-38883-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="38884"></span>

<div id="answer-container-38884" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38884-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38884-score" class="post-score" title="current number of votes">0</div><span id="post-38884-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>This looks like a bug. Please check the <a href="http://bugs.wireshark.org">bugtracker</a> to see if it has been reported already, otherwise report a new bug.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Jan '15, 16:20</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-38884" class="comments-container"><span id="38888"></span><div id="comment-38888" class="comment"><div id="post-38888-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jasper, bug added as per your reply.</p><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10831">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10831</a></p><p>Hopefully this gets fixed, its quite frustrating.</p></div><div id="comment-38888-info" class="comment-info"><span class="comment-age">(04 Jan '15, 17:20)</span> <span class="comment-user userinfo">updatelee</span></div></div></div><div id="comment-tools-38884" class="comment-tools"></div><div class="clear"></div><div id="comment-38884-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="38890"></span>

<div id="answer-container-38890" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38890-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38890-score" class="post-score" title="current number of votes">0</div><span id="post-38890-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>As per the response on the bug, it's one of the items that hasn't yet been implemented in the qt version. You may be able to use the gtk version to get the required functionality.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Jan '15, 03:45</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-38890" class="comments-container"></div><div id="comment-tools-38890" class="comment-tools"></div><div class="clear"></div><div id="comment-38890-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

