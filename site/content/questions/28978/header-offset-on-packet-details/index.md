+++
type = "question"
title = "Header Offset on packet details"
description = '''I&#x27;m a visual learner. Is there a way to have it show the header offsets in the packet details? See example shot where I added the first 4 offsets of the IP header. http://imgur.com/aUNHeIL'''
date = "2014-01-16T20:25:00Z"
lastmod = "2014-01-16T22:45:00Z"
weight = 28978
keywords = [ "header", "offset" ]
aliases = [ "/questions/28978" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Header Offset on packet details](/questions/28978/header-offset-on-packet-details)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28978-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28978-score" class="post-score" title="current number of votes">0</div><span id="post-28978-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm a visual learner. Is there a way to have it show the header offsets in the packet details? See example shot where I added the first 4 offsets of the IP header.</p><p><a href="http://imgur.com/aUNHeIL">http://imgur.com/aUNHeIL</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-header" rel="tag" title="see questions tagged &#39;header&#39;">header</span> <span class="post-tag tag-link-offset" rel="tag" title="see questions tagged &#39;offset&#39;">offset</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jan '14, 20:25</strong></p><img src="https://secure.gravatar.com/avatar/fb68f9aabd3aa8978f0d8bcf96fff4bd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="BeauGalbraith&#39;s gravatar image" /><p><span>BeauGalbraith</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="BeauGalbraith has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Jan '14, 20:25</strong> </span></p></div></div><div id="comments-container-28978" class="comments-container"></div><div id="comment-tools-28978" class="comment-tools"></div><div class="clear"></div><div id="comment-28978-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="28985"></span>

<div id="answer-container-28985" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28985-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28985-score" class="post-score" title="current number of votes">0</div><span id="post-28985-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The only way to do that is to modify Wireshark's source code so that it does so. If you think it would be a good idea, file an enhancement request on <a href="http://bugs.wireshark.org/">the Wireshark Bugzilla</a> (and add the image to the bug as an attachment).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Jan '14, 22:45</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-28985" class="comments-container"></div><div id="comment-tools-28985" class="comment-tools"></div><div class="clear"></div><div id="comment-28985-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

