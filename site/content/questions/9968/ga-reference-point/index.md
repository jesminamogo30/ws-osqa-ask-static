+++
type = "question"
title = "Ga Reference Point"
description = '''Is Ga reference point (3GPP 32.298) decoding supported by Wireshark? '''
date = "2012-04-05T11:15:00Z"
lastmod = "2012-04-05T13:09:00Z"
weight = 9968
keywords = [ "cdrs", "ga" ]
aliases = [ "/questions/9968" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Ga Reference Point](/questions/9968/ga-reference-point)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9968-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9968-score" class="post-score" title="current number of votes">0</div><span id="post-9968-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is Ga reference point (3GPP 32.298) decoding supported by Wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-cdrs" rel="tag" title="see questions tagged &#39;cdrs&#39;">cdrs</span> <span class="post-tag tag-link-ga" rel="tag" title="see questions tagged &#39;ga&#39;">ga</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Apr '12, 11:15</strong></p><img src="https://secure.gravatar.com/avatar/6f6b7c8c03b3343af09fc201341fb354?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="emarina&#39;s gravatar image" /><p><span>emarina</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="emarina has no accepted answers">0%</span></p></div></div><div id="comments-container-9968" class="comments-container"></div><div id="comment-tools-9968" class="comment-tools"></div><div class="clear"></div><div id="comment-9968-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9971"></span>

<div id="answer-container-9971" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9971-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9971-score" class="post-score" title="current number of votes">0</div><span id="post-9971-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://ask.wireshark.org/questions/9156/ga-reference-point">The last time you asked this question</a>, somebody answered it. Please refer back to that answer and, if you have further questions, ask them in comments on that answer.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Apr '12, 13:09</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-9971" class="comments-container"></div><div id="comment-tools-9971" class="comment-tools"></div><div class="clear"></div><div id="comment-9971-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

