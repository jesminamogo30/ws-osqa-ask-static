+++
type = "question"
title = "[TCP Previous segment not captured] unknown 0x6d6"
description = '''Hello, we are a Company with 7 stores. In one store we cannot connect to the Web Pages from a NAS and some printers. When i go to the IP segment in this store i can connect to the NAS and printers. We are connect with MPLS. When i let Wireshark sniff the connection then these Errors come: [TCP Previ...'''
date = "2014-12-04T07:25:00Z"
lastmod = "2014-12-04T07:35:00Z"
weight = 38329
keywords = [ "wireshake" ]
aliases = [ "/questions/38329" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[TCP Previous segment not captured\] unknown 0x6d6](/questions/38329/tcp-previous-segment-not-captured-unknown-0x6d6)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38329-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38329-score" class="post-score" title="current number of votes">0</div><span id="post-38329-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>we are a Company with 7 stores. In one store we cannot connect to the Web Pages from a NAS and some printers. When i go to the IP segment in this store i can connect to the NAS and printers. We are connect with MPLS.</p><p>When i let Wireshark sniff the connection then these Errors come:</p><p>[TCP Previous segment not captured] unknown 0xd5 [Malformed Packet] and then there are a lot of Duplicate ACK and the Webpage load and load and load</p><p>What could this be? Are there a problem with the MPLS connection to this store?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshake" rel="tag" title="see questions tagged &#39;wireshake&#39;">wireshake</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Dec '14, 07:25</strong></p><img src="https://secure.gravatar.com/avatar/9bc779e0b42318d26e8573c7e65a9771?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MarcoR&#39;s gravatar image" /><p><span>MarcoR</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MarcoR has no accepted answers">0%</span></p></div></div><div id="comments-container-38329" class="comments-container"><span id="38330"></span><div id="comment-38330" class="comment"><div id="post-38330-score" class="comment-score"></div><div class="comment-text"><p>Here a picture:</p><p><img src="https://osqa-ask.wireshark.org/upfiles/WIreshark.JPG" alt="alt text" /></p></div><div id="comment-38330-info" class="comment-info"><span class="comment-age">(04 Dec '14, 07:35)</span> <span class="comment-user userinfo">MarcoR</span></div></div></div><div id="comment-tools-38329" class="comment-tools"></div><div class="clear"></div><div id="comment-38329-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

