+++
type = "question"
title = "s6a interace Supported Features AVP (628) Feature-List(630) is decoded wrongly"
description = '''s6a interace Supported-Features AVP (628) Feature-List(630) is decoded wrongly. Always decodes the Cx interface feature-list flags ... '''
date = "2013-10-07T06:58:00Z"
lastmod = "2013-10-07T10:57:00Z"
weight = 25706
keywords = [ "diameter", "3gpp", "s6a" ]
aliases = [ "/questions/25706" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [s6a interace Supported Features AVP (628) Feature-List(630) is decoded wrongly](/questions/25706/s6a-interace-supported-features-avp-628-feature-list630-is-decoded-wrongly)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25706-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25706-score" class="post-score" title="current number of votes">0</div><span id="post-25706-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>s6a interace Supported-Features AVP (628) Feature-List(630) is decoded wrongly. Always decodes the Cx interface feature-list flags ...</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-diameter" rel="tag" title="see questions tagged &#39;diameter&#39;">diameter</span> <span class="post-tag tag-link-3gpp" rel="tag" title="see questions tagged &#39;3gpp&#39;">3gpp</span> <span class="post-tag tag-link-s6a" rel="tag" title="see questions tagged &#39;s6a&#39;">s6a</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Oct '13, 06:58</strong></p><img src="https://secure.gravatar.com/avatar/aed38d9a729a118d8084f46708fe3810?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MANDALIC&#39;s gravatar image" /><p><span>MANDALIC</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MANDALIC has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>07 Oct '13, 10:40</strong> </span></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span></p></div></div><div id="comments-container-25706" class="comments-container"></div><div id="comment-tools-25706" class="comment-tools"></div><div class="clear"></div><div id="comment-25706-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25723"></span>

<div id="answer-container-25723" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25723-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25723-score" class="post-score" title="current number of votes">0</div><span id="post-25723-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The decoding of the S6 Feature-List was added in <a href="http://anonsvn.wireshark.org/viewvc/viewvc.cgi?view=rev&amp;revision=50796">revision 50796</a>. If you want to see the proper decode you'll need to use an <a href="http://www.wireshark.org/download/automated/">automated development build</a> at least until the next major release (1.12.0 or whatever the next stable version is called).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Oct '13, 10:57</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span></p></div></div><div id="comments-container-25723" class="comments-container"></div><div id="comment-tools-25723" class="comment-tools"></div><div class="clear"></div><div id="comment-25723-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

