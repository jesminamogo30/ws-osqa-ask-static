+++
type = "question"
title = "Home network download analysis"
description = '''Hi, I have cable/router at home and then about 7 computers (wired &amp;amp; Wireless) .. also a PS3 and smart TV. I want to track the usage of each of these units in terms of download and unload. Basically, sometimes, when looking at my daily usage from my provider, I see huge traffic and I want to know...'''
date = "2012-01-24T17:09:00Z"
lastmod = "2012-01-25T13:44:00Z"
weight = 8594
keywords = [ "download" ]
aliases = [ "/questions/8594" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Home network download analysis](/questions/8594/home-network-download-analysis)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8594-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8594-score" class="post-score" title="current number of votes">0</div><span id="post-8594-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I have cable/router at home and then about 7 computers (wired &amp; Wireless) .. also a PS3 and smart TV. I want to track the usage of each of these units in terms of download and unload. Basically, sometimes, when looking at my daily usage from my provider, I see huge traffic and I want to know which computer/unit/IP is doing this. If I install WireShark on my computer which is wired to the cable/router.. will I able do that? If so, any directions to easily do this.</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-download" rel="tag" title="see questions tagged &#39;download&#39;">download</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '12, 17:09</strong></p><img src="https://secure.gravatar.com/avatar/03e936637b80834ac6a885354260a39b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="MeMe&#39;s gravatar image" /><p><span>MeMe</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="MeMe has no accepted answers">0%</span></p></div></div><div id="comments-container-8594" class="comments-container"></div><div id="comment-tools-8594" class="comment-tools"></div><div class="clear"></div><div id="comment-8594-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="8595"></span>

<div id="answer-container-8595" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8595-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8595-score" class="post-score" title="current number of votes">0</div><span id="post-8595-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No, it probably won't, because the devices wired to it will send their network packets directly to the internet service provider - without routing them to your computer first. That means that you will not see their communication, unless you find a way to tell the router to let you "listen in" on what the others do. That kind of feature is called a SPAN or monitor session, which needs to be configured first. Some routers can also write packet dumps locally that you can download afterwards. You should try to find out what kind of diagnostic options your router offers.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jan '12, 23:39</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-8595" class="comments-container"><span id="8611"></span><div id="comment-8611" class="comment"><div id="post-8611-score" class="comment-score"></div><div class="comment-text"><p>ok .. thanks for the reply</p></div><div id="comment-8611-info" class="comment-info"><span class="comment-age">(25 Jan '12, 13:44)</span> <span class="comment-user userinfo">MeMe</span></div></div></div><div id="comment-tools-8595" class="comment-tools"></div><div class="clear"></div><div id="comment-8595-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

