+++
type = "question"
title = "does wireshark player directly play g722 codec RTP trace"
description = '''does wireshark player directly play g722 codec RTP trace'''
date = "2015-01-23T01:29:00Z"
lastmod = "2016-11-24T09:44:00Z"
weight = 39361
keywords = [ "player", "g722" ]
aliases = [ "/questions/39361" ]
osqa_answers = 3
osqa_accepted = false
+++

<div class="headNormal">

# [does wireshark player directly play g722 codec RTP trace](/questions/39361/does-wireshark-player-directly-play-g722-codec-rtp-trace)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39361-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39361-score" class="post-score" title="current number of votes">1</div><span id="post-39361-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>does wireshark player directly play g722 codec RTP trace</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-player" rel="tag" title="see questions tagged &#39;player&#39;">player</span> <span class="post-tag tag-link-g722" rel="tag" title="see questions tagged &#39;g722&#39;">g722</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jan '15, 01:29</strong></p><img src="https://secure.gravatar.com/avatar/51c991e7458415743e7f936fa36013b5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="saq&#39;s gravatar image" /><p><span>saq</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="saq has no accepted answers">0%</span></p></div></div><div id="comments-container-39361" class="comments-container"></div><div id="comment-tools-39361" class="comment-tools"></div><div class="clear"></div><div id="comment-39361-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

3 Answers:

</div>

</div>

<span id="39363"></span>

<div id="answer-container-39363" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39363-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39363-score" class="post-score" title="current number of votes">0</div><span id="post-39363-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No only G711 is supported as other codecs requires licenses.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jan '15, 04:02</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-39363" class="comments-container"></div><div id="comment-tools-39363" class="comment-tools"></div><div class="clear"></div><div id="comment-39363-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="39365"></span>

<div id="answer-container-39365" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39365-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39365-score" class="post-score" title="current number of votes">0</div><span id="post-39365-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Currently this is only possible if the <a href="http://www.soft-switch.org/installing-spandsp.html">SpanDSP</a> has been compiled in.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jan '15, 04:19</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-39365" class="comments-container"><span id="39449"></span><div id="comment-39449" class="comment"><div id="post-39449-score" class="comment-score"></div><div class="comment-text"><p>Hi Jaap, I have been trying to play g722 codec rtp stream for ever!!I even installed spandsp library and compiled it with config options but it doesnt work!!So i finally gaveup... Please let me know if i have to do anything specific even after installing spandsp because i spent a considerable amount of time on it and would love to see g722 playing in wireshark!!</p></div><div id="comment-39449-info" class="comment-info"><span class="comment-age">(27 Jan '15, 23:38)</span> <span class="comment-user userinfo">koundi</span></div></div><span id="39461"></span><div id="comment-39461" class="comment"><div id="post-39461-score" class="comment-score"></div><div class="comment-text"><p>Never done it mysefl, it would help if you stated what platform you build for.</p></div><div id="comment-39461-info" class="comment-info"><span class="comment-age">(28 Jan '15, 12:01)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="39630"></span><div id="comment-39630" class="comment"><div id="post-39630-score" class="comment-score"></div><div class="comment-text"><p>I use ubuntu for building I made some minor changes to the code in codecs even after installing spandsp library I was the codecs were not being included which i presumed was because of the #ifdef so i uncommented them and tried running them but it dint work..I went through some queries in the mailing lists and i read one of yr answers on the version of spandsp that was being used then.I got that deom sourceforge and tried to run it ..to my surprise that also did not work.Then i finally gaveup!</p></div><div id="comment-39630-info" class="comment-info"><span class="comment-age">(04 Feb '15, 02:17)</span> <span class="comment-user userinfo">koundi</span></div></div></div><div id="comment-tools-39365" class="comment-tools"></div><div class="clear"></div><div id="comment-39365-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="57615"></span>

<div id="answer-container-57615" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-57615-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-57615-score" class="post-score" title="current number of votes">0</div><span id="post-57615-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark does not yet support G.722 playback, but support for it is currently being worked on. You can track the status in <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5619">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5619</a></p><p>As for the licensing concern raised by Anders, apparently the patents have expired so you can freely use G.722. As Jaap mentioned, Spandsp is needed for G.722 playback, but it was not fully integrated yet (see above bug).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Nov '16, 09:44</strong></p><img src="https://secure.gravatar.com/avatar/285b1f0f4caadc088a38c40aea22feba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lekensteyn&#39;s gravatar image" /><p><span>Lekensteyn</span><br />
<span class="score" title="2213 reputation points"><span>2.2k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="24 badges"><span class="bronze">●</span><span class="badgecount">24</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lekensteyn has 32 accepted answers">30%</span></p></div></div><div id="comments-container-57615" class="comments-container"></div><div id="comment-tools-57615" class="comment-tools"></div><div class="clear"></div><div id="comment-57615-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

