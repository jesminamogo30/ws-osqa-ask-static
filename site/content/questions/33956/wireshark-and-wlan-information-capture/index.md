+++
type = "question"
title = "Wireshark and WLAN information capture"
description = '''What is the best way to capture WLAN transimission information between an Access Point and another device using wireshark?  Is it advisable to use a separate device running wireshark between the AP and user or using wireshark on the users device?'''
date = "2014-06-19T05:45:00Z"
lastmod = "2014-06-21T17:24:00Z"
weight = 33956
keywords = [ "wireshark" ]
aliases = [ "/questions/33956" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark and WLAN information capture](/questions/33956/wireshark-and-wlan-information-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33956-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33956-score" class="post-score" title="current number of votes">0</div><span id="post-33956-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What is the best way to capture WLAN transimission information between an Access Point and another device using wireshark? Is it advisable to use a separate device running wireshark between the AP and user or using wireshark on the users device?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Jun '14, 05:45</strong></p><img src="https://secure.gravatar.com/avatar/f0b8218b8e0cd71d4903ea4e675f01d0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="haykhusaiby&#39;s gravatar image" /><p><span>haykhusaiby</span><br />
<span class="score" title="16 reputation points">16</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="haykhusaiby has no accepted answers">0%</span></p></div></div><div id="comments-container-33956" class="comments-container"></div><div id="comment-tools-33956" class="comment-tools"></div><div class="clear"></div><div id="comment-33956-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34026"></span>

<div id="answer-container-34026" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34026-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34026-score" class="post-score" title="current number of votes">1</div><span id="post-34026-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="haykhusaiby has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See the WLAN capturing Wiki.</p><blockquote><p><a href="http://wiki.wireshark.org/CaptureSetup/WLAN">http://wiki.wireshark.org/CaptureSetup/WLAN</a><br />
</p></blockquote><p>It should contain the answer to your question.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Jun '14, 17:24</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-34026" class="comments-container"></div><div id="comment-tools-34026" class="comment-tools"></div><div class="clear"></div><div id="comment-34026-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

