+++
type = "question"
title = "What happened to my comments in this forum?"
description = '''I just added (second time, sigh) a comment to the question &quot;ACK/RST loop&quot;. The comment was shown in my browser session, but vanished when I reloaded the screen. The problem seems to be consistent: I can add a second reply which is shown nicely. When converting the reply to a comment it vanishes. Is ...'''
date = "2017-01-11T14:16:00Z"
lastmod = "2017-01-12T02:26:00Z"
weight = 58673
keywords = [ "ask.wireshark.org", "forum" ]
aliases = [ "/questions/58673" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [What happened to my comments in this forum?](/questions/58673/what-happened-to-my-comments-in-this-forum)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58673-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58673-score" class="post-score" title="current number of votes">0</div><span id="post-58673-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I just added (second time, sigh) a comment to the question "ACK/RST loop". The comment was shown in my browser session, but vanished when I reloaded the screen.</p><p>The problem seems to be consistent: I can add a second reply which is shown nicely. When converting the reply to a comment it vanishes.</p><p>Is there a limit on the number of comments or total size of comments?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ask.wireshark.org" rel="tag" title="see questions tagged &#39;ask.wireshark.org&#39;">ask.wireshark.org</span> <span class="post-tag tag-link-forum" rel="tag" title="see questions tagged &#39;forum&#39;">forum</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '17, 14:16</strong></p><img src="https://secure.gravatar.com/avatar/3b60e92020a427bb24332efc0b560943?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="packethunter&#39;s gravatar image" /><p><span>packethunter</span><br />
<span class="score" title="2137 reputation points"><span>2.1k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="48 badges"><span class="bronze">●</span><span class="badgecount">48</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="packethunter has 8 accepted answers">8%</span></p></div></div><div id="comments-container-58673" class="comments-container"><span id="58679"></span><div id="comment-58679" class="comment"><div id="post-58679-score" class="comment-score">1</div><div class="comment-text"><p>I think your comments are there... is it a cache issue on your browser? I see multiple copies of the same thing,</p></div><div id="comment-58679-info" class="comment-info"><span class="comment-age">(11 Jan '17, 14:27)</span> <span class="comment-user userinfo">Bob Jones</span></div></div></div><div id="comment-tools-58673" class="comment-tools"></div><div class="clear"></div><div id="comment-58673-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="58674"></span>

<div id="answer-container-58674" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58674-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58674-score" class="post-score" title="current number of votes">1</div><span id="post-58674-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="packethunter has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The current setting sets a maximum of 2500 characters in the body of a comment.</p><p>Sometimes, graphics in comments lead to the width of the post exceeding the screen size, so the "show more comments" button on the bottom right is out of the visible screen. You need to scroll to find it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Jan '17, 14:21</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Jan '17, 14:34</strong> </span></p></div></div><div id="comments-container-58674" class="comments-container"><span id="58685"></span><div id="comment-58685" class="comment"><div id="post-58685-score" class="comment-score"></div><div class="comment-text"><p>[Palmface]</p><p>The whole communication has a whole bunch of screenshots, some of them very wide.</p><p>Scrolling aaallllll the way helps finding the button.</p><p>Time to clean up.</p></div><div id="comment-58685-info" class="comment-info"><span class="comment-age">(11 Jan '17, 22:56)</span> <span class="comment-user userinfo">packethunter</span></div></div><span id="58695"></span><div id="comment-58695" class="comment"><div id="post-58695-score" class="comment-score"></div><div class="comment-text"><p>Images in comments can have their width restricted by using a plain image tag, e.g.</p><pre><code>&lt;img src=&quot;url_of_image&quot; width=&quot;xxx&quot; /&gt;</code></pre><p>I find a width of 600 is reasonable.</p></div><div id="comment-58695-info" class="comment-info"><span class="comment-age">(12 Jan '17, 02:26)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-58674" class="comment-tools"></div><div class="clear"></div><div id="comment-58674-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

