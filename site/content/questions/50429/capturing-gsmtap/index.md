+++
type = "question"
title = "[closed] Capturing GSMTAP"
description = '''Hi everyone, I am a student who started to learn networking including GSM network. After a little bit of searching, I understood that I can capture GSM packets from port 4729 (which is the GSM TAP port). I had a little experiment which I used a USRP device with airprobe to get the GSM packets and us...'''
date = "2016-02-23T04:07:00Z"
lastmod = "2016-02-23T04:45:00Z"
weight = 50429
keywords = [ "gsmtap", "scapy", "wireshark" ]
aliases = [ "/questions/50429" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Capturing GSMTAP](/questions/50429/capturing-gsmtap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50429-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50429-score" class="post-score" title="current number of votes">0</div><span id="post-50429-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone, I am a student who started to learn networking including GSM network. After a little bit of searching, I understood that I can capture GSM packets from port 4729 (which is the GSM TAP port). I had a little experiment which I used a USRP device with airprobe to get the GSM packets and used scapy to capture those packets, but I got (as much as I can see) nothing. I have some weird string (looks like hexa) in the RAW layer. After that I used wireshark and... I was amazed to see all the data I could get.</p><p>My question is: How does wireshark capture all the packets as they can be seen on wireshark but Scapy can't? How can I use Scapy to capture these packets like wireshark does?</p><p>Thank you in advance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gsmtap" rel="tag" title="see questions tagged &#39;gsmtap&#39;">gsmtap</span> <span class="post-tag tag-link-scapy" rel="tag" title="see questions tagged &#39;scapy&#39;">scapy</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Feb '16, 04:07</strong></p><img src="https://secure.gravatar.com/avatar/404407b9041f40fa0a3f2976db3c29a3?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aGuy&#39;s gravatar image" /><p><span>aGuy</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aGuy has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>23 Feb '16, 04:46</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-50429" class="comments-container"><span id="50430"></span><div id="comment-50430" class="comment"><div id="post-50430-score" class="comment-score"></div><div class="comment-text"><p>As you've written yourself, Scapy <em>captures</em> the packets just fine, except that it is not, using the Wireshark vocabulary, <em>dissecting</em> them the way Wireshark does.</p><p>This site deals with Wireshark, so questions "how do I make software XXX other than Wireshark do YYY" are off-topic.</p></div><div id="comment-50430-info" class="comment-info"><span class="comment-age">(23 Feb '16, 04:45)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-50429" class="comment-tools"></div><div class="clear"></div><div id="comment-50429-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by sindy 23 Feb '16, 04:46

</div>

</div>

</div>

