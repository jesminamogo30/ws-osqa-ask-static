+++
type = "question"
title = "Pcaket Capture System Build Suggestions"
description = '''If you had to build a packet capture system how would you build it? You have 2500.00 max for the system. Thank you.'''
date = "2014-09-08T08:22:00Z"
lastmod = "2014-09-08T14:02:00Z"
weight = 36078
keywords = [ "pcap" ]
aliases = [ "/questions/36078" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Pcaket Capture System Build Suggestions](/questions/36078/pcaket-capture-system-build-suggestions)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36078-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36078-score" class="post-score" title="current number of votes">0</div><span id="post-36078-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>If you had to build a packet capture system how would you build it? You have 2500.00 max for the system. Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Sep '14, 08:22</strong></p><img src="https://secure.gravatar.com/avatar/19e79b2c96f0c36649e73e79c84c8fc6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bryanbent&#39;s gravatar image" /><p><span>bryanbent</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bryanbent has no accepted answers">0%</span></p></div></div><div id="comments-container-36078" class="comments-container"></div><div id="comment-tools-36078" class="comment-tools"></div><div class="clear"></div><div id="comment-36078-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36087"></span>

<div id="answer-container-36087" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36087-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36087-score" class="post-score" title="current number of votes">0</div><span id="post-36087-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>First it depends on the use case Server, "portable" or laptop. High speed capture e.g close to 1Gb/s CPU amount of RAM and disk speed and size of discs I suppose and which OS to be used. This presentation at Shark fest '14 may be of interest <a href="http://sharkfest.wireshark.org/sharkfest.14/presentations/I3:%20Sharkfest_2014_ABrown%20-%20Copy.pdf">http://sharkfest.wireshark.org/sharkfest.14/presentations/I3:%20Sharkfest_2014_ABrown%20-%20Copy.pdf</a><br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Sep '14, 14:02</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span> </br></p></div></div><div id="comments-container-36087" class="comments-container"></div><div id="comment-tools-36087" class="comment-tools"></div><div class="clear"></div><div id="comment-36087-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

