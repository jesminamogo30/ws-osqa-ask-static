+++
type = "question"
title = "How to identify SSL Certificate file name"
description = '''Hi. I often run into this problem... I&#x27;m asked to capture SSL traffic (we use a lot of it around here), and, of course, I need the Cert in order to decrypt. But, often, requesting the cert from the folks who manage it, requires that I have the exact filename of the cert (e.g., XYZ12.PFX). Is there a...'''
date = "2011-06-02T09:25:00Z"
lastmod = "2013-05-23T21:25:00Z"
weight = 4337
keywords = [ "ssl", "cert", "certificate" ]
aliases = [ "/questions/4337" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How to identify SSL Certificate file name](/questions/4337/how-to-identify-ssl-certificate-file-name)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4337-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4337-score" class="post-score" title="current number of votes">0</div><span id="post-4337-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi.</p><p>I often run into this problem... I'm asked to capture SSL traffic (we use a lot of it around here), and, of course, I need the Cert in order to decrypt. But, often, requesting the cert from the folks who manage it, requires that I have the exact filename of the cert (e.g., XYZ12.PFX).</p><p>Is there a way that I can see the filename of the cert in the undecrypted packets - perhaps in the Server Hello?</p><p>Thx for any suggestions.</p><p>Feenyman99</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl" rel="tag" title="see questions tagged &#39;ssl&#39;">ssl</span> <span class="post-tag tag-link-cert" rel="tag" title="see questions tagged &#39;cert&#39;">cert</span> <span class="post-tag tag-link-certificate" rel="tag" title="see questions tagged &#39;certificate&#39;">certificate</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Jun '11, 09:25</strong></p><img src="https://secure.gravatar.com/avatar/ba0791e3a82c059268b46a45ae90989f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="feenyman99&#39;s gravatar image" /><p><span>feenyman99</span><br />
<span class="score" title="96 reputation points">96</span><span title="22 badges"><span class="badge1">●</span><span class="badgecount">22</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="26 badges"><span class="bronze">●</span><span class="badgecount">26</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="feenyman99 has one accepted answer">25%</span></p></div></div><div id="comments-container-4337" class="comments-container"><span id="21429"></span><div id="comment-21429" class="comment"><div id="post-21429-score" class="comment-score"></div><div class="comment-text"><p>Do you mean the key? You can get the certificate from the traffic...</p></div><div id="comment-21429-info" class="comment-info"><span class="comment-age">(23 May '13, 21:25)</span> <span class="comment-user userinfo">rakslice</span></div></div></div><div id="comment-tools-4337" class="comment-tools"></div><div class="clear"></div><div id="comment-4337-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4339"></span>

<div id="answer-container-4339" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4339-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4339-score" class="post-score" title="current number of votes">1</div><span id="post-4339-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Jaap has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I don't think that this is possible. The Server Hello does not contain any information about local file paths or file names on the HTTPS server as far as I know. And I think it would be considered a more or less serious security flaw if it would since bad guys could use that kind of information to get to know more about the server in an attempt to break in.</p><p>Maybe the server guys can get you a copy of the server config files so that you can inspect them to see which vHost has which certificate file assigned.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Jun '11, 09:54</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-4339" class="comments-container"></div><div id="comment-tools-4339" class="comment-tools"></div><div class="clear"></div><div id="comment-4339-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

