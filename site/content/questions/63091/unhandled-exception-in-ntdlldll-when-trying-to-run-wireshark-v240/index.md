+++
type = "question"
title = "Unhandled exception in ntdll.dll when trying to run Wireshark v2.4.0"
description = '''After install and reboot on a new HP 64bit Windows 7 Pro machine, Wireshark V2.4.0 will not run. Debugging the error returns: Unhandled exception at 0x0000000077B7EEF1 (ntdll.dll) in Wireshark.exe: 0xC0000005: Access violation reading location 0x0000000000C3AC78.'''
date = "2017-07-25T08:08:00Z"
lastmod = "2017-07-25T08:48:00Z"
weight = 63091
keywords = [ "unhandled", "exception" ]
aliases = [ "/questions/63091" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unhandled exception in ntdll.dll when trying to run Wireshark v2.4.0](/questions/63091/unhandled-exception-in-ntdlldll-when-trying-to-run-wireshark-v240)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63091-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63091-score" class="post-score" title="current number of votes">0</div><span id="post-63091-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After install and reboot on a new HP 64bit Windows 7 Pro machine, Wireshark V2.4.0 will not run. Debugging the error returns: Unhandled exception at 0x0000000077B7EEF1 (ntdll.dll) in Wireshark.exe: 0xC0000005: Access violation reading location 0x0000000000C3AC78.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-unhandled" rel="tag" title="see questions tagged &#39;unhandled&#39;">unhandled</span> <span class="post-tag tag-link-exception" rel="tag" title="see questions tagged &#39;exception&#39;">exception</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jul '17, 08:08</strong></p><img src="https://secure.gravatar.com/avatar/76c660a6be26e7cb20afabef819dbd7b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="macday&#39;s gravatar image" /><p><span>macday</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="macday has no accepted answers">0%</span></p></div></div><div id="comments-container-63091" class="comments-container"><span id="63093"></span><div id="comment-63093" class="comment"><div id="post-63093-score" class="comment-score"></div><div class="comment-text"><p>Works for me on Win 7 Pro (Dell hardware) SP1.</p><p>Can you copy and paste the text from Help -&gt; About Wireshark, Wireshark tab?</p></div><div id="comment-63093-info" class="comment-info"><span class="comment-age">(25 Jul '17, 08:48)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-63091" class="comment-tools"></div><div class="clear"></div><div id="comment-63091-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

