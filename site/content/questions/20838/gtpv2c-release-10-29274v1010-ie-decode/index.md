+++
type = "question"
title = "GTPv2C Release 10 ( 29.274v10.10) IE Decode"
description = '''Hi Wireshark, I want to know that does any version of wireshark decodes the newly introduced IEs of GTPv2C (like Node Features, EPC Timer, Signalling Priority Indication etc)? Wireshark can identify the IEs, but not further decoding!! Please mention the version. Thanks in advance!! '''
date = "2013-04-29T06:53:00Z"
lastmod = "2013-05-22T08:41:00Z"
weight = 20838
keywords = [ "gtpv2c", "release10" ]
aliases = [ "/questions/20838" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [GTPv2C Release 10 ( 29.274v10.10) IE Decode](/questions/20838/gtpv2c-release-10-29274v1010-ie-decode)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20838-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20838-score" class="post-score" title="current number of votes">0</div><span id="post-20838-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Wireshark,</p><p>I want to know that does any version of wireshark decodes the newly introduced IEs of GTPv2C (like Node Features, EPC Timer, Signalling Priority Indication etc)? Wireshark can identify the IEs, but not further decoding!! Please mention the version.</p><p>Thanks in advance!!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtpv2c" rel="tag" title="see questions tagged &#39;gtpv2c&#39;">gtpv2c</span> <span class="post-tag tag-link-release10" rel="tag" title="see questions tagged &#39;release10&#39;">release10</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Apr '13, 06:53</strong></p><img src="https://secure.gravatar.com/avatar/e82780891a1e938f0bf3a529adc858a5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="baila&#39;s gravatar image" /><p><span>baila</span><br />
<span class="score" title="21 reputation points">21</span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="11 badges"><span class="silver">●</span><span class="badgecount">11</span></span><span title="15 badges"><span class="bronze">●</span><span class="badgecount">15</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="baila has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Apr '13, 06:54</strong> </span></p></div></div><div id="comments-container-20838" class="comments-container"></div><div id="comment-tools-20838" class="comment-tools"></div><div class="clear"></div><div id="comment-20838-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="20839"></span>

<div id="answer-container-20839" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20839-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20839-score" class="post-score" title="current number of votes">0</div><span id="post-20839-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Node Features is not dissected by any version, some of the others are probably dissected in the upcomming 1.10 release <a href="http://www.wireshark.org/download.html#development_release">http://www.wireshark.org/download.html#development_release</a></p><p>To get the latest you can try a development build from <a href="http://www.wireshark.org/download/automated/">http://www.wireshark.org/download/automated/</a></p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>29 Apr '13, 08:05</strong></p><img src="https://secure.gravatar.com/avatar/2d3d425a7a829209431fb38d326b53af?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Anders&#39;s gravatar image" /><p><span>Anders ♦</span><br />
<span class="score" title="4578 reputation points"><span>4.6k</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="52 badges"><span class="bronze">●</span><span class="badgecount">52</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Anders has 56 accepted answers">17%</span></p></div></div><div id="comments-container-20839" class="comments-container"><span id="21086"></span><div id="comment-21086" class="comment"><div id="post-21086-score" class="comment-score"></div><div class="comment-text"><p><span>@Anders</span></p><p>How to use the development build?</p></div><div id="comment-21086-info" class="comment-info"><span class="comment-age">(10 May '13, 08:32)</span> <span class="comment-user userinfo">baila</span></div></div><span id="21374"></span><div id="comment-21374" class="comment"><div id="post-21374-score" class="comment-score"></div><div class="comment-text"><p>hello all, please give me some suggestion how to build the development codes??</p></div><div id="comment-21374-info" class="comment-info"><span class="comment-age">(22 May '13, 08:26)</span> <span class="comment-user userinfo">baila</span></div></div><span id="21376"></span><div id="comment-21376" class="comment"><div id="post-21376-score" class="comment-score"></div><div class="comment-text"><p>You can find development nightly builds here: <a href="http://www.wireshark.org/download/automated/">http://www.wireshark.org/download/automated/</a></p></div><div id="comment-21376-info" class="comment-info"><span class="comment-age">(22 May '13, 08:41)</span> <span class="comment-user userinfo">Pascal Quantin</span></div></div></div><div id="comment-tools-20839" class="comment-tools"></div><div class="clear"></div><div id="comment-20839-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

