+++
type = "question"
title = "WinPcaps Bugs - No Wireshark Bugs"
description = '''There is a WinPcap bug in the Wireshark installation 1.12.4 which include WinPcap 4.1.3. During the installation I receive the info: There is another WinPcap on the computer, which cannot de-installed. But this is not a bug, it’s a result of another program which I use: FreemakeVideoDownloader 3.7.1...'''
date = "2015-03-20T10:07:00Z"
lastmod = "2015-03-23T09:59:00Z"
weight = 40743
keywords = [ "winpcap" ]
aliases = [ "/questions/40743" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [WinPcaps Bugs - No Wireshark Bugs](/questions/40743/winpcaps-bugs-no-wireshark-bugs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40743-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40743-score" class="post-score" title="current number of votes">0</div><span id="post-40743-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>There is a WinPcap bug in the Wireshark installation 1.12.4 which include WinPcap 4.1.3. During the installation I receive the info: There is another WinPcap on the computer, which cannot de-installed.</p><p>But this is <strong>not</strong> a bug, it’s a result of another program which I use: FreemakeVideoDownloader 3.7.1.5.</p><p>If I install WinPcap 4.1.3 exe at first alone, everything is okay. Also other programs (Media Sniffer, Media Tab) which using WinPcap are running well. If I install the latest Wireshark release everything is ok.</p><p>Also the installation of FreemakeVideoDownloader is running well, but I found out with the CCleaner Tool, that my single installation from <strong>WinPCap 4.1.3</strong> is changed after the installation of FreemakeVideoDownloader into <strong>WinPcap 4.1.2 version 4.1.0.2001.</strong></p><p>After the change into an older version of WinPcap every installation of Wireshark show during the installation that the installed or changed WinPcap 4.1.2 (from FreemakeVideoDownloader) cannot be changed into a newer one. Is that a bug? Freemake/Ellora Assets Corporation is not willing to use the latest version of WinPcap.</p><p>I don’t know if Wireshark or other programs show the right data after that change. Perhaps other programs which include also WinPcap will also disturb the Wireshark datas, if these programs also change the installed version of Wireshark with WinPcap.</p><p>A new clean version of WinPCap can only installed, if all programs which are using WinPCap will be uninstalled, rests of data must manually deleted including all – unseen - temporary data of the various programs, especially from WinPcap.</p><p>Now you can install a new clean Wireshark with the latest WinPcap. If you re-install the other programs it’s better to control after each installation if WinPcap is changed from the new program into an older version and if you test it with a second uninstall and reinstall of Wireshark, and WinPcap cannot be installed in the Wireshark installation routine you have a “WinPcap-bug” as result of the latest installation.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-winpcap" rel="tag" title="see questions tagged &#39;winpcap&#39;">winpcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Mar '15, 10:07</strong></p><img src="https://secure.gravatar.com/avatar/0cc836cd8810525ec95315516a4cb67a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Unseen&#39;s gravatar image" /><p><span>Unseen</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Unseen has no accepted answers">0%</span></p></div></div><div id="comments-container-40743" class="comments-container"><span id="40749"></span><div id="comment-40749" class="comment"><div id="post-40749-score" class="comment-score"></div><div class="comment-text"><p>O.K. and what is your question?</p></div><div id="comment-40749-info" class="comment-info"><span class="comment-age">(20 Mar '15, 11:47)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="40752"></span><div id="comment-40752" class="comment"><div id="post-40752-score" class="comment-score"></div><div class="comment-text"><p>I think this is more of an FYI...</p></div><div id="comment-40752-info" class="comment-info"><span class="comment-age">(20 Mar '15, 12:23)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="40785"></span><div id="comment-40785" class="comment"><div id="post-40785-score" class="comment-score"></div><div class="comment-text"><p>then, the wiki would be a better place...</p></div><div id="comment-40785-info" class="comment-info"><span class="comment-age">(23 Mar '15, 09:59)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-40743" class="comment-tools"></div><div class="clear"></div><div id="comment-40743-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

