+++
type = "question"
title = "After freeze Software updater window than update wireshark, wireshark did not run."
description = '''That error i see in log: kernel: [58668.188755] wireshark[5058]: segfault at 18 ip 00007fb40cd59000 sp 00007ffc8aa2e708 error 4 in libxcb.so.1.1.0[7fb40cd4b000+1d000] P.S:Sorry for my English.'''
date = "2016-12-26T18:57:00Z"
lastmod = "2016-12-28T05:36:00Z"
weight = 58341
keywords = [ "did", "not", "run", "wireshark" ]
aliases = [ "/questions/58341" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [After freeze Software updater window than update wireshark, wireshark did not run.](/questions/58341/after-freeze-software-updater-window-than-update-wireshark-wireshark-did-not-run)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58341-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58341-score" class="post-score" title="current number of votes">0</div><span id="post-58341-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>That error i see in log: kernel: [58668.188755] wireshark[5058]: segfault at 18 ip 00007fb40cd59000 sp 00007ffc8aa2e708 error 4 in libxcb.so.1.1.0[7fb40cd4b000+1d000]</p><p>P.S:Sorry for my English.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-did" rel="tag" title="see questions tagged &#39;did&#39;">did</span> <span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-run" rel="tag" title="see questions tagged &#39;run&#39;">run</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Dec '16, 18:57</strong></p><img src="https://secure.gravatar.com/avatar/ebef6356dd73fc6e18fa2c3340d7fd31?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Saldor&#39;s gravatar image" /><p><span>Saldor</span><br />
<span class="score" title="6 reputation points">6</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Saldor has no accepted answers">0%</span></p></div></div><div id="comments-container-58341" class="comments-container"><span id="58387"></span><div id="comment-58387" class="comment"><div id="post-58387-score" class="comment-score"></div><div class="comment-text"><p>What OS? And how did you do the update? And to which version of Wireshark?</p></div><div id="comment-58387-info" class="comment-info"><span class="comment-age">(27 Dec '16, 16:43)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="58388"></span><div id="comment-58388" class="comment"><div id="post-58388-score" class="comment-score"></div><div class="comment-text"><p>Ubuntu 14, upgrade to wireshark 12 from 10 version.</p></div><div id="comment-58388-info" class="comment-info"><span class="comment-age">(27 Dec '16, 16:47)</span> <span class="comment-user userinfo">Saldor</span></div></div><span id="58389"></span><div id="comment-58389" class="comment"><div id="post-58389-score" class="comment-score"></div><div class="comment-text"><p>Do you mean Wireshark 1.12 from 1.10, and via the Ubuntu software Update tool?</p></div><div id="comment-58389-info" class="comment-info"><span class="comment-age">(27 Dec '16, 17:12)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div><span id="58390"></span><div id="comment-58390" class="comment"><div id="post-58390-score" class="comment-score"></div><div class="comment-text"><p>yes via ubuntu software update tool. From wireshark 1.10 to 2.04. In Ubuntu Software Center the version change after this manipulation to 2.04, but did not run.</p></div><div id="comment-58390-info" class="comment-info"><span class="comment-age">(27 Dec '16, 17:40)</span> <span class="comment-user userinfo">Saldor</span></div></div><span id="58402"></span><div id="comment-58402" class="comment"><div id="post-58402-score" class="comment-score"></div><div class="comment-text"><p>That looks like a question for the Ubuntu packagers, the Wireshark project doesn't provide those packages.</p></div><div id="comment-58402-info" class="comment-info"><span class="comment-age">(28 Dec '16, 05:36)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-58341" class="comment-tools"></div><div class="clear"></div><div id="comment-58341-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

