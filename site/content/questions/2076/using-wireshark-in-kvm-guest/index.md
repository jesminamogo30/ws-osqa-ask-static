+++
type = "question"
title = "Using Wireshark in KVM Guest"
description = '''Hi there, is it possible to capture network traffic in a linux kvm guest with network bridge? Maybe it is a fundamental problem to capture from virtualized guests? My setup looks like: KVM host (debian squeeze) bridge device br0 on eth1 KVM guest (centos 5.5) has bridge interface br0 configured as b...'''
date = "2011-02-01T10:03:00Z"
lastmod = "2011-02-01T10:03:00Z"
weight = 2076
keywords = [ "kvm", "guest", "linux" ]
aliases = [ "/questions/2076" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Using Wireshark in KVM Guest](/questions/2076/using-wireshark-in-kvm-guest)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2076-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2076-score" class="post-score" title="current number of votes">0</div><span id="post-2076-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi there,</p><p>is it possible to capture network traffic in a linux kvm guest with network bridge? Maybe it is a fundamental problem to capture from virtualized guests?</p><p>My setup looks like: KVM host (debian squeeze) bridge device br0 on eth1<br />
KVM guest (centos 5.5) has bridge interface br0 configured as bridge (rtl8xxx)<br />
Two real computers with centos 5.5 and physical eth0</p><p>All computers are connected with a hub and the network connection is fine. I want to capture for e.g. pings between the two real computers .</p><p>Steffen</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-kvm" rel="tag" title="see questions tagged &#39;kvm&#39;">kvm</span> <span class="post-tag tag-link-guest" rel="tag" title="see questions tagged &#39;guest&#39;">guest</span> <span class="post-tag tag-link-linux" rel="tag" title="see questions tagged &#39;linux&#39;">linux</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Feb '11, 10:03</strong></p><img src="https://secure.gravatar.com/avatar/b10b57ea454770fe106e64b94f5d94c5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="h3r3tic&#39;s gravatar image" /><p><span>h3r3tic</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="h3r3tic has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-2076" class="comments-container"></div><div id="comment-tools-2076" class="comment-tools"></div><div class="clear"></div><div id="comment-2076-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

