+++
type = "question"
title = "what&#x27;s the primary reason for wireshark not being hosted on github"
description = '''when it&#x27;s free -- https://github.com/plans and the UI is far superior -- http://marmoush.com/2011/10/04/sourceforge-vs-github-2011/ and when it&#x27;s leading -- http://sourceforge.net/blog/github-collaboration-and-haters/'''
date = "2012-05-15T23:12:00Z"
lastmod = "2014-02-22T11:32:00Z"
weight = 11019
keywords = [ "wireshark" ]
aliases = [ "/questions/11019" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [what's the primary reason for wireshark not being hosted on github](/questions/11019/whats-the-primary-reason-for-wireshark-not-being-hosted-on-github)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11019-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11019-score" class="post-score" title="current number of votes">0</div><span id="post-11019-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>when it's free -- <a href="https://github.com/plans">https://github.com/plans</a><br />
and the UI is far superior -- <a href="http://marmoush.com/2011/10/04/sourceforge-vs-github-2011/">http://marmoush.com/2011/10/04/sourceforge-vs-github-2011/</a><br />
and when it's leading -- <a href="http://sourceforge.net/blog/github-collaboration-and-haters/">http://sourceforge.net/blog/github-collaboration-and-haters/</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 May '12, 23:12</strong></p><img src="https://secure.gravatar.com/avatar/b2a4006b4a0252f8be292c57acde97ff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wiresharkhelpers&#39;s gravatar image" /><p><span>wiresharkhel...</span><br />
<span class="score" title="30 reputation points">30</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="13 badges"><span class="bronze">●</span><span class="badgecount">13</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wiresharkhelpers has no accepted answers">0%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 May '12, 23:19</strong> </span></p></div></div><div id="comments-container-11019" class="comments-container"></div><div id="comment-tools-11019" class="comment-tools"></div><div class="clear"></div><div id="comment-11019-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="11021"></span>

<div id="answer-container-11021" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-11021-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-11021-score" class="post-score" title="current number of votes">1</div><span id="post-11021-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="wiresharkhelpers has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Because Wireshark has used svn since the projects start, and has a lot of infrastructure built around that.</p><p>Trials are underway with a git mirror of the svn repo, but AFAIK it isn't bidirectional yet. See the Developers page on the website <a href="http://www.wireshark.org/develop.html">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>15 May '12, 23:45</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 May '12, 23:49</strong> </span></p></div></div><div id="comments-container-11021" class="comments-container"></div><div id="comment-tools-11021" class="comment-tools"></div><div class="clear"></div><div id="comment-11021-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="30099"></span>

<div id="answer-container-30099" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30099-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30099-score" class="post-score" title="current number of votes">0</div><span id="post-30099-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Note also that GitHub is free, but not free/open source software.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>22 Feb '14, 11:32</strong></p><img src="https://secure.gravatar.com/avatar/162df4b0eff1277f9c4d1273a84041c9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jelmer&#39;s gravatar image" /><p><span>jelmer</span><br />
<span class="score" title="1 reputation points">1</span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jelmer has no accepted answers">0%</span></p></div></div><div id="comments-container-30099" class="comments-container"></div><div id="comment-tools-30099" class="comment-tools"></div><div class="clear"></div><div id="comment-30099-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

