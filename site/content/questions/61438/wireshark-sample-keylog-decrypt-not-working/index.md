+++
type = "question"
title = "Wireshark sample keylog decrypt not working"
description = '''While trying to track down why my key log files are not working I found the sample captures on the WIKI. The &quot;dump.pcapng&quot; with associated &quot;premaster.txt&quot; does not seem to be working for me.   I have no RSA keys in my RSA keys list Pre-Master-Shared log filename is set to the &quot;premaster.txt&quot; I downl...'''
date = "2017-05-16T10:02:00Z"
lastmod = "2017-05-16T10:02:00Z"
weight = 61438
keywords = [ "ssl_decrypt" ]
aliases = [ "/questions/61438" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark sample keylog decrypt not working](/questions/61438/wireshark-sample-keylog-decrypt-not-working)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61438-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61438-score" class="post-score" title="current number of votes">0</div><span id="post-61438-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>While trying to track down why my key log files are not working I found the sample captures on the <a href="https://wiki.wireshark.org/SampleCaptures#SSL_with_decryption_keys">WIKI</a>. The "dump.pcapng" with associated "premaster.txt" does not seem to be working for me.</p><ol><li>I have no RSA keys in my RSA keys list</li><li>Pre-Master-Shared log filename is set to the "premaster.txt" I downloaded from the WIKI</li><li>SSL debug file is set to a valid file/path</li></ol><p>Contents of resulting SSL debug file:</p><pre><code>Wireshark SSL debug log

Wireshark version: 2.2.6 (v2.2.6-0-g32dac6a)
GnuTLS version:    3.2.15
Libgcrypt version: 1.6.2</code></pre><p>I must be doing something wrong?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ssl_decrypt" rel="tag" title="see questions tagged &#39;ssl_decrypt&#39;">ssl_decrypt</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 May '17, 10:02</strong></p><img src="https://secure.gravatar.com/avatar/7b965bb47b61e97ae65639d40c05166a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="strtdusty&#39;s gravatar image" /><p><span>strtdusty</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="strtdusty has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Jun '17, 12:39</strong> </span></p></div></div><div id="comments-container-61438" class="comments-container"></div><div id="comment-tools-61438" class="comment-tools"></div><div class="clear"></div><div id="comment-61438-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

