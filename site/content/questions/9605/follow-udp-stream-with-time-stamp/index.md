+++
type = "question"
title = "follow UDP stream with time stamp?"
description = '''Hello How can I get a timestamp at the beginning of the sentences if I have carried out the “follow UDP stream” command.” $RZHTD,A,16.5,R,R,T,,,,1.7,,,,,,,,,71  $RZFPI,000,00,8,2,071  $RZRSA,15.9,A,,V4C  $RZROT,0.1,A,900A This data is from one IP address, I would like to mix it again with data from ...'''
date = "2012-03-17T12:56:00Z"
lastmod = "2012-03-17T14:02:00Z"
weight = 9605
keywords = [ "timestamp", "udp", "stream" ]
aliases = [ "/questions/9605" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [follow UDP stream with time stamp?](/questions/9605/follow-udp-stream-with-time-stamp)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9605-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9605-score" class="post-score" title="current number of votes">0</div><span id="post-9605-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello How can I get a timestamp at the beginning of the sentences if I have carried out the “follow UDP stream” command.”</p><p>$RZHTD,A,16.5,R,R,T,,,,1.7,,,,,,,,,<em>71 $RZFPI,000,00,8,2,0</em>71 $RZRSA,15.9,A,,V<em>4C $RZROT,0.1,A,90</em>0A</p><p>This data is from one IP address, I would like to mix it again with data from another IP address, therefore I need a timestamp. Is this possible? And How?? Thanks in advance, Regards, P Hoornstrijk.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamp" rel="tag" title="see questions tagged &#39;timestamp&#39;">timestamp</span> <span class="post-tag tag-link-udp" rel="tag" title="see questions tagged &#39;udp&#39;">udp</span> <span class="post-tag tag-link-stream" rel="tag" title="see questions tagged &#39;stream&#39;">stream</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Mar '12, 12:56</strong></p><img src="https://secure.gravatar.com/avatar/30d57bbe7bbe94491626ab93f54576df?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Piet&#39;s gravatar image" /><p><span>Piet</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Piet has no accepted answers">0%</span></p></div></div><div id="comments-container-9605" class="comments-container"></div><div id="comment-tools-9605" class="comment-tools"></div><div class="clear"></div><div id="comment-9605-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9606"></span>

<div id="answer-container-9606" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9606-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9606-score" class="post-score" title="current number of votes">0</div><span id="post-9606-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That is (currently) not possible with Follow UDP stream (or any other follow ... stream for that matter)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Mar '12, 14:02</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-9606" class="comments-container"></div><div id="comment-tools-9606" class="comment-tools"></div><div class="clear"></div><div id="comment-9606-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

