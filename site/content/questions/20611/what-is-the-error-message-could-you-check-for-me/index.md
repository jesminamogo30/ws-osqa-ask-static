+++
type = "question"
title = "what is the error message? could you check for me?"
description = '''Hi, Please check for me below link: is it block the 443? but i can telnet with 443. what is the root cause of this issue? http://www.sendspace.com/file/qyv69e Please help me, thanks. ko htwe'''
date = "2013-04-18T23:56:00Z"
lastmod = "2013-04-19T01:07:00Z"
weight = 20611
keywords = [ "tags" ]
aliases = [ "/questions/20611" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [what is the error message? could you check for me?](/questions/20611/what-is-the-error-message-could-you-check-for-me)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20611-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20611-score" class="post-score" title="current number of votes">0</div><span id="post-20611-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Please check for me below link:</p><p>is it block the 443? but i can telnet with 443.</p><p>what is the root cause of this issue?</p><p><a href="http://www.sendspace.com/file/qyv69e">http://www.sendspace.com/file/qyv69e</a></p><p>Please help me, thanks.</p><p>ko htwe</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tags" rel="tag" title="see questions tagged &#39;tags&#39;">tags</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Apr '13, 23:56</strong></p><img src="https://secure.gravatar.com/avatar/993a5013c0195ec1f207d5983af47efc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="aungkohtwe&#39;s gravatar image" /><p><span>aungkohtwe</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="aungkohtwe has no accepted answers">0%</span></p></div></div><div id="comments-container-20611" class="comments-container"></div><div id="comment-tools-20611" class="comment-tools"></div><div class="clear"></div><div id="comment-20611-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="20612"></span>

<div id="answer-container-20612" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20612-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20612-score" class="post-score" title="current number of votes">0</div><span id="post-20612-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The screen shot doesn't tell much except there is a reset coming back as an answer to a SYN, so it looks like the session is refused. You'd need to provide more detailed information to see what's going on.</p><p>BTW: removing parts of the target IP address in the packet list in the screenshot (where you put "x.x.x") doesn't help much hiding it when you forget that it is also seen in the IP layer in the decode pane...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '13, 00:04</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Apr '13, 00:05</strong> </span></p></div></div><div id="comments-container-20612" class="comments-container"><span id="20613"></span><div id="comment-20613" class="comment"><div id="post-20613-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jasper,</p><p>you can see below full of the list</p><p><a href="http://www.sendspace.com/file/8w0rta">http://www.sendspace.com/file/8w0rta</a></p><p>i want to check 203.126.29.157/57 to 192.168.100.148 , any port number is block?</p><p>Thanks,</p><p>Ko Htwe</p></div><div id="comment-20613-info" class="comment-info"><span class="comment-age">(19 Apr '13, 00:14)</span> <span class="comment-user userinfo">aungkohtwe</span></div></div><span id="20614"></span><div id="comment-20614" class="comment"><div id="post-20614-score" class="comment-score"></div><div class="comment-text"><p>No, there is no general port number block, because there are conversations in the trace you provided where data is exchanged successfully. Maybe there is a connection rate limit (meaning: if you try to connect too often, you'll be refused) since there are a few session rejects, but the port seems to be open generally.</p></div><div id="comment-20614-info" class="comment-info"><span class="comment-age">(19 Apr '13, 00:26)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="20615"></span><div id="comment-20615" class="comment"><div id="post-20615-score" class="comment-score"></div><div class="comment-text"><p>Thanks Jasper,</p><p>please help me check again for me, I am still leaner for wireshark</p><p><a href="http://www.sendspace.com/file/5pcgng">http://www.sendspace.com/file/5pcgng</a></p><p>picture 1_2 is ACK and RST and Win = 0 Len = 0, what is that mean?</p><p>picture 2_2 is TCP fast retransmission, is the packet lost?</p><p>Sometime Win number is bigger than ACK. why ? is it bcs of Duplicate packets?</p><p>Best Regards and Thanks,</p><p>Ko Htwe</p></div><div id="comment-20615-info" class="comment-info"><span class="comment-age">(19 Apr '13, 00:37)</span> <span class="comment-user userinfo">aungkohtwe</span></div></div><span id="20616"></span><div id="comment-20616" class="comment"><div id="post-20616-score" class="comment-score"></div><div class="comment-text"><p>[I converted your answer to a comment again; please use comments for follow ups]</p><p>Okay, so the reset in packet #63 seems pretty normal; you have a conversation starting in packet 36 which exchanges some data and gets torn down (even if a bit harsh, FIN followed by RST, but not unheard of). Filter used: "(ip.addr eq 203.126.29.157 and ip.addr eq 192.168.100.148) and (tcp.port eq 5061 and tcp.port eq 57810)"</p><p>The other one (RST in 685) seems more problematic: it is torn down as a result of no packets coming back from 203.126.29.158 for almost 15 seconds. On a closer look it is not that bad - the client acknowledges incoming data in 141 and has nothing else to say, so the connection is terminated after an inactivity timeout. Pretty normal, too. Filter used: "(ip.addr eq 192.168.100.148 and ip.addr eq 203.126.29.158) and (tcp.port eq 57811 and tcp.port eq 443)"</p><p>So far, nothing bad is happening with the resets in your trace ;-)</p></div><div id="comment-20616-info" class="comment-info"><span class="comment-age">(19 Apr '13, 00:51)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-20612" class="comment-tools"></div><div class="clear"></div><div id="comment-20612-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="20618"></span>

<div id="answer-container-20618" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-20618-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-20618-score" class="post-score" title="current number of votes">0</div><span id="post-20618-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><strong>UPDATE</strong></p><blockquote><p>is it <strong>block the 443</strong>? but i can <strong>telnet with 443</strong>.</p></blockquote><p>I just saw the pcap file you posted. The PCAP file contains a secure SIP session (port 5061), <strong>not</strong> a HTTPS connection (port 443) to the server in question (203.126.29.157). Maybe that's your problem, a misunderstanding of the ports being used!?! As I said, the server <strong>also</strong> accepts TCP connections on port 443, but it does not respond to the SSL handshake. That could be an error or it could be intentional. Please ask the operator of the site.</p><hr /><blockquote><p>is it block the 443? but i can telnet with 443.</p></blockquote><p>no, port 443 is <strong>not</strong> blocked.</p><blockquote><p>what is the root cause of this issue?</p></blockquote><p>The server accepts the TCP connection, but it does not answer the SSL/TLS handshake. It also does not answer a HTTP request on port 443, which is a common config error of web servers.</p><p>So, it could be a general configuration error or a problem with the server software. Please contact the owner of the site and ask for help.</p><p><strong>curl</strong></p><pre><code>C:\tools\curl&gt;curl -v https://203.126.29.157
 About to connect() to 203.126.29.157 port 443 (#0)
   Trying 203.126.29.157...
 connected
 Connected to 203.126.29.157 (203.126.29.157) port 443 (#0)
 SSLv3, TLS handshake, Client hello (1):
 Unknown SSL protocol error in connection to 203.126.29.157:443
* Closing connection #0
curl: (35) Unknown SSL protocol error in connection to 203.126.29.157:443</code></pre><p><strong>openssl</strong></p><pre><code>C:\tools\openssl\bin&gt;openssl s_client -connect 203.126.29.157:443 -debug
Loading &#39;screen&#39; into random state - done
CONNECTED(00000780)
write to 0xa72210 [0xa72cc8] (210 bytes =&gt; 210 (0xD2))
0000 - 16 03 01 00 cd 01 00 00-c9 03 01 51 70 fa a2 c1   ...........Qp...
0010 - 58 7f f8 5f e0 cd 1e 16-8b 03 83 e5 40 0f c9 bc   [email protected]
0020 - d7 ad 39 c2 96 55 c1 e6-a3 b7 88 00 00 5c c0 14   ..9..U.......\..
0030 - c0 0a 00 39 00 38 00 88-00 87 c0 0f c0 05 00 35   ...9.8.........5
0040 - 00 84 c0 12 c0 08 00 16-00 13 c0 0d c0 03 00 0a   ................
0050 - c0 13 c0 09 00 33 00 32-00 9a 00 99 00 45 00 44   .....3.2.....E.D
0060 - c0 0e c0 04 00 2f 00 96-00 41 00 07 c0 11 c0 07   ...../...A......
0070 - c0 0c c0 02 00 05 00 04-00 15 00 12 00 09 00 14   ................
0080 - 00 11 00 08 00 06 00 03-00 ff 01 00 00 44 00 0b   .............D..
0090 - 00 04 03 00 01 02 00 0a-00 34 00 32 00 01 00 02   .........4.2....
00a0 - 00 03 00 04 00 05 00 06-00 07 00 08 00 09 00 0a   ................
00b0 - 00 0b 00 0c 00 0d 00 0e-00 0f 00 10 00 11 00 12   ................
00c0 - 00 13 00 14 00 15 00 16-00 17 00 18 00 19 00 23   ...............#
00d2 - &lt;spaces nuls=&quot;&quot;&gt;
read from 0xa72210 [0xa78228] (7 bytes =&gt; -1 (0xFFFFFFFF))
write:errno=10054
---
no peer certificate available
---
No client certificate CA names sent
---
SSL handshake has read 0 bytes and written 210 bytes
---
New, (NONE), Cipher is (NONE)
Secure Renegotiation IS NOT supported
Compression: NONE
Expansion: NONE
---</code></pre><p><strong>cloudshark</strong></p><blockquote><p><a href="https://www.cloudshark.org/captures/a3e0573bbce3">https://www.cloudshark.org/captures/a3e0573bbce3</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Apr '13, 01:07</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Apr '13, 02:11</strong> </span></p></div></div><div id="comments-container-20618" class="comments-container"></div><div id="comment-tools-20618" class="comment-tools"></div><div class="clear"></div><div id="comment-20618-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

