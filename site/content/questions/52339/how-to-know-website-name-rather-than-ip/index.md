+++
type = "question"
title = "How to know website name rather than ip"
description = '''I want to know the website name of people visiting in my LAN rather than their ip, how to do this with Version 1.12.4 on my windows xp'''
date = "2016-05-09T04:24:00Z"
lastmod = "2016-05-09T04:54:00Z"
weight = 52339
keywords = [ "website", "ip" ]
aliases = [ "/questions/52339" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to know website name rather than ip](/questions/52339/how-to-know-website-name-rather-than-ip)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52339-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52339-score" class="post-score" title="current number of votes">0</div><span id="post-52339-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to know the website name of people visiting in my LAN rather than their ip, how to do this with Version 1.12.4 on my windows xp</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-website" rel="tag" title="see questions tagged &#39;website&#39;">website</span> <span class="post-tag tag-link-ip" rel="tag" title="see questions tagged &#39;ip&#39;">ip</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 May '16, 04:24</strong></p><img src="https://secure.gravatar.com/avatar/e0c449b01c48ded913c315802c4c8ebb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="monsterdevil&#39;s gravatar image" /><p><span>monsterdevil</span><br />
<span class="score" title="5 reputation points">5</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="monsterdevil has no accepted answers">0%</span></p></div></div><div id="comments-container-52339" class="comments-container"><span id="52340"></span><div id="comment-52340" class="comment"><div id="post-52340-score" class="comment-score"></div><div class="comment-text"><p>I think you meant to say DNS name.</p></div><div id="comment-52340-info" class="comment-info"><span class="comment-age">(09 May '16, 04:50)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-52339" class="comment-tools"></div><div class="clear"></div><div id="comment-52339-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="52341"></span>

<div id="answer-container-52341" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-52341-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-52341-score" class="post-score" title="current number of votes">1</div><span id="post-52341-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Enable <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChAdvNameResolutionSection.html">network name resolving</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 May '16, 04:54</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-52341" class="comments-container"></div><div id="comment-tools-52341" class="comment-tools"></div><div class="clear"></div><div id="comment-52341-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

