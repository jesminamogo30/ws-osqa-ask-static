+++
type = "question"
title = "ID Adobe Connect traffic"
description = '''I am looking for a way to filter adobe connect traffic for an ACL. Unfortunately my research is coming up blank for anything other than a destination IP to filter on. I am hoping for a more IP ambiguous and more application specific means of identify AC traffic. Thank you in advance for any insight/...'''
date = "2015-02-19T11:37:00Z"
lastmod = "2015-02-23T09:40:00Z"
weight = 39961
keywords = [ "adobe", "connect" ]
aliases = [ "/questions/39961" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [ID Adobe Connect traffic](/questions/39961/id-adobe-connect-traffic)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39961-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39961-score" class="post-score" title="current number of votes">0</div><span id="post-39961-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am looking for a way to filter adobe connect traffic for an ACL. Unfortunately my research is coming up blank for anything other than a destination IP to filter on. I am hoping for a more IP ambiguous and more application specific means of identify AC traffic.</p><p>Thank you in advance for any insight/assistance provided.</p><p>S. Dockham</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-adobe" rel="tag" title="see questions tagged &#39;adobe&#39;">adobe</span> <span class="post-tag tag-link-connect" rel="tag" title="see questions tagged &#39;connect&#39;">connect</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>19 Feb '15, 11:37</strong></p><img src="https://secure.gravatar.com/avatar/54fa66be39ffbdc4a4e0d496d063a10c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sdockham0712&#39;s gravatar image" /><p><span>sdockham0712</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sdockham0712 has no accepted answers">0%</span></p></div></div><div id="comments-container-39961" class="comments-container"></div><div id="comment-tools-39961" class="comment-tools"></div><div class="clear"></div><div id="comment-39961-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="40030"></span>

<div id="answer-container-40030" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-40030-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-40030-score" class="post-score" title="current number of votes">0</div><span id="post-40030-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>I am looking for a way to filter adobe connect traffic <strong>for an ACL</strong>. Unfortunately my research is coming up blank for anything <strong>other than a destination IP</strong> to filter on.</p></blockquote><p>Erm... If you are going to build an ACL (access control list), what other parameters could you specify besides <strong>destination IP</strong> and <strong>destination port</strong>? I doubt there is any router available that is able to build an ACL based on anything like a "Adobe Connect Traffic", especially as Adobe Connect is primarily using HTTPS, and RTMPS (or the unencrypted versions). So, I don't see any chance to build an ACL for that kind of traffic.</p><p>Without any further information (what are really trying to do), it's impossible to give any meaningful answer.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Feb '15, 09:40</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-40030" class="comments-container"></div><div id="comment-tools-40030" class="comment-tools"></div><div class="clear"></div><div id="comment-40030-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

