+++
type = "question"
title = "Host and VM capture gives only one direction package"
description = '''Hi, I am running a server in VirtualBox and a Host running client. Opening wireshark in the host and interface is：Virtual box host only network. However, wireshark only shows packages that source from VM but not any from Host. How to solve this problem? I wanna see packages source from Host too. Tha...'''
date = "2016-08-16T21:49:00Z"
lastmod = "2016-08-17T03:31:00Z"
weight = 54890
keywords = [ "virtualbox", "wireshark" ]
aliases = [ "/questions/54890" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Host and VM capture gives only one direction package](/questions/54890/host-and-vm-capture-gives-only-one-direction-package)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54890-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54890-score" class="post-score" title="current number of votes">0</div><span id="post-54890-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am running a server in VirtualBox and a Host running client. Opening wireshark in the host and interface is：Virtual box host only network. However, wireshark only shows packages that source from VM but not any from Host.</p><p>How to solve this problem? I wanna see packages source from Host too. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-virtualbox" rel="tag" title="see questions tagged &#39;virtualbox&#39;">virtualbox</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Aug '16, 21:49</strong></p><img src="https://secure.gravatar.com/avatar/3c7332bcc28d5a0a0f8ac025c147ccbc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jovons&#39;s gravatar image" /><p><span>jovons</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jovons has no accepted answers">0%</span></p></div></div><div id="comments-container-54890" class="comments-container"><span id="54891"></span><div id="comment-54891" class="comment"><div id="post-54891-score" class="comment-score"></div><div class="comment-text"><p>Do you have some firewall at the guest is active?</p></div><div id="comment-54891-info" class="comment-info"><span class="comment-age">(16 Aug '16, 22:58)</span> <span class="comment-user userinfo">Christian_R</span></div></div><span id="54902"></span><div id="comment-54902" class="comment"><div id="post-54902-score" class="comment-score"></div><div class="comment-text"><p>What OS is running on the host and in the VM?</p></div><div id="comment-54902-info" class="comment-info"><span class="comment-age">(17 Aug '16, 03:31)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-54890" class="comment-tools"></div><div class="clear"></div><div id="comment-54890-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

