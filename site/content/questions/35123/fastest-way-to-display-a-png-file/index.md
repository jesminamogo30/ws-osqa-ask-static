+++
type = "question"
title = "fastest way to display a png file"
description = '''When I look an packet dump I see a GET for a PNG or other image file. What is the fastest way to view and/or download that PNG file? Currently I COPY, prune junk off head and tail, and then prefix the host and paste that into a browser. I would love a way to do this with a single keystroke.'''
date = "2014-08-03T13:10:00Z"
lastmod = "2014-08-03T13:27:00Z"
weight = 35123
keywords = [ "image", "view", "display", "png", "get" ]
aliases = [ "/questions/35123" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [fastest way to display a png file](/questions/35123/fastest-way-to-display-a-png-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35123-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35123-score" class="post-score" title="current number of votes">0</div><span id="post-35123-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I look an packet dump I see a GET for a PNG or other image file. What is the fastest way to view and/or download that PNG file?</p><p>Currently I COPY, prune junk off head and tail, and then prefix the host and paste that into a browser. I would love a way to do this with a single keystroke.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-image" rel="tag" title="see questions tagged &#39;image&#39;">image</span> <span class="post-tag tag-link-view" rel="tag" title="see questions tagged &#39;view&#39;">view</span> <span class="post-tag tag-link-display" rel="tag" title="see questions tagged &#39;display&#39;">display</span> <span class="post-tag tag-link-png" rel="tag" title="see questions tagged &#39;png&#39;">png</span> <span class="post-tag tag-link-get" rel="tag" title="see questions tagged &#39;get&#39;">get</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Aug '14, 13:10</strong></p><img src="https://secure.gravatar.com/avatar/5fbd015a1887895b5162f7ecea7ae54b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="RoedyGreen&#39;s gravatar image" /><p><span>RoedyGreen</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="RoedyGreen has no accepted answers">0%</span></p></div></div><div id="comments-container-35123" class="comments-container"></div><div id="comment-tools-35123" class="comment-tools"></div><div class="clear"></div><div id="comment-35123-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="35124"></span>

<div id="answer-container-35124" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35124-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35124-score" class="post-score" title="current number of votes">1</div><span id="post-35124-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Fastest answer ;-)</p><blockquote><p>File -&gt; Export Objects -&gt; HTTP</p></blockquote><p>then select the PNG and click "Save as". Hint: Sometimes it takes a few seconds to find all objects in a large capture file.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Aug '14, 13:13</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Aug '14, 13:14</strong> </span></p></div></div><div id="comments-container-35124" class="comments-container"><span id="35125"></span><div id="comment-35125" class="comment"><div id="post-35125-score" class="comment-score">1</div><div class="comment-text"><p>or, if you have to select and extract a lot of files you could use tools like NetworkMiner: <a href="http://www.netresec.com/?page=NetworkMiner">http://www.netresec.com/?page=NetworkMiner</a></p></div><div id="comment-35125-info" class="comment-info"><span class="comment-age">(03 Aug '14, 13:27)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-35124" class="comment-tools"></div><div class="clear"></div><div id="comment-35124-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

