+++
type = "question"
title = "Client &amp;  Access Point association handshake sample"
description = '''Dear Sirs, My adapter does not support &quot;monitor&quot; mode and therefore I cannot capture the beacon packets and the association of a client to an AP to join a LAN. Is this something I could request from you? I basically want to see PC A detect the wifi and join the network. Thank you in advance. Okie'''
date = "2011-03-01T12:29:00Z"
lastmod = "2011-03-03T02:10:00Z"
weight = 2618
keywords = [ "beacon", "wifi", "handshake", "association" ]
aliases = [ "/questions/2618" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Client & Access Point association handshake sample](/questions/2618/client-access-point-association-handshake-sample)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2618-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2618-score" class="post-score" title="current number of votes">0</div><span id="post-2618-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Dear Sirs,</p><p>My adapter does not support "monitor" mode and therefore I cannot capture the beacon packets and the association of a client to an AP to join a LAN.</p><p>Is this something I could request from you? I basically want to see PC A detect the wifi and join the network.</p><p>Thank you in advance.</p><p>Okie</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-beacon" rel="tag" title="see questions tagged &#39;beacon&#39;">beacon</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span> <span class="post-tag tag-link-handshake" rel="tag" title="see questions tagged &#39;handshake&#39;">handshake</span> <span class="post-tag tag-link-association" rel="tag" title="see questions tagged &#39;association&#39;">association</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Mar '11, 12:29</strong></p><img src="https://secure.gravatar.com/avatar/0862ce5532fd177eb5219cb6dc6f17be?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="okie&#39;s gravatar image" /><p><span>okie</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="okie has no accepted answers">0%</span></p></div></div><div id="comments-container-2618" class="comments-container"></div><div id="comment-tools-2618" class="comment-tools"></div><div class="clear"></div><div id="comment-2618-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2644"></span>

<div id="answer-container-2644" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2644-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2644-score" class="post-score" title="current number of votes">0</div><span id="post-2644-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you want some sample traces google for "sample trace files" and proceed to the links, e.g. http://wiki.wireshark.org/SampleCaptures</p><p>If you wanna have a sample 802.11 client handshake trace including beacon frame, probe, auth, and association - there are some nice examples on aircrack-ng.</p><p>Try http://download.aircrack-ng.org/wiki-files/other/wpa.full.cap as an example</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Mar '11, 02:10</strong></p><img src="https://secure.gravatar.com/avatar/36b41326bff63eb5ad73a0436914e05c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Landi&#39;s gravatar image" /><p><span>Landi</span><br />
<span class="score" title="2269 reputation points"><span>2.3k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="42 badges"><span class="bronze">●</span><span class="badgecount">42</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Landi has 28 accepted answers">28%</span></p></div></div><div id="comments-container-2644" class="comments-container"></div><div id="comment-tools-2644" class="comment-tools"></div><div class="clear"></div><div id="comment-2644-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

