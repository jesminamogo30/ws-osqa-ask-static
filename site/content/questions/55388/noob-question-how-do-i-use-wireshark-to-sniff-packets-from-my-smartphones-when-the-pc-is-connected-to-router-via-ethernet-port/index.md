+++
type = "question"
title = "Noob Question. How do I use wireshark to sniff packets from my smartphones when the PC is connected to router via Ethernet port"
description = '''My PC is connected to D-Link router via ethernet cable while my android phone is connected to the same router via wifi. How can i see the packet from android phone on my pc. I used to use fiddler. But have no idea about how to do it here. PLEASE help'''
date = "2016-09-07T23:55:00Z"
lastmod = "2016-09-08T00:24:00Z"
weight = 55388
keywords = [ "router", "android", "wifi" ]
aliases = [ "/questions/55388" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Noob Question. How do I use wireshark to sniff packets from my smartphones when the PC is connected to router via Ethernet port](/questions/55388/noob-question-how-do-i-use-wireshark-to-sniff-packets-from-my-smartphones-when-the-pc-is-connected-to-router-via-ethernet-port)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55388-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55388-score" class="post-score" title="current number of votes">0</div><span id="post-55388-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My PC is connected to D-Link router via ethernet cable while my android phone is connected to the same router via wifi. How can i see the packet from android phone on my pc. I used to use fiddler. But have no idea about how to do it here. PLEASE help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-router" rel="tag" title="see questions tagged &#39;router&#39;">router</span> <span class="post-tag tag-link-android" rel="tag" title="see questions tagged &#39;android&#39;">android</span> <span class="post-tag tag-link-wifi" rel="tag" title="see questions tagged &#39;wifi&#39;">wifi</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Sep '16, 23:55</strong></p><img src="https://secure.gravatar.com/avatar/bbe634a2f6507ee8463bf05fbe7da81a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Katsuga50&#39;s gravatar image" /><p><span>Katsuga50</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Katsuga50 has no accepted answers">0%</span></p></div></div><div id="comments-container-55388" class="comments-container"></div><div id="comment-tools-55388" class="comment-tools"></div><div class="clear"></div><div id="comment-55388-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="55389"></span>

<div id="answer-container-55389" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55389-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55389-score" class="post-score" title="current number of votes">0</div><span id="post-55389-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>No way. You can take one of the following ways:</p><ul><li><p>sniff the WiFi directly (using a WLAN adaptor of the PC which is not an easy task),</p></li><li><p>sniff at the D-Link itself</p></li><li><p>sniff on D-Link's WAN interface (i.e. at the Ethernet connection between the D-Link and the modem).</p></li></ul><p><a href="https://wiki.wireshark.org/CaptureSetup">This page</a> and many similar Questions on this site explain the details.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Sep '16, 00:24</strong></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sindy has 110 accepted answers">24%</span></p></div></div><div id="comments-container-55389" class="comments-container"></div><div id="comment-tools-55389" class="comment-tools"></div><div class="clear"></div><div id="comment-55389-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

