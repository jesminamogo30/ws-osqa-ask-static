+++
type = "question"
title = "Use for anti-network-flooding"
description = '''So, I have it downloaded and all, my friend told me to get it to prevent people from flooding my network and host booting my internet offline. But I can not figure out how to do it, and or set it up to do that. Could someone help me out? I&#x27;m confused.'''
date = "2011-07-03T16:42:00Z"
lastmod = "2011-07-03T18:50:00Z"
weight = 4896
keywords = [ "ip-flooding", "anti-hostbooting", "help" ]
aliases = [ "/questions/4896" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Use for anti-network-flooding](/questions/4896/use-for-anti-network-flooding)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4896-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4896-score" class="post-score" title="current number of votes">0</div><span id="post-4896-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>So, I have it downloaded and all, my friend told me to get it to prevent people from flooding my network and host booting my internet offline. But I can not figure out how to do it, and or set it up to do that. Could someone help me out? I'm confused.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ip-flooding" rel="tag" title="see questions tagged &#39;ip-flooding&#39;">ip-flooding</span> <span class="post-tag tag-link-anti-hostbooting" rel="tag" title="see questions tagged &#39;anti-hostbooting&#39;">anti-hostbooting</span> <span class="post-tag tag-link-help" rel="tag" title="see questions tagged &#39;help&#39;">help</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Jul '11, 16:42</strong></p><img src="https://secure.gravatar.com/avatar/766221435ead76da5c870b4a372b1782?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="numbers%20aT&#39;s gravatar image" /><p><span>numbers aT</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="numbers aT has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Jul '11, 18:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-4896" class="comments-container"></div><div id="comment-tools-4896" class="comment-tools"></div><div class="clear"></div><div id="comment-4896-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4897"></span>

<div id="answer-container-4897" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4897-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4897-score" class="post-score" title="current number of votes">2</div><span id="post-4897-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>What your friend should have told you is to "get it to watch what traffic is going on your network, so you can see who's flooding your network".</p><p>In this context, Wireshark is not a security fence, it's a hidden security camera. A security fence might prevent people from breaking into your house; a hidden security camera will just let you see pictures of whoever's breaking into your house after it happens. If your friend thinks Wireshark can, by itself, prevent people from flooding your network, he or she is mistaken.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Jul '11, 18:50</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-4897" class="comments-container"></div><div id="comment-tools-4897" class="comment-tools"></div><div class="clear"></div><div id="comment-4897-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

