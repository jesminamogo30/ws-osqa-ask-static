+++
type = "question"
title = "I cannot connect to remote server"
description = '''I am working on a webserver trying to update my website. Periodically and randomly I can no longer connect and the page times out. I have run a wireshark trace and the info comes back with black and grey colored rows. I have saved the capture to a file, but I don&#x27;t know how to interpret it. I can re...'''
date = "2015-06-29T11:47:00Z"
lastmod = "2015-06-29T15:20:00Z"
weight = 43681
keywords = [ "bad-tcp" ]
aliases = [ "/questions/43681" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [I cannot connect to remote server](/questions/43681/i-cannot-connect-to-remote-server)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-43681-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-43681-score" class="post-score" title="current number of votes">0</div><span id="post-43681-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I am working on a webserver trying to update my website. Periodically and randomly I can no longer connect and the page times out. I have run a wireshark trace and the info comes back with black and grey colored rows. I have saved the capture to a file, but I don't know how to interpret it. I can resolve the problem by restarting the computer, but I would like to know how to fix the problem without restarting the computer. Can anyone help me if I send the capture?</p><p>Thanks much!</p><p>Jim Pridemore</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bad-tcp" rel="tag" title="see questions tagged &#39;bad-tcp&#39;">bad-tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jun '15, 11:47</strong></p><img src="https://secure.gravatar.com/avatar/06b265c3a05c3d0e2d4b06475b1aac61?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Pridemore&#39;s gravatar image" /><p><span>Jim Pridemore</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Pridemore has no accepted answers">0%</span></p></div></div><div id="comments-container-43681" class="comments-container"><span id="43688"></span><div id="comment-43688" class="comment"><div id="post-43688-score" class="comment-score"></div><div class="comment-text"><p>Can you share us a capture in a publicly accessible spot, e.g. CloudShark, Google Drive, Dropbox? Also you can use TraceWrangler to anonymize your capture file and then post the anonymized one.</p></div><div id="comment-43688-info" class="comment-info"><span class="comment-age">(29 Jun '15, 15:20)</span> <span class="comment-user userinfo">Christian_R</span></div></div></div><div id="comment-tools-43681" class="comment-tools"></div><div class="clear"></div><div id="comment-43681-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

