+++
type = "question"
title = "Why I can&#x27;t visit this site?"
description = '''Hello. I can&#x27;t visit &quot;miniclip.com&quot; and I captured some packets via Wireshark. Can anyone offer help? http://wikisend.com/download/667542/miniclip.pcapng Thank you.'''
date = "2017-05-29T07:21:00Z"
lastmod = "2017-05-29T07:21:00Z"
weight = 61685
keywords = [ "website", "blocked" ]
aliases = [ "/questions/61685" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Why I can't visit this site?](/questions/61685/why-i-cant-visit-this-site)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61685-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61685-score" class="post-score" title="current number of votes">0</div><span id="post-61685-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello. I can't visit "miniclip.com" and I captured some packets via Wireshark. Can anyone offer help? <a href="http://wikisend.com/download/667542/miniclip.pcapng">http://wikisend.com/download/667542/miniclip.pcapng</a></p><p>Thank you.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-website" rel="tag" title="see questions tagged &#39;website&#39;">website</span> <span class="post-tag tag-link-blocked" rel="tag" title="see questions tagged &#39;blocked&#39;">blocked</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 May '17, 07:21</strong></p><img src="https://secure.gravatar.com/avatar/1f1d393403ea997213960ee852d8f897?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="hack3rcon&#39;s gravatar image" /><p><span>hack3rcon</span><br />
<span class="score" title="11 reputation points">11</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="7 badges"><span class="silver">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="hack3rcon has no accepted answers">0%</span></p></div></div><div id="comments-container-61685" class="comments-container"></div><div id="comment-tools-61685" class="comment-tools"></div><div class="clear"></div><div id="comment-61685-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

