+++
type = "question"
title = "Data Row in display pane!"
description = '''After updating a plugin, I noticed that I have a new row in display pane in wireshark(As shown below). I want to know why it shows now, i&#x27;m not sure where it is coming from. Can someone explain? Collapsed: +Data (154 bytes)  Expanded: -Data (154 bytes)  Data: 00000400000081...  [Length: 154] '''
date = "2015-05-20T08:58:00Z"
lastmod = "2015-05-20T09:06:00Z"
weight = 42583
keywords = [ "wireshark" ]
aliases = [ "/questions/42583" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Data Row in display pane!](/questions/42583/data-row-in-display-pane)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42583-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42583-score" class="post-score" title="current number of votes">0</div><span id="post-42583-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>After updating a plugin, I noticed that I have a new row in display pane in wireshark(As shown below). I want to know why it shows now, i'm not sure where it is coming from. Can someone explain?</p><pre><code>Collapsed:
+Data (154 bytes)

Expanded:
-Data (154 bytes)
   Data: 00000400000081...
   [Length: 154]</code></pre></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 May '15, 08:58</strong></p><img src="https://secure.gravatar.com/avatar/42f084d62348c04d00bd67b129116cc4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="XQW1123&#39;s gravatar image" /><p><span>XQW1123</span><br />
<span class="score" title="46 reputation points">46</span><span title="8 badges"><span class="badge1">●</span><span class="badgecount">8</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="XQW1123 has no accepted answers">0%</span></p></div></div><div id="comments-container-42583" class="comments-container"></div><div id="comment-tools-42583" class="comment-tools"></div><div class="clear"></div><div id="comment-42583-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="42584"></span>

<div id="answer-container-42584" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-42584-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-42584-score" class="post-score" title="current number of votes">0</div><span id="post-42584-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="XQW1123 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It would be handy to show more context, but I suspect this is data in the frame that isn't dissected. Is it part of your protocol that should be dissected by your plugin?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>20 May '15, 09:06</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-42584" class="comments-container"></div><div id="comment-tools-42584" class="comment-tools"></div><div class="clear"></div><div id="comment-42584-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

