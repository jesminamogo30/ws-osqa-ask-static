+++
type = "question"
title = "finding IP pairs that exchange most bytes"
description = '''When I go to statistics and endpoints, it shows me the bytes for each IP. How do I find the IP pair that exchanges the most bytes, though ?'''
date = "2015-01-24T05:55:00Z"
lastmod = "2015-01-24T05:59:00Z"
weight = 39381
keywords = [ "bytes" ]
aliases = [ "/questions/39381" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [finding IP pairs that exchange most bytes](/questions/39381/finding-ip-pairs-that-exchange-most-bytes)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39381-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39381-score" class="post-score" title="current number of votes">0</div><span id="post-39381-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I go to statistics and endpoints, it shows me the bytes for each IP. How do I find the IP pair that exchanges the most bytes, though ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bytes" rel="tag" title="see questions tagged &#39;bytes&#39;">bytes</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Jan '15, 05:55</strong></p><img src="https://secure.gravatar.com/avatar/d33eb2b57ff4dc439fa8f9b3b8eceba6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Xandi&#39;s gravatar image" /><p><span>Xandi</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Xandi has no accepted answers">0%</span></p></div></div><div id="comments-container-39381" class="comments-container"></div><div id="comment-tools-39381" class="comment-tools"></div><div class="clear"></div><div id="comment-39381-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39382"></span>

<div id="answer-container-39382" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39382-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39382-score" class="post-score" title="current number of votes">3</div><span id="post-39382-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Use Statistics -&gt; Conversations instead, sort by Byte column.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>24 Jan '15, 05:59</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-39382" class="comments-container"></div><div id="comment-tools-39382" class="comment-tools"></div><div class="clear"></div><div id="comment-39382-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

