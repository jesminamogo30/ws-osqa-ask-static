+++
type = "question"
title = "Tshark tcp conv table"
description = '''Hi, can anyone tell me if it&#x27;s possible to add any field to a tshark tcp conv table? Specifically I&#x27;m interested in fix.sendercompid and fix.targetcompid. I can display the values in per-packet output but would prefer to generate a table with a single row per tcp connection.  Thanks. '''
date = "2017-05-27T13:59:00Z"
lastmod = "2017-05-27T13:59:00Z"
weight = 61662
keywords = [ "fix", "tshark", "conv" ]
aliases = [ "/questions/61662" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Tshark tcp conv table](/questions/61662/tshark-tcp-conv-table)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61662-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61662-score" class="post-score" title="current number of votes">0</div><span id="post-61662-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, can anyone tell me if it's possible to add any field to a tshark tcp conv table?</p><p>Specifically I'm interested in fix.sendercompid and fix.targetcompid. I can display the values in per-packet output but would prefer to generate a table with a single row per tcp connection.</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-fix" rel="tag" title="see questions tagged &#39;fix&#39;">fix</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-conv" rel="tag" title="see questions tagged &#39;conv&#39;">conv</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 May '17, 13:59</strong></p><img src="https://secure.gravatar.com/avatar/31534d84848dcc810494dce199a020f4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Timchampion&#39;s gravatar image" /><p><span>Timchampion</span><br />
<span class="score" title="6 reputation points">6</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Timchampion has no accepted answers">0%</span></p></div></div><div id="comments-container-61662" class="comments-container"></div><div id="comment-tools-61662" class="comment-tools"></div><div class="clear"></div><div id="comment-61662-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

