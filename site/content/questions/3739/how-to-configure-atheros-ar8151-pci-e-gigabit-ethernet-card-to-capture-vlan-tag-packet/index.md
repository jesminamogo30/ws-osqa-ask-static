+++
type = "question"
title = "How to configure Atheros AR8151 PCI-E Gigabit Ethernet card to capture vlan tag packet?"
description = '''The vlan tag is stripped when I using Atheros AR8151 PCI-E Gigabit Ethernet card . Could you please tell me how to configure Ethernet card for capturing vlan tag packet?'''
date = "2011-04-27T02:50:00Z"
lastmod = "2014-12-31T00:28:00Z"
weight = 3739
keywords = [ "vlan", "atheros", "configuration" ]
aliases = [ "/questions/3739" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [How to configure Atheros AR8151 PCI-E Gigabit Ethernet card to capture vlan tag packet?](/questions/3739/how-to-configure-atheros-ar8151-pci-e-gigabit-ethernet-card-to-capture-vlan-tag-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3739-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3739-score" class="post-score" title="current number of votes">0</div><span id="post-3739-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>The vlan tag is stripped when I using Atheros AR8151 PCI-E Gigabit Ethernet card . Could you please tell me how to configure Ethernet card for capturing vlan tag packet?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-vlan" rel="tag" title="see questions tagged &#39;vlan&#39;">vlan</span> <span class="post-tag tag-link-atheros" rel="tag" title="see questions tagged &#39;atheros&#39;">atheros</span> <span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>27 Apr '11, 02:50</strong></p><img src="https://secure.gravatar.com/avatar/d6de905eb175878338b0714e4ba690b5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasmine&#39;s gravatar image" /><p><span>Jasmine</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasmine has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>05 May '11, 19:25</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-3739" class="comments-container"><span id="3971"></span><div id="comment-3971" class="comment"><div id="post-3971-score" class="comment-score"></div><div class="comment-text"><p>What operating system are you using? The answer may depend on the OS.</p></div><div id="comment-3971-info" class="comment-info"><span class="comment-age">(05 May '11, 16:41)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="5302"></span><div id="comment-5302" class="comment"><div id="post-5302-score" class="comment-score"></div><div class="comment-text"><p>I am also waiting for an answer. Unfortunately my laptop has this NIC card as well.</p><p>Please if you have the answer, pls post here.</p></div><div id="comment-5302-info" class="comment-info"><span class="comment-age">(27 Jul '11, 02:32)</span> <span class="comment-user userinfo">tikili</span></div></div><span id="13939"></span><div id="comment-13939" class="comment"><div id="post-13939-score" class="comment-score"></div><div class="comment-text"><p>I've got the same issue. VLAN's get stripped off. I'm using Windows 7 64bit.</p></div><div id="comment-13939-info" class="comment-info"><span class="comment-age">(28 Aug '12, 08:45)</span> <span class="comment-user userinfo">connect3D</span></div></div></div><div id="comment-tools-3739" class="comment-tools"></div><div class="clear"></div><div id="comment-3739-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="19836"></span>

<div id="answer-container-19836" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-19836-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-19836-score" class="post-score" title="current number of votes">-1</div><span id="post-19836-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>in windows 7 x64 and atheros ar8151 pci-e gigabit ethernet controller do : start &gt; type cmd &gt; RClick on cmd icon, "Run as administrator" &gt; go to = C:\Windows\System32&gt; type : sc.exe start npf</p><p>then restart Wireshark ;)</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Mar '13, 04:56</strong></p><img src="https://secure.gravatar.com/avatar/717907e2f42a12a4836b1482db45482f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="HamedTirdad&#39;s gravatar image" /><p><span>HamedTirdad</span><br />
<span class="score" title="10 reputation points">10</span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="HamedTirdad has no accepted answers">0%</span></p></div></div><div id="comments-container-19836" class="comments-container"><span id="38816"></span><div id="comment-38816" class="comment"><div id="post-38816-score" class="comment-score"></div><div class="comment-text"><p>It doesn't work for me. Did anyone try this on Atheros interface?</p></div><div id="comment-38816-info" class="comment-info"><span class="comment-age">(31 Dec '14, 00:28)</span> <span class="comment-user userinfo">ortep</span></div></div></div><div id="comment-tools-19836" class="comment-tools"></div><div class="clear"></div><div id="comment-19836-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

