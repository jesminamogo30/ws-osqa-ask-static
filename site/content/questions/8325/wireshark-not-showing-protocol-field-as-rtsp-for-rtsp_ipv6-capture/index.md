+++
type = "question"
title = "Wireshark not showing protocol field   as &quot;RTSP&quot;  for RTSP_IPv6 capture"
description = '''I had opened RTSP_IPv6 capture using wireshark and it shows protocol field as &quot;TCP&quot; instead of &quot;RTSP&quot;. Can anyone explain me why its happening and also Info field saying that &quot;TCP segment reassembeld PDU&quot;.. Uploaded Capture image.Refer below link. http://www.flickr.com/photos/74113043@N06/6678771333...'''
date = "2012-01-11T06:08:00Z"
lastmod = "2012-01-11T06:08:00Z"
weight = 8325
keywords = [ "streaming", "rtsp", "ipv6" ]
aliases = [ "/questions/8325" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark not showing protocol field as "RTSP" for RTSP\_IPv6 capture](/questions/8325/wireshark-not-showing-protocol-field-as-rtsp-for-rtsp_ipv6-capture)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8325-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8325-score" class="post-score" title="current number of votes">0</div><span id="post-8325-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I had opened RTSP_IPv6 capture using wireshark and it shows protocol field as "TCP" instead of "RTSP". Can anyone explain me why its happening and also Info field saying that "TCP segment reassembeld PDU"..</p><p>Uploaded Capture image.Refer below link.</p><p><a href="http://www.flickr.com/photos/74113043@N06/6678771333/in/photostream/">http://www.flickr.com/photos/<span class="__cf_email__" data-cfemail="b28586838381828681f2fc8284">[email protected]</span>/6678771333/in/photostream/</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-streaming" rel="tag" title="see questions tagged &#39;streaming&#39;">streaming</span> <span class="post-tag tag-link-rtsp" rel="tag" title="see questions tagged &#39;rtsp&#39;">rtsp</span> <span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '12, 06:08</strong></p><img src="https://secure.gravatar.com/avatar/01febacc45af8ecf743c4f575d428326?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JK7&#39;s gravatar image" /><p><span>JK7</span><br />
<span class="score" title="31 reputation points">31</span><span title="11 badges"><span class="badge1">●</span><span class="badgecount">11</span></span><span title="12 badges"><span class="silver">●</span><span class="badgecount">12</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JK7 has no accepted answers">0%</span></p></div></div><div id="comments-container-8325" class="comments-container"></div><div id="comment-tools-8325" class="comment-tools"></div><div class="clear"></div><div id="comment-8325-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

