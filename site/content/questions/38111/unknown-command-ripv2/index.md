+++
type = "question"
title = "Unknown command Ripv2"
description = '''I have a simple topology of 2 Cisco 3600 routers simulated in gns3 connected via a serial link with hdlc encapsulation and using only triggered updates (no periodic ones) and I can not capture understandable Ripv2 packets. I only can capture packet with information &quot;unknown command 9&quot;, &quot;unknown comm...'''
date = "2014-11-24T13:36:00Z"
lastmod = "2014-11-24T13:36:00Z"
weight = 38111
keywords = [ "gns3", "cisco", "unknown", "rip", "wireshark" ]
aliases = [ "/questions/38111" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Unknown command Ripv2](/questions/38111/unknown-command-ripv2)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38111-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38111-score" class="post-score" title="current number of votes">0</div><span id="post-38111-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a simple topology of 2 Cisco 3600 routers simulated in gns3 connected via a serial link with hdlc encapsulation and using only triggered updates (no periodic ones) and I can not capture understandable Ripv2 packets. I only can capture packet with information "unknown command 9", "unknown command 10" and "unknown command 11" witch I think stands for "Update Request", "Update Response" and "Update Acknowledge" of the RIPv2 specification but no further details in each packet. I want to know if there is a problem with Wireshark handling Cisco triggered updates or the Cisco implementation of Ripv2 is done different from the specifications on the RFCs.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gns3" rel="tag" title="see questions tagged &#39;gns3&#39;">gns3</span> <span class="post-tag tag-link-cisco" rel="tag" title="see questions tagged &#39;cisco&#39;">cisco</span> <span class="post-tag tag-link-unknown" rel="tag" title="see questions tagged &#39;unknown&#39;">unknown</span> <span class="post-tag tag-link-rip" rel="tag" title="see questions tagged &#39;rip&#39;">rip</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '14, 13:36</strong></p><img src="https://secure.gravatar.com/avatar/685a7d70763d3f4f1e117b8d28315e65?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Mragrid&#39;s gravatar image" /><p><span>Mragrid</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Mragrid has no accepted answers">0%</span></p></div></div><div id="comments-container-38111" class="comments-container"></div><div id="comment-tools-38111" class="comment-tools"></div><div class="clear"></div><div id="comment-38111-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

