+++
type = "question"
title = "Incorrect average packet size"
description = '''Hi, Something looks wrong in the Statistics -&amp;gt; Packet Lengths command: the Average Packet Length appears to be consistently reported as twice its true value. I can&#x27;t upload the photo of the trace I&#x27;m looking at (I don&#x27;t seem to have enough &quot;karma&quot;), but take any trace of your own and check it out...'''
date = "2014-10-03T09:01:00Z"
lastmod = "2014-10-04T18:14:00Z"
weight = 36821
keywords = [ "average", "statistics", "length", "packet" ]
aliases = [ "/questions/36821" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Incorrect average packet size](/questions/36821/incorrect-average-packet-size)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36821-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36821-score" class="post-score" title="current number of votes">0</div><span id="post-36821-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>Something looks wrong in the Statistics -&gt; Packet Lengths command: the Average Packet Length appears to be consistently reported as twice its true value. I can't upload the photo of the trace I'm looking at (I don't seem to have enough "karma"), but take any trace of your own and check it out -- especially if you're filtering the trace down to a small number of frames displayed.</p><p>The Statistics -&gt; Summary window will show one value for the average size of the displayed packets, and it'll be correct. The Statistics -&gt; Packet Lengths window will show twice the value, and it'll be wrong. And it'll be nonsense compared to the histogram data (counts/sizes by bin).</p><p>What's up?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-average" rel="tag" title="see questions tagged &#39;average&#39;">average</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-length" rel="tag" title="see questions tagged &#39;length&#39;">length</span> <span class="post-tag tag-link-packet" rel="tag" title="see questions tagged &#39;packet&#39;">packet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Oct '14, 09:01</strong></p><img src="https://secure.gravatar.com/avatar/e933aa1bf07979066acd8f896fb8ee29?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dekaufman&#39;s gravatar image" /><p><span>dekaufman</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dekaufman has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>03 Oct '14, 13:22</strong> </span></p></div></div><div id="comments-container-36821" class="comments-container"></div><div id="comment-tools-36821" class="comment-tools"></div><div class="clear"></div><div id="comment-36821-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="36843"></span>

<div id="answer-container-36843" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36843-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36843-score" class="post-score" title="current number of votes">0</div><span id="post-36843-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It looks like you've encountered a bug. I suggest submitting a bug report at the <a href="https://bugs.wireshark.org/bugzilla/">Wireshark bugzilla</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>04 Oct '14, 18:14</strong></p><img src="https://secure.gravatar.com/avatar/071fe61f64868d98bdf4eb060b63b6ca?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jim%20Aragon&#39;s gravatar image" /><p><span>Jim Aragon</span><br />
<span class="score" title="7187 reputation points"><span>7.2k</span></span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="118 badges"><span class="bronze">●</span><span class="badgecount">118</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jim Aragon has 70 accepted answers">24%</span></p></div></div><div id="comments-container-36843" class="comments-container"></div><div id="comment-tools-36843" class="comment-tools"></div><div class="clear"></div><div id="comment-36843-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

