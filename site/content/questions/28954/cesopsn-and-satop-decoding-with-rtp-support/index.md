+++
type = "question"
title = "CESOPSN and SATOP decoding with RTP support"
description = '''Is there any way to decode CESOPSN andn SATOP packets tah includes RTP headres? The option showed in the &quot;decode as&quot; menu is CSOPSN (no RTp support) and SATOP (no RTP support). How can I see the structure of one of these types of packets with wireshark?'''
date = "2014-01-16T01:53:00Z"
lastmod = "2014-01-16T01:53:00Z"
weight = 28954
keywords = [ "pseudowires" ]
aliases = [ "/questions/28954" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [CESOPSN and SATOP decoding with RTP support](/questions/28954/cesopsn-and-satop-decoding-with-rtp-support)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28954-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28954-score" class="post-score" title="current number of votes">0</div><span id="post-28954-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is there any way to decode CESOPSN andn SATOP packets tah includes RTP headres? The option showed in the "decode as" menu is CSOPSN (no RTp support) and SATOP (no RTP support). How can I see the structure of one of these types of packets with wireshark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-pseudowires" rel="tag" title="see questions tagged &#39;pseudowires&#39;">pseudowires</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jan '14, 01:53</strong></p><img src="https://secure.gravatar.com/avatar/c5864235dd4c344b614a8b7707c41968?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="123wshark&#39;s gravatar image" /><p><span>123wshark</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="123wshark has no accepted answers">0%</span></p></div></div><div id="comments-container-28954" class="comments-container"></div><div id="comment-tools-28954" class="comment-tools"></div><div class="clear"></div><div id="comment-28954-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

