+++
type = "question"
title = "Cant Resize Columns in Graph Analysis Window"
description = '''Hi Folks, I&#x27;m new w/ WS. Learning how to use it to decode SIP session problems. I love the graph showing the dance that occurs between the endpoint and the Registrar/Proxy. I am not able to resize its columns even though the &quot;middle&quot; separator has those traction bumps on it and when the mouse hovers...'''
date = "2012-01-13T19:33:00Z"
lastmod = "2012-01-14T11:26:00Z"
weight = 8382
keywords = [ "color", "text", "resize", "columns", "inverted" ]
aliases = [ "/questions/8382" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [Cant Resize Columns in Graph Analysis Window](/questions/8382/cant-resize-columns-in-graph-analysis-window)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8382-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8382-score" class="post-score" title="current number of votes">0</div><span id="post-8382-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi Folks,</p><p>I'm new w/ WS. Learning how to use it to decode SIP session problems. I love the graph showing the dance that occurs between the endpoint and the Registrar/Proxy. I am not able to resize its columns even though the "middle" separator has those traction bumps on it and when the mouse hovers over it, I get a left/right arrow icon. Can anyone tell me what I am missing? I am also unable to change the space between lines in the "ladder" - they are pretty well "squished" (a technical term) together. I tried Version 1.6.5 (SVN Rev 40429 from /trunk-1.6) and the 1.7.0 prerelease. I am running on Mac OS 10.6.8 (Darwin 10.8.0).</p><p>Also, is there a way to either change the color of selected text (presently light green) or prevent the text from inverting when selected? My eyes don't see the white text contrasted with the light green very well.</p><p>Thanks for any help you can provide</p><p>Phil</p><p>San Diego, CA</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-color" rel="tag" title="see questions tagged &#39;color&#39;">color</span> <span class="post-tag tag-link-text" rel="tag" title="see questions tagged &#39;text&#39;">text</span> <span class="post-tag tag-link-resize" rel="tag" title="see questions tagged &#39;resize&#39;">resize</span> <span class="post-tag tag-link-columns" rel="tag" title="see questions tagged &#39;columns&#39;">columns</span> <span class="post-tag tag-link-inverted" rel="tag" title="see questions tagged &#39;inverted&#39;">inverted</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Jan '12, 19:33</strong></p><img src="https://secure.gravatar.com/avatar/ab9587cb3dcc75bef29baa376223171f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="PhilWells&#39;s gravatar image" /><p><span>PhilWells</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="PhilWells has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>13 Jan '12, 19:35</strong> </span></p></div></div><div id="comments-container-8382" class="comments-container"></div><div id="comment-tools-8382" class="comment-tools"></div><div class="clear"></div><div id="comment-8382-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="8386"></span>

<div id="answer-container-8386" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8386-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8386-score" class="post-score" title="current number of votes">0</div><span id="post-8386-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I guess you're referring to the "Flow Graph" in the statistics menu. As far as I know the graph window is pretty much unconfigurable, so you can only move the divider between the green ladder area and the comment pane to a certain degree (at least on Windows, you can). There is no option to adjust the vertical ladder spacing either.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jan '12, 07:24</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-8386" class="comments-container"></div><div id="comment-tools-8386" class="comment-tools"></div><div class="clear"></div><div id="comment-8386-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="8388"></span>

<div id="answer-container-8388" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8388-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8388-score" class="post-score" title="current number of votes">0</div><span id="post-8388-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You should file bugs about those two items in the <a href="http://bugs.wireshark.org/">Wireshark bugzilla</a> (separate items, please; the resizing and the coloration are separate issues).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>14 Jan '12, 11:26</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-8388" class="comments-container"></div><div id="comment-tools-8388" class="comment-tools"></div><div class="clear"></div><div id="comment-8388-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

