+++
type = "question"
title = "Problems with WiFi"
description = '''Posted on Petri but just thought might be worth giving it a shot here. Suddenly my laptop connected to my home WiFi stopped displaying websites. The thing is I can still ping both external IPs and urls. I tried system restore in case some update affected my NIC, deleted temporary internet files, res...'''
date = "2012-11-18T14:33:00Z"
lastmod = "2012-11-18T16:01:00Z"
weight = 16023
keywords = [ "wireless", "rst", "ack" ]
aliases = [ "/questions/16023" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Problems with WiFi](/questions/16023/problems-with-wifi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-16023-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-16023-score" class="post-score" title="current number of votes">0</div><span id="post-16023-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Posted on Petri but just thought might be worth giving it a shot here. Suddenly my laptop connected to my home WiFi stopped displaying websites. The thing is I can still ping both external IPs and urls. I tried system restore in case some update affected my NIC, deleted temporary internet files, reset my AP to factory and reconfigured, enabled WiFi on my router and tested with that to no avail, updated the driver from manufacturer's web site, scanned the laptop with Malwarebytes, disabled the firewall and run in safe mode with networking. Finally I run Wireshark on the laptop and I can see DNS traffic resolving IP but then the server sends RST,ACK for whatever reason. However, I noticed that when the browser connects to a website in it's status bar I can see some transfers going on but then it just hangs on waiting for page. Bizarely some websites manage to actually display some very small pieces of a page that I'm trying to visit like some titles, sometimes even some low-res graphics but that's rare and the page eventually dies. At the same time all works fine via wire. Any ideas?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-rst" rel="tag" title="see questions tagged &#39;rst&#39;">rst</span> <span class="post-tag tag-link-ack" rel="tag" title="see questions tagged &#39;ack&#39;">ack</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Nov '12, 14:33</strong></p><img src="https://secure.gravatar.com/avatar/725114602e51dce3b97b31c0c0189764?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="yaro&#39;s gravatar image" /><p><span>yaro</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="yaro has no accepted answers">0%</span></p></div></div><div id="comments-container-16023" class="comments-container"><span id="16027"></span><div id="comment-16027" class="comment"><div id="post-16027-score" class="comment-score"></div><div class="comment-text"><p>just by chance: If you are trying to download/view some site via Wifi, are you also connected via wire?</p></div><div id="comment-16027-info" class="comment-info"><span class="comment-age">(18 Nov '12, 16:01)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-16023" class="comment-tools"></div><div class="clear"></div><div id="comment-16023-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

