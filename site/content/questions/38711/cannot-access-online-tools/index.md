+++
type = "question"
title = "Cannot access online tools"
description = '''I&#x27;m new to this site.  I would like to access the IPv4 and IPv6 Connectivity Test listed on the online tools page. However I receive a &quot;Sorry, we only support http at this time (you&#x27;re using https).&quot; error when accessing that tool page. Every page on wireshark.org defaults to https and I cannot get ...'''
date = "2014-12-25T14:07:00Z"
lastmod = "2014-12-27T08:10:00Z"
weight = 38711
keywords = [ "test", "connectivity", "http", "ipv4", "ipv6" ]
aliases = [ "/questions/38711" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Cannot access online tools](/questions/38711/cannot-access-online-tools)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38711-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38711-score" class="post-score" title="current number of votes">0</div><span id="post-38711-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm new to this site.</p><p>I would like to access the IPv4 and IPv6 Connectivity Test listed on the online tools page. However I receive a "Sorry, we only support http at this time (you're using https)." error when accessing that tool page. Every page on wireshark.org defaults to https and I cannot get to an http version of the page in any browser.</p><p>How can I fix this?</p><p>Thanks in advance for your assistance.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-test" rel="tag" title="see questions tagged &#39;test&#39;">test</span> <span class="post-tag tag-link-connectivity" rel="tag" title="see questions tagged &#39;connectivity&#39;">connectivity</span> <span class="post-tag tag-link-http" rel="tag" title="see questions tagged &#39;http&#39;">http</span> <span class="post-tag tag-link-ipv4" rel="tag" title="see questions tagged &#39;ipv4&#39;">ipv4</span> <span class="post-tag tag-link-ipv6" rel="tag" title="see questions tagged &#39;ipv6&#39;">ipv6</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Dec '14, 14:07</strong></p><img src="https://secure.gravatar.com/avatar/5d24736d29b085158a326f68dab289e6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="rkenchel&#39;s gravatar image" /><p><span>rkenchel</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="rkenchel has no accepted answers">0%</span></p></div></div><div id="comments-container-38711" class="comments-container"></div><div id="comment-tools-38711" class="comment-tools"></div><div class="clear"></div><div id="comment-38711-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="38733"></span>

<div id="answer-container-38733" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-38733-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-38733-score" class="post-score" title="current number of votes">1</div><span id="post-38733-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Aparently you are talking about this page:</p><blockquote><p><a href="https://www.wireshark.org/tools/v46status.html">https://www.wireshark.org/tools/v46status.html</a></p></blockquote><p>You are right, it does not work anymore, as it redirects HTTP to HTTPS and then it shows the error message</p><blockquote><p>Sorry, we only support http at this time (you're using https).</p></blockquote><p>Please file a bug report at <a href="https://bugs.wireshark.org">https://bugs.wireshark.org</a> to report this. While you report the bug, you will be asked to choose the product. Please choose <strong>"Web sites"</strong></p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>27 Dec '14, 08:10</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-38733" class="comments-container"></div><div id="comment-tools-38733" class="comment-tools"></div><div class="clear"></div><div id="comment-38733-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

