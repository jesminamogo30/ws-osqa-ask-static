+++
type = "question"
title = "WIRELESS ADAPTER IS NOT DETECTED//Unable to capture wireless(802.11 packets)"
description = '''OS: WIN7 HOME PREMIUM CPU: INTEL CORE I5-450M Memory: 500 GB HDD 4 GB DDR3 MEMORY Adapter: ATHEROS AR5B97 Problem Title: ///WIRELESS ADAPTER IS NOT DETECTED///unable to capture 802.11 packets/// Problem Description:  Dear Sir/Madam I am with Cisco systems and evaluating the demo version of wildpacke...'''
date = "2011-04-17T21:34:00Z"
lastmod = "2011-04-18T00:37:00Z"
weight = 3543
keywords = [ "wireless", "not", "adapter", "detected", "is" ]
aliases = [ "/questions/3543" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [WIRELESS ADAPTER IS NOT DETECTED//Unable to capture wireless(802.11 packets)](/questions/3543/wireless-adapter-is-not-detectedunable-to-capture-wireless80211-packets)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3543-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3543-score" class="post-score" title="current number of votes">0</div><span id="post-3543-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>OS: WIN7 HOME PREMIUM CPU: INTEL CORE I5-450M Memory: 500 GB HDD 4 GB DDR3 MEMORY</p><p>Adapter: ATHEROS AR5B97</p><p>Problem Title: ///WIRELESS ADAPTER IS NOT DETECTED///unable to capture 802.11 packets///</p><p>Problem Description:</p><p>Dear Sir/Madam</p><p>I am with Cisco systems and evaluating the demo version of wildpackets packet capture tool on Windows 7 home premium OS. I am unable to edit the channel number in the 802..11 tab. I also do not see any 802..11 packets.</p><p>Can you please help? i am trying to capture on a Acer laptop with Atheros chip. Th laptop is connected wirelessly to the Access Point when I try to capture the log</p><p>Regards Kiran</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless" rel="tag" title="see questions tagged &#39;wireless&#39;">wireless</span> <span class="post-tag tag-link-not" rel="tag" title="see questions tagged &#39;not&#39;">not</span> <span class="post-tag tag-link-adapter" rel="tag" title="see questions tagged &#39;adapter&#39;">adapter</span> <span class="post-tag tag-link-detected" rel="tag" title="see questions tagged &#39;detected&#39;">detected</span> <span class="post-tag tag-link-is" rel="tag" title="see questions tagged &#39;is&#39;">is</span></div><div id="question-controls" class="post-controls"><div class="community-wiki">This question is marked "community wiki".</div></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Apr '11, 21:34</strong></p><img src="https://secure.gravatar.com/avatar/ca9371c488e16c825dfc61e26062999f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="KIRANBISWAL&#39;s gravatar image" /><p><span>KIRANBISWAL</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="KIRANBISWAL has no accepted answers">0%</span></p></div></div><div id="comments-container-3543" class="comments-container"></div><div id="comment-tools-3543" class="comment-tools"></div><div class="clear"></div><div id="comment-3543-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3544"></span>

<div id="answer-container-3544" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3544-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3544-score" class="post-score" title="current number of votes">0</div><span id="post-3544-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>On a machine running Windows, to capture anything other than packets to and from your host with Wireshark on an 802.11 network, or to capture on a channel other than the channel for the network with which you're associated, you will have to get an <a href="http://www.riverbed.com/us/products/cascade/airpcap.php">AirPcap adapter</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Apr '11, 22:06</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-3544" class="comments-container"><span id="3546"></span><div id="comment-3546" class="comment"><div id="post-3546-score" class="comment-score"></div><div class="comment-text"><p>Hello Guy. thanks for responding. So are you saying wireshark can capture the packets to and from my host? the wireless adapter is not detected at all.</p><p>Does the linux version of wireleshark work any differently?</p><p>How can I capture a log in promiscuous mode where my wireless adapter will not actively connect withh any AP but capture packets by listening on specific channel?</p><p>I used Ethereal before, is Wireshark completely different?</p><p>Thanks for your help</p><p>Kiran</p></div><div id="comment-3546-info" class="comment-info"><span class="comment-age">(17 Apr '11, 23:47)</span> <span class="comment-user userinfo">KIRANBISWAL</span></div></div><span id="3547"></span><div id="comment-3547" class="comment"><div id="post-3547-score" class="comment-score"></div><div class="comment-text"><p>I'm saying Wireshark can, at least for <em>some</em> wireless adapters, capture traffic to and from your host; I don't know which adapters WinPcap recognizes, or why it doesn't recognize all adapters.</p><p>The Linux (and *BSD and Mac OS X) versions of Wireshark work <em>very</em> differently. If you have Wireshark 1.4.0 or later, and libpcap 1.0.0 or later, and Wireshark was built with that version of libpcap, it will have a check box for monitor mode. Otherwise, you'll have to put the adapter into monitor mode yourself, but you'll still be able to use it from Wireshark.</p></div><div id="comment-3547-info" class="comment-info"><span class="comment-age">(18 Apr '11, 00:33)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="3548"></span><div id="comment-3548" class="comment"><div id="post-3548-score" class="comment-score"></div><div class="comment-text"><p>The capture mode you're talking about is monitor mode, not promiscuous mode. WinPcap, and thus Wireshark, don't support monitor mode on Windows. Libpcap 1.0.0 and later, as noted, support monitor mode on Linux, *BSD, and Mac OS X; earlier versions don't support it directly, but if you can put the adapter into monitor mode yourself (which you can't do on Windows) Wireshark can use it.</p></div><div id="comment-3548-info" class="comment-info"><span class="comment-age">(18 Apr '11, 00:34)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div><span id="3549"></span><div id="comment-3549" class="comment"><div id="post-3549-score" class="comment-score"></div><div class="comment-text"><p>"Wireshark" is just the name the program has in version 0.99.2; before that, it was called "Ethereal". The direct support of monitor mode was added in 1.4.0, so the versions called "Ethereal" didn't have that. They, and versions prior to 1.4.0 called "Wireshark", can capture in monitor mode if you put the adapter in monitor mode.</p></div><div id="comment-3549-info" class="comment-info"><span class="comment-age">(18 Apr '11, 00:37)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-3544" class="comment-tools"></div><div class="clear"></div><div id="comment-3544-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

