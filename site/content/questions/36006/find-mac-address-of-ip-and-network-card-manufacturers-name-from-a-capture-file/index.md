+++
type = "question"
title = "Find MAC Address of Ip and Network Card Manufacturer&#x27;s name from a capture file"
description = '''Hi, Please somebody tell me how can i find MAC adress associated with a LAN ip 192.168.1.12 from a capture file? &amp;amp; also, how can i find network card manufacturer&#x27;s name from a capture file? help me out....plzz.... thanks in advance.....'''
date = "2014-09-04T09:34:00Z"
lastmod = "2014-09-04T09:34:00Z"
weight = 36006
keywords = [ "mac", "mac-address", "network" ]
aliases = [ "/questions/36006" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Find MAC Address of Ip and Network Card Manufacturer's name from a capture file](/questions/36006/find-mac-address-of-ip-and-network-card-manufacturers-name-from-a-capture-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-36006-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-36006-score" class="post-score" title="current number of votes">0</div><span id="post-36006-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, Please somebody tell me how can i find MAC adress associated with a LAN ip 192.168.1.12 from a capture file? &amp; also, how can i find network card manufacturer's name from a capture file?</p><p>help me out....plzz....</p><p>thanks in advance.....</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-mac-address" rel="tag" title="see questions tagged &#39;mac-address&#39;">mac-address</span> <span class="post-tag tag-link-network" rel="tag" title="see questions tagged &#39;network&#39;">network</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>04 Sep '14, 09:34</strong></p><img src="https://secure.gravatar.com/avatar/183856450565425bb81d930ae46c1e5e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sandy700&#39;s gravatar image" /><p><span>sandy700</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="sandy700 has no accepted answers">0%</span></p></div></div><div id="comments-container-36006" class="comments-container"></div><div id="comment-tools-36006" class="comment-tools"></div><div class="clear"></div><div id="comment-36006-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

