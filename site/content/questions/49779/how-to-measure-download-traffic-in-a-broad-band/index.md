+++
type = "question"
title = "How to measure download traffic in a broad band?"
description = '''I only want to see how much data download I use every week in my normal broad band before I possibly will change to a Mobile alternative. But I don&#x27;t understand how to sat it up for that use. I get a note that no interface is selected. Where do I enter the interface? I do not even know what it mean ...'''
date = "2016-02-03T08:19:00Z"
lastmod = "2016-02-04T04:07:00Z"
weight = 49779
keywords = [ "broadband", "statistics", "datadownload" ]
aliases = [ "/questions/49779" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [How to measure download traffic in a broad band?](/questions/49779/how-to-measure-download-traffic-in-a-broad-band)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49779-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49779-score" class="post-score" title="current number of votes">0</div><span id="post-49779-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I only want to see how much data download I use every week in my normal broad band before I possibly will change to a Mobile alternative.</p><p>But I don't understand how to sat it up for that use. I get a note that no interface is selected.<br />
Where do I enter the interface? I do not even know what it mean by an interface at this stage.</p><p>I'm using a MacBook Pro and OS 10.11.3</p><p>Would appreciate a simple instruction for this I guess very simple task</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-broadband" rel="tag" title="see questions tagged &#39;broadband&#39;">broadband</span> <span class="post-tag tag-link-statistics" rel="tag" title="see questions tagged &#39;statistics&#39;">statistics</span> <span class="post-tag tag-link-datadownload" rel="tag" title="see questions tagged &#39;datadownload&#39;">datadownload</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Feb '16, 08:19</strong></p><img src="https://secure.gravatar.com/avatar/9b24cb1ee5f7e589f033d883798686d9?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="LZachs&#39;s gravatar image" /><p><span>LZachs</span><br />
<span class="score" title="0 reputation points">0</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="LZachs has no accepted answers">0%</span> </br></p></div></div><div id="comments-container-49779" class="comments-container"></div><div id="comment-tools-49779" class="comment-tools"></div><div class="clear"></div><div id="comment-49779-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="49783"></span>

<div id="answer-container-49783" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49783-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49783-score" class="post-score" title="current number of votes">0</div><span id="post-49783-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Wireshark isn't the tool for this task. You need something like the tools listed <a href="http://mac.appstorm.net/roundups/internet-roundup/4-ways-to-monitor-bandwidth-usage-on-your-mac/">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Feb '16, 08:36</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-49783" class="comments-container"></div><div id="comment-tools-49783" class="comment-tools"></div><div class="clear"></div><div id="comment-49783-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="49787"></span>

<div id="answer-container-49787" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49787-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49787-score" class="post-score" title="current number of votes">0</div><span id="post-49787-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Why bother with Wireshark, while you have the tools at hand? Go look for the Activity Monitor, it can show you the data usage since the last reboot. I assume you can keep your MacBook running for a week?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Feb '16, 09:02</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-49787" class="comments-container"><span id="49790"></span><div id="comment-49790" class="comment"><div id="post-49790-score" class="comment-score"></div><div class="comment-text"><p>Thanks for quick answers. I had tried Activity Monitor, but I was not aware that it begins at restart. Will use that and can certainly keep the Mac going for a week. :-) Always learn something new just by asking.</p></div><div id="comment-49790-info" class="comment-info"><span class="comment-age">(03 Feb '16, 09:20)</span> <span class="comment-user userinfo">LZachs</span></div></div><span id="49811"></span><div id="comment-49811" class="comment"><div id="post-49811-score" class="comment-score"></div><div class="comment-text"><p>Great! If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-49811-info" class="comment-info"><span class="comment-age">(04 Feb '16, 04:07)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div></div><div id="comment-tools-49787" class="comment-tools"></div><div class="clear"></div><div id="comment-49787-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

