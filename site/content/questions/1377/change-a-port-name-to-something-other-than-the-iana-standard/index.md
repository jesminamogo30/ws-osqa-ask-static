+++
type = "question"
title = "Change a port name to something other than the iana standard"
description = '''I have a trace of a NAS product that uses port 2050 for some internal application. The iana port list has Avaya EMB Config Port registered for that port. Is there anyway that I can change the configuration within Wireshark in order to change the name to something else?'''
date = "2010-12-16T07:55:00Z"
lastmod = "2010-12-16T08:54:00Z"
weight = 1377
keywords = [ "iana" ]
aliases = [ "/questions/1377" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Change a port name to something other than the iana standard](/questions/1377/change-a-port-name-to-something-other-than-the-iana-standard)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1377-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1377-score" class="post-score" title="current number of votes">0</div><span id="post-1377-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have a trace of a NAS product that uses port 2050 for some internal application. The iana port list has Avaya EMB Config Port registered for that port. Is there anyway that I can change the configuration within Wireshark in order to change the name to something else?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-iana" rel="tag" title="see questions tagged &#39;iana&#39;">iana</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Dec '10, 07:55</strong></p><img src="https://secure.gravatar.com/avatar/5f4ec1bad7badc4cfa885643b648fadc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bpiela&#39;s gravatar image" /><p><span>bpiela</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bpiela has no accepted answers">0%</span></p></div></div><div id="comments-container-1377" class="comments-container"></div><div id="comment-tools-1377" class="comment-tools"></div><div class="clear"></div><div id="comment-1377-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1378"></span>

<div id="answer-container-1378" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1378-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1378-score" class="post-score" title="current number of votes">1</div><span id="post-1378-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="cmaynard has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Yes. There is a file named "services" in your Wireshark installation directory that you can edit. I've done that on Windows once and it worked just fine; I guess it should do the same on any other plattform as well.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>16 Dec '10, 08:14</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>16 Dec '10, 08:15</strong> </span></p></div></div><div id="comments-container-1378" class="comments-container"><span id="1379"></span><div id="comment-1379" class="comment"><div id="post-1379-score" class="comment-score"></div><div class="comment-text"><p>awesome! Thanks for the tip!</p></div><div id="comment-1379-info" class="comment-info"><span class="comment-age">(16 Dec '10, 08:54)</span> <span class="comment-user userinfo">bpiela</span></div></div></div><div id="comment-tools-1378" class="comment-tools"></div><div class="clear"></div><div id="comment-1378-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

