+++
type = "question"
title = "Uninstall WS from Mac El Capitan"
description = '''How to completely uninstall WS from Mac El Capitan? Is it enough to Move to Trash Wireshark application icon? Are there any leftover WS support files hidden somewhere (in Library, etc.)?'''
date = "2016-01-11T11:23:00Z"
lastmod = "2016-01-11T11:23:00Z"
weight = 49099
keywords = [ "el", "mac", "capitan", "uninstall" ]
aliases = [ "/questions/49099" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Uninstall WS from Mac El Capitan](/questions/49099/uninstall-ws-from-mac-el-capitan)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49099-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49099-score" class="post-score" title="current number of votes">1</div><span id="post-49099-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How to completely uninstall WS from Mac El Capitan? Is it enough to Move to Trash Wireshark application icon? Are there any leftover WS support files hidden somewhere (in Library, etc.)?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-el" rel="tag" title="see questions tagged &#39;el&#39;">el</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-capitan" rel="tag" title="see questions tagged &#39;capitan&#39;">capitan</span> <span class="post-tag tag-link-uninstall" rel="tag" title="see questions tagged &#39;uninstall&#39;">uninstall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Jan '16, 11:23</strong></p><img src="https://secure.gravatar.com/avatar/f89e90223f0304be83bf02f0649324f0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Delos&#39;s gravatar image" /><p><span>Delos</span><br />
<span class="score" title="21 reputation points">21</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Delos has no accepted answers">0%</span></p></div></div><div id="comments-container-49099" class="comments-container"></div><div id="comment-tools-49099" class="comment-tools"></div><div class="clear"></div><div id="comment-49099-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

