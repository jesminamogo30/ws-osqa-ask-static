+++
type = "question"
title = "UCI IE is not decoded correctly in CSR message of GTPv2C"
description = '''In Wireshark Version 1.10.1 the User CSG Information IE is not decoded in Create session Request message.'''
date = "2013-10-21T04:52:00Z"
lastmod = "2013-10-21T05:08:00Z"
weight = 26237
keywords = [ "gtpv2c" ]
aliases = [ "/questions/26237" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [UCI IE is not decoded correctly in CSR message of GTPv2C](/questions/26237/uci-ie-is-not-decoded-correctly-in-csr-message-of-gtpv2c)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26237-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26237-score" class="post-score" title="current number of votes">0</div><span id="post-26237-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>In Wireshark Version 1.10.1 the User CSG Information IE is not decoded in Create session Request message.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-gtpv2c" rel="tag" title="see questions tagged &#39;gtpv2c&#39;">gtpv2c</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>21 Oct '13, 04:52</strong></p><img src="https://secure.gravatar.com/avatar/fce57bd76f0e94cfaa8daf467f5c0c5a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pikan&#39;s gravatar image" /><p><span>Pikan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pikan has no accepted answers">0%</span></p></div></div><div id="comments-container-26237" class="comments-container"><span id="26238"></span><div id="comment-26238" class="comment"><div id="post-26238-score" class="comment-score"></div><div class="comment-text"><p>Is this a question or a bug report?</p><p>If it is supposed to be question, please change your text to actually ask something.</p><p>If bug report, go to <a href="http://bugs.wireshark.org">http://bugs.wireshark.org</a> and file it over there, please. Wait. Check for an existing bug report of the same issue first. No. I didn't.</p></div><div id="comment-26238-info" class="comment-info"><span class="comment-age">(21 Oct '13, 04:57)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-26237" class="comment-tools"></div><div class="clear"></div><div id="comment-26237-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="26240"></span>

<div id="answer-container-26240" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-26240-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-26240-score" class="post-score" title="current number of votes">1</div><span id="post-26240-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Please see the answer to a similar question.</p><blockquote><p><a href="http://ask.wireshark.org/questions/20838/gtpv2c-release-10-29274v1010-ie-decode">http://ask.wireshark.org/questions/20838/gtpv2c-release-10-29274v1010-ie-decode</a></p></blockquote><p>You probably need the latest development release.</p><blockquote><p><a href="http://www.wireshark.org/download/automated/">http://www.wireshark.org/download/automated/</a></p></blockquote><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>21 Oct '13, 05:08</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-26240" class="comments-container"></div><div id="comment-tools-26240" class="comment-tools"></div><div class="clear"></div><div id="comment-26240-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

