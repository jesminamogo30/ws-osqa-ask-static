+++
type = "question"
title = "Did I get infected (slammer.pcap)"
description = '''Hi I opened a file slammer.pcap but later I checked on virustotal this file is infected. Did my computer get infected because of opening it? I was sure there are just ips and nothing more. I&#x27;m using Debian Jessie 8.8 and Wireshark as non-root.'''
date = "2017-06-08T04:21:00Z"
lastmod = "2017-06-08T04:31:00Z"
weight = 61861
keywords = [ "slammer.pcap", "virus" ]
aliases = [ "/questions/61861" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Did I get infected (slammer.pcap)](/questions/61861/did-i-get-infected-slammerpcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61861-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61861-score" class="post-score" title="current number of votes">0</div><span id="post-61861-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi I opened a file slammer.pcap but later I checked on virustotal this file is infected. Did my computer get infected because of opening it? I was sure there are just ips and nothing more. I'm using Debian Jessie 8.8 and Wireshark as non-root.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-slammer.pcap" rel="tag" title="see questions tagged &#39;slammer.pcap&#39;">slammer.pcap</span> <span class="post-tag tag-link-virus" rel="tag" title="see questions tagged &#39;virus&#39;">virus</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Jun '17, 04:21</strong></p><img src="https://secure.gravatar.com/avatar/0b32051015853b1c4a58de9951633aba?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nee4V&#39;s gravatar image" /><p><span>nee4V</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nee4V has no accepted answers">0%</span></p></div></div><div id="comments-container-61861" class="comments-container"></div><div id="comment-tools-61861" class="comment-tools"></div><div class="clear"></div><div id="comment-61861-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="61862"></span>

<div id="answer-container-61862" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-61862-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-61862-score" class="post-score" title="current number of votes">0</div><span id="post-61862-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="nee4V has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>If you opened the pcap in Wireshark (especially on a Linux system with non-root credentials) there is nothing to worry about. Slammer attacks SQL servers over the network, so there needs to be active communication of packets, and just reading them passively in Wireshark doesn't do that at all. You're safe.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>08 Jun '17, 04:24</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-61862" class="comments-container"><span id="61863"></span><div id="comment-61863" class="comment"><div id="post-61863-score" class="comment-score"></div><div class="comment-text"><p>Thank you for fast answer, I'm glad.</p></div><div id="comment-61863-info" class="comment-info"><span class="comment-age">(08 Jun '17, 04:31)</span> <span class="comment-user userinfo">nee4V</span></div></div></div><div id="comment-tools-61862" class="comment-tools"></div><div class="clear"></div><div id="comment-61862-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

