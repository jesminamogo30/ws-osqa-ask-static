+++
type = "question"
title = "Data reconstruction"
description = '''Is FTP data able to be replayed and reconstructed if the packets are captured on the wire? If an attack were to occur between the source and destination IP host with data replayed that has been altered, what kind of attack is this called?'''
date = "2014-01-15T07:04:00Z"
lastmod = "2014-01-15T13:38:00Z"
weight = 28915
keywords = [ "data", "homework", "reconstruction" ]
aliases = [ "/questions/28915" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Data reconstruction](/questions/28915/data-reconstruction)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-28915-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-28915-score" class="post-score" title="current number of votes">0</div><span id="post-28915-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Is FTP data able to be replayed and reconstructed if the packets are captured on the wire? If an attack were to occur between the source and destination IP host with data replayed that has been altered, what kind of attack is this called?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-homework" rel="tag" title="see questions tagged &#39;homework&#39;">homework</span> <span class="post-tag tag-link-reconstruction" rel="tag" title="see questions tagged &#39;reconstruction&#39;">reconstruction</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Jan '14, 07:04</strong></p><img src="https://secure.gravatar.com/avatar/81dc7ef173dc45679c115b238b7eeadb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jw123&#39;s gravatar image" /><p><span>jw123</span><br />
<span class="score" title="11 reputation points">11</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jw123 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>15 Jan '14, 07:24</strong> </span></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span></p></div></div><div id="comments-container-28915" class="comments-container"><span id="28917"></span><div id="comment-28917" class="comment"><div id="post-28917-score" class="comment-score"></div><div class="comment-text"><blockquote><p>what kind of attack is this called?</p></blockquote><p>I would call it a <strong>sophisticated</strong> attack. Not sure if that's the answer your teacher is expecting!!</p></div><div id="comment-28917-info" class="comment-info"><span class="comment-age">(15 Jan '14, 07:08)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="28934"></span><div id="comment-28934" class="comment"><div id="post-28934-score" class="comment-score"></div><div class="comment-text"><p>lol 4 "homework" tag :-)</p></div><div id="comment-28934-info" class="comment-info"><span class="comment-age">(15 Jan '14, 13:29)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="28937"></span><div id="comment-28937" class="comment"><div id="post-28937-score" class="comment-score"></div><div class="comment-text"><p>I thought it might help ;-)</p></div><div id="comment-28937-info" class="comment-info"><span class="comment-age">(15 Jan '14, 13:38)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-28915" class="comment-tools"></div><div class="clear"></div><div id="comment-28915-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

