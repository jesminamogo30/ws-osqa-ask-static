+++
type = "question"
title = "Error Opening file for Writing... wireTap-0.3.1.dll"
description = '''I downloaded Wireshark 1.4.4 (64-bit) and ran setup, but am getting an error about opening file for writing file wiretap-0.3.1.dll '''
date = "2011-04-05T11:14:00Z"
lastmod = "2011-04-05T23:32:00Z"
weight = 3347
keywords = [ "wiretap-0.3.1.dll", "setup", "windows7", "64-bit" ]
aliases = [ "/questions/3347" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Error Opening file for Writing... wireTap-0.3.1.dll](/questions/3347/error-opening-file-for-writing-wiretap-031dll)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3347-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3347-score" class="post-score" title="current number of votes">0</div><span id="post-3347-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I downloaded Wireshark 1.4.4 (64-bit) and ran setup, but am getting an error about opening file for writing file wiretap-0.3.1.dll</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wiretap-0.3.1.dll" rel="tag" title="see questions tagged &#39;wiretap-0.3.1.dll&#39;">wiretap-0.3.1.dll</span> <span class="post-tag tag-link-setup" rel="tag" title="see questions tagged &#39;setup&#39;">setup</span> <span class="post-tag tag-link-windows7" rel="tag" title="see questions tagged &#39;windows7&#39;">windows7</span> <span class="post-tag tag-link-64-bit" rel="tag" title="see questions tagged &#39;64-bit&#39;">64-bit</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Apr '11, 11:14</strong></p><img src="https://secure.gravatar.com/avatar/2ff8b38903522946cadb7a82db920b22?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Harryw411&#39;s gravatar image" /><p><span>Harryw411</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Harryw411 has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Feb '12, 18:53</strong> </span></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span></p></div></div><div id="comments-container-3347" class="comments-container"><span id="3348"></span><div id="comment-3348" class="comment"><div id="post-3348-score" class="comment-score"></div><div class="comment-text"><p>Can you be more specific? It will be easier to find out what is going wrong if you can post the actual error message you receive.</p></div><div id="comment-3348-info" class="comment-info"><span class="comment-age">(05 Apr '11, 11:17)</span> <span class="comment-user userinfo">multipleinte...</span></div></div></div><div id="comment-tools-3347" class="comment-tools"></div><div class="clear"></div><div id="comment-3347-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="3362"></span>

<div id="answer-container-3362" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3362-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3362-score" class="post-score" title="current number of votes">0</div><span id="post-3362-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>You stated you solved it <a href="http://ask.wireshark.org/questions/58/windows-7-installation-problems">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Apr '11, 22:57</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-3362" class="comments-container"><span id="3365"></span><div id="comment-3365" class="comment"><div id="post-3365-score" class="comment-score"></div><div class="comment-text"><p>...and he said he fixed it by running it as administrator.</p></div><div id="comment-3365-info" class="comment-info"><span class="comment-age">(05 Apr '11, 23:32)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-3362" class="comment-tools"></div><div class="clear"></div><div id="comment-3362-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

