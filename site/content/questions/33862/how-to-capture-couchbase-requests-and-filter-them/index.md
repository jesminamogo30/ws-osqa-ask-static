+++
type = "question"
title = "How to capture couchbase requests and filter them"
description = '''Hi  I would like to capture and filter couchbase requests. How do i do it. '''
date = "2014-06-16T06:56:00Z"
lastmod = "2014-06-16T08:43:00Z"
weight = 33862
keywords = [ "couchbase" ]
aliases = [ "/questions/33862" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to capture couchbase requests and filter them](/questions/33862/how-to-capture-couchbase-requests-and-filter-them)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-33862-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-33862-score" class="post-score" title="current number of votes">1</div><span id="post-33862-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi</p><p>I would like to capture and filter couchbase requests. How do i do it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-couchbase" rel="tag" title="see questions tagged &#39;couchbase&#39;">couchbase</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>16 Jun '14, 06:56</strong></p><img src="https://secure.gravatar.com/avatar/be8a9b2e9d87b13606c3b9e75d26e71d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="scara&#39;s gravatar image" /><p><span>scara</span><br />
<span class="score" title="31 reputation points">31</span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="14 badges"><span class="bronze">●</span><span class="badgecount">14</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="scara has no accepted answers">0%</span></p></div></div><div id="comments-container-33862" class="comments-container"><span id="33865"></span><div id="comment-33865" class="comment"><div id="post-33865-score" class="comment-score"></div><div class="comment-text"><p>You can try this <a href="https://github.com/avsej/wireshark">version</a>. See also this page in <a href="http://wiki.wireshark.org/Protocols/couchbase">Wireshark Wiki</a>.</p></div><div id="comment-33865-info" class="comment-info"><span class="comment-age">(16 Jun '14, 08:43)</span> <span class="comment-user userinfo">joke</span></div></div></div><div id="comment-tools-33862" class="comment-tools"></div><div class="clear"></div><div id="comment-33862-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

