+++
type = "question"
title = "Type of Frames"
description = '''Hi everyone! I am very new with Wireshark, I would like to know where the frames are coming from an where they are going to as well as what type of frames are they? Thanking you heaps!'''
date = "2014-03-11T15:54:00Z"
lastmod = "2014-03-11T15:54:00Z"
weight = 30697
keywords = [ "frames", "of", "type" ]
aliases = [ "/questions/30697" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Type of Frames](/questions/30697/type-of-frames)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30697-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30697-score" class="post-score" title="current number of votes">0</div><span id="post-30697-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi everyone!</p><p>I am very new with Wireshark, I would like to know where the frames are coming from an where they are going to as well as what type of frames are they?</p><p>Thanking you heaps!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-frames" rel="tag" title="see questions tagged &#39;frames&#39;">frames</span> <span class="post-tag tag-link-of" rel="tag" title="see questions tagged &#39;of&#39;">of</span> <span class="post-tag tag-link-type" rel="tag" title="see questions tagged &#39;type&#39;">type</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Mar '14, 15:54</strong></p><img src="https://secure.gravatar.com/avatar/7ea22f77e1ef9d6fa2e9c0ca4d69a36a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="The%20Juan&#39;s gravatar image" /><p><span>The Juan</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="The Juan has no accepted answers">0%</span></p></div></div><div id="comments-container-30697" class="comments-container"></div><div id="comment-tools-30697" class="comment-tools"></div><div class="clear"></div><div id="comment-30697-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

