+++
type = "question"
title = "Spam settings for new users"
description = '''Hi admins, sometimes new users report problems when posting an image or trying to attach a file. Apparently those actions are treated a SPAM.  I wonder how are the spam settings for new users? Thanks Kurt '''
date = "2013-09-30T08:07:00Z"
lastmod = "2013-10-09T15:54:00Z"
weight = 25389
keywords = [ "settings", "upload", "spam" ]
aliases = [ "/questions/25389" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Spam settings for new users](/questions/25389/spam-settings-for-new-users)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25389-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25389-score" class="post-score" title="current number of votes">0</div><span id="post-25389-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi admins,</p><p>sometimes new users report problems when posting an image or trying to attach a file. Apparently those actions are treated a SPAM.</p><p>I wonder how are the spam settings for new users?</p><p>Thanks<br />
Kurt<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-settings" rel="tag" title="see questions tagged &#39;settings&#39;">settings</span> <span class="post-tag tag-link-upload" rel="tag" title="see questions tagged &#39;upload&#39;">upload</span> <span class="post-tag tag-link-spam" rel="tag" title="see questions tagged &#39;spam&#39;">spam</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Sep '13, 08:07</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div></div><div id="comments-container-25389" class="comments-container"><span id="25785"></span><div id="comment-25785" class="comment"><div id="post-25785-score" class="comment-score"></div><div class="comment-text"><p>anyone??</p><p>See also here: <a href="http://ask.wireshark.org/questions/25782/unable-to-add-link-to-other-qa">http://ask.wireshark.org/questions/25782/unable-to-add-link-to-other-qa</a></p></div><div id="comment-25785-info" class="comment-info"><span class="comment-age">(09 Oct '13, 01:47)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="25786"></span><div id="comment-25786" class="comment"><div id="post-25786-score" class="comment-score"></div><div class="comment-text"><p>I think this is something the guys with two dots behind their name need to answer ;-)</p></div><div id="comment-25786-info" class="comment-info"><span class="comment-age">(09 Oct '13, 01:48)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="25789"></span><div id="comment-25789" class="comment"><div id="post-25789-score" class="comment-score"></div><div class="comment-text"><p>I know, that's why I asked the admins ;-))</p><p>Unfortunately there is no private message system available in OSQA, so direct communication is only possible via a question !?!</p></div><div id="comment-25789-info" class="comment-info"><span class="comment-age">(09 Oct '13, 01:56)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="25797"></span><div id="comment-25797" class="comment"><div id="post-25797-score" class="comment-score"></div><div class="comment-text"><p>Well, sideband communication via direct email to Gerald or Sake should work, too ;-)</p></div><div id="comment-25797-info" class="comment-info"><span class="comment-age">(09 Oct '13, 03:16)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="25807"></span><div id="comment-25807" class="comment"><div id="post-25807-score" class="comment-score"></div><div class="comment-text"><p>it's not that important for me. I just thought the admins might be interested to know ;-)</p></div><div id="comment-25807-info" class="comment-info"><span class="comment-age">(09 Oct '13, 06:19)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="25850"></span><div id="comment-25850" class="comment not_top_scorer"><div id="post-25850-score" class="comment-score"></div><div class="comment-text"><p>I did take a look at the settings in the GUI, but I could not find anything recognizable. I'll ping Gerald...</p></div><div id="comment-25850-info" class="comment-info"><span class="comment-age">(09 Oct '13, 14:34)</span> <span class="comment-user userinfo">SYN-bit ♦♦</span></div></div><span id="25851"></span><div id="comment-25851" class="comment not_top_scorer"><div id="post-25851-score" class="comment-score"></div><div class="comment-text"><p>thanks....</p></div><div id="comment-25851-info" class="comment-info"><span class="comment-age">(09 Oct '13, 14:46)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-25389" class="comment-tools"><span class="comments-showing"> showing 5 of 7 </span> <a href="#" class="show-all-comments-link">show 2 more comments</a></div><div class="clear"></div><div id="comment-25389-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="25854"></span>

<div id="answer-container-25854" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-25854-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-25854-score" class="post-score" title="current number of votes">2</div><span id="post-25854-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Kurt Knochner has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It looks like I completely missed the fact that the antispam service we were using had been EoLed (yay cloud!): <a href="http://everything.typepad.com/blog/2013/02/retiring-typepad-anti-spam-and-switching-to-impermium.html">http://everything.typepad.com/blog/2013/02/retiring-typepad-anti-spam-and-switching-to-impermium.html</a></p><p>I set up an Akismet account and configured OSQA to use it for spam checking. I also set the reCAPTCHA to zero. If we start getting too much spam I'll adjust it accordingly.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Oct '13, 15:54</strong></p><img src="https://secure.gravatar.com/avatar/6db117a984c6529df88330dc49fb1ee4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Gerald%20Combs&#39;s gravatar image" /><p><span>Gerald Combs ♦♦</span><br />
<span class="score" title="3332 reputation points"><span>3.3k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="58 badges"><span class="bronze">●</span><span class="badgecount">58</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Gerald Combs has 32 accepted answers">24%</span></p></div></div><div id="comments-container-25854" class="comments-container"></div><div id="comment-tools-25854" class="comment-tools"></div><div class="clear"></div><div id="comment-25854-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

