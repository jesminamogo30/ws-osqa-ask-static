+++
type = "question"
title = "Would you recommend XQuartz over Apple&#x27;s original X11.app for Mac OS X 10.5?"
description = '''I&#x27;m running Mac OS X 10.5, and while Wireshark is getting along reasonably well with X11 (version 2.1.6 of X11.app if I recall), XQuartz ( http://xquartz.macosforge.org/landing/ ) offers a newer version (2.6.3) which was originally developed with Apple&#x27;s help and from which some enhancements were me...'''
date = "2013-07-30T15:00:00Z"
lastmod = "2013-07-30T15:00:00Z"
weight = 23454
keywords = [ "osx", "mac", "macosx", "x11" ]
aliases = [ "/questions/23454" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Would you recommend XQuartz over Apple's original X11.app for Mac OS X 10.5?](/questions/23454/would-you-recommend-xquartz-over-apples-original-x11app-for-mac-os-x-105)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-23454-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-23454-score" class="post-score" title="current number of votes">0</div><span id="post-23454-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm running Mac OS X 10.5, and while Wireshark is getting along reasonably well with X11 (version 2.1.6 of X11.app if I recall), XQuartz ( <a href="http://xquartz.macosforge.org/landing/">http://xquartz.macosforge.org/landing/</a> ) offers a newer version (2.6.3) which was originally developed with Apple's help and from which some enhancements were merged back into the Apple build, but which has since superseded Apple's build (with the important caveat that you can only have one or the other installed, not both, and they 'clobber' each other, in the words of the developers). In fact Apple themselves have dropped X11 in Mountain Lion and recommend XQuartz to those users of Mountain Lion who need X11.</p><p>So my question is, would you recommend XQuartz over Apple's original X11.app for Mac OS X 10.5? Obviously directed at those using Mac OS X, especially version 10.5.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-osx" rel="tag" title="see questions tagged &#39;osx&#39;">osx</span> <span class="post-tag tag-link-mac" rel="tag" title="see questions tagged &#39;mac&#39;">mac</span> <span class="post-tag tag-link-macosx" rel="tag" title="see questions tagged &#39;macosx&#39;">macosx</span> <span class="post-tag tag-link-x11" rel="tag" title="see questions tagged &#39;x11&#39;">x11</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>30 Jul '13, 15:00</strong></p><img src="https://secure.gravatar.com/avatar/d0d7eb49c8a24a1860db62cba29f3bdd?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lubo%20Diakov&#39;s gravatar image" /><p><span>Lubo Diakov</span><br />
<span class="score" title="11 reputation points">11</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lubo Diakov has no accepted answers">0%</span></p></div></div><div id="comments-container-23454" class="comments-container"></div><div id="comment-tools-23454" class="comment-tools"></div><div class="clear"></div><div id="comment-23454-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

