+++
type = "question"
title = "key logger or spyware"
description = '''Can Wireshark detect a key logger or spyware on my mac computer? If so, can some explain to me in lay terms how to detect it? I am almost 99% certain that one has been installed either remotely or on my physical laptop. Thanks.'''
date = "2014-08-28T22:28:00Z"
lastmod = "2014-08-28T22:28:00Z"
weight = 35857
keywords = [ "keylogger", "spyware" ]
aliases = [ "/questions/35857" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [key logger or spyware](/questions/35857/key-logger-or-spyware)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35857-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35857-score" class="post-score" title="current number of votes">0</div><span id="post-35857-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Can Wireshark detect a key logger or spyware on my mac computer? If so, can some explain to me in lay terms how to detect it? I am almost 99% certain that one has been installed either remotely or on my physical laptop. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-keylogger" rel="tag" title="see questions tagged &#39;keylogger&#39;">keylogger</span> <span class="post-tag tag-link-spyware" rel="tag" title="see questions tagged &#39;spyware&#39;">spyware</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '14, 22:28</strong></p><img src="https://secure.gravatar.com/avatar/4fdd95fe9ebf2c2c2a5abca42f5cea61?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="gsmith31&#39;s gravatar image" /><p><span>gsmith31</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="gsmith31 has no accepted answers">0%</span></p></div></div><div id="comments-container-35857" class="comments-container"></div><div id="comment-tools-35857" class="comment-tools"></div><div class="clear"></div><div id="comment-35857-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

