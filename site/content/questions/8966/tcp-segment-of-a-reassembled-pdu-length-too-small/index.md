+++
type = "question"
title = "TCP segment of a reassembled PDU length too small"
description = '''Hello Everyone, I am having a sporadic issue when I attempt to download files from a certain web site. One of the things I noticed is that when the download succeeds the TCP segment of a reassembled PDU packet length is 1514. When the download fails the TCP segment of a reassembled PDU is 1314. I am...'''
date = "2012-02-11T19:03:00Z"
lastmod = "2012-02-11T19:03:00Z"
weight = 8966
keywords = [ "reassembled", "segment", "pdu", "tcp" ]
aliases = [ "/questions/8966" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [TCP segment of a reassembled PDU length too small](/questions/8966/tcp-segment-of-a-reassembled-pdu-length-too-small)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8966-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8966-score" class="post-score" title="current number of votes">0</div><span id="post-8966-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello Everyone,</p><p>I am having a sporadic issue when I attempt to download files from a certain web site. One of the things I noticed is that when the download succeeds the TCP segment of a reassembled PDU packet length is 1514. When the download fails the TCP segment of a reassembled PDU is 1314. I am not quite sure what to make of this. I have been troubleshooting extensively and this is one of the differences that I have noticed. If anyone has any input or point me in the right direction I would really appreciate it.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-reassembled" rel="tag" title="see questions tagged &#39;reassembled&#39;">reassembled</span> <span class="post-tag tag-link-segment" rel="tag" title="see questions tagged &#39;segment&#39;">segment</span> <span class="post-tag tag-link-pdu" rel="tag" title="see questions tagged &#39;pdu&#39;">pdu</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Feb '12, 19:03</strong></p><img src="https://secure.gravatar.com/avatar/90bd22a9767a8fc0a7481f0d856dbc08?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="kbcloud&#39;s gravatar image" /><p><span>kbcloud</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="kbcloud has no accepted answers">0%</span></p></div></div><div id="comments-container-8966" class="comments-container"></div><div id="comment-tools-8966" class="comment-tools"></div><div class="clear"></div><div id="comment-8966-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

