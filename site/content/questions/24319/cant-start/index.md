+++
type = "question"
title = "can&#x27;t start"
description = '''if i go to capture then options the start button won&#x27;t click. why is this'''
date = "2013-09-03T16:36:00Z"
lastmod = "2013-09-04T04:52:00Z"
weight = 24319
keywords = [ "starting" ]
aliases = [ "/questions/24319" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [can't start](/questions/24319/cant-start)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24319-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24319-score" class="post-score" title="current number of votes">0</div><span id="post-24319-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>if i go to capture then options the start button won't click. why is this</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-starting" rel="tag" title="see questions tagged &#39;starting&#39;">starting</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>03 Sep '13, 16:36</strong></p><img src="https://secure.gravatar.com/avatar/16e79bb59f136408b2979a17f6f783e0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="waffles&#39;s gravatar image" /><p><span>waffles</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="waffles has no accepted answers">0%</span></p></div></div><div id="comments-container-24319" class="comments-container"><span id="24331"></span><div id="comment-24331" class="comment"><div id="post-24331-score" class="comment-score"></div><div class="comment-text"><p>what do you mean by "won't click"? Is that button <strong>greyed out</strong>? If so, there are no interfaces available to capture on.</p></div><div id="comment-24331-info" class="comment-info"><span class="comment-age">(03 Sep '13, 23:50)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-24319" class="comment-tools"></div><div class="clear"></div><div id="comment-24319-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="24322"></span>

<div id="answer-container-24322" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24322-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24322-score" class="post-score" title="current number of votes">0</div><span id="post-24322-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Is this windows? You need "admin" rights to capture traces...</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>03 Sep '13, 21:19</strong></p><img src="https://secure.gravatar.com/avatar/5500bd1decb766660522dfb347eedc49?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mrEEde&#39;s gravatar image" /><p><span>mrEEde</span><br />
<span class="score" title="3892 reputation points"><span>3.9k</span></span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="22 badges"><span class="silver">●</span><span class="badgecount">22</span></span><span title="70 badges"><span class="bronze">●</span><span class="badgecount">70</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mrEEde has 48 accepted answers">20%</span></p></div></div><div id="comments-container-24322" class="comments-container"><span id="24333"></span><div id="comment-24333" class="comment"><div id="post-24333-score" class="comment-score"></div><div class="comment-text"><p>thanks but got it working with out being admin</p></div><div id="comment-24333-info" class="comment-info"><span class="comment-age">(04 Sep '13, 00:24)</span> <span class="comment-user userinfo">waffles</span></div></div><span id="24334"></span><div id="comment-24334" class="comment"><div id="post-24334-score" class="comment-score"></div><div class="comment-text"><p>Do you mind to 'enlighten' us as well ;-)</p></div><div id="comment-24334-info" class="comment-info"><span class="comment-age">(04 Sep '13, 00:25)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="24338"></span><div id="comment-24338" class="comment"><div id="post-24338-score" class="comment-score"></div><div class="comment-text"><p>My guess: there was no card selected... :-)</p></div><div id="comment-24338-info" class="comment-info"><span class="comment-age">(04 Sep '13, 03:53)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="24340"></span><div id="comment-24340" class="comment"><div id="post-24340-score" class="comment-score"></div><div class="comment-text"><p>maybe. I still don't get the "won't click" part !?!</p></div><div id="comment-24340-info" class="comment-info"><span class="comment-age">(04 Sep '13, 04:52)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-24322" class="comment-tools"></div><div class="clear"></div><div id="comment-24322-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

