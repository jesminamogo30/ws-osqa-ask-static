+++
type = "question"
title = "uninstall 1.4.2. for mac"
description = '''how do I uninstall 1.4.2 for mac 64bit?'''
date = "2010-12-07T16:03:00Z"
lastmod = "2010-12-07T16:26:00Z"
weight = 1277
keywords = [ "uninstall" ]
aliases = [ "/questions/1277" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [uninstall 1.4.2. for mac](/questions/1277/uninstall-142-for-mac)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1277-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1277-score" class="post-score" title="current number of votes">0</div><span id="post-1277-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>how do I uninstall 1.4.2 for mac 64bit?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-uninstall" rel="tag" title="see questions tagged &#39;uninstall&#39;">uninstall</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>07 Dec '10, 16:03</strong></p><img src="https://secure.gravatar.com/avatar/f84924ad3b8f2528c8713dfd7012343f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dumpster&#39;s gravatar image" /><p><span>dumpster</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dumpster has no accepted answers">0%</span></p></div></div><div id="comments-container-1277" class="comments-container"></div><div id="comment-tools-1277" class="comment-tools"></div><div class="clear"></div><div id="comment-1277-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="1278"></span>

<div id="answer-container-1278" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-1278-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-1278-score" class="post-score" title="current number of votes">0</div><span id="post-1278-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>nevermind... found it.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>07 Dec '10, 16:26</strong></p><img src="https://secure.gravatar.com/avatar/f84924ad3b8f2528c8713dfd7012343f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="dumpster&#39;s gravatar image" /><p><span>dumpster</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="dumpster has no accepted answers">0%</span></p></div></div><div id="comments-container-1278" class="comments-container"></div><div id="comment-tools-1278" class="comment-tools"></div><div class="clear"></div><div id="comment-1278-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

