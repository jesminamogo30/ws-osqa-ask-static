+++
type = "question"
title = "Getting started (capture filter)"
description = '''Using rev 1.4.3 My Capture Filter window does not look like the one in the help file. There are no Apply or Save options. How do I create or load a new filter?'''
date = "2011-02-02T12:01:00Z"
lastmod = "2011-02-02T13:45:00Z"
weight = 2114
keywords = [ "filter", "capture", "newbie", "questions" ]
aliases = [ "/questions/2114" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Getting started (capture filter)](/questions/2114/getting-started-capture-filter)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2114-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2114-score" class="post-score" title="current number of votes">0</div><span id="post-2114-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Using rev 1.4.3</p><p>My Capture Filter window does not look like the one in the help file. There are no Apply or Save options. How do I create or load a new filter?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-newbie" rel="tag" title="see questions tagged &#39;newbie&#39;">newbie</span> <span class="post-tag tag-link-questions" rel="tag" title="see questions tagged &#39;questions&#39;">questions</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>02 Feb '11, 12:01</strong></p><img src="https://secure.gravatar.com/avatar/f6ea101d4bc8dcd850d79485f76438c0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="garyhibb&#39;s gravatar image" /><p><span>garyhibb</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="garyhibb has no accepted answers">0%</span></p></div></div><div id="comments-container-2114" class="comments-container"></div><div id="comment-tools-2114" class="comment-tools"></div><div class="clear"></div><div id="comment-2114-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="2115"></span>

<div id="answer-container-2115" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2115-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2115-score" class="post-score" title="current number of votes">0</div><span id="post-2115-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>It is just a bit quirky until you get used to it. One might expect the "new" button to bring up a dialog box similar to the Expression builder with display filters. Actually, when you manually type in a filter name, a filter string and click new, it adds a new capture filter. So it is sort of like the viewing, creation and deletion of filters is all integrated into the two text boxes. Mess around with it, you will figure it out. What might be a struggle is learning the syntax, unless you are already familiar with the Berkeley Packet Filter format.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Feb '11, 13:45</strong></p><img src="https://secure.gravatar.com/avatar/e62501f00394530927e4b0c9e86bfb46?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paul%20Stewart&#39;s gravatar image" /><p><span>Paul Stewart</span><br />
<span class="score" title="301 reputation points">301</span><span title="8 badges"><span class="bronze">●</span><span class="badgecount">8</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paul Stewart has 3 accepted answers">6%</span></p></div></div><div id="comments-container-2115" class="comments-container"></div><div id="comment-tools-2115" class="comment-tools"></div><div class="clear"></div><div id="comment-2115-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

