+++
type = "question"
title = "smb2.time calculation"
description = '''Hi! I am calculating response time of server to check the performance of the smb2. After working with some manual method i found that now smb2.time can be used to determing the response time.  can anyone about it? Thanks'''
date = "2014-01-25T11:30:00Z"
lastmod = "2014-01-31T06:48:00Z"
weight = 29153
keywords = [ "smb2.time", "calculation" ]
aliases = [ "/questions/29153" ]
osqa_answers = 2
osqa_accepted = false
+++

<div class="headNormal">

# [smb2.time calculation](/questions/29153/smb2time-calculation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29153-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29153-score" class="post-score" title="current number of votes">0</div><span id="post-29153-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi!</p><p>I am calculating response time of server to check the performance of the smb2. After working with some manual method i found that now smb2.time can be used to determing the response time.</p><p>can anyone about it?</p><p>Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-smb2.time" rel="tag" title="see questions tagged &#39;smb2.time&#39;">smb2.time</span> <span class="post-tag tag-link-calculation" rel="tag" title="see questions tagged &#39;calculation&#39;">calculation</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Jan '14, 11:30</strong></p><img src="https://secure.gravatar.com/avatar/ace27de334352813ff6d6dcca2257949?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Capricorn&#39;s gravatar image" /><p><span>Capricorn</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Capricorn has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>25 Jan '14, 16:34</strong> </span></p></div></div><div id="comments-container-29153" class="comments-container"><span id="29245"></span><div id="comment-29245" class="comment"><div id="post-29245-score" class="comment-score"></div><div class="comment-text"><p>Hi guys!</p><p>Can anyone help with this.</p><p>thanks</p></div><div id="comment-29245-info" class="comment-info"><span class="comment-age">(28 Jan '14, 11:40)</span> <span class="comment-user userinfo">Capricorn</span></div></div><span id="29246"></span><div id="comment-29246" class="comment"><div id="post-29246-score" class="comment-score"></div><div class="comment-text"><p>There are some answers. What are the remaining questions?</p></div><div id="comment-29246-info" class="comment-info"><span class="comment-age">(28 Jan '14, 12:07)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-29153" class="comment-tools"></div><div class="clear"></div><div id="comment-29153-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="29154"></span>

<div id="answer-container-29154" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29154-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29154-score" class="post-score" title="current number of votes">1</div><span id="post-29154-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go to:<br />
- Statistics<br />
- Service Response Time<br />
- SMB2...<br />
- hit Create Stat<br />
</p><p>You can find more information about the "Statistics" Menu in the <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChUseStatisticsMenuSection.html">Wireshark User's Guide</a> or in the <a href="http://wiki.wireshark.org/Statistics">Wireshark Wiki</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>25 Jan '14, 12:29</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span> </br></br></p></div></div><div id="comments-container-29154" class="comments-container"><span id="29160"></span><div id="comment-29160" class="comment"><div id="post-29160-score" class="comment-score"></div><div class="comment-text"><p>Thanks I will look into this.</p><p>What about Time from Request? can we use that value and export into excel and convert the Time from Request in ms to get the results?</p></div><div id="comment-29160-info" class="comment-info"><span class="comment-age">(25 Jan '14, 15:24)</span> <span class="comment-user userinfo">Capricorn</span></div></div></div><div id="comment-tools-29154" class="comment-tools"></div><div class="clear"></div><div id="comment-29154-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="29163"></span>

<div id="answer-container-29163" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-29163-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-29163-score" class="post-score" title="current number of votes">0</div><span id="post-29163-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><a href="http://www.wireshark.org/docs/wsug_html_chunked/ChIOExportSection.html">Exporting data</a><br />
- select Time from request in the Packet Details pane<br />
- right-click and select Apply as Column<br />
<br />
Go to<br />
- File<br />
- Export Packet Dissections<br />
- As "CSV" (Comma Separated Values packet summary) file...<br />
- save as .csv file<br />
<br />
</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>26 Jan '14, 00:13</strong></p><img src="https://secure.gravatar.com/avatar/fac200552b0c24be2bc93a740bd54d0d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joke&#39;s gravatar image" /><p><span>joke</span><br />
<span class="score" title="1278 reputation points"><span>1.3k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="9 badges"><span class="silver">●</span><span class="badgecount">9</span></span><span title="34 badges"><span class="bronze">●</span><span class="badgecount">34</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joke has 6 accepted answers">9%</span> </br></br></p></div></div><div id="comments-container-29163" class="comments-container"><span id="29180"></span><div id="comment-29180" class="comment"><div id="post-29180-score" class="comment-score"></div><div class="comment-text"><p>Thanks.</p><p>I have already exported it but i want to know what is Time from Request?</p><p>How can it help in calculating the response time and performance of file server?</p><p>Regards,</p><p>Cap</p></div><div id="comment-29180-info" class="comment-info"><span class="comment-age">(27 Jan '14, 06:22)</span> <span class="comment-user userinfo">Capricorn</span></div></div><span id="29247"></span><div id="comment-29247" class="comment"><div id="post-29247-score" class="comment-score"></div><div class="comment-text"><p>Thanks.</p><p>I have already exported it but i want to know what is Time from Request?</p><p>How can it help in calculating the response time and performance of file server?</p><p>Regards,</p><p>Cap</p></div><div id="comment-29247-info" class="comment-info"><span class="comment-age">(28 Jan '14, 12:40)</span> <span class="comment-user userinfo">Capricorn</span></div></div><span id="29262"></span><div id="comment-29262" class="comment"><div id="post-29262-score" class="comment-score"></div><div class="comment-text"><p><a href="http://www.wireshark.org/docs/wsug_html_chunked/AppMessagesDetails.html">Time from request</a> is the time between the request en response packets.<br />
Information in the <a href="http://www.wireshark.org/docs/wsug_html_chunked/ChUsePacketDetailsPaneSection.html">Packets Details pane</a> enclosed in brackets is additional information generated by Wireshark.</p></div><div id="comment-29262-info" class="comment-info"><span class="comment-age">(28 Jan '14, 22:16)</span> <span class="comment-user userinfo">joke</span></div></div><span id="29351"></span><div id="comment-29351" class="comment"><div id="post-29351-score" class="comment-score"></div><div class="comment-text"><p>ok thanks.</p></div><div id="comment-29351-info" class="comment-info"><span class="comment-age">(31 Jan '14, 06:35)</span> <span class="comment-user userinfo">Capricorn</span></div></div><span id="29353"></span><div id="comment-29353" class="comment"><div id="post-29353-score" class="comment-score"></div><div class="comment-text"><p><span>@Capricorn</span></p><p>Your "answer" has been converted to a comment as that's how this site works. Please read the FAQ for more information.</p><p>If an answer has solved your issue, please accept the answer for the benefit of other users by clicking the checkmark icon next to the answer. Please read the FAQ for more information.</p></div><div id="comment-29353-info" class="comment-info"><span class="comment-age">(31 Jan '14, 06:48)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-29163" class="comment-tools"></div><div class="clear"></div><div id="comment-29163-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

