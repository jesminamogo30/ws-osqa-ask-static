+++
type = "question"
title = "How to search for a string inside the &quot;Uncompressed entities bodies&quot; (&#x27;when available) ?"
description = '''Hello, I captured packets associated with the loading of a plain text website in my browser. I am able to find the strings manually in the Uncompressed entity body of a packet. However when I run a string search or use the command http.response and data-text-lines contains &quot;xxxxx&quot; it doesn&#x27;t find an...'''
date = "2016-07-10T15:30:00Z"
lastmod = "2016-07-18T10:03:00Z"
weight = 53972
keywords = [ "search", "string" ]
aliases = [ "/questions/53972" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [How to search for a string inside the "Uncompressed entities bodies" ('when available) ?](/questions/53972/how-to-search-for-a-string-inside-the-uncompressed-entities-bodies-when-available)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-53972-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-53972-score" class="post-score" title="current number of votes">0</div><span id="post-53972-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello, I captured packets associated with the loading of a plain text website in my browser. I am able to find the strings manually in the Uncompressed entity body of a packet. However when I run a string search or use the command <strong>http.response and data-text-lines contains "xxxxx"</strong> it doesn't find anything. Do you have an idea how to get Wireshark to search for strings inside the uncompressed entity bodies ? Also how do I use regex or at least wildcards in my strings ?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-search" rel="tag" title="see questions tagged &#39;search&#39;">search</span> <span class="post-tag tag-link-string" rel="tag" title="see questions tagged &#39;string&#39;">string</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>10 Jul '16, 15:30</strong></p><img src="https://secure.gravatar.com/avatar/a00dee8f9775230dee2d5db97e3855c4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="user370&#39;s gravatar image" /><p><span>user370</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="user370 has no accepted answers">0%</span></p></div></div><div id="comments-container-53972" class="comments-container"><span id="54131"></span><div id="comment-54131" class="comment"><div id="post-54131-score" class="comment-score"></div><div class="comment-text"><p>Maybe you could post a small capture file as an example with a specific search string in mind?</p></div><div id="comment-54131-info" class="comment-info"><span class="comment-age">(18 Jul '16, 10:03)</span> <span class="comment-user userinfo">cmaynard ♦♦</span></div></div></div><div id="comment-tools-53972" class="comment-tools"></div><div class="clear"></div><div id="comment-53972-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

