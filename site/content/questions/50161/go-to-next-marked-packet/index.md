+++
type = "question"
title = "go to next marked packet"
description = '''How do I go to next marked packet in wireshark 2.0.1? Previously I was able to use key combination &quot;ctrl+shift+N&quot; but it doesn&#x27;t work anymore. Keyboard shortcut list says &quot;meta+shift+N&quot;, however I am not sure what &quot;meta&quot; means... Thanks. '''
date = "2016-02-12T11:26:00Z"
lastmod = "2016-02-12T12:30:00Z"
weight = 50161
keywords = [ "edit", "analysis" ]
aliases = [ "/questions/50161" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [go to next marked packet](/questions/50161/go-to-next-marked-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50161-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50161-score" class="post-score" title="current number of votes">0</div><span id="post-50161-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>How do I go to next marked packet in wireshark 2.0.1?</p><p>Previously I was able to use key combination "ctrl+shift+N" but it doesn't work anymore. Keyboard shortcut list says "meta+shift+N", however I am not sure what "meta" means... Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-edit" rel="tag" title="see questions tagged &#39;edit&#39;">edit</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Feb '16, 11:26</strong></p><img src="https://secure.gravatar.com/avatar/bab525eeb60b6533137f16a7f4ae4dae?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Airwire&#39;s gravatar image" /><p><span>Airwire</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Airwire has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Feb '16, 11:27</strong> </span></p></div></div><div id="comments-container-50161" class="comments-container"><span id="50162"></span><div id="comment-50162" class="comment"><div id="post-50162-score" class="comment-score"></div><div class="comment-text"><p>On which OS you're running Wireshark? Windows, Linux, Mac OS? Which language is your OS?</p><p>There is a bug in 2.0.1 (which is fixed in the master branch) with shortcuts and languages.</p><p>The normal shortcut for "go to next marked packet" on Linux and Mac OS is "Ctrl+Shift+N".</p></div><div id="comment-50162-info" class="comment-info"><span class="comment-age">(12 Feb '16, 12:21)</span> <span class="comment-user userinfo">Uli</span></div></div><span id="50164"></span><div id="comment-50164" class="comment"><div id="post-50164-score" class="comment-score"></div><div class="comment-text"><p>I run Windows 64-bit. I guess I will wait for next version...</p></div><div id="comment-50164-info" class="comment-info"><span class="comment-age">(12 Feb '16, 12:30)</span> <span class="comment-user userinfo">Airwire</span></div></div></div><div id="comment-tools-50161" class="comment-tools"></div><div class="clear"></div><div id="comment-50161-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

