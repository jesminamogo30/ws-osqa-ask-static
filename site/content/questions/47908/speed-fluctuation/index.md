+++
type = "question"
title = "Speed fluctuation"
description = '''Hi, we &quot;suffer&quot; networkproblems. Our internet provider delivers a very instable signal to us. They have sent several engineers, nobody found the &quot;problem&quot; Today they investigated for influences outside of our house and found nothing. Now the engineer told us it is possible we have a device in our ne...'''
date = "2015-11-24T00:42:00Z"
lastmod = "2015-11-24T00:42:00Z"
weight = 47908
keywords = [ "connection.speed" ]
aliases = [ "/questions/47908" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Speed fluctuation](/questions/47908/speed-fluctuation)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47908-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47908-score" class="post-score" title="current number of votes">0</div><span id="post-47908-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, we "suffer" networkproblems.</p><p>Our internet provider delivers a very instable signal to us. They have sent several engineers, nobody found the "problem" Today they investigated for influences outside of our house and found nothing.</p><p>Now the engineer told us it is possible we have a device in our network with a bad network-card that influences the performance in a bad way. I cannot deny it is a (theoretical) possibility. We have several devices, Apple, Windows, Android.</p><p>Does anybody know a tool that measures the network-performance in general over a longer period, and measures all IP's so we can find out wether there is a bad device.</p><p>Specifics: Our ISP is ziggo, it is one of the biggest providers in the Netherlands, recently merged with UPC, since the merger the problems started. Ofcourse they deny any possible connection of the merger and the problems.</p><p>The problems we have is the extreme fluctuation on speed. More then a100 mbit to less then 100 bit within seconds. No connection at all a second later. Sometimes it stays acceptable for hours, then it stays shitty for hours. We run several devices, wired and wireless, every os you can imagine.</p><p>I know unhooking everything, and hooking them up one at the time is the best thing to do, we tried that, and found nothing.</p><p>I am really looking for solutions like airsnort and wireshark. My question is, does wireshark tell me where the "rotten apple" is speedwise. Which networkcard is not doing what it should. I looked at the webinars, but did not see how i should address the problem.</p><p>Thanx in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-connection.speed" rel="tag" title="see questions tagged &#39;connection.speed&#39;">connection.speed</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Nov '15, 00:42</strong></p><img src="https://secure.gravatar.com/avatar/2a15094beca295da7a7f3e56519a2de8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Fred%20Mens&#39;s gravatar image" /><p><span>Fred Mens</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Fred Mens has no accepted answers">0%</span></p></div></div><div id="comments-container-47908" class="comments-container"></div><div id="comment-tools-47908" class="comment-tools"></div><div class="clear"></div><div id="comment-47908-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

