+++
type = "question"
title = "What hub can I use for Fios Router with Coaxial Cable input?"
description = '''I have Verizon fios and my Router does not permit port mirroring (at least that&#x27;s what I understand). Actiontec Model #: MI424WR for Verizon FiOS. I understand I can hook up a hub between the internet and the router, and then hook a computer to that hub running Wireshark which will then collect all ...'''
date = "2015-02-11T13:05:00Z"
lastmod = "2015-02-11T14:41:00Z"
weight = 39805
keywords = [ "adapter", "hub", "fios", "coaxial" ]
aliases = [ "/questions/39805" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [What hub can I use for Fios Router with Coaxial Cable input?](/questions/39805/what-hub-can-i-use-for-fios-router-with-coaxial-cable-input)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39805-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39805-score" class="post-score" title="current number of votes">0</div><span id="post-39805-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have Verizon fios and my Router does not permit port mirroring (at least that's what I understand). Actiontec Model #: MI424WR for Verizon FiOS.</p><p>I understand I can hook up a hub between the internet and the router, and then hook a computer to that hub running Wireshark which will then collect all the port data for all devices on the network. However, the incoming connection I have is Coaxial cable (and not CAT5) and I am not sure what device I can use to split it before the router?</p><p>I asked Actiontec Customer service: "Do you sell a product that I can hook up between the router and coaxial cable that will permit me to mirror all traffic? IF so please let me know what it is. Or can I upgrade the firmware to permit port mirroring?"</p><p>Their response (and I quote) "Negative, the MI424 series is the only series we make for Veirzon FIOS services.".</p><p>I found this item on Amazon which seems to look like it could be used for this purpose:</p><p>Actiontec Ethernet to Coax Adapter for Homes with Cable TV Service (ECB2500C) <a href="http://www.amazon.com/Actiontec-Ethernet-Adapter-Service-ECB2500C/dp/B008C1JC4O/ref=sr_1_2?ie=UTF8&amp;qid=1423688297&amp;sr=8-2&amp;keywords=Actiontec+adapter">http://www.amazon.com/Actiontec-Ethernet-Adapter-Service-ECB2500C/dp/B008C1JC4O/ref=sr_1_2?ie=UTF8&amp;qid=1423688297&amp;sr=8-2&amp;keywords=Actiontec+adapter</a></p><p>But to be honest I don't even know what it is normally used for.</p><p>I also know I can probably replace the router with one that mirrors the ports. But I am looking for a simpler solution since that seems beyond my current abilities.</p><p>Any help would be appreciated.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-adapter" rel="tag" title="see questions tagged &#39;adapter&#39;">adapter</span> <span class="post-tag tag-link-hub" rel="tag" title="see questions tagged &#39;hub&#39;">hub</span> <span class="post-tag tag-link-fios" rel="tag" title="see questions tagged &#39;fios&#39;">fios</span> <span class="post-tag tag-link-coaxial" rel="tag" title="see questions tagged &#39;coaxial&#39;">coaxial</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>11 Feb '15, 13:05</strong></p><img src="https://secure.gravatar.com/avatar/ba8bc718e34587f5b7d2a26ac831415e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Harry%20Black&#39;s gravatar image" /><p><span>Harry Black</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Harry Black has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>11 Feb '15, 13:07</strong> </span></p></div></div><div id="comments-container-39805" class="comments-container"><span id="39810"></span><div id="comment-39810" class="comment"><div id="post-39810-score" class="comment-score"></div><div class="comment-text"><p>In doing a little research, I was wondering if this will work: coaxial cable plugs into the Actiontec (WiFi and DHCP disabled), an Ethernet cable from there connects to a second router with port mirroring from which all connections are made. If so, how would this affect the fios television (if at all)?</p></div><div id="comment-39810-info" class="comment-info"><span class="comment-age">(11 Feb '15, 14:14)</span> <span class="comment-user userinfo">Harry Black</span></div></div></div><div id="comment-tools-39805" class="comment-tools"></div><div class="clear"></div><div id="comment-39805-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="39815"></span>

<div id="answer-container-39815" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-39815-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-39815-score" class="post-score" title="current number of votes">0</div><span id="post-39815-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><blockquote><p>But to be honest I don't even know what it is normally used for.</p></blockquote><p>Cite: "The MoCA Network Adapter is a high-speed bridge that takes your coaxial network and, in conjunction with a router, delivers Ethernet networking access anywhere you have a coaxial port.".</p><p>Anyway, that device won't help you and there is no (cheap) equipment you can buy on the free market to intercept traffic on a coax cable. The cable modem is used to "transcode" your IP frames to something called <a href="http://en.wikipedia.org/wiki/DOCSIS">DOCSIS</a>. Yes, there are DOCSIS protocol analyzers available, but they cost a fortune or even more ;-)</p><p>So, your only (realistic) option is to place the hub (or a switch with port mirroring) between your router and the network on the <strong>LAN side</strong> of your router to capture the whole internet traffic.</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>11 Feb '15, 14:41</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></p></div></div><div id="comments-container-39815" class="comments-container"></div><div id="comment-tools-39815" class="comment-tools"></div><div class="clear"></div><div id="comment-39815-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

