+++
type = "question"
title = "Wireshark 1.8.2 is only displaying 2 filters from the drop-down menu"
description = '''I have recently upgraded to Wireshark 1.8.2 on Windows 7 (64-bit). On the top menu bar, the filter area is only displaying the latest 2 created filters. In previous Wireshark versions, I was able to view at least the latest 10 or more filters. I have made changes to the following areas in Wireshark:...'''
date = "2012-09-17T07:21:00Z"
lastmod = "2012-09-17T11:06:00Z"
weight = 14319
keywords = [ "filter", "displayed", "1.8.2", "wireshark" ]
aliases = [ "/questions/14319" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Wireshark 1.8.2 is only displaying 2 filters from the drop-down menu](/questions/14319/wireshark-182-is-only-displaying-2-filters-from-the-drop-down-menu)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14319-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14319-score" class="post-score" title="current number of votes">0</div><span id="post-14319-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have recently upgraded to Wireshark 1.8.2 on Windows 7 (64-bit). On the top menu bar, the filter area is only displaying the latest 2 created filters. In previous Wireshark versions, I was able to view at least the latest 10 or more filters. I have made changes to the following areas in Wireshark: 1) Edit -&gt; Preferences -&gt; User Interface -&gt; Filter display max. list entries = 20 2) C:\Users\acarbonara\AppData\Roaming\Wireshark\prefernces: # The max. number of entries in the display filter list. # A decimal number. gui.recent_display_filter_entries.max: 20</p><p>But still only seeing 2 filters from the drop-down menu</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-displayed" rel="tag" title="see questions tagged &#39;displayed&#39;">displayed</span> <span class="post-tag tag-link-1.8.2" rel="tag" title="see questions tagged &#39;1.8.2&#39;">1.8.2</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Sep '12, 07:21</strong></p><img src="https://secure.gravatar.com/avatar/d9cf592a79eafbc3b2a8b3f38cf38362?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Amato_C&#39;s gravatar image" /><p><span>Amato_C</span><br />
<span class="score" title="1098 reputation points"><span>1.1k</span></span><span title="14 badges"><span class="badge1">●</span><span class="badgecount">14</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="32 badges"><span class="bronze">●</span><span class="badgecount">32</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Amato_C has 15 accepted answers">14%</span></p></div></div><div id="comments-container-14319" class="comments-container"></div><div id="comment-tools-14319" class="comment-tools"></div><div class="clear"></div><div id="comment-14319-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="14320"></span>

<div id="answer-container-14320" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-14320-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-14320-score" class="post-score" title="current number of votes">0</div><span id="post-14320-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Amato_C has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>I remember I have seen the same behaviour on one of my machines, but I'm not sure which one it was so I can't check right now. I think this might be a GUI bug and have done a search on the bug tracker for it, but did not find anything. I think you should open a new bug at <a href="http://bugs.wireshark.org/">http://bugs.wireshark.org/</a>, and include a screen shot of the shortened drop down list as well as the usual build information.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>17 Sep '12, 07:32</strong></p><img src="https://secure.gravatar.com/avatar/c578ba2967741f25aebd6afef702f432?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jasper&#39;s gravatar image" /><p><span>Jasper ♦♦</span><br />
<span class="score" title="23806 reputation points"><span>23.8k</span></span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="51 badges"><span class="silver">●</span><span class="badgecount">51</span></span><span title="284 badges"><span class="bronze">●</span><span class="badgecount">284</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jasper has 263 accepted answers">18%</span></p></div></div><div id="comments-container-14320" class="comments-container"><span id="14327"></span><div id="comment-14327" class="comment"><div id="post-14327-score" class="comment-score"></div><div class="comment-text"><p>I tried both the 64 and 32 bit versions of Wireshark 1.8.2 Issues still persists Created bug 7732</p></div><div id="comment-14327-info" class="comment-info"><span class="comment-age">(17 Sep '12, 11:06)</span> <span class="comment-user userinfo">Amato_C</span></div></div></div><div id="comment-tools-14320" class="comment-tools"></div><div class="clear"></div><div id="comment-14320-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

