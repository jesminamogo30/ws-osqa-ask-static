+++
type = "question"
title = "apply tshark patch to filter dynamic http header fields"
description = '''I was wondering if someone could help me getting this patch to work. I am interested in the ability to parse out the x-wap-profile field that this user was able to accomplish. I am unsure how I need to compile or what the process is so that I can use tshark in the manner described in the wireshark B...'''
date = "2013-02-06T17:17:00Z"
lastmod = "2013-02-07T15:58:00Z"
weight = 18382
keywords = [ "filter", "tshark", "patch" ]
aliases = [ "/questions/18382" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [apply tshark patch to filter dynamic http header fields](/questions/18382/apply-tshark-patch-to-filter-dynamic-http-header-fields)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18382-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18382-score" class="post-score" title="current number of votes">0</div><span id="post-18382-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I was wondering if someone could help me getting this patch to work. I am interested in the ability to parse out the x-wap-profile field that this user was able to accomplish. I am unsure how I need to compile or what the process is so that I can use tshark in the manner described in the wireshark Bug referenced below.</p><p>Appreciate the assistance.</p><p>Wireshark-bugs: [Wireshark-bugs] [Bug 3242] Dynamic header fields for HTTP</p><p><a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3242">https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3242</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-filter" rel="tag" title="see questions tagged &#39;filter&#39;">filter</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-patch" rel="tag" title="see questions tagged &#39;patch&#39;">patch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>06 Feb '13, 17:17</strong></p><img src="https://secure.gravatar.com/avatar/8ceffbaf0e6f49f46cca40225034f76d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="boardermartin&#39;s gravatar image" /><p><span>boardermartin</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="boardermartin has no accepted answers">0%</span></p></div></div><div id="comments-container-18382" class="comments-container"></div><div id="comment-tools-18382" class="comment-tools"></div><div class="clear"></div><div id="comment-18382-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="18387"></span>

<div id="answer-container-18387" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-18387-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-18387-score" class="post-score" title="current number of votes">3</div><span id="post-18387-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="boardermartin has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The bug's state is RESOLVED FIXED, with in the comments the remark that it's committed, so there's nothing you need to compile. Just a recent enough Wireshark version will do.</p><p>What you do need to do is setup a UAT (User Access Table) with the desired configuration. You usually take Wireshark to do this. Go to the HTTP protocol preferences and click Edit at Custom HTTP header fields. There you fill in what you need.</p><p>This then is stored in your profile with the other Wireshark settings in a file called <code>custom_http_header_fields</code>, which looks something like this:</p><pre><code># This file is automatically generated, DO NOT MODIFY.
&quot;x-wap-profile&quot;,&quot;The WAP profile&quot;</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>06 Feb '13, 22:12</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-18387" class="comments-container"><span id="18424"></span><div id="comment-18424" class="comment"><div id="post-18424-score" class="comment-score"></div><div class="comment-text"><p>Worked out perfect, thanks for the assistance!</p></div><div id="comment-18424-info" class="comment-info"><span class="comment-age">(07 Feb '13, 15:58)</span> <span class="comment-user userinfo">boardermartin</span></div></div></div><div id="comment-tools-18387" class="comment-tools"></div><div class="clear"></div><div id="comment-18387-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

