+++
type = "question"
title = "How can follow the tcp stream in batch?"
description = '''Hello, Wireshark is an excellent network tool, and I like the follow tcp/udp etc stream way that convert the raw data into a readable format, but how can I use it in a batch mode? You can image that if I want to watch thousands of data, had I click the items with mouse one by one? Any tips?  regards...'''
date = "2012-03-05T02:16:00Z"
lastmod = "2012-03-05T09:26:00Z"
weight = 9354
keywords = [ "tcp.stream", "wireshark" ]
aliases = [ "/questions/9354" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [How can follow the tcp stream in batch?](/questions/9354/how-can-follow-the-tcp-stream-in-batch)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9354-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9354-score" class="post-score" title="current number of votes">0</div><span id="post-9354-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,<br />
Wireshark is an excellent network tool, and I like the follow tcp/udp etc stream way that convert the raw data into a readable format, but how can I use it in a batch mode? You can image that if I want to watch thousands of data, had I click the items with mouse one by one?<br />
Any tips?<br />
</p><p>regards.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tcp.stream" rel="tag" title="see questions tagged &#39;tcp.stream&#39;">tcp.stream</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Mar '12, 02:16</strong></p><img src="https://secure.gravatar.com/avatar/d7511cd99041bcb5eda1ff4b6792b8c8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="liunx&#39;s gravatar image" /><p><span>liunx</span><br />
<span class="score" title="16 reputation points">16</span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="3 badges"><span class="silver">●</span><span class="badgecount">3</span></span><span title="6 badges"><span class="bronze">●</span><span class="badgecount">6</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="liunx has no accepted answers">0%</span> </br></br></p></div></div><div id="comments-container-9354" class="comments-container"></div><div id="comment-tools-9354" class="comment-tools"></div><div class="clear"></div><div id="comment-9354-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9362"></span>

<div id="answer-container-9362" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9362-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9362-score" class="post-score" title="current number of votes">1</div><span id="post-9362-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="liunx has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>The development version allows TCP and UDP streams to be "followed" in tshark. See bug <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=6684">6684</a>. This feature will show up in 1.8.0 (or 1.7.1).</p><p>[Update] Don't forget to drop by and Accept this answer if it answered your question.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Mar '12, 09:26</strong></p><img src="https://secure.gravatar.com/avatar/e0564001bb7deb960d5d9d9c1e0ba074?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="JeffMorriss&#39;s gravatar image" /><p><span>JeffMorriss ♦</span><br />
<span class="score" title="6219 reputation points"><span>6.2k</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="72 badges"><span class="bronze">●</span><span class="badgecount">72</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="JeffMorriss has 103 accepted answers">27%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>09 Mar '12, 06:58</strong> </span></p></div></div><div id="comments-container-9362" class="comments-container"></div><div id="comment-tools-9362" class="comment-tools"></div><div class="clear"></div><div id="comment-9362-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

