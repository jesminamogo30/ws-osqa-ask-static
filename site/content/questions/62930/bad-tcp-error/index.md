+++
type = "question"
title = "Bad TCP error"
description = '''I experienced a lot of &quot;Out of order&quot; TCP errors, but there is only one route and no chance to out-of-order.  And I had reached this site. Does this site tell truth? http://milestone-of-se.nesuke.com/en/knowhow/wireshark-tcp-error/'''
date = "2017-07-20T09:20:00Z"
lastmod = "2017-07-20T09:20:00Z"
weight = 62930
keywords = [ "bad", "tcp" ]
aliases = [ "/questions/62930" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Bad TCP error](/questions/62930/bad-tcp-error)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-62930-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-62930-score" class="post-score" title="current number of votes">0</div><span id="post-62930-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I experienced a lot of "Out of order" TCP errors, but there is only one route and no chance to out-of-order.</p><p>And I had reached this site. Does this site tell truth? <a href="http://milestone-of-se.nesuke.com/en/knowhow/wireshark-tcp-error/">http://milestone-of-se.nesuke.com/en/knowhow/wireshark-tcp-error/</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bad" rel="tag" title="see questions tagged &#39;bad&#39;">bad</span> <span class="post-tag tag-link-tcp" rel="tag" title="see questions tagged &#39;tcp&#39;">tcp</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jul '17, 09:20</strong></p><img src="https://secure.gravatar.com/avatar/350bad269b0300a2994af4fb681ecd38?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="duckn58&#39;s gravatar image" /><p><span>duckn58</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="duckn58 has no accepted answers">0%</span></p></div></div><div id="comments-container-62930" class="comments-container"></div><div id="comment-tools-62930" class="comment-tools"></div><div class="clear"></div><div id="comment-62930-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

