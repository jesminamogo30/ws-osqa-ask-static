+++
type = "question"
title = "Remote proxy based analysis on US product from UK"
description = '''I want to run analysis on a US based product (DISH Anywhere) from the UK via a proxy server. I have a US account and login details (via a friend) but he is not able to perform the analysis. Any insight into this would be greatly appreciated Kind regards SMXNCC'''
date = "2016-07-29T01:39:00Z"
lastmod = "2016-07-29T05:55:00Z"
weight = 54432
keywords = [ "proxy-server", "remote-capture", "remote", "proxy", "remote-monitoring" ]
aliases = [ "/questions/54432" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Remote proxy based analysis on US product from UK](/questions/54432/remote-proxy-based-analysis-on-us-product-from-uk)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54432-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54432-score" class="post-score" title="current number of votes">0</div><span id="post-54432-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I want to run analysis on a US based product (<a href="http://www.dishanywhere.com">DISH Anywhere</a>) from the UK via a proxy server.</p><p>I have a US account and login details (via a friend) but he is not able to perform the analysis.</p><p>Any insight into this would be greatly appreciated</p><p>Kind regards</p><p>SMXNCC</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-proxy-server" rel="tag" title="see questions tagged &#39;proxy-server&#39;">proxy-server</span> <span class="post-tag tag-link-remote-capture" rel="tag" title="see questions tagged &#39;remote-capture&#39;">remote-capture</span> <span class="post-tag tag-link-remote" rel="tag" title="see questions tagged &#39;remote&#39;">remote</span> <span class="post-tag tag-link-proxy" rel="tag" title="see questions tagged &#39;proxy&#39;">proxy</span> <span class="post-tag tag-link-remote-monitoring" rel="tag" title="see questions tagged &#39;remote-monitoring&#39;">remote-monitoring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Jul '16, 01:39</strong></p><img src="https://secure.gravatar.com/avatar/ded82f66e772a010ebf0be31a3652084?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="smxncc&#39;s gravatar image" /><p><span>smxncc</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="smxncc has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>29 Jul '16, 04:51</strong> </span></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span></p></div></div><div id="comments-container-54432" class="comments-container"><span id="54442"></span><div id="comment-54442" class="comment"><div id="post-54442-score" class="comment-score"></div><div class="comment-text"><p>What kind of setup would that 'proxy server' be?</p></div><div id="comment-54442-info" class="comment-info"><span class="comment-age">(29 Jul '16, 04:52)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="54443"></span><div id="comment-54443" class="comment"><div id="post-54443-score" class="comment-score"></div><div class="comment-text"><p>I was hoping it could be just a browser based proxy so I can log onto the dish anywhere site and run analysis on some on demand shows.</p><p>Is this feasible?</p></div><div id="comment-54443-info" class="comment-info"><span class="comment-age">(29 Jul '16, 05:55)</span> <span class="comment-user userinfo">smxncc</span></div></div></div><div id="comment-tools-54432" class="comment-tools"></div><div class="clear"></div><div id="comment-54432-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

