+++
type = "question"
title = "Problem trying to convert a text file to a pcap file"
description = '''I&#x27;m running into the same problem, except my file contains actual packet content. I&#x27;m capturing data directly from a device and getting data, but the target file is empty after the conversion. Any ideas? I can supply more info if needed.'''
date = "2016-08-22T15:20:00Z"
lastmod = "2016-08-22T18:22:00Z"
weight = 55055
keywords = [ "text", "convert", "pcap" ]
aliases = [ "/questions/55055" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Problem trying to convert a text file to a pcap file](/questions/55055/problem-trying-to-convert-a-text-file-to-a-pcap-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55055-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55055-score" class="post-score" title="current number of votes">0</div><span id="post-55055-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm running into the same problem, except my file contains actual packet content. I'm capturing data directly from a device and getting data, but the target file is empty after the conversion. Any ideas? I can supply more info if needed.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-text" rel="tag" title="see questions tagged &#39;text&#39;">text</span> <span class="post-tag tag-link-convert" rel="tag" title="see questions tagged &#39;convert&#39;">convert</span> <span class="post-tag tag-link-pcap" rel="tag" title="see questions tagged &#39;pcap&#39;">pcap</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>22 Aug '16, 15:20</strong></p><img src="https://secure.gravatar.com/avatar/ec44e20a2118c902f24aa9d303372d66?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="azbird&#39;s gravatar image" /><p><span>azbird</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="azbird has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> converted to question <strong>22 Aug '16, 18:20</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-55055" class="comments-container"><span id="55061"></span><div id="comment-55061" class="comment"><div id="post-55061-score" class="comment-score"></div><div class="comment-text"><p>So by "actual packet content" do you mean "a bunch of hex numbers giving the raw bytes in the packet"? The text format that Wireshark's "import" function, and its text2pcap command, handle is described in <a href="https://www.wireshark.org/docs/man-pages/text2pcap.html">the text2pcap man page</a>; is it in a format like that?</p></div><div id="comment-55061-info" class="comment-info"><span class="comment-age">(22 Aug '16, 18:22)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-55055" class="comment-tools"></div><div class="clear"></div><div id="comment-55055-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

