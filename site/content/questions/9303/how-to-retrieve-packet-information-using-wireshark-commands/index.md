+++
type = "question"
title = "how to retrieve packet information using wireshark commands"
description = '''I have pcap file containing all the packets captured for certain duration.Given a packet type and tag number i have to get all the information of that tagged parameter from the command line.how to do this? '''
date = "2012-03-01T22:41:00Z"
lastmod = "2012-03-02T04:18:00Z"
weight = 9303
keywords = [ "display-filter", "capture-filter", "tshark", "analysis", "wireshark" ]
aliases = [ "/questions/9303" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [how to retrieve packet information using wireshark commands](/questions/9303/how-to-retrieve-packet-information-using-wireshark-commands)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9303-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9303-score" class="post-score" title="current number of votes">0</div><span id="post-9303-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have pcap file containing all the packets captured for certain duration.Given a packet type and tag number i have to get all the information of that tagged parameter from the command line.how to do this?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-display-filter" rel="tag" title="see questions tagged &#39;display-filter&#39;">display-filter</span> <span class="post-tag tag-link-capture-filter" rel="tag" title="see questions tagged &#39;capture-filter&#39;">capture-filter</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-analysis" rel="tag" title="see questions tagged &#39;analysis&#39;">analysis</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Mar '12, 22:41</strong></p><img src="https://secure.gravatar.com/avatar/f46c9e12b1c2186822b2edd8f11c9a2e?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="automation&#39;s gravatar image" /><p><span>automation</span><br />
<span class="score" title="1 reputation points">1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="automation has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>02 Mar '12, 02:08</strong> </span></p></div></div><div id="comments-container-9303" class="comments-container"></div><div id="comment-tools-9303" class="comment-tools"></div><div class="clear"></div><div id="comment-9303-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="9308"></span>

<div id="answer-container-9308" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9308-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9308-score" class="post-score" title="current number of votes">0</div><span id="post-9308-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Study <a href="http://www.wireshark.org/docs/wsug_html_chunked/AppToolstshark.html">tshark</a>, of which the manual can be found <a href="http://www.wireshark.org/docs/man-pages/tshark.html">here</a>.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>02 Mar '12, 04:18</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-9308" class="comments-container"></div><div id="comment-tools-9308" class="comment-tools"></div><div class="clear"></div><div id="comment-9308-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

