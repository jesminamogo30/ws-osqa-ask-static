+++
type = "question"
title = "[closed] [beginner]How to intercept traffic bringing out and entering on a machine distant ?"
description = '''Hi, I work on WireShark on Windows 7 Pro. How do I have to make exactly for: -1)to intercept the traffic of packages bringing out of a machine distant from address ipv4:XXX.XXX.XXX.XXX ? -2)to intercept the traffic of packages entering towards a machine distant from address ipv4:XXX.XXX.XXX.XXX ? Th...'''
date = "2016-08-23T07:01:00Z"
lastmod = "2016-08-23T08:00:00Z"
weight = 55074
keywords = [ "#velvet" ]
aliases = [ "/questions/55074" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] \[beginner\]How to intercept traffic bringing out and entering on a machine distant ?](/questions/55074/beginnerhow-to-intercept-traffic-bringing-out-and-entering-on-a-machine-distant)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-55074-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-55074-score" class="post-score" title="current number of votes">0</div><span id="post-55074-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I work on WireShark on Windows 7 Pro.</p><p>How do I have to make exactly for: -1)to intercept the traffic of packages bringing out of a machine distant from address ipv4:XXX.XXX.XXX.XXX ? -2)to intercept the traffic of packages entering towards a machine distant from address ipv4:XXX.XXX.XXX.XXX ?</p><p>Thank you for your help</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-#velvet" rel="tag" title="see questions tagged &#39;#velvet&#39;">#velvet</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Aug '16, 07:01</strong></p><img src="https://secure.gravatar.com/avatar/56d94591c91e28d29d99fb214a880aff?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Tercyanos&#39;s gravatar image" /><p><span>Tercyanos</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Tercyanos has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>23 Aug '16, 08:00</strong> </span></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span></p></div></div><div id="comments-container-55074" class="comments-container"><span id="55076"></span><div id="comment-55076" class="comment"><div id="post-55076-score" class="comment-score"></div><div class="comment-text"><p>Please don't repost the same question.</p></div><div id="comment-55076-info" class="comment-info"><span class="comment-age">(23 Aug '16, 08:00)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-55074" class="comment-tools"></div><div class="clear"></div><div id="comment-55074-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Duplicate Question" by grahamb 23 Aug '16, 08:00

</div>

</div>

</div>

