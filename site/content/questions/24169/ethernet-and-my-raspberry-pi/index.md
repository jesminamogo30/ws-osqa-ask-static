+++
type = "question"
title = "Ethernet and my Raspberry Pi"
description = '''Hello all, I recently purchased a Raspberry Pi with the hopes of making an inexpensive Bluetooth Packet sniffer.  I have WireShark on my Windows PC and am unable to capture BT data(I know it&#x27;s not supported). My plan is to capture the data using the Pi and dump the data over Ethernet to my PC. My qu...'''
date = "2013-08-29T09:13:00Z"
lastmod = "2013-08-29T09:13:00Z"
weight = 24169
keywords = [ "ethernet", "pi", "ftp", "raspberry", "bluetooth" ]
aliases = [ "/questions/24169" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Ethernet and my Raspberry Pi](/questions/24169/ethernet-and-my-raspberry-pi)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-24169-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-24169-score" class="post-score" title="current number of votes">0</div><span id="post-24169-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello all,</p><p>I recently purchased a Raspberry Pi with the hopes of making an inexpensive Bluetooth Packet sniffer. I have WireShark on my Windows PC and am unable to capture BT data(I know it's not supported). My plan is to capture the data using the Pi and dump the data over Ethernet to my PC. My question is: Will this method allow for a legible readout of the BT data that was captured by the Pi?</p><p>Any comments or advice are greatly appreciated. Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ethernet" rel="tag" title="see questions tagged &#39;ethernet&#39;">ethernet</span> <span class="post-tag tag-link-pi" rel="tag" title="see questions tagged &#39;pi&#39;">pi</span> <span class="post-tag tag-link-ftp" rel="tag" title="see questions tagged &#39;ftp&#39;">ftp</span> <span class="post-tag tag-link-raspberry" rel="tag" title="see questions tagged &#39;raspberry&#39;">raspberry</span> <span class="post-tag tag-link-bluetooth" rel="tag" title="see questions tagged &#39;bluetooth&#39;">bluetooth</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>29 Aug '13, 09:13</strong></p><img src="https://secure.gravatar.com/avatar/49fdf50a3625e83b3ef61f06927aa745?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="bukkockey&#39;s gravatar image" /><p><span>bukkockey</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="bukkockey has no accepted answers">0%</span></p></div></div><div id="comments-container-24169" class="comments-container"></div><div id="comment-tools-24169" class="comment-tools"></div><div class="clear"></div><div id="comment-24169-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

