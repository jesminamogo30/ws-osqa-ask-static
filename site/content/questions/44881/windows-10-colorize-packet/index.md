+++
type = "question"
title = "Windows 10 Colorize Packet"
description = '''Application acts like Colorize Packet List is Disabled, no colors just white background with black text, while under the menu option View/Colorize Pack List i have a check mark?'''
date = "2015-08-05T12:21:00Z"
lastmod = "2015-08-05T12:27:00Z"
weight = 44881
keywords = [ "coloring" ]
aliases = [ "/questions/44881" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Windows 10 Colorize Packet](/questions/44881/windows-10-colorize-packet)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44881-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44881-score" class="post-score" title="current number of votes">0</div><span id="post-44881-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Application acts like Colorize Packet List is Disabled, no colors just white background with black text, while under the menu option View/Colorize Pack List i have a check mark?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-coloring" rel="tag" title="see questions tagged &#39;coloring&#39;">coloring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>05 Aug '15, 12:21</strong></p><img src="https://secure.gravatar.com/avatar/6a39eb039cfb06138b33ef5ef3925aa5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jdmcastro&#39;s gravatar image" /><p><span>jdmcastro</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jdmcastro has no accepted answers">0%</span></p></div></div><div id="comments-container-44881" class="comments-container"></div><div id="comment-tools-44881" class="comment-tools"></div><div class="clear"></div><div id="comment-44881-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="44882"></span>

<div id="answer-container-44882" class="answer answered-by-owner">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-44882-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-44882-score" class="post-score" title="current number of votes">0</div><span id="post-44882-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Uninstall and reinstall fixed... ?</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>05 Aug '15, 12:27</strong></p><img src="https://secure.gravatar.com/avatar/6a39eb039cfb06138b33ef5ef3925aa5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="jdmcastro&#39;s gravatar image" /><p><span>jdmcastro</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="jdmcastro has no accepted answers">0%</span></p></div></div><div id="comments-container-44882" class="comments-container"></div><div id="comment-tools-44882" class="comment-tools"></div><div class="clear"></div><div id="comment-44882-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

