+++
type = "question"
title = "Coloring Rules Crash"
description = '''When I find an object to filter for a coloring rule. It crashes wireshark 1.10.6 and 1.10.8. I am running OS X Snow Leopard. It is when I choose a color, I see the hex code populate, and click OK. It pauses like it is configuring it, then crashes. Anyone have this issue or should I go the bug route....'''
date = "2014-07-23T09:37:00Z"
lastmod = "2014-07-23T15:15:00Z"
weight = 34853
keywords = [ "coloring" ]
aliases = [ "/questions/34853" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Coloring Rules Crash](/questions/34853/coloring-rules-crash)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34853-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34853-score" class="post-score" title="current number of votes">0</div><span id="post-34853-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I find an object to filter for a coloring rule. It crashes wireshark 1.10.6 and 1.10.8. I am running OS X Snow Leopard. It is when I choose a color, I see the hex code populate, and click OK. It pauses like it is configuring it, then crashes. Anyone have this issue or should I go the bug route. Would the bug be addressed at this point with the next major wireshark version coming and Snow Leopard no longer supported by Apple?</p><p>Sincerely, tsharkoatech</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-coloring" rel="tag" title="see questions tagged &#39;coloring&#39;">coloring</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>23 Jul '14, 09:37</strong></p><img src="https://secure.gravatar.com/avatar/6e9e15ae18848195be4020824cc3d8b4?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tsharkoatech&#39;s gravatar image" /><p><span>tsharkoatech</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tsharkoatech has no accepted answers">0%</span></p></div></div><div id="comments-container-34853" class="comments-container"></div><div id="comment-tools-34853" class="comment-tools"></div><div class="clear"></div><div id="comment-34853-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="34858"></span>

<div id="answer-container-34858" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-34858-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-34858-score" class="post-score" title="current number of votes">0</div><span id="post-34858-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Go the bug route. Whether the bug would be addressed with the next Wireshark <em>minor</em> version depends on what's causing it, but if we can determine what the cause is (which is almost certainly an issue completely unrelated to whether Apple supports Snow Leopard or not) and fix it in time, it'll be addressed in 1.10.9.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>23 Jul '14, 15:15</strong></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Guy Harris has 216 accepted answers">19%</span></p></div></div><div id="comments-container-34858" class="comments-container"></div><div id="comment-tools-34858" class="comment-tools"></div><div class="clear"></div><div id="comment-34858-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

