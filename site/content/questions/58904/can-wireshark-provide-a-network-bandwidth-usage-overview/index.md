+++
type = "question"
title = "Can Wireshark provide a network bandwidth usage overview"
description = '''I run a shared internet data feed into a 30 unit apartment block where there appears be some abnormally high usage in contravention of the usage policy. A common fibre router splits through two hubs via wired ethernet into each apartment from where most users have a wireless router to distribute the...'''
date = "2017-01-20T05:06:00Z"
lastmod = "2017-01-20T05:06:00Z"
weight = 58904
keywords = [ "bandwidth", "monitor" ]
aliases = [ "/questions/58904" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Can Wireshark provide a network bandwidth usage overview](/questions/58904/can-wireshark-provide-a-network-bandwidth-usage-overview)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58904-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58904-score" class="post-score" title="current number of votes">0</div><span id="post-58904-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I run a shared internet data feed into a 30 unit apartment block where there appears be some abnormally high usage in contravention of the usage policy.</p><p>A common fibre router splits through two hubs via wired ethernet into each apartment from where most users have a wireless router to distribute their feed.</p><p>Wireshark was recommended to me as a good way to get a bandwidth and file type report that would enable me to track down the heavy hitters.</p><p>Can anyone confirm that this would be possible - and if so how to configure the software.?</p><p>thanks</p><p>.les.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-bandwidth" rel="tag" title="see questions tagged &#39;bandwidth&#39;">bandwidth</span> <span class="post-tag tag-link-monitor" rel="tag" title="see questions tagged &#39;monitor&#39;">monitor</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>20 Jan '17, 05:06</strong></p><img src="https://secure.gravatar.com/avatar/e8c6bdcf9fa3f25e40397035544a073b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="lesjuby&#39;s gravatar image" /><p><span>lesjuby</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="lesjuby has no accepted answers">0%</span></p></div></div><div id="comments-container-58904" class="comments-container"></div><div id="comment-tools-58904" class="comment-tools"></div><div class="clear"></div><div id="comment-58904-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

