+++
type = "question"
title = "Can&#x27;t shrink the size of packets in a pcap"
description = '''Try to shrink a pcap by make each packet at most 500 bytes, the following command doesn&#x27;t seem to work: the resulted file packets in &quot;delme.pcap&quot; is still bigger than 500 bytes. tshark -r out.pcap -s 500 -w delme.pcap  Could it be because my tshark (v 1.10.6) is not too old? Pcap is here, note that ...'''
date = "2015-09-09T21:27:00Z"
lastmod = "2015-09-10T07:25:00Z"
weight = 45742
keywords = [ "tshark" ]
aliases = [ "/questions/45742" ]
osqa_answers = 1
osqa_accepted = true
+++

<div class="headNormal">

# [Can't shrink the size of packets in a pcap](/questions/45742/cant-shrink-the-size-of-packets-in-a-pcap)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45742-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45742-score" class="post-score" title="current number of votes">0</div><span id="post-45742-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Try to shrink a pcap by make each packet at most 500 bytes, the following command doesn't seem to work: the resulted file packets in "delme.pcap" is still bigger than 500 bytes.</p><pre><code>tshark -r out.pcap -s 500 -w delme.pcap</code></pre><p>Could it be because my tshark (v 1.10.6) is not too old? Pcap is <a href="https://www.cloudshark.org/captures/3577f8af01f9">here</a>, note that it contains a malware.<br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Sep '15, 21:27</strong></p><img src="https://secure.gravatar.com/avatar/7bb7310612573625abd07a67f22724ad?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="pktUser1001&#39;s gravatar image" /><p><span>pktUser1001</span><br />
<span class="score" title="201 reputation points">201</span><span title="49 badges"><span class="badge1">●</span><span class="badgecount">49</span></span><span title="50 badges"><span class="silver">●</span><span class="badgecount">50</span></span><span title="54 badges"><span class="bronze">●</span><span class="badgecount">54</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="pktUser1001 has one accepted answer">12%</span> </br></p></div></div><div id="comments-container-45742" class="comments-container"></div><div id="comment-tools-45742" class="comment-tools"></div><div class="clear"></div><div id="comment-45742-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="45743"></span>

<div id="answer-container-45743" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-45743-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-45743-score" class="post-score" title="current number of votes">1</div><span id="post-45743-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="pktUser1001 has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>the -s parameter in tshark command line will be used during a live capture, not when reading an existing pcap file.</p><p>If you want to modify an existing pcap, use editcap instead:</p><pre><code>editcap -s 500 out.pcap delme.pcap</code></pre></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Sep '15, 22:23</strong></p><img src="https://secure.gravatar.com/avatar/713f24fd877861260b71ecd455018625?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Pascal%20Quantin&#39;s gravatar image" /><p><span>Pascal Quantin</span><br />
<span class="score" title="5544 reputation points"><span>5.5k</span></span><span title="10 badges"><span class="silver">●</span><span class="badgecount">10</span></span><span title="60 badges"><span class="bronze">●</span><span class="badgecount">60</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Pascal Quantin has 92 accepted answers">30%</span></p></div></div><div id="comments-container-45743" class="comments-container"><span id="45760"></span><div id="comment-45760" class="comment"><div id="post-45760-score" class="comment-score"></div><div class="comment-text"><p>Thanks <span>@pascal</span>-quantin for the explanation!</p></div><div id="comment-45760-info" class="comment-info"><span class="comment-age">(10 Sep '15, 07:25)</span> <span class="comment-user userinfo">pktUser1001</span></div></div></div><div id="comment-tools-45743" class="comment-tools"></div><div class="clear"></div><div id="comment-45743-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

