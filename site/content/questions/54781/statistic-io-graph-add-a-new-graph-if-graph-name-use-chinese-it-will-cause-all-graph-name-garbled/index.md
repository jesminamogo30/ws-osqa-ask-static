+++
type = "question"
title = "[closed] Statistic---&gt; I/O Graph---&gt;add a new graph, if graph  name use Chinese it will cause all graph name garbled"
description = '''Statistic---&amp;gt; I/O Graph---&amp;gt;add a new graph, graph name use Chinese ,close and reopen wireshark, all graph name will cause garbled  All Version 2.x.x will cause all graph name garbled  Pls add more Color to display more graph item  '''
date = "2016-08-13T07:59:00Z"
lastmod = "2016-08-13T08:03:00Z"
weight = 54781
keywords = [ "graph", "configuration", "wireshark-2.0", "error", "iograph" ]
aliases = [ "/questions/54781" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Statistic---&gt; I/O Graph---&gt;add a new graph, if graph name use Chinese it will cause all graph name garbled](/questions/54781/statistic-io-graph-add-a-new-graph-if-graph-name-use-chinese-it-will-cause-all-graph-name-garbled)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54781-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54781-score" class="post-score" title="current number of votes">0</div><span id="post-54781-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Statistic---&gt; I/O Graph---&gt;add a new graph, graph name use Chinese ,close and reopen wireshark, all graph name will cause garbled</p><p>All Version 2.x.x will cause all graph name garbled</p><p>Pls add more Color to display more graph item</p><p><img src="https://osqa-ask.wireshark.org/upfiles/io_K5EsaHP.PNG" alt="graph name use Chinese will cause all graph name garbled" /><br />
</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-graph" rel="tag" title="see questions tagged &#39;graph&#39;">graph</span> <span class="post-tag tag-link-configuration" rel="tag" title="see questions tagged &#39;configuration&#39;">configuration</span> <span class="post-tag tag-link-wireshark-2.0" rel="tag" title="see questions tagged &#39;wireshark-2.0&#39;">wireshark-2.0</span> <span class="post-tag tag-link-error" rel="tag" title="see questions tagged &#39;error&#39;">error</span> <span class="post-tag tag-link-iograph" rel="tag" title="see questions tagged &#39;iograph&#39;">iograph</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>13 Aug '16, 07:59</strong></p><img src="https://secure.gravatar.com/avatar/ad2e75fa48eead4fe1005e614be2722c?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="tqangxl&#39;s gravatar image" /><p><span>tqangxl</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="tqangxl has no accepted answers">0%</span> </br></p></img></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>13 Aug '16, 08:04</strong> </span></p><img src="https://secure.gravatar.com/avatar/00fc6e2633725bd871ff636f0175eabc?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="sindy&#39;s gravatar image" /><p><span>sindy</span><br />
<span class="score" title="6049 reputation points"><span>6.0k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="51 badges"><span class="bronze">●</span><span class="badgecount">51</span></span></p></div></div><div id="comments-container-54781" class="comments-container"><span id="54782"></span><div id="comment-54782" class="comment"><div id="post-54782-score" class="comment-score"></div><div class="comment-text"><p>This is to be filed as a bug at <a href="https://bugs.wireshark.org/bugzilla/enter_bug.cgi">Wireshark bugzilla</a>, not as a question here.</p></div><div id="comment-54782-info" class="comment-info"><span class="comment-age">(13 Aug '16, 08:03)</span> <span class="comment-user userinfo">sindy</span></div></div></div><div id="comment-tools-54781" class="comment-tools"></div><div class="clear"></div><div id="comment-54781-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Other" by sindy 13 Aug '16, 08:04

</div>

</div>

</div>

