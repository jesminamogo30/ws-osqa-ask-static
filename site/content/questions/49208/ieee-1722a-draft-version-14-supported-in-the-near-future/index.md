+++
type = "question"
title = "IEEE 1722a draft version 14 supported in the near future?"
description = '''Hi, I have another question regarding the supported IEEE 1722a draft version. Is it planned to support the draft version 14 in the near future? Is someone currently working on a dissector based on this draft version? Leila'''
date = "2016-01-14T02:36:00Z"
lastmod = "2016-01-21T00:08:00Z"
weight = 49208
keywords = [ "ieee1722" ]
aliases = [ "/questions/49208" ]
osqa_answers = 2
osqa_accepted = true
+++

<div class="headNormal">

# [IEEE 1722a draft version 14 supported in the near future?](/questions/49208/ieee-1722a-draft-version-14-supported-in-the-near-future)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49208-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49208-score" class="post-score" title="current number of votes">0</div><span id="post-49208-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I have another question regarding the supported IEEE 1722a draft version. Is it planned to support the draft version 14 in the near future? Is someone currently working on a dissector based on this draft version?</p><p>Leila</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-ieee1722" rel="tag" title="see questions tagged &#39;ieee1722&#39;">ieee1722</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Jan '16, 02:36</strong></p><img src="https://secure.gravatar.com/avatar/adff8d731ffe044e74b218776bff2c64?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Lumi&#39;s gravatar image" /><p><span>Lumi</span><br />
<span class="score" title="16 reputation points">16</span><span title="5 badges"><span class="badge1">●</span><span class="badgecount">5</span></span><span title="5 badges"><span class="silver">●</span><span class="badgecount">5</span></span><span title="9 badges"><span class="bronze">●</span><span class="badgecount">9</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Lumi has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>14 Jan '16, 05:33</strong> </span></p></div></div><div id="comments-container-49208" class="comments-container"></div><div id="comment-tools-49208" class="comment-tools"></div><div class="clear"></div><div id="comment-49208-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

2 Answers:

</div>

</div>

<span id="49381"></span>

<div id="answer-container-49381" class="answer accepted-answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49381-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49381-score" class="post-score" title="current number of votes">2</div><span id="post-49381-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span class="accept-answer on" rel="nofollow" title="Kurt Knochner has selected this answer as the correct answer"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>A better place to ask this is the <a href="https://www.wireshark.org/lists/">Wireshark Developers Mailing list</a>, or file an enhancement request on the <a href="https://bugs.wireshark.org">Wireshark Bugzilla</a> (after checking for no existing similar requests first).</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jan '16, 08:14</strong></p><img src="https://secure.gravatar.com/avatar/d2a7e24ca66604c749c7c88c1da8ff78?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="grahamb&#39;s gravatar image" /><p><span>grahamb ♦</span><br />
<span class="score" title="19834 reputation points"><span>19.8k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="30 badges"><span class="silver">●</span><span class="badgecount">30</span></span><span title="206 badges"><span class="bronze">●</span><span class="badgecount">206</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="grahamb has 274 accepted answers">22%</span></p></div></div><div id="comments-container-49381" class="comments-container"><span id="49414"></span><div id="comment-49414" class="comment"><div id="post-49414-score" class="comment-score"></div><div class="comment-text"><p>I accepted the answer, as it's the best way to answer this question :-)</p></div><div id="comment-49414-info" class="comment-info"><span class="comment-age">(20 Jan '16, 13:55)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div><span id="49423"></span><div id="comment-49423" class="comment"><div id="post-49423-score" class="comment-score"></div><div class="comment-text"><p>Thanks for your answer, I just wrote a little dissector for myself :)</p></div><div id="comment-49423-info" class="comment-info"><span class="comment-age">(21 Jan '16, 00:08)</span> <span class="comment-user userinfo">Lumi</span></div></div></div><div id="comment-tools-49381" class="comment-tools"></div><div class="clear"></div><div id="comment-49381-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<span id="49377"></span>

<div id="answer-container-49377" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-49377-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-49377-score" class="post-score" title="current number of votes">0</div><span id="post-49377-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p><del>There is code available for ieee1722a based on draft 7. However, to me it looks like this was never added to the Wireshark binary.</del></p><del></del><blockquote><p><a href="https://github.com/wireshark/wireshark/blob/master/epan/dissectors/packet-ieee1722a.c">https://github.com/wireshark/wireshark/blob/master/epan/dissectors/packet-ieee1722a.c</a><br />
</p></blockquote><p>Please ask the developers. You'll find their e-mail addresses in the code.</p></s><p><del>Maybe <span>@Pascal Quantin</span> has more information about this.</del></p><p>see the latest comment!</p><p>Regards<br />
Kurt</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>19 Jan '16, 07:54</strong></p><img src="https://secure.gravatar.com/avatar/23b7bf5b13bc2c98b2e8aa9869ca5d75?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Kurt%20Knochner&#39;s gravatar image" /><p><span>Kurt Knochner ♦</span><br />
<span class="score" title="24767 reputation points"><span>24.8k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="39 badges"><span class="silver">●</span><span class="badgecount">39</span></span><span title="237 badges"><span class="bronze">●</span><span class="badgecount">237</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Kurt Knochner has 344 accepted answers">15%</span> </br></br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>20 Jan '16, 13:52</strong> </span></p></div></div><div id="comments-container-49377" class="comments-container"><span id="49397"></span><div id="comment-49397" class="comment"><div id="post-49397-score" class="comment-score">1</div><div class="comment-text"><p><span>@Kurt Knochner</span>: you've got that backwards, I'm afraid. GitHub is downstream from code.wireshark.org, so all you see there is from the official repository, like <a href="https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob;f=epan/dissectors/packet-ieee1722a.c;hb=HEAD">the relevant file</a>. No other indication its not included in the build.</p></div><div id="comment-49397-info" class="comment-info"><span class="comment-age">(20 Jan '16, 00:21)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="49413"></span><div id="comment-49413" class="comment"><div id="post-49413-score" class="comment-score"></div><div class="comment-text"><p>Ah, my fault. I just thought it was not added, as I was unable to use the filters defined in the code (ieee1722a.*), but apparently it was a typo, as it works now when I try it again. So, please ignore my answer!</p></div><div id="comment-49413-info" class="comment-info"><span class="comment-age">(20 Jan '16, 13:52)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-49377" class="comment-tools"></div><div class="clear"></div><div id="comment-49377-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

