+++
type = "question"
title = "[closed] use a timer in lua"
description = '''hi i need to process data withen 1 minute using lua, data processing starts at t=0s and stops at t=60s,how can i figure it out using lua?if possible explain with an example.'''
date = "2012-02-17T20:37:00Z"
lastmod = "2012-02-25T17:06:00Z"
weight = 9112
keywords = [ "lua" ]
aliases = [ "/questions/9112" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] use a timer in lua](/questions/9112/use-a-timer-in-lua)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-9112-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-9112-score" class="post-score" title="current number of votes">0</div><span id="post-9112-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hi i need to process data withen 1 minute using lua, data processing starts at t=0s and stops at t=60s,how can i figure it out using lua?if possible explain with an example.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-lua" rel="tag" title="see questions tagged &#39;lua&#39;">lua</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Feb '12, 20:37</strong></p><img src="https://secure.gravatar.com/avatar/912ebc145cb38ec3da99be6003d7d9b8?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Leena&#39;s gravatar image" /><p><span>Leena</span><br />
<span class="score" title="51 reputation points">51</span><span title="17 badges"><span class="badge1">●</span><span class="badgecount">17</span></span><span title="18 badges"><span class="silver">●</span><span class="badgecount">18</span></span><span title="21 badges"><span class="bronze">●</span><span class="badgecount">21</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Leena has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>25 Feb '12, 17:04</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-9112" class="comments-container"><span id="9211"></span><div id="comment-9211" class="comment"><div id="post-9211-score" class="comment-score"></div><div class="comment-text"><p><a href="http://stackoverflow.com/questions/9393693/how-to-use-a-timer-in-lua">http://stackoverflow.com/questions/9393693/how-to-use-a-timer-in-lua</a></p></div><div id="comment-9211-info" class="comment-info"><span class="comment-age">(25 Feb '12, 17:06)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-9112" class="comment-tools"></div><div class="clear"></div><div id="comment-9112-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "http://stackoverflow.com/questions/9393693/how-to-use-a-timer-in-lua" by helloworld 25 Feb '12, 17:04

</div>

</div>

</div>

