+++
type = "question"
title = "Wireshark logs"
description = '''Hello, I made a successfull test call and the call was connected fine. But i can&#x27;t see the signals from the called party and the only signals are from calling party. Its seems to be quite strange. What could be the reason? ====================== This is a SIP call and the SONUS SBC and the media is ...'''
date = "2012-06-18T07:59:00Z"
lastmod = "2012-06-19T07:53:00Z"
weight = 12026
keywords = [ "wireshark" ]
aliases = [ "/questions/12026" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark logs](/questions/12026/wireshark-logs)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12026-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12026-score" class="post-score" title="current number of votes">0</div><span id="post-12026-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I made a successfull test call and the call was connected fine. But i can't see the signals from the called party and the only signals are from calling party. Its seems to be quite strange. What could be the reason? ======================</p><p>This is a SIP call and the SONUS SBC and the media is voice. The Wireshark is not showing the signals from the called party. Hope its clear now. Please confirm what could be the reason.</p><p>Thanks and regards, Paresh Acharya</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>18 Jun '12, 07:59</strong></p><img src="https://secure.gravatar.com/avatar/b5e1738ce272b1f4096e2e83b96bf071?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Paresh&#39;s gravatar image" /><p><span>Paresh</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Paresh has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>19 Jun '12, 07:37</strong> </span></p></div></div><div id="comments-container-12026" class="comments-container"><span id="12054"></span><div id="comment-12054" class="comment"><div id="post-12054-score" class="comment-score"></div><div class="comment-text"><p>Add a little more detail: like what type of call, what type of client, what type of service, what type of media, what type of connectivety, etc, etc.</p></div><div id="comment-12054-info" class="comment-info"><span class="comment-age">(19 Jun '12, 06:18)</span> <span class="comment-user userinfo">Jaap ♦</span></div></div><span id="12056"></span><div id="comment-12056" class="comment"><div id="post-12056-score" class="comment-score"></div><div class="comment-text"><ul><li>where do you sniff with wireshark? On Client, server or switch (mirror/monitor port)?</li><li>What is your OS?</li><li>What is your Wireshark version?</li></ul></div><div id="comment-12056-info" class="comment-info"><span class="comment-age">(19 Jun '12, 07:53)</span> <span class="comment-user userinfo">Kurt Knochner ♦</span></div></div></div><div id="comment-tools-12026" class="comment-tools"></div><div class="clear"></div><div id="comment-12026-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

