+++
type = "question"
title = "Find out how many minutes of streaming video(Youtube) a user watched?"
description = '''My admin has tasked me with finding out how much time a user is spending watching Youtube videos. How can I go about finding this out, without having to find the difference of start and stop times of every TCP stream. Is there a way in statistics on Wireshark, or some way it can be done in Tshark. J...'''
date = "2016-12-28T11:49:00Z"
lastmod = "2016-12-28T11:49:00Z"
weight = 58407
keywords = [ "timestamps", "videostream", "tshark", "wireshark" ]
aliases = [ "/questions/58407" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Find out how many minutes of streaming video(Youtube) a user watched?](/questions/58407/find-out-how-many-minutes-of-streaming-videoyoutube-a-user-watched)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-58407-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-58407-score" class="post-score" title="current number of votes">0</div><span id="post-58407-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>My admin has tasked me with finding out how much time a user is spending watching Youtube videos. How can I go about finding this out, without having to find the difference of start and stop times of every TCP stream. Is there a way in statistics on Wireshark, or some way it can be done in Tshark. Just looking for some direction, thanks!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-timestamps" rel="tag" title="see questions tagged &#39;timestamps&#39;">timestamps</span> <span class="post-tag tag-link-videostream" rel="tag" title="see questions tagged &#39;videostream&#39;">videostream</span> <span class="post-tag tag-link-tshark" rel="tag" title="see questions tagged &#39;tshark&#39;">tshark</span> <span class="post-tag tag-link-wireshark" rel="tag" title="see questions tagged &#39;wireshark&#39;">wireshark</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Dec '16, 11:49</strong></p><img src="https://secure.gravatar.com/avatar/7c34b5795df1aaa486754544342bfc1d?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="zer0day&#39;s gravatar image" /><p><span>zer0day</span><br />
<span class="score" title="21 reputation points">21</span><span title="7 badges"><span class="badge1">●</span><span class="badgecount">7</span></span><span title="8 badges"><span class="silver">●</span><span class="badgecount">8</span></span><span title="11 badges"><span class="bronze">●</span><span class="badgecount">11</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="zer0day has 3 accepted answers">60%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>28 Dec '16, 11:50</strong> </span></p></div></div><div id="comments-container-58407" class="comments-container"></div><div id="comment-tools-58407" class="comment-tools"></div><div class="clear"></div><div id="comment-58407-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

