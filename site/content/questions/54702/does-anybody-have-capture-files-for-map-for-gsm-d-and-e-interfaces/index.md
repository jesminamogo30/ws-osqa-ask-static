+++
type = "question"
title = "Does anybody have capture files for MAP for GSM D and E interfaces?"
description = '''Hi, I need the pcap file of MAP protocol of GSM network for D interface (between two MSCs) and E interface (between VLR and HLR). I searched too much everywhere but i didn&#x27;t find any of them. Please if anyone has the pcaps, please send to me. (Mail: nonofficial1980@gmail.com) Thanks.'''
date = "2016-08-09T06:42:00Z"
lastmod = "2016-08-09T11:00:00Z"
weight = 54702
keywords = [ "capture", "gsm_map" ]
aliases = [ "/questions/54702" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Does anybody have capture files for MAP for GSM D and E interfaces?](/questions/54702/does-anybody-have-capture-files-for-map-for-gsm-d-and-e-interfaces)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54702-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54702-score" class="post-score" title="current number of votes">0</div><span id="post-54702-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi, I need the pcap file of MAP protocol of GSM network for D interface (between two MSCs) and E interface (between VLR and HLR). I searched too much everywhere but i didn't find any of them. Please if anyone has the pcaps, please send to me. (Mail: <span class="__cf_email__" data-cfemail="bfd1d0d1d0d9d9d6dcd6ded38e86878fffd8d2ded6d391dcd0d2">[email protected]</span>)</p><p>Thanks.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-capture" rel="tag" title="see questions tagged &#39;capture&#39;">capture</span> <span class="post-tag tag-link-gsm_map" rel="tag" title="see questions tagged &#39;gsm_map&#39;">gsm_map</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>09 Aug '16, 06:42</strong></p><img src="https://secure.gravatar.com/avatar/81fa95ff23dee3304e83ec8b9e8b5d79?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SAl&#39;s gravatar image" /><p><span>SAl</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SAl has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>10 Aug '16, 10:04</strong> </span></p><img src="https://secure.gravatar.com/avatar/f93de7000747ab5efb5acd3034b2ebd7?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Guy%20Harris&#39;s gravatar image" /><p><span>Guy Harris ♦♦</span><br />
<span class="score" title="17443 reputation points"><span>17.4k</span></span><span title="3 badges"><span class="badge1">●</span><span class="badgecount">3</span></span><span title="35 badges"><span class="silver">●</span><span class="badgecount">35</span></span><span title="196 badges"><span class="bronze">●</span><span class="badgecount">196</span></span></p></div></div><div id="comments-container-54702" class="comments-container"></div><div id="comment-tools-54702" class="comment-tools"></div><div class="clear"></div><div id="comment-54702-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="54708"></span>

<div id="answer-container-54708" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-54708-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-54708-score" class="post-score" title="current number of votes">0</div><span id="post-54708-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>Some time ago, Jakub Zawadzki ran <code>list_protos_in_cap.sh</code> on the capture files in the Wireshark menagerie and posted the output <a href="https://www.wireshark.org/~darkjames/capture-files.txt">here</a>. You can try searching the list for files containing traffic of interest to you.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 Aug '16, 11:00</strong></p><img src="https://secure.gravatar.com/avatar/55158e2322c4e365a5e0a4a0ac3fbcef?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="cmaynard&#39;s gravatar image" /><p><span>cmaynard ♦♦</span><br />
<span class="score" title="9361 reputation points"><span>9.4k</span></span><span title="10 badges"><span class="badge1">●</span><span class="badgecount">10</span></span><span title="38 badges"><span class="silver">●</span><span class="badgecount">38</span></span><span title="142 badges"><span class="bronze">●</span><span class="badgecount">142</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="cmaynard has 108 accepted answers">20%</span></p></div></div><div id="comments-container-54708" class="comments-container"></div><div id="comment-tools-54708" class="comment-tools"></div><div class="clear"></div><div id="comment-54708-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

