+++
type = "question"
title = "[closed] Problem with Exchange 2010 and Outlook"
description = '''Hello,  I needed some information from my work email. So I connected to my work desktop from home, went on my work outlook and i needed to download 2 outlook data tsp files. I did so, then imported them to my home computer. Now when i am at work ( i work in a remote location) and i open my outlook, ...'''
date = "2011-09-26T20:56:00Z"
lastmod = "2011-09-26T21:38:00Z"
weight = 6574
keywords = [ "access", "outlook", "2010", "exchange" ]
aliases = [ "/questions/6574" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [\[closed\] Problem with Exchange 2010 and Outlook](/questions/6574/problem-with-exchange-2010-and-outlook)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-6574-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-6574-score" class="post-score" title="current number of votes">0</div><span id="post-6574-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello,</p><p>I needed some information from my work email. So I connected to my work desktop from home, went on my work outlook and i needed to download 2 outlook data tsp files. I did so, then imported them to my home computer. Now when i am at work ( i work in a remote location) and i open my outlook, the exchange server 2010 doesnt work anymore it like logs me on to 2007, On top of that i cannot access or even delete the emails or files that i had downloaded unto my home computer. I feel like everything is bugged. What do i need to do to be able to a) use exchange 2010 server again like i did before 2) be able to access or delete the emails/files that i downloaded or viewed from my home computer?</p><p>please let me know, i would so appreciate it!</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-access" rel="tag" title="see questions tagged &#39;access&#39;">access</span> <span class="post-tag tag-link-outlook" rel="tag" title="see questions tagged &#39;outlook&#39;">outlook</span> <span class="post-tag tag-link-2010" rel="tag" title="see questions tagged &#39;2010&#39;">2010</span> <span class="post-tag tag-link-exchange" rel="tag" title="see questions tagged &#39;exchange&#39;">exchange</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>26 Sep '11, 20:56</strong></p><img src="https://secure.gravatar.com/avatar/fa747c58e2a282276420bf013bb16a0f?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="springinparadise&#39;s gravatar image" /><p><span>springinpara...</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="springinparadise has no accepted answers">0%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> closed <strong>26 Sep '11, 21:38</strong> </span></p><img src="https://secure.gravatar.com/avatar/362ba1008ad9a075d1556d33e97dfed6?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="helloworld&#39;s gravatar image" /><p><span>helloworld</span><br />
<span class="score" title="3149 reputation points"><span>3.1k</span></span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="20 badges"><span class="silver">●</span><span class="badgecount">20</span></span><span title="41 badges"><span class="bronze">●</span><span class="badgecount">41</span></span></p></div></div><div id="comments-container-6574" class="comments-container"><span id="6577"></span><div id="comment-6577" class="comment"><div id="post-6577-score" class="comment-score"></div><div class="comment-text"><p>This has nothing to do with Wireshark. Ask your IT department at work or <a href="http://superuser.com">superuser.com</a>.</p></div><div id="comment-6577-info" class="comment-info"><span class="comment-age">(26 Sep '11, 21:38)</span> <span class="comment-user userinfo">helloworld</span></div></div></div><div id="comment-tools-6574" class="comment-tools"></div><div class="clear"></div><div id="comment-6574-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

<div class="question-status" style="margin-bottom:15px">

### The question has been closed for the following reason "Question is off-topic or not relevant" by helloworld 26 Sep '11, 21:38

</div>

</div>

</div>

