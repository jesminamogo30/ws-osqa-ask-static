+++
type = "question"
title = "Packet Analyzer - PE Value for Wireshark Users"
description = '''Hi all, I was checking the PA-PE option to integrate to Wireshark, and got the trial evaluation. First I would thank Riverbed to sustain the wordeful job done here , so my question is a genuine need for understanding . I got the trial and was thinking to get the subscription exposed here on the web,...'''
date = "2016-03-12T07:12:00Z"
lastmod = "2016-03-12T09:28:00Z"
weight = 50840
keywords = [ "added", "value", "pa-pe" ]
aliases = [ "/questions/50840" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Packet Analyzer - PE Value for Wireshark Users](/questions/50840/packet-analyzer-pe-value-for-wireshark-users)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50840-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50840-score" class="post-score" title="current number of votes">0</div><span id="post-50840-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all, I was checking the PA-PE option to integrate to Wireshark, and got the trial evaluation. First I would thank Riverbed to sustain the wordeful job done here , so my question is a genuine need for understanding . I got the trial and was thinking to get the subscription exposed here on the web, , but at a first look, I was really not able to understand the added value it should add to wireshark. I see some good dashboard , but cannot find something 'you cannot miss' or is not better detailed in wireshark. If someone could explain the use case for such an integration , I would be grateful. My eyes say more scapy after wireshark than PA-PE , so I am probably missing something . Thanks in Advance Fabio D'Alfonso</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-added" rel="tag" title="see questions tagged &#39;added&#39;">added</span> <span class="post-tag tag-link-value" rel="tag" title="see questions tagged &#39;value&#39;">value</span> <span class="post-tag tag-link-pa-pe" rel="tag" title="see questions tagged &#39;pa-pe&#39;">pa-pe</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Mar '16, 07:12</strong></p><img src="https://secure.gravatar.com/avatar/e5463d5ff7c14dbe04082962df0b04bb?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="fabio_dalfonso&#39;s gravatar image" /><p><span>fabio_dalfonso</span><br />
<span class="score" title="6 reputation points">6</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="4 badges"><span class="bronze">●</span><span class="badgecount">4</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="fabio_dalfonso has no accepted answers">0%</span></p></div></div><div id="comments-container-50840" class="comments-container"></div><div id="comment-tools-50840" class="comment-tools"></div><div class="clear"></div><div id="comment-50840-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="50841"></span>

<div id="answer-container-50841" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-50841-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-50841-score" class="post-score" title="current number of votes">0</div><span id="post-50841-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>From my point of view it is a very good add-on to wireshark:<br />
</p><ul><li>It can be used for mass data analysis</li><li>It has a great wireshark integration</li><li>Nice report functions</li><li>The analysis is more graphical based, but I like it, when it comes to mass analysis</li><li>You should have an expectation, if you want to analyze mass data</li></ul><p>From my point of view Wireshark is mostly better, when you just need to analyze one single session.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>12 Mar '16, 07:43</strong></p><img src="https://secure.gravatar.com/avatar/3b24b339fc62fb46dced6a443d3202ea?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Christian_R&#39;s gravatar image" /><p><span>Christian_R</span><br />
<span class="score" title="1830 reputation points"><span>1.8k</span></span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="6 badges"><span class="silver">●</span><span class="badgecount">6</span></span><span title="25 badges"><span class="bronze">●</span><span class="badgecount">25</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Christian_R has 25 accepted answers">16%</span> </br></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>12 Mar '16, 08:19</strong> </span></p></div></div><div id="comments-container-50841" class="comments-container"><span id="50842"></span><div id="comment-50842" class="comment"><div id="post-50842-score" class="comment-score"></div><div class="comment-text"><p>agreed, PA is intended to help with drilling down into massive amounts of packet data, not just a couple of sessions.</p></div><div id="comment-50842-info" class="comment-info"><span class="comment-age">(12 Mar '16, 07:54)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div><span id="50843"></span><div id="comment-50843" class="comment"><div id="post-50843-score" class="comment-score"></div><div class="comment-text"><p>Thanks, I will try to focus on pa-pe using the trial period to get a better idea of functionality and cases of use. From what you both told, it seems something that could be better used to get a the big picture. But details are still got in wireshark. I will probably get a better idea when against some large traffic scenario. At the moment I still cannot see a free / 690$ balance , with their respective parts in the job. Obviously 29 / year is a coin , but I reported the license fee just to give another shot at the question starting this post.</p><p>The feeling was that when you have the top, imagine the paid will be the TOP.</p></div><div id="comment-50843-info" class="comment-info"><span class="comment-age">(12 Mar '16, 09:28)</span> <span class="comment-user userinfo">fabio_dalfonso</span></div></div></div><div id="comment-tools-50841" class="comment-tools"></div><div class="clear"></div><div id="comment-50841-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

