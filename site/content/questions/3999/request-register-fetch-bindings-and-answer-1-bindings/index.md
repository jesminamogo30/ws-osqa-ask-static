+++
type = "question"
title = "request REGISTER - fetch bindings and answer (1 bindings)"
description = '''What&#x27;s the meaning of &quot;(fetch) bindings&quot;?'''
date = "2011-05-08T05:29:00Z"
lastmod = "2011-05-09T11:38:00Z"
weight = 3999
keywords = [ "answer", "request", "register", "binding", "fetch" ]
aliases = [ "/questions/3999" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [request REGISTER - fetch bindings and answer (1 bindings)](/questions/3999/request-register-fetch-bindings-and-answer-1-bindings)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-3999-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-3999-score" class="post-score" title="current number of votes">0</div><span id="post-3999-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>What's the meaning of "(fetch) bindings"?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-answer" rel="tag" title="see questions tagged &#39;answer&#39;">answer</span> <span class="post-tag tag-link-request" rel="tag" title="see questions tagged &#39;request&#39;">request</span> <span class="post-tag tag-link-register" rel="tag" title="see questions tagged &#39;register&#39;">register</span> <span class="post-tag tag-link-binding" rel="tag" title="see questions tagged &#39;binding&#39;">binding</span> <span class="post-tag tag-link-fetch" rel="tag" title="see questions tagged &#39;fetch&#39;">fetch</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 May '11, 05:29</strong></p><img src="https://secure.gravatar.com/avatar/13231e33ab17a93476f7b98c9d5b272a?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="wired&#39;s gravatar image" /><p><span>wired</span><br />
<span class="score" title="44 reputation points">44</span><span title="13 badges"><span class="badge1">●</span><span class="badgecount">13</span></span><span title="14 badges"><span class="silver">●</span><span class="badgecount">14</span></span><span title="17 badges"><span class="bronze">●</span><span class="badgecount">17</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="wired has one accepted answer">9%</span></p></div></div><div id="comments-container-3999" class="comments-container"><span id="4001"></span><div id="comment-4001" class="comment"><div id="post-4001-score" class="comment-score"></div><div class="comment-text"><p>Binding: A process associates its input or output channel file descriptors (sockets) with a port number and an IP address, a process known as binding, to send and receive data via the network.</p><p>Right definition?</p><p>(From page http://en.wikipedia.org/wiki/UDP_ports.)</p></div><div id="comment-4001-info" class="comment-info"><span class="comment-age">(08 May '11, 05:44)</span> <span class="comment-user userinfo">wired</span></div></div></div><div id="comment-tools-3999" class="comment-tools"></div><div class="clear"></div><div id="comment-3999-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="4016"></span>

<div id="answer-container-4016" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-4016-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-4016-score" class="post-score" title="current number of votes">0</div><span id="post-4016-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>See <a href="http://tools.ietf.org/html/rfc3261#section-10.2.3">RFC3261</a>, or the whole section 10 for that matter.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>09 May '11, 06:09</strong></p><img src="https://secure.gravatar.com/avatar/2337f0406681e5c72ea0e6f1f0d6c0b0?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Jaap&#39;s gravatar image" /><p><span>Jaap ♦</span><br />
<span class="score" title="11680 reputation points"><span>11.7k</span></span><span title="16 badges"><span class="silver">●</span><span class="badgecount">16</span></span><span title="101 badges"><span class="bronze">●</span><span class="badgecount">101</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Jaap has 155 accepted answers">14%</span></p></div></div><div id="comments-container-4016" class="comments-container"><span id="4018"></span><div id="comment-4018" class="comment"><div id="post-4018-score" class="comment-score"></div><div class="comment-text"><p>Thank you.</p></div><div id="comment-4018-info" class="comment-info"><span class="comment-age">(09 May '11, 11:38)</span> <span class="comment-user userinfo">wired</span></div></div></div><div id="comment-tools-4016" class="comment-tools"></div><div class="clear"></div><div id="comment-4016-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

