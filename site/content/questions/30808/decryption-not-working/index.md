+++
type = "question"
title = "Decryption not working"
description = '''I&#x27;m using Wireshark on OSX and it&#x27;s seems to be working ok, I&#x27;m seeing the icon for monitor mode and it&#x27;s capturing the packages. However, when I add the WPA-PSK key for decryption, it only seems to decrypt LLC, NetBIOS, STP, SNA, RPL, X.25 and Vines LLC protocol. I still don&#x27;t see UDP or TCP for in...'''
date = "2014-03-15T03:22:00Z"
lastmod = "2014-03-15T03:22:00Z"
weight = 30808
keywords = [ "wpa-psk", "decryption", "802.11", "wpa2" ]
aliases = [ "/questions/30808" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Decryption not working](/questions/30808/decryption-not-working)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-30808-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-30808-score" class="post-score" title="current number of votes">0</div><span id="post-30808-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm using Wireshark on OSX and it's seems to be working ok, I'm seeing the icon for monitor mode and it's capturing the packages. However, when I add the WPA-PSK key for decryption, it only seems to decrypt LLC, NetBIOS, STP, SNA, RPL, X.25 and Vines LLC protocol. I still don't see UDP or TCP for instance, and a lot of 802.11</p><p>I tried the different settings (Assume packets have FCS, Ignore protection bit) but that didn't help. I used this tool to generate the PSK; <a href="http://www.wireshark.org/tools/wpa-psk.html">http://www.wireshark.org/tools/wpa-psk.html</a></p><p>Is there another setting that I'm overlooking? How can I make sure all packages are decrypted?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wpa-psk" rel="tag" title="see questions tagged &#39;wpa-psk&#39;">wpa-psk</span> <span class="post-tag tag-link-decryption" rel="tag" title="see questions tagged &#39;decryption&#39;">decryption</span> <span class="post-tag tag-link-802.11" rel="tag" title="see questions tagged &#39;802.11&#39;">802.11</span> <span class="post-tag tag-link-wpa2" rel="tag" title="see questions tagged &#39;wpa2&#39;">wpa2</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>15 Mar '14, 03:22</strong></p><img src="https://secure.gravatar.com/avatar/034d4f917134c83ddccfb3dc0b8699c2?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="nikao&#39;s gravatar image" /><p><span>nikao</span><br />
<span class="score" title="11 reputation points">11</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="nikao has no accepted answers">0%</span></p></div></div><div id="comments-container-30808" class="comments-container"></div><div id="comment-tools-30808" class="comment-tools"></div><div class="clear"></div><div id="comment-30808-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

