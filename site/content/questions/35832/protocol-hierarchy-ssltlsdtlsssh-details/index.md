+++
type = "question"
title = "Protocol Hierarchy: SSL/TLS/DTLS/SSH details"
description = '''Hello everyone, In Statistics -&amp;gt; Protocol Hierarchy, we have a list of all protocols running on the network. Wouldn&#x27;t it be possible to detail the type of protocols secured by SSL/TLS/DTLS: HTTPs, FTPs, SMTPs, POPs, IMAPs, and so on? The TCP/UDP port numbers point to the right protocol. Also, som...'''
date = "2014-08-28T00:58:00Z"
lastmod = "2014-08-28T00:58:00Z"
weight = 35832
keywords = [ "tls", "dtls", "ssh" ]
aliases = [ "/questions/35832" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Protocol Hierarchy: SSL/TLS/DTLS/SSH details](/questions/35832/protocol-hierarchy-ssltlsdtlsssh-details)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-35832-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-35832-score" class="post-score" title="current number of votes">0</div><span id="post-35832-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hello everyone,</p><p>In Statistics -&gt; Protocol Hierarchy, we have a list of all protocols running on the network. Wouldn't it be possible to detail the type of protocols secured by SSL/TLS/DTLS: HTTPs, FTPs, SMTPs, POPs, IMAPs, and so on? The TCP/UDP port numbers point to the right protocol.</p><p>Also, some protocols are tunnelled inside SSH: same question as above.</p><p>At last, it is kind of difficult to read the report with all the protocol names: wouldn't it be clearer to show only the acronyms (or to offer that choice in the settings)?</p><p>Great product BTW. Nice work guys. JC</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-tls" rel="tag" title="see questions tagged &#39;tls&#39;">tls</span> <span class="post-tag tag-link-dtls" rel="tag" title="see questions tagged &#39;dtls&#39;">dtls</span> <span class="post-tag tag-link-ssh" rel="tag" title="see questions tagged &#39;ssh&#39;">ssh</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Aug '14, 00:58</strong></p><img src="https://secure.gravatar.com/avatar/c86fb9accfde44bdbe661d8582c39b7b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="actionmystique&#39;s gravatar image" /><p><span>actionmystique</span><br />
<span class="score" title="11 reputation points">11</span><span title="4 badges"><span class="badge1">●</span><span class="badgecount">4</span></span><span title="4 badges"><span class="silver">●</span><span class="badgecount">4</span></span><span title="7 badges"><span class="bronze">●</span><span class="badgecount">7</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="actionmystique has no accepted answers">0%</span></p></div></div><div id="comments-container-35832" class="comments-container"></div><div id="comment-tools-35832" class="comment-tools"></div><div class="clear"></div><div id="comment-35832-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

