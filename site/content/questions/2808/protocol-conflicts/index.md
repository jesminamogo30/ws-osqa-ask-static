+++
type = "question"
title = "Protocol Conflicts"
description = '''Hi,  I am a rookie when it comes to building a dissector . However I was able to build a dissector over TCP.  I use port 80 to identify the packet. The packet which is not under my protocol comes under TCP but not HTTP.  How did I rectify this problem. I would also like to &#x27;point&#x27; it to any other pr...'''
date = "2011-03-14T13:44:00Z"
lastmod = "2011-03-14T20:09:00Z"
weight = 2808
keywords = [ "resolution", "conflict" ]
aliases = [ "/questions/2808" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Protocol Conflicts](/questions/2808/protocol-conflicts)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-2808-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-2808-score" class="post-score" title="current number of votes">0</div><span id="post-2808-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi,</p><p>I am a rookie when it comes to building a dissector . However I was able to build a dissector over TCP. I use port 80 to identify the packet. The packet which is not under my protocol comes under TCP but not HTTP. How did I rectify this problem. I would also like to 'point' it to any other protocol I desire. Is this possible?</p><p>Thanks in advance</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-resolution" rel="tag" title="see questions tagged &#39;resolution&#39;">resolution</span> <span class="post-tag tag-link-conflict" rel="tag" title="see questions tagged &#39;conflict&#39;">conflict</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>14 Mar '11, 13:44</strong></p><img src="https://secure.gravatar.com/avatar/46023e482c60329a251a137848f8f5f5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="niks3089&#39;s gravatar image" /><p><span>niks3089</span><br />
<span class="score" title="21 reputation points">21</span><span title="15 badges"><span class="badge1">●</span><span class="badgecount">15</span></span><span title="15 badges"><span class="silver">●</span><span class="badgecount">15</span></span><span title="18 badges"><span class="bronze">●</span><span class="badgecount">18</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="niks3089 has no accepted answers">0%</span></p></div></div><div id="comments-container-2808" class="comments-container"><span id="2813"></span><div id="comment-2813" class="comment"><div id="post-2813-score" class="comment-score"></div><div class="comment-text"><p>I.e., you have a protocol that runs over TCP, using port 80?</p><p>Is that protocol based on HTTP, such as, for example, IPP is, with a particular medium type assigned to it, or is it some protocol that is <em>not</em> based on HTTP and, for some reason, uses port 80 anyway?</p></div><div id="comment-2813-info" class="comment-info"><span class="comment-age">(14 Mar '11, 20:09)</span> <span class="comment-user userinfo">Guy Harris ♦♦</span></div></div></div><div id="comment-tools-2808" class="comment-tools"></div><div class="clear"></div><div id="comment-2808-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

