+++
type = "question"
title = "packet data size with padding using capinfos"
description = '''hello, I want to display the data size of the captured file along with the padding. which option of the capinfos has to be used? thank u '''
date = "2012-04-24T03:16:00Z"
lastmod = "2012-04-24T05:16:00Z"
weight = 10417
keywords = [ "data", "filesize", "capinfos" ]
aliases = [ "/questions/10417" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [packet data size with padding using capinfos](/questions/10417/packet-data-size-with-padding-using-capinfos)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-10417-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-10417-score" class="post-score" title="current number of votes">0</div><span id="post-10417-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>hello,</p><p>I want to display the data size of the captured file along with the padding. which option of the capinfos has to be used?</p><p>thank u</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-data" rel="tag" title="see questions tagged &#39;data&#39;">data</span> <span class="post-tag tag-link-filesize" rel="tag" title="see questions tagged &#39;filesize&#39;">filesize</span> <span class="post-tag tag-link-capinfos" rel="tag" title="see questions tagged &#39;capinfos&#39;">capinfos</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>24 Apr '12, 03:16</strong></p><img src="https://secure.gravatar.com/avatar/0aba772e8e3564a308f2bdb6077efd44?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="prashanth%20cm&#39;s gravatar image" /><p><span>prashanth cm</span><br />
<span class="score" title="-1 reputation points">-1</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="prashanth cm has no accepted answers">0%</span></p></div></div><div id="comments-container-10417" class="comments-container"><span id="10418"></span><div id="comment-10418" class="comment"><div id="post-10418-score" class="comment-score"></div><div class="comment-text"><p>can you elaborate a little? What do you mean by "with the padding"? Do you want to know how much padding was added in total for the file?</p></div><div id="comment-10418-info" class="comment-info"><span class="comment-age">(24 Apr '12, 05:16)</span> <span class="comment-user userinfo">Jasper ♦♦</span></div></div></div><div id="comment-tools-10417" class="comment-tools"></div><div class="clear"></div><div id="comment-10417-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

