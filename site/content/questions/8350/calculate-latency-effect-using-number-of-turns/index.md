+++
type = "question"
title = "Calculate latency effect using number of &quot;turns&quot;"
description = '''Hi all, i&#x27;d like to know if Wireshask could provide the number of change direction during a communication between 2 hosts.  With this information i could estimate / calculate the latency effect for this communication. I mean, if there are 26 (it&#x27;s an example) change direction of IP traffic during a ...'''
date = "2012-01-12T09:32:00Z"
lastmod = "2012-01-12T09:32:00Z"
weight = 8350
keywords = [ "latency", "calculate" ]
aliases = [ "/questions/8350" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Calculate latency effect using number of "turns"](/questions/8350/calculate-latency-effect-using-number-of-turns)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-8350-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-8350-score" class="post-score" title="current number of votes">0</div><span id="post-8350-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>Hi all,</p><p>i'd like to know if Wireshask could provide the number of change direction during a communication between 2 hosts. With this information i could estimate / calculate the latency effect for this communication.</p><p>I mean, if there are 26 (it's an example) change direction of IP traffic during a communication with 50ms of Latency. The total latency effect (loss time caused by latency) is : 26 * 0,050 = 1,3s</p><p>Thanks for your help Best regards</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-latency" rel="tag" title="see questions tagged &#39;latency&#39;">latency</span> <span class="post-tag tag-link-calculate" rel="tag" title="see questions tagged &#39;calculate&#39;">calculate</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>12 Jan '12, 09:32</strong></p><img src="https://secure.gravatar.com/avatar/2c12522ae4ea8d95f3eb970932a21453?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Zounours69&#39;s gravatar image" /><p><span>Zounours69</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Zounours69 has no accepted answers">0%</span></p></div></div><div id="comments-container-8350" class="comments-container"></div><div id="comment-tools-8350" class="comment-tools"></div><div class="clear"></div><div id="comment-8350-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

