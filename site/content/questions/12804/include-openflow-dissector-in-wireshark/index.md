+++
type = "question"
title = "Include OpenFlow dissector in Wireshark"
description = '''There is an OpenFlow dissector developed for Wireshark. It does not look like it was ever added to trunk. Is there any history here about why this has not been submitted as a patch?  http://www.openflow.org/wk/index.php/OpenFlow_Wireshark_Dissector'''
date = "2012-07-17T11:48:00Z"
lastmod = "2012-07-18T13:07:00Z"
weight = 12804
keywords = [ "openflow" ]
aliases = [ "/questions/12804" ]
osqa_answers = 1
osqa_accepted = false
+++

<div class="headNormal">

# [Include OpenFlow dissector in Wireshark](/questions/12804/include-openflow-dissector-in-wireshark)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12804-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12804-score" class="post-score" title="current number of votes">0</div><span id="post-12804-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>There is an OpenFlow dissector developed for Wireshark. It does not look like it was ever added to trunk. Is there any history here about why this has not been submitted as a patch?</p><p><a href="http://www.openflow.org/wk/index.php/OpenFlow_Wireshark_Dissector">http://www.openflow.org/wk/index.php/OpenFlow_Wireshark_Dissector</a></p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-openflow" rel="tag" title="see questions tagged &#39;openflow&#39;">openflow</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>17 Jul '12, 11:48</strong></p><img src="https://secure.gravatar.com/avatar/9681c8a3b1c4620c300ab9e3fdce439b?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="joemc&#39;s gravatar image" /><p><span>joemc</span><br />
<span class="score" title="21 reputation points">21</span><span title="2 badges"><span class="badge1">●</span><span class="badgecount">2</span></span><span title="2 badges"><span class="silver">●</span><span class="badgecount">2</span></span><span title="5 badges"><span class="bronze">●</span><span class="badgecount">5</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="joemc has no accepted answers">0%</span></p></div></div><div id="comments-container-12804" class="comments-container"></div><div id="comment-tools-12804" class="comment-tools"></div><div class="clear"></div><div id="comment-12804-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

------------------------------------------------------------------------

<div class="tabBar">

<span id="sort-top"></span>

<div class="headQuestions">

One Answer:

</div>

</div>

<span id="12821"></span>

<div id="answer-container-12821" class="answer">

<table style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-12821-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-12821-score" class="post-score" title="current number of votes">1</div><span id="post-12821-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span></div></td><td><div class="item-right"><div class="answer-body"><p>That question can be best asked at the people who developed the dissector. Looking into the mailing-list archives and <a href="http://bugs.wireshark.org">http://bugs.wireshark.org</a>, there is no history of it being submitted for inclusion.</p></div><div class="answer-controls post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>answered <strong>18 Jul '12, 01:53</strong></p><img src="https://secure.gravatar.com/avatar/7901a94d8fdd1f9f47cda9a32fcfa177?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="SYN-bit&#39;s gravatar image" /><p><span>SYN-bit ♦♦</span><br />
<span class="score" title="17094 reputation points"><span>17.1k</span></span><span title="9 badges"><span class="badge1">●</span><span class="badgecount">9</span></span><span title="57 badges"><span class="silver">●</span><span class="badgecount">57</span></span><span title="245 badges"><span class="bronze">●</span><span class="badgecount">245</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="SYN-bit has 174 accepted answers">20%</span></p></div></div><div id="comments-container-12821" class="comments-container"><span id="12836"></span><div id="comment-12836" class="comment"><div id="post-12836-score" class="comment-score"></div><div class="comment-text"><p>I'll see if I can find out from the <a href="http://openflow.org">openflow.org</a> folks.</p></div><div id="comment-12836-info" class="comment-info"><span class="comment-age">(18 Jul '12, 13:07)</span> <span class="comment-user userinfo">joemc</span></div></div></div><div id="comment-tools-12821" class="comment-tools"></div><div class="clear"></div><div id="comment-12821-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

<div class="paginator-container-left">

</div>

</div>

</div>

