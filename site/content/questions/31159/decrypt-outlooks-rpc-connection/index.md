+++
type = "question"
title = "decrypt outlook&#x27;s RPC connection"
description = '''This is a capture of a failed attempt to connect from Outlook 2010 on windows xp to exchange 2013 on server 2012 -&amp;gt; https://www.cloudshark.org/captures/577e132826c3 Is there a way to decrypt the communication to see why client resets the connection? Client computer is not a part of the domain.'''
date = "2014-03-25T14:02:00Z"
lastmod = "2014-03-25T14:02:00Z"
weight = 31159
keywords = [ "over", "rcp", "https", "2013", "exchange" ]
aliases = [ "/questions/31159" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [decrypt outlook's RPC connection](/questions/31159/decrypt-outlooks-rpc-connection)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-31159-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-31159-score" class="post-score" title="current number of votes">0</div><span id="post-31159-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>This is a capture of a failed attempt to connect from Outlook 2010 on windows xp to exchange 2013 on server 2012 -&gt; <a href="https://www.cloudshark.org/captures/577e132826c3">https://www.cloudshark.org/captures/577e132826c3</a></p><p>Is there a way to decrypt the communication to see why client resets the connection?</p><p>Client computer is not a part of the domain.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-over" rel="tag" title="see questions tagged &#39;over&#39;">over</span> <span class="post-tag tag-link-rcp" rel="tag" title="see questions tagged &#39;rcp&#39;">rcp</span> <span class="post-tag tag-link-https" rel="tag" title="see questions tagged &#39;https&#39;">https</span> <span class="post-tag tag-link-2013" rel="tag" title="see questions tagged &#39;2013&#39;">2013</span> <span class="post-tag tag-link-exchange" rel="tag" title="see questions tagged &#39;exchange&#39;">exchange</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>25 Mar '14, 14:02</strong></p><img src="https://secure.gravatar.com/avatar/bcfdf26904f3a8a9fb69c7ca0dc5e7b1?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="net_tech&#39;s gravatar image" /><p><span>net_tech</span><br />
<span class="score" title="116 reputation points">116</span><span title="30 badges"><span class="badge1">●</span><span class="badgecount">30</span></span><span title="33 badges"><span class="silver">●</span><span class="badgecount">33</span></span><span title="37 badges"><span class="bronze">●</span><span class="badgecount">37</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="net_tech has 2 accepted answers">13%</span></p></div><div class="post-update-info post-update-info-edited"><p><span> edited <strong>26 Mar '14, 11:32</strong> </span></p></div></div><div id="comments-container-31159" class="comments-container"></div><div id="comment-tools-31159" class="comment-tools"></div><div class="clear"></div><div id="comment-31159-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

