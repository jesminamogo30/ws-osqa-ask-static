+++
type = "question"
title = "Wireless toolbar"
description = '''I have just updated to Wireshark 2.4.0. 64 bit I use the Legacy interface and the Wireless toolbar is greyed out. The 802.11 Channel, Channel Offset and FCS Filter: and Wireless Settings... fields are all greyed out and do nothing. The Decryption Keys... item does work. It worked OK before the updat...'''
date = "2017-08-01T07:06:00Z"
lastmod = "2017-08-01T07:37:00Z"
weight = 63293
keywords = [ "wireless_toolbar", "2.4.0" ]
aliases = [ "/questions/63293" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireless toolbar](/questions/63293/wireless-toolbar)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-63293-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-63293-score" class="post-score" title="current number of votes">0</div><span id="post-63293-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I have just updated to Wireshark 2.4.0. 64 bit I use the Legacy interface and the Wireless toolbar is greyed out. The 802.11 Channel, Channel Offset and FCS Filter: and Wireless Settings... fields are all greyed out and do nothing. The Decryption Keys... item does work. It worked OK before the update. This has happened on 2 PCs. How do re-enable the full operation of the wireless toolbar.</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-wireless_toolbar" rel="tag" title="see questions tagged &#39;wireless_toolbar&#39;">wireless_toolbar</span> <span class="post-tag tag-link-2.4.0" rel="tag" title="see questions tagged &#39;2.4.0&#39;">2.4.0</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>01 Aug '17, 07:06</strong></p><img src="https://secure.gravatar.com/avatar/857bd443d52039959295d6712101ef25?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="srspanton&#39;s gravatar image" /><p><span>srspanton</span><br />
<span class="score" title="6 reputation points">6</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="2 badges"><span class="bronze">●</span><span class="badgecount">2</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="srspanton has no accepted answers">0%</span></p></div></div><div id="comments-container-63293" class="comments-container"><span id="63296"></span><div id="comment-63296" class="comment"><div id="post-63296-score" class="comment-score"></div><div class="comment-text"><p>What is your host OS?</p></div><div id="comment-63296-info" class="comment-info"><span class="comment-age">(01 Aug '17, 07:37)</span> <span class="comment-user userinfo">grahamb ♦</span></div></div></div><div id="comment-tools-63293" class="comment-tools"></div><div class="clear"></div><div id="comment-63293-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

