+++
type = "question"
title = "Wireshark performance - freezing when stopping capture / closing file"
description = '''I&#x27;m capturing several remote interfaces with rpcap://host/emac0 and saving to files with size 50MB each. After a couple of hours when I try to stop the captures many of them are freezing. With each &quot;stop capture&quot; a new window pops up &quot;Closing file&quot; that will stay on screen forever. The parent window...'''
date = "2010-09-28T06:07:00Z"
lastmod = "2010-10-06T00:43:00Z"
weight = 346
keywords = [ "large", "performance", "closing", "freezing", "file" ]
aliases = [ "/questions/346" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Wireshark performance - freezing when stopping capture / closing file](/questions/346/wireshark-performance-freezing-when-stopping-capture-closing-file)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-346-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-346-score" class="post-score" title="current number of votes">1</div><span id="post-346-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>I'm capturing several remote interfaces with rpcap://host/emac0 and saving to files with size 50MB each. After a couple of hours when I try to stop the captures many of them are freezing. With each "stop capture" a new window pops up "Closing file" that will stay on screen forever. The parent window stays freezing so I cannot close it (only to kill in taskman). Number of "Closing file" windows are actully more then the number of started captures. For 10 captures I got 30 "closing file"-s.</p><p>Anybody experienced the same ? Solution ? Thanks</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-large" rel="tag" title="see questions tagged &#39;large&#39;">large</span> <span class="post-tag tag-link-performance" rel="tag" title="see questions tagged &#39;performance&#39;">performance</span> <span class="post-tag tag-link-closing" rel="tag" title="see questions tagged &#39;closing&#39;">closing</span> <span class="post-tag tag-link-freezing" rel="tag" title="see questions tagged &#39;freezing&#39;">freezing</span> <span class="post-tag tag-link-file" rel="tag" title="see questions tagged &#39;file&#39;">file</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>28 Sep '10, 06:07</strong></p><img src="https://secure.gravatar.com/avatar/d3439144f4810a1b7813b18d35d55993?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="mreg&#39;s gravatar image" /><p><span>mreg</span><br />
<span class="score" title="16 reputation points">16</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="3 badges"><span class="bronze">●</span><span class="badgecount">3</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="mreg has no accepted answers">0%</span></p></div></div><div id="comments-container-346" class="comments-container"><span id="430"></span><div id="comment-430" class="comment"><div id="post-430-score" class="comment-score"></div><div class="comment-text"><p>I have met same problem, but don't know solution yet</p></div><div id="comment-430-info" class="comment-info"><span class="comment-age">(06 Oct '10, 00:43)</span> <span class="comment-user userinfo">nreal</span></div></div></div><div id="comment-tools-346" class="comment-tools"></div><div class="clear"></div><div id="comment-346-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

