+++
type = "question"
title = "Why is &#x27;classicstun&#x27; not working for me?"
description = '''When I&#x27;m trying to get someones IP through Wireshark and call them on steam with the filter &#x27;classicstun&#x27; nothing comes up, it is just blank. Can anyone explain why?'''
date = "2015-11-08T03:37:00Z"
lastmod = "2015-11-08T03:37:00Z"
weight = 47369
keywords = [ "classicstun" ]
aliases = [ "/questions/47369" ]
osqa_answers = 0
osqa_accepted = false
+++

<div class="headNormal">

# [Why is 'classicstun' not working for me?](/questions/47369/why-is-classicstun-not-working-for-me)

</div>

<div id="main-body">

<div id="askform">

<table id="question-table" style="width:100%;"><colgroup><col style="width: 50%" /><col style="width: 50%" /></colgroup><tbody><tr class="odd"><td style="width: 30px; vertical-align: top"><div class="vote-buttons"><span id="post-47369-upvote" class="ajax-command post-vote up" rel="nofollow" title="I like this post (click again to cancel)"> </span><div id="post-47369-score" class="post-score" title="current number of votes">0</div><span id="post-47369-downvote" class="ajax-command post-vote down" rel="nofollow" title="I dont like this post (click again to cancel)"> </span> <span id="favorite-mark" class="ajax-command favorite-mark" rel="nofollow" title="mark/unmark this question as favorite (click again to cancel)"> </span><div id="favorite-count" class="favorite-count"></div></div></td><td><div id="item-right"><div class="question-body"><p>When I'm trying to get someones IP through Wireshark and call them on steam with the filter 'classicstun' nothing comes up, it is just blank. Can anyone explain why?</p></div><div id="question-tags" class="tags-container tags"><span class="post-tag tag-link-classicstun" rel="tag" title="see questions tagged &#39;classicstun&#39;">classicstun</span></div><div id="question-controls" class="post-controls"></div><div class="post-update-info-container"><div class="post-update-info post-update-info-user"><p>asked <strong>08 Nov '15, 03:37</strong></p><img src="https://secure.gravatar.com/avatar/2505d5cab1b313c8414de84ba052b9d5?s=32&amp;d=identicon&amp;r=g" class="gravatar" width="32" height="32" alt="Marsyy&#39;s gravatar image" /><p><span>Marsyy</span><br />
<span class="score" title="1 reputation points">1</span><span title="1 badges"><span class="badge1">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="silver">●</span><span class="badgecount">1</span></span><span title="1 badges"><span class="bronze">●</span><span class="badgecount">1</span></span><br />
<span class="accept_rate" title="Rate of the user&#39;s accepted answers">accept rate:</span> <span title="Marsyy has no accepted answers">0%</span></p></div></div><div id="comments-container-47369" class="comments-container"></div><div id="comment-tools-47369" class="comment-tools"></div><div class="clear"></div><div id="comment-47369-form-container" class="comment-form-container"></div><div class="clear"></div></div></td></tr></tbody></table>

</div>

</div>

